<?php return array (
  'sans-serif' => array(
    'normal' => $rootDir . '\lib\fonts\Helvetica',
    'bold' => $rootDir . '\lib\fonts\Helvetica-Bold',
    'italic' => $rootDir . '\lib\fonts\Helvetica-Oblique',
    'bold_italic' => $rootDir . '\lib\fonts\Helvetica-BoldOblique',
  ),
  'times' => array(
    'normal' => $rootDir . '\lib\fonts\Times-Roman',
    'bold' => $rootDir . '\lib\fonts\Times-Bold',
    'italic' => $rootDir . '\lib\fonts\Times-Italic',
    'bold_italic' => $rootDir . '\lib\fonts\Times-BoldItalic',
  ),
  'times-roman' => array(
    'normal' => $rootDir . '\lib\fonts\Times-Roman',
    'bold' => $rootDir . '\lib\fonts\Times-Bold',
    'italic' => $rootDir . '\lib\fonts\Times-Italic',
    'bold_italic' => $rootDir . '\lib\fonts\Times-BoldItalic',
  ),
  'courier' => array(
    'normal' => $rootDir . '\lib\fonts\Courier',
    'bold' => $rootDir . '\lib\fonts\Courier-Bold',
    'italic' => $rootDir . '\lib\fonts\Courier-Oblique',
    'bold_italic' => $rootDir . '\lib\fonts\Courier-BoldOblique',
  ),
  'helvetica' => array(
    'normal' => $rootDir . '\lib\fonts\Helvetica',
    'bold' => $rootDir . '\lib\fonts\Helvetica-Bold',
    'italic' => $rootDir . '\lib\fonts\Helvetica-Oblique',
    'bold_italic' => $rootDir . '\lib\fonts\Helvetica-BoldOblique',
  ),
  'zapfdingbats' => array(
    'normal' => $rootDir . '\lib\fonts\ZapfDingbats',
    'bold' => $rootDir . '\lib\fonts\ZapfDingbats',
    'italic' => $rootDir . '\lib\fonts\ZapfDingbats',
    'bold_italic' => $rootDir . '\lib\fonts\ZapfDingbats',
  ),
  'symbol' => array(
    'normal' => $rootDir . '\lib\fonts\Symbol',
    'bold' => $rootDir . '\lib\fonts\Symbol',
    'italic' => $rootDir . '\lib\fonts\Symbol',
    'bold_italic' => $rootDir . '\lib\fonts\Symbol',
  ),
  'serif' => array(
    'normal' => $rootDir . '\lib\fonts\Times-Roman',
    'bold' => $rootDir . '\lib\fonts\Times-Bold',
    'italic' => $rootDir . '\lib\fonts\Times-Italic',
    'bold_italic' => $rootDir . '\lib\fonts\Times-BoldItalic',
  ),
  'monospace' => array(
    'normal' => $rootDir . '\lib\fonts\Courier',
    'bold' => $rootDir . '\lib\fonts\Courier-Bold',
    'italic' => $rootDir . '\lib\fonts\Courier-Oblique',
    'bold_italic' => $rootDir . '\lib\fonts\Courier-BoldOblique',
  ),
  'fixed' => array(
    'normal' => $rootDir . '\lib\fonts\Courier',
    'bold' => $rootDir . '\lib\fonts\Courier-Bold',
    'italic' => $rootDir . '\lib\fonts\Courier-Oblique',
    'bold_italic' => $rootDir . '\lib\fonts\Courier-BoldOblique',
  ),
  'dejavu sans' => array(
    'bold' => $rootDir . '\lib\fonts\DejaVuSans-Bold',
    'bold_italic' => $rootDir . '\lib\fonts\DejaVuSans-BoldOblique',
    'italic' => $rootDir . '\lib\fonts\DejaVuSans-Oblique',
    'normal' => $rootDir . '\lib\fonts\DejaVuSans',
  ),
  'dejavu sans mono' => array(
    'bold' => $rootDir . '\lib\fonts\DejaVuSansMono-Bold',
    'bold_italic' => $rootDir . '\lib\fonts\DejaVuSansMono-BoldOblique',
    'italic' => $rootDir . '\lib\fonts\DejaVuSansMono-Oblique',
    'normal' => $rootDir . '\lib\fonts\DejaVuSansMono',
  ),
  'dejavu serif' => array(
    'bold' => $rootDir . '\lib\fonts\DejaVuSerif-Bold',
    'bold_italic' => $rootDir . '\lib\fonts\DejaVuSerif-BoldItalic',
    'italic' => $rootDir . '\lib\fonts\DejaVuSerif-Italic',
    'normal' => $rootDir . '\lib\fonts\DejaVuSerif',
  ),
  'glyphicons halflings' => array(
    'normal' => $fontDir . '\c24eff5aed0853cce68b01a0a14e92de',
  ),
  'fontawesome' => array(
    'normal' => $fontDir . '\fb1827e21d747dd0df355f152062794d',
  ),
  'simple-line-icons' => array(
    'normal' => $fontDir . '\0a9f2fd33249b4855d1cc4550a092ab2',
  ),
  'weathericons' => array(
    'normal' => $fontDir . '\e32b7329f7053507884226fd1c33c7c1',
  ),
  'linea-arrows-10' => array(
    'normal' => $fontDir . '\5c507ed68c3b07169f3eafa8094372db',
  ),
  'linea-basic-10' => array(
    'normal' => $fontDir . '\92305395745a4e44ee10cfde583a029b',
  ),
  'linea-basic-elaboration-10' => array(
    'normal' => $fontDir . '\bb86e181463150b1d8b9775c9f492444',
  ),
  'linea-ecommerce-10' => array(
    'normal' => $fontDir . '\705bfa97fb6d33b18e0efe7578693b1f',
  ),
  'linea-music-10' => array(
    'normal' => $fontDir . '\82c6c27316fb7d3c157e789aac8f7881',
  ),
  'linea-software-10' => array(
    'normal' => $fontDir . '\47a560dfe77877f6808067d782630043',
  ),
  'linea-weather-10' => array(
    'normal' => $fontDir . '\bdfcf6f17fc07d71a26b2d436c5eb3a5',
  ),
  'themify' => array(
    'normal' => $fontDir . '\7ad5939cb5f707d181bb98e2686aef96',
  ),
  'material design icons' => array(
    'normal' => $fontDir . '\54b619d3586fdd17af1491d1d43e0fbc',
  ),
) ?>
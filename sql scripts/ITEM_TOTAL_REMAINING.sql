drop procedure if exists get_item_remaining_for_beginning_of_month;
drop function if exists get_item_remaining_for_beginning_of_month;
drop function if exists date_to_month;

DELIMITER $$
create function date_to_month(date_ date) returns int deterministic
begin
	declare converted_date date default date(date_);
    return year(converted_date) * 12 + month(converted_date);
end;

create procedure get_item_remaining_for_beginning_of_month(in item_id_ int, in date_ date, out result float)
begin
    declare deadline_in_months int default date_to_month(date_);
    declare curr_month int default date_to_month(curdate());
    
    declare tmp_consumption float default 0;
	set result = 0;
    
	select IFNULL(sum(dream_transaction_list.amount*dream_transaction_types.operation), 0) into result from dream_transaction_list
	inner join dream_transaction_storage_bind on dream_transaction_storage_bind.transaction_id=dream_transaction_list.transaction_id
	inner join dream_transaction_types on dream_transaction_types.id=dream_transaction_storage_bind.operation_id 
	inner join dream_transactions on dream_transactions.id=dream_transaction_list.transaction_id
	where dream_transaction_list.item_id=item_id_ and dream_transactions.create_time < date_;
	
    if curdate() < date_ then
		select IFNULL(sum(dream_monthly_plan.production_count-dream_monthly_plan.sales_count) + result, result) into result from dream_monthly_plan
        where dream_monthly_plan.month < deadline_in_months and dream_monthly_plan.month >= curr_month and dream_monthly_plan.item_id=item_id_;
        
        while deadline_in_months > curr_month do
            call get_consumption_by_plan(item_id_, subdate(date_, interval (deadline_in_months - curr_month) month), 0, tmp_consumption);
			set result = result - tmp_consumption;
            
            set deadline_in_months = deadline_in_months - 1;
        end while;
    end if;
end;

create function get_item_remaining_for_beginning_of_month(item_id_ int, date_ date) returns float deterministic
begin
	declare result float default 0;
    call get_item_remaining_for_beginning_of_month(item_id_, date_, result);
    
    return result;
end;
$$


#select get_item_remaining_for_beginning_of_month(38, '2019-05-01');

set @@GLOBAL.max_sp_recursion_depth = 255;
set @@SESSION.max_sp_recursion_depth = 255;

drop procedure if exists get_consumption_by_plan;
drop function if exists get_consumption_by_plan;

DELIMITER $$
create procedure get_consumption_by_plan(in item_id_ int, in date_ date, in level_ int, out total_consumption_ float)
begin
	declare curr_parent_id int;
    declare curr_parent_plan int;
    declare curr_consumption_by_parent float;
    declare parent_consumption float default 0;
	declare remaining float default 0;
    
	declare done tinyint default false;
    declare done_holder tinyint default false;
    
	declare parents_cursor cursor for select parent_id, quantity from dream_item_bind where child_id = item_id_;
    declare continue handler for not found set done = true;
    
    set total_consumption_ = 0;
    set done = 0;
    
    open parents_cursor;
    parents_loop:
    loop
        fetch next from parents_cursor into curr_parent_id, curr_consumption_by_parent;
        if done then
			leave parents_loop;
        else
			set done_holder = done;
			select sum( production_count ) into curr_parent_plan from dream_monthly_plan where dream_monthly_plan.item_id = curr_parent_id and dream_monthly_plan.month = date_to_month(date_) limit 1;
            if curr_parent_plan is null then
				call get_consumption_by_plan(curr_parent_id, date_, level_ + 1, parent_consumption);
				set total_consumption_ = total_consumption_ + curr_consumption_by_parent * parent_consumption;
            elseif curr_parent_plan > 0 then
				set total_consumption_ = total_consumption_ + curr_parent_plan * curr_consumption_by_parent;
            end if;
            set done = done_holder;
        end if;
    end loop;
    
    if level_ > 0 and total_consumption_ > 0 then
		call get_item_remaining_for_beginning_of_month(item_id_, date_, remaining);
        if remaining > total_consumption_ then
			set total_consumption_ = 0;
        elseif remaining > 0 then
			set total_consumption_ = total_consumption_ - remaining;
        end if;
    end if;
end;

create function get_consumption_by_plan(item_id_ int, date_ date) returns float deterministic
begin
	declare result float default 0;
    call get_consumption_by_plan(item_id_, date_, 0, result);
    
    return result;
end;
$$
select get_consumption_by_plan(416, '2020-08-01');
#select get_item_remaining_for_beginning_of_month(416, '2019-08-01');
<?php

namespace App\Providers;

use App\DreamDailyPlan;
use App\DreamItem;
use App\DreamMonthlyPlan;
use App\DreamOrder;
use App\DreamStorage;
use App\DreamTransaction;
use App\DreamRequest;
use App\Policies\DreamItemPolicy;
use App\Policies\DreamOrderPolicy;
use App\Policies\DreamPlanPolicy;
use App\Policies\DreamStoragePolicy;
use App\Policies\DreamTransactionPolicy;
use App\Policies\DreamTransactionRequestPolicy;
use App\Policies\UserPolicy;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        DreamItem::class => DreamItemPolicy::class,
        User::class => UserPolicy::class,
        DreamRequest::class => DreamTransactionRequestPolicy::class,
        DreamTransaction::class => DreamTransactionPolicy::class,
        DreamMonthlyPlan::class => DreamPlanPolicy::class,
        DreamDailyPlan::class => DreamPlanPolicy::class,
        DreamOrder::class => DreamOrderPolicy::class,
        DreamStorage::class => DreamStoragePolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        date_default_timezone_set('Asia/Tashkent');
        Gate::define('VIEW_IMPORT_PAGE', function ($user){
            return $user->canDo('VIEW_IMPORT_PAGE', true);
        });

        Gate::define('VIEW_USERS_PAGE', function ($user){
            return $user->canDo('VIEW_USERS_PAGE', true);
        });
    }
}

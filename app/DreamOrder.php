<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DreamOrder extends Model
{
    //
    protected $table = 'dream_orders';
    protected $fillable = ['user_id', 'description', 'invoice_number', 'supplier_id', 'state_id', 'shipment_time', 'billing_id', 'delete_time'];
    public $timestamps = false;
}

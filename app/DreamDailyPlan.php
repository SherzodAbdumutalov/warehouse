<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DreamDailyPlan extends Model
{
    //

    protected $table = 'dream_daily_plan';
    protected $fillable = ['monthly_plan_id', 'day', 'production_count'];
    public $timestamps = false;
}

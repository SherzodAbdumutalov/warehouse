<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DreamSupplier extends Model
{
    protected $table = 'dream_suppliers';
    protected $fillable = ['name', 'address', 'phone', 'website', 'type', 'country', 'is_active'];
    public $timestamps = false;
}

<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DreamOrderPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function save(User $user){
        return $user->canDo('SAVE_ORDER', true);
    }

    public function read(User $user){
        return $user->canDo('READ_ORDER_PAGE', true);
    }

    public function delete(User $user){
        return $user->canDo('DELETE_ORDER', true);
    }

    public function viewOrderPrice(User $user){
        return $user->canDo('VIEW_ORDER_PRICE', true);
    }

    public function enterLogisticsPrices(User $user){
        return $user->canDo('ENTER_LOGISTICS_PRICE', true);
    }

    public function viewLogisticsPricePerUnit(User $user){
        return $user->canDo('VIEW_LOGISTICS_COST_PER_UNIT', true);
    }
}

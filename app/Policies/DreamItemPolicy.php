<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DreamItemPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function save(User $user){
        return $user->canDo('ADD_ITEM', true);
    }

    public function delete(User $user){
        return $user->canDo('DELETE_ITEM', true);
    }

    public function add_item_to_norm(User $user){
        return $user->canDo('ADD_ITEM_TO_NORM', true);
    }
}

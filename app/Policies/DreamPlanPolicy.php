<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DreamPlanPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function save(User $user){
        return $user->canDo('ADD_PLAN', true);
    }

    public function view(User $user){
        return $user->canDo('VIEW_PLAN', true);
    }
}

<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DreamTransactionRequestPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function approve_request(User $user){
        return $user->canDo('APPROVE_REQUEST', true);
    }

    public function add_request(User $user){
        return $user->canDo('ADD_REQUEST', true);
    }
}

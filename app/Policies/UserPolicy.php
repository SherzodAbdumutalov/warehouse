<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function add_user(User $user){
        return $user->canDo('ADD_USER', true);
    }

    public function delete_user(User $user){
        return $user->canDo('DELETE_USER', true);
    }
}

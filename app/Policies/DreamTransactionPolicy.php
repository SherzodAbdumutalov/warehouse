<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DreamTransactionPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function allow_input_output_operation(User $user){
        return $user->canDo('ALLOW_INPUT_OUTPUT_OPERATION', true);
    }

    public function delete_transaction_list_item(User $user){
        return $user->canDo('DELETE_TRANSACTION_LIST_ITEM', true);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DreamMonthlyPlan extends Model
{
    protected $table = 'dream_monthly_plan';
    protected $fillable = ['month', 'item_id', 'production_count', 'sales_count', 'user_id'];
    public $timestamps = false;

    public function dream_items(){
        return $this->hasMany('App\DreamItem', 'item_id');
    }

    public function users(){
        return $this->hasMany('App\User', 'user_id');
    }

    public static function change_name_language($month_num){
        $m = '';
        switch ($month_num){
            case 1: $m='январь'; break;
            case 2: $m='февраль'; break;
            case 3: $m='март'; break;
            case 4: $m='апрель'; break;
            case 5: $m='май'; break;
            case 6: $m='июнь'; break;
            case 7: $m='июль'; break;
            case 8: $m='август'; break;
            case 9: $m='сентябрь'; break;
            case 10: $m='октябрь'; break;
            case 11: $m='ноябрь'; break;
            case 12: $m='декабрь'; break;
        }

        return $m;
    }
}

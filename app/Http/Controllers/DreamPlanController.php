<?php

namespace App\Http\Controllers;

use App\DreamDailyPlan;
use App\DreamItem;
use App\DreamMonthlyPlan;
use App\DreamUnit;
use App\DreamStorage;
use App\Policies\DreamPlanPolicy;
use Carbon\Carbon;
use function foo\func;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class DreamPlanController extends Controller
{
    public function index(){
        if (Gate::denies('view', new DreamMonthlyPlan())){
            return response()->json(['errors'=>array('нет доступа!')], 500);
        }
        $storages = DreamStorage::whereIn('type_id', array(1,7))->whereNotIn('id', array(3,23))->get();
        $plan_storages = DreamStorage::where('type_id', 1)->whereIn('id', array(3,23))->get();
        return view('index', ['page'=>'plans.plan', 'storages'=>$storages, 'plan_storages'=>$plan_storages]);
    }

    public function get_plan_by_month(Request $request){
        if (Gate::denies('view', new DreamMonthlyPlan())){
            return response()->json(['errors'=>array('нет доступа!')], 500);
        }
        $columns = array(
            0 => 'item_name',
            1 => 'articula_new',
            2 => 'sap_code',
            3 => 'last_remaining',
            4 => 'production_count',
            5 => 'sales_count',
            6 => 'predictedCount',
            7 => 'remaining',

        );

        $totalProduction=0;
        $totalSale=0;
        $totalStartRemain=0;
        $totalAvailableRemain=0;
        $totalEndRemain=0;

        $totalData = DreamItem::where('articula_new', 'LIKE', '3%')->count();
        if ($request->input('length')==-1){
            $limit = $totalData;
        }else{
            $limit = $request->input('length');
        }
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $curdate = Carbon::now();
        $curMonth_id = $curdate->format('Y')*12 + $curdate->format('m');
        $date = Carbon::createFromFormat('Y-m', $request->plan_date);
        $month_id = $date->format('Y')*12 + $date->format('m');
        $lastdate = Carbon::createFromFormat('Y-m', $request->plan_date)->endOfMonth()->format('Y-m-d');

        if ($month_id>=$curMonth_id){
            $query_text = "
                select SQL_CALC_FOUND_ROWS  i.id, i.item_name, i.articula_new, i.sap_code, u.unit,
                sum(tl.amount * tt.operation) as remaining,
                sum(COALESCE(tl1.amount, 0) * tt.operation) as last_remaining,
                sum(COALESCE(tl1.amount, 0) * tt.operation) + IFNULL(monthly.result, 0) as predictedCount,
                dmp.production_count, dmp.sales_count
                from dream_items i
                inner join dream_storage_item_bind on dream_storage_item_bind.item_id = i.id
                left join dream_monthly_plan dmp on dmp.item_id=i.id and dmp.month = $month_id
                inner join dream_transaction_list tl on i.id = tl.item_id
                inner join dream_transactions t on tl.transaction_id = t.id
                inner join dream_transaction_storage_bind tsb on t.id = tsb.transaction_id
                inner join dream_transaction_types tt on tsb.operation_id = tt.id
                inner join dream_storages s on tsb.storage_id = s.id
                inner join dream_units u on tl.unit_id = u.id
                left join
                (
                select tsb1.storage_id as storage_id, max(t1.id) as transaction_id
                from dream_transactions t1
                inner join dream_transaction_storage_bind tsb1 on t1.id = tsb1.transaction_id
                where t1.is_inventory = 1 and date(t1.create_time) <= date('$lastdate')
                group by tsb1.storage_id
                ) last_inv_by_storage on s.id = last_inv_by_storage.storage_id
                left join dream_transaction_list tl1 on tl.id = tl1.id and tl1.transaction_id <= last_inv_by_storage.transaction_id
                left join (
                select sum(dream_monthly_plan.production_count - dream_monthly_plan.sales_count) as result, dream_monthly_plan.item_id from dream_monthly_plan
                where dream_monthly_plan.month between $curMonth_id and  $month_id
                group by dream_monthly_plan.item_id
                ) as monthly on monthly.item_id = i.id
                where tl.delete_time is null and date(t.create_time) <= date('$lastdate') and dream_storage_item_bind.storage_id = $request->plan_storage_id
            ";
        }else{
            $query_text = "
                select SQL_CALC_FOUND_ROWS  i.id, i.item_name, i.articula_new, i.sap_code, u.unit,
                sum(tl.amount * tt.operation) as remaining,
                sum(COALESCE(tl1.amount, 0) * tt.operation) as last_remaining,
                sum(tl.amount * tt.operation) as predictedCount,
                dmp.production_count, dmp.sales_count
                from dream_items i
                inner join dream_storage_item_bind on dream_storage_item_bind.item_id = i.id
                left join dream_monthly_plan dmp on dmp.item_id=i.id and dmp.month = $month_id
                inner join dream_transaction_list tl on i.id = tl.item_id
                inner join dream_transactions t on tl.transaction_id = t.id
                inner join dream_transaction_storage_bind tsb on t.id = tsb.transaction_id
                inner join dream_transaction_types tt on tsb.operation_id = tt.id
                inner join dream_storages s on tsb.storage_id = s.id
                inner join dream_units u on tl.unit_id = u.id
                left join
                (
                select tsb1.storage_id as storage_id, max(t1.id) as transaction_id
                from dream_transactions t1
                inner join dream_transaction_storage_bind tsb1 on t1.id = tsb1.transaction_id
                where t1.is_inventory = 1 and date(t1.create_time) <= date('$lastdate')
                group by tsb1.storage_id
                ) last_inv_by_storage on s.id = last_inv_by_storage.storage_id
                left join dream_transaction_list tl1 on tl.id = tl1.id and tl1.transaction_id <= last_inv_by_storage.transaction_id
                left join (
                select sum(dream_monthly_plan.production_count) as result, dream_monthly_plan.item_id from dream_monthly_plan
                where dream_monthly_plan.month between $curMonth_id and  $month_id
                group by dream_monthly_plan.item_id
                ) as monthly on monthly.item_id = i.id
                where tl.delete_time is null and date(t.create_time) <= date('$lastdate') and dream_storage_item_bind.storage_id = $request->plan_storage_id
            ";
        }


        $search_text = '';
        if (!empty($request->input('search.value'))){
            $search = $request->input('search.value');
            $search_text .= "
                     and ( i.articula_new like '%$search%' || i.articula_old like '%$search%' || i.sap_code like '%$search%' || i.item_name like '%$search%' )
                    ";
        }
        $query_text.=$search_text." group by i.id, u.id, dmp.production_count, dmp.sales_count order by $order $dir limit $limit offset $start;";
        $items = DB::select($query_text);
        $totalCount = DB::select('select found_rows() as total_count')[0];
        $data = array();
        if (!empty($items)){
            foreach ($items as $item){
                $nestedData['DT_RowId'] = $item->id;
                $nestedData['id'] = $item->id;
                $nestedData['item_name'] = $item->item_name;
                $nestedData['articula_new'] = $item->articula_new;
                $nestedData['sap_code'] = $item->sap_code;
                $nestedData['production_count'] = $item->production_count;
                $nestedData['sales_count'] = $item->sales_count;
                $nestedData['remaining_at_the_beginning'] = $item->last_remaining;
                $nestedData['predictedCount'] = $item->predictedCount;
                $nestedData['remaining'] = $item->remaining;

                $data[] = $nestedData;

                $totalAvailableRemain +=$item->remaining;
                $totalSale += $item->sales_count;
                $totalStartRemain += $item->last_remaining;
                $totalEndRemain += $item->predictedCount;
                $totalProduction += $item->production_count;
            }
        }
        $json_data = array(
            "draw"                  => intval($request->input('draw')),
            "recordsTotal"          => intval($totalData),
            "recordsFiltered"       => intval($totalCount->total_count),
            "data"                  => $data,
            "total_remaining"       => $totalAvailableRemain,
            "total_start_remaining" => $totalStartRemain,
            "total_end_remaining"   => $totalEndRemain,
            "total_sale"            => $totalSale,
            "total_production"      => $totalProduction
        );

        return json_encode($json_data);
    }

    public function getPlanStatistics(Request $request){
        if (Gate::denies('view', new DreamMonthlyPlan())){
            return response()->json(['errors'=>array('нет доступа!')], 500);
        }
        $columns = array(
            0 => 'item_name',
            1 => 'articula_new',
            2 => 'sap_code',
            3 => 'remaining_at_the_beginning',
            4 => 'orderCount',
            5 => 'inputCount',
            6 => 'outputCount',
            7 => 'consumption',
            8 => 'remaining_at_the_end'
        );

        $storage_arr = '';
        $counter=0;
        if (!is_null($request->storage_id)){

            foreach ($request->storage_id as $storage_id){
                $counter++;
                if($counter==count($request->storage_id)){
                    $storage_arr .= $storage_id.' ';
                }
                else{
                    $storage_arr .= $storage_id.', ';
                }

            }
            $storage_arr = substr($storage_arr, 0, strlen($storage_arr)-1);
            $storage_arr = " and tsb.storage_id in($storage_arr)";
        }
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        $totalData = DB::table('dream_transaction_list')
            ->join('dream_transaction_storage_bind', 'dream_transaction_storage_bind.transaction_id', '=', 'dream_transaction_list.transaction_id')->count();

        if ($request->input('length')==-1){
            $limit = $totalData;
        }else{
            $limit = $request->input('length');
        }
        $curdate = Carbon::now();
        $curMonth_id = $curdate->format('Y')*12 + $curdate->format('m');
        $date = Carbon::createFromFormat('Y-m', $request->plan_date);
        $month_id = $date->format('Y')*12 + $date->format('m');
        $lastdate = Carbon::createFromFormat('Y-m', $request->plan_date)->endOfMonth()->format('Y-m-d');
        $startdate = Carbon::createFromFormat('Y-m', $request->plan_date)->startOfMonth()->format('Y-m-d');

        if ($curMonth_id<=$month_id){
            $query_text = "
                select SQL_CALC_FOUND_ROWS  i.id, i.id as DT_RowId, i.item_name, i.articula_new, i.sap_code, u.unit,
                sum(tl.amount * tt.operation) as remaining,
                ROUND(sum(COALESCE(tl1.amount, 0) * tt.operation), 3) as remaining_at_the_beginning,
                ROUND(IFNULL(monthly.consumption, 0), 3) as consumption,
                ROUND(sum(COALESCE(tl1.amount, 0) * tt.operation) - IFNULL(monthly.consumption, 0), 3) as remaining_at_the_end,
                IFNULL(orders.orderCount, 0) as orderCount,
                ROUND(IFNULL(iput.isum, 0), 3) as inputCount,
                ROUND(IFNULL(oput.osum, 0), 3) as outputCount
                from dream_items i
                inner join dream_transaction_list tl on i.id = tl.item_id
                inner join dream_transactions t on tl.transaction_id = t.id
                inner join dream_transaction_storage_bind tsb on t.id = tsb.transaction_id
                inner join dream_transaction_types tt on tsb.operation_id = tt.id
                inner join dream_storages s on tsb.storage_id = s.id
                inner join dream_units u on tl.unit_id = u.id
                left join
                (
                select tsb1.storage_id as storage_id, max(t1.id) as transaction_id
                from dream_transactions t1
                inner join dream_transaction_storage_bind tsb1 on t1.id = tsb1.transaction_id
                where t1.is_inventory = 1 and date(t1.create_time) <= date('$lastdate')
                group by tsb1.storage_id
                ) last_inv_by_storage on s.id = last_inv_by_storage.storage_id
                left join dream_transaction_list tl1 on tl.id = tl1.id and tl1.transaction_id <= last_inv_by_storage.transaction_id
                left join (
                select sum(dream_monthly_plan.production_count*dib.quantity) as consumption, dib.child_id, dib.unit_id from dream_monthly_plan
                inner join dream_item_bind dib on dib.parent_id = dream_monthly_plan.item_id
                where dream_monthly_plan.month between $curMonth_id and $month_id
                group by dib.child_id, dib.unit_id
                ) as monthly on monthly.child_id = tl.item_id and monthly.unit_id = u.id
                left join (
                select sum(dream_order_list.amount) as orderCount, dream_order_list.item_id, dream_order_list.unit_id from dream_orders
                inner join dream_order_list on dream_order_list.order_id = dream_orders.id
                where dream_orders.create_time between date('$startdate') and date('$lastdate')
                group by dream_order_list.item_id, dream_order_list.unit_id
                ) as orders on orders.item_id = i.id and orders.unit_id = u.id
                left join (
                select tl1.item_id, tl1.unit_id, sum(tl1.amount) as isum from dream_transactions dt1
                inner join dream_transaction_storage_bind tsb on tsb.transaction_id = dt1.id
                inner join dream_transaction_types tt1 on tt1.id = tsb.operation_id
                inner join dream_transaction_list tl1 on tl1.transaction_id = dt1.id
                inner join dream_storages ds1 on ds1.id = tsb.storage_id
                where date(dt1.create_time) between date('$startdate') and date('$lastdate') and ds1.type_id = 1 and tt1.operation = 1 and tt1.id not in (5)
                and tl1.delete_time is null
                group by tl1.item_id, tl1.unit_id
                ) as iput on iput.item_id = tl.item_id and iput.unit_id = tl.unit_id
                left join (
                select tl1.item_id, tl1.unit_id, sum(tl1.amount) as osum from dream_transactions dt1
                inner join dream_transaction_storage_bind tsb on tsb.transaction_id = dt1.id
                inner join dream_transaction_types tt1 on tt1.id = tsb.operation_id
                inner join dream_transaction_list tl1 on tl1.transaction_id = dt1.id
                inner join dream_storages ds1 on ds1.id = tsb.storage_id
                where date(dt1.create_time) between date('$startdate') and date('$lastdate') and ds1.type_id = 1 and tt1.operation = -1 and tt1.id not in (6)
                and tl1.delete_time is null
                group by tl1.item_id, tl1.unit_id
                ) as oput on oput.item_id = tl.item_id and oput.unit_id = tl.unit_id
                where tl.delete_time is null and date(t.create_time) <= date('$lastdate') and s.type_id = 1 $storage_arr
            ";
        }else{
            $query_text = "
                select SQL_CALC_FOUND_ROWS  i.id, i.id as DT_RowId, i.item_name, i.articula_new, i.sap_code, u.unit,
                sum(tl.amount * tt.operation) as remaining,
                ROUND(sum(COALESCE(tl1.amount, 0) * tt.operation), 3) as remaining_at_the_beginning,
                ROUND(IFNULL(monthly.consumption, 0), 3) as consumption,
                sum(tl.amount * tt.operation) as remaining_at_the_end,
                IFNULL(orders.orderCount, 0) as orderCount,
                ROUND(IFNULL(iput.isum, 0), 3) as inputCount,
                ROUND(IFNULL(oput.osum, 0), 3) as outputCount
                from dream_items i
                inner join dream_transaction_list tl on i.id = tl.item_id
                inner join dream_transactions t on tl.transaction_id = t.id
                inner join dream_transaction_storage_bind tsb on t.id = tsb.transaction_id
                inner join dream_transaction_types tt on tsb.operation_id = tt.id
                inner join dream_storages s on tsb.storage_id = s.id
                inner join dream_units u on tl.unit_id = u.id
                left join
                (
                select tsb1.storage_id as storage_id, max(t1.id) as transaction_id
                from dream_transactions t1
                inner join dream_transaction_storage_bind tsb1 on t1.id = tsb1.transaction_id
                where t1.is_inventory = 1 and date(t1.create_time) <= date('$lastdate')
                group by tsb1.storage_id
                ) last_inv_by_storage on s.id = last_inv_by_storage.storage_id
                left join dream_transaction_list tl1 on tl.id = tl1.id and tl1.transaction_id <= last_inv_by_storage.transaction_id
                left join (
                select sum(dream_monthly_plan.production_count*dib.quantity) as consumption, dib.child_id, dib.unit_id from dream_monthly_plan
                inner join dream_item_bind dib on dib.parent_id = dream_monthly_plan.item_id
                where dream_monthly_plan.month between $curMonth_id and $month_id
                group by dib.child_id, dib.unit_id
                ) as monthly on monthly.child_id = tl.item_id and monthly.unit_id = u.id
                left join (
                select sum(dream_order_list.amount) as orderCount, dream_order_list.item_id, dream_order_list.unit_id from dream_orders
                inner join dream_order_list on dream_order_list.order_id = dream_orders.id
                where dream_orders.create_time between date('$startdate') and date('$lastdate')
                group by dream_order_list.item_id, dream_order_list.unit_id
                ) as orders on orders.item_id = i.id and orders.unit_id = u.id
                left join (
                select tl1.item_id, tl1.unit_id, sum(tl1.amount) as isum from dream_transactions dt1
                inner join dream_transaction_storage_bind tsb on tsb.transaction_id = dt1.id
                inner join dream_transaction_types tt1 on tt1.id = tsb.operation_id
                inner join dream_transaction_list tl1 on tl1.transaction_id = dt1.id
                inner join dream_storages ds1 on ds1.id = tsb.storage_id
                where date(dt1.create_time) between date('$startdate') and date('$lastdate') and ds1.type_id = 1 and tt1.operation = 1 and tt1.id not in (5)
                and tl1.delete_time is null
                group by tl1.item_id, tl1.unit_id
                ) as iput on iput.item_id = tl.item_id and iput.unit_id = tl.unit_id
                left join (
                select tl1.item_id, tl1.unit_id, sum(tl1.amount) as osum from dream_transactions dt1
                inner join dream_transaction_storage_bind tsb on tsb.transaction_id = dt1.id
                inner join dream_transaction_types tt1 on tt1.id = tsb.operation_id
                inner join dream_transaction_list tl1 on tl1.transaction_id = dt1.id
                inner join dream_storages ds1 on ds1.id = tsb.storage_id
                where date(dt1.create_time) between date('$startdate') and date('$lastdate') and ds1.type_id = 1 and tt1.operation = -1 and tt1.id not in (6)
                and tl1.delete_time is null
                group by tl1.item_id, tl1.unit_id
                ) as oput on oput.item_id = tl.item_id and oput.unit_id = tl.unit_id
                where tl.delete_time is null and date(t.create_time) <= date('$lastdate') and s.type_id = 1 $storage_arr
            ";
        }

        $search_text = '';
        if (!empty($request->input('search.value'))){
            $search = $request->input('search.value');
            $search_text .= "
                     and ( i.articula_new like '%$search%' || i.articula_old like '%$search%' || i.sap_code like '%$search%' || i.item_name like '%$search%' )
                    ";
        }
        $query_text.=$search_text." group by i.id, u.id order by $order $dir limit $limit offset $start;";
        $items = DB::select($query_text);
        $totalCount = DB::select('select found_rows() as total_count')[0];
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalCount->total_count),
            "data"            => $items
        );

        return json_encode($json_data);
    }

    public function set_plan_values(Request $request){
        $item_id = $request->item_id;
        $month_id = $request->month_id;
        $month_data = DB::table('dream_monthly_plan')->where([['month', $month_id], ['item_id', $item_id]])->get()->first();
        $data = [
            'month_data' => $month_data
        ];
        return response()->json($data);
    }

    public function planByModal(Request $request){
        $item_id = $request->item_id;
        $month_id = $request->month_id;
//        $date = Carbon::createFromDate(($month_id-$month_id%12)/12, $month_id%12)->format('Y-m-d');
        $month_data = DB::table('dream_monthly_plan')->where([['month', $month_id], ['item_id', $item_id]])->get()->first();
        $str = '';
        if (is_null($request->production_count)){
            if (!is_null($month_data)){
                $str = 'and dream_monthly_plan.id != '.$month_data->id;
                $production_count = $month_data->production_count;
            }else{
                $production_count = 0;
            }
        }else{
            if ($request->production_count == 0){
                if (!is_null($month_data)){
                    $str = 'and dream_monthly_plan.id != '.$month_data->id;
                }

            }
            $production_count = $request->production_count;

        }

        $curdate = Carbon::now();
        $curMonth_id = $curdate->format('Y')*12 + $curdate->format('m');
        $date = Carbon::createFromFormat('Y-m', $request->plan_date);
        $month_id = $date->format('Y')*12 + $date->format('m');
        $lastdate = Carbon::createFromFormat('Y-m', $request->plan_date)->endOfMonth()->format('Y-m-d');

        $query_text = "
            select i.item_name, i.articula_new, i.sap_code, u.unit,
            sum(tl.amount * tt.operation) as remaining,
            ROUND(sum(COALESCE(tl1.amount, 0) * tt.operation), 3) as remaining_at_the_beginning,
            ROUND(IFNULL(monthly.consumption, 0), 3) as consumption,
            ROUND(IFNULL((dib.quantity * $production_count), 0), 3) as total_consumption_by_plan,
            ROUND(sum(COALESCE(tl1.amount, 0) * tt.operation) - IFNULL(monthly.consumption, 0) - IFNULL((dib.quantity * $production_count), 0), 3) as remaining_at_the_end,
            dib.quantity as consumption_by_norm
            from dream_items i
            inner join dream_transaction_list tl on i.id = tl.item_id
            inner join dream_transactions t on tl.transaction_id = t.id
            inner join dream_item_bind dib on dib.child_id = i.id and dib.unit_id = tl.unit_id and dib.parent_id = $item_id
            inner join dream_transaction_storage_bind tsb on t.id = tsb.transaction_id
            inner join dream_transaction_types tt on tsb.operation_id = tt.id
            inner join dream_storages s on tsb.storage_id = s.id
            inner join dream_units u on tl.unit_id = u.id
            left join
            (
            select tsb1.storage_id as storage_id, max(t1.id) as transaction_id
            from dream_transactions t1
            inner join dream_transaction_storage_bind tsb1 on t1.id = tsb1.transaction_id
            where t1.is_inventory = 1 and date(t1.create_time) <= date('$lastdate')
            group by tsb1.storage_id
            ) last_inv_by_storage on s.id = last_inv_by_storage.storage_id
            left join dream_transaction_list tl1 on tl.id = tl1.id and tl1.transaction_id <= last_inv_by_storage.transaction_id
            left join (
            select sum(dream_monthly_plan.production_count*dib.quantity) as consumption, dib.child_id, dib.unit_id from dream_monthly_plan
            inner join dream_item_bind dib on dib.parent_id = dream_monthly_plan.item_id
            where dream_monthly_plan.month between $curMonth_id and $month_id $str
            group by dib.child_id, dib.unit_id
            ) as monthly on monthly.child_id = tl.item_id and monthly.unit_id = u.id
            where tl.delete_time is null and date(t.create_time) <= date('$lastdate') and s.type_id = 1
            group by i.id, u.id, dib.quantity
        ";

        $items = DB::select($query_text);
        $data = [
            'month_id' => $month_id,
            'month_name' => DreamMonthlyPlan::change_name_language($month_id%12),
            'item_id' => $item_id,
            'item' => DreamItem::find($item_id),
            'data' => $items,
            'month_data' => $month_data
        ];
        return response()->json($data);
    }

    public function save_monthly_plan(Request $request){
        if (Gate::denies('save', new DreamMonthlyPlan())){
            return response()->json(['errors'=>array('нет доступа!')], 500);
        }

        $data = [
            'month' => $request->month_id,
            'item_id' => $request->item_id,
            'production_count' => $request->production_count,
            'sales_count' => (is_null($request->sales_count))?0:$request->sales_count,
            'user_id' => Auth::user()->id
        ];
        if (DB::table('dream_monthly_plan')->where([['month', $request->month_id], ['item_id', $request->item_id]])->count()==1){
            DB::table('dream_monthly_plan')->where([['month', $request->month_id], ['item_id', $request->item_id]])->update($data);
        }else{
            DreamMonthlyPlan::create($data);
        }
        return response()->json($request->all());
    }

    public function dailyPlan(){
        if (Gate::denies('view', new DreamMonthlyPlan())){
            return response()->json(['errors'=>array('нет доступа!')], 500);
        }
        $storages = DreamStorage::whereIn('type_id', array(1,7))->whereNotIn('id', array(3,23))->get();
        $plan_storages = DreamStorage::where('type_id', 1)->whereIn('id', array(3,23))->get();
        return view('index', ['page'=>'plans.dailyPlan', 'storages'=>$storages, 'plan_storages'=>$plan_storages]);
    }

    public function get_daily_plan(Request $request){
        $columns = array(
            0 => 'item_name',
            1 => 'articula_new',
            2 => 'sap_code',
            3 => 'remaining',
            4 => 'produced',
            5 => 'production_count',
            7 => 'predictedCount'
        );
        $totalData = DreamItem::where('articula_new', 'LIKE', '3%')->count();

        if ($request->input('length')==-1){
            $limit = $totalData;
        }else{
            $limit = $request->input('length');
        }
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        $curdate = Carbon::now();
        $currentDateFormat = $curdate->format('Y-m-d');
        $curMonth_id = $curdate->format('Y')*12 + $curdate->format('m');
        $currentDate = $curdate->format('d');


        $date = Carbon::createFromFormat('Y-m-d', $request->plan_date);
        $dateFormated = $date->format('Y-m-d');
        $month_id = $date->format('Y')*12 + $date->format('m');
        $requestedDate = $date->format('d');
        $startdate = Carbon::createFromFormat('Y-m-d', $request->plan_date)->startOfMonth()->format('Y-m-d');
        $startday = Carbon::createFromFormat('Y-m-d', $request->plan_date)->startOfMonth()->format('d');

        $lastdate = Carbon::createFromFormat('Y-m-d', $request->plan_date)->endOfMonth()->format('Y-m-d');

        if ($month_id > $curMonth_id){
            $query_text = "
                select SQL_CALC_FOUND_ROWS  dmp.id, dmp.id, i.item_name, i.articula_new, i.sap_code, u.unit,
                sum(tl.amount * tt.operation) as remaining,
                sum(COALESCE(tl1.amount, 0) * tt.operation) as remaining_at_the_beginning,
                IFNULL(monthlyPlan.result, 0) as remaining_at_the_beginning_of_model,
                sum(tl.amount * tt.operation) + IFNULL(monthly.result, 0) as predictedCount,
                IFNULL(ddp.production_count, 0) as production_count,
                0 as produced
                from dream_items i
                inner join dream_storage_item_bind on dream_storage_item_bind.item_id = i.id
                inner join dream_monthly_plan dmp on dmp.item_id = i.id and dmp.month = $month_id
                left join dream_daily_plan ddp on ddp.monthly_plan_id = dmp.id and ddp.day = $requestedDate
                inner join dream_transaction_list tl on i.id = tl.item_id
                inner join dream_transactions t on tl.transaction_id = t.id
                inner join dream_transaction_storage_bind tsb on t.id = tsb.transaction_id
                inner join dream_transaction_types tt on tsb.operation_id = tt.id
                inner join dream_storages s on tsb.storage_id = s.id
                inner join dream_units u on tl.unit_id = u.id
                left join
                (
                select tsb1.storage_id as storage_id, max(t1.id) as transaction_id
                from dream_transactions t1
                inner join dream_transaction_storage_bind tsb1 on t1.id = tsb1.transaction_id
                where t1.is_inventory = 1 and date(t1.create_time) <= date('$lastdate')
                group by tsb1.storage_id
                ) last_inv_by_storage on s.id = last_inv_by_storage.storage_id
                left join dream_transaction_list tl1 on tl.id = tl1.id and tl1.transaction_id <= last_inv_by_storage.transaction_id
                left join (
                select sum(dream_monthly_plan.production_count - dream_monthly_plan.sales_count) as result, dream_monthly_plan.item_id from dream_monthly_plan
                where dream_monthly_plan.month between $curMonth_id and $month_id - 1
                group by dream_monthly_plan.item_id
                ) as monthlyPlan on monthlyPlan.item_id = i.id
                left join (
                select sum(dream_daily_plan.production_count) as result, dream_monthly_plan.id, dream_monthly_plan.item_id from dream_monthly_plan
                left join dream_daily_plan on dream_daily_plan.monthly_plan_id = dream_monthly_plan.id
                where dream_daily_plan.day between $startday and $requestedDate
                group by dream_monthly_plan.item_id, dream_monthly_plan.id
                ) as monthly on monthly.id = dmp.id and dmp.item_id = i.id
                where tl.delete_time is null and date(t.create_time) <= date('$dateFormated') and dream_storage_item_bind.storage_id = $request->plan_storage_id
            ";
        }
        elseif ($month_id == $curMonth_id && $requestedDate >= $currentDate){

            $query_text = "
                select SQL_CALC_FOUND_ROWS  dmp.id, dmp.id, i.item_name, i.articula_new, i.sap_code, u.unit,
                sum(tl.amount * tt.operation) as remaining,
                sum(COALESCE(tl1.amount, 0) * tt.operation) as remaining_at_the_beginning,
                sum(COALESCE(tl1.amount, 0) * tt.operation) as remaining_at_the_beginning_of_model,
                sum(tl.amount * tt.operation) + IFNULL(monthly.result, 0) as predictedCount,
                IFNULL(ddp.production_count, 0) as production_count,
                IFNULL(produced.produced, 0) as produced
                from dream_items i
                inner join dream_storage_item_bind on dream_storage_item_bind.item_id = i.id
                inner join dream_monthly_plan dmp on dmp.item_id = i.id and dmp.month = $month_id
                left join dream_daily_plan ddp on ddp.monthly_plan_id = dmp.id and ddp.day = $requestedDate
                inner join dream_transaction_list tl on i.id = tl.item_id
                inner join dream_transactions t on tl.transaction_id = t.id
                inner join dream_transaction_storage_bind tsb on t.id = tsb.transaction_id
                inner join dream_transaction_types tt on tsb.operation_id = tt.id
                inner join dream_storages s on tsb.storage_id = s.id
                inner join dream_units u on tl.unit_id = u.id
                left join
                (
                select tsb1.storage_id as storage_id, max(t1.id) as transaction_id
                from dream_transactions t1
                inner join dream_transaction_storage_bind tsb1 on t1.id = tsb1.transaction_id
                where t1.is_inventory = 1 and date(t1.create_time) <= date('$lastdate')
                group by tsb1.storage_id
                ) last_inv_by_storage on s.id = last_inv_by_storage.storage_id
                left join dream_transaction_list tl1 on tl.id = tl1.id and tl1.transaction_id <= last_inv_by_storage.transaction_id
                left join (
                select sum(dream_daily_plan.production_count) as result, dream_monthly_plan.id, dream_monthly_plan.item_id from dream_monthly_plan
                left join dream_daily_plan on dream_daily_plan.monthly_plan_id = dream_monthly_plan.id
                where dream_daily_plan.day between $currentDate and $requestedDate
                group by dream_monthly_plan.item_id, dream_monthly_plan.id
                ) as monthly on monthly.id = dmp.id and dmp.item_id = i.id
                left join (
                select sum(dream_transaction_list.amount) as produced, dream_transaction_list.item_id from dream_transaction_list
                inner join dream_transactions on dream_transactions.id = dream_transaction_list.transaction_id
                inner join dream_transaction_storage_bind on dream_transaction_storage_bind.transaction_id = dream_transactions.id
                inner join dream_storages on dream_storages.id = dream_transaction_storage_bind.storage_id
                where dream_transactions.create_time between '$dateFormated 00:00:00' and '$dateFormated 23:59:59' and dream_transactions.user_id = 51 and dream_transaction_storage_bind.operation_id = 8
                and dream_storages.type_id = 2
                group by dream_transaction_list.item_id
                ) as produced on produced.item_id = tl.item_id
                where tl.delete_time is null and date(t.create_time) <= date('$dateFormated') and dream_storage_item_bind.storage_id = $request->plan_storage_id
            ";
        }
        else{
            $query_text = "
                select SQL_CALC_FOUND_ROWS  dmp.id, dmp.id, i.item_name, i.articula_new, i.sap_code, u.unit,
                sum(tl.amount * tt.operation) as remaining,
                sum(COALESCE(tl1.amount, 0) * tt.operation) as remaining_at_the_beginning,
                sum(COALESCE(tl1.amount, 0) * tt.operation) as remaining_at_the_beginning_of_model,
                sum(tl.amount * tt.operation) as predictedCount,
                IFNULL(ddp.production_count, 0) as production_count,
                IFNULL(produced.produced, 0) as produced
                from dream_items i
                inner join dream_storage_item_bind on dream_storage_item_bind.item_id = i.id
                inner join dream_monthly_plan dmp on dmp.item_id = i.id and dmp.month = $month_id
                left join dream_daily_plan ddp on ddp.monthly_plan_id = dmp.id and ddp.day = $requestedDate
                inner join dream_transaction_list tl on i.id = tl.item_id
                inner join dream_transactions t on tl.transaction_id = t.id
                inner join dream_transaction_storage_bind tsb on t.id = tsb.transaction_id
                inner join dream_transaction_types tt on tsb.operation_id = tt.id
                inner join dream_storages s on tsb.storage_id = s.id
                inner join dream_units u on tl.unit_id = u.id
                left join
                (
                select tsb1.storage_id as storage_id, max(t1.id) as transaction_id
                from dream_transactions t1
                inner join dream_transaction_storage_bind tsb1 on t1.id = tsb1.transaction_id
                where t1.is_inventory = 1 and date(t1.create_time) <= date('$lastdate')
                group by tsb1.storage_id
                ) last_inv_by_storage on s.id = last_inv_by_storage.storage_id
                left join dream_transaction_list tl1 on tl.id = tl1.id and tl1.transaction_id <= last_inv_by_storage.transaction_id
                left join (
                select sum(dream_daily_plan.production_count) as result, dream_monthly_plan.id, dream_monthly_plan.item_id from dream_monthly_plan
                left join dream_daily_plan on dream_daily_plan.monthly_plan_id = dream_monthly_plan.id
                where dream_daily_plan.day between $startday and $requestedDate
                group by dream_monthly_plan.item_id, dream_monthly_plan.id
                ) as monthly on monthly.id = dmp.id and dmp.item_id = i.id
                left join (
                select sum(dream_transaction_list.amount) as produced, dream_transaction_list.item_id from dream_transaction_list
                inner join dream_transactions on dream_transactions.id = dream_transaction_list.transaction_id
                inner join dream_transaction_storage_bind on dream_transaction_storage_bind.transaction_id = dream_transactions.id
                inner join dream_storages on dream_storages.id = dream_transaction_storage_bind.storage_id
                where dream_transactions.create_time between '$dateFormated 00:00:00' and '$dateFormated 23:59:59' and dream_transactions.user_id = 51 and dream_transaction_storage_bind.operation_id = 8
                and dream_storages.type_id = 2
                group by dream_transaction_list.item_id
                ) as produced on produced.item_id = tl.item_id
                where tl.delete_time is null and date(t.create_time) <= date('$dateFormated') and dream_storage_item_bind.storage_id = $request->plan_storage_id
            ";
        }

        $search_text = '';
        if (!empty($request->input('search.value'))){
            $search = $request->input('search.value');
            $search_text .= "
                     and ( i.articula_new like '%$search%' || i.articula_old like '%$search%' || i.sap_code like '%$search%' || i.item_name like '%$search%' )
                    ";
        }
        $query_text.=$search_text." group by i.id, u.id, dmp.id, ddp.id order by $order $dir limit $limit offset $start;";
        $items = DB::select($query_text);
        $totalCount = DB::select('select found_rows() as total_count')[0];

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalCount->total_count),
            "data"            => $items
        );

        return json_encode($json_data);
    }

    public function set_daily_plan_values(Request $request){
        $planDay = $request->planDay;
        $month = $request->month;
        $monthly_plan_id = $request->monthly_plan_id;
        $daily_data = DB::table('dream_monthly_plan')
            ->join('dream_daily_plan', 'dream_daily_plan.monthly_plan_id', '=', 'dream_monthly_plan.id')
            ->join('dream_items', 'dream_items.id', '=', 'dream_monthly_plan.item_id')
            ->where([['dream_daily_plan.day', $planDay], ['dream_monthly_plan.id', $monthly_plan_id]])
            ->select('dream_items.articula_new', 'dream_items.item_name', 'dream_items.sap_code', 'dream_daily_plan.*')->get()->first();
        $data = [
            'daily_data' => $daily_data
        ];
        return response()->json($data);
    }

    public function showPlanAddModal(Request $request){
        $month_id = $request->month_id;
        $day = $request->planDay;
        $monthly_plan_id = $request->monthly_plan_id;
        $day_data = DB::table('dream_monthly_plan')
            ->leftJoin('dream_daily_plan', 'dream_daily_plan.monthly_plan_id', '=', 'dream_monthly_plan.id')
            ->select('dream_monthly_plan.*', 'dream_daily_plan.id as daily_plan_id', 'dream_daily_plan.day', 'dream_daily_plan.production_count as daily_production')
            ->where([['dream_daily_plan.monthly_plan_id', $monthly_plan_id], ['dream_daily_plan.day', $day]])->get()->first();

        $item = DB::table('dream_monthly_plan')
            ->where([['dream_monthly_plan.id', $monthly_plan_id]])->get()->first();
        $str = '';
        if (is_null($request->production_count)){
            if (!is_null($day_data)){
                $production_count = $day_data->daily_production;
                $str = ' and dream_daily_plan.id != '.$day_data->daily_plan_id;
            }else{
                $production_count = 0;
            }
        }else{
            $production_count = $request->production_count;
        }

        $curdate = Carbon::now();
        $curMonth_id = $curdate->format('Y')*12 + $curdate->format('m');
        $currentDate = $curdate->format('d');

        $date = Carbon::createFromFormat('Y-m-d', $request->plan_date);
        $requestDateFormated = $date->format('Y-m-d');

        $month_id = $date->format('Y')*12 + $date->format('m');
        $requestDate = $date->format('d');

        $startdate = Carbon::createFromFormat('Y-m-d', $request->plan_date)->startOfMonth()->format('Y-m-d');
        $startday = Carbon::createFromFormat('Y-m-d', $request->plan_date)->startOfMonth()->format('Y-m-d');
        $lastdate = Carbon::createFromFormat('Y-m-d', $request->plan_date)->endOfMonth()->format('Y-m-d');

        if ($month_id > $curMonth_id){
            $query_text = "
                select i.item_name, i.articula_new, i.sap_code, u.unit,
                sum(tl.amount * tt.operation) as remaining,
                ROUND(sum(COALESCE(tl1.amount, 0) * tt.operation), 3) as remaining_at_the_beginning,
                ROUND(IFNULL((dib.quantity * $production_count), 0), 3) as total_consumption_by_plan,
                dib.quantity as consumption_by_norm,
                ROUND(sum(COALESCE(tl1.amount, 0) * tt.operation) - IFNULL(monthly.predictConsumption, 0) - IFNULL(daily.dailyPredictConsumption, 0), 3) as remaining_at_the_end
                from dream_items i
                inner join dream_transaction_list tl on i.id = tl.item_id
                inner join dream_item_bind dib on dib.child_id = i.id and dib.unit_id = tl.unit_id and dib.parent_id = $item->item_id
                inner join dream_transactions t on tl.transaction_id = t.id
                inner join dream_transaction_storage_bind tsb on t.id = tsb.transaction_id
                inner join dream_transaction_types tt on tsb.operation_id = tt.id
                inner join dream_storages s on tsb.storage_id = s.id
                inner join dream_units u on tl.unit_id = u.id
                left join
                (
                select tsb1.storage_id as storage_id, max(t1.id) as transaction_id
                from dream_transactions t1
                inner join dream_transaction_storage_bind tsb1 on t1.id = tsb1.transaction_id
                where t1.is_inventory = 1 and date(t1.create_time) <= date('$requestDateFormated')
                group by tsb1.storage_id
                ) last_inv_by_storage on s.id = last_inv_by_storage.storage_id
                left join dream_transaction_list tl1 on tl.id = tl1.id and tl1.transaction_id <= last_inv_by_storage.transaction_id
                left join (
                SELECT IFNULL(sum(dream_monthly_plan.production_count*dream_item_bind.quantity), 0) as predictConsumption, dream_item_bind.child_id, dream_item_bind.unit_id FROM dream_monthly_plan
                inner join dream_item_bind on dream_item_bind.parent_id = dream_monthly_plan.item_id
                where dream_monthly_plan.month between $curMonth_id and $month_id - 1
                group by dream_item_bind.child_id, dream_item_bind.unit_id
                ) as monthly on monthly.child_id = tl.item_id and monthly.unit_id = u.id
                left join (
                SELECT IFNULL(sum(dream_daily_plan.production_count*dream_item_bind.quantity), 0) as dailyPredictConsumption, dream_item_bind.child_id, dream_item_bind.unit_id FROM dream_daily_plan
                inner join dream_monthly_plan on dream_monthly_plan.id = dream_daily_plan.monthly_plan_id and dream_monthly_plan.month = $month_id
                inner join dream_item_bind on dream_item_bind.parent_id = dream_monthly_plan.item_id
                where dream_daily_plan.day between $startday and $requestDate $str
                group by dream_item_bind.child_id, dream_item_bind.unit_id
                ) as daily on daily.child_id = tl.item_id and daily.unit_id = u.id
                where tl.delete_time is null and date(t.create_time) <= date('$requestDateFormated') and s.type_id = 1
                group by i.id, u.id, dib.quantity
            ";
        }
        elseif ($month_id == $curMonth_id && $requestDate >= $currentDate){
            $query_text = "
                select i.item_name, i.articula_new, i.sap_code, u.unit,
                sum(tl.amount * tt.operation) as remaining,
                ROUND(sum(COALESCE(tl1.amount, 0) * tt.operation), 3) as remaining_at_the_beginning,
                ROUND(IFNULL((dib.quantity * $production_count), 0), 3) as total_consumption_by_plan,
                dib.quantity as consumption_by_norm,
                ROUND((sum(tl.amount * tt.operation) - IFNULL(daily.dailyPredictConsumption, 0) - IFNULL((dib.quantity * $production_count), 0)), 3) as remaining_at_the_end
                from dream_items i
                inner join dream_transaction_list tl on i.id = tl.item_id
                inner join dream_item_bind dib on dib.child_id = i.id and dib.unit_id = tl.unit_id and dib.parent_id = $item->item_id
                inner join dream_transactions t on tl.transaction_id = t.id
                inner join dream_transaction_storage_bind tsb on t.id = tsb.transaction_id
                inner join dream_transaction_types tt on tsb.operation_id = tt.id
                inner join dream_storages s on tsb.storage_id = s.id
                inner join dream_units u on tl.unit_id = u.id
                left join
                (
                select tsb1.storage_id as storage_id, max(t1.id) as transaction_id
                from dream_transactions t1
                inner join dream_transaction_storage_bind tsb1 on t1.id = tsb1.transaction_id
                where t1.is_inventory = 1 and date(t1.create_time) <= date('$lastdate')
                group by tsb1.storage_id
                ) last_inv_by_storage on s.id = last_inv_by_storage.storage_id
                left join dream_transaction_list tl1 on tl.id = tl1.id and tl1.transaction_id <= last_inv_by_storage.transaction_id
                left join (
                SELECT IFNULL(sum(dream_daily_plan.production_count*dream_item_bind.quantity), 0) as dailyPredictConsumption, dream_item_bind.child_id, dream_item_bind.unit_id FROM dream_daily_plan
                inner join dream_monthly_plan on dream_monthly_plan.id = dream_daily_plan.monthly_plan_id and dream_monthly_plan.month = $month_id
                inner join dream_item_bind on dream_item_bind.parent_id = dream_monthly_plan.item_id
                where dream_daily_plan.day between $currentDate and $requestDate $str
                group by dream_item_bind.child_id, dream_item_bind.unit_id
                ) as daily on daily.child_id = tl.item_id and daily.unit_id = u.id
                where tl.delete_time is null and date(t.create_time) <= date('$lastdate') and s.type_id = 1
                group by i.id, u.id, dib.quantity
            ";
        }
        else{
            $query_text = "
            select i.item_name, i.articula_new, i.sap_code, u.unit,
            sum(tl.amount * tt.operation) as remaining,
            ROUND(sum(COALESCE(tl1.amount, 0) * tt.operation), 3) as remaining_at_the_beginning,
            ROUND(IFNULL((dib.quantity * $production_count), 0), 3) as total_consumption_by_plan,
            dib.quantity as consumption_by_norm,
            ROUND(sum(tl.amount * tt.operation), 3) as remaining_at_the_end
            from dream_items i
            inner join dream_transaction_list tl on i.id = tl.item_id
            inner join dream_item_bind dib on dib.child_id = i.id and dib.unit_id = tl.unit_id and dib.parent_id = $item->item_id
            inner join dream_transactions t on tl.transaction_id = t.id
            inner join dream_transaction_storage_bind tsb on t.id = tsb.transaction_id
            inner join dream_transaction_types tt on tsb.operation_id = tt.id
            inner join dream_storages s on tsb.storage_id = s.id
            inner join dream_units u on tl.unit_id = u.id
            left join
            (
            select tsb1.storage_id as storage_id, max(t1.id) as transaction_id
            from dream_transactions t1
            inner join dream_transaction_storage_bind tsb1 on t1.id = tsb1.transaction_id
            where t1.is_inventory = 1 and date(t1.create_time) <= date('$requestDateFormated')
            group by tsb1.storage_id
            ) last_inv_by_storage on s.id = last_inv_by_storage.storage_id
            left join dream_transaction_list tl1 on tl.id = tl1.id and tl1.transaction_id <= last_inv_by_storage.transaction_id
            left join (
            SELECT IFNULL(sum(dream_monthly_plan.production_count*dream_item_bind.quantity), 0) as predictConsumption, dream_item_bind.child_id, dream_item_bind.unit_id FROM dream_monthly_plan
            inner join dream_item_bind on dream_item_bind.parent_id = dream_monthly_plan.item_id
            where dream_monthly_plan.month between $curMonth_id and $month_id - 1
            group by dream_item_bind.child_id, dream_item_bind.unit_id
            ) as monthly on monthly.child_id = tl.item_id and monthly.unit_id = u.id
            left join (
            SELECT IFNULL(sum(dream_daily_plan.production_count*dream_item_bind.quantity), 0) as dailyPredictConsumption, dream_item_bind.child_id, dream_item_bind.unit_id FROM dream_daily_plan
            inner join dream_monthly_plan on dream_monthly_plan.id = dream_daily_plan.monthly_plan_id and dream_monthly_plan.month = $month_id
            inner join dream_item_bind on dream_item_bind.parent_id = dream_monthly_plan.item_id
            where dream_daily_plan.day between $startday and $requestDate
            group by dream_item_bind.child_id, dream_item_bind.unit_id
            ) as daily on daily.child_id = tl.item_id and daily.unit_id = u.id
            where tl.delete_time is null and date(t.create_time) <= date('$requestDateFormated') and s.type_id = 1
            group by i.id, u.id, dib.quantity
        ";
        }

        $items = DB::select($query_text);
        $data = [
            'month_id' => $month_id,
            'month_name' => DreamMonthlyPlan::change_name_language($month_id%12),
            'item_id' => $item->item_id,
            'item' => DreamItem::find($item->item_id),
            'data' => $items,
            'month_data' => $day_data
        ];
        return response()->json($data);

    }

    public function save_daily_plan(Request $request){
        if (Gate::denies('save', new DreamMonthlyPlan())){
            return response()->json(['errors'=>array('нет доступа!')], 500);
        }

        $data = [
            'monthly_plan_id' => $request->monthly_plan_id,
            'production_count' => $request->production_count,
            'day' => $request->day
        ];
        if (DB::table('dream_daily_plan')->where([['monthly_plan_id', $request->monthly_plan_id], ['day', $request->day]])->count()==1){
            DB::table('dream_daily_plan')->where([['monthly_plan_id', $request->monthly_plan_id], ['day', $request->day]])->update($data);
        }else{
            DreamDailyPlan::create($data);
        }
        return response()->json($request->all());
    }

    public function getDailyPlanStatistics(Request $request){
        $columns = array(
            0 => 'item_name',
            1 => 'articula_new',
            2 => 'sap_code',
            3 => 'remaining_at_the_beginning',
            4 => 'outputCount',
            5 => 'consumption',
            6 => 'remaining_at_the_end'
        );

        $storage_arr = '';
        if (!is_null($request->storage_id)){
            foreach ($request->storage_id as $storage_id){
                $storage_arr .= $storage_id.',';
            }
            $storage_arr = substr($storage_arr, 0, strlen($storage_arr)-1);
            $storage_arr = " and ds.storage_id in ($storage_arr)";
        }
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        $totalData = DB::table('dream_transaction_list')
            ->join('dream_transaction_storage_bind', 'dream_transaction_storage_bind.transaction_id', '=', 'dream_transaction_list.transaction_id')->count();

        if ($request->input('length')==-1){
            $limit = $totalData;
        }else{
            $limit = $request->input('length');
        }
        $curdate = Carbon::now();
        $curMonth_id = $curdate->format('Y')*12 + $curdate->format('m');
        $currentDate = $curdate->format('d');

        $date = Carbon::createFromFormat('Y-m-d', $request->plan_date);

        $month_id = $date->format('Y')*12 + $date->format('m');
        $requestDate = $date->format('d');
        $lastdate = Carbon::createFromFormat('Y-m-d', $request->plan_date)->endOfMonth()->format('Y-m-d');

        if ($curMonth_id<$month_id){
            $query_text = "
                select SQL_CALC_FOUND_ROWS  i.id, i.id as DT_RowId, i.item_name, i.articula_new, i.sap_code, u.unit,
                sum(tl.amount * tt.operation) as remaining,
                ROUND(sum(COALESCE(tl1.amount, 0) * tt.operation), 3) as remaining_at_the_beginning,
                ROUND(IFNULL(daily.dailyPredictConsumption, 0), 3) as consumption,
                ROUND((sum(COALESCE(tl1.amount, 0) * tt.operation) - IFNULL(monthly.consumption, 0) - IFNULL(daily.dailyPredictConsumption, 0)), 3) as remaining_at_the_end,
                IFNULL(rq.outputCount, 0) as outputCount
                from dream_items i
                inner join dream_transaction_list tl on i.id = tl.item_id
                inner join dream_transactions t on tl.transaction_id = t.id
                inner join dream_transaction_storage_bind tsb on t.id = tsb.transaction_id
                inner join dream_transaction_types tt on tsb.operation_id = tt.id
                inner join dream_storages s on tsb.storage_id = s.id
                inner join dream_units u on tl.unit_id = u.id
                inner join dream_storage_item_bind ds on ds.item_id = i.id
                left join
                (
                select tsb1.storage_id as storage_id, max(t1.id) as transaction_id
                from dream_transactions t1
                inner join dream_transaction_storage_bind tsb1 on t1.id = tsb1.transaction_id
                where t1.is_inventory = 1 and date(t1.create_time) <= date('$lastdate')
                group by tsb1.storage_id
                ) last_inv_by_storage on s.id = last_inv_by_storage.storage_id
                left join dream_transaction_list tl1 on tl.id = tl1.id and tl1.transaction_id <= last_inv_by_storage.transaction_id
                left join (
                select sum(dream_monthly_plan.production_count*dib.quantity) as consumption, dib.child_id, dib.unit_id from dream_monthly_plan
                inner join dream_item_bind dib on dib.parent_id = dream_monthly_plan.item_id
                where dream_monthly_plan.month between $curMonth_id and $month_id - 1
                group by dib.child_id, dib.unit_id
                ) as monthly on monthly.child_id = tl.item_id and monthly.unit_id = u.id
                left join (
                SELECT IFNULL(sum(dream_daily_plan.production_count*dream_item_bind.quantity), 0) as dailyPredictConsumption, dream_item_bind.child_id, dream_item_bind.unit_id FROM dream_daily_plan
                inner join dream_monthly_plan on dream_monthly_plan.id = dream_daily_plan.monthly_plan_id and dream_monthly_plan.month = $month_id
                inner join dream_item_bind on dream_item_bind.parent_id = dream_monthly_plan.item_id
                where dream_daily_plan.day between $currentDate and $requestDate
                group by dream_item_bind.child_id, dream_item_bind.unit_id
                ) as daily on daily.child_id = tl.item_id and daily.unit_id = u.id
                left join (
                select sum(dream_request_list.amount) as outputCount, dream_request_list.item_id, dream_request_list.unit_id from dream_requests
                inner join dream_request_list on dream_request_list.request_id = dream_requests.id
                inner join dream_transaction_request_bind on dream_transaction_request_bind.request_list_id = dream_request_list.id
                inner join dream_transaction_list on dream_transaction_list.id = dream_transaction_request_bind.transaction_list_id
                inner join dream_transaction_storage_bind on dream_transaction_storage_bind.transaction_id = dream_transaction_list.transaction_id
                inner join dream_storages on dream_storages.id = dream_transaction_storage_bind.storage_id
                where dream_requests.create_time>=date('$date 00:00:01') and date('$date 23:59:59') and dream_storages.type_id = 1
                group by dream_request_list.item_id, dream_request_list.unit_id
                ) as rq on rq.item_id = tl.item_id and rq.unit_id = tl.unit_id
                where tl.delete_time is null and date(t.create_time) <= date('$lastdate') and s.type_id = 1 $storage_arr
            ";
        }
        elseif ($curMonth_id = $month_id && $currentDate<=$requestDate){
            $query_text = "
                select SQL_CALC_FOUND_ROWS  i.id, i.id as DT_RowId, i.item_name, i.articula_new, i.sap_code, u.unit,
                sum(tl.amount * tt.operation) as remaining,
                ROUND(sum(COALESCE(tl1.amount, 0) * tt.operation), 3) as remaining_at_the_beginning,
                ROUND(IFNULL(daily.dailyPredictConsumption, 0), 3) as consumption,
                ROUND((sum(tl.amount * tt.operation) - IFNULL(monthly.consumption, 0) - IFNULL(daily.dailyPredictConsumption, 0)), 3) as remaining_at_the_end,
                IFNULL(rq.outputCount, 0) as outputCount
                from dream_items i
                inner join dream_transaction_list tl on i.id = tl.item_id
                inner join dream_transactions t on tl.transaction_id = t.id
                inner join dream_transaction_storage_bind tsb on t.id = tsb.transaction_id
                inner join dream_transaction_types tt on tsb.operation_id = tt.id
                inner join dream_storages s on tsb.storage_id = s.id
                inner join dream_units u on tl.unit_id = u.id
                inner join dream_storage_item_bind ds on ds.item_id = i.id
                left join
                (
                select tsb1.storage_id as storage_id, max(t1.id) as transaction_id
                from dream_transactions t1
                inner join dream_transaction_storage_bind tsb1 on t1.id = tsb1.transaction_id
                where t1.is_inventory = 1 and date(t1.create_time) <= date('$lastdate')
                group by tsb1.storage_id
                ) last_inv_by_storage on s.id = last_inv_by_storage.storage_id
                left join dream_transaction_list tl1 on tl.id = tl1.id and tl1.transaction_id <= last_inv_by_storage.transaction_id
                left join (
                select sum(dream_monthly_plan.production_count*dib.quantity) as consumption, dib.child_id, dib.unit_id from dream_monthly_plan
                inner join dream_item_bind dib on dib.parent_id = dream_monthly_plan.item_id
                where dream_monthly_plan.month between $curMonth_id and $month_id - 1
                group by dib.child_id, dib.unit_id
                ) as monthly on monthly.child_id = tl.item_id and monthly.unit_id = u.id
                left join (
                SELECT IFNULL(sum(dream_daily_plan.production_count*dream_item_bind.quantity), 0) as dailyPredictConsumption, dream_item_bind.child_id, dream_item_bind.unit_id FROM dream_daily_plan
                inner join dream_monthly_plan on dream_monthly_plan.id = dream_daily_plan.monthly_plan_id and dream_monthly_plan.month = $month_id
                inner join dream_item_bind on dream_item_bind.parent_id = dream_monthly_plan.item_id
                where dream_daily_plan.day between $currentDate and $requestDate
                group by dream_item_bind.child_id, dream_item_bind.unit_id
                ) as daily on daily.child_id = tl.item_id and daily.unit_id = u.id
                left join (
                select sum(dream_request_list.amount) as outputCount, dream_request_list.item_id, dream_request_list.unit_id from dream_requests
                inner join dream_request_list on dream_request_list.request_id = dream_requests.id
                inner join dream_transaction_request_bind on dream_transaction_request_bind.request_list_id = dream_request_list.id
                inner join dream_transaction_list on dream_transaction_list.id = dream_transaction_request_bind.transaction_list_id
                inner join dream_transaction_storage_bind on dream_transaction_storage_bind.transaction_id = dream_transaction_list.transaction_id
                inner join dream_storages on dream_storages.id = dream_transaction_storage_bind.storage_id
                where dream_requests.create_time>=date('$date 00:00:01') and date('$date 23:59:59') and dream_storages.type_id = 1
                group by dream_request_list.item_id, dream_request_list.unit_id
                ) as rq on rq.item_id = tl.item_id and rq.unit_id = tl.unit_id
                where tl.delete_time is null and date(t.create_time) <= date('$lastdate') and s.type_id = 1 $storage_arr
            ";
        }
        else{
            $query_text = "
                select SQL_CALC_FOUND_ROWS  i.id, i.id as DT_RowId, i.item_name, i.articula_new, i.sap_code, u.unit,
                sum(tl.amount * tt.operation) as remaining,
                ROUND(sum(COALESCE(tl1.amount, 0) * tt.operation), 3) as remaining_at_the_beginning,
                ROUND(IFNULL(daily.dailyPredictConsumption, 0), 3) as consumption,
                sum(tl.amount * tt.operation) as remaining_at_the_end,
                IFNULL(rq.outputCount, 0) as outputCount
                from dream_items i
                inner join dream_transaction_list tl on i.id = tl.item_id
                inner join dream_transactions t on tl.transaction_id = t.id
                inner join dream_transaction_storage_bind tsb on t.id = tsb.transaction_id
                inner join dream_transaction_types tt on tsb.operation_id = tt.id
                inner join dream_storages s on tsb.storage_id = s.id
                inner join dream_units u on tl.unit_id = u.id
                inner join dream_storage_item_bind ds on ds.item_id = i.id
                left join
                (
                select tsb1.storage_id as storage_id, max(t1.id) as transaction_id
                from dream_transactions t1
                inner join dream_transaction_storage_bind tsb1 on t1.id = tsb1.transaction_id
                where t1.is_inventory = 1 and date(t1.create_time) <= date('$lastdate')
                group by tsb1.storage_id
                ) last_inv_by_storage on s.id = last_inv_by_storage.storage_id
                left join dream_transaction_list tl1 on tl.id = tl1.id and tl1.transaction_id <= last_inv_by_storage.transaction_id
                left join (
                select sum(dream_monthly_plan.production_count*dib.quantity) as consumption, dib.child_id, dib.unit_id from dream_monthly_plan
                inner join dream_item_bind dib on dib.parent_id = dream_monthly_plan.item_id
                where dream_monthly_plan.month = $month_id
                group by dib.child_id, dib.unit_id
                ) as monthly on monthly.child_id = tl.item_id and monthly.unit_id = u.id
                left join (
                SELECT IFNULL(sum(dream_daily_plan.production_count*dream_item_bind.quantity), 0) as dailyPredictConsumption, dream_item_bind.child_id, dream_item_bind.unit_id FROM dream_daily_plan
                inner join dream_monthly_plan on dream_monthly_plan.id = dream_daily_plan.monthly_plan_id and dream_monthly_plan.month = $month_id
                inner join dream_item_bind on dream_item_bind.parent_id = dream_monthly_plan.item_id
                where dream_daily_plan.day between $currentDate and $requestDate
                group by dream_item_bind.child_id, dream_item_bind.unit_id
                ) as daily on daily.child_id = tl.item_id and daily.unit_id = u.id
                left join (
                select sum(dream_request_list.amount) as outputCount, dream_request_list.item_id, dream_request_list.unit_id from dream_requests
                inner join dream_request_list on dream_request_list.request_id = dream_requests.id
                inner join dream_transaction_request_bind on dream_transaction_request_bind.request_list_id = dream_request_list.id
                inner join dream_transaction_list on dream_transaction_list.id = dream_transaction_request_bind.transaction_list_id
                inner join dream_transaction_storage_bind on dream_transaction_storage_bind.transaction_id = dream_transaction_list.transaction_id
                inner join dream_storages on dream_storages.id = dream_transaction_storage_bind.storage_id
                where dream_requests.create_time>=date('$date 00:00:01') and date('$date 23:59:59') and dream_storages.type_id = 1
                group by dream_request_list.item_id, dream_request_list.unit_id
                ) as rq on rq.item_id = tl.item_id and rq.unit_id = tl.unit_id
                where tl.delete_time is null and date(t.create_time) <= date('$lastdate') and s.type_id = 1 $storage_arr
            ";
        }

        $search_text = '';
        if (!empty($request->input('search.value'))){
            $search = $request->input('search.value');
            $search_text .= "
                     and ( i.articula_new like '%$search%' || i.articula_old like '%$search%' || i.sap_code like '%$search%' || i.item_name like '%$search%' )
                    ";
        }
        $query_text.=$search_text." group by i.id, u.id order by $order $dir limit $limit offset $start;";
        $items = DB::select($query_text);
        $totalCount = DB::select('select found_rows() as total_count')[0];
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalCount->total_count),
            "data"            => $items
        );

        return json_encode($json_data);
    }

    public function show_import_plan_page(Request $request){
        $plan_storages = DreamStorage::where('type_id', 1)->whereIn('id', array(3,23,6,4))->get();
        return view('index', ['page'=>'plans.importPlan', 'plan_storages'=>$plan_storages]);
    }

    public function upload_plan_file(Request $request){
        session()->forget('plan_items');
        if ($request->hasFile('plan_file')){
            $date = Carbon::createFromFormat('Y-m', $request->plan_date);
            $month_id = $date->format('Y')*12 + $date->format('m');
            $path = $request->file('plan_file')->getRealPath();

            $array = [];
            $articuls = [];
            $excelData = \Excel::load($path)->get();

            $import_btn = "<button type='submit' class='btn btn-warning' style='float: right' id='import_btn'>импортировать</button>";
            if($excelData->count()){

                $data = array();
                foreach ($excelData as $key => $value) {
                    if (!is_null($value->articula_new) && !is_null($value->production_count) && is_numeric($value->production_count) && $value->production_count > 0){
                        if (in_array($value->articula_new, $articuls)){
                            $array['articula_new'] = $value->articula_new;
                            $array['production_count'] = $value->production_count;
                            $array['sales_count'] = $value->sales_count;
                            $array['storage'] = 'дубликат';
                            $import_btn = null;
                        }else{
                            $correctItem = DB::table('dream_items')
                                ->join('dream_storage_item_bind', 'dream_storage_item_bind.item_id', '=', 'dream_items.id')
                                ->where([['dream_items.articula_new', $value->articula_new], ['dream_storage_item_bind.storage_id', $request->storage_id]])->first();
                            if (!empty($correctItem)){
                                $array['articula_new'] = $value->articula_new;
                                $array['production_count'] = $value->production_count;
                                $array['sales_count'] = $value->sales_count;
                                $array['storage'] = null;
                            }else{
                                $isExist = DreamItem::where('articula_new', $value->articula_new)->first();
                                if (!empty($isExist)){
                                    $hasStorage = DB::table('dream_storage_item_bind')
                                        ->where([['dream_storage_item_bind.storage_id', $request->storage_id], ['dream_storage_item_bind.item_id', $isExist->id]])->first();
                                    if (empty($hasStorage)){
                                        $array['articula_new'] = $value->articula_new;
                                        $array['production_count'] = $value->production_count;
                                        $array['sales_count'] = $value->sales_count;
                                        $array['storage'] = 'не тот склад';
                                        $import_btn = null;
                                    }else{
                                        $array['articula_new'] = $value->articula_new;
                                        $array['production_count'] = $value->production_count;
                                        $array['sales_count'] = $value->sales_count;
                                        $array['storage'] = 'не привязын к складу';
                                        $import_btn = null;
                                    }
                                }else{
                                    $array['articula_new'] = $value->articula_new;
                                    $array['production_count'] = $value->production_count;
                                    $array['sales_count'] = $value->sales_count;
                                    $array['storage'] = 'нет в базе';
                                    $import_btn = null;
                                }
                            }
                        }
                    }else{
                        if (is_null($value->articula_new)){
                            $array['articula_new'] = '';
                            $array['production_count'] = $value->production_count;
                            $array['sales_count'] = $value->sales_count;
                            $array['storage'] = 'без артикул';
                            $import_btn = null;
                        }elseif (is_null($value->production_count) || $value->prduction_count==0){
                            $array['articula_new'] = $value->articula_new;
                            $array['production_count'] = '0';
                            $array['sales_count'] = $value->sales_count;
                            $array['storage'] = 'без кол-во';
                            $import_btn = null;
                        }
                    }
                    $articuls[] = $value->articula_new;
                    $data[] = $array;
                }
                if(!empty($data)){
                    session()->put('plan_items.items', $data);
                    session()->put('plan_items.storage_id', $request->storage_id);
                    session()->put('plan_items.plan_date', $request->plan_date);
                }
                else{
                    session()->forget('plan_items');
                }
            }
            return redirect()->back()->with('plan_btn', $import_btn);
        }else{
            return redirect()->back()->with('plan_btn', null);
        }

    }

    public function import_plan(Request $request){
        if (Gate::denies('save', new DreamMonthlyPlan())){
            return response()->json(['errors'=>array('нет доступа!')], 500);
        }
        if (!is_null(session()->get('plan_items.items')) && !empty(session()->get('plan_items.items'))){

            DB::transaction(function () use($request){
                $items = session()->get('plan_items.items');
                $plan_date = session()->get('plan_items.plan_date');
                $date = Carbon::createFromFormat('Y-m', $plan_date);
                $month_id = $date->format('Y')*12 + $date->format('m');
                $user_id = Auth::user()->id;
                foreach ($items as $item){
                    $item_id = DB::table('dream_items')->where('articula_new', $item['articula_new'])->first()->id;
                    $val = [
                        'item_id'=>$item_id,
                        'month'=>$month_id,
                        'production_count'=>$item['production_count'],
                        'sales_count'=>(is_null($item['sales_count']))?0:$item['sales_count'],
                        'user_id'=>$user_id
                    ];
                    $hasMonthlyPlan = DB::table('dream_monthly_plan')->where([['month', $month_id], ['item_id', $item_id]])->get()->first();
                    if (is_null($hasMonthlyPlan)){
                        DB::table('dream_monthly_plan')->insert($val);
                    }else{
                        DB::table('dream_monthly_plan')->where('id', $hasMonthlyPlan->id)->update($val);
                    }
                }
            });
        }
        session()->forget('plan_items');
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers;

use App\DreamStorage;
use App\Policies\UserPolicy;
use App\Role;
use App\User;
use function foo\func;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class UserController extends Controller
{
    const WAREHOUSE_CONTROLLER = 2;
    public function index(){
        $has_perm=true;
        if (Gate::denies('VIEW_USERS_PAGE')){
            $has_perm=false;
        }
        $roles = Role::all();
        $storages = DreamStorage::all();
        return view('index', ['page'=>'users.users', 'roles'=>$roles, 'storages'=>$storages, 'has_perm'=>$has_perm]);
    }

    public function getUsers(){
        $users = DB::table('users')->join('user_role', 'user_role.user_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'user_role.role_id')
            ->leftJoin('dream_storage_user_bind', 'dream_storage_user_bind.user_id', '=', 'users.id')
            ->leftJoin('dream_storages', 'dream_storages.id', '=', 'dream_storage_user_bind.storage_id')
            ->select('users.*', 'roles.name as role_name',
                DB::raw("IFNULL(group_concat(dream_storages.name), '') as storage_name"))
            ->groupBy('users.id', 'roles.id')->get()->toArray();
        $data = array();
        foreach ($users as $user){
            $filename = public_path().'/assets/usersPhoto/'.$user->id.'_'.$user->name.'.png';
            if ($user->is_active){
                $nestedData['is_active'] = "<span class='label label-warning' style='font-size: 14px; padding-top: 0;'>активный</span>";
            }else{
                $nestedData['is_active'] = "<span class='label label-danger' style='font-size: 14px; padding-top: 0;'>не активный</span>";
            }
            $nestedData['id'] = $user->id;
            if (file_exists($filename)) {
                $nestedData['firstname'] = "<p data-id='".$user->id."_".$user->name."' class='showImageLink' style='color: grey;' onmouseover=\"showUserImage(event, this)\" onmouseleave=\"hideUserImage(event)\">$user->firstname</p>";

            } else {
                $nestedData['firstname'] = $user->firstname;
            }
            $nestedData['surname'] = $user->surname;
            $nestedData['email'] = $user->email;
            $nestedData['phone'] = $user->phone_number;
            $nestedData['name'] = $user->name;
            $nestedData['role_name'] = $user->role_name;
            $nestedData['storage_name'] = $user->storage_name;
            $data[] = $nestedData;
        }
        $json_data = [
            'data' => $data
        ];
        return json_encode($json_data);
    }

    public function getUserValues(Request $request){
        $has_perm=true;
        if (Gate::denies('VIEW_USERS_PAGE')){
            $has_perm=false;
        }
        $user_id = $request->user_id;
        $user = DB::table('users')
            ->join('user_role', 'user_role.user_id', '=', 'users.id')
            ->select('users.*', 'user_role.role_id as role_id')
            ->where('users.id', '=', $user_id)->first();
        $user_storages = DB::table('dream_storage_user_bind')
            ->where('user_id', $user_id)->select('dream_storage_user_bind.storage_id')->get()->toArray();
        $arr = array();
        foreach ($user_storages as $user_storage) {
            $arr[] = $user_storage->storage_id;
        }
        if (file_exists(public_path().'/assets/usersPhoto/'.$user_id.'_'.$user->name.'.png')){
            $path = asset('assets').'/usersPhoto/'.$user_id.'_'.$user->name.'.png';
        }else{
            $path = null;
        }
        $data = [
            'user'=>$user,
            'user_storage'=>$arr,
            'hasPerm'=>$has_perm,
            'path'=>$path
        ];
        return json_encode($data);
    }

    public function createUser(Request $request){
        if (Gate::denies('add_user', new User)){
            abort(403);
        }

        $validateNames = array(
            'name'=>'логин',
            'password'=>'парол',
            'role_id'=>'роль'
        );

        if ($request->role_id==self::WAREHOUSE_CONTROLLER){
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'password' => 'required',
                'role_id' => 'required',
                'storage' => 'required'
            ]);
        }else{
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'password' => 'required',
                'role_id' => 'required',
            ]);
        }

        $validator->setAttributeNames($validateNames);

        if ($validator->fails()) {
            return response()->json(['errors'=>array($validator->errors()->all())], 500);
        }

        if (User::where('name', $request->name)->get()->count()>0){
            return response()->json(['errors'=>array('логин уже существуеть!')], 500);
        }
        try{
            $res = DB::transaction(function () use($request){
                $storages = $request->storage;

                $data = [
                    'name' => $request->name,
                    'firstname' => $request->firstname,
                    'surname' => $request->surname,
                    'phone_number' => $request->phone_number,
                    'photo' => $request->userPhotoInput,
                    'api_token' => str_random(60),
                    'email' => $request->email,
                    'password' => bcrypt($request->password),
                    'is_active' => 1
                ];
                $id = User::create($data)->id;
                $user_role = [
                    'user_id' => $id,
                    'role_id' => $request->role_id
                ];
                DB::table('user_role')->insert($user_role);
                if ($request->role_id==self::WAREHOUSE_CONTROLLER){
                    if (!is_null($storages)){
                        foreach ($storages as $storage_id){
                            DB::table('dream_storage_user_bind')->insert(['user_id'=>$id, 'storage_id'=>$storage_id]);
                        }
                    }
                }
                if($request->hasFile('userPhotoInput'))
                {
                    $root=public_path()."/assets/usersPhoto/";
                    $f_name=$id.'_'.$request->name.'.png';
                    $request->file('userPhotoInput')->move($root,$f_name);
                }
            });
            return json_encode($request->all());
        }catch (\Exception $e){
            return response()->json(['errors'=>array($e, $request->all())], 500);
        }
    }

    public function updateUser(Request $request){
        if (Gate::denies('add_user', new User)){
            abort(403);
        }
        $validateNames = array(
            'edit_name'=>'логин',
            'edit_role_id'=>'роль'
        );

        if ($request->edit_role_id==self::WAREHOUSE_CONTROLLER){
            $validator = Validator::make($request->all(), [
                'edit_name' => 'required',
                'edit_role_id' => 'required',
                'edit_storage' => 'required'
            ]);
        }else{
            $validator = Validator::make($request->all(), [
                'edit_name' => 'required',
                'edit_role_id' => 'required',
            ]);
        }

        $validator->setAttributeNames($validateNames);

        if ($validator->fails()) {
            return response()->json(['errors'=>array($validator->errors()->all())], 500);
        }


        try{
            $res = DB::transaction(function () use($request){
                $id = $request->user_hidden_id;
                $old_role_id = DB::table('user_role')->where('user_id', $id)->first();
                $has_transactions = DB::table('dream_transactions')->where('user_id', $id)->count();

                if (is_null($request->edit_password)){
                    $data = [
                        'name' => $request->edit_name,
                        'email' => $request->edit_email,
                        'firstname' => $request->edit_firstname,
                        'surname' => $request->edit_surname,
                        'phone_number' => $request->edit_phone_number,
                        'photo' => $request->edit_photo
                    ];

                }else{
                    $data = [
                        'name' => $request->edit_name,
                        'email' => $request->edit_email,
                        'firstname' => $request->edit_firstname,
                        'surname' => $request->edit_surname,
                        'phone_number' => $request->edit_phone_number,
                        'photo' => $request->edit_photo,
                        'password' => bcrypt($request->edit_password)
                    ];
                }

                User::find($id)->update($data);

                if (!is_null($old_role_id)){
                    DB::table('user_role')->where('user_id', $id)->update(['role_id'=>$request->edit_role_id]);

                    if ($request->edit_role_id==self::WAREHOUSE_CONTROLLER){
                        $storages = $request->edit_storage;
                        DB::table('dream_storage_user_bind')->where([['user_id', $id]])->delete();
                        if (!is_null($storages)){
                            foreach ($storages as $storage_id){
                                DB::table('dream_storage_user_bind')->insert(['user_id'=>$id, 'storage_id'=>$storage_id]);
                            }
                        }
                    }
                }
                $old_photo_path = $id.'_'.User::find($id)->name;
                if($request->hasFile('edit_userPhotoInput'))
                {
                    $root=public_path("/assets/usersPhoto/");
                    if (file_exists($root.$old_photo_path.'.png')){
                        unlink($root.$old_photo_path.'.png');
                    }
                    $f_name=$id.'_'.$request->edit_name.'.png';
                    $request->file('edit_userPhotoInput')->move($root,$f_name);
                }
                return $old_role_id;
            });
            return json_encode($request->all());

        }catch (\Exception $e){
            return response()->json(['errors'=>array($e, $request->all())], 500);
        }
    }

    public function deleteUser(Request $request){
        if (Gate::denies('delete_user', new User)){
            abort(403);
        }
        $data = ['is_active' => 0];
        $usr = User::find($request->user_id);
        if ($usr->is_active == 1){
            $data = [
                'is_active' => 0
            ];
        }
        elseif ($usr->is_active==0){
            $data = [
                'is_active' => 1
            ];
        }
        $usr->update($data);
        return redirect()->back();
    }

    public function inventarization(){
        if (Gate::denies('VIEW_USERS_PAGE')){
            abort(403);
        }
        $all_storages = DreamStorage::all();
        return view('index', ['page'=>'import_transactions', 'storages'=>$all_storages]);
    }
}

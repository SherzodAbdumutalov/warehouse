<?php

namespace App\Http\Controllers;

use App\DreamItem;
use App\DreamTransaction;
use App\DreamRequest;
use App\DreamUnit;
use App\DreamStorage;
use Carbon\Carbon;
use Egulias\EmailValidator\Exception\ExpectingCTEXT;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Gate;


class DreamRequestController extends Controller
{
    const INPUT = 1;
    const OUTPUT = 2;
    const DEFECT_TYPE = 2;
    const RETURN_BACK = 3;
    const MOVING_REQUEST = 4;
    const PROCESSED = 1;
    const NO_PROCESSED = 2;
    const STATE_ACTIVE = 1;
    const WAREHOUSE = 1;
    const REQUEST_TYPE = 1;
    const SUPER_ADMIN = 1;
    const DEFECTIVE_STORAGE = 5;
//requests logic
    public function index()
    {
        $storages = DreamStorage::where('type_id', 1)->get();
        $production_lines = DreamStorage::whereIn('type_id', array(1,2,4))->get();
        return view('index', ['page' => 'create_request', 'storages' => $storages, 'production_lines' => $production_lines]);
    }

    public function add_request(Request $request)
    {
        try{
            $validateNames = array(
                'item_id'=>'элемент',
                'amount'=>'кол-во',
                'to_storage_id'=>'склад',
            );
            $validator = Validator::make($request->all(), [
                'item_id' => 'required',
                'amount' => 'required',
                'to_storage_id' => 'required',
            ]);
            $validator->setAttributeNames($validateNames);
            if ($validator->fails()) {
                return response()->json(['errors'=>array($validator->errors()->all())], 500);
            }

            $amount = $request->amount;

            $storage_id = $request->to_storage_id;
            $from_storage_id = $request->from_storage_id;
            $item_id = $request->item_id;
            if ($storage_id==0){
                $count_children_items = DB::table('dream_item_bind')->where('parent_id', $item_id)->count();
                if ($count_children_items>0 && !$request->withoutNorm){
                    $has_child = true;
                    //incorrect
                    $not_contain_storage_request_list_items = DB::table('dream_item_bind')
                    ->leftJoin('dream_storage_item_bind', 'dream_storage_item_bind.item_id', '=', 'dream_item_bind.child_id')
                    ->join('dream_items', 'dream_items.id', '=', 'dream_item_bind.child_id')
                    ->select('dream_items.*', DB::raw('null as storage_name'), DB::raw('null as storage_id'), DB::raw('null as unit_name'), DB::raw('null as unit_id'), 'dream_item_bind.quantity',
                        DB::raw('null as norm_unit_name'), DB::raw('null as norm_unit_id'))
                    ->where([['dream_item_bind.parent_id', $item_id]])->whereNull('dream_storage_item_bind.storage_id');

                    $conflict_unit_request_list_items = DB::table('dream_item_bind')
                    ->leftJoin('dream_storage_item_bind', 'dream_storage_item_bind.item_id', '=', 'dream_item_bind.child_id')
                    ->join('dream_storages', 'dream_storages.id', '=', 'dream_storage_item_bind.storage_id')
                    ->join('dream_items', 'dream_items.id', '=', 'dream_storage_item_bind.item_id')
                    ->join('dream_units', 'dream_units.id', '=', 'dream_storage_item_bind.unit_id')
                    ->join('dream_units as dream_units1', 'dream_units1.id', '=', 'dream_item_bind.unit_id')
                    ->select('dream_items.*', 'dream_storages.name as storage_name', 'dream_storages.id as storage_id', 'dream_units.unit as unit_name', 'dream_units.id as unit_id', 'dream_item_bind.quantity',
                        'dream_units1.unit as norm_unit_name', 'dream_units1.id as norm_unit_id')
                    ->where([['dream_item_bind.parent_id', $item_id], ['dream_item_bind.unit_id', '!=', 'dream_storage_item_bind.unit_id'], ['dream_storages.type_id', 1]])
                    ->whereNotIn('dream_items.id', array(DB::raw("
                        (SELECT dream_items1.id FROM dream_item_bind as dream_item_bind1
                        inner join dream_storage_item_bind as dream_storage_item_bind1 on dream_storage_item_bind1.item_id=dream_item_bind1.child_id and dream_storage_item_bind1.unit_id=dream_item_bind1.unit_id
                        inner join dream_items as dream_items1 on dream_items1.id=dream_item_bind1.child_id
                        inner join dream_storages as dream_storages1 on dream_storages1.id=dream_storage_item_bind1.storage_id
                        inner join dream_units as dream_units_c on dream_units_c.id=dream_storage_item_bind1.unit_id
                        inner join dream_units as dream_units_uc on dream_units_uc.id=dream_item_bind1.unit_id
                        where dream_item_bind1.parent_id=$item_id and dream_storages1.type_id=1)
                    ")))
                    ->groupBy('dream_items.id', 'dream_units.id', 'dream_storages.id', 'dream_item_bind.quantity', 'dream_units1.id');

                    $request_list_items = DB::table('dream_item_bind')
                    ->join('dream_storage_item_bind', function ($join){
                        $join->on('dream_storage_item_bind.item_id', '=', 'dream_item_bind.child_id');
                        $join->on('dream_storage_item_bind.unit_id', '=', 'dream_item_bind.unit_id');
                    })
                    ->join('dream_storages', 'dream_storages.id', '=', 'dream_storage_item_bind.storage_id')
                    ->join('dream_items', 'dream_items.id', '=', 'dream_storage_item_bind.item_id')
                    ->join('dream_units', 'dream_units.id', '=', 'dream_storage_item_bind.unit_id')
                    ->join('dream_units as dream_units1', 'dream_units1.id', '=', 'dream_item_bind.unit_id')
                    ->select('dream_items.*', 'dream_storages.name as storage_name', 'dream_storages.id as storage_id', 'dream_units.unit as unit_name', 'dream_units.id as unit_id', 'dream_item_bind.quantity',
                        'dream_units1.unit as norm_unit_name', 'dream_units1.id as norm_unit_id')
                    ->where([['dream_item_bind.parent_id', $item_id], ['dream_storages.type_id', 1]])
                    ->union($conflict_unit_request_list_items)
                    ->union($not_contain_storage_request_list_items)
                    ->get();
                }else{
                    $has_child = false;
                    $request_list_items = DB::select("SELECT dream_items.*, dream_storages.name as storage_name, dream_storages.id as storage_id, dream_units.unit as unit_name,
                    dream_units.id as unit_id, null as quantity, dream_units.unit as norm_unit_name, dream_units.id as norm_unit_id
                    FROM  dream_storage_item_bind
                    inner join dream_items on dream_items.id=dream_storage_item_bind.item_id
                    inner join dream_storages on dream_storages.id=dream_storage_item_bind.storage_id
                    inner join dream_units on dream_units.id=dream_storage_item_bind.unit_id
                    where dream_storage_item_bind.item_id=$item_id and dream_storages.type_id=1");
                }
            }else{

                $count_children_items = DB::table('dream_item_bind')->where('parent_id', $item_id)->count();
                if ($count_children_items>0 && !$request->withoutNorm){
                    $has_child = true;
                    //incorrect
                    $request_list_items = DB::table('dream_item_bind')
                        ->join('dream_storage_item_bind', function ($join){
                            $join->on('dream_storage_item_bind.item_id', '=', 'dream_item_bind.child_id');
                            $join->on('dream_storage_item_bind.unit_id', '=', 'dream_item_bind.unit_id');
                        })
                        ->join('dream_storages', 'dream_storages.id', '=', 'dream_storage_item_bind.storage_id')
                        ->join('dream_items', 'dream_items.id', '=', 'dream_storage_item_bind.item_id')
                        ->join('dream_units', 'dream_units.id', '=', 'dream_storage_item_bind.unit_id')
                        ->join('dream_units as dream_units1', 'dream_units1.id', '=', 'dream_item_bind.unit_id')
                        ->select('dream_items.*', 'dream_storages.name as storage_name', 'dream_storages.id as storage_id', 'dream_units.unit as unit_name', 'dream_units.id as unit_id', 'dream_item_bind.quantity',
                            'dream_units1.unit as norm_unit_name', 'dream_units1.id as norm_unit_id')
                        ->where([['dream_item_bind.parent_id', $item_id], ['dream_storages.id', $storage_id]])
                        ->get();
                }else{
                    $has_child = false;
                    $request_list_items = DB::select("SELECT dream_items.*, dream_storages.name as storage_name, dream_storages.id as storage_id, dream_units.unit as unit_name, dream_units.id as unit_id,
                    null as quantity, dream_units.unit as norm_unit_name, dream_units.id as norm_unit_id
                    FROM  dream_storage_item_bind
                    inner join dream_items on dream_items.id=dream_storage_item_bind.item_id
                    inner join dream_storages on dream_storages.id=dream_storage_item_bind.storage_id
                    inner join dream_units on dream_units.id=dream_storage_item_bind.unit_id
                    where dream_storage_item_bind.item_id=$item_id and dream_storages.id=$storage_id");
                }
            }
            $array = [];

            foreach ($request_list_items as $key=>$item){
                $storage_id = (!is_null($item->storage_id))?$item->storage_id:null;
                $unit = (!is_null($item->unit_name))?($item->unit_id==$item->norm_unit_id)?$item->unit_name:"<p style='background-color: blue; color: white; padding: 5px'>$item->unit_name</p>":null;
                $unit_id = (!is_null($item->unit_id))?($item->unit_id==$item->norm_unit_id)?$item->unit_id:$item->unit_name:null;
                $storage_name = (!is_null($item->storage_name))?($item->unit_id==$item->norm_unit_id)?$item->storage_name:'конфликтующие':'неизвестно';
                $amo = (is_null($item->quantity))?$amount: round($item->quantity * $amount, 4);
                $filename = public_path().'/assets/itemsPhoto/'.$item->articula_new.'.png';
                if (file_exists($filename)) {
                    $data = [
                        'storage' => "<p style='display: block; font-size: 20px; line-height: 4px; color: black; font-weight: bold; float: left; padding: 0; margin-top: 7px'>".$storage_name."</p><button type='button' class='btn btn-danger  float-right' onclick=\"remove_items('".$storage_name."')\" style='width: 100px'><i class='fa fa-trash'></i> <i class='fa fa-arrow-down'></i></button>",
                        'storage_id' => $storage_id,
                        'item_id' => $item->id,
                        'unit_id' => $unit_id,
                        'unit' => $unit,
                        'articula_new' => "<p data-id='$item->articula_new' class='showImageLink' style='color: grey;' onmouseover=\"showImage(event, this)\" onmouseleave=\"hideImage(event)\">$item->articula_new</p>",
                        'sap_code' => $item->sap_code,
                        'item_name' => $item->item_name,
                        'storage_name' => $storage_name,
                        'amount' => (is_null($item->quantity))?$amount: round($item->quantity * $amount, 4),
                        'remaining' => (!is_null($storage_id))?$this->get_item_remaining($storage_id, $item->id):null,
                        'remaining_of_from_storage' => ($from_storage_id!=0)?$this->get_item_remaining($from_storage_id, $item->id):'',
                        'error' => (!is_null($item->storage_name))?($item->unit_id==$item->norm_unit_id)?0:1:2,
                        'edit_btn' => '<button type="button" class="btn btn-info " data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#edit_amount" onclick="set_data(this,\''.$amo.'\',\''.$item->id.'\')"><i class="fa fa-edit"></i></button><button type="button" class="btn btn-danger " onclick="delete_item(this)"><i class="fa fa-trash"></i></button>'
                    ];
                } else {
                    $data = [
                        'storage' => "<p style='display: block; font-size: 20px; line-height: 4px; color: black; font-weight: bold; float: left; padding: 0; margin-top: 7px'>".$storage_name."</p><button type='button' class='btn btn-danger  float-right' onclick=\"remove_items('".$storage_name."')\" style='width: 100px'><i class='fa fa-trash'></i> <i class='fa fa-arrow-down'></i></button>",
                        'storage_id' => $storage_id,
                        'item_id' => $item->id,
                        'unit_id' => $unit_id,
                        'unit' => $unit,
                        'articula_new' => $item->articula_new,
                        'sap_code' => $item->sap_code,
                        'item_name' => $item->item_name,
                        'storage_name' => $storage_name,
                        'amount' => (is_null($item->quantity))?$amount: round($item->quantity * $amount, 4),
                        'remaining' => (!is_null($storage_id))?$this->get_item_remaining($storage_id, $item->id):null,
                        'remaining_of_from_storage' => ($from_storage_id!=0)?$this->get_item_remaining($from_storage_id, $item->id):'',
                        'error' => (!is_null($item->storage_name))?($item->unit_id==$item->norm_unit_id)?0:1:2,
                        'edit_btn' => '<button type="button" class="btn btn-info " data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#edit_amount" onclick="set_data(this,\''.$amo.'\',\''.$item->id.'\')"><i class="fa fa-edit"></i></button><button type="button" class="btn btn-danger " onclick="delete_item(this)"><i class="fa fa-trash"></i></button>'
                    ];
                }


                array_push($array, $data);
            }
            $data = [
                'data' => $array,
                'has_child' => $has_child,
                'item_id' => $item_id
            ];
            return json_encode($data);
        }catch(\Exception $e){
            return response()->json(['error'=>array('невозможно добавить', $e)], 500);
        }
    }

    public function changeFromStorageRemaining(Request $request){
        $from_storage_id = $request->from_storage_id;
        $items = json_decode($request->items);
        foreach ($items->data as $key=>$item){
            $items->data[$key]->remaining_of_from_storage = ($from_storage_id!=0)?$this->get_item_remaining($from_storage_id, $item->item_id):'';
        }
        return json_encode($items);
    }

//    change name later
    public function is_shown($item_id, $storage_id, $parent_id){
        if (is_null($storage_id)){
            return false;
        }
        $exist = DB::table('dream_requests')->where('model_id', $parent_id)->first();
        if (empty($exist)){
            return true;
        }
        $result = DB::select("SELECT * FROM dream_request_list r_list
        inner join dream_requests r on r_list.request_id = r.id
        inner join (select id from dream_requests where  dream_requests.model_id = $parent_id
        and dream_requests.to_storage_id = $storage_id order by update_time desc limit 1) as last_r on r.id = last_r.id");
        if (empty($result)){
            return false;
        }
        foreach ($result as $item){
            if ($item->item_id==$item_id){
                return true;
            }
        }
        return empty($result)?true:false;
    }

    public function requestItemsByStorage(Request $request){
        $storage_id = $request->to_storage_id;
        $model_id = $request->model_id;
        if (is_null($model_id)){
            if ($storage_id==0){
                $storage_items = DB::select("SELECT dream_items.* FROM dream_items where id in ".
                    "(select parent_id from dream_item_bind inner join dream_storage_item_bind on dream_item_bind.child_id=dream_storage_item_bind.item_id group by parent_id " .
                    "union select child_id from dream_item_bind inner join dream_storage_item_bind on dream_item_bind.child_id=dream_storage_item_bind.item_id group by parent_id " .
                    "union select item_id from dream_storage_item_bind group by item_id)");
            }else{
                $storage_items = DB::select("SELECT dream_items.* FROM dream_items where id in ".
                    "(select parent_id from dream_item_bind inner join dream_storage_item_bind on dream_item_bind.child_id=dream_storage_item_bind.item_id " .
                    "where dream_storage_item_bind.storage_id=$storage_id group by parent_id union select child_id from dream_item_bind inner join dream_storage_item_bind " .
                    "on dream_item_bind.child_id=dream_storage_item_bind.item_id where dream_storage_item_bind.storage_id=$storage_id group by parent_id union " .
                    "select item_id from dream_storage_item_bind where dream_storage_item_bind.storage_id=$storage_id group by item_id)");
            }
        }else{
            if ($storage_id==0){
                $r_i = DB::table('dream_replacement_items')
                    ->join('dream_items', 'dream_items.id', '=', 'dream_replacement_items.item_id')
                    ->leftJoin('dream_item_bind', 'dream_item_bind.id', '=', 'dream_replacement_items.item_bind_id')
                    ->select('dream_items.*')
                    ->where('dream_item_bind.parent_id', $model_id)->groupBy('dream_items.id');
                $storage_items = DB::table('dream_items')
                    ->join('dream_item_bind', 'dream_item_bind.child_id', '=', 'dream_items.id')
                    ->select('dream_items.*')
                    ->where('dream_item_bind.parent_id', $model_id)
                    ->union($r_i)->get();
            }else{
                $storage_items = DB::table('dream_items')
                    ->join('dream_item_bind', 'dream_item_bind.child_id', '=', 'dream_items.id')
                    ->join('dream_storage_item_bind', 'dream_storage_item_bind.item_id', '=', 'dream_items.id')
                    ->select('dream_items.*')
                    ->where([['dream_item_bind.parent_id', $model_id], ['dream_storage_item_bind.storage_id', $storage_id]])->get();
            }

        }
        return json_encode($storage_items);
    }

//    public function get_storage_data_with_storage($all_items, $amount){
//        $array = [];
//        foreach ($all_items as $d){
//            $storage = (!is_null($d->storage_id))?DB::table('dream_storages')->where([['dream_storages.id', $d->storage_id], ['dream_storages.type_id', self::WAREHOUSE]])->get()->first():null;
//            $unit = (!is_null($d->unit_id))?DreamUnit::find($d->unit_id):null;
//            $storage_name = (!is_null($storage))?$storage->name:'неизвестно';
//            $data = [
//                'storage' => "<p style='display: block; font-size: 20px; line-height: 4px; color: black; font-weight: bold; float: left; padding: 0; margin-top: 7px'>".$storage_name."</p><button type='button' class='btn btn-danger  float-right' onclick=\"remove_items('".$storage_name."')\" style='width: 100px'><i class='fa fa-trash'></i> <i class='fa fa-arrow-down'></i></button>",
//                'storage_id' => (!is_null($storage))?$storage->id:null,
//                'item_id' => $d->dream_item_id,
//                'unit_id' => $d->unit_id,
//                'unit' => (!is_null($unit))?$unit->unit:null,
//                'articula_new' => $d->articula_new,
//                'sap_code' => $d->sap_code,
//                'item_name' => $d->item_name,
//                'storage_name' => $storage_name,
//                'amount' => $amount,
//                'remaining' => (!is_null($storage))?$this->get_item_remaining($storage->id, $d->dream_item_id):null,
//                'error' => '0'
//            ];
//
//            array_push($array, $data);
//        }
//        return $array;
//    }

    public function get_item_remaining($storage_id, $item_id)
    {
        $total = DB::select("select sum(operation*amount) as total from dream_transaction_list ".
            "left join dream_transaction_storage_bind on dream_transaction_list.transaction_id=dream_transaction_storage_bind.transaction_id and dream_transaction_storage_bind.storage_id=$storage_id " .
            "left join dream_transaction_types on dream_transaction_types.id=dream_transaction_storage_bind.operation_id where item_id=$item_id and dream_transaction_list.delete_time is null")[0];
        if ($total->total == null){
            $total->total = 0;
        }
        return round($total->total, 3);
    }

    public function register_request(Request $request){

        if (Gate::denies('add_request', new DreamRequest)){
            return response()->json(['errors'=>array('нет доступа!')], 500);
        }
        $validateNames = array(
            'items'=>'элементы',
            'description'=>'примечание',
            'from_storage_id'=>'получатель',
        );
        if ($request->from_storage_id==0){
            $validator = Validator::make($request->all(), [
                'items' => 'required',
                'description' => 'required'
            ]);
        }else{
            $validator = Validator::make($request->all(), [
                'items' => 'required',
                'from_storage_id' => 'required'
            ]);
        }
        $validator->setAttributeNames($validateNames);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()], 500);
        }

        try{
            DB::transaction(function () use($request){

                $request_items = json_decode($request->items);
                $storages = DreamStorage::all();
                foreach ($storages as $storage){
                    if ($this->contain_storage($storage->id, $request_items)){
                        $data = [
                            'to_storage_id' => $storage->id,
                            'from_storage_id' => $request->from_storage_id,
                            'user_id' => Auth::user()->id,
                            'description' => $request->description,
                            'model_id' => $request->model_id,
                            'amount' => $request->model_amount
                        ];

                        $tr_req_id = DreamRequest::create($data)->id;

                        foreach ($request_items as $item){
                            if ($item->storage_id==$storage->id){
                                $item_data = [
                                    'request_id' => $tr_req_id,
                                    'item_id' => $item->item_id,
                                    'amount' => (float)$item->amount,
                                    'unit_id' => $item->unit_id
                                ];

                                DB::table('dream_request_list')->insert($item_data);
                            }
                        }
                    }
                }
            });
            return json_encode($request->except('storage_name'));
        }catch (\Exception $e){
            return response()->json(['error'=>array($e)], 500);
        }
    }

    public function contain_storage($storage_id, $arr){
        foreach ($arr as $value){
            if ($storage_id==$value->storage_id){
                return true;
            }
        }
        return false;
    }

    public function replaceItems(Request $request){
        $replace_items = [];
        if (!is_null($request->model_id)){
            $item_bind_id = DB::table('dream_item_bind')->where([['parent_id', $request->model_id], ['child_id', $request->item_id]])->get();
            if (!empty($item_bind_id)){
                $replace_items = DB::table('dream_replacement_items')
                    ->join('dream_items', 'dream_items.id', '=', 'dream_replacement_items.item_id')
                    ->where('dream_replacement_items.item_bind_id', $item_bind_id->first()->id)->select('dream_items.*')->get();
            }
        }

        return json_encode($replace_items);
    }

//    public function checkForEnough(){
//        $enough = false;
//        if (session()->has('dream_request_list')){
//            $data = session()->get('dream_request_list');
//            foreach ($data as $item){
//                if ($item['remaining']<$item['not_accepted_items_count']){
//                    return false;
//                }else{
//                    $enough = true;
//                }
//            }
//        }
//        return $enough;
//    }

//    correct

    public function show_requests_page(){
        return  view('index', ['page' => 'all_requests']);
    }

    public function get_requests(Request $request){
//2019-03-16 16:12:40
        $startdate = $request->start ? Carbon::createFromFormat('Y-m-d', $request->start)->subDay()->toDateTimeString() : Carbon::createFromFormat('Y-m-d', "2015-03-16")->toDateTimeString();
        $endDate = Carbon::createFromFormat("Y-m-d", $request->end)->toDateTimeString();

//        var_dump($startdate);
//        dd();

        try{
            DB::table('dream_requests')
                ->leftJoin('dream_request_list', 'dream_request_list.request_id', '=', 'dream_requests.id')
                ->select('dream_request_list.request_id', 'dream_requests.id')
                ->whereNull('dream_request_list.request_id')
                ->groupBy('dream_requests.id')->delete();

            $columns = array(
                0 => 'id',
                1 => 'state_name',
                2 => 'progress',
                3 => 'create_time',
                4 => 'to_storage_name',
                5 => 'articula_new',
                6 => 'item_name',
                7 => 'amount',
                8 => 'from_storage_name',
                9 => 'description',
                10 => 'user_name'
            );
            $filterColumns = array(
                0 => 'dream_requests.id',
                1 => 'dream_request_states.name',
                2 => 'progress',
                3 => 'dream_requests.create_time',
                4 => 'to_storage.name',
                5 => 'dream_items.articula_new',
                6 => 'dream_items.item_name',
                7 => 'dream_requests.amount',
                8 => 'from_storage.name',
                9 => 'dream_requests.description',
                10 => 'users.firstname',
                11 => 'dream_requests.create_time'
            );
            $totalData = DreamRequest::count();
            if ($request->input('length')==-1){
                $limit = $totalData;
            }else{
                $limit = $request->input('length');
            }
            $filters = [];
            $str = '';

            foreach ($request->columns as $key=>$column){
                if ($column['search']['value']!=null){
                    if ($column['search']['value']=='пустой'){
                        $filters = array_add($filters, $key, 'пустой');
                        $str.=$filterColumns[$key]." is null and ";
                    }else{
                        $filters = array_add($filters, $key, stripslashes($column['search']['value']));
                        $str.=$filterColumns[$key]."='".$column['search']['value']."' and ";
                    }
                }
            }
            if (!empty($str)>0){
                $str = substr($str, 0, strlen($str)-4);
            }
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');
            if (count($filters)==0){
                if (empty($request->input('search.value'))){
                    $items = DB::table('dream_requests')
                        ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                        ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                        ->leftJoin('dream_items', 'dream_items.id', '=', 'dream_requests.model_id')
                        ->join('users', 'users.id', '=', 'dream_requests.user_id')
                        ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                        ->select('dream_requests.*', 'to_storage.name as to_storage_name', 'from_storage.name as from_storage_name', 'users.firstname as user_name',
                            'dream_request_states.name as state_name', 'dream_request_states.id as state_id',
                            'dream_items.articula_new', 'dream_items.item_name',
                            DB::raw("
                          (
                            (select sum(dream_transaction_list.amount) from dream_transaction_list where id in
                              (
                                select dream_transaction_request_bind.transaction_list_id from dream_transaction_request_bind
                                inner join dream_request_list on dream_request_list.id=dream_transaction_request_bind.request_list_id
                                where dream_request_list.request_id=dream_requests.id and dream_request_list.delete_time is null
                              )  and dream_transaction_list.delete_time is null
                            )/
                            (select sum(dream_request_list.amount) from dream_request_list where dream_request_list.request_id=dream_requests.id and dream_request_list.delete_time is null
                            )
                          ) as progress
                        ")
                        )->where('dream_requests.type_id', 1)
                        ->whereDate('dream_requests.create_time',"<=", $endDate)
                        ->whereDate('dream_requests.create_time',">=", $startdate)
                        ->limit($limit)->orderBy($order, $dir)->get();
                    $filteredData = DB::table('dream_requests')
                        ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                        ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                        ->leftJoin('dream_items', 'dream_items.id', '=', 'dream_requests.model_id')
                        ->join('users', 'users.id', '=', 'dream_requests.user_id')
                        ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                        ->whereDate('dream_requests.create_time',"<=", $endDate)
                        ->whereDate('dream_requests.create_time',">=", $startdate)
                        ->select('dream_requests.*', 'to_storage.name as to_storage_name', DB::raw('IFNULL(from_storage.name, " ") as from_storage_name'), 'users.firstname as user_name',
                            'dream_request_states.name as state_name', 'dream_request_states.id as state_id', 'dream_items.articula_new', 'dream_items.item_name'
                        )->where('dream_requests.type_id', 1)
                        ->get()->count();

                }else{
                    $search = $request->input('search.value');
                    $items = DB::table('dream_requests')
                        ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                        ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                        ->leftJoin('dream_items', 'dream_items.id', '=', 'dream_requests.model_id')
                        ->join('users', 'users.id', '=', 'dream_requests.user_id')
                        ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                        ->select('dream_requests.*', 'to_storage.name as to_storage_name', 'from_storage.name as from_storage_name', 'users.firstname as user_name',
                            'dream_request_states.name as state_name', 'dream_items.articula_new', 'dream_items.item_name',
                            DB::raw("
                          (
                            (select sum(dream_transaction_list.amount) from dream_transaction_list where id in
                              (
                                select dream_transaction_request_bind.transaction_list_id from dream_transaction_request_bind
                                inner join dream_request_list on dream_request_list.id=dream_transaction_request_bind.request_list_id
                                where dream_request_list.request_id=dream_requests.id and dream_request_list.delete_time is null
                              )  and dream_transaction_list.delete_time is null
                            )/
                            (select sum(dream_request_list.amount) from dream_request_list where dream_request_list.request_id=dream_requests.id and dream_request_list.delete_time is null
                            )
                          ) as progress
                        ")
                        )
                        ->where(function ($query) use ($search){
                            $query->orWhere('dream_requests.description', 'LIKE', "%$search%")->orWhere('to_storage.name', 'LIKE',  "%$search%")->orWhere('from_storage.name', 'LIKE',  "%$search%")
                                ->orWhere('users.firstname', 'LIKE',  "%$search%")->orWhere('dream_request_states.name', 'LIKE',  "%$search%")->orWhere('dream_requests.create_time', 'LIKE', "%$search%")
                                ->orWhere('dream_requests.id', 'LIKE', "%$search%")->orWhere('dream_items.articula_new', 'LIKE', "%$search%")->orWhere('dream_items.item_name', 'LIKE', "%$search%");
                        })->where('dream_requests.type_id', 1)
                        ->whereDate('dream_requests.create_time',"<=",$endDate)
                        ->whereDate('dream_requests.create_time',">=",$startdate)
                        ->offset($start)->limit($limit)->orderBy($order, $dir)->get();

                    $filteredData = DB::table('dream_requests')
                        ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                        ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                        ->leftJoin('dream_items', 'dream_items.id', '=', 'dream_requests.model_id')
                        ->join('users', 'users.id', '=', 'dream_requests.user_id')
                        ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                        ->where(function ($query) use ($search){
                            $query->orWhere('dream_requests.description', 'LIKE', "%$search%")->orWhere('to_storage.name', 'LIKE',  "%$search%")->orWhere('from_storage.name', 'LIKE',  "%$search%")
                                ->orWhere('users.firstname', 'LIKE',  "%$search%")->orWhere('dream_request_states.name', 'LIKE',  "%$search%")->orWhere('dream_requests.create_time', 'LIKE', "%$search%")
                                ->orWhere('dream_requests.id', 'LIKE', "%$search%")->orWhere('dream_items.articula_new', 'LIKE', "%$search%")->orWhere('dream_items.item_name', 'LIKE', "%$search%");
                        })->where('dream_requests.type_id', 1)
                        ->whereDate('dream_requests.create_time',"<=",$endDate)
                        ->whereDate('dream_requests.create_time',">=",$startdate)
                        ->select('dream_requests.*', 'to_storage.name as to_storage_name', DB::raw('IFNULL(from_storage.name, " ") as from_storage_name'), 'users.firstname as user_name',
                            'dream_request_states.name as state_name', 'dream_request_states.id as state_id','dream_items.articula_new', 'dream_items.item_name')
                        ->get()->count();

                }
            }else{
                if (empty($request->input('search.value'))){
                    $items = DB::table('dream_requests')
                        ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                        ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                        ->leftJoin('dream_items', 'dream_items.id', '=', 'dream_requests.model_id')
                        ->join('users', 'users.id', '=', 'dream_requests.user_id')
                        ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                        ->select('dream_requests.*', 'to_storage.name as to_storage_name', 'from_storage.name as from_storage_name', 'users.firstname as user_name',
                            'dream_request_states.name as state_name', 'dream_request_states.id as state_id', 'dream_items.articula_new', 'dream_items.item_name',
                            DB::raw("
                          (
                            (select sum(dream_transaction_list.amount) from dream_transaction_list where id in
                              (
                                select dream_transaction_request_bind.transaction_list_id from dream_transaction_request_bind
                                inner join dream_request_list on dream_request_list.id=dream_transaction_request_bind.request_list_id
                                where dream_request_list.request_id=dream_requests.id and dream_request_list.delete_time is null
                              )  and dream_transaction_list.delete_time is null
                            )/
                            (select sum(dream_request_list.amount) from dream_request_list where dream_request_list.request_id=dream_requests.id and dream_request_list.delete_time is null
                            )
                          ) as progress
                        ")
                        )->where('dream_requests.type_id', 1)
                        ->whereDate('dream_requests.create_time',"<=",$endDate)
                        ->whereDate('dream_requests.create_time',">=",$startdate)
                        ->whereRaw($str)
                        ->limit($limit)->orderBy($order, $dir)->get();

                    $filteredData = DB::table('dream_requests')
                        ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                        ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                        ->leftJoin('dream_items', 'dream_items.id', '=', 'dream_requests.model_id')
                        ->join('users', 'users.id', '=', 'dream_requests.user_id')
                        ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                        ->whereRaw($str)->where('dream_requests.type_id', 1)
                        ->whereDate('dream_requests.create_time',"<=",$endDate)
                        ->whereDate('dream_requests.create_time',">=",$startdate)
                        ->select('dream_requests.id', 'dream_request_states.name as state_name', 'to_storage.name as to_storage_name', DB::raw('IFNULL(from_storage.name, " ") as from_storage_name'),
                            'dream_requests.create_time', 'users.firstname as user_name', 'dream_items.articula_new', 'dream_items.item_name')
                        ->get()->count();

                }else{
                    $search = $request->input('search.value');
                    $items = DB::table('dream_requests')
                        ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                        ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                        ->leftJoin('dream_items', 'dream_items.id', '=', 'dream_requests.model_id')
                        ->join('users', 'users.id', '=', 'dream_requests.user_id')
                        ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                        ->select('dream_requests.*', 'to_storage.name as to_storage_name', 'from_storage.name as from_storage_name', 'users.firstname as user_name',
                            'dream_request_states.name as state_name', 'dream_items.articula_new', 'dream_items.item_name',
                            DB::raw("
                          (
                            (select sum(dream_transaction_list.amount) from dream_transaction_list where id in
                              (
                                select dream_transaction_request_bind.transaction_list_id from dream_transaction_request_bind
                                inner join dream_request_list on dream_request_list.id=dream_transaction_request_bind.request_list_id
                                where dream_request_list.request_id=dream_requests.id and dream_request_list.delete_time is null
                              )  and dream_transaction_list.delete_time is null
                            )/
                            (select sum(dream_request_list.amount) from dream_request_list where dream_request_list.request_id=dream_requests.id and dream_request_list.delete_time is null
                            )
                          ) as progress
                        ")
                        )
                        ->whereDate('dream_requests.create_time',"<=",$endDate)
                        ->whereDate('dream_requests.create_time',">=",$startdate)
                        ->where(function ($query) use ($search){
                            $query->orWhere('dream_requests.description', 'LIKE', "%$search%")->orWhere('to_storage.name', 'LIKE',  "%$search%")->orWhere('from_storage.name', 'LIKE',  "%$search%")
                                ->orWhere('users.firstname', 'LIKE',  "%$search%")->orWhere('dream_request_states.name', 'LIKE',  "%$search%")->orWhere('dream_requests.create_time', 'LIKE', "%$search%")
                                ->orWhere('dream_requests.id', 'LIKE', "%$search%")->orWhere('dream_items.articula_new', 'LIKE', "%$search%")->orWhere('dream_items.item_name', 'LIKE', "%$search%");
                        })->where('dream_requests.type_id', 1)
                        ->whereRaw($str)
                        ->limit($limit)->orderBy($order, $dir)->get();

                    $filteredData = DB::table('dream_requests')
                        ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                        ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                        ->leftJoin('dream_items', 'dream_items.id', '=', 'dream_requests.model_id')
                        ->join('users', 'users.id', '=', 'dream_requests.user_id')
                        ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                        ->whereDate('dream_requests.create_time',"<=",$endDate)
                        ->whereDate('dream_requests.create_time',">=",$startdate)
                        ->where(function ($query) use ($search){
                            $query->orWhere('dream_requests.description', 'LIKE', "%$search%")->orWhere('to_storage.name', 'LIKE',  "%$search%")->orWhere('from_storage.name', 'LIKE',  "%$search%")
                                ->orWhere('users.firstname', 'LIKE',  "%$search%")->orWhere('dream_request_states.name', 'LIKE',  "%$search%")->orWhere('dream_requests.create_time', 'LIKE', "%$search%")
                                ->orWhere('dream_requests.id', 'LIKE', "%$search%")->orWhere('dream_items.articula_new', 'LIKE', "%$search%")->orWhere('dream_items.item_name', 'LIKE', "%$search%");
                        })->where('dream_requests.type_id', 1)
                        ->whereRaw($str)
                        ->select('dream_requests.id', 'dream_request_states.name as state_name', 'to_storage.name as to_storage_name', DB::raw('IFNULL(from_storage.name, " ") as from_storage_name'),
                            'dream_requests.create_time', 'users.firstname as user_name', 'dream_items.articula_new', 'dream_items.item_name')
                        ->get()->count();
                }
            }
            $totalFiltered = $filteredData;
            $data = array();
            $tr = [];
            $roles = array('3');
            foreach ($columns as $key=>$column){
                if ($column=='from_storage_name'){
                    $storage = DB::table('dream_storages')->whereIn('type_id', array(1,2,3,4))->select('name as name')->get()->toArray();
                    $tr = array_add($tr, $key, $storage);

                }
                else if ($column=='state_name'){
                    $states = DB::table('dream_request_states')->select('name as name')->get()->toArray();
                    $tr = array_add($tr, $key, $states);
                }
                else if ($column=='to_storage_name'){
                    $from_dest = DB::table('dream_storages')->whereIn('type_id', array(1,2,3))->select('name as name')->get()->toArray();
                    $tr = array_add($tr, $key, $from_dest);
                }
                else if ($column=='user_name'){
                    $username = DB::table('users')
                        ->join('user_role', 'user_role.user_id', '=', 'users.id')
                        ->join('roles', 'roles.id', '=', 'user_role.role_id')
                        ->select('firstname as name')
                        ->whereIn('roles.id', $roles)
                        ->get()->toArray();
                    array_push($username, ['name'=>'admin']);
                    $tr = array_add($tr, $key, $username);
                }
            }
            if(!empty($items)){
                foreach ($items as $item)
                {
                    $nestedData['DT_RowId'] = $item->id;
                    $nestedData['id'] = $item->id;
                    $nestedData['state'] = ($item->state==self::PROCESSED)?"<span class='label label-primary' style='font-size: 14px; padding-top: 0;'>".$item->state_name."</span>":"<span class='label label-danger' style='font-size: 14px; padding-top: 0;'>".$item->state_name."</span>";
                    if ($item->progress>=1){
                        $nestedData['progress'] = "<div class='progress progress-striped' style='height: 22px; padding: 0; margin: 0; background-color: #eababa'><div class='progress-bar progress-bar-danger' role='progressbar' aria-valuenow='100' aria-valuemin='0' aria-valuemax='0' style='width:100%; color: black'>".(round($item->progress, 3)*100)."<span>%</span></div></div>";
                    }else if ($item->progress>0 && $item->progress<1){
                        $nestedData['progress'] = "<div class='progress progress-striped' style='height: 22px; padding: 0; margin: 0; background-color: #eababa'><p style='position: absolute; color: black; font-weight: bold;'>".(round($item->progress, 3)*100)." %</p><div class='progress-bar progress-bar-stripped' role='progressbar' aria-valuenow='".(round($item->progress, 3)*100)."' aria-valuemin='0' aria-valuemax='0' style='width:".(round($item->progress, 3)*100)."%; text-align: center'></div></div>";
                    }else{
                        $nestedData['progress'] = "<div class='progress progress-striped' style='height: 22px; padding: 0; margin: 0; background-color: #eababa'><p style='position: absolute; color: black; font-weight: bold;'>0 %</p><div class='progress-bar progress-bar-stripped' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='0' style='width:".(round($item->progress, 3)*100)."%; text-align: center'></div></div>";
                    }
                    $nestedData['to_storage'] = $item->to_storage_name;
                    $nestedData['from_storage'] = $item->from_storage_name;
                    $nestedData['articula_new'] = $item->articula_new;
                    $nestedData['item_name'] = $item->item_name;
                    $nestedData['amount'] = $item->amount;
                    $nestedData['create_time'] = Carbon::createFromFormat('Y-m-d H:i:s', $item->create_time)->format('Y-m-d H:i');
                    $nestedData['applier'] = $item->user_name;
                    $nestedData['description'] = $item->description;
//                    $nestedData['detail'] = "<button type='button' data-toggle='modal' data-backdrop='static' data-keyboard='false'  data-target='#show_detail' class='btn btn-info ' onclick='show_detail(".$item->id.")'><i class='fa fa-info-circle'></i></button>";
                    $nestedData['detail'] = "";

                    $data[] = $nestedData;

                }
            }

            $json_data = array(
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => intval($totalData),
                "recordsFiltered" => intval($totalFiltered),
                "data"            => $data,
                "filteredData"    => $tr,
                "filters"         => $filters
            );

            return json_encode($json_data);
        }catch (\Exception $e){
            return response()->json(['errors'=>array($e)], 500);
        }
    }

    public function get_request_list_items(Request $request){
        $request_id = $request->request_id;
        $data = array();
        $total = 0;
        $request_data = DreamRequest::find($request_id);

        $items = DB::table('dream_request_list')
            ->join('dream_items', 'dream_items.id', '=', 'dream_request_list.item_id')
            ->join('dream_units', 'dream_units.id', '=', 'dream_request_list.unit_id')
            ->select('dream_request_list.amount as request_amount', 'dream_items.id as item_id', 'dream_items.articula_new', 'dream_items.articula_old', 'dream_items.item_name',
                'dream_items.sap_code', 'dream_units.unit', 'dream_request_list.id',
                DB::raw("(select sum(dream_transaction_list.amount) as output from dream_transaction_request_bind
                                   inner join dream_transaction_list on dream_transaction_list.id=dream_transaction_request_bind.transaction_list_id
                                   where dream_transaction_list.id=dream_transaction_request_bind.transaction_list_id
                                   and dream_transaction_request_bind.request_list_id=dream_request_list.id
                                   and dream_transaction_list.item_id=dream_request_list.item_id and dream_transaction_list.unit_id=dream_request_list.unit_id and dream_transaction_list.delete_time is null
                                   group by dream_transaction_list.item_id=dream_request_list.item_id, dream_transaction_list.unit_id=dream_request_list.unit_id
                                   ) as output"))
            ->where([['dream_request_list.request_id', $request_id], ['dream_request_list.delete_time', null]])->get();
        $model = DB::table('dream_requests')
            ->leftJoin('dream_items', 'dream_items.id', '=', 'dream_requests.model_id')
            ->leftJoin('dream_storages', 'dream_storages.id', '=', 'dream_requests.from_storage_id')
            ->select('dream_items.item_name', 'dream_items.articula_new', 'dream_requests.amount', 'dream_storages.name as to_storage', 'dream_requests.description', 'dream_requests.create_time')
            ->where('dream_requests.id', $request_id)->first();
        if ($this->isFullyAccepted($items)){
            DB::table('dream_requests')->where('id', $request_id)->update(['state'=>2]);
        }
        if (DreamRequest::find($request_id)->user_id==Auth::user()->id){
            $hasPerm = true;
        }else{
            $hasPerm = false;
        }
        if (!empty($items)){
            foreach ($items as $item)
            {
                $remaining = $this->get_item_remaining($request_data->to_storage_id, $item->item_id);
                $total += round($item->request_amount, 3);
                $remaining_of_output = $item->request_amount - $item->output;
                $nestedData['DT_RowId'] = $item->id;
                $nestedData['articula_new'] = $item->articula_new;
                $nestedData['articula_old'] = $item->articula_old;
                $nestedData['sap_code'] = $item->sap_code;
                $nestedData['item_name'] = $item->item_name;
                $nestedData['amount'] = round($item->request_amount, 3);
                $nestedData['output'] = round($item->output, 3);
                $nestedData['input_field'] = "<input type='number' value='$remaining_of_output' name='outputs[]' disabled>";
                if ($remaining_of_output>$remaining){
                    $nestedData['remaining'] = "<p style='background-color: red; color: white; padding: 0; margin: 0; display: block'>&nbsp;".round($remaining, 3)."</p>";
                }else{
                    $nestedData['remaining'] = round($remaining, 3);
                }
                $nestedData['unit'] = $item->unit;
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "data" => $data,
            "model" => $model,
            "hasPerm" => $hasPerm,
            "requestData" => $request_data,
            "total" => $total
        );

        return json_encode($json_data);
    }

    public function get_request_list_items_by_storage(Request $request){
        $request_id = $request->request_id;
        $data = array();
        $total = 0;
        $request_data = DreamRequest::find($request_id);
        $allowToAccept = false;

        $items = DB::table('dream_request_list')
            ->join('dream_items', 'dream_items.id', '=', 'dream_request_list.item_id')
            ->join('dream_units', 'dream_units.id', '=', 'dream_request_list.unit_id')
            ->select('dream_request_list.amount as request_amount', 'dream_items.id as item_id', 'dream_items.articula_new', 'dream_items.articula_old', 'dream_items.item_name',
                'dream_items.sap_code', 'dream_units.unit', 'dream_request_list.id',
                DB::raw("(select sum(dream_transaction_list.amount) as output from dream_transaction_request_bind
                                   inner join dream_transaction_list on dream_transaction_list.id=dream_transaction_request_bind.transaction_list_id
                                   where dream_transaction_list.id=dream_transaction_request_bind.transaction_list_id
                                   and dream_transaction_request_bind.request_list_id=dream_request_list.id
                                   and dream_transaction_list.item_id=dream_request_list.item_id and dream_transaction_list.unit_id=dream_request_list.unit_id and dream_transaction_list.delete_time is null
                                   group by dream_transaction_list.item_id=dream_request_list.item_id, dream_transaction_list.unit_id=dream_request_list.unit_id
                                   ) as output"))
            ->where([['dream_request_list.request_id', $request_id], ['dream_request_list.delete_time', null]])->get();
        if ($this->isFullyAccepted($items)){
            DB::table('dream_requests')->where('id', $request_id)->update(['state'=>2]);
        }
        $model = DB::table('dream_requests')
            ->join('dream_items', 'dream_items.id', '=', 'dream_requests.model_id')
            ->select('dream_items.item_name', 'dream_items.articula_new', 'dream_requests.amount')
            ->where('dream_requests.id', $request_id)->first();
        if (DreamRequest::find($request_id)->user_id==Auth::user()->id){
            $hasPerm = true;
        }else{
            $hasPerm = false;
        }
        if (!empty($items)){
            foreach ($items as $item)
            {
                $total += round($item->request_amount, 3);
                $remaining = $this->get_item_remaining($request_data->to_storage_id, $item->item_id);
                $remaining_of_output = round(($item->request_amount - $item->output), 3);
                $nestedData['DT_RowId'] = $item->id;
                $nestedData['articula_new'] = $item->articula_new;
                $nestedData['articula_old'] = $item->articula_old;
                $nestedData['sap_code'] = $item->sap_code;
                $nestedData['item_name'] = $item->item_name;
                $nestedData['amount'] = $item->request_amount;
                $nestedData['output'] = round($item->output, 3);
                if ($remaining>0){
                    if ($remaining_of_output<=0){
                        $nestedData['input_field'] = "<input type='number' value='0' max='0' id='$item->id' name='outputs[]' onkeyup='this.value = 0;' disabled>";
                    }else{
                        $allowToAccept = true;
                        if ($remaining >= $remaining_of_output){
                            $nestedData['input_field'] = "<input type='number' value='$remaining_of_output' max='$remaining_of_output' id='$item->id' name='outputs[]' onkeyup='if(parseFloat(this.value) >= parseFloat(this.max)) this.value = this.max;'>";
                        }else{
                            $nestedData['input_field'] = "<input type='number' value='$remaining' max='$remaining' id='$item->id' name='outputs[]' onkeyup='if(parseFloat(this.value) >= parseFloat(this.max)) this.value = this.max;'>";
                        }
                    }
                }else{
                    $nestedData['input_field'] = "<input type='number' value='0' max='0' id='$item->id' name='outputs[]' onkeyup='this.value = 0;' disabled>";
                }
                if ($remaining>=round($item->request_amount, 3)){
                    $nestedData['remaining'] = $remaining;
                }else{
                    $nestedData['remaining'] = '<p style="display: block; background-color: red; color: white; padding: 0; margin: 0">'.$remaining.'</p>';
                }
                $nestedData['diff'] = $remaining_of_output;
                $nestedData['remaining_num'] = $remaining;
                $nestedData['unit'] = $item->unit;
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "data" => $data,
            "model" => $model,
            "hasPerm" => $hasPerm,
            "requestData" => $request_data,
            "total" => $total,
            "allowToAccept" => $allowToAccept
        );
        echo json_encode($json_data);
    }

    public function get_replace_items(){
        $list_id = Input::all()['list_id'];
        $item_id = DB::table('dream_request_list')->where('id', $list_id)->first()->item_id;
        $replace_items = DB::table('dream_replacement_items')->
        join('dream_item_bind', 'dream_replacement_items.item_bind_id', 'dream_item_bind.id')->
        join('dream_items', 'dream_items.id', '=', 'dream_replacement_items.item_id')->where('dream_item_bind.child_id', $item_id)->select('dream_items.*')->get();
        return json_encode($replace_items);
    }

//    correct
    public function accept_request(Request $request){
        try{
            DB::transaction(function () use($request){
                $request_id = $request->request_id;
                $outputs = $this->getEnoughItems(json_decode($request->outputs));
                if (empty($outputs)){
                    return response()->json(['errors'=>array('cant add')], 500);
                }
                $requestData = DreamRequest::find($request_id);
                $transaction = [
                    'user_id' => Auth::user()->id,
                    'description' => $requestData->description
                ];
                $transaction_id = DreamTransaction::create($transaction)->id;
                $output_from_storage = [
                    'transaction_id'=>$transaction_id,
                    'storage_id'=>$requestData->to_storage_id,
                    'operation_id'=>self::OUTPUT
                ];

                DB::table('dream_transaction_storage_bind')->insert($output_from_storage);

                if ($requestData->from_storage_id!=null){
                    $input_to_line_storage = [
                        'transaction_id'=>$transaction_id,
                        'storage_id'=>$requestData->from_storage_id,
                        'operation_id'=>self::INPUT
                    ];
                    DB::table('dream_transaction_storage_bind')->insert($input_to_line_storage);
                }

                foreach ($outputs as $key=>$output){
                    $request_list_item = DB::table('dream_request_list')->where('id', $key)->first();
                    if ($output<=$request_list_item->amount){
                        $data = [
                            'transaction_id' => $transaction_id,
                            'item_id' => $request_list_item->item_id,
                            'unit_id' => $request_list_item->unit_id,
                            'amount' => $output
                        ];
                    }else{
                        continue;
                    }
                    $transaction_list_id = DB::table('dream_transaction_list')->insertGetId($data);
                    DB::table('dream_transaction_request_bind')->insert(['transaction_list_id'=>$transaction_list_id, 'request_list_id'=>$key]);
                }
                $request_list_items = DB::table('dream_request_list')
                    ->join('dream_items', 'dream_items.id', '=', 'dream_request_list.item_id')
                    ->join('dream_units', 'dream_units.id', '=', 'dream_request_list.unit_id')
                    ->select('dream_request_list.amount as request_amount', 'dream_items.id as item_id', 'dream_items.articula_new', 'dream_items.articula_old', 'dream_items.item_name',
                        'dream_items.sap_code', 'dream_units.unit', 'dream_request_list.id',
                        DB::raw("(select sum(dream_transaction_list.amount) as output from dream_transaction_request_bind
                                   inner join dream_transaction_list on dream_transaction_list.id=dream_transaction_request_bind.transaction_list_id
                                   where dream_transaction_list.id=dream_transaction_request_bind.transaction_list_id
                                   and dream_transaction_request_bind.request_list_id=dream_request_list.id
                                   and dream_transaction_list.item_id=dream_request_list.item_id and dream_transaction_list.unit_id=dream_request_list.unit_id and dream_transaction_list.delete_time is null
                                   group by dream_transaction_list.item_id=dream_request_list.item_id, dream_transaction_list.unit_id=dream_request_list.unit_id
                                   ) as output"))
                    ->where([['dream_request_list.request_id', $request_id], ['dream_request_list.delete_time', null]])->get();
                if ($this->isFullyAccepted($request_list_items)){
                    DB::table('dream_requests')->where('id', $request_id)->update(['state'=>2]);
                }
                return $transaction;
            });
            return json_encode($request);
        }catch (\Exception $e){
            return response()->json(['errors'=>array($e)], 500);
        }
    }

    public function getEnoughItems($items){
        $arr = [];
        foreach ($items as $key => $item){
            if ($item>0){
                $arr = array_add($arr, $key, $item);
            }
        }
        return $arr;
    }

    public function isFullyAccepted($items){
        foreach ($items as $item){
            if ($item->request_amount!=$item->output){
                return false;
            }
        }
        return true;
    }

    public function get_accepted_request_list_history(Request $request){
        $request_id = $request->request_id;
        $all_items = DB::table('dream_request_list')
            ->join('dream_transaction_request_bind', 'dream_transaction_request_bind.request_list_id', '=', 'dream_request_list.id')
            ->join('dream_transaction_list', 'dream_transaction_list.id', '=', 'dream_transaction_request_bind.transaction_list_id')
            ->join('dream_transactions', 'dream_transactions.id', '=', 'dream_transaction_list.transaction_id')
            ->join('dream_items', 'dream_items.id', '=', 'dream_transaction_list.item_id')
            ->join('dream_units', 'dream_units.id', '=', 'dream_transaction_list.unit_id')
            ->select('dream_items.articula_new', 'dream_items.articula_old', 'dream_items.item_name', DB::raw('IFNULL(dream_items.sap_code, \'\') as sap_code'), 'dream_units.unit', 'dream_transaction_list.amount as output',
                'dream_request_list.amount as request_amount', 'dream_transactions.create_time as accepted_time', 'dream_transactions.id as transaction_id')
            ->where([['dream_request_list.request_id', $request_id], ['dream_request_list.delete_time', null], ['dream_transaction_list.delete_time', null]])->get()->toArray();
        $arr = $this->convertToHistoryArray($all_items);
        return json_encode($arr);
    }

    public function convertToHistoryArray($items){
        $arr = array();
        foreach ($items as $key => $item) {
            $arr[$item->transaction_id][$key] = $item;
        }

        ksort($arr, SORT_NUMERIC);
        return $arr;
    }

    public function delete_request(Request $request){
        if (Gate::denies('add_request', new DreamRequest)){
            return response()->json(['errors'=>array('нет доступа')], 500);
        }

        try{
            $request_ = DreamRequest::find(DB::table('dream_request_list')->where('request_id', $request->request_id)->first()->request_id);
            if ($this->isExistInTransaction($request->request_id)){
                return response()->json(['errors'=>array('вы не можете удалить')], 500);
            }
            else{
                if ($request_->user_id!=Auth::user()->id){
                    return response()->json(['errors'=>array('вы не можете удалить')], 500);
                }else{
                    if (DreamRequest::find($request->request_id)->state == self::PROCESSED) {
                        return response()->json(['errors' => array('вы не можете удалить!')], 500);
                    }else{
                        DB::table('dream_requests')->where('id', $request->request_id)->delete();
                    }
                }
            }
            return json_encode($request->all());
        }catch (\Exception $exception){
            return response()->json(['errors'=>array($exception)], 500);
        }
    }

    public function close_request(Request $request){
        if (Gate::denies('add_request', new DreamRequest)){
            return response()->json(['errors'=>array('нет доступа')], 500);
        }

        try{
            $request_ = DreamRequest::find(DB::table('dream_request_list')->where('request_id', $request->request_id)->first()->request_id);

            if ($request_->user_id!=Auth::user()->id){
                return response()->json(['errors'=>array('вы не можете закрыть')], 500);
            }else{
                DB::table('dream_requests')->where('id', $request->request_id)->update(['state'=>2]);
            }
            return json_encode($request->all());
        }catch (\Exception $exception){
            return response()->json(['errors'=>array($exception)], 500);
        }
    }

    public function isExistInTransaction($request_id){
        $isExistInTransaction = DB::table('dream_request_list')
            ->join('dream_transaction_request_bind', 'dream_transaction_request_bind.request_list_id', '=', 'dream_request_list.id')
            ->where('dream_request_list.request_id', $request_id)->get()->count();
        if ($isExistInTransaction>0){
            return true;
        }
        return false;
    }

    public function update_request(Request $request){
        if (Gate::denies('add_request', new DreamRequest)){
            return response()->json(['errors'=>array('нет доступа')], 500);
        }

        if ($this->isExistInTransaction($request->request_id)){
            return response()->json(['errors'=>array('вы не можете обнавить')], 500);
        }
        else {
            if (DreamRequest::find($request->request_id)->user_id != Auth::user()->id) {
                return response()->json(['errors' => array('вы не можете обнавить!')], 500);
            }

            if (DreamRequest::find($request->request_id)->state == self::PROCESSED) {
                return response()->json(['errors' => array('вы не можете обнавить!')], 500);
            }
        }

        $data = [
            'description' => $request->description,
        ];
        DreamRequest::find((int)$request->request_id)->update($data);
        return json_encode($request->description);
    }

    public function get_requests_belongs_storage(Request $request){
        $storage_id = $request->storage_id;
        $columns = array(
            0 => 'id',
            1 => 'progress',
            2 => 'user_name',
            3 => 'create_time',
            4 => 'articula_new',
            5 => 'item_name',
            6 => 'amount',
            7 => 'from_storage_name',
            8 => 'description',
        );
        $filterColumns = array(
            0 => 'dream_requests.id',
            1 => 'progress',
            2 => 'users.firstname',
            3 => 'dream_requests.create_time',
            4 => 'dream_items.articula_new',
            5 => 'dream_items.item_name',
            6 => 'dream_requests.amount',
            7 => 'from_storage.name',
            8 => 'dream_requests.description'
        );
        $totalData = DreamRequest::where([['state', self::STATE_ACTIVE], ['to_storage_id', $storage_id], ['type_id', self::REQUEST_TYPE]])->count();
        if ($request->input('length')==-1){
            $limit = $totalData;
        }else{
            $limit = $request->input('length');
        }
        $filters = [];
        $str = '';
        foreach ($request->columns as $key=>$column){
            if ($column['search']['value']!=null){
                $filters = array_add($filters, $key, $column['search']['value']);
                $str.=$filterColumns[$key]."='".$column['search']['value']."' and ";
            }
        }
        if (!empty($str)>0){
            $str = substr($str, 0, strlen($str)-4);
        }
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if (!is_null($storage_id)){
            if (count($filters)==0){
                if (empty($request->input('search.value'))){
                    $items = DB::table('dream_requests')
                        ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                        ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                        ->leftJoin('dream_items', 'dream_items.id', '=', 'dream_requests.model_id')
                        ->join('users', 'users.id', '=', 'dream_requests.user_id')
                        ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                        ->select('dream_requests.*', 'from_storage.name as from_storage_name', 'to_storage.name as to_storage_name', 'users.firstname as user_name',
                            'dream_request_states.name as state_name', 'dream_request_states.id as state_id',
                            'dream_items.articula_new', 'dream_items.item_name',
                            DB::raw("
                              (
                                (select sum(dream_transaction_list.amount) from dream_transaction_list where id in
                                  (
                                    select dream_transaction_request_bind.transaction_list_id from dream_transaction_request_bind
                                    inner join dream_request_list on dream_request_list.id=dream_transaction_request_bind.request_list_id
                                    where dream_request_list.request_id=dream_requests.id and dream_request_list.delete_time is null and dream_transaction_list.delete_time is null
                                  )
                                )/
                                (select sum(dream_request_list.amount) from dream_request_list where dream_request_list.request_id=dream_requests.id and dream_request_list.delete_time is null
                                )
                              ) as progress
                            ")
                            )
                        ->where([['dream_requests.to_storage_id', $storage_id], ['dream_requests.state', self::STATE_ACTIVE], ['dream_requests.type_id', 1]])
                        ->offset($start)->limit($limit)->orderBy($order, $dir)->get();

                    $filteredData = DB::table('dream_requests')
                        ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                        ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                        ->leftJoin('dream_items', 'dream_items.id', '=', 'dream_requests.model_id')
                        ->join('users', 'users.id', '=', 'dream_requests.user_id')
                        ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                        ->select('dream_requests.*', 'from_storage.name as from_storage_name',
                            DB::raw('IFNULL(to_storage.name, " ") as to_storage_name'), 'users.firstname as user_name',
                            'dream_request_states.name as state_name', 'dream_request_states.id as state_id', 'dream_items.articula_new', 'dream_items.item_name')
                        ->where([['dream_requests.to_storage_id', $storage_id], ['dream_requests.state', self::STATE_ACTIVE], ['dream_requests.type_id', 1]])
                        ->get();

                    $totalFiltered = count($filteredData);
                }else{
                    $search = $request->input('search.value');
                    $items = DB::table('dream_requests')
                        ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                        ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                        ->leftJoin('dream_items', 'dream_items.id', '=', 'dream_requests.model_id')
                        ->join('users', 'users.id', '=', 'dream_requests.user_id')
                        ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                        ->select('dream_requests.*', 'from_storage.name as from_storage_name', 'to_storage.name as to_storage_name', 'users.firstname as user_name',
                            'dream_request_states.name as state_name', 'dream_items.articula_new', 'dream_items.item_name',
                            DB::raw("
                              (
                                (select sum(dream_transaction_list.amount) from dream_transaction_list where id in
                                  (
                                    select dream_transaction_request_bind.transaction_list_id from dream_transaction_request_bind
                                    inner join dream_request_list on dream_request_list.id=dream_transaction_request_bind.request_list_id
                                    where dream_request_list.request_id=dream_requests.id and dream_request_list.delete_time is null and dream_transaction_list.delete_time is null
                                  )
                                )/
                                (select sum(dream_request_list.amount) from dream_request_list where dream_request_list.request_id=dream_requests.id and dream_request_list.delete_time is null
                                )
                              ) as progress
                            ")
                            )
                        ->where(function ($query) use ($search){
                            $query->orWhere('dream_requests.description', 'LIKE', "%$search%")->orWhere('from_storage.name', 'LIKE',  "%$search%")->orWhere('to_storage.name', 'LIKE',  "%$search%")
                                ->orWhere('users.firstname', 'LIKE',  "%$search%")->orWhere('dream_request_states.name', 'LIKE',  "%$search%")->orWhere('dream_requests.create_time', 'LIKE', "%$search%")
                                ->orWhere('dream_requests.id', 'LIKE', "%$search%")->orWhere('dream_items.articula_new', 'LIKE', "%$search%")->orWhere('dream_items.item_name', 'LIKE', "%$search%");
                        })
                        ->where([['dream_requests.to_storage_id', $storage_id], ['dream_requests.state', self::STATE_ACTIVE], ['dream_requests.type_id', 1]])
                        ->offset($start)->limit($limit)->orderBy($order, $dir)->get();

                    $filteredData = DB::table('dream_requests')
                        ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                        ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                        ->leftJoin('dream_items', 'dream_items.id', '=', 'dream_requests.model_id')
                        ->join('users', 'users.id', '=', 'dream_requests.user_id')
                        ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                        ->where(function ($query) use ($search){
                            $query->orWhere('dream_requests.description', 'LIKE', "%$search%")->orWhere('from_storage.name', 'LIKE',  "%$search%")->orWhere('to_storage.name', 'LIKE',  "%$search%")
                                ->orWhere('users.firstname', 'LIKE',  "%$search%")->orWhere('dream_request_states.name', 'LIKE',  "%$search%")->orWhere('dream_requests.create_time', 'LIKE', "%$search%")
                                ->orWhere('dream_requests.id', 'LIKE', "%$search%")->orWhere('dream_items.articula_new', 'LIKE', "%$search%")->orWhere('dream_items.item_name', 'LIKE', "%$search%");
                        })
                        ->where([['dream_requests.to_storage_id', $storage_id], ['dream_requests.state', self::STATE_ACTIVE], ['dream_requests.type_id', 1]])
                        ->select('dream_requests.*', 'from_storage.name as from_storage_name', DB::raw('IFNULL(to_storage.name, " ") as to_storage_name'), 'users.firstname as user_name',
                            'dream_request_states.name as state_name', 'dream_request_states.id as state_id','dream_items.articula_new', 'dream_items.item_name')
                        ->orderBy($order, $dir)->get();
                    $totalFiltered = count($filteredData);

                }
            }else{
                if (empty($request->input('search.value'))){
                    $items = DB::table('dream_requests')
                        ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                        ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                        ->leftJoin('dream_items', 'dream_items.id', '=', 'dream_requests.model_id')
                        ->join('users', 'users.id', '=', 'dream_requests.user_id')
                        ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                        ->select('dream_requests.*', 'from_storage.name as from_storage_name', 'to_storage.name as to_storage_name', 'users.firstname as user_name',
                            'dream_request_states.name as state_name', 'dream_request_states.id as state_id', 'dream_items.articula_new', 'dream_items.item_name',
                            DB::raw("
                              (
                                (select sum(dream_transaction_list.amount) from dream_transaction_list where id in
                                  (
                                    select dream_transaction_request_bind.transaction_list_id from dream_transaction_request_bind
                                    inner join dream_request_list on dream_request_list.id=dream_transaction_request_bind.request_list_id
                                    where dream_request_list.request_id=dream_requests.id and dream_request_list.delete_time is null and dream_transaction_list.delete_time is null
                                  )
                                )/
                                (select sum(dream_request_list.amount) from dream_request_list where dream_request_list.request_id=dream_requests.id and dream_request_list.delete_time is null
                                )
                              ) as progress
                            ")
                            )
                        ->whereRaw($str)
                        ->where([['dream_requests.to_storage_id', $storage_id], ['dream_requests.state', self::STATE_ACTIVE], ['dream_requests.type_id', 1]])
                        ->offset($start)->limit($limit)->orderBy($order, $dir)->get();

                    $filteredData = DB::table('dream_requests')
                        ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                        ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                        ->leftJoin('dream_items', 'dream_items.id', '=', 'dream_requests.model_id')
                        ->join('users', 'users.id', '=', 'dream_requests.user_id')
                        ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                        ->whereRaw($str)
                        ->where([['dream_requests.to_storage_id', $storage_id], ['dream_requests.state', self::STATE_ACTIVE], ['dream_requests.type_id', 1]])
                        ->select('dream_requests.id', 'dream_request_states.name as state_name', 'from_storage.name as from_storage_name', DB::raw('IFNULL(to_storage.name, " ") as to_storage_name'),
                            'dream_requests.create_time', 'users.firstname as user_name', 'dream_items.articula_new', 'dream_items.item_name')
                        ->get();
                    $totalFiltered = count($filteredData);

                }else{
                    $search = $request->input('search.value');
                    $items = DB::table('dream_requests')
                        ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                        ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                        ->leftJoin('dream_items', 'dream_items.id', '=', 'dream_requests.model_id')
                        ->join('users', 'users.id', '=', 'dream_requests.user_id')
                        ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                        ->select('dream_requests.*', 'from_storage.name as from_storage_name', 'to_storage.name as to_storage_name', 'users.firstname as user_name',
                            'dream_request_states.name as state_name', 'dream_items.articula_new', 'dream_items.item_name',
                            DB::raw("
                              (
                                (select sum(dream_transaction_list.amount) from dream_transaction_list where id in
                                  (
                                    select dream_transaction_request_bind.transaction_list_id from dream_transaction_request_bind
                                    inner join dream_request_list on dream_request_list.id=dream_transaction_request_bind.request_list_id
                                    where dream_request_list.request_id=dream_requests.id and dream_request_list.delete_time is null and dream_transaction_list.delete_time is null
                                  )
                                )/
                                (select sum(dream_request_list.amount) from dream_request_list where dream_request_list.request_id=dream_requests.id and dream_request_list.delete_time is null
                                )
                              ) as progress
                            ")
                            )
                        ->where(function ($query) use ($search){
                            $query->orWhere('dream_requests.description', 'LIKE', "%$search%")->orWhere('from_storage.name', 'LIKE',  "%$search%")->orWhere('to_storage.name', 'LIKE',  "%$search%")
                                ->orWhere('users.firstname', 'LIKE',  "%$search%")->orWhere('dream_request_states.name', 'LIKE',  "%$search%")->orWhere('dream_requests.create_time', 'LIKE', "%$search%")
                                ->orWhere('dream_requests.id', 'LIKE', "%$search%")->orWhere('dream_items.articula_new', 'LIKE', "%$search%")->orWhere('dream_items.item_name', 'LIKE', "%$search%");
                        })
                        ->whereRaw($str)
                        ->where([['dream_requests.to_storage_id', $storage_id], ['dream_requests.state', self::STATE_ACTIVE], ['dream_requests.type_id', 1]])
                        ->offset($start)->limit($limit)->orderBy($order, $dir)->get();

                    $filteredData = DB::table('dream_requests')
                        ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                        ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                        ->leftJoin('dream_items', 'dream_items.id', '=', 'dream_requests.model_id')
                        ->join('users', 'users.id', '=', 'dream_requests.user_id')
                        ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                        ->where(function ($query) use ($search){
                            $query->orWhere('dream_requests.description', 'LIKE', "%$search%")->orWhere('from_storage.name', 'LIKE',  "%$search%")->orWhere('to_storage.name', 'LIKE',  "%$search%")
                                ->orWhere('users.firstname', 'LIKE',  "%$search%")->orWhere('dream_request_states.name', 'LIKE',  "%$search%")->orWhere('dream_requests.create_time', 'LIKE', "%$search%")
                                ->orWhere('dream_requests.id', 'LIKE', "%$search%")->orWhere('dream_items.articula_new', 'LIKE', "%$search%")->orWhere('dream_items.item_name', 'LIKE', "%$search%");
                        })
                        ->whereRaw($str)
                        ->where([['dream_requests.to_storage_id', $storage_id], ['dream_requests.state', self::STATE_ACTIVE], ['dream_requests.type_id', 1]])
                        ->select('dream_requests.id', 'dream_request_states.name as state_name', 'from_storage.name as from_storage_name', DB::raw('IFNULL(to_storage.name, " ") as to_storage_name'),
                            'dream_requests.create_time', 'users.firstname as user_name', 'dream_items.articula_new', 'dream_items.item_name')
                        ->orderBy($order, $dir)->get();
                    $totalFiltered = count($filteredData);
                }
            }
        }else{
            $items = [];
            $totalFiltered = 0;
            $filteredData = [];
        }
        $data = array();
        $tr = [];
        if (!empty($filteredData)){
            foreach ($columns as $key=>$column){
                $tr = array_add($tr, $key, $filteredData->unique($column)->pluck($column)->all());
            }
        }else{
            $tr = [];
        }
        if (!empty($items)){
            foreach ($items as $item)
            {
                $nestedData['DT_RowId'] = $item->id;
                $nestedData['id'] = $item->id;
                if ($item->progress>=1){
                    $nestedData['progress'] = "<div class='progress progress-striped' style='height: 22px; padding: 0; margin: 0; background-color: #eababa'><div class='progress-bar progress-bar-danger' role='progressbar' aria-valuenow='100' aria-valuemin='0' aria-valuemax='0' style='width:100%; color: black'>".(round($item->progress, 3)*100)."<span>%</span></div></div>";
                }else if ($item->progress>0 && $item->progress<1){
                    $nestedData['progress'] = "<div class='progress progress-striped' style='height: 22px; padding: 0; margin: 0; background-color: #eababa'><p style='position: absolute; color: black; font-weight: bold;'>".(round($item->progress, 3)*100)." %</p><div class='progress-bar progress-bar-stripped' role='progressbar' aria-valuenow='".(round($item->progress, 3)*100)."' aria-valuemin='0' aria-valuemax='0' style='width:".(round($item->progress, 3)*100)."%; text-align: center'></div></div>";
                }else{
                    $nestedData['progress'] = "<div class='progress progress-striped' style='height: 22px; padding: 0; margin: 0; background-color: #eababa'><p style='position: absolute; color: black; font-weight: bold;'>0 %</p><div class='progress-bar progress-bar-stripped' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='0' style='width:".(round($item->progress, 3)*100)."%; text-align: center'></div></div>";
                }
                $nestedData['from_storage'] = $item->from_storage_name;
                $nestedData['articula_new'] = $item->articula_new;
                $nestedData['item_name'] = $item->item_name;
                $nestedData['amount'] = round($item->amount, 3);
                $nestedData['create_time'] = Carbon::createFromFormat('Y-m-d H:i:s', $item->create_time)->format('Y-m-d H:i');
                $nestedData['applier'] = $item->user_name;
                $nestedData['description'] = $item->description;
                $nestedData['detail'] = "<button type='button' data-toggle='modal' data-backdrop='static' data-keyboard='false'  data-target='#show_request_detail' class='btn btn-info ' onclick='show_request_detail(".$item->id.")'><i class='fa fa-info-circle'></i></button>";
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
            "filteredData"    => $tr,
            "filters"         => $filters
        );

        return json_encode($json_data);
    }

    public function getNotAcceptedRequestsAmount(){
        $storage_user = DB::table('dream_storage_user_bind')
            ->select('dream_storage_user_bind.*')
            ->join('dream_storages', 'dream_storages.id', '=', 'dream_storage_user_bind.storage_id')
            ->where([['dream_storage_user_bind.user_id', Auth::user()->id], ['dream_storages.type_id', 1]])->get();
        $storage_user_arr = array();
        foreach ($storage_user as $item){
            $storage_user_arr[] = $item->storage_id;
        }
        $defective_storage_user = DB::table('dream_storage_user_bind')
            ->select('dream_storage_user_bind.*')
            ->join('dream_storages', 'dream_storages.id', '=', 'dream_storage_user_bind.storage_id')
            ->where([['dream_storage_user_bind.user_id', Auth::user()->id], ['dream_storages.type_id', self::DEFECTIVE_STORAGE]])->get();
        $defective_storage_user_arr = array();
        foreach ($defective_storage_user as $item){
            $defective_storage_user_arr[] = $item->storage_id;
        }

        if (!empty($storage_user_arr)){
            $notAcceptedRequests = DB::table('dream_requests')->where([['state', 1]])
                ->whereIn('to_storage_id', $storage_user_arr)
                ->whereIn('type_id', array(self::REQUEST_TYPE, self::RETURN_BACK, self::MOVING_REQUEST))->get()->count();

            $notAcceptedRequestsAmount = DB::table('dream_requests')->where([['state', 1]])
                ->whereIn('to_storage_id', $storage_user_arr)
                ->whereIn('type_id', array(self::REQUEST_TYPE))->get()->count();

            $notAcceptedReturnRequestsAmount = DB::table('dream_requests')->where([['state', 1]])
                ->whereIn('to_storage_id', $storage_user_arr)
                ->whereIn('type_id', array(self::RETURN_BACK))->get()->count();

            $notAcceptedMovingRequestsAmount = DB::table('dream_requests')->where([['state', 1]])
                ->whereIn('to_storage_id', $storage_user_arr)
                ->whereIn('type_id', array(self::MOVING_REQUEST))->get()->count();

            $activeOrdersCount = DB::table('logistics')
                ->where([['logistics.state_id', 1]])
                ->whereIn('logistics.to_storage_id', $storage_user_arr)->get()->count();
        }else{
            $notAcceptedRequests = null;
            $notAcceptedRequestsAmount = null;
            $notAcceptedReturnRequestsAmount = null;
            $notAcceptedMovingRequestsAmount = null;
            $activeOrdersCount = null;
        }

        if (!empty($defective_storage_user_arr)){
            $notAcceptedDefectiveRequests = DB::table('dream_requests')
                ->whereIn('to_storage_id', $defective_storage_user_arr)
                ->where([['state', 1], ['type_id', self::DEFECT_TYPE]])->get()->count();
        }else{
            $notAcceptedDefectiveRequests = null;
        }
        $data = [
            'requests'=>$notAcceptedRequestsAmount,
            'returns'=>$notAcceptedReturnRequestsAmount,
            'moving_requests'=>$notAcceptedMovingRequestsAmount,
            'existRequest'=>$notAcceptedRequests,
            'defective_requests'=>$notAcceptedDefectiveRequests,
            'ordersCount'=>$activeOrdersCount
        ];
        return json_encode($data);
    }
}

<?php

namespace App\Http\Controllers;

use App\Permission;
use App\Role;
use Gate;
use Illuminate\Http\Request;

class PermissionController extends Controller
{

    public function index(){

        if (Gate::denies('VIEW_USERS_PAGE')){
            abort(403);
        }

        $roles = $this->getRoles();
        $permissions = Permission::all();

        return view('index', ['page'=>'users.permissions', 'permissions'=>$permissions, 'roles'=>$roles])->with('title','Xuquqlarni boshqarish');
    }

    public function getRoles(){
        $roles = Role::all();
        return $roles;
    }

    public function store(Request $request){
        $permission = new Permission();
        $result = $permission->changePermissions($request);

        if (is_array($result) && !empty($result['error'])){
            return back()->with($result);
        }

        return back()->with($result);

    }
}

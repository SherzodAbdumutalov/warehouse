<?php

namespace App\Http\Controllers;

use App\DreamItem;
use App\DreamSupplier;
use App\DreamUnit;
use App\DreamStorage;
use Illuminate\Http\Response;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\StreamedResponse;
use function foo\func;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Validation;

class DreamItemController extends Controller
{
    const STATE_ACTIVE = 1;
    public function index(){
        $allProduct = DreamItem::all();
        $units = DreamUnit::all();
        $storages = DreamStorage::where('type_id', 1)->get();
        return view('index', ['page'=>'all_items', 'units'=>$units, 'all_items'=>$allProduct, 'storages'=>$storages]);
    }

    public function getItems(Request $request){
        $columns = array(
            0 => 'articula_new',
            1 => 'articula_old',
            2 => 'sap_code',
            3 => 'item_name',
            4 => 'dream_units.unit',
            5 => 'storages'
        );

        $totalData = DreamItem::count();
        if ($request->input('length')==-1){
            $limit = $totalData;
        }else{
            $limit = $request->input('length');
        }

        $storage_arr = '';
        $filterText = '';
        if (!is_null($request->storages)){
            foreach ($request->storages as $storage_id){
                $storage_arr .= $storage_id.',';
            }
            $storage_arr = substr($storage_arr, 0, strlen($storage_arr)-1);
            $filterText .= " and dream_storages.id in ($storage_arr) ";
        }

        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $query_text = "
        SELECT SQL_CALC_FOUND_ROWS  dream_items.id, dream_items.*, dream_units.unit, group_concat(dream_storages.name, \" \") as storages, d_i.parent_id FROM dream_items
        left join dream_storage_item_bind on dream_storage_item_bind.item_id=dream_items.id
        left join dream_storages on dream_storages.id=dream_storage_item_bind.storage_id and dream_storages.type_id=1
        left join dream_units on dream_units.id=dream_storage_item_bind.unit_id
        left join (select dream_item_bind.parent_id as parent_id from dream_item_bind group by dream_item_bind.parent_id) as d_i on d_i.parent_id=dream_items.id
        where dream_items.id>0
        ";

        $search_text = '';
        if (!empty($request->input('search.value'))){
            $search = $request->input('search.value');
            $search_text .= "
             and ( dream_items.articula_new like '%$search%' || dream_items.articula_old like '%$search%' || dream_storages.name like '%$search%' || dream_items.sap_code like '%$search%' ||
             dream_items.item_name like '%$search%' )
            ";
        }


        $query_text.=$search_text.$filterText." group by dream_items.id, dream_units.id order by $order $dir limit $limit offset $start;";
        $items = DB::select($query_text);
        $totalCount = DB::select('select found_rows() as total_count')[0];

        $hasPerm = true;
        $data = array();
        if (!empty($items)){
            foreach ($items as $item)
            {
                if (!is_null($item->parent_id)){
                    $hasNorm = '* ';
                }else{
                    $hasNorm = '';
                }
                $filename = public_path().'/assets/itemsPhoto/'.$item->articula_new.'.png';
                $nestedData['DT_RowId'] = "parent_item_".$item->id;
                $nestedData['id'] = $item->id;
                $nestedData['show_detail'] = "";
                if (file_exists($filename)) {
                    $nestedData['articula_new'] = "<p data-id='$item->articula_new' class='showImageLink' style='color: grey;' onmouseover=\"showImage(event, this)\" onmouseleave=\"hideImage(event)\">$item->articula_new</p>";
                } else {
                    $nestedData['articula_new'] = $item->articula_new;
                }
                $nestedData['articula_old'] = $item->articula_old;
                $nestedData['item_name'] = $item->item_name;
                $nestedData['sap_code'] = $item->sap_code;
                $nestedData['unit'] = $item->unit;
                $nestedData['storages'] = $item->storages;
                if (Gate::denies('save', new DreamItem)){
                    $hasPerm = false;
//                    $nestedData['editButtons'] = "<button type='button' class='btn btn-primary' data-toggle='modal' data-backdrop='static' data-keyboard='false' data-target='#ItemInfo' id='$item->id'><span class='fa fa-gear'></span></button>";
                    $nestedData['editButtons'] = "";
                }else{
                    $nestedData['editButtons'] = "<button type='button' class='btn btn-danger' onclick='delete_item(".$item->id.")'><i class='fa fa-trash'></i></button>";
                }
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalCount->total_count),
            "data"            => $data,
            "hasPerm"         => $hasPerm
        );

        return json_encode($json_data);
    }

    public function exportDetailedItemsNorm(Request $request){

        if(file_exists(public_path()."/WMS_detail_norm.xlsx")){
            unlink(public_path()."/WMS_detail_norm.xlsx");
        }


        try {

            $pagedItems = $request->items;
            //var_dump($pagedItems);
            //dd();


            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setCellValueByColumnAndRow(1, 1, "артикул ГП");
            $sheet->setCellValueByColumnAndRow(2, 1, "SAP код ГП");
            $sheet->setCellValueByColumnAndRow(3, 1, "наименование");
            $sheet->setCellValueByColumnAndRow(4, 1, "артикул");
            $sheet->setCellValueByColumnAndRow(5, 1, "старый артикул");
            $sheet->setCellValueByColumnAndRow(6, 1, "SAP code");
            $sheet->setCellValueByColumnAndRow(7, 1, "кол-во");
            $sheet->setCellValueByColumnAndRow(8, 1, "ед. изм.");
            $sheet->setCellValueByColumnAndRow(9, 1, "склад");
            $sheet->setCellValueByColumnAndRow(10, 1, "ед.изм. скл.");

            $row_num = 4;

            if(is_array($pagedItems)){
                foreach ($pagedItems as $item){
                    $parent_item_id = $item["id"];


                    //$sheet->mergeCellsByColumnAndRow(1, $row_num-2, 7, $row_num-2);
                    //$sheet->setCellValueByColumnAndRow(1, $row_num-2, $item["articula_new"] . " " . $item["item_name"] . $item["sap_code"]);

                    $parent_item_children = DB::table('dream_item_bind')
                        ->join('dream_items', 'dream_items.id', '=', 'dream_item_bind.child_id')
                        ->join('dream_units', 'dream_units.id', '=', 'dream_item_bind.unit_id')
                        ->leftJoin(DB::raw('(select dib.parent_id as parent_id from dream_item_bind as dib group by dib.parent_id) as d_i'), 'd_i.parent_id', '=', 'dream_items.id')
                        ->select('dream_items.*', DB::raw("(group_concat(dream_units.unit separator '/')) as units_name"), DB::raw('group_concat(dream_units.id) as units_id'),
                            DB::raw("(group_concat(dream_item_bind.quantity separator '/')) as quantities"), 'd_i.parent_id',
                            DB::raw("(select group_concat(dream_storages.name) from dream_storage_item_bind
                                    left join dream_storages on dream_storages.id = dream_storage_item_bind.storage_id
                                    where dream_storage_item_bind.item_id = dream_items.id and dream_storages.type_id = 1
                                    ) as storages"), DB::raw("(select dream_units.unit from dream_storage_item_bind
                                    inner join dream_units on dream_units.id = dream_storage_item_bind.unit_id
                                    where dream_storage_item_bind.item_id = dream_items.id group by dream_units.id limit 1
                                    ) as unit")
                        )
                        ->where([['dream_item_bind.parent_id', $parent_item_id]])
                        ->whereNull('dream_items.delete_time')
                        ->groupBy('dream_items.id')->get()->toArray();

//                    var_dump($parent_item_children);
//                    dd();

                    if (is_array($parent_item_children)){
                        foreach ($parent_item_children as $child){
//                            var_dump($child);
//                            dd();
                            $sheet->setCellValueByColumnAndRow(1, $row_num, $item["articula_new"]);
                            $sheet->setCellValueByColumnAndRow(2, $row_num, $item["sap_code"]);
                            $sheet->setCellValueByColumnAndRow(3, $row_num, $item["item_name"]);
                            $sheet->setCellValueByColumnAndRow(4, $row_num, $child->articula_new);
                            $sheet->setCellValueByColumnAndRow(5, $row_num, $child->articula_old);
                            $sheet->setCellValueByColumnAndRow(6, $row_num, $child->sap_code);
                            $sheet->setCellValueByColumnAndRow(7, $row_num, $child->quantities);
                            $sheet->setCellValueByColumnAndRow(8, $row_num, $child->units_name);
                            $sheet->setCellValueByColumnAndRow(9, $row_num, $child->storages);
                            $sheet->setCellValueByColumnAndRow(10, $row_num, $child->unit);

                            $row_num++;
                        }
                    }


                    $row_num +=3;
                }
            }

            $writer = new Xlsx($spreadsheet);

            $writer->save(public_path()."/WMS_detail_norm.xlsx");
            return json_encode(array("url"=>url("/")."/WMS_detail_norm.xlsx"));

        }
        catch (\Exception $e){
            return \response()->json([
                'errors'=>array('ошибка!'),
                "message"=>$e
            ], 500);
        }
    }

    public function exportDetailedItems(Request $request){

        if(file_exists(public_path()."/WMS_buxgalteriya_norm.xlsx")){
            unlink(public_path()."/WMS_buxgalteriya_norm.xlsx");
        }

        function findParentItem($items, $child){
            $parent_item = null;

            foreach ($items as $item){
                if ($child->parent_articula == $item->child_articula){
                    return $item;
                }
            }

            return null;
        }

        function hasChildItem($items, $element){

            foreach ($items as $item){
                if ($item->parent_articula == $element->child_articula){
                    return true;
                }
            }

            return false;
        }

        try {

            $items = $request->items;
            $excel = new Spreadsheet();
            $sheet = $excel->getActiveSheet();
            $sheet->setTitle("Buxgalteriya normi");
            $sheet->setCellValueByColumnAndRow(1, 1, "Артикуль ГП");
            $sheet->setCellValueByColumnAndRow(2, 1, "SAP код ГП");
            $sheet->setCellValueByColumnAndRow(3, 1, "Наименование ГП");


            $row_num = 3;
            $col_num = 5;
            if (is_array($items)){
                foreach ($items as $item){

                    $parent_articulas = array();
                    $detail_articulas = array();

                    $max_level=1;

                    $item_id = $item['id'];
                    $details = DB::select("
                        with RECURSIVE item_consumptions as
                        (
                            select i.id as id, i.articula_new, i.sap_code, i.item_name as item_name, ib.child_id as child_id, ib.quantity as consumption, u.unit as unit, ib.unit_id as unit_id, 1 as lvl from dream_items i
                                inner join dream_item_bind ib on ib.parent_id = i.id
                                inner join dream_units u on u.id = ib.unit_id
                                where  i.id = $item_id
                                union all
                            select ii.id, ii.articula_new, ii.sap_code, ii.item_name, iib.child_id, iib.quantity, uu.unit, uu.id as unit_id, lvl + 1 from dream_items ii
                                inner join dream_item_bind iib on iib.parent_id = ii.id
                                inner join item_consumptions ic on ic.child_id = ii.id
                                inner join dream_units uu on uu.id = iib.unit_id
                        )
                        select ic.id, ic.articula_new as parent_articula, ic.sap_code as parent_sap_code, ic.item_name as parent_name, ic.child_id, i.articula_new as child_articula, i.sap_code as child_sap_code,
                        i.item_name as child_name, consumption, ic.unit, ic.unit_id, lvl from item_consumptions ic
                        inner join dream_items i on i.id = ic.child_id
                        where ic.id in (select parent_id from dream_item_bind)
                    ");


                    foreach ($details as $datail){
                        $parent_articulas[]=$datail->parent_articula;
                        $detail_articulas[]=$datail->child_articula;
                        $max_level = $datail->lvl > $max_level ? $datail->lvl : $max_level;
                    }

                    $col_num = 5;
                    for ($i = 0; $i < $max_level && $i<=5; $i++){
                        $sheet->setCellValueByColumnAndRow($col_num, 1, "Артикуль");
                        $sheet->setCellValueByColumnAndRow($col_num+1, 1, "Наименование");
                        $sheet->setCellValueByColumnAndRow($col_num+2, 1, "кол.");
                        $sheet->setCellValueByColumnAndRow($col_num+3, 1, "ед. изм.");

                        $col_num+=5;
                    }



                    foreach ($details as $detail){
                        if ($detail->lvl == 1 && !hasChildItem($details, $detail)){
                            $sheet->setCellValueByColumnAndRow(1, $row_num, $detail->parent_articula);
                            $sheet->setCellValueByColumnAndRow(2, $row_num, $detail->parent_sap_code);
                            $sheet->setCellValueByColumnAndRow(3, $row_num, $detail->parent_name);

                            $sheet->setCellValueByColumnAndRow(5, $row_num, $detail->child_articula);
                            $sheet->setCellValueByColumnAndRow(6, $row_num, $detail->child_name);
                            $sheet->setCellValueByColumnAndRow(7, $row_num, $detail->consumption);
                            $sheet->setCellValueByColumnAndRow(8, $row_num, $detail->unit);

                            $col_index=10;
                            for ($i=1; $i<$max_level; $i++){
                                $sheet->setCellValueByColumnAndRow($col_index, $row_num, $detail->child_articula);
                                $sheet->setCellValueByColumnAndRow($col_index+1, $row_num, $detail->child_name);
                                $sheet->setCellValueByColumnAndRow($col_index+2, $row_num, $detail->consumption);
                                $sheet->setCellValueByColumnAndRow($col_index+3, $row_num, $detail->unit);
                                $col_index +=5;

                            }

                            $sheet->setCellValueByColumnAndRow($col_index-1, $row_num, "=G".$row_num);

                            $row_num++;
                        }
                        elseif ($detail->lvl ==2 && !hasChildItem($details, $detail)){

                            $parent_item = findParentItem($details, $detail);

                            $sheet->setCellValueByColumnAndRow(1, $row_num, $parent_item->parent_articula);
                            $sheet->setCellValueByColumnAndRow(2, $row_num, $parent_item->parent_sap_code);
                            $sheet->setCellValueByColumnAndRow(3, $row_num, $parent_item->parent_name);

                            $sheet->setCellValueByColumnAndRow(5, $row_num, $parent_item->child_articula);
                            $sheet->setCellValueByColumnAndRow(6, $row_num, $parent_item->child_name);
                            $sheet->setCellValueByColumnAndRow(7, $row_num, $parent_item->consumption);
                            $sheet->setCellValueByColumnAndRow(8, $row_num, $parent_item->unit);


                            $sheet->setCellValueByColumnAndRow(10, $row_num, $detail->child_articula);
                            $sheet->setCellValueByColumnAndRow(11, $row_num, $detail->child_name);
                            $sheet->setCellValueByColumnAndRow(12, $row_num, $detail->consumption);
                            $sheet->setCellValueByColumnAndRow(13, $row_num, $detail->unit);

                            $col_index=15;
                            for ($i=2; $i<$max_level; $i++){
                                $sheet->setCellValueByColumnAndRow($col_index, $row_num, $detail->child_articula);
                                $sheet->setCellValueByColumnAndRow($col_index+1, $row_num, $detail->child_name);
                                $sheet->setCellValueByColumnAndRow($col_index+2, $row_num, $detail->consumption);
                                $sheet->setCellValueByColumnAndRow($col_index+3, $row_num, $detail->unit);
                                $col_index +=5;

                            }
                            $sheet->setCellValueByColumnAndRow($col_index-1, $row_num, "=G".$row_num."*L".$row_num);

                            $row_num++;
                        }

                        elseif ($detail->lvl == 3 && !hasChildItem($details, $detail)){

                            $middle_item = findParentItem($details, $detail);
                            $parent_item = findParentItem($details, $middle_item);

                            $sheet->setCellValueByColumnAndRow(1, $row_num, $parent_item->parent_articula);
                            $sheet->setCellValueByColumnAndRow(2, $row_num, $parent_item->parent_sap_code);
                            $sheet->setCellValueByColumnAndRow(3, $row_num, $parent_item->parent_name);

                            $sheet->setCellValueByColumnAndRow(5, $row_num, $parent_item->child_articula);
                            $sheet->setCellValueByColumnAndRow(6, $row_num, $parent_item->child_name);
                            $sheet->setCellValueByColumnAndRow(7, $row_num, $parent_item->consumption);
                            $sheet->setCellValueByColumnAndRow(8, $row_num, $parent_item->unit);


                            $sheet->setCellValueByColumnAndRow(10, $row_num, $middle_item->child_articula);
                            $sheet->setCellValueByColumnAndRow(11, $row_num, $middle_item->child_name);
                            $sheet->setCellValueByColumnAndRow(12, $row_num, $middle_item->consumption);
                            $sheet->setCellValueByColumnAndRow(13, $row_num, $middle_item->unit);

                            $sheet->setCellValueByColumnAndRow(15, $row_num, $detail->child_articula);
                            $sheet->setCellValueByColumnAndRow(16, $row_num, $detail->child_name);
                            $sheet->setCellValueByColumnAndRow(17, $row_num, $detail->consumption);
                            $sheet->setCellValueByColumnAndRow(18, $row_num, $detail->unit);

                            $col_index=20;
                            for ($i=3; $i<$max_level; $i++){
                                $sheet->setCellValueByColumnAndRow($col_index, $row_num, $detail->child_articula);
                                $sheet->setCellValueByColumnAndRow($col_index+1, $row_num, $detail->child_name);
                                $sheet->setCellValueByColumnAndRow($col_index+2, $row_num, $detail->consumption);
                                $sheet->setCellValueByColumnAndRow($col_index+3, $row_num, $detail->unit);
                                $col_index +=5;

                            }
                            $sheet->setCellValueByColumnAndRow($col_index-1, $row_num, "=G".$row_num."*L".$row_num."*Q".$row_num);
                            $row_num++;

                        }

                        elseif ($detail->lvl == 4 && !hasChildItem($details, $detail)){
                            $post_middle_item = findparentItem($details, $detail);
                            $middle_item = findParentItem($details, $post_middle_item);
                            $parent_item = findParentItem($details, $middle_item);

                            $sheet->setCellValueByColumnAndRow(1, $row_num, $parent_item->parent_articula);
                            $sheet->setCellValueByColumnAndRow(2, $row_num, $parent_item->parent_sap_code);
                            $sheet->setCellValueByColumnAndRow(3, $row_num, $parent_item->parent_name);

                            $sheet->setCellValueByColumnAndRow(5, $row_num, $parent_item->child_articula);
                            $sheet->setCellValueByColumnAndRow(6, $row_num, $parent_item->child_name);
                            $sheet->setCellValueByColumnAndRow(7, $row_num, $parent_item->consumption);
                            $sheet->setCellValueByColumnAndRow(8, $row_num, $parent_item->unit);


                            $sheet->setCellValueByColumnAndRow(10, $row_num, $middle_item->child_articula);
                            $sheet->setCellValueByColumnAndRow(11, $row_num, $middle_item->child_name);
                            $sheet->setCellValueByColumnAndRow(12, $row_num, $middle_item->consumption);
                            $sheet->setCellValueByColumnAndRow(13, $row_num, $middle_item->unit);

                            $sheet->setCellValueByColumnAndRow(15, $row_num, $post_middle_item->child_articula);
                            $sheet->setCellValueByColumnAndRow(16, $row_num, $post_middle_item->child_name);
                            $sheet->setCellValueByColumnAndRow(17, $row_num, $post_middle_item->consumption);
                            $sheet->setCellValueByColumnAndRow(18, $row_num, $post_middle_item->unit);

                            $sheet->setCellValueByColumnAndRow(20, $row_num, $detail->child_articula);
                            $sheet->setCellValueByColumnAndRow(21, $row_num, $detail->child_name);
                            $sheet->setCellValueByColumnAndRow(22, $row_num, $detail->consumption);
                            $sheet->setCellValueByColumnAndRow(23, $row_num, $detail->unit);
                            $col_index=25;
                            for ($i=4; $i<$max_level; $i++){
                                $sheet->setCellValueByColumnAndRow($col_index, $row_num, $detail->child_articula);
                                $sheet->setCellValueByColumnAndRow($col_index+1, $row_num, $detail->child_name);
                                $sheet->setCellValueByColumnAndRow($col_index+2, $row_num, $detail->consumption);
                                $sheet->setCellValueByColumnAndRow($col_index+3, $row_num, $detail->unit);
                                $col_index +=5;

                            }
                            $sheet->setCellValueByColumnAndRow($col_index-1, $row_num, "=G".$row_num."*L".$row_num."*Q".$row_num."*V".$row_num);
                            $row_num++;
                        }
                        elseif ($detail->lvl == 5 && !hasChilditem($details, $detail)){
                            $pre_detail_item = findParentItem($details, $detail);
                            $post_middle_item = findParentItem($details, $pre_detail_item);
                            $middle_item = findParentItem($details, $post_middle_item);
                            $parent_item = findParentItem($details, $middle_item);

                            $sheet->setCellValueByColumnAndRow(1, $row_num, $parent_item->parent_articula);
                            $sheet->setCellValueByColumnAndRow(2, $row_num, $parent_item->parent_sap_code);
                            $sheet->setCellValueByColumnAndRow(3, $row_num, $parent_item->parent_name);

                            $sheet->setCellValueByColumnAndRow(5, $row_num, $parent_item->child_articula);
                            $sheet->setCellValueByColumnAndRow(6, $row_num, $parent_item->child_name);
                            $sheet->setCellValueByColumnAndRow(7, $row_num, $parent_item->consumption);
                            $sheet->setCellValueByColumnAndRow(8, $row_num, $parent_item->unit);


                            $sheet->setCellValueByColumnAndRow(10, $row_num, $middle_item->child_articula);
                            $sheet->setCellValueByColumnAndRow(11, $row_num, $middle_item->child_name);
                            $sheet->setCellValueByColumnAndRow(12, $row_num, $middle_item->consumption);
                            $sheet->setCellValueByColumnAndRow(13, $row_num, $middle_item->unit);

                            $sheet->setCellValueByColumnAndRow(15, $row_num, $post_middle_item->child_articula);
                            $sheet->setCellValueByColumnAndRow(16, $row_num, $post_middle_item->child_name);
                            $sheet->setCellValueByColumnAndRow(17, $row_num, $post_middle_item->consumption);
                            $sheet->setCellValueByColumnAndRow(18, $row_num, $post_middle_item->unit);

                            $sheet->setCellValueByColumnAndRow(20, $row_num, $pre_detail_item->child_articula);
                            $sheet->setCellValueByColumnAndRow(21, $row_num, $pre_detail_item->child_name);
                            $sheet->setCellValueByColumnAndRow(22, $row_num, $pre_detail_item->consumption);
                            $sheet->setCellValueByColumnAndRow(23, $row_num, $pre_detail_item->unit);

                            $sheet->setCellValueByColumnAndRow(25, $row_num, $detail->child_articula);
                            $sheet->setCellValueByColumnAndRow(26, $row_num, $detail->child_name);
                            $sheet->setCellValueByColumnAndRow(27, $row_num, $detail->consumption);
                            $sheet->setCellValueByColumnAndRow(28, $row_num, $detail->unit);

                            $sheet->setCellValueByColumnAndRow($col_index-1, $row_num, "=G".$row_num."*L".$row_num."*Q".$row_num."*V".$row_num."*AA".$row_num);

                            $row_num++;
                        }



                    }


                    $row_num+=3;
                }
            }

            $writer = new Xlsx($excel);

            $writer->save(public_path()."/WMS_buxgalteriya_norm.xlsx");
            return json_encode(array("url"=>url("/")."/WMS_buxgalteriya_norm.xlsx"));


        }catch (\Exception $e){
            return \response()->json([
                'errors'=>array('ошибка!'),
                "message"=>$e
                ], 500);
        }

    }

    public function set_required(Request $request){
        session()->flush();
        $arr = $request->checkbox;
        $c = [];
        if(empty($arr)){
            return redirect()->back();
        }
        for($i=0; $i<sizeof($arr); $i++){
            array_push($c, array_keys($arr)[$i]);

            $value = array_values($arr)[$i];
            $key = array_keys($arr)[$i];
            session()->put($key, $value);

        }
        session()->put('check', $c);
        return redirect()->back();
    }

//    correct
    public function createItem(Request $request){

        if (Gate::denies('save', new DreamItem)){
            return \response()->json(['errors'=>array('нет доступа!')], 500);
        }

        $data = $request->except('_token');
        $request->validate([
            'articula_new' => 'required|unique:dream_items',
            'item_name' => 'required',
            'units' => 'required'
        ]);

        try{
            $id = DreamItem::create($data)->id;
            for ($i=0; $i<sizeof($data['units']); $i++){
                $unit = [
                    'unit_id'=>$data['units'][$i],
                    'item_id'=>$id
                ];
                DB::table('dream_item_unit_bind')->insert($unit);
            }

            if($request->hasFile('item_file'))
            {
                $root=public_path()."/assets/itemsPhoto/";
                $f_name=$request->articula_new.'.png';
                $request->file('item_file')->move($root,$f_name);
            }

            $response_data = [
                'id' => $id,
                'articula_new' => $request->articula_new,
                'articula_old' => $request->articula_old,
                'sap_code' => $request->sap_code,
                'item_name' => $request->item_name,
            ];
        }catch (\Exception $e){
            return \response()->json(['errors'=>array('ошибка!')], 500);
        }
        return json_encode($response_data);
    }

    public function destroyItem(Request $request){
        if (Gate::denies('delete', new DreamItem)){
            return \response()->json(['errors'=>array('нет доступа!')], 500);
        }
        $item_id = $request->item_id;
        $item = DreamItem::find($item_id);
        try{
            if (!is_null($item)){
                if (DB::table("dream_item_bind")->where("child_id", $item_id)->count()!=0){
                    DB::table("dream_item_bind")->where("child_id", $item_id)->delete();
                }

                if(DB::table("dream_item_bind")->where("parent_id", $item_id)->count()!=0){
                    DB::table("dream_item_bind")->where("parent_id", $item_id)->delete();
                }

                $item->delete();
            }else{
                return \response()->json(['errors'=>array('произошла ошибка!')], 500);
            }
        }catch (\Exception $e){
            return \response()->json(['errors'=>array('удалить невозможно, использовано в транзакции!')], 500);
        }
        return \response()->json(array('success' => true, 'done' =>'удален!'), 200);
    }

    public function getItemValues(Request $request){
        $item_id = $request->item_id;
        if (Gate::denies('save', new DreamItem)){
            $hasPerm = false;
        }else{
            $hasPerm = true;
        }
        $item = DB::table('dream_items')
            ->join('dream_item_unit_bind', 'dream_item_unit_bind.item_id', '=', 'dream_items.id')
            ->join('dream_units', 'dream_units.id', '=', 'dream_item_unit_bind.unit_id')
            ->select('dream_items.*', DB::raw('group_concat(dream_units.unit) as units'))->where('dream_items.id', $item_id)
            ->groupBy('dream_items.id')->first();
        $item_units = DB::table('dream_units')
            ->join('dream_item_unit_bind', 'dream_item_unit_bind.unit_id', '=', 'dream_units.id')
            ->select('dream_units.id')
            ->where('dream_item_unit_bind.item_id', $item_id)->get();
        $units = array();
        foreach ($item_units as $unit){
            $units[] = $unit->id;
        }
        $all_units = DreamUnit::all()->toArray();
        if (file_exists(public_path().'/assets/itemsPhoto/'.$item->articula_new.'.png')){
            $path = asset('assets').'/itemsPhoto/'.$item->articula_new.'.png';
        }else{
            $path = null;
        }
        $data = [
            'data' => $item,
            'hasPerm' => $hasPerm,
            'units' => $units,
            'all_units' => $all_units,
            'path' => $path
        ];
        return json_encode($data);
    }

    public function updateItem(Request $request){
        if (Gate::denies('save', new DreamItem)){
            return \response()->json(['errors'=>array('нет доступа!')], 500);
        }
        $item_id = $request->itemIdOfShownModal;
        $first_articula_new = DreamItem::find($item_id)->articula_new;
        $root = '';
        try{
            $data = [
                'articula_new' => $request->edit_articula_new,
                'articula_old' => $request->edit_articula_old,
                'item_name' => $request->edit_item_name,
                'sap_code' => $request->edit_sap_code,
            ];
            DB::table('dream_items')->where('id', $item_id)->update($data);
            if (!empty($request->units)){
                foreach ($request->units as $unit_id){
                    $isExistUnit = DB::table('dream_item_unit_bind')->where([['item_id', $item_id], ['unit_id', $unit_id]])->get();
                    if (empty($isExistUnit->first())){
                        DB::table('dream_item_unit_bind')->insert(['item_id'=>$item_id, 'unit_id'=>$unit_id]);
                    }
                }
            }
            if($request->hasFile('item_photo_upload'))
            {
                $root=public_path("/assets/itemsPhoto/");
                if (file_exists($root.$first_articula_new.'.png')){
                    unlink($root.$first_articula_new.'.png');
                }
                $f_name=$request->edit_articula_new.'.png';
                $request->file('item_photo_upload')->move($root,$f_name);
            }

            return json_encode(asset('assets/itemsPhoto').'/'.$first_articula_new.'.png');
        }catch (\Exception $e){
            return \response()->json(['errors'=>array('произошла ошибка!', $e)], 500);
        }
    }

    public function deleteItemPhoto(Request $request){
        $item_id = $request->item_id;
        try{
            $articula_new = DreamItem::find($item_id)->articula_new;
            $root=public_path("/assets/itemsPhoto/");
            if (file_exists($root.$articula_new.'.png')){
                unlink($root.$articula_new.'.png');
            }
            return json_encode($root);
        }catch (\Exception $exception){
            return \response()->json(['errors'=>array('произошла ошибка!', $exception)], 500);
        }
    }

//    norms
    public function getItemNormItems(Request $request){
        $data = array();
        $parent_item_id = $request->item_id;
        $hasPerm = true;
        if (Gate::denies('save', new DreamItem)){
            $hasPerm = false;
        }
        $parentItem = DreamItem::find($parent_item_id);
        $parent_item_children = DB::table('dream_item_bind')
            ->join('dream_items', 'dream_items.id', '=', 'dream_item_bind.child_id')
            ->join('dream_units', 'dream_units.id', '=', 'dream_item_bind.unit_id')
            ->leftJoin(DB::raw('(select dib.parent_id as parent_id from dream_item_bind as dib group by dib.parent_id) as d_i'), 'd_i.parent_id', '=', 'dream_items.id')
            ->select('dream_items.*', DB::raw("(group_concat(dream_units.unit separator '/')) as units_name"), DB::raw('group_concat(dream_units.id) as units_id'),
                DB::raw("(group_concat(dream_item_bind.quantity separator '/')) as quantities"), 'd_i.parent_id',
                DB::raw("(select group_concat(dream_storages.name) from dream_storage_item_bind
                left join dream_storages on dream_storages.id = dream_storage_item_bind.storage_id
                where dream_storage_item_bind.item_id = dream_items.id and dream_storages.type_id = 1
                ) as storages"), DB::raw("(select dream_units.unit from dream_storage_item_bind
                inner join dream_units on dream_units.id = dream_storage_item_bind.unit_id
                where dream_storage_item_bind.item_id = dream_items.id group by dream_units.id limit 1
                ) as unit")
                )
            ->where([['dream_item_bind.parent_id', $parent_item_id]])
            ->whereNull('dream_items.delete_time')
            ->groupBy('dream_items.id')->get();
        if (!empty($parent_item_children)){
            foreach ($parent_item_children as $item)
            {
                if (!is_null($item->parent_id)){
                    $hasNorm = '* ';
                }else{
                    $hasNorm = '';
                }
                $nestedData['DT_RowId'] = $item->id;
                $nestedData['id'] = $item->id;
                $nestedData['articula_new'] = $item->articula_new;
                $nestedData['articula_old'] = $item->articula_old;
                $nestedData['sap_code'] = $item->sap_code;
                $nestedData['item_name'] = $item->item_name;
                $nestedData['quantities'] = $item->quantities;
                $nestedData['storages'] = $item->storages;
                $nestedData['unit'] = $item->unit;
                if (Gate::denies('save', new DreamItem)){
//                    $nestedData['buttons'] = "<button type='button' onclick=\"show_detail('".$item->id."')\" class='btn btn-info'><span class='fa fa-gear'></span></button>";
                    $nestedData['buttons'] = "";
                }else{
                    $nestedData['buttons'] = "<button  class='btn btn-warning' data-toggle='modal' data-backdrop='static' data-keyboard='false' data-target='#editNormItemModal' onclick=\"edit_item_norm_modal('".$item->id."')\"><i class='fa fa-edit'></i></button> " .
                        "<button type='button' onclick=\"remove_norm_item('".$item->id."')\" class='btn btn-danger'><i class='fa fa-trash'></i></button> ".
                        "<button type='button' onclick=\"setReplaceItemId('".$item->id."')\" class='btn btn-info' data-toggle='modal' data-backdrop='static' data-keyboard='false' data-target='#replaceNormItemModal'><i class='fa fa-refresh'></i></button>";
                }
                $nestedData['infoButton'] = "";
                $nestedData['units_name'] = $item->units_name;
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "data" => $data,
            "hasPerm" => $hasPerm,
            "parentItem" => $parentItem,
        );

        echo json_encode($json_data);
    }

    public function get_items_for_adding_to_norm(Request $request){
        $items = DreamItem::where('id', '!=', $request->item_id)->get();
        return json_encode($items);
    }

    public function get_item_units(Request $request){
        $item_id = $request->item_id;
        $units = DB::table('dream_item_unit_bind')
            ->join('dream_units', 'dream_units.id', '=', 'dream_item_unit_bind.unit_id')->where('dream_item_unit_bind.item_id', $item_id)->get();
        return json_encode($units);
    }

    public function add_item_to_norm(Request $request){
        if (Gate::denies('add_item_to_norm', new DreamItem)){
            return \response()->json(['errors'=>array('нет доступа!')], 500);
        }
        $request->validate([
            'unit' => 'required',
            'item_id' => 'required',
            'quantity' => 'required'
        ]);
        $c = DB::table('dream_item_bind')->where([['parent_id', $request->parent_id], ['child_id', $request->item_id], ['unit_id', $request->unit]])->count();
        if ($c!=0){
            return response()->json(['errors'=>array('уже существует!')], 404);
        }

        try{
            $last_inserted_id = DB::transaction(function () use ($request){
                $dreamItem = new DreamItem();
                $id = $request->parent_id;
                if (!$dreamItem->checkForCycle($id, $request->item)){
                    $last_inserted_id = DB::table("dream_item_bind")
                        ->insertGetId(['parent_id'=>$request->parent_id, 'child_id'=>$request->item_id, 'quantity'=>$request->quantity, 'unit_id'=>$request->unit]);
                }
                else{
                    return \response()->json(['errora'=>array('невозможно добавить!')], 500);
                }
                return $last_inserted_id;
            });
            return \response()->json(array('success' => true, 'last_id' => $last_inserted_id), 200);

        }catch (\Exception $e){
            return \response()->json(['errors'=>array($e, 'невозможно добавить!')], 500);
        }
    }

    public function add_parent_item_unit(){
        if (Gate::denies('save', new DreamItem)){
            return \response()->json(['errors'=>array('нет доступа!')], 500);
        }
        $inputs = Input::all();
        $item_unit_bind = DB::table('dream_item_unit_bind')->where('item_id', $inputs['parent_id'])->get();
        foreach ($inputs['units'] as $key=>$unit){
            if ($this->isNotExist($item_unit_bind, $unit)){
                $data = [
                    'item_id'=>$inputs['parent_id'],
                    'unit_id'=>$unit
                ];
                DB::table('dream_item_unit_bind')->insert($data);
            }
        }
        $id=$inputs['parent_id'];
        $item = DB::select("select dream_items.*, dream_items.id as item_id, dream_units.id as unit_id, dream_units.unit as unit from dream_item_unit_bind ".
            "inner join dream_items on dream_items.id=dream_item_unit_bind.item_id inner join dream_units on dream_item_unit_bind.unit_id=dream_units.id where dream_items.id=$id");
        return response()->json(['data'=>$item]);
    }

    public function isNotExist($units, $unit_id){
        foreach ($units as $unit) {
            if ($unit_id==$unit->unit_id){
                return false;
            }
        }
        return true;
    }

    public function get_item_child_values(Request $request){
        $parent_id = $request->parent_id;
        $child_id = $request->child_id;
        $item = DB::select("select dream_item_bind.*, dream_units.unit as unit from dream_item_bind inner join dream_units on dream_item_bind.unit_id=dream_units.id ".
        "where parent_id=$parent_id and child_id=$child_id");
        $rep_item = DB::table('dream_item_bind')
            ->join('dream_replacement_items', 'dream_replacement_items.item_bind_id', '=', 'dream_item_bind.id')
            ->join('dream_items', 'dream_items.id', '=', 'dream_replacement_items.item_id')
            ->where([['dream_item_bind.parent_id', $parent_id], ['dream_item_bind.child_id', $child_id]])->select('dream_items.id')->first();
        $data = [
            'item'=>$item,
            'rep_item'=>$rep_item
        ];
        return json_encode($data);
    }

    public function updateChild(Request $request){
        if (Gate::denies('add_item_to_norm', new DreamItem)){
            return \response()->json(['errors'=>array('нет доступа!')], 500);
        }
        try{
            $parent_id = $request->parent_id;
            $child_id = $request->child_id;
            $quantities = $request->quantities;
            $item_children = DB::table('dream_item_bind')->where([['parent_id', $parent_id], ['child_id', $child_id]])->get();
            foreach ($item_children as $key=>$child){
                $data = [
                    'quantity' => $quantities[$key]
                ];
                DB::table('dream_item_bind')->where([['parent_id', $parent_id], ['child_id', $child_id], ['id', $child->id]])->update($data);
            }
            return \response()->json(array('success' => true, 'data' => $request), 200);
        }catch (\Exception $exception){
            return \response()->json(['errors'=>array('ошибка', $exception)], 500);
        }
    }

    public function destroyChildItem(){
        if (Gate::denies('add_item_to_norm', new DreamItem)){
            return \response()->json(['errors'=>array('нет доступа!')], 500);
        }
        $inputs = Input::all();
        $parent_id = $inputs['parent_item_id'];
        $child_id = $inputs['child_item_id'];
        DB::table('dream_item_bind')->where([['parent_id', $parent_id],['child_id', $child_id]])->delete();
        return json_encode($inputs);
    }

    public function show_remaining(){
        $storages = DreamStorage::all();
        $models = DB::table('dream_items')
            ->whereIn('dream_items.id', array(DB::raw('(select dream_storage_item_bind.item_id from dream_storage_item_bind ' .
                'where dream_storage_item_bind.storage_id=3 or dream_storage_item_bind.storage_id=23 or dream_storage_item_bind.storage_id=18 or dream_storage_item_bind.storage_id=26)')))
            ->select('dream_items.*')->get();
        $suppliers = DreamStorage::where([['state', self::STATE_ACTIVE], ['type_id', 7]])->get();
        return view('index', ['page'=>'show_remaining', 'storages'=>$storages, 'models'=>$models, 'suppliers'=>$suppliers]);
    }

    public function getLastInventoryDate($storage_id, $date){
        $it = DB::table('dream_transaction_storage_bind')
            ->join('dream_transactions', 'dream_transactions.id', '=', 'dream_transaction_storage_bind.transaction_id')
            ->select('dream_transactions.*')
            ->where([['dream_transactions.is_inventory', 1], ['dream_transactions.create_time', '<=', $date.' 23:59:99'], ['dream_transaction_storage_bind.storage_id', $storage_id]])
            ->orderBy('dream_transactions.create_time', 'desc')->limit(1)->first();
        if (empty($it)){
            return null;
        }
        return $it->id;
    }

    public function get_item_remaining($storage_id, $item_id)
    {
        $total = DB::select("select sum(operation*amount) as total from dream_transaction_list ".
            "left join dream_transaction_storage_bind on dream_transaction_list.transaction_id=dream_transaction_storage_bind.transaction_id and dream_transaction_storage_bind.storage_id=$storage_id " .
            "left join dream_transaction_types on dream_transaction_types.id=dream_transaction_storage_bind.operation_id where item_id=$item_id  and dream_transaction_list.delete_time is null")[0];
        if ($total->total == null){
            $total->total = 0;
        }
        return round($total->total, 4);
    }

    public function add_replacement_items(){
        if (Gate::denies('save', new DreamItem)){
            return \response()->json(['errors'=>array('нет доступа!')], 500);
        }
        $inputs = Input::all();
        $s_i = DB::table('dream_storage_item_bind')->where('item_id', $inputs['selected_item'])->first();
        if (is_null($s_i)){
            return \response()->json(['errors'=>array('невозможно добавить, выбранный деталь не привязан к складу')], 500);
        }

        try{
            $bind_id = DB::table('dream_item_bind')->select('id')->where([['parent_id', $inputs['parent_id']], ['child_id', $inputs['child_id'], ['unit_id', $s_i->unit_id]]])->first();
            if (!is_null($bind_id)){
                if ($inputs['selected_item'] == 0){
                    $r_items = DB::table('dream_replacement_items')->where('id', $bind_id)->get();
                    if (!is_null($r_items)){
                        DB::table('dream_replacement_items')->where('id', $bind_id)->delete();
                    }
                }else{
                    $data = [
                        'item_bind_id' => $bind_id->id,
                        'item_id' => $inputs['selected_item']
                    ];
                    DB::table('dream_replacement_items')->insertGetId($data);
                }
            }else{
                return \response()->json(['errors'=>array('невозможно добавить, единица измерения не совпадаеть')], 500);
            }
            return json_encode($inputs);
        }catch (\Exception $exception){
            return \response()->json(['errors'=>array('невозможно добавить')], 500);
        }
    }

    public function delete_replacement_items(){
        if (Gate::denies('delete', new DreamItem)){
            return \response()->json(['errors'=>array('нет доступа!')], 500);
        }
        $inputs = Input::all();
        try{
            DB::table('dream_replacement_items')->delete($inputs['id']);
        }catch (\Exception $exception){
            return \response()->json(['errors'=>array('не удален')], 500);
        }
        return \response()->json(array('success' => true, 'data' => 'удален'), 200);
    }

    public function filter_items_by_checkbox(){
        $inputs = Input::all();
        $select_query = "select *from dream_items ";

        if (isset($inputs['checkbox_data'])) {
            $my_array = $this->merge_to_array($inputs['checkbox_data']);

            $atr = [
                0 => [
                    'size' => 1,
                    'index' => 1
                ],
                1 => [
                    'size' => 1,
                    'index' => 2
                ],
                2 => [
                    'size' => 1,
                    'index' => 3
                ],
                3 => [
                    'size' => 2,
                    'index' => 4
                ],
                4 => [
                    'size' => 2,
                    'index' => 6
                ]
            ];


            $select_query.="where ";
            foreach ($my_array as $key=>$checkbox_datum){
                $values = "(";
                foreach ($checkbox_datum as $value){
                    $values.="'".$value."',";
                }
                $values = substr($values, 0, strlen($values)-1).")";
                $select_query.="substring(articula_new, ".$atr[$key]['index'].", ".$atr[$key]['size']." ) in $values and ";
            }
            $select_query = substr($select_query, 0, strlen($select_query)-5);
        }
        $filtered_items = DB::select($select_query);
        return redirect()->back()->with('filtered_items', $filtered_items);
    }

    public function merge_to_array($array){
        $offerArray = array();
        foreach ($array as $arr){
            $key = explode('_', $arr)[0];
            $value = explode('_', $arr)[1];
            $offerArray[$key][] = $value;
        }
        return $offerArray;
    }

    public function generate_articula(Request $request){
        $prefix = $this->conver_to_str($request->cat_num);
        $numeric_prefix = (int)$prefix * 100000;
        $free_articula = null;

        for ($serial_number=0; $serial_number<100000; $serial_number++){
            $numeric_articula = $numeric_prefix+$serial_number+1;
            if (!$this->is_existing_articula($numeric_articula.'')){
                $free_articula = $numeric_articula;
                break;
            }
        }

        return redirect()->back()->with('articula_new', $free_articula);
    }

    public function is_existing_articula($articula){
        $is_empty = DB::table('dream_items')->where('articula_new', $articula)->count();
        if ($is_empty){
            return true;
        }else{
            return false;
        }
    }

    public function conver_to_str($arr){
        $str = '';
        foreach ($arr as $value){
            $str.=$value;
        }
        return $str;
    }

    public function delete_norm(){
        if (Gate::denies('delete', new DreamItem)){
            return \response()->json(['errors'=>array('нет доступа!')], 500);
        }
        $item_id = Input::all()['item_id'];
        DB::table('dream_item_bind')->where('parent_id', $item_id)->delete();
        return json_encode(array('item_id'=>$item_id));
    }

    public function searchModelByDetail(Request $request){
        $models = DB::table('dream_items')
            ->join('dream_item_bind', 'dream_item_bind.parent_id', '=', 'dream_items.id')
            //->join(DB::raw("(select unit_id from dream_storage_item_bind where item_id=$request->item_id group by unit_id) as si"), 'si.unit_id', '=', 'dream_item_bind.unit_id')
            ->join('dream_units', 'dream_units.id', '=', 'dream_item_bind.unit_id')
            ->where('dream_item_bind.child_id', '=', $request->item_id)
            ->select('dream_items.*', 'dream_item_bind.quantity', 'dream_units.unit')
            ->groupBy('dream_items.id', 'dream_item_bind.quantity', 'dream_units.id')->get()->toArray();
        $data = [
            "data" => $models
        ];
        return json_encode($data);
    }

    public function getItemSuppliers(Request $request){
        $item_id = $request->item_id;
        $suppliers = DB::table('dream_storages')
            ->join('dream_storage_item_bind', 'dream_storage_item_bind.storage_id', '=', 'dream_storages.id')
            ->leftJoin('storage_info', 'storage_info.storage_id', '=', 'dream_storages.id')
            ->where('dream_storage_item_bind.item_id', '=', $item_id)
            ->select('dream_storages.name', 'storage_info.address', 'storage_info.phone', DB::raw('IFNULL(storage_info.website, " ") as website'), 'storage_info.country')
            ->where('dream_storages.type_id', 7)
            ->get()->toArray();
        $data = [
            'data' => $suppliers
        ];
        return json_encode($data);
    }

    public function getReplacementItems(Request $request){
        $item_id = $request->item_id;
        $child_id = $request->child_id;
        $items = DB::table('dream_replacement_items')
            ->join('dream_item_bind', 'dream_item_bind.id', '=', 'dream_replacement_items.item_bind_id')
            ->join('dream_items', 'dream_items.id', '=', 'dream_replacement_items.item_id')
            ->where([['dream_item_bind.child_id', $child_id], ['dream_item_bind.parent_id', $item_id]])
            ->select('dream_items.id')->groupBy('dream_items.id')->get()->toArray();
        $r = array();
        foreach ($items as $item){
            $r[] = $item->id;
        }
        $data = [
            'replace_items'=>$r
        ];
        return json_encode($data);
    }

    public function show_remaining_by_storage(Request $request){
        $columns = array(
            0 => 'articula_new',
            1 => 'sap_code',
            2 => 'item_name',
            3 => 'last_remaining',
            4 => 'remaining',
            5 => 'unit',
            6 => 'storage_name',
            7 => 'invoices_numbers'
        );

        $filterByModel = null;
        $filterBySupplier = null;
        $total = 0;
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $session_data = [
            'storage_id'=>$request->storage_id,
            'date'=>$request->date
        ];
        session()->put('remainingTable', $session_data);

        $totalData = DB::table('dream_transaction_list')
            ->join('dream_transaction_storage_bind', 'dream_transaction_storage_bind.transaction_id', '=', 'dream_transaction_list.transaction_id')->count();
        if ($request->input('length')==-1){
            $limit = $totalData;
        }else{
            $limit = $request->input('length');
        }
        $filterText = '';
        $model_arr = '';
        if (!is_null($request->models_id)){
            foreach ($request->models_id as $model_id){
                $model_arr .= $model_id.',';
            }
            $model_arr = substr($model_arr, 0, strlen($model_arr)-1);
            $filterText .= " and i.id in (select child_id from dream_item_bind where parent_id in ($model_arr)) ";
        }

        $supplier_arr = '';
        if (!is_null($request->suppliers_id)){
            foreach ($request->suppliers_id as $supplier_id){
                $supplier_arr .= $supplier_id.',';
            }
            $supplier_arr = substr($supplier_arr, 0, strlen($supplier_arr)-1);
            $filterText .= " and i.id in (select item_id from dream_storage_item_bind where storage_id in ($supplier_arr)) ";
        }

        $storage_arr = '';
        if (!is_null($request->storages)){
            foreach ($request->storages as $storage_id){
                $storage_arr .= $storage_id.',';
            }
            $storage_arr = substr($storage_arr, 0, strlen($storage_arr)-1);
            $filterText .= " and s.id in ($storage_arr) ";
        }

        if ($request->type_id>0){
            $filterText .= " and substring(articula_new, 3, 1 ) in (".$request->type_id.")";
        }
        $query_text = "
            select SQL_CALC_FOUND_ROWS  i.id, i.item_name, i.articula_new, i.sap_code, s.id as storage_id, s.name as storage_name, u.unit,
            sum(tl.amount * tt.operation) as remaining,
            sum(COALESCE(tl1.amount, 0) * tt.operation) as last_remaining

            from dream_items i
            inner join dream_transaction_list tl on i.id = tl.item_id
            inner join dream_transactions t on tl.transaction_id = t.id
            inner join dream_transaction_storage_bind tsb on t.id = tsb.transaction_id
            inner join dream_transaction_types tt on tsb.operation_id = tt.id
            inner join dream_storages s on tsb.storage_id = s.id
            inner join dream_units u on tl.unit_id = u.id
            left join
            (
            select tsb1.storage_id as storage_id, max(t1.id) as transaction_id
            from dream_transactions t1
            inner join dream_transaction_storage_bind tsb1 on t1.id = tsb1.transaction_id
            where t1.is_inventory = 1 and date(t1.create_time) <= date('$request->date')
            group by tsb1.storage_id
            ) last_inv_by_storage on s.id = last_inv_by_storage.storage_id

            left join dream_transaction_list tl1 on tl.id = tl1.id and tl1.transaction_id <= last_inv_by_storage.transaction_id

            where tl.delete_time is null and date(t.create_time) <= date('$request->date')
        ";

        $search_text = '';
        if (!empty($request->input('search.value'))){
            $search = $request->input('search.value');
            $search_text .= "
                     and ( i.articula_new like '%$search%' || i.articula_old like '%$search%' || s.name like '%$search%' || i.sap_code like '%$search%' || i.item_name like '%$search%' )
                    ";
        }

        $query_text.=$search_text.$filterText." group by i.id, u.id, s.id order by $order $dir limit $limit offset $start;";
        $all_items_remaining = DB::select($query_text);
        $totalCount = DB::select('select found_rows() as total_count')[0];
        $data = array();
        if (!empty($all_items_remaining)){
            foreach ($all_items_remaining as $item)
            {
                $total += round($item->remaining, 4);
                $filename = public_path().'/assets/itemsPhoto/'.$item->articula_new.'.png';
                if (file_exists($filename)) {
                    $nestedData['articula_new'] = "<p data-id='$item->articula_new' class='showImageLink' style='color: grey;' onmouseover=\"showImage(event, this)\" onmouseleave=\"hideImage(event)\">$item->articula_new</p>";
                } else {
                    $nestedData['articula_new'] = $item->articula_new;
                }
                $nestedData['sap_code'] = $item->sap_code;
                $nestedData['item_name'] = $item->item_name;
                $nestedData['remaining'] = round($item->remaining, 4);
                $nestedData['last_remaining'] = round($item->last_remaining, 4);
                $nestedData['unit'] = $item->unit;
                $nestedData['storage_name'] = $item->storage_name;
//                $nestedData['invoices_numbers'] = (isset($item->invoices_numbers) && !is_null($item->invoices_numbers))?$item->invoices_numbers:'';
                $data[] = $nestedData;
            }
        }else{
            $total = 0;
            $nestedData['articula_new'] = null;
            $nestedData['sap_code'] = null;
            $nestedData['item_name'] = null;
            $nestedData['remaining'] = null;
            $nestedData['last_remaining'] = null;
            $nestedData['remaining_on_the_way'] = null;
            $nestedData['unit'] = null;
            $nestedData['storage_name'] = null;
//            $nestedData['invoices_numbers'] = null;
            $data[] = $nestedData;
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalCount->total_count),
            "data"            => $data,
            "total"           => $total
        );
        return response()->json($json_data);
    }

    public function getRawOfModel(Request $request){
        $item_id = $request->item_id;
        $items = DB::select("
            with RECURSIVE item_consumptions as
            (
                select i.id as id, i.articula_new, i.sap_code, i.item_name as item_name, ib.child_id as child_id, ib.quantity as consumption, u.unit as unit, ib.unit_id as unit_id, 1 as lvl from dream_items i
                    inner join dream_item_bind ib on ib.parent_id = i.id
                    inner join dream_units u on u.id = ib.unit_id
                    where  i.id = $item_id
                    union all
                select ii.id, ii.articula_new, ii.sap_code, ii.item_name, iib.child_id, consumption * iib.quantity, uu.unit, uu.id as unit_id, lvl + 1 from dream_items ii
                    inner join dream_item_bind iib on iib.parent_id = ii.id
                    inner join item_consumptions ic on ic.child_id = ii.id
                    inner join dream_units uu on uu.id = iib.unit_id
            )
            select ic.id, ic.articula_new as parent_articula, ic.sap_code as parent_sap_code, ic.item_name as parent_name, ic.child_id, i.articula_new as child_articula, i.sap_code as child_sap_code,
            i.item_name as child_name, consumption, ic.unit, ic.unit_id, lvl from item_consumptions ic
            inner join dream_items i on i.id = ic.child_id
            where ic.id in (select parent_id from dream_item_bind)
        ");
        $json = [
            'data'=>$items
        ];
        return json_encode($json);
    }

    public function normArchive(){
        return view('index', ['page'=>'items_page_includes.normArchive']);
    }

    public function getNormArchive(Request $request){
        $columns = array(
            0 => 'model_name',
            1 => 'old_name',
            2 => 'new_name',
            3 => 'old_amount',
            4 => 'new_amount',
            5 => 'old_item_name',
            6 => 'new_item_name',
            7 => 'changed_time',
            8 => 'status'
        );

        $totalData = DreamItem::count();
        if ($request->input('length')==-1){
            $limit = $totalData;
        }else{
            $limit = $request->input('length');
        }

        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        $query_text = "
        SELECT SQL_CALC_FOUND_ROWS  norma_archive.id, concat(mi.articula_new, \" \", mi.item_name) as model_name, concat(oi.articula_new, \" \", oi.item_name) as old_name, concat(ni.articula_new, \" \", ni.item_name) as new_name,
        ROUND(norma_archive.old_amount, 4) as old_amount, ROUND(norma_archive.new_amount, 4) as new_amount, norma_archive.status, SUBSTRING(norma_archive.changed_time, 1, 16) as changed_time, norma_archive.status_id, norma_archive.old_name as old_item_name, norma_archive.new_name as new_item_name
        FROM norma_archive
        left join dream_items mi on mi.id = norma_archive.parent_id
        left join dream_items oi on oi.id = norma_archive.old_item_id
        left join dream_items ni on ni.id = norma_archive.new_item_id
        ";

        $search_text = '';
        if (!empty($request->input('search.value'))){
            $search = $request->input('search.value');
            $search_text .= "
            where ( mi.articula_new like '%$search%' || mi.item_name like '%$search%' ||  oi.articula_new like '%$search%' ||  oi.item_name like '%$search%' ||
              ni.articula_new like '%$search%' ||  ni.item_name like '%$search%' )
            ";
        }


        $query_text.=$search_text." order by $order $dir limit $limit offset $start;";
        $items = DB::select($query_text);
        $totalCount = DB::select('select found_rows() as total_count')[0];
        $data = array();
        if (!empty($items)){
            foreach ($items as $item)
            {
                $nestedData['model_name'] = $item->model_name;
                $nestedData['old_name'] = $item->old_name;
                $nestedData['new_name'] = $item->new_name;
                $nestedData['old_amount'] = $item->old_amount;
                $nestedData['new_amount'] = $item->new_amount;
                $nestedData['new_item_name'] = $item->new_item_name;
                $nestedData['old_item_name'] = $item->old_item_name;
                if ($item->status_id == 1){
                    $nestedData['status'] = "<span class='label label-primary' style='font-size: 14px; padding-top: 0;'>".$item->status."</span>";
                }elseif ($item->status_id == 2){
                    $nestedData['status'] = "<span class='label label-warning' style='font-size: 14px; padding-top: 0;'>".$item->status."</span>";
                }else{
                    $nestedData['status'] = "<span class='label label-danger' style='font-size: 14px; padding-top: 0;'>".$item->status."</span>";
                }

                $nestedData['changed_time'] = $item->changed_time;
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalCount->total_count),
            "data"            => $data
        );

        return json_encode($json_data);
    }

    public function replaceNormItem(Request $request)
    {
        $parent_id = $request->parent_id;

        $main_item_id = $request->main_item_id;

        $replace_item_id = $request->replace_item_id;

        $unit_id = DB::table('dream_storage_item_bind')->where('item_id', $replace_item_id)->get()->first();


        if (is_null($unit_id))
        {
            DB::table('dream_item_bind')->where([['parent_id', $parent_id], ['child_id', $main_item_id]])->update(['child_id' => (int) $replace_item_id]);
        }else{
            DB::table('dream_item_bind')->where([['parent_id', $parent_id], ['child_id', $main_item_id]])->update(['child_id' => (int) $replace_item_id, 'unit_id' => (int) $unit_id->unit_id]);
        }

        $data = [
            'parent_id' => $parent_id,
            'item_id' => $main_item_id,
            'rep_id' => $replace_item_id,
            'unit_id' => ($unit_id) ? $unit_id->unit_id : ''
        ];

        return json_encode($data);
    }
}

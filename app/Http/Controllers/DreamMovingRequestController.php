<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DreamRequest;
use App\DreamStorage;
use App\DreamTransaction;
use Carbon\Carbon;
use Gate;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PDF;

class DreamMovingRequestController extends Controller
{

    const SUPER_ADMIN = 1;
    const REQUESTOR = 3;
    const MOVING_REQUEST_TYPE = 4;
    const DEFECTIVE_STORAGE = 25;
    const PROCESSED = 1;
    const STATE_ACTIVE = 1;
    const OUTPUT = 2;
    const INPUT = 1;
    const RETURN_TO_STORAGE = 4;
    const RETURN_FROM = 10;
    const INPUT_ = 1;
    const WAREHOUSE = 1;
    const OTHER_STORAGE_TYPE = 4;


    public function index(){
        return view('index', ['page'=>'all_moving_requests']);
    }

    public function getMovingRequest(Request $request){
        $storage_id = $request->storage_id;
        DB::table('dream_requests')
            ->leftJoin('dream_request_list', 'dream_request_list.request_id', '=', 'dream_requests.id')
            ->select('dream_request_list.request_id', 'dream_requests.id')
            ->whereNull('dream_request_list.request_id')
            ->groupBy('dream_requests.id')->delete();

        $columns = array(
            0 => 'id',
            1 => 'state_name',
            2 => 'create_time',
            3 => 'from_storage_name',
            4 => 'to_storage_name',
            5 => 'description',
            6 => 'user_name'
        );
        $filterColumns = array(
            0 => 'dream_requests.id',
            1 => 'dream_request_states.name',
            2 => 'dream_requests.create_time',
            3 => 'from_storage.name',
            4 => 'to_storage.name',
            5 => 'dream_requests.description',
            6 => 'users.firstname'
        );
        $totalData = DreamRequest::count();
        if ($request->input('length')==-1){
            $limit = $totalData;
        }else{
            $limit = $request->input('length');
        }
        $filters = [];
        $str = '';
        foreach ($request->columns as $key=>$column){
            if ($column['search']['value']!=null){
                $filters = array_add($filters, $key, $column['search']['value']);
                $str.=$filterColumns[$key]."='".$column['search']['value']."' and ";
            }
        }
        if (!empty($str)>0){
            $str = substr($str, 0, strlen($str)-4);
        }
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        try{
            if (count($filters)==0){
                if (empty($request->input('search.value'))){
                    $items = DB::table('dream_requests')
                        ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                        ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                        ->join('users', 'users.id', '=', 'dream_requests.user_id')
                        ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                        ->select('dream_requests.*', 'to_storage.name as to_storage_name', 'from_storage.name as from_storage_name', 'users.firstname as user_name',
                            'dream_request_states.name as state_name', 'dream_request_states.id as state_id'
                        )->where([['dream_requests.type_id', self::MOVING_REQUEST_TYPE], ['dream_requests.from_storage_id', $storage_id]])
                        ->offset($start)->limit($limit)->orderBy($order, $dir)->get();
                    $filteredData = DB::table('dream_requests')
                        ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                        ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                        ->join('users', 'users.id', '=', 'dream_requests.user_id')
                        ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                        ->select('dream_requests.*', 'to_storage.name as to_storage_name', DB::raw('IFNULL(from_storage.name, " ") as from_storage_name'), 'users.firstname as user_name',
                            'dream_request_states.name as state_name', 'dream_request_states.id as state_id'
                        )->where([['dream_requests.type_id', self::MOVING_REQUEST_TYPE], ['dream_requests.from_storage_id', $storage_id]])
                        ->get();

                    $totalFiltered = count($filteredData);
                }else{
                    $search = $request->input('search.value');
                    $items = DB::table('dream_requests')
                        ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                        ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                        ->join('users', 'users.id', '=', 'dream_requests.user_id')
                        ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                        ->select('dream_requests.*', 'to_storage.name as to_storage_name', 'from_storage.name as from_storage_name', 'users.firstname as user_name',
                            'dream_request_states.name as state_name'
                        )
                        ->where(function ($query) use ($search){
                            $query->orWhere('dream_requests.description', 'LIKE', "%$search%")->orWhere('to_storage.name', 'LIKE',  "%$search%")->orWhere('from_storage.name', 'LIKE',  "%$search%")
                                ->orWhere('users.firstname', 'LIKE',  "%$search%")->orWhere('dream_request_states.name', 'LIKE',  "%$search%")->orWhere('dream_requests.create_time', 'LIKE', "%$search%")
                                ->orWhere('dream_requests.id', 'LIKE', "%$search%");
                        })->where([['dream_requests.type_id', self::MOVING_REQUEST_TYPE], ['dream_requests.from_storage_id', $storage_id]])
                        ->offset($start)->limit($limit)->orderBy($order, $dir)->get();

                    $filteredData = DB::table('dream_requests')
                        ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                        ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                        ->join('users', 'users.id', '=', 'dream_requests.user_id')
                        ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                        ->where(function ($query) use ($search){
                            $query->orWhere('dream_requests.description', 'LIKE', "%$search%")->orWhere('to_storage.name', 'LIKE',  "%$search%")->orWhere('from_storage.name', 'LIKE',  "%$search%")
                                ->orWhere('users.firstname', 'LIKE',  "%$search%")->orWhere('dream_request_states.name', 'LIKE',  "%$search%")->orWhere('dream_requests.create_time', 'LIKE', "%$search%")
                                ->orWhere('dream_requests.id', 'LIKE', "%$search%");
                        })->where([['dream_requests.type_id', self::MOVING_REQUEST_TYPE], ['dream_requests.from_storage_id', $storage_id]])
                        ->select('dream_requests.*', 'to_storage.name as to_storage_name', DB::raw('IFNULL(from_storage.name, " ") as from_storage_name'), 'users.firstname as user_name',
                            'dream_request_states.name as state_name', 'dream_request_states.id as state_id')
                        ->orderBy($order, $dir)->get();
                    $totalFiltered = count($filteredData);

                }
            }else{
                if (empty($request->input('search.value'))){
                    $items = DB::table('dream_requests')
                        ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                        ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                        ->join('users', 'users.id', '=', 'dream_requests.user_id')
                        ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                        ->select('dream_requests.*', 'to_storage.name as to_storage_name', 'from_storage.name as from_storage_name', 'users.firstname as user_name',
                            'dream_request_states.name as state_name', 'dream_request_states.id as state_id'
                        )->where([['dream_requests.type_id', self::MOVING_REQUEST_TYPE], ['dream_requests.from_storage_id', $storage_id]])
                        ->whereRaw($str)
                        ->offset($start)->limit($limit)->orderBy($order, $dir)->get();

                    $filteredData = DB::table('dream_requests')
                        ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                        ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                        ->join('users', 'users.id', '=', 'dream_requests.user_id')
                        ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                        ->where([['dream_requests.type_id', self::MOVING_REQUEST_TYPE], ['dream_requests.from_storage_id', $storage_id]])
                        ->whereRaw($str)
                        ->select('dream_requests.id', 'dream_request_states.name as state_name', 'to_storage.name as to_storage_name', DB::raw('IFNULL(from_storage.name, " ") as from_storage_name'),
                            'dream_requests.create_time', 'users.firstname as user_name')
                        ->get();
                    $totalFiltered = count($filteredData);

                }else{
                    $search = $request->input('search.value');
                    $items = DB::table('dream_requests')
                        ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                        ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                        ->join('users', 'users.id', '=', 'dream_requests.user_id')
                        ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                        ->select('dream_requests.*', 'to_storage.name as to_storage_name', 'from_storage.name as from_storage_name', 'users.firstname as user_name',
                            'dream_request_states.name as state_name'
                        )
                        ->where(function ($query) use ($search){
                            $query->orWhere('dream_requests.description', 'LIKE', "%$search%")->orWhere('to_storage.name', 'LIKE',  "%$search%")->orWhere('from_storage.name', 'LIKE',  "%$search%")
                                ->orWhere('users.firstname', 'LIKE',  "%$search%")->orWhere('dream_request_states.name', 'LIKE',  "%$search%")->orWhere('dream_requests.create_time', 'LIKE', "%$search%")
                                ->orWhere('dream_requests.id', 'LIKE', "%$search%");
                        })->where([['dream_requests.type_id', self::MOVING_REQUEST_TYPE], ['dream_requests.from_storage_id', $storage_id]])
                        ->whereRaw($str)
                        ->offset($start)->limit($limit)->orderBy($order, $dir)->get();

                    $filteredData = DB::table('dream_requests')
                        ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                        ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                        ->join('users', 'users.id', '=', 'dream_requests.user_id')
                        ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                        ->where(function ($query) use ($search){
                            $query->orWhere('dream_requests.description', 'LIKE', "%$search%")->orWhere('to_storage.name', 'LIKE',  "%$search%")->orWhere('from_storage.name', 'LIKE',  "%$search%")
                                ->orWhere('users.firstname', 'LIKE',  "%$search%")->orWhere('dream_request_states.name', 'LIKE',  "%$search%")->orWhere('dream_requests.create_time', 'LIKE', "%$search%")
                                ->orWhere('dream_requests.id', 'LIKE', "%$search%");
                        })->where([['dream_requests.type_id', self::MOVING_REQUEST_TYPE], ['dream_requests.from_storage_id', $storage_id]])
                        ->whereRaw($str)
                        ->select('dream_requests.id', 'dream_request_states.name as state_name', 'to_storage.name as to_storage_name', DB::raw('IFNULL(from_storage.name, " ") as from_storage_name'),
                            'dream_requests.create_time', 'users.firstname as user_name')
                        ->orderBy($order, $dir)->get();
                    $totalFiltered = count($filteredData);
                }
            }
            $data = array();
            $tr = [];
            foreach ($columns as $key=>$column){
                $tr = array_add($tr, $key, $filteredData->unique($column)->pluck($column)->all());
            }
            if (!empty($items)){
                foreach ($items as $item)
                {
                    $nestedData['DT_RowId'] = $item->id;
                    $nestedData['id'] = $item->id;
                    $nestedData['state'] = ($item->state==self::PROCESSED)?"<span class='label label-primary' style='font-size: 14px; padding-top: 0;'>".$item->state_name."</span>":"<span class='label label-danger' style='font-size: 14px; padding-top: 0;'>".$item->state_name."</span>";
                    $nestedData['to_storage'] = $item->to_storage_name;
                    $nestedData['from_storage'] = $item->from_storage_name;
                    $nestedData['create_time'] = Carbon::createFromFormat('Y-m-d H:i:s', $item->create_time)->format('Y-m-d H:i');
                    $nestedData['applier'] = $item->user_name;
                    $nestedData['description'] = $item->description;
                    $nestedData['detail'] = "<button type='button' data-toggle='modal' data-backdrop='static' data-keyboard='false'  data-target='#show_detail' class='btn btn-info ' onclick='show_detail(".$item->id.")'><i class='fa fa-info-circle'></i></button>";
                    $data[] = $nestedData;
                }
            }

            $json_data = array(
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => intval($totalData),
                "recordsFiltered" => intval($totalFiltered),
                "data"            => $data,
                "filteredData"    => $tr,
                "filters"         => $filters
            );

            return json_encode($json_data);
        }catch (\Exception $e){
            return response()->json(['errors'=>'невозможно добавить!', $e], 500);
        }
    }

    public function getMovingRequestList(Request $request){
        $request_id = $request->moving_request_id;
        $data = array();
        $total = 0;
        $request_data = DreamRequest::find($request_id);
        $allowToAccept = false;

        $items = DB::table('dream_request_list')
            ->join('dream_items', 'dream_items.id', '=', 'dream_request_list.item_id')
            ->join('dream_units', 'dream_units.id', '=', 'dream_request_list.unit_id')
            ->select('dream_request_list.amount as request_amount', 'dream_items.id as item_id', 'dream_items.articula_new', 'dream_items.articula_old', 'dream_items.item_name',
                'dream_items.sap_code', 'dream_units.unit', 'dream_request_list.id')
            ->where([['dream_request_list.request_id', $request_id], ['dream_request_list.delete_time', null]])->get();

        if (DreamRequest::find($request_id)->user_id==Auth::user()->id){
            $hasPerm = true;
        }else{
            $hasPerm = false;
        }
        if (!empty($items)){
            foreach ($items as $item)
            {
                $total += round($item->request_amount, 3);
                $remaining = $this->get_item_remaining($request_data->from_storage_id, $item->item_id);
                $nestedData['DT_RowId'] = $item->id;
                $nestedData['id'] = $item->id;
                $nestedData['articula_new'] = $item->articula_new;
                $nestedData['articula_old'] = $item->articula_old;
                $nestedData['sap_code'] = $item->sap_code;
                $nestedData['item_name'] = $item->item_name;
                $nestedData['amount'] = round($item->request_amount, 3);
                if ($remaining>=round($item->request_amount, 3)){
                    $nestedData['remaining'] = $remaining;
                }else{
                    $nestedData['remaining'] = '<p style="display: block; background-color: red; color: white; padding: 0; margin: 0">'.$remaining.'</p>';
                }
                $nestedData['remaining_num'] = $remaining;
                $nestedData['unit'] = $item->unit;
                if ($request_data->state==self::STATE_ACTIVE && ($request_data->user_id==Auth::user()->id)){
                    $nestedData['edit_btn'] = "<button type='button' class='btn btn-warning' data-toggle='modal' data-backdrop='static' data-keyboard='false' data-target='#edit_moving_request_item_amount' onclick=\"edit_moving_request_item_amount('".$item->id."', '".round($item->request_amount, 3)."')\"><i class='fa fa-edit'></i></button>" .
                        "<button type='button' class='btn btn-danger' onclick='delete_moving_request_list(".$item->id.")'><i class='fa fa-trash'></i></button>";
                }else{
                    $nestedData['edit_btn'] = "";
                }
                $data[] = $nestedData;
            }
        }else{
            $request_data->update(['status'=>2]);
        }

        $json_data = array(
            "data" => $data,
            "hasPerm" => $hasPerm,
            "requestData" => $request_data,
            "total" => $total,
            "allowToAccept" => $allowToAccept
        );
        return json_encode($json_data);
    }

    public function get_item_remaining($storage_id, $item_id)
    {
        $total = DB::select("select sum(operation*amount) as total from dream_transaction_list ".
            "left join dream_transaction_storage_bind on dream_transaction_list.transaction_id=dream_transaction_storage_bind.transaction_id and dream_transaction_storage_bind.storage_id=$storage_id " .
            "left join dream_transaction_types on dream_transaction_types.id=dream_transaction_storage_bind.operation_id where item_id=$item_id and dream_transaction_list.delete_time is null")[0];
        if ($total->total == null){
            $total->total = 0;
        }
        return round($total->total, 3);
    }

    public function closeMovingRequest(Request $request){
        try{
            $request_ = DreamRequest::find(DB::table('dream_request_list')->where('request_id', $request->moving_request_id)->first()->request_id);

            if ($request_->user_id!=Auth::user()->id){
                return response()->json(['errors'=>array('вы не можете закрыть')], 500);
            }else{
                DB::table('dream_requests')->where('id', $request->moving_request_id)->update(['state'=>2]);
            }
            return json_encode($request->all());
        }catch (\Exception $exception){
            return response()->json(['errors'=>array($exception)], 500);
        }
    }

    public function createMovingRequest(){
        $storages = DB::table('dream_storages')->whereIn('dream_storages.type_id', array(1))->get();
        if (Auth::user()->roles->first()->id==self::SUPER_ADMIN){
            $lines = DB::table('dream_storages')->whereIn('dream_storages.type_id', array(2))->get();
        }else{
            $lines = DB::table('dream_storage_user_bind')
                ->join('dream_storages', 'dream_storages.id', '=', 'dream_storage_user_bind.storage_id')
                ->select('dream_storages.*')
                ->where([['dream_storage_user_bind.user_id', Auth::user()->id], ['dream_storages.type_id', 2]])->get();
        }
        return view('index', ['page'=>'create_moving_request', 'storages'=>$storages, 'lines'=>$lines]);
    }

    public function getStorageMovingRequestItems(Request $request){
        try{
            $from_storage_id = $request->from_storage_id;
            $to_storage_id = $request->to_storage_id;
            $items = DB::table('dream_transaction_list')
                ->join('dream_items', 'dream_items.id', '=', 'dream_transaction_list.item_id')
                ->join('dream_units', 'dream_units.id', '=',  'dream_transaction_list.unit_id')
                ->join('dream_transaction_storage_bind', 'dream_transaction_storage_bind.transaction_id', '=', 'dream_transaction_list.transaction_id')
                ->join('dream_transactions', 'dream_transactions.id', '=', 'dream_transaction_list.transaction_id')
                ->join('dream_transaction_types', 'dream_transaction_types.id', '=', 'dream_transaction_storage_bind.operation_id')
                ->join('dream_storage_item_bind as ds', 'ds.item_id', '=', 'dream_items.id')
                ->join(DB::raw("(select dream_storage_item_bind.item_id from dream_storage_item_bind where dream_storage_item_bind.storage_id=$from_storage_id) as merged_items"), 'merged_items.item_id', '=', 'dream_items.id')
                ->select('dream_items.*', DB::raw('sum(dream_transaction_list.amount * dream_transaction_types.operation) as remaining'),
                    'dream_units.id as unit_id', 'dream_units.unit'
                )->where([['ds.storage_id', $to_storage_id]])->whereNull('dream_transaction_list.delete_time')
                ->groupBy('dream_transaction_list.item_id', 'dream_transaction_list.unit_id')->get();
            return json_encode($items);
        }catch (\Exception $exception){
            return response()->json(['errors'=>array('нет доступа!')], 500);
        }
    }

    public function collectMovingRequestItems(Request $request){
        try{
            $storage_user = DB::table('dream_storage_user_bind')
                ->join('dream_storages', 'dream_storages.id', '=', 'dream_storage_user_bind.storage_id')
                ->where([['dream_storage_user_bind.user_id', Auth::user()->id], ['dream_storages.type_id', 2]])->get();

            $arr = [];
            if (!is_null($storage_user) || DB::table('user_role')->where('user_id', Auth::user()->id)->get()->first()->role_id==self::SUPER_ADMIN ){
                $storage_id = $request->from_storage_id;
                $item_id = $request->item_id;
                $amount = $request->amount;
                $item = DB::table('dream_transaction_list')
                    ->join('dream_items', 'dream_items.id', '=', 'dream_transaction_list.item_id')
                    ->join('dream_units', 'dream_units.id', '=',  'dream_transaction_list.unit_id')
                    ->join('dream_transaction_storage_bind', 'dream_transaction_storage_bind.transaction_id', '=', 'dream_transaction_list.transaction_id')
                    ->join('dream_transactions', 'dream_transactions.id', '=', 'dream_transaction_list.transaction_id')
                    ->join('dream_transaction_types', 'dream_transaction_types.id', '=', 'dream_transaction_storage_bind.operation_id')
                    ->where([['dream_transaction_storage_bind.storage_id', $storage_id], ['dream_transaction_list.item_id', $item_id], ['dream_transactions.create_time', '<=', Carbon::now()]])
                    ->whereNull('dream_transaction_list.delete_time')
                    ->select('dream_items.*', DB::raw('sum(dream_transaction_list.amount * dream_transaction_types.operation) as remaining'),
                        'dream_units.id as unit_id', 'dream_units.unit'
                    )->groupBy('dream_transaction_list.item_id', 'dream_transaction_list.unit_id')->first();
                if (!empty($item)){
                    $data = [
                        'articula_new'=>$item->articula_new,
                        'sap_code'=>$item->sap_code,
                        'item_name'=>$item->item_name,
                        'unit'=>$item->unit,
                        'unit_id'=>$item->unit_id,
                        'storage_id'=>$storage_id,
                        'amount'=>$amount,
                        'remaining'=>round($item->remaining, 4),
                        'item_id'=>$item->id
                    ];
                    array_push($arr, $data);
                }else{
                    return response()->json(['errors'=>array('нет на линии!')], 500);
                }
            }else{
                return response()->json(['errors'=>array('у вас нет доступа!')], 500);
            }
            return json_encode($arr);
        }catch(\Exception $e){
            return response()->json(['errors'=>'невозможно добавить!', $e], 500);
        }
    }

    public function collectItemsByStorageForMMovingRequest(Request $request){
        $data = array();
        $items = json_decode($request->items);
        if (!empty($items)){
            foreach ($items as $item){
                $storage_items = DB::table('dream_storage_item_bind')
                    ->join('dream_items', 'dream_items.id', '=', 'dream_storage_item_bind.item_id')
                    ->join('dream_units', 'dream_units.id', '=', 'dream_storage_item_bind.unit_id')
                    ->join('dream_storages', 'dream_storages.id', '=', 'dream_storage_item_bind.storage_id')
                    ->where([['dream_storage_item_bind.item_id', $item->item_id], ['dream_storages.type_id', self::WAREHOUSE]])
                    ->select('dream_items.*', 'dream_units.id as unit_id', 'dream_units.unit as unit_name', 'dream_storages.id as storage_id', 'dream_storages.name as storage_name')
                    ->get();

                if (!empty($storage_items)){
                    foreach ($storage_items as $storage_item){
                        $arr = [
                            'articula_new'=>$storage_item->articula_new,
                            'sap_code'=>$storage_item->sap_code,
                            'item_name'=>$storage_item->item_name,
                            'unit'=>$storage_item->unit_name,
                            'storage_name'=>$storage_item->storage_name,
                            'unit_id'=>$storage_item->unit_id,
                            'storage_id'=>$storage_item->storage_id,
                            'amount'=>$item->amount,
                            'remaining'=>$this->get_item_remaining($request->storage_id, $storage_item->id),
                            'item_id'=>$storage_item->id
                        ];
                        $data[] = $arr;
                    }
                }
            }
        }
        return json_encode($data);
    }

    public function saveMovingRequestItems(Request $request){

        //Parameters the function gets
        //items
        //from_storage_id
        //to_storage_id
        //description

        $storage_user = DB::table('dream_storage_user_bind')
            ->join('dream_storages', 'dream_storages.id', '=', 'dream_storage_user_bind.storage_id')
            ->where([['dream_storage_user_bind.user_id', Auth::user()->id], ['dream_storages.type_id', 2]])->get();

        $validateNames = array(
            'items'=>'элементы',
            'description'=>'примечание',
            'from_storage_id'=>'отправитель',
            'to_storage_id'=>'получатель',
        );
        if ($request->from_storage_id==0){
            $validator = Validator::make($request->all(), [
                'items' => 'required',
                'description' => 'required'
            ]);
        }else{
            $validator = Validator::make($request->all(), [
                'items' => 'required',
                'from_storage_id' => 'required',
                'to_storage_id' => 'required'
            ]);
        }
        $validator->setAttributeNames($validateNames);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()], 500);
        }

        try{
            if (!is_null($storage_user) || DB::table('user_role')->where('user_id', Auth::user()->id)->get()->first()->role_id==self::SUPER_ADMIN){
                DB::transaction(function () use($request){

                    $request_items = json_decode($request->items);
                    $data = [
                        'to_storage_id' => $request->to_storage_id,
                        'from_storage_id' => $request->from_storage_id,
                        'user_id' => Auth::user()->id,
                        'description' => $request->description,
                        'type_id' => self::MOVING_REQUEST_TYPE
                    ];

                    $tr_req_id = DreamRequest::create($data)->id;

                    foreach ($request_items as $item){
                        $item_data = [
                            'request_id' => $tr_req_id,
                            'item_id' => $item->item_id,
                            'amount' => (float)$item->amount,
                            'unit_id' => $item->unit_id
                        ];

                        DB::table('dream_request_list')->insert($item_data);
                    }
                });
                return json_encode($request->except('storage_name'));
            }else{
                return response()->json(['errors'=>array('у вас нет доступа!')], 500);
            }

        }catch (\Exception $e){
            return response()->json(['error'=>array($e)], 500);
        }
    }

    public function saveMovingRequestItemsByStorage(Request $request){
        $storage_user = DB::table('dream_storage_user_bind')
            ->join('dream_storages', 'dream_storages.id', '=', 'dream_storage_user_bind.storage_id')
            ->where([['dream_storage_user_bind.user_id', Auth::user()->id], ['dream_storages.type_id', 2]])->get();

        $validateNames = array(
            'items'=>'элементы',
            'description'=>'примечание',
            'from_storage_id'=>'отправитель',
            'to_storage_id'=>'получатель',
        );
        $validator = Validator::make($request->all(), [
            'items' => 'required'
        ]);
        $validator->setAttributeNames($validateNames);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()], 500);
        }

        try{
            if (!is_null($storage_user) || DB::table('user_role')->where('user_id', Auth::user()->id)->get()->first()->role_id==self::SUPER_ADMIN){
                DB::transaction(function () use($request){

                    $request_items = json_decode($request->items);
                    $storages = DreamStorage::all();
                    foreach ($storages as $storage){
                        if ($this->contain_storage($storage->id, $request_items)){
                            $data = [
                                'to_storage_id' => $storage->id,
                                'from_storage_id' => $request->from_storage_id,
                                'user_id' => Auth::user()->id,
                                'description' => $request->description,
                                'type_id' => self::MOVING_REQUEST_TYPE
                            ];

                            $tr_req_id = DreamRequest::create($data)->id;

                            foreach ($request_items as $item){
                                $item_data = [
                                    'request_id' => $tr_req_id,
                                    'item_id' => $item->item_id,
                                    'amount' => (float)$item->amount,
                                    'unit_id' => $item->unit_id
                                ];

                                DB::table('dream_request_list')->insert($item_data);
                            }
                        }
                    }

                });
                return json_encode($request->except('storage_name'));
            }else{
                return response()->json(['errors'=>array('у вас нет доступа!')], 500);
            }

        }catch (\Exception $e){
            return response()->json(['error'=>array($e)], 500);
        }
    }

    public function contain_storage($storage_id, $arr){
        foreach ($arr as $value){
            if ($storage_id==$value->storage_id){
                return true;
            }
        }
        return false;
    }

    public function getMovingRequestsForAccept(Request $request){
        $storage_id = $request->storage_id;

        $columns = array(
            0 => 'id',
            1 => 'user_name',
            2 => 'create_time',
            3 => 'from_storage_name',
            4 => 'description',
        );
        $filterColumns = array(
            0 => 'dream_requests.id',
            1 => 'users.firstname',
            2 => 'dream_requests.create_time',
            3 => 'from_storage.name',
            4 => 'dream_requests.description'
        );
        $totalData = DreamRequest::where([['type_id', self::MOVING_REQUEST_TYPE], ['to_storage_id', $storage_id], ['state', self::STATE_ACTIVE]])->count();
        if ($request->input('length')==-1){
            $limit = $totalData;
        }else{
            $limit = $request->input('length');
        }
        $filters = [];
        $str = '';
        foreach ($request->columns as $key=>$column){
            if ($column['search']['value']!=null){
                $filters = array_add($filters, $key, $column['search']['value']);
                $str.=$filterColumns[$key]."='".$column['search']['value']."' and ";
            }
        }
        if (!empty($str)>0){
            $str = substr($str, 0, strlen($str)-4);
        }
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if (count($filters)==0){
            if (empty($request->input('search.value'))){
                $items = DB::table('dream_requests')
                    ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                    ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                    ->join('users', 'users.id', '=', 'dream_requests.user_id')
                    ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                    ->select('dream_requests.*', 'from_storage.name as from_storage_name', 'to_storage.name as to_storage_name', 'users.firstname as user_name',
                        'dream_request_states.name as state_name', 'dream_request_states.id as state_id'
                    )
                    ->where([['dream_requests.to_storage_id', $storage_id], ['dream_requests.state', self::STATE_ACTIVE], ['dream_requests.type_id', self::MOVING_REQUEST_TYPE]])
                    ->offset($start)->limit($limit)->orderBy($order, $dir)->get();

                $filteredData = DB::table('dream_requests')
                    ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                    ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                    ->join('users', 'users.id', '=', 'dream_requests.user_id')
                    ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                    ->select('dream_requests.*', 'from_storage.name as from_storage_name',
                        DB::raw('IFNULL(to_storage.name, " ") as to_storage_name'), 'users.firstname as user_name',
                        'dream_request_states.name as state_name', 'dream_request_states.id as state_id')
                    ->where([['dream_requests.to_storage_id', $storage_id], ['dream_requests.state', self::STATE_ACTIVE], ['dream_requests.type_id', self::MOVING_REQUEST_TYPE]])
                    ->get();

                $totalFiltered = count($filteredData);
            }else{
                $search = $request->input('search.value');
                $items = DB::table('dream_requests')
                    ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                    ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                    ->join('users', 'users.id', '=', 'dream_requests.user_id')
                    ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                    ->select('dream_requests.*', 'from_storage.name as from_storage_name', 'to_storage.name as to_storage_name', 'users.firstname as user_name',
                        'dream_request_states.name as state_name'
                    )
                    ->where(function ($query) use ($search){
                        $query->orWhere('dream_requests.description', 'LIKE', "%$search%")->orWhere('from_storage.name', 'LIKE',  "%$search%")->orWhere('to_storage.name', 'LIKE',  "%$search%")
                            ->orWhere('users.firstname', 'LIKE',  "%$search%")->orWhere('dream_request_states.name', 'LIKE',  "%$search%")->orWhere('dream_requests.create_time', 'LIKE', "%$search%")
                            ->orWhere('dream_requests.id', 'LIKE', "%$search%");
                    })
                    ->where([['dream_requests.to_storage_id', $storage_id], ['dream_requests.state', self::STATE_ACTIVE], ['dream_requests.type_id', self::MOVING_REQUEST_TYPE]])
                    ->offset($start)->limit($limit)->orderBy($order, $dir)->get();

                $filteredData = DB::table('dream_requests')
                    ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                    ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                    ->join('users', 'users.id', '=', 'dream_requests.user_id')
                    ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                    ->where(function ($query) use ($search){
                        $query->orWhere('dream_requests.description', 'LIKE', "%$search%")->orWhere('from_storage.name', 'LIKE',  "%$search%")->orWhere('to_storage.name', 'LIKE',  "%$search%")
                            ->orWhere('users.firstname', 'LIKE',  "%$search%")->orWhere('dream_request_states.name', 'LIKE',  "%$search%")->orWhere('dream_requests.create_time', 'LIKE', "%$search%")
                            ->orWhere('dream_requests.id', 'LIKE', "%$search%");
                    })
                    ->where([['dream_requests.to_storage_id', $storage_id], ['dream_requests.state', self::STATE_ACTIVE], ['dream_requests.type_id', self::MOVING_REQUEST_TYPE]])
                    ->select('dream_requests.*', 'from_storage.name as from_storage_name', DB::raw('IFNULL(to_storage.name, " ") as to_storage_name'), 'users.firstname as user_name',
                        'dream_request_states.name as state_name', 'dream_request_states.id as state_id')
                    ->orderBy($order, $dir)->get();
                $totalFiltered = count($filteredData);

            }
        }else{
            if (empty($request->input('search.value'))){
                $items = DB::table('dream_requests')
                    ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                    ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                    ->join('users', 'users.id', '=', 'dream_requests.user_id')
                    ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                    ->select('dream_requests.*', 'from_storage.name as from_storage_name', 'to_storage.name as to_storage_name', 'users.firstname as user_name',
                        'dream_request_states.name as state_name', 'dream_request_states.id as state_id'
                    )
                    ->whereRaw($str)
                    ->where([['dream_requests.to_storage_id', $storage_id], ['dream_requests.state', self::STATE_ACTIVE], ['dream_requests.type_id', self::MOVING_REQUEST_TYPE]])
                    ->offset($start)->limit($limit)->orderBy($order, $dir)->get();

                $filteredData = DB::table('dream_requests')
                    ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                    ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                    ->join('users', 'users.id', '=', 'dream_requests.user_id')
                    ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                    ->whereRaw($str)
                    ->where([['dream_requests.to_storage_id', $storage_id], ['dream_requests.state', self::STATE_ACTIVE], ['dream_requests.type_id', self::MOVING_REQUEST_TYPE]])
                    ->select('dream_requests.id', 'dream_request_states.name as state_name', 'from_storage.name as from_storage_name', DB::raw('IFNULL(to_storage.name, " ") as to_storage_name'),
                        'dream_requests.create_time', 'users.firstname as user_name')
                    ->get();
                $totalFiltered = count($filteredData);

            }else{
                $search = $request->input('search.value');
                $items = DB::table('dream_requests')
                    ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                    ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                    ->join('users', 'users.id', '=', 'dream_requests.user_id')
                    ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                    ->select('dream_requests.*', 'from_storage.name as from_storage_name', 'to_storage.name as to_storage_name', 'users.firstname as user_name',
                        'dream_request_states.name as state_name'
                    )
                    ->where(function ($query) use ($search){
                        $query->orWhere('dream_requests.description', 'LIKE', "%$search%")->orWhere('from_storage.name', 'LIKE',  "%$search%")->orWhere('to_storage.name', 'LIKE',  "%$search%")
                            ->orWhere('users.firstname', 'LIKE',  "%$search%")->orWhere('dream_request_states.name', 'LIKE',  "%$search%")->orWhere('dream_requests.create_time', 'LIKE', "%$search%")
                            ->orWhere('dream_requests.id', 'LIKE', "%$search%");
                    })
                    ->whereRaw($str)
                    ->where([['dream_requests.to_storage_id', $storage_id], ['dream_requests.state', self::STATE_ACTIVE], ['dream_requests.type_id', self::MOVING_REQUEST_TYPE]])
                    ->offset($start)->limit($limit)->orderBy($order, $dir)->get();

                $filteredData = DB::table('dream_requests')
                    ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                    ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                    ->join('users', 'users.id', '=', 'dream_requests.user_id')
                    ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                    ->where(function ($query) use ($search){
                        $query->orWhere('dream_requests.description', 'LIKE', "%$search%")->orWhere('from_storage.name', 'LIKE',  "%$search%")->orWhere('to_storage.name', 'LIKE',  "%$search%")
                            ->orWhere('users.firstname', 'LIKE',  "%$search%")->orWhere('dream_request_states.name', 'LIKE',  "%$search%")->orWhere('dream_requests.create_time', 'LIKE', "%$search%")
                            ->orWhere('dream_requests.id', 'LIKE', "%$search%");
                    })
                    ->whereRaw($str)
                    ->where([['dream_requests.to_storage_id', $storage_id], ['dream_requests.state', self::STATE_ACTIVE], ['dream_requests.type_id', self::MOVING_REQUEST_TYPE]])
                    ->select('dream_requests.id', 'dream_request_states.name as state_name', 'from_storage.name as from_storage_name', DB::raw('IFNULL(to_storage.name, " ") as to_storage_name'),
                        'dream_requests.create_time', 'users.firstname as user_name')
                    ->orderBy($order, $dir)->get();
                $totalFiltered = count($filteredData);
            }
        }
        $data = array();
        $tr = [];
        if (!empty($filteredData)){
            foreach ($columns as $key=>$column){
                $tr = array_add($tr, $key, $filteredData->unique($column)->pluck($column)->all());
            }
        }else{
            $tr = [];
        }
        if (!empty($items)){
            foreach ($items as $item)
            {
                $nestedData['DT_RowId'] = $item->id;
                $nestedData['id'] = $item->id;
                $nestedData['from_storage'] = $item->from_storage_name;
                $nestedData['create_time'] = Carbon::createFromFormat('Y-m-d H:i:s', $item->create_time)->format('Y-m-d H:i');
                $nestedData['applier'] = $item->user_name;
                $nestedData['description'] = $item->description;
                $nestedData['detail'] = "<button type='button' data-toggle='modal' data-backdrop='static' data-keyboard='false'  data-target='#show_moving_request_detail' class='btn btn-info ' onclick='show_moving_request_detail(".$item->id.")'><i class='fa fa-info-circle'></i></button>";
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
            "filteredData"    => $tr,
            "filters"         => $filters
        );

        return json_encode($json_data);
    }

    public function showMovingRequestListItemsForAccept(Request $request){
        $request_id = $request->moving_request_id;
        $data = array();
        $total = 0;
        $request_data = DreamRequest::find($request_id);
        $allowToAccept = true;

        $items = DB::table('dream_request_list')
            ->join('dream_items', 'dream_items.id', '=', 'dream_request_list.item_id')
            ->join('dream_units', 'dream_units.id', '=', 'dream_request_list.unit_id')
            ->select('dream_request_list.amount as request_amount', 'dream_items.id as item_id', 'dream_items.articula_new', 'dream_items.articula_old', 'dream_items.item_name',
                'dream_items.sap_code', 'dream_units.unit', 'dream_request_list.id')
            ->where([['dream_request_list.request_id', $request_id], ['dream_request_list.delete_time', null]])->get();

        if (!empty($items)){
            foreach ($items as $item)
            {
                $total += round($item->request_amount, 3);
                $remaining = $this->get_item_remaining($request_data->from_storage_id, $item->item_id);
                if ($remaining<round($item->request_amount, 3)) {
                    $allowToAccept = false;
                }
                $nestedData['DT_RowId'] = $item->id;
                $nestedData['articula_new'] = $item->articula_new;
                $nestedData['articula_old'] = $item->articula_old;
                $nestedData['sap_code'] = $item->sap_code;
                $nestedData['item_name'] = $item->item_name;
                $nestedData['amount'] = round($item->request_amount, 3);
                $nestedData['input_edit']= '<input id="item-id-'.$item->id.'" max="'.round($item->request_amount, 3).'" onkeyup="if(parseFloat(this.value) >= parseFloat(this.max) || parseFloat(this.value)<0 || NaN(this.value)) this.value = this.max;" name="request_list_id[' . $item->id . ']" value="' . round($item->request_amount, 3) . '" class="form-control" />';
                if ($remaining>=round($item->request_amount, 3)){
                    $nestedData['remaining'] = $remaining;
                }else{
                    $nestedData['remaining'] = '<p style="display: block; background-color: red; color: white; padding: 0; margin: 0">'.$remaining.'</p>';
                }
                $nestedData['remaining_num'] = $remaining;
                $nestedData['unit'] = $item->unit;
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "data" => $data,
            "requestData" => $request_data,
            "total" => $total,
            "allowToAccept" => $allowToAccept
        );
        return json_encode($json_data);
    }

    public function acceptMovingRequestItems(Request $request){
        try{

            //dd($request->request->all());
            $request_data = $request->request->all();
            $res = DB::transaction(function () use($request_data){
                $request_id = $request_data["moving_request_id_for_accept"];
                $request_list_ids = $request_data["request_list_id"];
                $requestData = DreamRequest::find($request_id);
                if ($requestData->state==2){
                    return response()->json(['errors'=>array('закрыт')], 500);
                }
                $request_list_item = DB::table('dream_request_list')->where('dream_request_list.request_id', $request_id)->get();
                $transaction = [
                    'user_id' => Auth::user()->id,
                    'description' => $requestData->description
                ];
                $transaction_id = DreamTransaction::create($transaction)->id;
                $output_from_storage = [
                    'transaction_id'=>$transaction_id,
                    'storage_id'=>$requestData->to_storage_id,
                    'operation_id'=>self::INPUT
                ];

                DB::table('dream_transaction_storage_bind')->insert($output_from_storage);

                $input_to_defective_storage = [
                    'transaction_id'=>$transaction_id,
                    'storage_id'=>$requestData->from_storage_id,
                    'operation_id'=>self::OUTPUT
                ];
                DB::table('dream_transaction_storage_bind')->insert($input_to_defective_storage);

                $is_all_amount_accepted=true;

                foreach ($request_list_item as $item){
                    //dd($item->amount);
                    //dd($request_list_ids[$item->id]);
                    if((float)$request_list_ids[$item->id]!=(float)$item->amount){
                        $is_all_amount_accepted=false;
                        //dd($is_all_amount_accepted);

                        DB::table('dream_request_list')->where('id', $item->id)->update(["amount"=> $item->amount-$request_list_ids[$item->id]]);
                    }
                    $data = [
                        'transaction_id' => $transaction_id,
                        'item_id' => $item->item_id,
                        'unit_id' => $item->unit_id,
                        'amount' => $request_list_ids[$item->id]
                    ];
                    $transaction_list_id = DB::table('dream_transaction_list')->insertGetId($data);
                    DB::table('dream_transaction_request_bind')->insert(['transaction_list_id'=>$transaction_list_id, 'request_list_id'=>$item->id]);
                }

                if($is_all_amount_accepted){
                    DB::table('dream_requests')->where('id', $request_id)->update(['state'=>2]);
                }
                //return redirect()->route('storageInputOutputOperation');
                //return redirect(route('storageInputOutputOperation'), 302);
                return back();
            });
            return back();
        }catch (\Exception $e){
            return response()->json(['errors'=>array($e)], 500);
        }
    }

    public function updateMovingRequestList(Request $request){
        try{
            $res = DB::transaction(function () use($request){
                $list = DB::table('dream_request_list')
                    ->where('dream_request_list.id', $request->moving_request_list_id)->first();
                $requestData = DreamRequest::find($list->request_id);
                if ($requestData->state==self::STATE_ACTIVE){
                    DB::table('dream_request_list')
                        ->where('dream_request_list.id', $request->moving_request_list_id)->update(['amount'=>$request->edit_amount]);
                }

                return $request->moving_request_list_id;
            });
            return json_encode($res);
        }catch (\Exception $e){
            return response()->json(['errors'=>array($e)], 500);
        }
    }

    public function deleteMovingRequestList(Request $request){
        try{
            $res = DB::transaction(function () use($request){
                $list = DB::table('dream_request_list')
                    ->where('dream_request_list.id', $request->moving_request_list_id)->first();
                $requestData = DreamRequest::find($list->request_id);
                if ($requestData->state==self::STATE_ACTIVE){
                    DB::table('dream_request_list')
                        ->where('dream_request_list.id', $request->moving_request_list_id)->delete();
                }

                return $request->moving_request_list_id;
            });
            return json_encode($res);
        }catch (\Exception $e){
            return response()->json(['errors'=>array($e)], 500);
        }
    }

    public function moving_requests_invoice_generate($moving_request_id){
        $managers_of_lines = [
            '18'=>'Хусниддинов С.',
            '19'=>'Алибаев К.',
            '20'=>'Алибаев К.',
            '17'=>'___________________________',
            '6'=>'___________________________',
            '22'=>'Алибаев К.',
            '26'=>'Хужаназаров Ж.',
        ];
        $managers_of_warehouse = [
            '4'=>'Султонов О.',
            '1'=>'Содиков Б.',
            '2'=>'Бийминов М.',
            '8'=>'Мирзахмедов М'
        ];
        $model = DB::table('dream_requests')
            ->leftJoin('dream_items', 'dream_items.id', '=', 'dream_requests.model_id')
            ->leftJoin('dream_storage_item_bind', 'dream_storage_item_bind.item_id', '=', 'dream_requests.model_id')
            ->leftJoin('dream_units', 'dream_units.id', '=', 'dream_storage_item_bind.unit_id')
            ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
            ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
            ->select('dream_items.articula_new', 'dream_items.item_name',  DB::raw("IFNULL(dream_items.sap_code, '') as sap_code"), 'dream_requests.*', 'from_storage.name as from_storage_name',
                'from_storage.id as from_storage_id', 'to_storage.name as to_storage_name', 'to_storage.id as to_storage_id', 'dream_units.unit as unit_name')
            ->where('dream_requests.id', $moving_request_id)
            ->first();
        $items = DB::table('dream_request_list')
            ->join('dream_items', 'dream_items.id', '=', 'dream_request_list.item_id')
            ->join('dream_units', 'dream_units.id', '=', 'dream_request_list.unit_id')
            ->select('dream_items.articula_new', 'dream_items.item_name', DB::raw("IFNULL(dream_items.sap_code, '') as sap_code"), 'dream_request_list.*', 'dream_units.unit as unit_name')
            ->where([['dream_request_list.request_id', $moving_request_id], ['dream_request_list.delete_time', null]])
            ->get();
        $pages = $this->breakToPages($items);
        $date = Carbon::createFromFormat('Y-m-d H:i:s', $model->create_time);
        $storage_type = DB::table('dream_storages')->where('dream_storages.id', $model->from_storage_id)->first();
        if ($storage_type->type_id==self::OTHER_STORAGE_TYPE){
            $data = [
                'pages' => $pages,
                'created_date'=>$date->format('d.m.Y'),
                'created_time'=>$date->format('H:i'),
                'production_line'=>$model->from_storage_name,
                'storage_name'=>$model->to_storage_name,
                'request_id'=>$model->id,
                'manager_of_line'=>'________________',
                'manager_of_warehouse'=>(!is_null($model->to_storage_id))?(isset($managers_of_warehouse[$model->to_storage_id]))?$managers_of_warehouse[$model->to_storage_id]:'________________':'________________',
                'isOtherType'=>true
            ];
        }else{
            $data = [
                'pages' => $pages,
                'created_date'=>$date->format('d.m.Y'),
                'created_time'=>$date->format('H:i'),
                'production_line'=>$model->from_storage_name,
                'storage_name'=>$model->to_storage_name,
                'request_id'=>$model->id,
                'manager_of_line'=>(!is_null($model->from_storage_id))?(isset($managers_of_lines[$model->from_storage_id]))?$managers_of_lines[$model->from_storage_id]:'________________':'________________',
                'manager_of_warehouse'=>(!is_null($model->to_storage_id))?(isset($managers_of_warehouse[$model->to_storage_id]))?$managers_of_warehouse[$model->to_storage_id]:'________________':'________________',
                'isOtherType'=>false
            ];
        }

        $pdf = PDF::loadView('pdf.moving_request', $data);
//        $pdf->setEncryption('1','sherzod',array('copy'));
        return $pdf->stream('returns.pdf', array("Attachment" => false));
    }

    public function breakToPages($items){
        $arrayCount = count($items);
        $currentPage = 0;
        $rowCount = 0;
        $pages = [];
        $shouldBreak = false;
        for ($i=0; $i<$arrayCount; $i++){
            $rowCount += ceil((float)(iconv_strlen($items[$i]->item_name, 'UTF-8')/68));
            if ($currentPage==0){
                if ($rowCount>34){
                    $shouldBreak = true;
                }
            }else{
                if ($rowCount>44){
                    $shouldBreak = true;
                }
            }
            if ($shouldBreak){
                $rowCount = 0;
                $i-=1;
                $currentPage++;
                $shouldBreak = false;
                continue;
            }else{
                $pages[$currentPage][] = $items[$i];
            }
        }
        return $pages;
    }
}

<?php

namespace App\Http\Controllers;

use App\DreamRequest;
use App\DreamStorage;
use App\DreamSupplier;
use App\DreamTransaction;
use Carbon\Carbon;
use Validator;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PDF;

class DreamDefectiveController extends Controller
{

    const SUPER_ADMIN = 1;

    const REQUESTOR = 3;
    const DEFECTIVE_TYPE = 5;
    const PROCESSED = 1;
    const STATE_ACTIVE = 1;
    const OUTPUT = 2;
    const INPUT = 1;
    const DEFECTIVE_INPUT = 1;
    const DEFECTIVE_OUTPUT = 2;
    const WAREHOUSE = 1;
    const LOCAL = 1;
    const OTHER_STORAGE_TYPE = 4;

    public function index(){
        return view('index', ['page'=>'all_defectives']);
    }

    public function create_defective(){
        $isAdmin = false;
        if (DB::table('user_role')->where('user_id', Auth::user()->id)->get()->first()->role_id==self::SUPER_ADMIN){
            $storages = DreamStorage::all();
            $isAdmin = true;
        }else{
            if(DB::table('user_role')->where('user_id', Auth::user()->id)->get()->first()->role_id==self::REQUESTOR){
                $storages = DreamStorage::all();
            }else{
                $storages = DB::table('dream_storages')
                    ->join('dream_storage_user_bind', 'dream_storage_user_bind.storage_id', '=', 'dream_storages.id')
                    ->select('dream_storages.*')->where([['dream_storage_user_bind.user_id', Auth::user()->id]])
                    ->whereNotIn('dream_storages.type_id', array(self::DEFECTIVE_TYPE))->get();
            }
        }
        $defective_storages = DreamStorage::where('type_id', self::DEFECTIVE_TYPE)->get();
        return view('index', ['page'=>'create_defective', 'storages'=>$storages, 'defective_storages'=>$defective_storages]);
    }

    public function getStorageDefectiveItems(Request $request){
        $storage_id = $request->from_storage_id;
        $items = DB::table('dream_transaction_list')
            ->join('dream_items', 'dream_items.id', '=', 'dream_transaction_list.item_id')
            ->join('dream_units', 'dream_units.id', '=',  'dream_transaction_list.unit_id')
            ->join('dream_transaction_storage_bind', 'dream_transaction_storage_bind.transaction_id', '=', 'dream_transaction_list.transaction_id')
            ->join('dream_transactions', 'dream_transactions.id', '=', 'dream_transaction_list.transaction_id')
            ->join('dream_transaction_types', 'dream_transaction_types.id', '=', 'dream_transaction_storage_bind.operation_id')
            ->where([['dream_transaction_storage_bind.storage_id', $storage_id]])->whereNull('dream_transaction_list.delete_time')
            ->select('dream_items.*', DB::raw('sum(dream_transaction_list.amount * dream_transaction_types.operation) as remaining'),
                'dream_units.id as unit_id', 'dream_units.unit'
            )->groupBy('dream_transaction_list.item_id', 'dream_transaction_list.unit_id')->get();
        return json_encode($items);
    }

    public function collectDefectiveItems(Request $request){
        try{
            $storage_user = DB::table('dream_storage_user_bind')->where('user_id', Auth::user()->id)->get();

            $arr = [];
            if (!is_null($storage_user) || DB::table('user_role')->where('user_id', Auth::user()->id)->get()->first()->role_id==self::SUPER_ADMIN || DB::table('user_role')
                    ->where('user_id', Auth::user()->id)->get()->first()->role_id==self::REQUESTOR ){
                $storage_id = $request->from_storage_id;
                $item_id = $request->item_id;
                $amount = $request->amount;
                $item = DB::table('dream_transaction_list')
                    ->join('dream_items', 'dream_items.id', '=', 'dream_transaction_list.item_id')
                    ->join('dream_units', 'dream_units.id', '=',  'dream_transaction_list.unit_id')
                    ->join('dream_transaction_storage_bind', 'dream_transaction_storage_bind.transaction_id', '=', 'dream_transaction_list.transaction_id')
                    ->join('dream_transactions', 'dream_transactions.id', '=', 'dream_transaction_list.transaction_id')
                    ->join('dream_transaction_types', 'dream_transaction_types.id', '=', 'dream_transaction_storage_bind.operation_id')
                    ->where([['dream_transaction_storage_bind.storage_id', $storage_id], ['dream_transaction_list.item_id', $item_id], ['dream_transactions.create_time', '<=', Carbon::now()]])
                    ->whereNull('dream_transaction_list.delete_time')
                    ->select('dream_items.*', DB::raw('sum(dream_transaction_list.amount * dream_transaction_types.operation) as remaining'),
                    'dream_units.id as unit_id', 'dream_units.unit'
                    )->groupBy('dream_transaction_list.item_id', 'dream_transaction_list.unit_id')->first();
                if (!empty($item)){
                    $data = [
                        'articula_new'=>$item->articula_new,
                        'sap_code'=>$item->sap_code,
                        'item_name'=>$item->item_name,
                        'unit'=>$item->unit,
                        'unit_id'=>$item->unit_id,
                        'storage_id'=>$storage_id,
                        'amount'=>$amount,
                        'remaining'=>round($item->remaining, 4),
                        'reason'=>'',
                        'item_id'=>$item->id
                    ];
                    array_push($arr, $data);
                }
            }else{
                return response()->json(['errors'=>array('у вас нет доступа!')], 500);
            }
            return json_encode($arr);
        }catch(\Exception $e){
            return response()->json(['errors'=>'невозможно добавить!', $e], 500);
        }
    }

    public function saveDefectiveItems(Request $request){
        $storage_user = DB::table('dream_storage_user_bind')->where('user_id', Auth::user()->id)->get();
        $validateNames = array(
            'items'=>'элементы',
            'description'=>'примечание',
            'from_storage_id'=>'отправитель',
            'to_storage_id'=>'получатель',
        );
        $validator = Validator::make($request->all(), [
            'items' => 'required',
            'from_storage_id' => 'required',
            'to_storage_id' => 'required'
        ]);
        $validator->setAttributeNames($validateNames);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()], 500);
        }

        try{
            if (!is_null($storage_user) || DB::table('user_role')->where('user_id', Auth::user()->id)->get()->first()->role_id==self::SUPER_ADMIN || DB::table('user_role')
                    ->where('user_id', Auth::user()->id)->get()->first()->role_id==self::REQUESTOR ){
                DB::transaction(function () use($request){

                    $request_items = json_decode($request->items);
                    $data = [
                        'to_storage_id' => $request->to_storage_id,
                        'from_storage_id' => $request->from_storage_id,
                        'user_id' => Auth::user()->id,
                        'description' => $request->description,
                        'type_id' => 2
                    ];

                    $tr_req_id = DreamRequest::create($data)->id;

                    foreach ($request_items as $item){
                        $item_data = [
                            'request_id' => $tr_req_id,
                            'item_id' => $item->item_id,
                            'amount' => (float)$item->amount,
                            'unit_id' => $item->unit_id
                        ];

                        DB::table('dream_request_list')->insert($item_data);
                    }
                });
                return json_encode($request->except('storage_name'));
            }else{
                return response()->json(['errors'=>array('у вас нет доступа!')], 500);
            }

        }catch (\Exception $e){
            return response()->json(['error'=>array($e)], 500);
        }
    }

    public function get_defectives(Request $request){
        DB::table('dream_requests')
            ->leftJoin('dream_request_list', 'dream_request_list.request_id', '=', 'dream_requests.id')
            ->select('dream_request_list.request_id', 'dream_requests.id')
            ->whereNull('dream_request_list.request_id')
            ->groupBy('dream_requests.id')->delete();

        $columns = array(
            0 => 'id',
            1 => 'state_name',
            2 => 'create_time',
            3 => 'from_storage_name',
            4 => 'to_storage_name',
            5 => 'description',
            6 => 'user_name'
        );
        $filterColumns = array(
            0 => 'dream_requests.id',
            1 => 'dream_request_states.name',
            2 => 'dream_requests.create_time',
            3 => 'from_storage.name',
            4 => 'to_storage.name',
            5 => 'dream_requests.description',
            6 => 'users.name'
        );
        $totalData = DreamRequest::count();
        if ($request->input('length')==-1){
            $limit = $totalData;
        }else{
            $limit = $request->input('length');
        }
        $filters = [];
        $str = '';

        foreach ($request->columns as $key=>$column){
            if ($column['search']['value']!=null){
                if ($column['search']['value']=='пустой'){
                    $filters = array_add($filters, $key, 'пустой');
                    $str.=$filterColumns[$key]." is null and ";
                }else{
                    $filters = array_add($filters, $key, stripslashes($column['search']['value']));
                    $str.=$filterColumns[$key]."='".$column['search']['value']."' and ";
                }
            }
        }
        if (!empty($str)>0){
            $str = substr($str, 0, strlen($str)-4);
        }
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if (count($filters)==0){
            if (empty($request->input('search.value'))){
                $items = DB::table('dream_requests')
                    ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                    ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                    ->join('users', 'users.id', '=', 'dream_requests.user_id')
                    ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                    ->select('dream_requests.*', 'to_storage.name as to_storage_name', 'from_storage.name as from_storage_name', 'users.firstname as user_name',
                        'dream_request_states.name as state_name', 'dream_request_states.id as state_id'
                    )->where('dream_requests.type_id', 2)
                    ->offset($start)->limit($limit)->orderBy($order, $dir)->get();
                $filteredData = DB::table('dream_requests')
                    ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                    ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                    ->join('users', 'users.id', '=', 'dream_requests.user_id')
                    ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                    ->select('dream_requests.*', 'to_storage.name as to_storage_name', DB::raw('IFNULL(from_storage.name, " ") as from_storage_name'), 'users.firstname as user_name',
                        'dream_request_states.name as state_name', 'dream_request_states.id as state_id'
                    )->where('dream_requests.type_id', 2)
                    ->get();

                $totalFiltered = count($filteredData);
            }else{
                $search = $request->input('search.value');
                $items = DB::table('dream_requests')
                    ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                    ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                    ->join('users', 'users.id', '=', 'dream_requests.user_id')
                    ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                    ->select('dream_requests.*', 'to_storage.name as to_storage_name', 'from_storage.name as from_storage_name', 'users.firstname as user_name',
                        'dream_request_states.name as state_name'
                    )
                    ->where(function ($query) use ($search){
                        $query->orWhere('dream_requests.description', 'LIKE', "%$search%")->orWhere('to_storage.name', 'LIKE',  "%$search%")->orWhere('from_storage.name', 'LIKE',  "%$search%")
                            ->orWhere('users.firstname', 'LIKE',  "%$search%")->orWhere('dream_request_states.name', 'LIKE',  "%$search%")->orWhere('dream_requests.create_time', 'LIKE', "%$search%")
                            ->orWhere('dream_requests.id', 'LIKE', "%$search%");
                    })->where('dream_requests.type_id', 2)
                    ->offset($start)->limit($limit)->orderBy($order, $dir)->get();

                $filteredData = DB::table('dream_requests')
                    ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                    ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                    ->join('users', 'users.id', '=', 'dream_requests.user_id')
                    ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                    ->where(function ($query) use ($search){
                        $query->orWhere('dream_requests.description', 'LIKE', "%$search%")->orWhere('to_storage.name', 'LIKE',  "%$search%")->orWhere('from_storage.name', 'LIKE',  "%$search%")
                            ->orWhere('users.firstname', 'LIKE',  "%$search%")->orWhere('dream_request_states.name', 'LIKE',  "%$search%")->orWhere('dream_requests.create_time', 'LIKE', "%$search%")
                            ->orWhere('dream_requests.id', 'LIKE', "%$search%");
                    })->where('dream_requests.type_id', 2)
                    ->select('dream_requests.*', 'to_storage.name as to_storage_name', DB::raw('IFNULL(from_storage.name, " ") as from_storage_name'), 'users.firstname as user_name',
                        'dream_request_states.name as state_name', 'dream_request_states.id as state_id')
                    ->orderBy($order, $dir)->get();
                $totalFiltered = count($filteredData);

            }
        }else{
            if (empty($request->input('search.value'))){
                $items = DB::table('dream_requests')
                    ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                    ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                    ->join('users', 'users.id', '=', 'dream_requests.user_id')
                    ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                    ->select('dream_requests.*', 'to_storage.name as to_storage_name', 'from_storage.name as from_storage_name', 'users.firstname as user_name',
                        'dream_request_states.name as state_name', 'dream_request_states.id as state_id'
                    )->where('dream_requests.type_id', 2)
                    ->whereRaw($str)
                    ->offset($start)->limit($limit)->orderBy($order, $dir)->get();

                $filteredData = DB::table('dream_requests')
                    ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                    ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                    ->join('users', 'users.id', '=', 'dream_requests.user_id')
                    ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')->where('dream_requests.type_id', 2)
                    ->whereRaw($str)
                    ->select('dream_requests.id', 'dream_request_states.name as state_name', 'to_storage.name as to_storage_name', DB::raw('IFNULL(from_storage.name, " ") as from_storage_name'),
                        'dream_requests.create_time', 'users.firstname as user_name')
                    ->get();
                $totalFiltered = count($filteredData);

            }else{
                $search = $request->input('search.value');
                $items = DB::table('dream_requests')
                    ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                    ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                    ->join('users', 'users.id', '=', 'dream_requests.user_id')
                    ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                    ->select('dream_requests.*', 'to_storage.name as to_storage_name', 'from_storage.name as from_storage_name', 'users.firstname as user_name',
                        'dream_request_states.name as state_name'
                    )
                    ->where(function ($query) use ($search){
                        $query->orWhere('dream_requests.description', 'LIKE', "%$search%")->orWhere('to_storage.name', 'LIKE',  "%$search%")->orWhere('from_storage.name', 'LIKE',  "%$search%")
                            ->orWhere('users.firstname', 'LIKE',  "%$search%")->orWhere('dream_request_states.name', 'LIKE',  "%$search%")->orWhere('dream_requests.create_time', 'LIKE', "%$search%")
                            ->orWhere('dream_requests.id', 'LIKE', "%$search%");
                    })->where('dream_requests.type_id', 2)
                    ->whereRaw($str)
                    ->offset($start)->limit($limit)->orderBy($order, $dir)->get();

                $filteredData = DB::table('dream_requests')
                    ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                    ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                    ->join('users', 'users.id', '=', 'dream_requests.user_id')
                    ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                    ->where(function ($query) use ($search){
                        $query->orWhere('dream_requests.description', 'LIKE', "%$search%")->orWhere('to_storage.name', 'LIKE',  "%$search%")->orWhere('from_storage.name', 'LIKE',  "%$search%")
                            ->orWhere('users.firstname', 'LIKE',  "%$search%")->orWhere('dream_request_states.name', 'LIKE',  "%$search%")->orWhere('dream_requests.create_time', 'LIKE', "%$search%")
                            ->orWhere('dream_requests.id', 'LIKE', "%$search%");
                    })->where('dream_requests.type_id', 2)
                    ->whereRaw($str)
                    ->select('dream_requests.id', 'dream_request_states.name as state_name', 'to_storage.name as to_storage_name', DB::raw('IFNULL(from_storage.name, " ") as from_storage_name'),
                        'dream_requests.create_time', 'users.firstname as user_name')
                    ->orderBy($order, $dir)->get();
                $totalFiltered = count($filteredData);
            }
        }
        $data = array();
        $tr = [];
        foreach ($columns as $key=>$column){
            if ($column=='from_storage_name'){
                $storage = DB::table('dream_storages')->whereIn('type_id', array(1,2,3))->select('name as name')->get()->toArray();
                $tr = array_add($tr, $key, $storage);

            }
            else if ($column=='state_name'){
                $states = DB::table('dream_request_states')->select('name as name')->get()->toArray();
                $tr = array_add($tr, $key, $states);
            }
            else if ($column=='to_storage_name'){
                $from_dest = DB::table('dream_storages')->whereIn('type_id', array(5))->select('name as name')->get()->toArray();
                $tr = array_add($tr, $key, $from_dest);
            }
            else if ($column=='user_name'){
                $username = DB::table('users')
                    ->join('dream_requests', 'dream_requests.user_id', '=', 'users.id')
                    ->select('users.*')->groupBy('users.id')->get()->toArray();
                $tr = array_add($tr, $key, $username);
            }
        }
        if (!empty($items)){
            foreach ($items as $item)
            {
                $nestedData['DT_RowId'] = $item->id;
                $nestedData['id'] = $item->id;
                $nestedData['state'] = ($item->state==self::PROCESSED)?"<span class='label label-primary' style='font-size: 14px; padding-top: 0;'>".$item->state_name."</span>":"<span class='label label-danger' style='font-size: 14px; padding-top: 0;'>".$item->state_name."</span>";
                $nestedData['to_storage'] = $item->to_storage_name;
                $nestedData['from_storage'] = $item->from_storage_name;
                $nestedData['to_storage'] = $item->to_storage_name;
                $nestedData['create_time'] = Carbon::createFromFormat('Y-m-d H:i:s', $item->create_time)->format('Y-m-d H:i');
                $nestedData['applier'] = $item->user_name;
                $nestedData['description'] = $item->description;
                $nestedData['detail'] = "<button type='button' data-toggle='modal' data-backdrop='static' data-keyboard='false'  data-target='#show_detail' class='btn btn-info ' onclick='show_detail(".$item->id.")'><i class='fa fa-info-circle'></i></button>";
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
            "filteredData"    => $tr,
            "filters"         => $filters
        );

        return json_encode($json_data);
    }

    public function get_defective_list(Request $request){
        $request_id = $request->defective_id;
        $data = array();
        $total = 0;
        $request_data = DreamRequest::find($request_id);
        $allowToAccept = false;

        $items = DB::table('dream_request_list')
            ->leftJoin('dream_transaction_request_bind', 'dream_transaction_request_bind.request_list_id', '=', 'dream_request_list.id')
            ->leftJoin('dream_transaction_list', 'dream_transaction_list.id', '=', 'dream_transaction_request_bind.transaction_list_id')
            ->join('dream_items', 'dream_items.id', '=', 'dream_request_list.item_id')
            ->join('dream_units', 'dream_units.id', '=', 'dream_request_list.unit_id')
            ->select('dream_request_list.amount as request_amount', 'dream_items.id as item_id', 'dream_items.articula_new', 'dream_items.articula_old', 'dream_items.item_name',
                'dream_items.sap_code', 'dream_units.unit', 'dream_request_list.id', DB::raw('sum(dream_transaction_list.amount) as accepted_amount'))
            ->groupBy('dream_items.id', 'dream_units.id', 'dream_request_list.id')
            ->where([['dream_request_list.request_id', $request_id], ['dream_request_list.delete_time', null]])->get();

        if (DreamRequest::find($request_id)->user_id==Auth::user()->id){
            $hasPerm = true;
        }else{
            $hasPerm = false;
        }
        if (!empty($items)){
            foreach ($items as $item)
            {
                $total += round($item->request_amount, 3);
                $remaining = $this->get_item_remaining($request_data->from_storage_id, $item->item_id);
                $nestedData['DT_RowId'] = $item->id;
                $nestedData['articula_new'] = $item->articula_new;
                $nestedData['articula_old'] = $item->articula_old;
                $nestedData['sap_code'] = $item->sap_code;
                $nestedData['item_name'] = $item->item_name;
                $nestedData['amount'] = round($item->request_amount, 3);
                if ($remaining>=round($item->request_amount, 3) || $request_data->state!=self::STATE_ACTIVE){
                    $nestedData['remaining'] = $remaining;
                }else{
                    $nestedData['remaining'] = '<p style="display: block; background-color: red; color: white; padding: 0; margin: 0">'.$remaining.'</p>';
                }
                $nestedData['remaining_num'] = $remaining;
                $nestedData['unit'] = $item->unit;
                if ($request_data->state==self::STATE_ACTIVE && ($request_data->user_id==Auth::user()->id)){
                    $nestedData['edit_btn'] = "<button type='button' class='btn btn-warning' data-toggle='modal' data-backdrop='static' data-keyboard='false' data-target='#edit_defective_item_modal' onclick=\"set_data_for_edit_defective_item('".$item->id."', '".round($item->request_amount, 3)."')\"><i class='fa fa-edit'></i></button> <button type='button' onclick=\"remove_defective_item($item->id)\" class='btn btn-danger'><i class='fa fa-trash'></i></button>";
                }else{
                    $nestedData['edit_btn'] = "";
                }
                $nestedData['accepted_amount'] = $item->accepted_amount;
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "data" => $data,
            "hasPerm" => $hasPerm,
            "requestData" => $request_data,
            "total" => $total,
            "allowToAccept" => $allowToAccept
        );
        return json_encode($json_data);
    }

    public function get_item_remaining($storage_id, $item_id)
    {
        $total = DB::select("select sum(operation*amount) as total from dream_transaction_list ".
            "left join dream_transaction_storage_bind on dream_transaction_list.transaction_id=dream_transaction_storage_bind.transaction_id and dream_transaction_storage_bind.storage_id=$storage_id " .
            "left join dream_transaction_types on dream_transaction_types.id=dream_transaction_storage_bind.operation_id where item_id=$item_id and dream_transaction_list.delete_time is null")[0];
        if ($total->total == null){
            $total->total = 0;
        }
        return round($total->total, 3);
    }

    public function update_defective_list(Request $request){
        try{
            $list_id = $request->defective_list_id;
            $amount = $request->amount;
            if (DB::table('dream_transaction_request_bind')->where('request_list_id', $list_id)->get()->count()==0){
                DB::table('dream_request_list')->where('id', $list_id)->update(['amount'=>$amount]);
            }else{
                return response()->json(['errors'=>array('невозможно изменить')], 500);
            }
            return json_encode($list_id);
        }catch (\Exception $exception){
            return response()->json(['errors'=>array('невозможно изменить')], 500);
        }
    }

    public function remove_defective_list(Request $request){
        try{
            $list_id = $request->defective_list_id;
            $request_list = DB::table('dream_request_list')->get();
            if (DB::table('dream_transaction_request_bind')->where('request_list_id', $list_id)->get()->count()==0){
                DB::table('dream_request_list')->where('id', $list_id)->delete();
            }else{
                return response()->json(['errors'=>array('невозможно изменить')], 500);
            }
            if (count($request_list)==0){
                DreamRequest::find($request_list->first()->request_id)->update(['state'=>2]);
            }
            return json_encode($list_id);
        }catch (\Exception $exception){
            return response()->json(['errors'=>array('невозможно удалить')], 500);
        }
    }

    public function accept_defective_items(Request $request){
        try{
            $res = DB::transaction(function () use($request){
                $request_id = $request->defective_id;
                $requestData = DreamRequest::find($request_id);
                if ($requestData->state==2){
                    return response()->json(['errors'=>array('закрыт')], 500);
                }
                $request_list_item = DB::table('dream_request_list')->where('dream_request_list.request_id', $request_id)->get();
                $transaction = [
                    'user_id' => Auth::user()->id,
                    'description' => $requestData->description
                ];
                $transaction_id = DreamTransaction::create($transaction)->id;
                $output_from_storage = [
                    'transaction_id'=>$transaction_id,
                    'storage_id'=>$requestData->from_storage_id,
                    'operation_id'=>self::DEFECTIVE_OUTPUT
                ];

                DB::table('dream_transaction_storage_bind')->insert($output_from_storage);

                if ($requestData->from_storage_id!=null){
                    $input_to_defective_storage = [
                        'transaction_id'=>$transaction_id,
                        'storage_id'=>$requestData->to_storage_id,
                        'operation_id'=>self::DEFECTIVE_INPUT
                    ];
                    DB::table('dream_transaction_storage_bind')->insert($input_to_defective_storage);
                }

                foreach ($request_list_item as $item){
                    $data = [
                        'transaction_id' => $transaction_id,
                        'item_id' => $item->item_id,
                        'unit_id' => $item->unit_id,
                        'amount' => $item->amount
                    ];
                    $transaction_list_id = DB::table('dream_transaction_list')->insertGetId($data);
                    DB::table('dream_transaction_request_bind')->insert(['transaction_list_id'=>$transaction_list_id, 'request_list_id'=>$item->id]);
                }

                DB::table('dream_requests')->where('id', $request_id)->update(['state'=>2]);
                return $transaction;
            });
            return json_encode($res);
        }catch (\Exception $e){
            return response()->json(['errors'=>array($e)], 500);
        }
    }

    public function defectiveInputOutputOperation(){
        if (DB::table('user_role')->where('user_id', Auth::user()->id)->get()->first()->role_id==self::SUPER_ADMIN){
            $storages = DreamStorage::where('type_id', self::DEFECTIVE_TYPE)->get();
        }else{
            $storages = DB::table('dream_storages')
                ->join('dream_storage_user_bind', 'dream_storage_user_bind.storage_id', '=', 'dream_storages.id')
                ->select('dream_storages.*')->where([['dream_storage_user_bind.user_id', Auth::user()->id], ['dream_storages.type_id', self::DEFECTIVE_TYPE]])->get();
        }
        $suppliers = DreamSupplier::where([['type', self::LOCAL], ['is_active', self::STATE_ACTIVE]])->get();
        return view('index', ['page'=>'defective_input_output_operation', 'storages'=>$storages, 'suppliers'=>$suppliers]);
    }

    public function getDefectiveStorageRemainingItems(Request $request){
        $storage_id = $request->storage_id;
        $supplier_id = $request->supplier_id;
        if ($supplier_id==0 || is_null($supplier_id)){
            $items = DB::table('dream_transaction_list')
                ->join('dream_transaction_storage_bind', 'dream_transaction_storage_bind.transaction_id', '=', 'dream_transaction_list.transaction_id')
                ->join('dream_items', 'dream_items.id', '=', 'dream_transaction_list.item_id')
                ->where('dream_transaction_storage_bind.storage_id', $storage_id)
                ->select('dream_items.*')->groupBy('dream_items.id')->get();
        }else{
            $items = DB::table('dream_transaction_list')
                ->join('dream_transaction_storage_bind', 'dream_transaction_storage_bind.transaction_id', '=', 'dream_transaction_list.transaction_id')
                ->join('dream_items', 'dream_items.id', '=', 'dream_transaction_list.item_id')
                ->join('dream_supplier_item_bind', 'dream_supplier_item_bind.item_id', '=', 'dream_items.id')
                ->where([['dream_transaction_storage_bind.storage_id', $storage_id], ['dream_supplier_item_bind.supplier_id', $supplier_id]])
                ->select('dream_items.*')->groupBy('dream_items.id')->get();
        }

        return json_encode($items);
    }

    public function collect_defective_item_input_output(Request $request){
        $validateNames = array(
            'items'=>'элементы'
        );
        try{
            $storage_user = DB::table('dream_storage_user_bind')->where('user_id', Auth::user()->id)->get();
            if (Auth::user()->roles->first()->id!=self::SUPER_ADMIN){
                if (is_null($storage_user)){
                    return response()->json(['errors'=>array('у вас нет доступа!')], 500);
                }
            }

            $arr = [];
            if (!is_null($storage_user) || Auth::user()->roles->first()->id){
                $storage_id = $request->storage_id;
                $item_id = $request->item_id;
                $amount = $request->amount;
                $item = DB::table('dream_items')
                    ->join('dream_storage_item_bind', 'dream_storage_item_bind.item_id', '=', 'dream_items.id')
                    ->join('dream_units', 'dream_units.id', '=', 'dream_storage_item_bind.unit_id')
                    ->where([['dream_storage_item_bind.item_id', $item_id]])->get()->first();
                if (!is_null($item)){
                    $data = [
                        'articula_new'=>$item->articula_new,
                        'sap_code'=>$item->sap_code,
                        'item_name'=>$item->item_name,
                        'unit'=>$item->unit,
                        'unit_id'=>$item->unit_id,
                        'storage_id'=>$storage_id,
                        'amount'=>$amount,
                        'remaining'=>$this->get_item_remaining($storage_id, $item->item_id),
                        'item_id'=>$item->item_id
                    ];
                    array_push($arr, $data);
                }
            }

            return json_encode($arr);
        }catch(\Exception $e){
            return response()->json(['errors'=>'невозможно добавить!', $e], 500);
        }
    }

    public function save_defective_inputs_outputs_items(Request $request){
        $validateNames = array(
            'items'=>'элементы'
        );
        $storage_user = DB::table('dream_storage_user_bind')->where('user_id', Auth::user()->id)->get();
        if (Auth::user()->roles->first()->id!=self::SUPER_ADMIN){
            if (is_null($storage_user)){
                return response()->json(['errors'=>array('у вас нет доступа!')], 500);
            }
            if (Gate::denies('allow_input_output_operation', new DreamTransaction)){
                return response()->json(['errors'=>array('у вас нет доступа!')], 500);
            }
        }
        $operation_type = DB::table('dream_transaction_types')->where('id', $request->operation_id)->get()->first();
        if (is_null($operation_type)){
            return response()->json(['errors'=>array('невозможно добавить!')], 500);
        }
        $validator = Validator::make($request->all(), [
            'items' => 'required'
        ]);
        $validator->setAttributeNames($validateNames);

        if ($operation_type->operation<0){
            if ($request->supplier_id==0 && is_null($request->description)){
                return response()->json(['errors'=>array('выберите поставщика или напишите примечание!')], 500);
            }
        }
        if ($validator->fails()) {
            return response()->json(['errors'=>array($validator->errors()->all())], 500);
        }
        try{
            $items = DB::transaction(function () use($request, $storage_user, $operation_type){
                $items = json_decode($request->items);
                $desc = $request->description;
                $user_id = Auth::user()->id;

                $storage_id = $request->storage_id;
                $operation_id = $request->operation_id;
                $transaction_id = $this->create_transaction($items, $desc, $user_id);
                $store_data = [
                    'transaction_id'=>$transaction_id,
                    'storage_id'=>$storage_id,
                    'operation_id'=>$operation_id
                ];
                DB::table('dream_transaction_storage_bind')->insert(array($store_data));
                return $items;
            });
            return json_encode($items);
        }
        catch (\Exception $e){
            return response()->json(['errors'=>array('ошибка при выпольнении транзакции!', $e)], 500);
        }
    }

    public static function create_transaction($items, $description, $user_id){
        $transaction = [
            'user_id' => $user_id,
            'description' => $description
        ];
        $transaction_id = DreamTransaction::create($transaction)->id;

        foreach ($items as $item){
            $transaction_list_data = [
                'item_id' => $item->item_id,
                'transaction_id' => $transaction_id,
                'amount' => $item->amount,
                'unit_id' => $item->unit_id
            ];

            DB::table('dream_transaction_list')->insert($transaction_list_data);
        }
        return $transaction_id;
    }

    public function getDefectivesForAccept(Request $request){
        $storage_id = $request->storage_id;

        $columns = array(
            0 => 'id',
            1 => 'user_name',
            2 => 'create_time',
            3 => 'from_storage_name',
            4 => 'description',
        );
        $filterColumns = array(
            0 => 'dream_requests.id',
            1 => 'users.firstname',
            2 => 'dream_requests.create_time',
            3 => 'from_storage.name',
            4 => 'dream_requests.description'
        );
        $totalData = DreamRequest::count();
        if ($request->input('length')==-1){
            $limit = $totalData;
        }else{
            $limit = $request->input('length');
        }
        $filters = [];
        $str = '';
        foreach ($request->columns as $key=>$column){
            if ($column['search']['value']!=null){
                $filters = array_add($filters, $key, $column['search']['value']);
                $str.=$filterColumns[$key]."='".$column['search']['value']."' and ";
            }
        }
        if (!empty($str)>0){
            $str = substr($str, 0, strlen($str)-4);
        }
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if (count($filters)==0){
            if (empty($request->input('search.value'))){
                $items = DB::table('dream_requests')
                    ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                    ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                    ->join('users', 'users.id', '=', 'dream_requests.user_id')
                    ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                    ->select('dream_requests.*', 'from_storage.name as from_storage_name', 'to_storage.name as to_storage_name', 'users.firstname as user_name',
                        'dream_request_states.name as state_name', 'dream_request_states.id as state_id'
                    )
                    ->where([['dream_requests.to_storage_id', $storage_id], ['dream_requests.state', self::STATE_ACTIVE], ['dream_requests.type_id', 2]])
                    ->offset($start)->limit($limit)->orderBy($order, $dir)->get();

                $filteredData = DB::table('dream_requests')
                    ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                    ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                    ->join('users', 'users.id', '=', 'dream_requests.user_id')
                    ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                    ->select('dream_requests.*', 'from_storage.name as from_storage_name',
                        DB::raw('IFNULL(to_storage.name, " ") as to_storage_name'), 'users.firstname as user_name',
                        'dream_request_states.name as state_name', 'dream_request_states.id as state_id')
                    ->where([['dream_requests.to_storage_id', $storage_id], ['dream_requests.state', self::STATE_ACTIVE], ['dream_requests.type_id', 2]])
                    ->get();

                $totalFiltered = count($filteredData);
            }else{
                $search = $request->input('search.value');
                $items = DB::table('dream_requests')
                    ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                    ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                    ->join('users', 'users.id', '=', 'dream_requests.user_id')
                    ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                    ->select('dream_requests.*', 'from_storage.name as from_storage_name', 'to_storage.name as to_storage_name', 'users.firstname as user_name',
                        'dream_request_states.name as state_name'
                    )
                    ->where(function ($query) use ($search){
                        $query->orWhere('dream_requests.description', 'LIKE', "%$search%")->orWhere('from_storage.name', 'LIKE',  "%$search%")->orWhere('to_storage.name', 'LIKE',  "%$search%")
                            ->orWhere('users.firstname', 'LIKE',  "%$search%")->orWhere('dream_request_states.name', 'LIKE',  "%$search%")->orWhere('dream_requests.create_time', 'LIKE', "%$search%")
                            ->orWhere('dream_requests.id', 'LIKE', "%$search%");
                    })
                    ->where([['dream_requests.to_storage_id', $storage_id], ['dream_requests.state', self::STATE_ACTIVE], ['dream_requests.type_id', 2]])
                    ->offset($start)->limit($limit)->orderBy($order, $dir)->get();

                $filteredData = DB::table('dream_requests')
                    ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                    ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                    ->join('users', 'users.id', '=', 'dream_requests.user_id')
                    ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                    ->where(function ($query) use ($search){
                        $query->orWhere('dream_requests.description', 'LIKE', "%$search%")->orWhere('from_storage.name', 'LIKE',  "%$search%")->orWhere('to_storage.name', 'LIKE',  "%$search%")
                            ->orWhere('users.firstname', 'LIKE',  "%$search%")->orWhere('dream_request_states.name', 'LIKE',  "%$search%")->orWhere('dream_requests.create_time', 'LIKE', "%$search%")
                            ->orWhere('dream_requests.id', 'LIKE', "%$search%");
                    })
                    ->where([['dream_requests.to_storage_id', $storage_id], ['dream_requests.state', self::STATE_ACTIVE], ['dream_requests.type_id', 2]])
                    ->select('dream_requests.*', 'from_storage.name as from_storage_name', DB::raw('IFNULL(to_storage.name, " ") as to_storage_name'), 'users.firstname as user_name',
                        'dream_request_states.name as state_name', 'dream_request_states.id as state_id')
                    ->orderBy($order, $dir)->get();
                $totalFiltered = count($filteredData);

            }
        }else{
            if (empty($request->input('search.value'))){
                $items = DB::table('dream_requests')
                    ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                    ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                    ->join('users', 'users.id', '=', 'dream_requests.user_id')
                    ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                    ->select('dream_requests.*', 'from_storage.name as from_storage_name', 'to_storage.name as to_storage_name', 'users.firstname as user_name',
                        'dream_request_states.name as state_name', 'dream_request_states.id as state_id'
                    )
                    ->whereRaw($str)
                    ->where([['dream_requests.to_storage_id', $storage_id], ['dream_requests.state', self::STATE_ACTIVE], ['dream_requests.type_id', 2]])
                    ->offset($start)->limit($limit)->orderBy($order, $dir)->get();

                $filteredData = DB::table('dream_requests')
                    ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                    ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                    ->join('users', 'users.id', '=', 'dream_requests.user_id')
                    ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                    ->whereRaw($str)
                    ->where([['dream_requests.to_storage_id', $storage_id], ['dream_requests.state', self::STATE_ACTIVE], ['dream_requests.type_id', 2]])
                    ->select('dream_requests.id', 'dream_request_states.name as state_name', 'from_storage.name as from_storage_name', DB::raw('IFNULL(to_storage.name, " ") as to_storage_name'),
                        'dream_requests.create_time', 'users.firstname as user_name')
                    ->get();
                $totalFiltered = count($filteredData);

            }else{
                $search = $request->input('search.value');
                $items = DB::table('dream_requests')
                    ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                    ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                    ->join('users', 'users.id', '=', 'dream_requests.user_id')
                    ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                    ->select('dream_requests.*', 'from_storage.name as from_storage_name', 'to_storage.name as to_storage_name', 'users.firstname as user_name',
                        'dream_request_states.name as state_name'
                    )
                    ->where(function ($query) use ($search){
                        $query->orWhere('dream_requests.description', 'LIKE', "%$search%")->orWhere('from_storage.name', 'LIKE',  "%$search%")->orWhere('to_storage.name', 'LIKE',  "%$search%")
                            ->orWhere('users.firstname', 'LIKE',  "%$search%")->orWhere('dream_request_states.name', 'LIKE',  "%$search%")->orWhere('dream_requests.create_time', 'LIKE', "%$search%")
                            ->orWhere('dream_requests.id', 'LIKE', "%$search%");
                    })
                    ->whereRaw($str)
                    ->where([['dream_requests.to_storage_id', $storage_id], ['dream_requests.state', self::STATE_ACTIVE], ['dream_requests.type_id', 2]])
                    ->offset($start)->limit($limit)->orderBy($order, $dir)->get();

                $filteredData = DB::table('dream_requests')
                    ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                    ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                    ->join('users', 'users.id', '=', 'dream_requests.user_id')
                    ->join('dream_request_states', 'dream_request_states.id', '=', 'dream_requests.state')
                    ->where(function ($query) use ($search){
                        $query->orWhere('dream_requests.description', 'LIKE', "%$search%")->orWhere('from_storage.name', 'LIKE',  "%$search%")->orWhere('to_storage.name', 'LIKE',  "%$search%")
                            ->orWhere('users.firstname', 'LIKE',  "%$search%")->orWhere('dream_request_states.name', 'LIKE',  "%$search%")->orWhere('dream_requests.create_time', 'LIKE', "%$search%")
                            ->orWhere('dream_requests.id', 'LIKE', "%$search%");
                    })
                    ->whereRaw($str)
                    ->where([['dream_requests.to_storage_id', $storage_id], ['dream_requests.state', self::STATE_ACTIVE], ['dream_requests.type_id', 2]])
                    ->select('dream_requests.id', 'dream_request_states.name as state_name', 'from_storage.name as from_storage_name', DB::raw('IFNULL(to_storage.name, " ") as to_storage_name'),
                        'dream_requests.create_time', 'users.firstname as user_name')
                    ->orderBy($order, $dir)->get();
                $totalFiltered = count($filteredData);
            }
        }
        $data = array();
        $tr = [];
        if (!empty($filteredData)){
            foreach ($columns as $key=>$column){
                $tr = array_add($tr, $key, $filteredData->unique($column)->pluck($column)->all());
            }
        }else{
            $tr = [];
        }
        if (!empty($items)){
            foreach ($items as $item)
            {
                $nestedData['DT_RowId'] = $item->id;
                $nestedData['id'] = $item->id;
                $nestedData['from_storage'] = $item->from_storage_name;
                $nestedData['create_time'] = Carbon::createFromFormat('Y-m-d H:i:s', $item->create_time)->format('Y-m-d H:i');
                $nestedData['applier'] = $item->user_name;
                $nestedData['description'] = $item->description;
                $nestedData['detail'] = "<button type='button' data-toggle='modal' data-backdrop='static' data-keyboard='false'  data-target='#show_defective_detail' class='btn btn-info ' onclick='show_defective_detail(".$item->id.")'><i class='fa fa-info-circle'></i></button>";
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data,
            "filteredData"    => $tr,
            "filters"         => $filters
        );

        return json_encode($json_data);
    }

    public function showDefectiveListItemsForAccept(Request $request){
        $request_id = $request->defective_id;
        $data = array();
        $total = 0;
        $request_data = DreamRequest::find($request_id);
        $allowToAccept = true;

        $items = DB::table('dream_request_list')
            ->join('dream_items', 'dream_items.id', '=', 'dream_request_list.item_id')
            ->join('dream_units', 'dream_units.id', '=', 'dream_request_list.unit_id')
            ->select('dream_request_list.amount as request_amount', 'dream_items.id as item_id', 'dream_items.articula_new', 'dream_items.articula_old', 'dream_items.item_name',
                'dream_items.sap_code', 'dream_units.unit', 'dream_request_list.id')
            ->where([['dream_request_list.request_id', $request_id], ['dream_request_list.delete_time', null]])->get();

        if (!empty($items)){
            foreach ($items as $item)
            {
                $total += round($item->request_amount, 3);
                $remaining = $this->get_item_remaining($request_data->from_storage_id, $item->item_id);
                if ($remaining<round($item->request_amount, 3)) {
                    $allowToAccept = false;
                }
                $nestedData['DT_RowId'] = $item->id;
                $nestedData['articula_new'] = $item->articula_new;
                $nestedData['articula_old'] = $item->articula_old;
                $nestedData['sap_code'] = $item->sap_code;
                $nestedData['item_name'] = $item->item_name;
                $nestedData['amount'] = round($item->request_amount, 3);
                if ($remaining>=round($item->request_amount, 3)){
                    $nestedData['remaining'] = $remaining;
                }else{
                    $nestedData['remaining'] = '<p style="display: block; background-color: red; color: white; padding: 0; margin: 0">'.$remaining.'</p>';
                }
                $nestedData['remaining_num'] = $remaining;
                $nestedData['unit'] = $item->unit;
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "data" => $data,
            "requestData" => $request_data,
            "total" => $total,
            "allowToAccept" => $allowToAccept
        );
        return json_encode($json_data);
    }

    public function close_defective(Request $request){
        try{
            $request_ = DreamRequest::find(DB::table('dream_request_list')->where('request_id', $request->defective_id)->first()->request_id);

            if ($request_->user_id!=Auth::user()->id){
                return response()->json(['errors'=>array('вы не можете закрыть')], 500);
            }else{
                DB::table('dream_requests')->where('id', $request->defective_id)->update(['state'=>2]);
            }
            return json_encode($request->all());
        }catch (\Exception $exception){
            return response()->json(['errors'=>array($exception)], 500);
        }
    }

    public function defective_invoice_generate($defective_id){
        $managers_of_lines = [
            '18'=>'Хусниддинов С.',
            '19'=>'Алибаев К.',
            '20'=>'Алибаев К.',
            '17'=>'___________________________',
            '6'=>'___________________________',
            '22'=>'Алибаев К.',
            '26'=>'Хужаназаров Ж.',
        ];
        $managers_of_warehouse = [
            '4'=>'Султонов О.',
            '1'=>'Содиков Б.',
            '2'=>'Миртожиев М.',
            '40'=>'Миртожиев М.',
            '8'=>'Мирзахмедов М'
        ];
        $model = DB::table('dream_requests')
            ->leftJoin('dream_items', 'dream_items.id', '=', 'dream_requests.model_id')
            ->leftJoin('dream_storage_item_bind', 'dream_storage_item_bind.item_id', '=', 'dream_requests.model_id')
            ->leftJoin('dream_units', 'dream_units.id', '=', 'dream_storage_item_bind.unit_id')
            ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
            ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
            ->select('dream_items.articula_new', 'dream_items.item_name',  DB::raw("IFNULL(dream_items.sap_code, '') as sap_code"), 'dream_requests.*', 'from_storage.name as from_storage_name',
                'from_storage.id as from_storage_id', 'to_storage.name as to_storage_name', 'to_storage.id as to_storage_id', 'dream_units.unit as unit_name')
            ->where('dream_requests.id', $defective_id)
            ->first();
        $items = DB::table('dream_request_list')
            ->join('dream_items', 'dream_items.id', '=', 'dream_request_list.item_id')
            ->join('dream_units', 'dream_units.id', '=', 'dream_request_list.unit_id')
            ->select('dream_items.articula_new', 'dream_items.item_name', DB::raw("IFNULL(dream_items.sap_code, '') as sap_code"), 'dream_request_list.*', 'dream_units.unit as unit_name')
            ->where([['dream_request_list.request_id', $defective_id], ['dream_request_list.delete_time', null]])
            ->get();
        $pages = $this->breakToPages($items);
        $date = Carbon::createFromFormat('Y-m-d H:i:s', $model->create_time);
        $storage_type = DB::table('dream_storages')->where('dream_storages.id', $model->from_storage_id)->first();
        if ($storage_type->type_id==self::OTHER_STORAGE_TYPE){
            $data = [
                'pages' => $pages,
                'created_date'=>$date->format('d.m.Y'),
                'created_time'=>$date->format('H:i'),
                'production_line'=>$model->from_storage_name,
                'storage_name'=>$model->to_storage_name,
                'request_id'=>$model->id,
                'manager_of_line'=>'________________',
                'manager_of_warehouse'=>(!is_null($model->to_storage_id))?(isset($managers_of_warehouse[$model->to_storage_id]))?$managers_of_warehouse[$model->to_storage_id]:'________________':'________________',
                'isOtherType'=>true
            ];
        }else{
            $data = [
                'pages' => $pages,
                'created_date'=>$date->format('d.m.Y'),
                'created_time'=>$date->format('H:i'),
                'production_line'=>$model->from_storage_name,
                'storage_name'=>$model->to_storage_name,
                'request_id'=>$model->id,
                'manager_of_line'=>(!is_null($model->from_storage_id))?(isset($managers_of_lines[$model->from_storage_id]))?$managers_of_lines[$model->from_storage_id]:'________________':'________________',
                'manager_of_warehouse'=>(!is_null($model->to_storage_id))?(isset($managers_of_warehouse[$model->to_storage_id]))?$managers_of_warehouse[$model->to_storage_id]:'________________':'________________',
                'isOtherType'=>false
            ];
        }

        $pdf = PDF::loadView('pdf.defective', $data);
//        $pdf->setEncryption('1','sherzod',array('copy'));
        return $pdf->stream('returns.pdf', array("Attachment" => false));
    }

    public function breakToPages($items){
        $arrayCount = count($items);
        $currentPage = 0;
        $rowCount = 0;
        $pages = [];
        $shouldBreak = false;
        for ($i=0; $i<$arrayCount; $i++){
            $rowCount += ceil((float)(iconv_strlen($items[$i]->item_name, 'UTF-8')/68));
            if ($currentPage==0){
                if ($rowCount>34){
                    $shouldBreak = true;
                }
            }else{
                if ($rowCount>44){
                    $shouldBreak = true;
                }
            }
            if ($shouldBreak){
                $rowCount = 0;
                $i-=1;
                $currentPage++;
                $shouldBreak = false;
                continue;
            }else{
                $pages[$currentPage][] = $items[$i];
            }
        }
        return $pages;
    }
}

<?php

namespace App\Http\Controllers;

use App\DreamItem;
use App\DreamOrder;
use App\DreamSupplier;
use App\DreamTransaction;
use App\DreamRequest;
use App\DreamUnit;
use App\DreamStorage;
use App\User;
use Carbon\Carbon;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Gate;

class DreamTransactionController extends Controller
{
    const OUTPUT = 2;
    const SUPER_ADMIN = 1;
    const DEFECT = 3;
    const RETURN_ITEM = 4;
    const INPUT = 1;
    const INPUT_INVENTAR = 5;
    const OUTPUT_INVENTAR = 6;
    const STATE_ACTIVE = 1;
    const STATE_INACTIVE = 0;
    const STATE_CLOSED = 2;
    const PRODUCTION_LINE_ID = 2;
    const PRODUCTION = 8;
    const WAREHOUSE = 1;
    const BUFFER_STORAGE = 3;
    const PRODUCTION_LINE_PRODUCT_UNIT = 1;

    public function showTransaction(){
        $storages = DreamStorage::where('type_id', '!=', 7)->get();
        $operations = DB::table('dream_transaction_types')->get();
        $suppliers = DreamStorage::where('type_id', 7)->get();
        $users = DB::table('users')
            ->join('user_role', 'user_role.user_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'user_role.role_id')
            ->select('users.*')
            ->whereIn('roles.id', array(2))
            ->get();
        $users = DB::table('dream_transactions')
            ->join('users', 'users.id', '=', 'dream_transactions.user_id')
            ->select('users.*')->groupBy('users.id')->get();
        return view('index', ['page'=>'transaction', 'storages'=>$storages, 'operations'=>$operations, 'suppliers'=>$suppliers, 'users'=>$users]);
    }

    public function filterOfFilters(Request $request){
        $str = '';
        if (is_null($request->storages)){

        }else{
            $str .='(';
            foreach ($request->storages as $key=>$storage_id){
                $str .= $storage_id.',';
            }
            $str = ' and dream_transaction_storage_bind.storage_id in '.substr($str, 0, strlen($str)-1).')';
        }
        $storages = DB::select("
        select dream_storages.* from dream_transactions
        inner join dream_transaction_storage_bind on dream_transaction_storage_bind.transaction_id = dream_transactions.id
        inner join dream_storages on dream_storages.id = dream_transaction_storage_bind.storage_id
        where dream_transactions.create_time between '$request->start_date 00:00:01' and '$request->end_date 23:59:59' and dream_storages.type_id != 7
        group by dream_storages.id
        ");

        $sender = DB::select("
        select dream_storages.* from dream_transactions
        inner join dream_transaction_storage_bind on dream_transaction_storage_bind.transaction_id = dream_transactions.id
        inner join dream_transaction_types on dream_transaction_types.id = dream_transaction_storage_bind.operation_id
        inner join dream_storages on dream_storages.id = dream_transaction_storage_bind.storage_id
        where dream_transactions.create_time between '$request->start_date 00:00:01' and '$request->end_date 23:59:59' and dream_storages.type_id != 7 and dream_transaction_types.operation = -1
        group by dream_storages.id
        ");

        $acceptor = DB::select("
        select dream_storages.* from dream_transactions
        inner join dream_transaction_storage_bind on dream_transaction_storage_bind.transaction_id = dream_transactions.id
        inner join dream_transaction_types on dream_transaction_types.id = dream_transaction_storage_bind.operation_id
        inner join dream_storages on dream_storages.id = dream_transaction_storage_bind.storage_id
        where dream_transactions.create_time between '$request->start_date 00:00:01' and '$request->end_date 23:59:59' and dream_storages.type_id != 7 and dream_transaction_types.operation = 1
        group by dream_storages.id
        ");

        $operations = DB::select("
        select dream_transaction_types.* from dream_transactions
        inner join dream_transaction_storage_bind on dream_transaction_storage_bind.transaction_id = dream_transactions.id
        inner join dream_transaction_types on dream_transaction_types.id = dream_transaction_storage_bind.operation_id
        where dream_transactions.create_time between '$request->start_date 00:00:01' and '$request->end_date 23:59:59' $str
        group by dream_transaction_types.id
        ");

        $suppliers = DB::select("
        select dream_storages.* from dream_transactions
        inner join dream_transaction_list on dream_transaction_list.transaction_id = dream_transactions.id
        inner join transaction_order_bind on transaction_order_bind.transaction_list_id = dream_transaction_list.id
        inner join dream_order_list on dream_order_list.id = transaction_order_bind.order_list_id
        inner join dream_orders on dream_orders.id = dream_order_list.order_id
        inner join dream_storages on dream_storages.id = dream_orders.supplier_id
        where dream_transactions.create_time between '$request->start_date 00:00:01' and '$request->end_date 23:59:59' and dream_storages.type_id = 7
        group by dream_storages.id
        ");

        $users = DB::select("
        select users.* from dream_transactions
        inner join users on users.id = dream_transactions.user_id
        where dream_transactions.create_time between '$request->start_date 00:00:01' and '$request->end_date 23:59:59'
        group by users.id
        ");

        $data = [
            'storages' => $storages,
            'sender' => $sender,
            'acceptor' => $acceptor,
            'operations' => $operations,
            'suppliers' => $suppliers,
            'users' => $users,
        ];
        return json_encode($data);
    }

    public function getTransactions(Request $request){
        try{
            $columns = array(
                0 => 'articula_new',
                1 => 'sap_code',
                2 => 'item_name',
                3 => 'amount',
                4 => 'unit_name',
                5 => 'price_per_unit',
                6 => 'price',
                7 => 'currency',
                8 => 'storage_name',
                9 => 'operation_name',
                10 => 't.create_time',
                11 => 'destination',
                12 => 'from_destination',
                13 => 'invoice_number',
                14 => 'logistics_invoice',
                15 => 'container_number',
                16 => 'supplier_name',
                17 => 'user_name',
                18 => 'description',
                19 => 'request_id',
                20 => 'order_id'
            );
            $filterColumns = array(
                0 => 'dream_items.articula_new',
                1 => 'dream_items.sap_code',
                2 => 'dream_items.item_name',
                3 => 'amount',
                4 => 'unit_name',
                'filter_by_storage' => 's.id',
                'filter_by_operation' => 'tt.id',
                7 => 'dream_transactions.create_time',
                'filter_by_to_storage' => 's1.id',
                'filter_by_from_storage' => 's1.id',
                11 => 'invoice_number',
                'filter_by_supplier' => 'supl.id',
                'filter_by_user' => 'uu.id',
                13 => 'description',
                14 => 'dream_requests.id',
                15 => 'dream_orders.id'
            );
            $filterArr = json_decode($request->filterArr);
            $totalData = DB::table('dream_transaction_list')->count();
            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');
            $filter_str = '';
            if (!is_null($filterArr)){
                foreach ($filterArr as $key=>$values){
                    $hasEmpty = false;

                    if (!empty($values)){

                        if (count($values) == 1 && $values[0] == -1)
                        {
                            if ($key == 'filter_by_to_storage')
                            {
                                $filter_str .= " and s1.id is null and tt.operation = -1 ";
                            }
                            if ($key == 'filter_by_from_storage')
                            {
                                $filter_str .= " and s1.id is null and tt.operation = 1 ";
                            }
                        }else{
                            $filter_str.=" and ($filterColumns[$key] in (";

                            foreach ($values as $value){
                                if ($value == -1)
                                {
                                    $hasEmpty = true;
                                }else{
                                    $filter_str.=$value.',';
                                }
                            }

                            if ($hasEmpty){
                                $filter_str = substr($filter_str, 0, strlen($filter_str)-1).') or s1.id is null) ';
                            }else{
                                $filter_str = substr($filter_str, 0, strlen($filter_str)-1).')) ';
                            }

                            if ($key == 'filter_by_to_storage'){
                                if ($hasEmpty){
                                    $filter_str .= ' and tt.operation = -1';
                                }else{
                                    $filter_str .= ' and tt.operation = -1 and tt1.operation = 1';
                                }
                            }elseif( $key == 'filter_by_from_storage'){
                                if ($hasEmpty){
                                    $filter_str .= ' and tt.operation = 1';
                                }else{
                                    $filter_str .= ' and tt.operation = 1 and tt1.operation = -1';
                                }
                            }

                        }
                    }
                }
            }
            $data = array();
            $sql_q = "
            select SQL_CALC_FOUND_ROWS t.*, s.name as storage_name, tsb.storage_id, tsb.transaction_id, tsb.operation_id, tl.item_id, tl.amount, tl.unit_id,
            u.unit as unit_name, uu.firstname as user_name, i.articula_new, i.item_name, i.sap_code, tl.id as transaction_list_id, tt.name as operation_name,
            o.invoice_number as invoice_number, rl.request_id, r.type_id as request_type_id, o.id as order_id, supl.name as supplier_name, logistics.invoice_number as logistics_invoice,
            ol.price_per_unit, ol.price_per_unit*tl.amount as price, dc.name as currency, logistics.container_number,
            CASE
            WHEN tt.operation = 1 and tt1.operation = -1 then s1.name
            ELSE NULL
            END from_destination,

            CASE
            WHEN tt.operation = -1 and tt1.operation = 1 then s1.name
            ELSE NULL
            END destination

            from dream_transactions t
            inner join dream_transaction_storage_bind tsb on t.id = tsb.transaction_id
            inner join dream_storages s on tsb.storage_id = s.id
            inner join dream_transaction_list tl on t.id = tl.transaction_id
            inner join dream_transaction_types tt on tsb.operation_id = tt.id
            inner join dream_items i on tl.item_id = i.id
            inner join dream_units u on tl.unit_id = u.id
            inner join users uu on t.user_id = uu.id
            left join dream_transaction_storage_bind tsb1 on t.id = tsb1.transaction_id and tsb.id != tsb1.id
            left join dream_storages s1 on tsb1.storage_id = s1.id
            left join dream_transaction_types tt1 on tsb1.operation_id = tt1.id
            left join transaction_order_bind otb on tl.id = otb.transaction_list_id
            left join logistics_list on logistics_list.id = otb.logistics_list_id
            left join logistics on logistics.id = logistics_list.logistics_id
            left join dream_order_list ol on otb.order_list_id = ol.id
            left join dream_orders o on ol.order_id = o.id
            left join dream_billings db on db.id = o.billing_id
            left join dream_currencies dc on dc.id = db.currency_id
            left join dream_storages supl on o.supplier_id = supl.id
            left join dream_transaction_request_bind trb on tl.id = trb.transaction_list_id
            left join dream_request_list rl on trb.request_list_id = rl.id
            left join dream_requests r on rl.request_id = r.id
            where date(t.create_time) between date('$request->start_date') and date('$request->end_date') and tl.delete_time is null and s.type_id!=7
            ".$filter_str;

            $search_text = '';
            if (!empty($request->input('search.value'))){
                $search = $request->input('search.value');
                $search_text .= "
                     and ( i.articula_new like '%$search%' || i.articula_old like '%$search%' || s.name like '%$search%' || i.sap_code like '%$search%' || i.item_name like '%$search%' ||
                     o.invoice_number like '%$search%' || uu.firstname like '%$search%' || supl.name like '%$search%' || t.description like '%$search%' || o.id like '%$search%' || rl.id like '%$search%' ||
                     logistics.invoice_number like '%$search%' || logistics.container_number like '%$search%' || rl.request_id like '%$search%' )
                    ";
            }
            $q_t = $sql_q.$search_text."  order by $order $dir limit $limit offset $start";
            $transactions = DB::select($q_t);
            $totalCount = DB::select('select found_rows() as total_count')[0];
            $total = 0;
            if (!empty($transactions)){
                foreach ($transactions as $item)
                {
                    $nestedData['DT_RowId'] = "transaction_list_id".$item->transaction_list_id;
                    $total += $item->amount;
                    $nestedData['articula_new'] = $item->articula_new;
                    $nestedData['sap_code'] = $item->sap_code;
                    $nestedData['item_name'] = "<p style='color: black' title='".$item->item_name."'>".$item->item_name."</p>";
                    $nestedData['amount'] = round($item->amount, 3);
                    $nestedData['unit_name'] = $item->unit_name;
                    if (Gate::denies('viewOrderPrice', new DreamOrder)){
                        $nestedData['price_per_unit'] = '';
                        $nestedData['price'] = '';
                        $nestedData['currency'] = '';
                    }else{
                        $nestedData['price_per_unit'] = $item->price_per_unit;
                        $nestedData['price'] = $item->price;
                        $nestedData['currency'] = $item->currency;
                    }

                    $nestedData['storage_name'] = $item->storage_name;
                    $nestedData['operation_name'] = $item->operation_name;
                    $nestedData['create_time'] = Carbon::createFromFormat('Y-m-d H:i:s', $item->create_time)->format('Y-m-d H:i');
                    $nestedData['user_name'] = $item->user_name;
                    $nestedData['invoice_number'] = $item->invoice_number;
                    $nestedData['logistics_invoice'] = $item->logistics_invoice;
                    $nestedData['container_number'] = $item->container_number;
                    $nestedData['supplier_name'] = $item->supplier_name;
                    $nestedData['description'] = $item->description;
                    $nestedData['destination'] = $item->destination;
                    if ($item->request_type_id==1){
                        $nestedData['request_id'] = "<a href='".route('show_requests_page')."?id=".$item->request_id."' target='_blank'>$item->request_id</a>";
                    }elseif($item->request_type_id==2){
                        $nestedData['request_id'] = "<a href='".route('defective')."?id=".$item->request_id."' target='_blank'>$item->request_id</a>";
                    }elseif($item->request_type_id==3){
                        $nestedData['request_id'] = "<a href='".route('returns')."?id=".$item->request_id."' target='_blank'>$item->request_id</a>";
                    }else{
                        $nestedData['request_id'] = '';
                    }
                    if (is_null($item->order_id)){
                        $nestedData['order_id'] = '';
                    }else{
                        $nestedData['order_id'] = "<a href='".route('orders_page')."?id=".$item->order_id."' target='_blank'>$item->order_id</a>";
                    }
                    $nestedData['from_destination'] = $item->from_destination;
                    if (Auth::user()->roles->first()->id==self::SUPER_ADMIN){
                        $nestedData['buttons'] = "<button class='btn btn-warning ' data-toggle='modal' data-backdrop='static' data-keyboard='false'  data-target='#edit_amount_modal' type='button' onclick=\"setItemData('".$item->transaction_list_id."', '".$item->amount."')\"><i class='fa fa-edit'></i></button>&nbsp;" .
                            "<button class='btn btn-danger ' type='button' onclick=\"delete_item('".$item->transaction_list_id."')\"><i class='fa fa-trash'></i></button>";
                    }else{
                        $nestedData['buttons'] = '';
                    }
                    $data[] = $nestedData;
                }
            }
            $json_data = array(
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => intval($totalData),
                "recordsFiltered" => intval($totalCount->total_count),
                "data"            => $data,
                "requests"        => $request->all(),
                "total"           => round($total, 2)
            );
            return json_encode($json_data);
        }catch (\Exception $exception){
            return response()->json(['error'=>array($exception)], 500);
        }
    }

    public function getStatisticsColumns(Request $request){
        $operations = DB::table('dream_transaction_types')->whereIn('id', json_decode($request->operation_id))->get();
        $header = [
            json_encode(['data'=>'sap_code', 'title'=>'сап код']),
            json_encode(['data'=>'articula_new', 'title'=>'артикул нов.']),
            json_encode(['data'=>'item_name', 'title'=>'наименование'])
        ];
        foreach ($operations as $operation){
            $data = json_encode([ 'data' => $operation->name, 'title' => $operation->name]);
            $header = array_prepend($header, $data);
        }
        return json_encode(array_reverse($header));
    }

    public function getTransactionsStatistics(Request $request){
        $totalData = DB::table('dream_transaction_list')->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $operations = DB::table('dream_transaction_types')->whereIn('id', json_decode($request->operation_id))->get();
        if (empty($request->input('search.value'))){
            $transaction_statistics = DB::table('dream_transactions')
                ->join('dream_transaction_list', 'dream_transaction_list.transaction_id', '=', 'dream_transactions.id')
                ->join('dream_transaction_storage_bind', 'dream_transaction_storage_bind.transaction_id', '=', 'dream_transactions.id')
                ->join('dream_storages', 'dream_storages.id', '=', 'dream_transaction_storage_bind.storage_id')
                ->join('dream_transaction_types', 'dream_transaction_types.id', '=', 'dream_transaction_storage_bind.operation_id')
                ->join('dream_items', 'dream_items.id', '=', 'dream_transaction_list.item_id')
                ->join('dream_units', 'dream_units.id', '=', 'dream_transaction_list.unit_id')
                ->select(
                    'dream_storages.name as storage_name', 'dream_transaction_list.item_id', 'dream_transaction_list.unit_id', 'dream_units.unit as unit_name',
                    'dream_items.articula_new', 'dream_items.item_name', 'dream_items.sap_code', 'dream_transaction_types.name as operation_name', 'dream_transaction_types.id as operation_id',
                    DB::raw("SUM(dream_transaction_list.amount*dream_transaction_types.operation) as statistic_sum")
                )
                ->whereBetween('dream_transactions.create_time', array($request->start_date.' 00:00:00', $request->end_date.' 23:59:59'))
                ->whereIn('dream_storages.id', json_decode($request->storage_id))
                ->whereIn( 'dream_transaction_storage_bind.operation_id', json_decode($request->operation_id))
                ->where([['dream_storages.type_id', '=', self::WAREHOUSE], ['is_inventory', 0]])->limit($limit)->offset($start)
                ->groupBy('dream_transaction_list.item_id', 'dream_transaction_list.unit_id', 'dream_storages.id', 'dream_transaction_types.id')
                ->get();

            $transaction_statistics_count = DB::table('dream_transactions')
                ->join('dream_transaction_list', 'dream_transaction_list.transaction_id', '=', 'dream_transactions.id')
                ->join('dream_transaction_storage_bind', 'dream_transaction_storage_bind.transaction_id', '=', 'dream_transactions.id')
                ->join('dream_storages', 'dream_storages.id', '=', 'dream_transaction_storage_bind.storage_id')
                ->join('dream_transaction_types', 'dream_transaction_types.id', '=', 'dream_transaction_storage_bind.operation_id')
                ->join('dream_items', 'dream_items.id', '=', 'dream_transaction_list.item_id')
                ->join('dream_units', 'dream_units.id', '=', 'dream_transaction_list.unit_id')
                ->select(
                    'dream_storages.name as storage_name', 'dream_transaction_list.item_id', 'dream_transaction_list.unit_id', 'dream_units.unit as unit_name',
                    'dream_items.articula_new', 'dream_items.item_name', 'dream_items.sap_code', 'dream_transaction_types.name as operation_name', 'dream_transaction_types.id as operation_id',
                    DB::raw("SUM(dream_transaction_list.amount*dream_transaction_types.operation) as statistic_sum")
                )
                ->whereBetween('dream_transactions.create_time', array($request->start_date.' 00:00:00', $request->end_date.' 23:59:59'))
                ->whereIn('dream_storages.id', json_decode($request->storage_id))
                ->whereIn( 'dream_transaction_storage_bind.operation_id', json_decode($request->operation_id))
                ->where([['dream_storages.type_id', '=', self::WAREHOUSE], ['is_inventory', 0]])
                ->groupBy('dream_transaction_list.item_id', 'dream_transaction_list.unit_id', 'dream_storages.id', 'dream_transaction_types.id')
                ->get();
            $totalFiltered = count($transaction_statistics_count);
            if ($transaction_statistics->isEmpty()){
                $totalFiltered = 1;
            }
        }else{
            $search = $request->input('search.value');
            $transaction_statistics = DB::table('dream_transactions')
                ->join('dream_transaction_list', 'dream_transaction_list.transaction_id', '=', 'dream_transactions.id')
                ->join('dream_transaction_storage_bind', 'dream_transaction_storage_bind.transaction_id', '=', 'dream_transactions.id')
                ->join('dream_storages', 'dream_storages.id', '=', 'dream_transaction_storage_bind.storage_id')
                ->join('dream_transaction_types', 'dream_transaction_types.id', '=', 'dream_transaction_storage_bind.operation_id')
                ->join('dream_items', 'dream_items.id', '=', 'dream_transaction_list.item_id')
                ->join('dream_units', 'dream_units.id', '=', 'dream_transaction_list.unit_id')
                ->select(
                    'dream_storages.name as storage_name', 'dream_transaction_list.item_id', 'dream_transaction_list.unit_id', 'dream_units.unit as unit_name',
                    'dream_items.articula_new', 'dream_items.item_name', 'dream_items.sap_code', 'dream_transaction_types.name as operation_name', 'dream_transaction_types.id as operation_id',
                    DB::raw("SUM(dream_transaction_list.amount*dream_transaction_types.operation) as statistic_sum")
                )
                ->whereBetween('dream_transactions.create_time', array($request->start_date.' 00:00:00', $request->end_date.' 23:59:59'))
                ->whereIn('dream_storages.id', json_decode($request->storage_id))
                ->whereIn( 'dream_transaction_storage_bind.operation_id', json_decode($request->operation_id))
                ->where(function ($query) use ($search){
                    $query->orWhere('dream_items.articula_new', 'like', "%$search%")->orWhere('dream_items.item_name', 'like', "%$search%");
                })
                ->where([['dream_storages.type_id', '=', self::WAREHOUSE], ['is_inventory', 0]])->limit($limit)->offset($start)
                ->groupBy('dream_transaction_list.item_id', 'dream_transaction_list.unit_id', 'dream_storages.id', 'dream_transaction_types.id')
                ->get();

            $transaction_statistics_count = DB::table('dream_transactions')
                ->join('dream_transaction_list', 'dream_transaction_list.transaction_id', '=', 'dream_transactions.id')
                ->join('dream_transaction_storage_bind', 'dream_transaction_storage_bind.transaction_id', '=', 'dream_transactions.id')
                ->join('dream_storages', 'dream_storages.id', '=', 'dream_transaction_storage_bind.storage_id')
                ->join('dream_transaction_types', 'dream_transaction_types.id', '=', 'dream_transaction_storage_bind.operation_id')
                ->join('dream_items', 'dream_items.id', '=', 'dream_transaction_list.item_id')
                ->join('dream_units', 'dream_units.id', '=', 'dream_transaction_list.unit_id')
                ->select(
                    'dream_storages.name as storage_name', 'dream_transaction_list.item_id', 'dream_transaction_list.unit_id', 'dream_units.unit as unit_name',
                    'dream_items.articula_new', 'dream_items.item_name', 'dream_items.sap_code', 'dream_transaction_types.name as operation_name', 'dream_transaction_types.id as operation_id',
                    DB::raw("SUM(dream_transaction_list.amount*dream_transaction_types.operation) as statistic_sum")
                )
                ->whereBetween('dream_transactions.create_time', array($request->start_date.' 00:00:00', $request->end_date.' 23:59:59'))
                ->whereIn('dream_storages.id', json_decode($request->storage_id))
                ->whereIn( 'dream_transaction_storage_bind.operation_id', json_decode($request->operation_id))
                ->where(function ($query) use ($search){
                    $query->orWhere('dream_items.articula_new', 'like', "%$search%")->orWhere('dream_items.item_name', 'like', "%$search%");
                })
                ->where([['dream_storages.type_id', '=', self::WAREHOUSE], ['is_inventory', 0]])->limit($limit)->offset($start)
                ->groupBy('dream_transaction_list.item_id', 'dream_transaction_list.unit_id', 'dream_storages.id', 'dream_transaction_types.id')
                ->get();
            $totalFiltered = count($transaction_statistics_count);

        }
        $result = $this->convert_statistics_to_array($transaction_statistics, $operations);
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $result,
        );
        return json_encode($json_data);
    }

    public function do_array($collections){
        $array = array();
        if (!$collections->isEmpty()){
            foreach ($collections as $collection){
                $array[] = $collection->id;
            }
        }
        return $array;
    }

    public function convert_statistics_to_array($statistics, $operations){
        $arr = array();
        foreach ($statistics as $key=>$statistic){
            $arr[$statistic->item_id][]=$statistic;
        }
        ksort($arr, SORT_NUMERIC);
        $result = $this->get_result($arr, $operations);
        return $result;
    }

    public function get_result($arr, $operations){
        $result = array();
        foreach ($arr as $item){
            $data = [
                'articula_new'=>$item[0]->articula_new,
                'sap_code'=>$item[0]->sap_code,
                'item_name'=>$item[0]->item_name,
                'item_id'=>$item[0]->item_id,
                'unit'=>$item[0]->unit_name,
                'storage_name'=>$item[0]->storage_name
            ];

            foreach ($operations as $operation){
                $data = array_add($data, $operation->name, $this->get_total_by_operation($operation->id, $item));
            }
            $result[] = $data;
        }
        return $result;
    }

    public function get_total_by_operation($operation, $items){
        foreach ($items as $item){
            if ($operation==$item->operation_id){
                return $item->statistic_sum;
            }
        }
        return 0;
    }

    public function getTransactionDetail(){
        try{
            $tr_id = Input::all()['transaction_id'];
            $files_arr = $this->getTransactionFiles($tr_id);
            $tr_list_items = DB::select("SELECT dream_transaction_list.*, dream_items.articula_new, dream_items.sap_code, dream_items.item_name, dream_units.unit FROM dream_transaction_list ".
                "inner join dream_items on dream_items.id=dream_transaction_list.item_id inner join dream_units on dream_units.id=dream_transaction_list.unit_id where dream_transaction_list.transaction_id=$tr_id");
            $tr_desc = DreamTransaction::find($tr_id)->description;
            $data = [
                'tr_list_items'=>$tr_list_items,
                'tr_desc'=>$tr_desc,
                'files'=>$files_arr,
            ];
            return json_encode($data);
        }catch (\Exception $e){
            return response()->json(array('error'=>true, 'data'=>'невозможно загрузить'), 500);
        }
    }


    public function getTransactionFiles($transaction_id){
        $files_arr = [];
        $has_dir = \File::exists(public_path().'/assets/files/'.$transaction_id);
        if ($has_dir){
            $files = \File::allFiles(public_path().'/assets/files/'.$transaction_id);
            foreach ($files as $file){
                $data = [
                    'name'=>$file->getFilename(),
                    'url'=>$transaction_id.'/'.$file->getFilename()
                ];
                array_push($files_arr, $data);
            }
        }else{
            $files_arr = null;
        }
        return $files_arr;
    }
//    correct
    public function delete_transaction(Request $request){
        if (Gate::denies('allow_input_output_operation', new DreamTransaction)){
            return response()->json(['errors'=>array('у вас нет доступа!')], 500);
        }
        try{
            $transaction_data = DreamTransaction::find($request->transaction_id);
            $transaction_request_bind = DB::table('dream_transaction_request_bind')
                ->whereIn('transaction_list_id', array(DB::raw("select id from dream_transaction_list where transaction_id=$request->transaction_id")))->count();

            if (Auth::user()->roles->first()->id!=self::SUPER_ADMIN){
                if ($transaction_data->user_id==Auth::user()->id){
                    return \response()->json(['errors'=>array('вы не можете удалить!')], 500);
                }else{
                    if ($transaction_request_bind>0){
                        return \response()->json(['errors'=>array('удалить невозможно!')], 500);
                    }else{
                        $transaction_data->delete();
                    }
                }
            }else{
                if ($transaction_request_bind>0){
                    return \response()->json(['errors'=>array('удалить невозможно!')], 500);
                }else{
                    $transaction_data->delete();
                }
            }

        }catch(\Exception $e){
            return \response()->json(['errors'=>array('невозможно удалить!')], 500);
        }
        return json_encode($request->all());
    }

    public function edit_transaction_list(Request $request){
        try{
            $trl = DB::select("
                select dream_transaction_list.amount, dream_order_list.amount as order_amount, logistics_list.amount as logistics_amount from dream_transaction_list
                left join transaction_order_bind on transaction_order_bind.transaction_list_id=dream_transaction_list.id
                left join logistics_list on logistics_list.id=transaction_order_bind.logistics_list_id
                left join dream_order_list on dream_order_list.id=transaction_order_bind.order_list_id
                where dream_transaction_list.id=$request->list_id limit 1
            ");
            if (isset($trl[0])){
                return json_encode($trl[0]);
            }else{
                return \response()->json(['errors'=>array('ошибка')], 500);
            }
        }catch (\Exception $e){
            return \response()->json(['errors'=>array($e)], 500);
        }
    }
//  correct
    public function delete_transaction_list_item(){
        if (Gate::denies('allow_input_output_operation', new DreamTransaction)){
            return response()->json(['errors'=>array('у вас нет доступа!')], 500);
        }
        $tr_list_id = Input::all()['transaction_list_id'];
        $tr_id = DB::table('dream_transaction_list')->where('id', $tr_list_id)->first()->transaction_id;
        $transaction_data = DreamTransaction::find($tr_id);
        $transaction_request_bind = DB::table('dream_transaction_request_bind')
            ->whereIn('transaction_list_id', array(DB::raw("select id from dream_transaction_list where transaction_id=$tr_id")))->count();

        if (Auth::user()->roles->first()->id!=self::SUPER_ADMIN){
            if ($transaction_data->user_id!=Auth::user()->id){
                return \response()->json(['errors'=>array('вы не можете удалить!')], 500);
            }
        }
        try{
            if ($transaction_request_bind>0){
                DB::table('dream_transaction_list')->where('id', $tr_list_id)->update(['delete_time' => Carbon::now()]);
                return json_encode($tr_id);
            }else{
                DB::table('dream_transaction_list')->where('id', $tr_list_id)->update(['delete_time'=>Carbon::now()]);
                $this->clearEmptyTransactions();
                return json_encode($tr_id);
            }
        }catch(\Exception $e){
            return \response()->json(['errors'=>array('невозможно удалить!')], 500);
        }
    }

    public function clearEmptyTransactions(){
        $emptyTransactions = DB::table('dream_transactions')->leftJoin('dream_transaction_list', 'dream_transaction_list.transaction_id', '=', 'dream_transactions.id')
            ->whereNull('dream_transaction_list.id')->select('dream_transactions.id')->groupBy('dream_transactions.id')->get();
        foreach ($emptyTransactions as $transaction){
            DreamTransaction::find($transaction->id)->delete();
        }
    }

//    correct
    public function update_transaction_list_amount(){
        if (Gate::denies('allow_input_output_operation', new DreamTransaction)){
            return response()->json(array('error'=>true, 'data'=>'у вас нет доступа!'), 500);
        }
        try{
            $inputs = Input::all();
            $tr_id = DB::table('dream_transaction_list')->where('id', $inputs['dream_transaction_list_id'])->first()->transaction_id;
            $transaction_request_bind = DB::table('dream_transaction_request_bind')
                ->whereIn('transaction_list_id', array(DB::raw("select id from dream_transaction_list where transaction_id=$tr_id")))->count();
            $transaction_data = DreamTransaction::find($tr_id);
            if (Auth::user()->roles->first()->id!=self::SUPER_ADMIN){
                if ($transaction_data->user_id!=Auth::user()->id){
                    return \response()->json(['errors'=>array('вы не можете изменить!')], 500);
                }else{
                    if ($transaction_request_bind>0){
                        return \response()->json(['errors'=>array('вы не можете изменить!')], 500);
                    }else{
                        DB::table('dream_transaction_list')->where('id', $inputs['dream_transaction_list_id'])->update(['amount'=>(float)$inputs['amount']]);
                        DB::table('dream_transactions')->where('id', $tr_id)->update(['update_time'=>Carbon::now()]);
                    }
                }
            }else{
                DB::table('dream_transaction_list')->where('id', $inputs['dream_transaction_list_id'])->update(['amount'=>(float)$inputs['amount']]);
                DB::table('dream_transactions')->where('id', $tr_id)->update(['update_time'=>Carbon::now()]);
            }
        }catch(\Exception $e){
            return response()->json(['errors'=>array('возникла ошибка', $e)], 500);
        }
        return json_encode($tr_id);
    }

//    correct
    public function upload_transaction_list_file(Request $request){
        if (Gate::denies('allow_input_output_operation', new DreamTransaction)){
            return response()->json(array('error'=>true, 'data'=>'у вас нет доступа!'), 500);
        }
        $transaction_data = DreamTransaction::find($request->transaction_id_for_file);
        if ($transaction_data->user_id!=Auth::user()->id){
            return \response()->json(['errors'=>array('вы не можете добавить файл!')], 500);
        }
        try{
            if($request->hasFile('transaction_file'))
            {
                $dir_name=$request->transaction_id_for_file;
                $root=public_path()."/assets/files/";
                $f_name=$request->file('transaction_file')->getClientOriginalName();
                $request->file('transaction_file')->move($root.$dir_name,$f_name);
            }
            $arr = $this->getTransactionFiles($request->transaction_id_for_file);
            return json_encode($arr);
        }catch(\Exception $e){
            return response()->json(['errors'=>array('невозможно загрузить', $e)], 500);
        }
    }

//    correct
    public function delete_transaction_file(Request $request){
        if (Gate::denies('allow_input_output_operation', new DreamTransaction)){
            return response()->json(array('error'=>true, 'data'=>'у вас нет доступа!'), 500);
        }
        $transaction_data = DreamTransaction::find($request->transaction_id);
        if ($transaction_data->user_id!=Auth::user()->id){
            return \response()->json(['errors'=>array('вы не можете добавить файл!')], 500);
        }
        try{
            $root=$_SERVER['DOCUMENT_ROOT'].'/assets/files/'.$request->transaction_id.'/';
            if(!empty($request->file_name))
            {
                unlink($root.$request->file_name);
            }
            $arr = $this->getTransactionFiles($request->transaction_id);
            return json_encode($arr);
        }catch (\Exception $e){
            return response()->json(['errors'=>array('невозможно удалить')], 500);
        }
    }

    public function show_import_transactions_page(){
        $id = Input::all()['id'];
        $storage = DreamStorage::find($id);
        $items = DreamItem::all();
        $units = DreamUnit::all();
        $storage_items = DB::table('dream_storage_item_bind')->join('dream_items', 'dream_items.id', '=', 'dream_storage_item_bind.item_id')
            ->join('dream_units', 'dream_units.id', '=', 'dream_storage_item_bind.unit_id')
            ->select('dream_items.articula_new', 'dream_units.unit', 'dream_units.id as unit_id')->where('dream_storage_item_bind.storage_id', $id)->get();
        return view('index', ['page'=>'import_transactions', 'storage'=>$storage, 'storage_items'=>$storage_items, 'items'=>$items, 'units'=>$units]);
    }

    public function upload_inventarisation_file(Request $request){
        if ($request->hasFile('inventarization_file')){
            $path = $request->file('inventarization_file')->getRealPath();
            $array = [];
            $articuls = [];
            $excelData = \Excel::load($path)->get();
            $storage = DreamStorage::find($request->storage_id);
            $import_btn = "<button type='submit' class='btn btn-warning' style='float: right' id='import_btn'>импортировать</button>";
            if($excelData->count()){
                $request->file('inventarization_file')->move(public_path().'/assets/exampleFiles/', 'inventarization_file.xlsx');
                $data = array();
                foreach ($excelData as $key => $value) {
                    if (!is_null($value->articula_new) && !is_null($value->quantity) && !is_null($value->unit) && is_numeric($value->quantity)){
                        if (in_array($value->articula_new, $articuls)){
                            $array['articula_new'] = $value->articula_new;
                            $array['quantity'] = $value->quantity;
                            $array['unit'] = $value->unit;
                            $array['storage'] = 'дубликат';
                            $import_btn = null;
                        }else{
                            if ($storage->type_id==self::WAREHOUSE){
                                $correctItem = DB::table('dream_items')
                                    ->join('dream_storage_item_bind', 'dream_storage_item_bind.item_id', '=', 'dream_items.id')
                                    ->join('dream_units', 'dream_units.id', '=', 'dream_storage_item_bind.unit_id')
                                    ->select('dream_storage_item_bind.*', 'dream_units.unit')
                                    ->where([['dream_items.articula_new', $value->articula_new], ['dream_storage_item_bind.storage_id', $request->storage_id]])->first();
                                if (!empty($correctItem)){
                                    if ($correctItem->unit==$value->unit){
                                        $array['articula_new'] = $value->articula_new;
                                        $array['quantity'] = $value->quantity;
                                        $array['unit'] = $value->unit;
                                        $array['storage'] = $storage->name;
                                    }else{
                                        $array['articula_new'] = $value->articula_new;
                                        $array['quantity'] = $value->quantity;
                                        $array['unit'] = $value->unit;
                                        $array['storage'] = 'конфликт';
                                        $import_btn = null;
                                    }
                                }else{
                                    $isExist = DreamItem::where('articula_new', $value->articula_new)->first();
                                    if (!empty($isExist)){
                                        $hasStorage = DB::table('dream_storage_item_bind')
                                            ->where([['dream_storage_item_bind.storage_id', $request->storage_id], ['dream_storage_item_bind.item_id', $isExist->id]])->first();
                                        if (empty($hasStorage)){
                                            $array['articula_new'] = $value->articula_new;
                                            $array['quantity'] = $value->quantity;
                                            $array['unit'] = $value->unit;
                                            $array['storage'] = 'не тот склад';
                                            $import_btn = null;
                                        }else{
                                            $array['articula_new'] = $value->articula_new;
                                            $array['quantity'] = $value->quantity;
                                            $array['unit'] = $value->unit;
                                            $array['storage'] = 'error';
                                            $import_btn = null;
                                        }
                                    }else{
                                        $array['articula_new'] = $value->articula_new;
                                        $array['quantity'] = $value->quantity;
                                        $array['unit'] = $value->unit;
                                        $array['storage'] = 'нет в базе';
                                        $import_btn = null;
                                    }
                                }
                            }else{
                                $correctItem = DB::table('dream_items')
                                    ->join('dream_storage_item_bind', 'dream_storage_item_bind.item_id', '=', 'dream_items.id')
                                    ->select('dream_storage_item_bind.*')
                                    ->where([['dream_items.articula_new', $value->articula_new]])->first();
                                if (is_null($correctItem)){
                                    $array['articula_new'] = $value->articula_new;
                                    $array['quantity'] = $value->quantity;
                                    $array['unit'] = $value->unit;
                                    $array['storage'] = 'без склада';
                                    $import_btn = null;
                                }else{
                                    $array['articula_new'] = $value->articula_new;
                                    $array['quantity'] = $value->quantity;
                                    $array['unit'] = $value->unit;
                                    $array['storage'] = $storage->name;
                                }
                            }
                        }
                    }else{
                        if (is_null($value->articula_new)){
                            $array['articula_new'] = '';
                            $array['quantity'] = $value->quantity;
                            $array['unit'] = $value->unit;
                            $array['storage'] = 'без артикул';
                            $import_btn = null;
                        }elseif (is_null($value->quantity)){
                            $array['articula_new'] = $value->articula_new;
                            $array['quantity'] = '';
                            $array['unit'] = $value->unit;
                            $array['storage'] = 'без кол-во';
                            $import_btn = null;
                        }
                        elseif (is_numeric($value->quantity)){
                            $array['articula_new'] = $value->articula_new;
                            $array['quantity'] = '';
                            $array['unit'] = $value->unit;
                            $array['storage'] = 'не цыфра.';
                            $import_btn = null;
                        }
                        else{
                            $array['articula_new'] = $value->articula_new;
                            $array['quantity'] = $value->quantity;
                            $array['unit'] = '';
                            $array['storage'] = 'без unit';
                            $import_btn = null;
                        }

                    }
                    $articuls[] = $value->articula_new;
                    $data[] = $array;
                }
                if(is_null($request->date)){
                    $date = Carbon::now()->format('Y-m-d');
                }else{
                    $date = $request->date;
                }
                if(!empty($data)){
                    session()->put('inventarization_items.items', $data);
                    session()->put('inventarization_file_address', public_path().'/assets/exampleFiles/inventarization_file.xlsx');
                    session()->put('inventarization_items.storage_id', $request->storage_id);
                    session()->put('inventarization_items.date', $date);
                }
                else{
                    session()->forget('inventarization_items');
                }
            }
            return redirect()->back()->with('btn', $import_btn);
        }else{
            return redirect()->back()->with('btn', null);
        }
    }

    public function import_transaction(){
        try{
            DB::transaction(function () {
                $items = session()->get('inventarization_items.items');
                $storage_id = session()->get('inventarization_items.storage_id');
                $date = session()->get('inventarization_items.date');
                $result = $this->separate_input_output($items, $storage_id, $date);
                if (count($result['input_arr'])>0){
                    $this->save_transaction(self::INPUT_INVENTAR, $result['input_arr'], $storage_id, $date);
                }
                if (count($result['output_arr'])>0){
                    $this->save_transaction(self::OUTPUT_INVENTAR, $result['output_arr'], $storage_id, $date);
                }

                File::move(session()->get('inventarization_file_address'), public_path().'/assets/last_transaction_import_file/inventarization_file.xlsx');
            });
            session()->forget('inventarization_file_address');
            session()->forget('inventarization_items');
            return redirect()->back()->with('status', 'импортировано успешно');
        }catch(\Exception $e){
            dd($e);
            return redirect()->back()->with('status', 'невозможно импортировать');
        }
    }

    public function separate_input_output($items, $storage_id, $date){
        $input = [];
        $output = [];
        foreach ($items as $item){
            $item_id = DB::table('dream_items')->where('articula_new', $item['articula_new'])->first()->id;
            $unit = DB::table('dream_storage_item_bind')->where([['item_id', $item_id]])->first();
            if (!is_null($unit)){
                $unit_id = $unit->unit_id;
                $remaining = $this->get_item_remaining($storage_id, $item_id, $unit_id, $date);

                if ($item['quantity']-$remaining>0){
                    $val = [
                        'item_id'=>$item_id,
                        'quantity'=>$item['quantity']-$remaining,
                        'unit_id'=>$unit_id
                    ];
                    array_push($input, $val);
                }
                else if ($item['quantity']-$remaining<0){
                    $val = [
                        'item_id'=>$item_id,
                        'quantity'=>$remaining-$item['quantity'],
                        'unit_id'=>$unit_id
                    ];
                    array_push($output, $val);
                }
                else if ($item['quantity']==0){
                    if ($remaining>0){
                        $val = [
                            'item_id'=>$item_id,
                            'quantity'=>$remaining,
                            'unit_id'=>$unit_id
                        ];
                        array_push($output, $val);
                    }elseif ($remaining<0){
                        if ($remaining>0){
                            $val = [
                                'item_id'=>$item_id,
                                'quantity'=>(-1)*$remaining,
                                'unit_id'=>$unit_id
                            ];
                            array_push($input, $val);
                        }
                    }
                }
            }
        }
        $data = [
            'input_arr'=>$input,
            'output_arr'=>$output
        ];
        return $data;
    }

    public function get_item_remaining($storage_id, $item_id, $unit_id, $date)
    {
        $total = DB::select("select sum(operation*amount) as total from dream_transaction_list ".
            "inner join dream_transactions on dream_transactions.id=dream_transaction_list.transaction_id ".
            "left join dream_transaction_storage_bind on dream_transaction_list.transaction_id=dream_transaction_storage_bind.transaction_id and dream_transaction_storage_bind.storage_id=$storage_id " .
            "left join dream_transaction_types on dream_transaction_types.id=dream_transaction_storage_bind.operation_id where dream_transaction_list.item_id=$item_id and
            dream_transaction_list.unit_id=$unit_id and dream_transaction_list.delete_time is null
            and date(dream_transactions.create_time) <= date('$date')")[0];
        if ($total->total == null){
            $total->total = 0;
        }
        return round($total->total, 4);
    }

    public function save_transaction($operation, $items, $storage_id, $date){
        $data = [
            'user_id' => Auth::user()->id,
            'description' => 'Инвентаризация',
            'is_inventory' => 1,
            'create_time' => $date.' '.Carbon::now()->format('H:i:s')
        ];
        $tr_id = DreamTransaction::create($data)->id;
        DB::table('dream_transaction_storage_bind')->insert(['transaction_id'=>$tr_id, 'storage_id'=>$storage_id, 'operation_id'=>$operation]);
        foreach ($items as $item){
            if ($item['quantity']>0){
                $item_unit_bind = DB::table('dream_item_unit_bind')->where([['item_id',$item['item_id']], ['unit_id',$item['unit_id']]])->count();
                if ($item_unit_bind==0){
                    $data = [
                        'item_id'=>$item['item_id'],
                        'unit_id'=>$item['unit_id']
                    ];
                    DB::table('dream_item_unit_bind')->insert($data);
                }
                $transaction_list_data = [
                    'item_id' => $item['item_id'],
                    'transaction_id' => $tr_id,
                    'amount' => $item['quantity'],
                    'unit_id' => $item['unit_id']
                ];
                DB::table('dream_transaction_list')->insert($transaction_list_data);
            }
        }
    }

//    ajax get requests
    function get_transactions_by_storage()
    {
        $storage_id = Input::all()['storage_id'];
        $transactions = DB::table("dream_transactions")->join('dream_transaction_storage_bind', 'dream_transaction_storage_bind.transaction_id', '=', 'dream_transactions.id')
            ->join('users', 'users.id', '=', 'dream_transactions.user_id')
            ->select('dream_transactions.create_time', 'dream_transaction_storage_bind.*', 'users.name as user_name')->where('dream_transaction_storage_bind.storage_id', $storage_id)->get();
        return json_encode($transactions);
    }

    public function add_transaction_description(Request $request){
        if (Gate::denies('allow_input_output_operation', new DreamTransaction)){
            return response()->json(array('error'=>true, 'data'=>'у вас нет доступа!'), 500);
        }
        try{
            $transaction_data = DreamTransaction::find($request->tr_id);
            if ($transaction_data->user_id!=Auth::user()->id){
                return \response()->json(['errors'=>array('вы не можете изменить!')], 500);
            }else{
                DreamTransaction::find($request->tr_id)->update(['description'=>$request->desc]);
            }
        }catch(\Exception $e){
            return response()->json(['errors'=>array('возникла ошибка', $e)], 500);
        }
        return json_encode($request->all());
    }

    public function AddTransaction(Request $request){
        $user = User::where('name', '=', 'technologist')->first();
        if (is_null($user)){
            return json_encode(['errors'=>array('ошибка, пользователь не нашёлься')], 500);
        }
        $user_id = $user->id;
        $parsedStr = explode('|', $request->str);
        if (count($parsedStr)>2){
            $item_code = substr($parsedStr[1], 0, 5);
        }else{
            $item_code = substr($parsedStr[1], 3, 5);
        }
        $line_code = $parsedStr[0];
        DB::table('technologist_sent_serias')->insert(['serianumbers'=>$request->str, 'line'=>$line_code, 'item'=>$item_code]);
        $item = DB::table('technologist_model_bind')->where('item_code', $item_code)->first();
        //return json_encode($item);
//        $line = DB::table('technologist_line_bind')->where('line_code', $line_code)->first();
        $line = DB::table('dream_storage_item_bind')
            ->join('dream_storages', 'dream_storages.id', '=', 'dream_storage_item_bind.storage_id')
            ->select('dream_storages.id as line_id')
            ->where([['item_id', $item->item_id], ['dream_storages.type_id', 2]])->first();

        $storage = DB::table('dream_storage_item_bind')
            ->join('dream_storages', 'dream_storages.id', '=', 'dream_storage_item_bind.storage_id')
            ->select('dream_storages.id as storage_id')
            ->where([['item_id', $item->item_id], ['dream_storages.type_id', 1]])->first();

        if (is_null($item)){
            return json_encode(['errors'=>array('ошибка, модель не нашёлься')], 500);
        }
        if (is_null($line)){
            return json_encode(['errors'=>array('ошибка, линия не нашёлься')], 500);
        }


        try{

            //Add to moving request
            $valid_item = DB::table('dream_items')
                ->leftJoin('dream_item_unit_bind', 'dream_item_unit_bind.item_id', '=', 'dream_items.id')
                ->select('dream_items.*', 'dream_items.id as item_id','dream_item_unit_bind.unit_id as unit_id')
                ->where('dream_items.id', $item->item_id)
                ->groupBy(['dream_items.id', 'dream_item_unit_bind.unit_id'])
                ->first();


            $movingRequestData= (object)[];

            $movingRequestData->from_storage_id = $line->line_id;
            $movingRequestData->to_storage_id = $storage->storage_id;
            $movingRequestData->description = "It is coming from technologist scanner!";
            $valid_item->amount = 1;
            $movingRequestData->items = [$valid_item];

            $data = [
                'to_storage_id'=>$storage->storage_id,
                'from_storage_id'=>$line->line_id,
                'description' => "It is coming from technologist scanner!",
                'amount'=>1,
                'type_id'=>4,
                'user_id' => Auth::user()->id,
            ];


            $last_open_request = DB::table("dream_requests")
                ->select('id')
                ->where([
                    ['user_id', Auth::user()->id],
                    ['state', 1], ['create_time', '>', Carbon::now()->startOfDay()->format('Y-m-d 00:00:00')],
                    ['from_storage_id', $line->line_id]])
                ->orderBy('id', 'desc')
                ->first();

            //return json_encode($last_open_request);
            $tr_req_id=-1;
            if($last_open_request)
            {
                $tr_req_id = $last_open_request->id;

                $exist_requested_item = DB::table('dream_request_list')
                    ->select('dream_request_list.*')
                    ->where([['item_id', $valid_item->item_id], ['request_id', $tr_req_id]])
                    ->first();
                if($exist_requested_item){
                    DB::table('dream_request_list')
                        ->where('id', $exist_requested_item->id)
                        ->update(['amount'=>$exist_requested_item->amount + 1]);
                }
                else{
                    $item_data = [
                        'request_id' => $tr_req_id,
                        'item_id' => $valid_item->item_id,
                        'amount' => (float)1,
                        'unit_id' => $valid_item->unit_id
                    ];

                    DB::table('dream_request_list')->insert($item_data);
                }
            }
            else{
                $tr_req_id = DreamRequest::create($data)->id;
                $item_data = [
                    'request_id' => $tr_req_id,
                    'item_id' => $valid_item->item_id,
                    'amount' => (float)1,
                    'unit_id' => $valid_item->unit_id
                ];

                DB::table('dream_request_list')->insert($item_data);
            }



            $result = DB::transaction(function () use($item, $line, $user_id){

                $item_children = DB::table('dream_item_bind')->join('dream_storage_item_bind', function ($join){
                    $join->on('dream_storage_item_bind.item_id', '=', 'dream_item_bind.child_id');
                    $join->on('dream_storage_item_bind.unit_id', '=', 'dream_item_bind.unit_id');
                })->select('dream_storage_item_bind.item_id', 'dream_storage_item_bind.unit_id', 'dream_item_bind.quantity')
                    ->where('dream_item_bind.parent_id', $item->item_id)->groupBy('dream_storage_item_bind.item_id', 'dream_storage_item_bind.unit_id', 'dream_item_bind.quantity')->get();

                $transaction = DB::table('dream_transactions')
                    ->join('dream_transaction_storage_bind', 'dream_transaction_storage_bind.transaction_id', '=', 'dream_transactions.id')
                    ->join('dream_transaction_list', 'dream_transaction_list.transaction_id', '=', 'dream_transactions.id')
                    ->where([['dream_transactions.user_id', $user_id], ['dream_transactions.create_time', '>', Carbon::now()->startOfDay()->format('Y-m-d 00:00:00')],
                        ['dream_transaction_storage_bind.storage_id', $line->line_id]])
                    ->select('dream_transactions.*')->first();

                if (is_null($transaction)){
                    $data = [
                        'user_id'=>$user_id
                    ];
                    $transaction_id = DreamTransaction::create($data)->id;
                    $this->createTransactionStorageBind($transaction_id, $line->line_id);
                    foreach ($item_children as $child){
                        $this->updateOrInsertItem($child->item_id, $child->unit_id, $child->quantity, $transaction_id, $line->line_id);
                    }

                }else{
                    $transaction_storage_bind = DB::table('dream_transaction_storage_bind')->where([['transaction_id', $transaction->id],['storage_id', $line->line_id]])->first();
                    if (is_null($transaction_storage_bind)){
                        $this->createTransactionStorageBind($transaction->id, $line->line_id);
                    }
                    foreach ($item_children as $child){
                        $this->updateOrInsertItem($child->item_id, $child->unit_id, $child->quantity, $transaction->id, $line->line_id);
                    }
                }


                $storage_id = DB::table('dream_storage_item_bind')
                    ->join('dream_items', 'dream_items.id', '=', 'dream_storage_item_bind.item_id')
                    ->select('dream_storage_item_bind.*')->where([['item_id', $item->item_id], ['dream_items.articula_new', 'like', '34%']])->first();


                if (!is_null($line)){
                    $transactionForModel = DB::table('dream_transactions')
                        ->join('dream_transaction_storage_bind', 'dream_transaction_storage_bind.transaction_id', '=', 'dream_transactions.id')
                        ->where([['dream_transactions.user_id', $user_id], ['dream_transactions.create_time', '>', Carbon::now()->startOfDay()->format('Y-m-d 00:00:00')],
                            ['dream_transaction_storage_bind.operation_id', self::PRODUCTION], ['storage_id', $line->line_id]])->select('dream_transactions.id')->get()->first();
                    if (is_null($transactionForModel)){
                        $data = [
                            'user_id'=>$user_id
                        ];
                        $transaction_id_of_model = DreamTransaction::create($data)->id;
                        $this->createTransactionStorageBindOfModel($transaction_id_of_model, $line->line_id);
                        $this->updateOrInsertModel($storage_id->item_id, $storage_id->unit_id, $transaction_id_of_model);
                    }else{
                        $transaction_storage_bind = DB::table('dream_transaction_storage_bind')->where('transaction_id', $transactionForModel->id)->get()->first();
                        if (is_null($transaction_storage_bind)){
                            $this->createTransactionStorageBindOfModel($transactionForModel->id, $line->line_id);
                        }
                        $this->updateOrInsertModel($storage_id->item_id, $storage_id->unit_id, $transactionForModel->id);
                    }
                }
                return $transaction;
            });
            return response()->json($result, 200);

        }catch(\Exception $e){
            return json_encode([
                    'errors'    =>array('ошибка'),
                    'message'   =>$e
                ], 500);
        }
    }

    public function createTransactionStorageBind($transaction_id, $line_id){
        $bind_data = [
            'transaction_id'=>$transaction_id,
            'storage_id'=>$line_id,
            'operation_id'=>self::OUTPUT
        ];
        DB::table('dream_transaction_storage_bind')->insert($bind_data);
    }

    public function updateOrInsertItem($item_id, $unit_id, $amount, $transaction_id, $line_id){
        $list_item = DB::table('dream_transaction_list')
            ->join('dream_transaction_storage_bind', 'dream_transaction_storage_bind.transaction_id', '=', 'dream_transaction_list.transaction_id')
            ->where([['dream_transaction_list.transaction_id', $transaction_id], ['dream_transaction_list.item_id', $item_id], ['dream_transaction_list.unit_id', $unit_id],
                ['dream_transaction_storage_bind.storage_id', $line_id]])->select('dream_transaction_list.*')->get()->first();
        if (is_null($list_item)){
            $data = [
                'transaction_id'=>$transaction_id,
                'item_id'=>$item_id,
                'unit_id'=>$unit_id,
                'amount'=>$amount
            ];
            DB::table('dream_transaction_list')->insert($data);
        }else{
            DB::table('dream_transaction_list')->where('id', $list_item->id)->update(['amount'=>($list_item->amount+$amount)]);
        }

    }

    public function createTransactionStorageBindOfModel($transaction_id, $storage_id){
        $bind_data = [
            'transaction_id'=>$transaction_id,
            'storage_id'=>$storage_id,
            'operation_id'=>self::PRODUCTION
        ];
        DB::table('dream_transaction_storage_bind')->insert($bind_data);
    }

    public function updateOrInsertModel($item_id, $unit_id, $transaction_id){
        $list_item = DB::table('dream_transaction_list')->where([['transaction_id', $transaction_id], ['item_id', $item_id], ['unit_id', $unit_id]])->first();
        if (is_null($list_item)){
            $data = [
                'transaction_id'=>$transaction_id,
                'item_id'=>$item_id,
                'unit_id'=>$unit_id,
                'amount'=>1
            ];
            DB::table('dream_transaction_list')->insert($data);
        }else{
            DB::table('dream_transaction_list')->where('id', $list_item->id)->update(['amount'=>($list_item->amount + 1)]);
        }
    }

    /**
     * @param Request $request
     */
    public function inputByQRCode(Request $request)
    {
        $arr = explode('/', $request->str);
        $date = null;
        $amount = null;
        $articul = null;

        if (isset($arr[0]))
        {
            $date = $arr[0];
        }
        if (isset($arr[2]))
        {
            $amount = $arr[2];
        }
        if (isset($arr[3]))
        {
            $articul = $arr[3];
        }

        $hasArticul = DreamItem::where('articula_new', $articul);
        if ($hasArticul->count() == 1)
        {
            $hasStorage = DB::table('dream_storage_item_bind')
                ->join('dream_items', 'dream_items.id', '=', 'dream_storage_item_bind.item_id')
                ->join('dream_storages', 'dream_storages.id', '=', 'dream_storage_item_bind.storage_id')
                ->select("dream_storage_item_bind.storage_id", "dream_storages.name", "dream_storage_item_bind.item_id", "dream_storage_item_bind.unit_id")
                ->where([['dream_items.articula_new', $articul], ['dream_storages.type_id', 2]]);
            if ($hasStorage->count() == 1){

                $result = DB::transaction(function () use($hasStorage, $amount, $date){
                    $storage_id = $hasStorage->first()->storage_id;
                    $storage_name = $hasStorage->first()->name;
                    $item_id = $hasStorage->first()->item_id;
                    $unit_id = $hasStorage->first()->unit_id;
                    $user_id = User::where('name', 'radiator_technologist')->first()->id;

                    if (!is_numeric($amount) || is_null($amount)){
                        return response()->json('количество не цифра или null', 500);
                    }

                    if (!is_numeric($item_id) || is_null($item_id)){
                        return response()->json('item_id не цифра или null', 500);
                    }

                    if (!is_numeric($unit_id) || is_null($unit_id)){
                        return response()->json('unit_id не цифра или null', 500);
                    }

                    if (!is_numeric($storage_id) || is_null($storage_id)){
                        return response()->json('storage_name не цифра или null', 500);
                    }

                    if (!is_numeric($user_id) || is_null($user_id)){
                        return response()->json('user_id не цифра или null', 500);
                    }

                    try{
                        $tr_id = $this->qrCodeInputOutputOperations($item_id, $user_id, $storage_id, $amount);
//                        $tr_data = [
//                            'user_id' => 1,
//                            'description' => $storage_name." ".$date
//                        ];
//                        $tr_id = DB::table('dream_transactions')->insertGetId($tr_data);
//
//                        $list_data = [
//                            'item_id' => $item_id,
//                            'amount' => $amount,
//                            'transaction_id' => $tr_id,
//                            'unit_id' => $unit_id
//                        ];
//                        DB::table('dream_transaction_list')->insert($list_data);
//
//                        $tr_storage_data = [
//                            'storage_id' => $storage_id,
//                            'transaction_id' => $tr_id,
//                            'operation_id' => self::INPUT
//                        ];
//                        DB::table('dream_transaction_storage_bind')->insert($tr_storage_data);
                        return $tr_id;
                    }catch (\Exception $e){
                        return response()->json($e, 500);
                    }
                });

                return response()->json($result, 200);
            }elseif ($hasStorage->count()>1){
                return response()->json('артикул находится в двух складах', 500);
            }else{
                return response()->json('артикул не привязан к линии', 500);
            }
        }else{
            return response()->json('артикул не существует в базе', 500);
        }

    }

    public function qrCodeInputOutputOperations($item_id, $user_id, $line_id, $amount)
    {
        $item_children = DB::table('dream_item_bind')->join('dream_storage_item_bind', function ($join){
            $join->on('dream_storage_item_bind.item_id', '=', 'dream_item_bind.child_id');
            $join->on('dream_storage_item_bind.unit_id', '=', 'dream_item_bind.unit_id');
        })->select('dream_storage_item_bind.item_id', 'dream_storage_item_bind.unit_id', 'dream_item_bind.quantity')
            ->where('dream_item_bind.parent_id', $item_id)->groupBy('dream_storage_item_bind.item_id', 'dream_storage_item_bind.unit_id', 'dream_item_bind.quantity')->get();

        $transaction = DB::table('dream_transactions')
            ->join('dream_transaction_storage_bind', 'dream_transaction_storage_bind.transaction_id', '=', 'dream_transactions.id')
            ->join('dream_transaction_list', 'dream_transaction_list.transaction_id', '=', 'dream_transactions.id')
            ->where([['dream_transactions.user_id', $user_id], ['dream_transactions.create_time', '>', Carbon::now()->startOfDay()->format('Y-m-d 00:00:00')],
                ['dream_transaction_storage_bind.storage_id', $line_id], ['dream_transaction_storage_bind.operation_id', self::OUTPUT]])
            ->select('dream_transactions.*')->first();

        if (is_null($transaction)){
            $data = [
                'user_id'=>$user_id
            ];
            $transaction_id = DreamTransaction::create($data)->id;
            $this->createTransactionStorageBind1($transaction_id, $line_id);
            foreach ($item_children as $child){
                $this->updateOrInsertItem1($child->item_id, $child->unit_id, $child->quantity, $transaction_id, $line_id, $amount);
            }

        }else{
            $transaction_storage_bind = DB::table('dream_transaction_storage_bind')->where([['transaction_id', $transaction->id],['storage_id', $line_id]])->first();
            if (is_null($transaction_storage_bind)){
                $this->createTransactionStorageBind1($transaction->id, $line_id);
            }
            foreach ($item_children as $child){
                $this->updateOrInsertItem1($child->item_id, $child->unit_id, $child->quantity, $transaction->id, $line_id, $amount);
            }
        }


        $storage_id = DB::table('dream_storage_item_bind')
            ->join('dream_items', 'dream_items.id', '=', 'dream_storage_item_bind.item_id')
            ->select('dream_storage_item_bind.*')->where([['item_id', $item_id]])->first();
        if (!is_null($line_id)){
            $transactionForModel = DB::table('dream_transactions')
                ->join('dream_transaction_storage_bind', 'dream_transaction_storage_bind.transaction_id', '=', 'dream_transactions.id')
                ->where([['dream_transactions.user_id', $user_id], ['dream_transactions.create_time', '>', Carbon::now()->startOfDay()->format('Y-m-d 00:00:00')],
                    ['dream_transaction_storage_bind.operation_id', self::PRODUCTION], ['storage_id', $line_id]])->select('dream_transactions.id')->get()->first();
            if (is_null($transactionForModel)){
                $data = [
                    'user_id'=>$user_id
                ];
                $transaction_id_of_model = DreamTransaction::create($data)->id;
                $this->createTransactionStorageBindOfModel1($transaction_id_of_model, $line_id);
                $this->updateOrInsertModel1($storage_id->item_id, $storage_id->unit_id, $transaction_id_of_model, $amount);
            }else{
                $transaction_storage_bind = DB::table('dream_transaction_storage_bind')->where('transaction_id', $transactionForModel->id)->get()->first();
                if (is_null($transaction_storage_bind)){
                    $this->createTransactionStorageBindOfModel1($transactionForModel->id, $line_id);
                }
                $this->updateOrInsertModel1($storage_id->item_id, $storage_id->unit_id, $transactionForModel->id, $amount);
            }
        }
        return $transaction;
    }

    public function createTransactionStorageBind1($transaction_id, $line_id){
        $bind_data = [
            'transaction_id'=>$transaction_id,
            'storage_id'=>$line_id,
            'operation_id'=>self::OUTPUT
        ];
        DB::table('dream_transaction_storage_bind')->insert($bind_data);
    }

    public function updateOrInsertItem1($item_id, $unit_id, $amount, $transaction_id, $line_id, $model_amount){
        $list_item = DB::table('dream_transaction_list')
            ->join('dream_transaction_storage_bind', 'dream_transaction_storage_bind.transaction_id', '=', 'dream_transaction_list.transaction_id')
            ->where([['dream_transaction_list.transaction_id', $transaction_id], ['dream_transaction_list.item_id', $item_id], ['dream_transaction_list.unit_id', $unit_id],
                ['dream_transaction_storage_bind.storage_id', $line_id]])->select('dream_transaction_list.*')->get()->first();
        if (is_null($list_item)){
            $data = [
                'transaction_id'=>$transaction_id,
                'item_id'=>$item_id,
                'unit_id'=>$unit_id,
                'amount'=>$amount*$model_amount
            ];
            DB::table('dream_transaction_list')->insert($data);
        }else{
            DB::table('dream_transaction_list')->where('id', $list_item->id)->update(['amount'=>($list_item->amount + $amount*$model_amount)]);
        }

    }

    public function createTransactionStorageBindOfModel1($transaction_id, $storage_id){
        $bind_data = [
            'transaction_id'=>$transaction_id,
            'storage_id'=>$storage_id,
            'operation_id'=>self::PRODUCTION
        ];
        DB::table('dream_transaction_storage_bind')->insert($bind_data);
    }

    public function updateOrInsertModel1($item_id, $unit_id, $transaction_id, $amount){
        $list_item = DB::table('dream_transaction_list')->where([['transaction_id', $transaction_id], ['item_id', $item_id], ['unit_id', $unit_id]])->first();
        if (is_null($list_item)){
            $data = [
                'transaction_id'=>$transaction_id,
                'item_id'=>$item_id,
                'unit_id'=>$unit_id,
                'amount'=>$amount
            ];
            DB::table('dream_transaction_list')->insert($data);
        }else{
            DB::table('dream_transaction_list')->where('id', $list_item->id)->update(['amount'=>($list_item->amount + $amount)]);
        }
    }
}

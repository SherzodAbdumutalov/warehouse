<?php

namespace App\Http\Controllers;

use App\DreamItem;
use App\DreamUnit;
use Egulias\EmailValidator\Exception\ExpectingCTEXT;
use Illuminate\Http\Request;
use DB;
use Gate;

class ItemImportController extends Controller
{

    protected static  $headers_array = ['articula_new', 'articula_old', 'sap_code', 'name', 'unit'];

    public function importExportView($parentId){
        if (Gate::denies('VIEW_IMPORT_PAGE')){
            abort(403);
        }
        $id = $parentId;
        return view('index', ['page'=>'import_parent', 'parentId'=>$id]);

    }

    public function uploadExcelFile(Request $request){

        if (Gate::denies('save', new DreamItem)){
            abort(403);
        }

        $items = DreamItem::all();

        $allData = [];
        $units = [];

        foreach ($items as $item){

            array_push($allData, $item->toArray());

        }

        $all_units_data = DreamUnit::all();

        foreach ($all_units_data as $unit){

            array_push($units, $unit->unit);

        }

        if($request->hasFile('sample_file')){

            try{
                $path = $request->file('sample_file')->getRealPath();

                $data = \Excel::load($path)->get();

                if($data->count()){

                    foreach ($data as $key => $value) {
                        $arr[] = ['articula_new' => $value->articula_new, 'articula_old' => $value->articula_old, 'sap_code' => $value->sap_code, 'item_name' => $value->item_name, 'item_model' => $value->item_model, 'unit' => $value->unit];

                    }

                    if(!empty($arr)){

                        session()->put('import_parent_item_units', $units);
                        session()->put('import_parent_item', $arr);
                        session()->put('allItem', $allData);
                        return redirect()->back();

                    }

                }
            }
            catch (\Exception $e){
                return redirect()->back()->with('status', 'ошибка!');
            }

        }

        session()->put('import_parent_item_units', $units);
        session()->put('import_parent_item', '');
        session()->put('allItem', $allData);

        return redirect()->back();

    }

    public function importFile(){

        if (Gate::denies('save', new DreamItem)){
            abort(403);
        }

        try{
            DB::transaction(function (){

                $arr = [];
                $all_new_articula = DB::table("dream_items")->select("articula_new")->get()->toArray();
                foreach ($all_new_articula as $articula){
                    array_push($arr, $articula->articula_new);
                }
                $data = session()->get('import_parent_item');
                foreach ($data as $item){
                    $items = [

                        'articula_new' => $item['articula_new'],
                        'articula_old' => $item['articula_old'],
                        'sap_code' => $item['sap_code'],
                        'item_name' => $item['item_name'],
                        'item_model' => $item['item_model']

                    ];
                    if (!in_array($items['articula_new'], $arr)){
                        $id = DreamItem::create($items)->id;
                        $parsedUnits = explode("/", $item['unit']);
                        if (empty($item['unit'])){
                            $parsedUnits=[
                                0 => 'шт'
                            ];
                        }
                        for ($i=0; $i<sizeof($parsedUnits); $i++){
                            $unitId = DB::select("select id from dream_units where unit='$parsedUnits[$i]'");
                            DB::table("dream_item_unit_bind")->insert(['unit_id'=>$unitId[0]->id, 'item_id'=>$id]);
                        }
                    }else{
                        $update_item = DreamItem::where('articula_new', $item['articula_new'])->first();
                        $update_item->update($items);
                    }
                }
            });
        }
        catch (\Exception $e){
            dd($e);
            return redirect()->back()->with('status', 'ошибка при загрузке');
        }

        session()->forget('import_parent_item');
        return redirect()->back()->with('status', 'добавлено');

    }
}

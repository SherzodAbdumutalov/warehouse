<?php

namespace App\Http\Controllers;

use App\DreamItem;
use App\DreamUnit;
use function foo\func;
use Gate;
use Illuminate\Http\Request;
use DB;
use Symfony\Component\Translation\Tests\Writer\BackupDumper;

class ImportExportController extends Controller
{
    public function show_child_item_import_page($id){
        if (Gate::denies('VIEW_IMPORT_PAGE')){
            abort(403);
        }
        $parentData = DreamItem::find($id);
        return view('index', ['parentData'=>$parentData, 'page'=>'import_child_items']);
    }

    public function uploadExcelFile(Request $request){
        if (Gate::denies('save', new DreamItem)){
            abort(403);
        }
        $items = DreamItem::all();
        $allItems = [];
        $allUnits = [];
        foreach ($items as $item){
            array_push($allItems, $item->toArray());
        }
        $units = DreamUnit::all();
        foreach ($units as $unit){
            array_push($allUnits, $unit->unit);
        }
        try{
            if($request->hasFile('sample_file')){
                $path = $request->file('sample_file')->getRealPath();
                $data = \Excel::load($path)->get();
                if($data->count()){
                    foreach ($data as $key => $value) {
                        $arr[] = ['articula_new' => $value->articula_new, 'quantity' => $value->quantity, 'units' => $value->units];
                    }
                    if(!empty($arr)){
                        session()->put('import_child_item', $arr);
                    }
                    else{
                        session()->put('import_child_item', '');
                    }
                }
            }
            session()->put('import_child_item_units', $allUnits);
            session()->put('allItem', $allItems);
            return redirect()->back();

        }catch(\Exception $e){
            dd($e);
            return redirect()->back()->with('status', 'не возможно загрузить файл');
        }

    }

    public function parsedText($text){
        $arr = explode('/', $text);
        return $arr;
    }

    public function importExcelFile(Request $request){
        if (Gate::denies('save', new DreamItem)){
            abort(403);
        }
        try{
            $parentId = $request->parentId;
            DB::transaction(function () use($parentId) {
                $dreamItem = new DreamItem();
                $data = session()->get('import_child_item');
                $collect_child_items = [];
                foreach ($data as $item){
                    $articula = $item['articula_new'];
                    $child_id = \Illuminate\Support\Facades\DB::select("select id from dream_items where articula_new='$articula'");
                    $parsedUnits = explode("/", $item['units']);
                    $quantities = explode("/", $item['quantity']);
                    $child_items = DB::select("select child_id from dream_item_bind where parent_id=$parentId");
                    foreach ($child_items as $child_item){
                        array_push($collect_child_items, $child_item->child_id);
                    }
                    for ($i=0; $i<sizeof($parsedUnits); $i++){
                        if (!$dreamItem->checkForCycle($parentId, $child_id[0]->id)){
                            $unitId = DB::select("select id from dream_units where unit='$parsedUnits[$i]'");
                            if (!in_array($child_id[0]->id, $collect_child_items)){
                                DB::table("dream_item_bind")->insert(['parent_id'=>$parentId, 'child_id'=>$child_id[0]->id, 'quantity'=>$quantities[$i], 'unit_id'=>$unitId[0]->id]);
                            }
                        }
                        else{
                            throw new \Exception();
                        }

                    }
                }
            });
        }
        catch (\Exception $e){
            return redirect()->back()->with('status', 'ошибка при загрузке');
        }

        session()->forget('import_child_item');
        return redirect()->to(route('items'))->with('status', 'добавлено');
    }

    public function import(Request $request){
        $res = [];

        if($request->hasFile('file')){
            $path = $request->file('file')->getRealPath();
            $data = \Excel::load($path)->get();
            if($data->count()){
                $res = DB::transaction(function () use ($data){
                    $notExistArticul = array();

                    foreach ($data as $key => $value) {
                        $item_id = DB::table('dream_items')->where('articula_new', $value->articula_new)->get();
                        if (!empty($item_id->toArray())){
                            if (!empty($item_id->first())){
                                $c = DB::table('technologist_model_bind')->where('item_id', $item_id->first()->id)->get()->first();
                                if (empty($c)){
                                    DB::table('technologist_model_bind')->insert([
                                        'item_id'=>$item_id->first()->id,
                                        'item_code'=>$value->code
                                    ]);
                                }
                            }
                        }else{
                            array_push($notExistArticul, $value);
                        }
                    }
                    return $notExistArticul;
                });
            }
        }
        dd($res);
        return redirect()->back();
    }

    public function import_uchet(Request $request){
        $res = [];

        if($request->hasFile('file')){
            $path = $request->file('file')->getRealPath();
            $data = \Excel::load($path)->get();
            if($data->count()){
                $res = DB::transaction(function () use ($data){
                    $notExistArticul = array();

                    foreach ($data as $key => $value) {
                        $item_id = DB::table('dream_items')->where('articula_new', $value->articula_new)->get();
                        if (!empty($item_id->toArray())){
                            if (!empty($item_id->first())){
                                DB::update("update dream_transaction_list 
                                inner join dream_transaction_storage_bind on dream_transaction_storage_bind.transaction_id = dream_transaction_list.transaction_id
                                inner join dream_items on dream_items.id = dream_transaction_list.item_id
                                set dream_transaction_list.delete_time = now() 
                                where dream_transaction_storage_bind.storage_id = 4 and dream_items.articula_new = $value->articula_new");
                            }
                        }else{
                            array_push($notExistArticul, $value);
                        }
                    }
                    return $notExistArticul;
                });
            }
        }
        dd($res);
        return redirect()->back();
    }

//    public function import(Request $request){
//        if($request->hasFile('file')){
//            $path = $request->file('file')->getRealPath();
//            $data = \Excel::load($path)->get();
//            if($data->count()){
//                $res = DB::transaction(function () use ($data){
//                    $notExistArticul = array();
//
//                    foreach ($data as $key => $value) {
//                        $item_id = DB::table('dream_items')->where('articula_new', $value->articula)->get();
//                        if (!empty($item_id->toArray())){
//                            if (!empty($item_id->first())){
//                                $c = DB::table('dream_items_data')->where('item_id', $item_id->first()->id)->get()->first();
//                                if (empty($c)){
//                                    DB::table('dream_items_data')->insert([
//                                        'item_id'=>$item_id->first()->id,
//                                        'name'=>$value->name,
//                                        'hs_code'=>$value->hs_code,
//                                        'unit'=>$value->unit,
//                                    ]);
//                                }
//                            }
//                        }else{
//                            array_push($notExistArticul, $value);
//                        }
//                    }
//                    return $notExistArticul;
//                });
//                dd($res);
//            }
//        }
//        return redirect()->back();
//    }
}

<?php

namespace App\Http\Controllers;

use App\DreamItem;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Excel;
use PDF;
use Gate;
use Illuminate\Http\Request;
use Auth;
use Hash;

class HomeController extends Controller
{
    const POLUFABRIKAT = 26;
    const RADIATOR = 18;
    const OTHER_STORAGE_TYPE = 4;

    public function index()
    {
        return view('index', ['page'=>'home']);
    }

    public function showChangePasswordForm(){
        return view('auth.changepassword');
    }
    public function changePassword(Request $request){
        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }
        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }
        $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);

        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();
        return redirect()->route('home')->with("success","Password changed successfully !");
    }

    public function pdf_remaining(){
        return view('pdf.remaining');
    }

    public function pdf_remaining_generate($request_id){
        $managers_of_lines = [
            '1'=>'Содиков Б.',
            '2'=>'Бийменов М.',
            '3'=>'Набихожаев К.',
            '4'=>'Султонов О.',
            '8'=>'Мирзахмедов М.',
            '19'=>'Алибаев К.',
            '20'=>'Алибаев К.',
            '17'=>'___________________________',
            '6'=>'___________________________',
            '22'=>'Алибаев К.',
            '23'=>'Мирзаахмедов М.',
            '26'=>'Хужаназаров Ж.',
            '18'=>'Хусниддинов С.',
            '36'=>'Акрамов Б.',
            '162'=>'Акрамов Б.',
            '163'=>'Акрамов Б.',
            '34'=>'Акрамов Б.',
            '42'=>'Акрамов Б.',
            '35'=>'Азимов Ш.',
        ];
        $model = DB::table('dream_requests')
            ->leftJoin('dream_items', 'dream_items.id', '=', 'dream_requests.model_id')
            ->leftJoin('dream_storage_item_bind', 'dream_storage_item_bind.item_id', '=', 'dream_requests.model_id')
            ->leftJoin('dream_units', 'dream_units.id', '=', 'dream_storage_item_bind.unit_id')
            ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
            ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
            ->select('dream_items.articula_new', 'dream_items.item_name',  DB::raw("IFNULL(dream_items.sap_code, '') as sap_code"), 'dream_requests.*', 'from_storage.name as from_storage_name', 'from_storage.id as from_storage_id', 'to_storage.name as to_storage_name', 'to_storage.id as to_storage_id', 'dream_units.unit as unit_name')
            ->where('dream_requests.id', $request_id)
            ->first();
        $items = DB::table('dream_request_list')->join('dream_items', 'dream_items.id', '=', 'dream_request_list.item_id')
            ->join('dream_storage_item_bind', 'dream_storage_item_bind.item_id', '=', 'dream_request_list.item_id')
            ->join('dream_units', 'dream_units.id', '=', 'dream_storage_item_bind.unit_id')
            ->select('dream_items.articula_new', 'dream_items.item_name', DB::raw("IFNULL(dream_items.sap_code, '') as sap_code"), 'dream_request_list.*', 'dream_units.unit as unit_name')
            ->where([['dream_request_list.request_id', $request_id], ['dream_storage_item_bind.storage_id', $model->to_storage_id], ['dream_request_list.delete_time', null]])
            ->get();
        $num_of_request = DB::table('dream_requests')
            ->where([['create_time', '>=', Carbon::createFromFormat('Y-m-d H:i:s', $model->create_time)->startOfMonth()->format('Y-m-d H:i:s')], ['create_time', '<=', $model->create_time]])->count();
        $b = $num_of_request;
        $t=0;
        while ($b>1){
            $b/=10;
            $t++;
        }
        $pages = $this->breakToPages($items, $model);
        $date = Carbon::createFromFormat('Y-m-d H:i:s', $model->create_time);
        if (!is_null($model->from_storage_name)){
            $storage_type = DB::table('dream_storages')->where('dream_storages.id', $model->from_storage_id)->first();
            if ($storage_type->type_id==self::OTHER_STORAGE_TYPE){
                $data = [
                    'model' => $model,
                    'pages' => $pages,
                    'created_date'=>$date->format('d.m.Y'),
                    'created_time'=>$date->format('H:i'),
                    'production_line'=>$model->from_storage_name,
                    'storage_name'=>$model->to_storage_name,
                    'request_id'=>$model->id,
                    'manager_of_line'=>(isset($managers_of_lines[$model->from_storage_id]))?$managers_of_lines[$model->from_storage_id]:'___________________________',
                    'isService'=>true
                ];
            }else{
                $data = [
                    'model' => $model,
                    'pages' => $pages,
                    'created_date'=>$date->format('d.m.Y'),
                    'created_time'=>$date->format('H:i'),
                    'production_line'=>$model->from_storage_name,
                    'storage_name'=>$model->to_storage_name,
                    'request_id'=>$model->id,
                    'manager_of_line'=>"",//(!is_null($model->from_storage_id))?(isset($managers_of_lines[$model->from_storage_id]))?$managers_of_lines[$model->from_storage_id]:$model->description:$model->description,
                    'isService'=>false
                ];
            }

        }else{
            $data = [
                'model' => $model,
                'pages' => $pages,
                'created_date'=>$date->format('d.m.Y'),
                'created_time'=>$date->format('H:i'),
                'production_line'=>$model->description,
                'storage_name'=>$model->to_storage_name,
                'request_id'=>$model->id,
                'manager_of_line'=>'',
                'isService'=>true
            ];
        }

        //return view('pdf.request', $data);

        $pdf = PDF::loadView('pdf.request', $data);
//        $pdf->setEncryption('1','sherzod',array('copy'));
        return $pdf->stream('request.pdf', array("Attachment" => false));
    }

    public function breakToPages($items, $model){
        $arrayCount = count($items);
        $currentPage = 0;
        $rowCount = 0;
        $pages = [];
        $shouldBreak = false;
        if (!is_null($model)){
            $rowCount += ceil((float)(iconv_strlen($model->item_name, 'UTF-8')/68));
        }
        for ($i=0; $i<$arrayCount; $i++){
            $rowCount += ceil((float)(iconv_strlen($items[$i]->item_name, 'UTF-8')/68));
            if ($currentPage==0){
                if ($rowCount>34){
                    $shouldBreak = true;
                }
            }else{
                if ($rowCount>44){
                    $shouldBreak = true;
                }
            }
            if ($shouldBreak){
                $rowCount = 0;
                $i-=1;
                $currentPage++;
                $shouldBreak = false;
                continue;
            }else{
                $pages[$currentPage][] = $items[$i];
            }
        }
        return $pages;
    }

    public function pdf_remaining_generate_by_history($transaction_id){
        $managers_of_lines = [
            '1'=>'Содиков Б.',
            '2'=>'Бийменов М.',
            '3'=>'Набихожаев К.',
            '4'=>'Султонов О.',
            '8'=>'Мирзахмедов М.',
            '19'=>'Алибаев К.',
            '20'=>'Алибаев К.',
            '17'=>'___________________________',
            '6'=>'___________________________',
            '22'=>'Алибаев К.',
            '23'=>'Мирзаахмедов М.',
            '26'=>'Хужаназаров Ж.',
            '18'=>'Хусниддинов С.',
            '36'=>'Акрамов Б.',
            '34'=>'Акрамов Б.',
            '42'=>'Акрамов Б.',
            '162'=>'Акрамов Б.',
            '163'=>'Акрамов Б.',
        ];

        $managers_of_warehouse = [
            '4'=>'Султонов О.',
            '1'=>'Содиков Б.',
            '2'=>'Беминов М.',
            '8'=>'Мирзахмедов М'
        ];
        $transaction = DB::table('dream_transactions')
            ->join('dream_transaction_list', 'dream_transaction_list.transaction_id', '=', 'dream_transactions.id')
            ->join('dream_transaction_request_bind', 'dream_transaction_request_bind.transaction_list_id', '=', 'dream_transaction_list.id')
            ->join('dream_request_list', 'dream_request_list.id', '=', 'dream_transaction_request_bind.request_list_id')
            ->select('dream_request_list.request_id as request_id', 'dream_transactions.create_time')
            ->where('dream_transactions.id', $transaction_id)->get()->first();

        if (!is_null($transaction)){
            $request_id = $transaction->request_id;
            $model = DB::table('dream_requests')
                ->leftJoin('dream_items', 'dream_items.id', '=', 'dream_requests.model_id')
                ->leftJoin('dream_storage_item_bind', 'dream_storage_item_bind.item_id', '=', 'dream_requests.model_id')
                ->leftJoin('dream_units', 'dream_units.id', '=', 'dream_storage_item_bind.unit_id')
                ->join('dream_storages as to_storage', 'to_storage.id', '=', 'dream_requests.to_storage_id')
                ->leftJoin('dream_storages as from_storage', 'from_storage.id', '=', 'dream_requests.from_storage_id')
                ->select('dream_items.articula_new', 'dream_items.item_name', DB::raw("IFNULL(dream_items.sap_code, '') as sap_code"), 'dream_requests.*', 'from_storage.name as from_storage_name',
                    'from_storage.id as from_storage_id', 'to_storage.name as to_storage_name', 'to_storage.id as to_storage_id', 'dream_units.unit as unit_name')
                ->where('dream_requests.id', $request_id)
                ->first();

            $items = DB::table('dream_transaction_list')->join('dream_items', 'dream_items.id', '=', 'dream_transaction_list.item_id')
                ->join('dream_units', 'dream_units.id', '=', 'dream_transaction_list.unit_id')
                ->select('dream_items.articula_new', 'dream_items.item_name', DB::raw("IFNULL(dream_items.sap_code, '') as sap_code"), 'dream_transaction_list.*', 'dream_units.unit as unit_name')
                ->where('dream_transaction_list.transaction_id', $transaction_id)
                ->get();

            $num_of_request = DB::table('dream_requests')
                ->where([['create_time', '>=', Carbon::createFromFormat('Y-m-d H:i:s', $model->create_time)->startOfMonth()->format('Y-m-d H:i:s')], ['create_time', '<=', $model->create_time]])->count();
            $b = $num_of_request;
            $t=0;
            while ($b>1){
                $b/=10;
                $t++;
            }
            $pages = $this->breakToPages($items, $model);
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $transaction->create_time);
            if (!is_null($model->from_storage_id)){
                $storage_type = DB::table('dream_storages')->where('dream_storages.id', $model->from_storage_id)->first();
                if ($model->to_storage_id==self::POLUFABRIKAT || $model->to_storage_id==self::RADIATOR){
                    $data = [
                        'model' => $model,
                        'pages' => $pages,
                        'created_date'=>$date->format('d.m.Y'),
                        'created_time'=>$date->format('H:i'),
                        'production_line'=>$model->from_storage_name,
                        'storage_name'=>$model->to_storage_name,
                        'request_id'=>$model->id,
                        'manager_of_line'=>(!is_null($model->from_storage_id))?$managers_of_lines[$model->from_storage_id]:$model->description,
                        'manager_of_warehouse'=>(!is_null($model->to_storage_id))?(isset($managers_of_warehouse[$model->to_storage_id]))?$managers_of_warehouse[$model->to_storage_id]:'____________________':'___________________',
                        'isSpecial'=>true,
                        'hasDestination'=>true,
                        'storage_type'=>$storage_type->type_id
                    ];
                }else{
                    if ($storage_type->type_id==self::OTHER_STORAGE_TYPE){
                        $data = [
                            'model' => $model,
                            'pages' => $pages,
                            'created_date'=>$date->format('d.m.Y'),
                            'created_time'=>$date->format('H:i'),
                            'production_line'=>$model->from_storage_name,
                            'storage_name'=>$model->to_storage_name,
                            'request_id'=>$model->id,
                            'manager_of_line'=>(isset($managers_of_lines[$model->from_storage_id]))?$managers_of_lines[$model->from_storage_id]:'____________________',
                            'manager_of_warehouse'=>(!is_null($model->to_storage_id))?(isset($managers_of_warehouse[$model->to_storage_id]))?$managers_of_warehouse[$model->to_storage_id]:'____________________':'___________________',
                            'isSpecial'=>false,
                            'hasDestination'=>true,
                            'storage_type'=>$storage_type->type_id
                        ];
                    }else{
                        $data = [
                            'model' => $model,
                            'pages' => $pages,
                            'created_date'=>$date->format('d.m.Y'),
                            'created_time'=>$date->format('H:i'),
                            'production_line'=>$model->from_storage_name,
                            'storage_name'=>$model->to_storage_name,
                            'request_id'=>$model->id,
                            'manager_of_line'=>(!is_null($model->from_storage_id))?(isset($managers_of_lines[$model->from_storage_id]))?$managers_of_lines[$model->from_storage_id]:'____________________':$model->description,
                            'manager_of_warehouse'=>(!is_null($model->to_storage_id))?(isset($managers_of_warehouse[$model->to_storage_id]))?$managers_of_warehouse[$model->to_storage_id]:'____________________':'___________________',
                            'isSpecial'=>false,
                            'hasDestination'=>true,
                            'storage_type'=>$storage_type->type_id
                        ];
                    }
                }


            }else{
                $data = [
                    'model' => $model,
                    'pages' => $pages,
                    'created_date'=>$date->format('d.m.Y'),
                    'created_time'=>$date->format('H:i'),
                    'production_line'=>$model->description,
                    'storage_name'=>$model->to_storage_name,
                    'request_id'=>$model->id,
                    'manager_of_line'=>$model->description,
                    'manager_of_warehouse'=>(!is_null($model->to_storage_id))?(isset($managers_of_warehouse[$model->to_storage_id]))?$managers_of_warehouse[$model->to_storage_id]:'____________________':'___________________',
                    'hasDestination'=>false,
                    'storage_type'=>null
                ];
            }

            $pdf = PDF::loadView('pdf.request_history', $data);
//        $pdf->setEncryption('1','sherzod',array('copy'));
            return $pdf->stream('request_history.pdf', array("Attachment" => false));
        }


    }
}

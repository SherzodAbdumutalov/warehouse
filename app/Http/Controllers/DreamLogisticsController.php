<?php

namespace App\Http\Controllers;

use App\DreamLogistics;
use App\DreamOrder;
use App\DreamStorage;
use App\DreamSupplier;
use App\DreamTransaction;
use PDF;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Excel;
use Validator;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DreamLogisticsController extends Controller
{

    const STATE_ACTIVE = 1;
    const LINE = 2;
    const WAREHOUSE = 1;
    const IMPORT = 1;
    const LOGISTICS = 2;
    const ORDER = 1;
    const OUTPUT = 2;
    const INPUT = 1;
    const BUFFER = 3;
    const ADMIN = 1;

    public function createLogistics(){
        $orders = DB::table('dream_orders')->select('dream_orders.invoice_number')->where('dream_orders.state_id', self::STATE_ACTIVE)->get();
        $storages = DreamStorage::whereIn('type_id', array(self::BUFFER, 7))->where('state', 1)->get();
        $storages1 = DreamStorage::whereIn('type_id', array(self::BUFFER, 1))->get();
        $price_units = DB::table('dream_currencies')->get();
        return view('index', ['page'=>'create_logistics', 'orders'=>$orders, 'storages'=>$storages, 'storages1'=>$storages1, 'currencies'=>$price_units]);
    }

    public function do_array($invoices){
        $arr = array();
        foreach ($invoices as $invoice){
            $arr[] = (is_null($invoice->invoice_number))?'':$invoice->invoice_number;
        }
        return $arr;
    }

    public function printFormattedLogistics($request_id, $logistics_date){

        try {
            $logistics_id = $request_id;

            $query_text = "
            SELECT SQL_CALC_FOUND_ROWS  logistics.id, logistics.*, from_storage.name as from_storage_name, to_storage.name as to_storage_name, users.firstname as user_name, container_types.name as container_type_name,
            logistics_states.name as state_name, IFNULL(logistics_transaction.amount/logistics_list1.amount, 0) as percent, group_concat(orders.order_invoice, '') as order_invoice_number,
            group_concat(orders.shipment_time, '') as order_shipment_time,
            dream_suppliers.address as address
            FROM logistics
            inner join dream_storages as to_storage on to_storage.id=logistics.to_storage_id
            inner join dream_storages as from_storage on from_storage.id=logistics.from_storage_id
            left join container_types on container_types.id=logistics.container_type_id
            inner join users on users.id=logistics.user_id
            inner join logistics_states on logistics_states.id=logistics.state_id
            left join (
                SELECT sum(dream_transaction_list.amount) as amount, logistics_list.logistics_id as logistics_id FROM logistics_list
                inner join transaction_order_bind on transaction_order_bind.logistics_list_id=logistics_list.id
                inner join dream_transaction_list on dream_transaction_list.id=transaction_order_bind.transaction_list_id
                inner join dream_transaction_storage_bind on dream_transaction_storage_bind.transaction_id=dream_transaction_list.transaction_id
                where dream_transaction_storage_bind.operation_id=1 and dream_transaction_list.delete_time is null
                group by logistics_list.logistics_id
            ) as logistics_transaction on logistics_transaction.logistics_id=logistics.id
            inner join (
                select dream_orders.invoice_number as order_invoice, logistics_list.logistics_id as logistics_id, dream_orders.shipment_time from logistics_list
                inner join dream_order_list on dream_order_list.id=logistics_list.order_list_id
                inner join dream_orders on dream_orders.id=dream_order_list.order_id
                group by dream_orders.invoice_number, logistics_list.logistics_id, dream_orders.shipment_time
            ) as orders on orders.logistics_id=logistics.id
            inner join (
              select sum(logistics_list.amount) as amount, logistics_list.logistics_id as logistics_id from logistics_list
              group by logistics_list.logistics_id
            ) as logistics_list1 on logistics_list1.logistics_id=logistics.id
            left join dream_suppliers on dream_suppliers.name like concat('%', from_storage.name, '%')
            where logistics.id=$logistics_id  group by dream_suppliers.address";

            $logistics = DB::select($query_text);

            $details_query = " select `logistics_list`.*,
 `dream_order_list`.`item_id`,
 `dream_order_list`.`unit_id`,
 `dream_order_list`.`order_id`,
 `dream_order_list`.`amount` as `order_amount`,
 `dream_items`.`articula_new`,
 `dream_items`.`sap_code`,
 `dream_items`.`item_name`,
 `dream_units`.`unit` as `unit_name`,
 `dream_items_data`.`name` as `english_name`,
 `complete`.`input_amount` as `complete`,
 `last_rec`.`input_amount` as `last_rec`,
 `dream_orders`.`invoice_number` as
 `order_invoice`
 from `logistics_list`
 inner join `dream_order_list` on `dream_order_list`.`id` = `logistics_list`.`order_list_id`
 inner join `dream_orders` on `dream_orders`.`id` = `dream_order_list`.`order_id`
 inner join `dream_items` on `dream_items`.`id` = `dream_order_list`.`item_id`
 left join `dream_items_data` on `dream_items_data`.`item_id` = `dream_order_list`.`item_id`
 inner join `dream_units` on `dream_units`.`id` = `dream_order_list`.`unit_id`
 left join (select sum(dream_transaction_list.amount) as input_amount, transaction_order_bind.logistics_list_id from dream_transaction_list
		  inner join dream_transaction_storage_bind on dream_transaction_storage_bind.transaction_id=dream_transaction_list.transaction_id
		  inner join dream_transaction_types on dream_transaction_types.id=dream_transaction_storage_bind.operation_id
		  inner join transaction_order_bind on transaction_order_bind.transaction_list_id=dream_transaction_list.id
		  inner join dream_storages on dream_storages.id = dream_transaction_storage_bind.storage_id
		  where dream_transaction_list.delete_time is null and dream_transaction_types.operation=1 and dream_storages.type_id = 1
		  group by dream_transaction_list.item_id, dream_transaction_list.unit_id, transaction_order_bind.logistics_list_id)
          as complete on `complete`.`logistics_list_id` = `logistics_list`.`id`
left join (select sum(dream_transaction_list.amount) as input_amount, transaction_order_bind.logistics_list_id from dream_transaction_list
		  inner join dream_transaction_storage_bind on dream_transaction_storage_bind.transaction_id=dream_transaction_list.transaction_id
		  inner join dream_transaction_types on dream_transaction_types.id=dream_transaction_storage_bind.operation_id
		  inner join transaction_order_bind on transaction_order_bind.transaction_list_id=dream_transaction_list.id
		  inner join dream_storages on dream_storages.id = dream_transaction_storage_bind.storage_id
          inner join dream_transactions on dream_transactions.id = dream_transaction_list.transaction_id
		  where dream_transaction_list.delete_time is null
			and dream_transaction_types.operation=1
            and dream_storages.type_id = 1
            and date(dream_transactions.create_time) =date('$logistics_date')
		  group by dream_transaction_list.item_id, dream_transaction_list.unit_id, transaction_order_bind.logistics_list_id)
          as last_rec on `last_rec`.`logistics_list_id` = `logistics_list`.`id`
where (`logistics_list`.`logistics_id` = $logistics_id)";

            $details=DB::select($details_query);
            //DB::enableQueryLog();

//            $details = DB::table('logistics_list')
//                ->join('dream_order_list', 'dream_order_list.id', '=', 'logistics_list.order_list_id')
//                ->join('dream_orders', 'dream_orders.id', '=', 'dream_order_list.order_id')
//                ->join('dream_items', 'dream_items.id', '=', 'dream_order_list.item_id')
//                ->leftJoin('dream_items_data', 'dream_items_data.item_id', '=', 'dream_order_list.item_id')
//                ->join('dream_units', 'dream_units.id', '=', 'dream_order_list.unit_id')
//                ->leftJoin(DB::raw('(select sum(dream_transaction_list.amount) as input_amount, transaction_order_bind.logistics_list_id from dream_transaction_list
//            inner join dream_transaction_storage_bind on dream_transaction_storage_bind.transaction_id=dream_transaction_list.transaction_id
//            inner join dream_transaction_types on dream_transaction_types.id=dream_transaction_storage_bind.operation_id
//            inner join transaction_order_bind on transaction_order_bind.transaction_list_id=dream_transaction_list.id
//            inner join dream_storages on dream_storages.id = dream_transaction_storage_bind.storage_id
//            where dream_transaction_list.delete_time is null and dream_transaction_types.operation=1 and dream_storages.type_id = 1
//            group by dream_transaction_list.item_id, dream_transaction_list.unit_id, transaction_order_bind.logistics_list_id) as complete'),
//                    'complete.logistics_list_id', '=', 'logistics_list.id')
//                ->leftJoin(DB::raw('(select sum(dream_transaction_list.amount) as input_amount, transaction_order_bind.logistics_list_id from dream_transaction_list
//            inner join dream_transaction_storage_bind on dream_transaction_storage_bind.transaction_id=dream_transaction_list.transaction_id
//            inner join dream_transaction_types on dream_transaction_types.id=dream_transaction_storage_bind.operation_id
//            inner join transaction_order_bind on transaction_order_bind.transaction_list_id=dream_transaction_list.id
//            inner join dream_storages on dream_storages.id = dream_transaction_storage_bind.storage_id
//            inner join dream_transactions on dream_transactions.id = dream_transaction_list.transaction_id
//            where dream_transaction_list.delete_time is null and dream_transaction_types.operation=1 and dream_storages.type_id = 1 and date(dream_transactions.create_time) = date("'.$logistics_date.'")
//            group by dream_transaction_list.item_id, dream_transaction_list.unit_id, transaction_order_bind.logistics_list_id) as last_rec'), 'complete.logistics_list_id', '=', 'logistics_list.id')
//                ->select('logistics_list.*', 'dream_order_list.item_id', 'dream_order_list.unit_id', 'dream_order_list.order_id', 'dream_order_list.amount as order_amount',
//                    'dream_items.articula_new', 'dream_items.sap_code', 'dream_items.item_name', 'dream_units.unit as unit_name', 'dream_items_data.name as english_name',
//                    'complete.input_amount as complete', 'dream_orders.invoice_number as order_invoice', 'last_rec.input_amount as last_rec')
//                ->where([['logistics_list.logistics_id', $logistics_id]])->get();

            //dd(DB::getQueryLog());
            $items= array();
            $totalQuantity=0;
            foreach ($details as $item){
                $items[]=$item;
                $totalQuantity +=$item->amount;
            }
            $data = [
                'print_date'    => $logistics_date,
                'supplier'      =>$logistics[0],
                'items'         =>$items,
                'key'           =>0,
            ];
            //dd($data);
//
//            return view('pdf.packingList', [
//                'supplier' =>$logistics[0],
//                'items'    =>$items,
//                'key'      =>0
//            ]);

            $pdf = PDF::loadView('pdf.packingList', $data);

            return $pdf->stream("logistics.pdf", array("Attachment" => false));

        }catch (\Exception $e){
            return response()->json(['errors'=>array($e)], 500);
        }
    }

    public function getItemsOfBase(Request $request){
        try{
            $columns = array(
                0 => 'order_list_id',
                1 => 'articula_new',
                2 => 'sap_code',
                3 => 'item_name',
                4 => 'english_name',
                5 => 'invoice_number',
                6 => 'remaining',
                7 => 'input',
                8 => 'unit_name'
            );

            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');
            $from_storage_id = $request->fromStorage;
            $to_storage_id = $request->toStorage;
            $storage_type = DreamStorage::find($to_storage_id)->type_id;
            $invoice_numbers = $request->invoice_numbers;
            $filterText = '';
            if (!is_null($invoice_numbers)){
                $filterText = ' and dream_orders.invoice_number in (';
                foreach ($invoice_numbers as $invoice_number){
                    $filterText.="'".$invoice_number."',";
                }
                $filterText = substr($filterText, 0, strlen($filterText)-1);
                $filterText.=')';
            }

            if ($storage_type==1){
                $query_text = "SELECT dream_items.*, dream_units.unit as unit_name, sum(dream_transaction_list.amount*dream_transaction_types.operation) as remaining,
                    dream_orders.invoice_number, dream_orders.id as order_id, dream_orders.shipment_time, dream_order_list.id as order_list_id,
                    (select dream_items_data.name from dream_items_data where dream_items_data.item_id=dream_items.id limit 1) as english_name
                    FROM dream_transactions
                    inner join dream_transaction_storage_bind on dream_transaction_storage_bind.transaction_id=dream_transactions.id
                    inner join dream_transaction_list on dream_transaction_list.transaction_id=dream_transactions.id
                    inner join dream_transaction_types on dream_transaction_types.id=dream_transaction_storage_bind.operation_id
                    inner join transaction_order_bind on transaction_order_bind.transaction_list_id=dream_transaction_list.id
                    inner join dream_order_list on dream_order_list.id=transaction_order_bind.order_list_id
                    inner join dream_orders on dream_orders.id=dream_order_list.order_id
                    inner join dream_items on dream_items.id=dream_transaction_list.item_id
                    inner join dream_storage_item_bind on dream_storage_item_bind.item_id=dream_items.id
                    inner join dream_units on dream_units.id=dream_transaction_list.unit_id
                    where dream_transaction_list.delete_time is null and dream_transaction_storage_bind.storage_id=$from_storage_id and dream_storage_item_bind.storage_id=$to_storage_id
                    and dream_orders.state_id = 1
                ";
//                $query_text = "SELECT dream_items.*, dream_units.unit as unit_name, sum(dream_transaction_list.amount*dream_transaction_types.operation)-IFNULL(log.amount, 0) as remaining,
//                dream_orders.invoice_number, dream_orders.id as order_id, dream_orders.shipment_time, dream_order_list.id as order_list_id,
//                (select dream_items_data.name from dream_items_data where dream_items_data.item_id=dream_items.id limit 1) as english_name
//                FROM dream_transactions
//                inner join dream_transaction_storage_bind on dream_transaction_storage_bind.transaction_id=dream_transactions.id
//                inner join dream_transaction_list on dream_transaction_list.transaction_id=dream_transactions.id
//                inner join dream_transaction_types on dream_transaction_types.id=dream_transaction_storage_bind.operation_id
//                inner join transaction_order_bind on transaction_order_bind.transaction_list_id=dream_transaction_list.id
//                inner join dream_order_list on dream_order_list.id=transaction_order_bind.order_list_id
//                inner join dream_orders on dream_orders.id=dream_order_list.order_id
//                left join (select sum(logistics_list.amount) as amount, logistics_list.order_list_id from logistics_list
//                inner join logistics on logistics.id=logistics_list.logistics_id and logistics.from_storage_id=$from_storage_id group by logistics_list.order_list_id)
//                as log on log.order_list_id=dream_order_list.id
//                inner join dream_storages on dream_storages.id=dream_transaction_storage_bind.storage_id
//                inner join dream_items on dream_items.id=dream_transaction_list.item_id
//                inner join dream_storage_item_bind on dream_storage_item_bind.item_id=dream_items.id
//                inner join dream_units on dream_units.id=dream_transaction_list.unit_id
//                where dream_transaction_list.delete_time is null and dream_transaction_storage_bind.storage_id=$from_storage_id and dream_storage_item_bind.storage_id=$to_storage_id
//                ";
            }else{
                $query_text = "SELECT dream_items.*, dream_units.unit as unit_name, sum(dream_transaction_list.amount*dream_transaction_types.operation) as remaining,
                    dream_orders.invoice_number, dream_orders.id as order_id, dream_orders.shipment_time, dream_order_list.id as order_list_id,
                    (select dream_items_data.name from dream_items_data where dream_items_data.item_id=dream_items.id limit 1) as english_name
                    FROM dream_transactions
                    inner join dream_transaction_storage_bind on dream_transaction_storage_bind.transaction_id=dream_transactions.id
                    inner join dream_transaction_list on dream_transaction_list.transaction_id=dream_transactions.id
                    inner join dream_transaction_types on dream_transaction_types.id=dream_transaction_storage_bind.operation_id
                    inner join transaction_order_bind on transaction_order_bind.transaction_list_id=dream_transaction_list.id
                    inner join dream_order_list on dream_order_list.id=transaction_order_bind.order_list_id
                    inner join dream_orders on dream_orders.id=dream_order_list.order_id
                    inner join dream_items on dream_items.id=dream_transaction_list.item_id
                    inner join dream_units on dream_units.id=dream_transaction_list.unit_id
                    where dream_transaction_list.delete_time is null and dream_transaction_storage_bind.storage_id=$from_storage_id and dream_orders.state_id = 1
                ";
//                $query_text = "SELECT dream_items.*, dream_units.unit as unit_name, sum(dream_transaction_list.amount*dream_transaction_types.operation)-IFNULL(log.amount, 0) as remaining,
//                dream_orders.invoice_number, dream_orders.id as order_id, dream_orders.shipment_time, dream_order_list.id as order_list_id,
//                (select dream_items_data.name from dream_items_data where dream_items_data.item_id=dream_items.id limit 1) as english_name
//                FROM dream_transactions
//                inner join dream_transaction_storage_bind on dream_transaction_storage_bind.transaction_id=dream_transactions.id
//                inner join dream_transaction_list on dream_transaction_list.transaction_id=dream_transactions.id
//                inner join dream_transaction_types on dream_transaction_types.id=dream_transaction_storage_bind.operation_id
//                inner join transaction_order_bind on transaction_order_bind.transaction_list_id=dream_transaction_list.id
//                inner join dream_order_list on dream_order_list.id=transaction_order_bind.order_list_id
//                inner join dream_orders on dream_orders.id=dream_order_list.order_id
//                left join (select sum(logistics_list.amount) as amount, logistics_list.order_list_id from logistics_list
//                inner join logistics on logistics.id=logistics_list.logistics_id and logistics.from_storage_id=$from_storage_id group by logistics_list.order_list_id)
//                as log on log.order_list_id=dream_order_list.id
//                inner join dream_storages on dream_storages.id=dream_transaction_storage_bind.storage_id
//                inner join dream_items on dream_items.id=dream_transaction_list.item_id
//                inner join dream_units on dream_units.id=dream_transaction_list.unit_id
//                where dream_transaction_list.delete_time is null and dream_transaction_storage_bind.storage_id=$from_storage_id
//                ";
            }

            $search_text = '';
            if (!empty($request->input('search.value'))){
                $search = $request->input('search.value');
                $search_text .= "
                     and ( dream_items.articula_new like '%$search%' || dream_items.item_name like '%$search%' || dream_items.sap_code like '%$search%' || dream_orders.invoice_number like '%$search%' ||
                     dream_orders.shipment_time like '%$search%' )
                    ";
            }


            $query_text.=$search_text.$filterText." group by dream_transaction_list.item_id, dream_transaction_list.unit_id, dream_transaction_storage_bind.storage_id, dream_orders.id, dream_order_list.id " .
                " having (remaining)>0" .
                " order by $order $dir limit $limit offset $start";
            $items = DB::select($query_text);
            $totalCount = DB::select('select found_rows() as total_count')[0];
            $data = array();
            if (!empty($items)){
                foreach ($items as $key=>$item){
                    $nested['DT_RowId'] = $item->order_list_id;
                    $nested['order_list_id'] = $item->order_list_id;
                    $nested['articula_new'] = $item->articula_new;
                    $nested['item_name'] = $item->item_name;
                    $nested['english_name'] = $item->english_name;
                    $nested['sap_code'] = $item->sap_code;
                    $nested['invoice_number'] = $item->invoice_number;
                    $nested['unit_name'] = $item->unit_name;
                    $nested['remaining'] = $item->remaining;
                    $nested['input_field'] = "<input type='number' style='width: 100px' name='logistics_item_amount[]' id='$item->order_list_id' max='$item->remaining' onkeyup=\"if(parseFloat(this.value) >= parseFloat(this.max)) this.value = this.max; save_value_in_session(this);\" value='' placeholder='0'>" .
                        "<input type='checkbox' id='checkbox$item->order_list_id' value='$item->remaining' style='margin-left: 10px;' onclick=\"fillInput('".$item->order_list_id."', '".$item->remaining."')\">";
                    $data[] = $nested;
                }
            }

            $json_data = array(
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => intval($totalCount->total_count),
                "recordsFiltered" => intval($totalCount->total_count),
                "data"            => $data
            );

            return json_encode($json_data);
        }catch (\Exception $e){
            return response()->json(['errors'=>array($e)], 500);
        }
    }

    public function saveLogistics(Request $request){
        if (Gate::denies('save', new DreamOrder)){
            return \response()->json(['errors'=>array('нет доступа!')], 500);
        }
        $validateNames = array(
            'logistics_list'=>'элементы',
            'delivery_time'=>'дата прибытие',
            'description'=>'примечание',
            'fromStorage'=>'from storage',
            'toStorage'=>'to storage'
        );
        $validator = Validator::make($request->all(), [
            'fromStorage'=>'required',
            'toStorage'=>'required',
            'logistics_list'=>'required'
        ]);
        $validator->setAttributeNames($validateNames);

        if ($validator->fails()) {
            return response()->json(['errors'=>array($validator->errors()->all())], 500);
        }

        try{
            $res = DB::transaction(function () use($request){
                $items = json_decode($request->logistics_list, true);
                if (is_null($items)){
                    return response()->json(['errors'=>array('данные отсутствует')], 500);
                }
                $user_id = Auth::user()->id;
                if (is_null($request->invoice_number)){
                    $invoice_number = Carbon::now()->format('ymd');
                }else{
                    $invoice_number = $request->invoice_number;
                }
                $logistics_data = [
                    'user_id'=>$user_id,
                    'description'=>$request->description,
                    'container_number'=>$request->container_info,
                    'invoice_number'=>$invoice_number,
                    'to_storage_id'=>$request->toStorage,
                    'delivery_time'=>$request->delivery_time,
                    'from_storage_id'=>$request->fromStorage,
                    'logistics_price' => (float)$request->logistics_price,
                    'price_unit'=> (int)$request->price_unit,
                ];
                $log_id = DB::table('logistics')->insertGetId($logistics_data);
                if (is_null($request->invoice_number)){
                    DB::table('logistics')->where('id', $log_id)->update(['invoice_number'=>Carbon::now()->format('md').$log_id]);
                }

                $tr = [
                    'user_id'=>$user_id,
                    'description'=>$request->description
                ];
                $tr_id = DreamTransaction::create($tr)->id;
                $tr_s = [
                    'transaction_id'=>$tr_id,
                    'storage_id'=>$request->fromStorage,
                    'operation_id'=>2
                ];
                DB::table('dream_transaction_storage_bind')->insert($tr_s);
                foreach ($items as $order_list_id=>$value){
                    if ((float)$value>0){
                        $list_data = [
                            'order_list_id'=>$order_list_id,
                            'logistics_id'=>$log_id,
                            'amount'=>(float)$value,
                        ];
                        $logistics_list_id = DB::table('logistics_list')->insertGetId($list_data);
                        $item = DB::table('dream_order_list')->where('id', $order_list_id)->get()->first();
                        $tr_list = [
                            'transaction_id'=>$tr_id,
                            'item_id'=>$item->item_id,
                            'unit_id'=>$item->unit_id,
                            'amount'=>(float)$value
                        ];
                        $list_id = DB::table('dream_transaction_list')->insertGetId($tr_list);
                        $tr_order = [
                            'transaction_list_id'=>$list_id,
                            'order_list_id'=>$order_list_id,
                            'logistics_list_id'=>$logistics_list_id
                        ];
                        DB::table('transaction_order_bind')->insert($tr_order);
                    }
                }
                return $items;
            });
            return json_encode($res);

        }catch(\Exception $e){
            return \response()->json(['errors'=>array($e)], 500);
        }
    }

    public function index(){
        $fromStorages = DreamStorage::whereIn('type_id', array(self::BUFFER, 7))->get();
        $toStorages = DreamStorage::whereIn('type_id', array(self::BUFFER, 1))->get();
        $users = DB::table('logistics')
            ->join('users', 'users.id', '=', 'logistics.user_id')
            ->select('users.*')->groupBy('users.id')->get();

        $currencies = DB::table('dream_currencies')->get();

        $states = [
            1 => 'открытый',
            2 => 'закрытый'
        ];
        return view('index', ['page'=>'all_logistics', 'toStorages'=>$toStorages, 'fromStorages'=>$fromStorages, 'users'=>$users, 'states'=>$states, 'currencies' => $currencies]);
    }

    public function allLogistics(Request $request){
        try{
            $columns = array(
                0 => 'id',
                1 => 'percent',
                2 => 'percent',
                3 => 'delivery_time',
                4 => 'order_shipment_time',
                5 => 'from_storage_name',
                6 => 'to_storage_name',
                7 => 'invoice_number',
                8 => 'order_invoice_number',
                9 => 'container_number',
                10 => 'container_type_name',
                11 => 'user_name',
                12 => 'create_time',
            );

            $filterColumns = array(
                0 => 'id',
                'filter_by_state' => 'percent',
                2 => 'percent',
                'filter_by_from_storage' => 'from_storage.id',
                'filter_by_to_storage' => 'to_storage.id',
                5 => 'invoice_number',
                6 => 'order_invoice_number',
                7 => 'container_number',
                8 => 'container_type_name',
                9 => 'create_time',
                'filter_by_user' => 'users.id',
                11 => 'delivery_time'
            );

            $totalData = DB::table('logistics')->get()->count();
            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');
            $filterArr = json_decode($request->filterArr);
            $filter_str = '';
            if (!is_null($filterArr)){
                foreach ($filterArr as $key=>$values){
                    if (!empty($values)){
                        $filter_str.=" and $filterColumns[$key] in (";
                        foreach ($values as $value){
                            $filter_str.=$value.',';
                        }
                        $filter_str = substr($filter_str, 0, strlen($filter_str)-1).')';
                    }
                }
            }

            if (!is_null($request->stateFilter) && $request->stateFilter!=0){
                $filter_str.=" and logistics_states.id=".$request->stateFilter;
            }

            if ($request->l_upload_start && $request->l_upload_end){
                $filter_str.=" and logistics.delivery_time between date('$request->l_upload_start') and date('$request->l_upload_end')";
            }

            if ($request->l_order_start && $request->l_order_end){
                $filter_str.=" and orders.shipment_time between date('$request->l_order_start') and date('$request->l_order_end')";
            }

            $query_text = "
            SELECT SQL_CALC_FOUND_ROWS  logistics.id, logistics.*, from_storage.name as from_storage_name, to_storage.name as to_storage_name, users.firstname as user_name, container_types.name as container_type_name,
            logistics_states.name as state_name, IFNULL(logistics_transaction.amount/logistics_list1.amount, 0) as percent, group_concat(orders.order_invoice, '') as order_invoice_number,
            group_concat(orders.shipment_time, '') as order_shipment_time
            FROM logistics
            inner join dream_storages as to_storage on to_storage.id=logistics.to_storage_id
            inner join dream_storages as from_storage on from_storage.id=logistics.from_storage_id
            left join container_types on container_types.id=logistics.container_type_id
            inner join users on users.id=logistics.user_id
            inner join logistics_states on logistics_states.id=logistics.state_id
            left join (
                SELECT sum(dream_transaction_list.amount) as amount, logistics_list.logistics_id as logistics_id FROM logistics_list
                inner join transaction_order_bind on transaction_order_bind.logistics_list_id=logistics_list.id
                inner join dream_transaction_list on dream_transaction_list.id=transaction_order_bind.transaction_list_id
                inner join dream_transaction_storage_bind on dream_transaction_storage_bind.transaction_id=dream_transaction_list.transaction_id
                where dream_transaction_storage_bind.operation_id=1 and dream_transaction_list.delete_time is null
                group by logistics_list.logistics_id
            ) as logistics_transaction on logistics_transaction.logistics_id=logistics.id
            inner join (
                select dream_orders.invoice_number as order_invoice, logistics_list.logistics_id as logistics_id, dream_orders.shipment_time from logistics_list
                inner join dream_order_list on dream_order_list.id=logistics_list.order_list_id
                inner join dream_orders on dream_orders.id=dream_order_list.order_id
                group by dream_orders.invoice_number, logistics_list.logistics_id, dream_orders.shipment_time
            ) as orders on orders.logistics_id=logistics.id
            inner join (
              select sum(logistics_list.amount) as amount, logistics_list.logistics_id as logistics_id from logistics_list
              group by logistics_list.logistics_id
            ) as logistics_list1 on logistics_list1.logistics_id=logistics.id
            where logistics.id>0 ";
            $search_text = '';
            if (!empty($request->input('search.value'))){
                $search = $request->input('search.value');
                $search_text .= "
                     and ( to_storage.name like '%$search%' || from_storage.name like '%$search%' || logistics.invoice_number like '%$search%' || logistics.container_number like '%$search%'
                     || logistics.delivery_time like '%$search%' || users.firstname like '%$search%' || orders.order_invoice like '%$search%' || logistics.id like '%$search%' )
                    ";
            }

            $query_text.=$search_text.$filter_str." group by logistics.id order by $order $dir limit $limit offset $start;";
            $items = DB::select($query_text);

            $totalCount = DB::select('select found_rows() as total_count')[0];
            $data = array();

            if (!empty($items)){
                foreach ($items as $item)
                {
                    $nestedData['id'] = $item->id;
                    $nestedData['state_name'] = ($item->state_id==self::STATE_ACTIVE)?"<span class='label label-primary' style='font-size: 14px; padding-top: 0;'>".$item->state_name."</span>":"<span class='label label-danger' style='font-size: 14px; padding-top: 0;'>".$item->state_name."</span>";
                    if ($item->percent>=1){
                        $nestedData['percent'] = "<div class='progress progress-striped' style='height: 22px; padding: 0; margin: 0; background-color: #eababa'><div class='progress-bar progress-bar-danger' role='progressbar' aria-valuenow='100' aria-valuemin='0' aria-valuemax='0' style='width:100%; color: black'>".(round($item->percent, 3)*100)."<span>%</span></div></div>";
                    }else if ($item->percent>0 && $item->percent<1){
                        $nestedData['percent'] = "<div class='progress progress-striped' style='height: 22px; padding: 0; margin: 0; background-color: #eababa'><p style='position: absolute; color: black; font-weight: bold;'>".(round($item->percent, 3)*100)." %</p><div class='progress-bar progress-bar-stripped' role='progressbar' aria-valuenow='".(round($item->percent, 3)*100)."' aria-valuemin='0' aria-valuemax='0' style='width:".(round($item->percent, 3)*100)."%; text-align: center'></div></div>";
                    }else{
                        $nestedData['percent'] = "<div class='progress progress-striped' style='height: 22px; padding: 0; margin: 0; background-color: #eababa'><p style='position: absolute; color: black; font-weight: bold;'>0 %</p><div class='progress-bar progress-bar-stripped' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='0' style='width:".(round($item->percent, 3)*100)."%; text-align: center'></div></div>";
                    }
                    $nestedData['from_storage_name'] = $item->from_storage_name;
                    $nestedData['to_storage_name'] = $item->to_storage_name;
                    $nestedData['logistics_invoice_number'] = $item->invoice_number;
                    $nestedData['order_invoice_number'] = $item->order_invoice_number;
                    $nestedData['container_number'] = $item->container_number;
                    $nestedData['container_type'] = $item->container_type_name;
                    $nestedData['user_name'] = $item->user_name;
                    $nestedData['create_time'] = str_limit($item->create_time, 16, '');
                    $nestedData['shipment_time'] = str_limit($item->order_shipment_time, 10, '');
                    $nestedData['delivery_time'] = str_limit($item->delivery_time, 10, '');
                    $data[] = $nestedData;
                }
            }
            $json_data = array(
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => intval($totalData),
                "recordsFiltered" => intval($totalCount->total_count),
                "data"            => $data
            );

            return json_encode($json_data);
        }catch (\Exception $exception){
            return \response()->json(['errors'=>array($exception)], 500);
        }
    }

    public function editLogistics(Request $request){
        try{
            $logistics = DB::table('logistics')
                ->where('logistics.id', $request->logistics_id)->select('logistics.*')
                ->get()->first();
            return json_encode($logistics);
        }catch (\Exception $e){
            dd($e);
            return \response()->json(['errors'=>array($e)], 500);
        }
    }

    public function updateLogisticsList(Request $request){
        if (Gate::denies('save', new DreamOrder)){
            return \response()->json(['errors'=>array('нет доступа!')], 500);
        }
        $list_id = $request->log_list_id;
        $log = DB::table('logistics')
            ->join('logistics_list', 'logistics_list.logistics_id', '=', 'logistics.id')->where('logistics_list.id', $list_id)->first();
        $amount = $request->amount;
        $allow = true;
        if (Gate::denies('delete', new DreamOrder)){
            $allow = false;
        }
        try{
            if ($allow==self::ADMIN && $log->state_id==self::STATE_ACTIVE){
                if ($amount!=0){
                    $tr_bind = DB::table('transaction_order_bind')
                    ->join('dream_transaction_list', 'dream_transaction_list.id', '=', 'transaction_order_bind.transaction_list_id')
                    ->join('dream_transaction_storage_bind as dtsb', 'dtsb.transaction_id', '=', 'dream_transaction_list.transaction_id')
                    ->join('dream_storages', 'dream_storages.id', '=', 'dtsb.storage_id')
                    ->where([['logistics_list_id', $list_id], ['dream_storages.type_id', 7]])->get();
                    foreach ($tr_bind as $bind){
                        if (!is_null($bind->transaction_list_id)){
                            DB::table('dream_transaction_list')->where('id', $bind->transaction_list_id)->update(['amount'=>$amount]);
                        }
                    }
                    DB::table('logistics_list')->where('id', $list_id)->update(['amount'=>$amount]);
                }else{
                    return \response()->json(['errors'=>array('недопустимое значение!!')], 500);
                }

            }else{
                return \response()->json(['errors'=>array('вы неможете обнавить!')], 500);
            }

            return json_encode($list_id);
        }catch (\Exception $exception){
            return \response()->json(['errors'=>array($exception)], 500);
        }
    }

    public function getNotConsistedItemsOfLogistics(Request $request){
        $logistics_id = $request->logistics_id;
        $supplier_id = DB::table('logistics')->where('logistics.id', $logistics_id)->get()->first()->from_storage_id;
        $items = DB::select("
            select dream_items.* from dream_transaction_storage_bind
            inner join dream_transaction_list on dream_transaction_list.transaction_id=dream_transaction_storage_bind.transaction_id
            inner join dream_transaction_types on dream_transaction_types.id=dream_transaction_storage_bind.operation_id
            inner join dream_items on dream_items.id=dream_transaction_list.item_id
            where dream_transaction_list.item_id not in (select dream_order_list.item_id from logistics_list
            inner join dream_order_list on dream_order_list.id=logistics_list.order_list_id where logistics_list.logistics_id=$logistics_id)
            and dream_transaction_storage_bind.storage_id=$supplier_id and dream_transaction_list.delete_time is null
            group by dream_transaction_list.id having sum(dream_transaction_list.amount*dream_transaction_types.operation)>0
        ");
        return json_encode($items);
    }

    public function getCurrentItemInvoice(Request $request){
        $logistics_id = $request->logistics_id;
        $item_id = $request->item_id;
        $supplier_id = DB::table('logistics')->where('logistics.id', $logistics_id)->get()->first()->from_storage_id;
        $items = DB::select("
            select dream_items.* from dream_transaction_storage_bind
            inner join dream_transaction_list on dream_transaction_list.transaction_id=dream_transaction_storage_bind.transaction_id
            inner join dream_transaction_types on dream_transaction_types.id=dream_transaction_storage_bind.operation_id
            inner join dream_items on dream_items.id=dream_transaction_list.item_id
            where dream_transaction_list.item_id not in (select dream_order_list.item_id from logistics_list
            inner join dream_order_list on dream_order_list.id=logistics_list.order_list_id where logistics_list.logistics_id=$logistics_id)
            and dream_transaction_storage_bind.storage_id=$supplier_id and dream_transaction_list.item_id=$item_id and dream_transaction_list.delete_time is null
            group by dream_transaction_list.id having sum(dream_transaction_list.amount*dream_transaction_types.operation)>0
        ");
        return json_encode($items);
    }

    public function updateLogistics(Request $request){
        $allowPriceOnly=false;
        $allowAll=false;

        if(Gate::denies('enterLogisticsPrices', new DreamOrder)){
            return \response()->json(['errors'=>array('нет доступа!')], 500);
        }

        //dd(Auth::user()->canDo('ENTER_LOGISTICS_PRICE', true));
        //dd($allowPriceOnly);

        if (Gate::denies('save', new DreamOrder)){
            $allowPriceOnly=true;
        }
        else{
            $allowAll=true;
        }
        //dd($allowPriceOnly);
        //dd($allowAll);
        $validateNames = array(
            'edit_invoice_number'=>'invoice номер',
            'delivery_time'=>'дата прибытие',
            'description'=>'примечание',
            'truck_number'=>'номер транспорта'
        );
        $validator = Validator::make($request->all(), [
            'delivery_time' => 'required',
            'invoice_number' => 'required'
        ]);
        $validator->setAttributeNames($validateNames);
        if ($validator->fails()) {
            return response()->json(['errors'=>array($validator->errors()->all())], 500);
        }

        try{
            $res=null;
            if ($allowAll){
                $res = DB::transaction(function () use ($request){
                    $user_id = Auth::user()->id;
                    $data = [
                        'invoice_number'=>$request->invoice_number,
                        'description'=>$request->description,
                        'container_number'=>$request->truck_number,
                        'delivery_time'=>$request->delivery_time,
                        'logistics_price' => $request->logistics_price,
                        'price_unit' => $request->price_unit,
                    ];
                    $logistics = DB::table('logistics')->where('id', $request->logistics_id)->first();
                    if ($logistics->user_id==$user_id){
                        DB::table('logistics')->where('id', $request->logistics_id)->update($data);
                    }else{
                        return \response()->json(['errors'=>array('вы не можете изменить!')], 500);
                    }
                    return $logistics;
                });
            }
            elseif ($allowPriceOnly){
                //dd($request->logistics_price);
                $res = DB::transaction(function () use ($request){
                    $data = [
                        'logistics_price' => $request->logistics_price,
                        'price_unit' => $request->price_unit,
                    ];
                    $logistics = DB::table('logistics')->where('id', $request->logistics_id)->first();

                    DB::table('logistics')->where('id', $request->logistics_id)->update($data);

                    return $logistics;
                });
            }
            return json_encode($res);
        }catch(\Exception $e){
            return \response()->json(['errors'=>array('невозможно изменить!', $e)], 500);
        }
    }

    public function closeLogistics(Request $request){
        if (Gate::denies('save', new DreamOrder)){
            return \response()->json(['errors'=>array('нет доступа!')], 500);
        }

        try{
            $log = DB::table('logistics')->where('logistics.id', $request->logistics_id)->first();
            if ($log->user_id==Auth::user()->id){
                $items = DB::select("
                    SELECT IFNULL(sum(dream_transaction_list.amount*dream_transaction_types.operation), 0) as remaining, logistics_list.amount, logistics_list.id, fs.tr_list_id
                    FROM logistics_list
                    left join transaction_order_bind on transaction_order_bind.logistics_list_id = logistics_list.id
                    left join dream_transaction_list on dream_transaction_list.id = transaction_order_bind.transaction_list_id
                    left join dream_transaction_storage_bind on dream_transaction_storage_bind.transaction_id = dream_transaction_list.transaction_id and dream_transaction_storage_bind.storage_id=$log->to_storage_id
                    left join dream_transaction_types on dream_transaction_types.id = dream_transaction_storage_bind.operation_id
                    left join (
                    select tob.logistics_list_id, dtl.id as tr_list_id from transaction_order_bind tob
                    inner join dream_transaction_list dtl on dtl.id=tob.transaction_list_id
                    inner join dream_transaction_storage_bind dtsb on dtsb.transaction_id=dtl.transaction_id
                    inner join dream_transaction_types dtt on dtt.id=dtsb.operation_id
                    where dtsb.storage_id=$log->from_storage_id and dtt.operation=-1 and dtl.delete_time is null
                    group by tob.logistics_list_id, dtl.id
                    ) as fs on fs.logistics_list_id=logistics_list.id
                    where logistics_list.logistics_id=$request->logistics_id and dream_transaction_list.delete_time is null
                    group by logistics_list.id, fs.tr_list_id
                ");
                foreach ($items as $item){
                    $diff = $item->amount - $item->remaining;
                    if (!is_null($item->tr_list_id)){
                        if ($diff>0){
                            DB::table('dream_transaction_list')->where('id', $item->tr_list_id)->update(['amount'=>$item->remaining]);
                        }
                    }
                }

                DB::table('logistics')->where('id', $request->logistics_id)->update(['state_id'=>2]);
            }else{
                return \response()->json(['errors'=>array('вы неможете закрыть!')], 500);
            }
            return json_encode($items);
        }catch (\Exception $e){
            return \response()->json(['errors'=>array('не возможно закрыть!', $e)], 500);
        }
    }

    public function deleteLogistics(Request $request){
        if (Gate::denies('save', new DreamOrder)){
            return \response()->json(['errors'=>array('нет доступа!')], 500);
        }
        try{
            $tr_item_count = DB::table('transaction_order_bind')->join('logistics_list', 'logistics_list.id', '=', 'transaction_order_bind.logistics_list_id')
                ->where('logistics_list.logistics_id', $request->logistics_id)->get()->count();
            if ($tr_item_count>0){
                return \response()->json(['errors'=>array('невозможно удалить, участвует в транзакции!')], 500);
            }
            else{
                $logistics = DB::table('logistics')->where('id', $request->logistics_id)->first();
                if ($logistics->user_id==Auth::user()->id){
                    DB::table('logistics')->where('id', $request->logistics_id)->delete();
                }else{
                    return \response()->json(['errors'=>array('вы неможете удалить!')], 500);
                }
            }
            return json_encode($tr_item_count);
        }catch (\Exception $exception){
            return \response()->json(['errors'=>array('не возможно удалить!', $exception)], 500);
        }
    }

    public function showLogisticsList(Request $request){
        $allow = true;
        $hideLogisticsPrice=false;
        if (Gate::denies('delete', new DreamOrder)){
            $allow = false;
        }
        if (Gate::denies('viewLogisticsPricePerUnit', new DreamOrder)){
            $hideLogisticsPrice=true;
        }


        $logistics_id = $request->logistics_id;
        $logistics = DB::table('logistics')
            //->join('dream_currencies', 'dream_currencies.id', '=', 'logistics.price_unit')
            ->where('logistics.id', $logistics_id)->get(['logistics.*'])->first();
        if ($logistics->price_unit){
            $unit = DB::table('dream_currencies')->where('id', $logistics->price_unit)->get()->first();
            //dd($unit);
            $logistics->currency = $unit->name;
        }
        else{
            $logistics->currency = null;
        }
        $allowToSave = true;
        $allowToClose = true;
        $allowToDelete = true;
        $items = DB::table('logistics_list')
            ->join('dream_order_list', 'dream_order_list.id', '=', 'logistics_list.order_list_id')
            ->join('dream_orders', 'dream_orders.id', '=', 'dream_order_list.order_id')
            ->join('dream_items', 'dream_items.id', '=', 'dream_order_list.item_id')
            ->leftJoin('dream_items_data', 'dream_items_data.item_id', '=', 'dream_order_list.item_id')
            ->join('dream_units', 'dream_units.id', '=', 'dream_order_list.unit_id')
            ->leftJoin(DB::raw('(select sum(dream_transaction_list.amount) as input_amount, transaction_order_bind.logistics_list_id from dream_transaction_list
            inner join dream_transaction_storage_bind on dream_transaction_storage_bind.transaction_id=dream_transaction_list.transaction_id
            inner join dream_transaction_types on dream_transaction_types.id=dream_transaction_storage_bind.operation_id
            inner join transaction_order_bind on transaction_order_bind.transaction_list_id=dream_transaction_list.id
            inner join dream_storages on dream_storages.id = dream_transaction_storage_bind.storage_id
            where dream_transaction_list.delete_time is null and dream_transaction_types.operation=1 and dream_storages.type_id = 1
            group by dream_transaction_list.item_id, dream_transaction_list.unit_id, transaction_order_bind.logistics_list_id) as complete'),
                'complete.logistics_list_id', '=', 'logistics_list.id')
            ->select('logistics_list.*', 'dream_order_list.item_id', 'dream_order_list.price_per_unit', 'dream_order_list.gross_weight','dream_order_list.unit_id', 'dream_order_list.order_id', 'dream_order_list.amount as order_amount',
                'dream_items.articula_new', 'dream_items.sap_code', 'dream_items.item_name', 'dream_units.unit as unit_name', 'dream_items_data.name as english_name',
                'complete.input_amount as complete', 'dream_orders.invoice_number as order_invoice')
            ->where([['logistics_list.logistics_id', $logistics_id]])->get();

        $total_logistics_gross_weight = 0;
        $data = array();
        if (!empty($items)){



            foreach ($items as $item){
                $total_logistics_gross_weight += $item->gross_weight * ($item->amount / $item->order_amount);
            }

            foreach ($items as $item)
            {
                $calculated_weight = round($item->gross_weight * ($item->amount / $item->order_amount), 3);
                $calculated_logistics_price = 0;

                if (is_numeric($total_logistics_gross_weight) && !is_null($total_logistics_gross_weight) && $total_logistics_gross_weight > 0 && $logistics->logistics_price > 0)
                {
                    $calculated_logistics_price =  round(($calculated_weight / $total_logistics_gross_weight) * $logistics->logistics_price, 4);
                }


                $nestedData['id'] = $item->id;
                $nestedData['DT_RowId'] = $item->id;
                $nestedData['articula_new'] = $item->articula_new;
                $nestedData['sap_code'] = $item->sap_code;
                $nestedData['item_name'] = $item->item_name;
                $nestedData['english_name'] = $item->english_name;
                $nestedData['order_invoice'] = $item->order_invoice;
                $nestedData['amount'] = $item->amount;
                $nestedData['complete'] = $item->complete;
                $nestedData['unit_name'] = $item->unit_name;
                $nestedData['price_per_unit'] = round($item->price_per_unit, 3);
                $nestedData['total_price'] = round($item->amount * $item->price_per_unit, 3);

                if($hideLogisticsPrice){
                    $nestedData['gross_weight'] = null;
                    $nestedData['gross_weight_per_unit'] = null;
                    $nestedData['logistics_price'] = null;
                    $nestedData['logistics_price_per_unit'] = null;
                    $nestedData['logistics_price_unit'] = null;
                }
                else{
                    $nestedData['gross_weight'] = $calculated_weight;
                    $nestedData['gross_weight_per_unit'] = $calculated_weight / $item->amount;
                    $nestedData['logistics_price'] = $calculated_logistics_price;
                    $nestedData['logistics_price_per_unit'] = $calculated_logistics_price / $item->amount;
                    $nestedData['logistics_price_unit'] = $logistics->currency;
                }

                if ($allow){
                    if ($logistics->state_id==self::STATE_ACTIVE){
                        $nestedData['buttons'] = "<button type='button' data-toggle='modal' data-backdrop='static' data-keyboard='false' data-target='#edit_amount_modal' class='btn btn-info' onclick='set_list_data(".$item->id.")'><i class='fa fa-edit'></i></button>" .
                            "<button type='button' class='btn btn-danger' onclick=\"delete_item($item->id)\" ><i class='fa fa-trash'></i></button>";
                    }else{
                        $nestedData['buttons'] = "";
                    }
                }else{
                    $nestedData['buttons'] = "";
                }
                $data[] = $nestedData;
            }
        }

        $logisticsToStorageType = DB::table('dream_storages')->where('dream_storages.id', $logistics->to_storage_id)->first();
        if ($logistics->user_id!=Auth::user()->id){
            $allowToSave = false;
            $allowToClose = false;
            $allowToDelete = false;
        }else{
            if ($logistics->state_id==1){
                if ($logisticsToStorageType->type_id==1){
                    $allowToSave = false;
                }else{
                    $involveInTr = DB::table('logistics_list')
                        ->join('transaction_order_bind', 'transaction_order_bind.logistics_list_id','=', 'logistics_list.id')
                        ->where('logistics_list.logistics_id', $logistics_id)->count();
                    if ($involveInTr>0){
                        $allowToSave = true;
                        $allowToClose = true;
                        $allowToDelete = false;
                    }else{
                        $allowToSave = true;
                        $allowToClose = true;
                        $allowToDelete = true;
                    }
                }
            }else{
                $allowToSave = false;
                $allowToClose = false;
                $allowToDelete = false;
            }
        }


        $json = [
            'total_gross_weight' => $total_logistics_gross_weight,
            'data'=>$data,
            'state'=>$logistics->state_id,
            'logistics_info'=>$logistics,
            'allowToSave'=>$allowToSave,
            'allowToClose'=>$allowToClose,
            'allowToDelete'=>$allowToDelete
        ];
        return json_encode($json);
    }

    public function editLogisticsList(Request $request){
        try{
            $lgs = DB::select("
            select sum(dream_transaction_list.amount*dream_transaction_types.operation)+logistics_list.amount as remaining, logistics_list.amount from logistics_list
            inner join logistics on logistics.id=logistics_list.logistics_id
            inner join transaction_order_bind on transaction_order_bind.order_list_id=logistics_list.order_list_id
            inner join dream_transaction_list on dream_transaction_list.id=transaction_order_bind.transaction_list_id
            inner join dream_transaction_storage_bind on dream_transaction_storage_bind.transaction_id=dream_transaction_list.transaction_id and dream_transaction_storage_bind.storage_id=logistics.from_storage_id
            inner join dream_transaction_types on dream_transaction_types.id=dream_transaction_storage_bind.operation_id
            where logistics_list.id=$request->logistics_list_id
            group by logistics_list.order_list_id
            ");
            if (isset($lgs[0])){
                return json_encode($lgs[0]);
            }else{
                return \response()->json(['errors'=>array('ошибка')], 500);
            }
        }catch (\Exception $e){
            return \response()->json(['errors'=>array($e)], 500);
        }
    }

    public function deleteLogisticsListItem(Request $request){
        $allow = true;
        if (Gate::denies('delete', new DreamOrder)){
            $allow = false;
        }
        $list_id = $request->logistics_list_id;
        try{
            $log = DB::table('logistics')
                ->join('logistics_list', 'logistics_list.logistics_id', '=', 'logistics.id')
                ->select('logistics.*')->where('logistics_list.id', $list_id)->groupBy('logistics.id')->first();
            if (!is_null($log)){
                if ($log->state_id==2 || !$allow){
                    return \response()->json(['errors'=>array('невозможно удалить!')], 500);
                }

                $tr_binds = DB::table('transaction_order_bind')->where('logistics_list_id', $list_id)->get();
                foreach ($tr_binds as $bind){
                    if (is_null($bind->transaction_list_id)){
                        DB::table('dream_transaction_list')->where('id', $bind->transaction_list_id)->delete();
                    }
                }
                DB::table('logistics_list')->where('logistics_list.id', $list_id)->delete();

            }else{
                return \response()->json(['errors'=>array('пустое значение!')], 500);
            }
            return json_encode($list_id);
        }catch (\Exception $exception){
            return \response()->json(['errors'=>array($exception)], 500);
        }
    }

    public function saveArrivedLogistics(Request $request){
        if (Gate::denies('save', new DreamOrder)){
            return \response()->json(['errors'=>array('нет доступа!')], 500);
        }
        try{
            $result = DB::transaction(function () use($request){
                $log = DB::table('logistics')->where('id', $request->logistics_id)->get()->first();
                if ($log->state_id==2){
                    return \response()->json(['errors'=>array('уже сохранено!')], 500);
                }
                $data = [
                    'user_id'=>Auth::user()->id,
                    'description'=>$log->description
                ];
                $transaction_id = DreamTransaction::create($data)->id;

                $items = DB::table('logistics_list')
                    ->join('dream_order_list', 'dream_order_list.id', '=', 'logistics_list.order_list_id')
                    ->join('dream_items', 'dream_items.id', '=', 'dream_order_list.item_id')
                    ->join('dream_units', 'dream_units.id', '=', 'dream_order_list.unit_id')
                    ->select('logistics_list.*', 'dream_order_list.item_id', 'dream_order_list.unit_id', 'dream_order_list.order_id', 'dream_order_list.amount as order_amount',
                        'dream_items.articula_new', 'dream_items.sap_code', 'dream_items.item_name', 'dream_units.unit as unit_name')
                    ->where([['logistics_list.logistics_id', $request->logistics_id]])->get();

//                if (!is_null($log->from_storage_id)){
//                    $output_data = [
//                        'transaction_id'=>$transaction_id,
//                        'storage_id'=>$log->from_storage_id,
//                        'operation_id'=>self::OUTPUT
//                    ];
//                    DB::table('dream_transaction_storage_bind')->insert($output_data);
//                }
                $input_data = [
                    'transaction_id'=>$transaction_id,
                    'storage_id'=>$log->to_storage_id,
                    'operation_id'=>self::INPUT
                ];
                DB::table('dream_transaction_storage_bind')->insert($input_data);
                foreach ($items as $item){
                    $data = [
                        'transaction_id'=>$transaction_id,
                        'item_id'=>$item->item_id,
                        'unit_id'=>$item->unit_id,
                        'amount'=>$item->amount
                    ];
                    $transaction_logistics_order_list_id = DB::table('dream_transaction_list')->insertGetId($data);

                    $logistics_transaction = [
                        'order_list_id'=>$item->order_list_id,
                        'transaction_list_id'=>$transaction_logistics_order_list_id,
                        'logistics_list_id'=>$item->id
                    ];
                    DB::table('transaction_order_bind')->insert($logistics_transaction);
                }
                DB::table('logistics')->where('id', $request->logistics_id)->update(['state_id'=>2]);
                return $transaction_id;
            });
            return response()->json($result, 200);

        }catch(\Exception $e){
            return json_encode(['errors'=>array('ошибка')], 500);
        }
    }

//    public function get_orders_belongs_buffer_storage(Request $request){
//        $input_fields = session()->get('input_fields_buffer_storage');
//        $columns = array(
//            0 => 'articula_new',
//            1 => 'sap_code',
//            2 => 'item_name',
//            3 => 'english_name',
//            4 => 'amount',
//            5 => 'complete',
//            6 => 'input_field',
//            7 => 'unit',
//            8 => 'group_row',
//        );
//
//        $totalData = count($all_orders);
//        $limit = $request->input('length');
//        $start = $request->input('start');
//        $order = 'create_time';
//        $dir = 'desc';
//        if (empty($request->input('search.value'))){
//            $items = DB::table('dream_order_list')
//                ->join('dream_orders', 'dream_orders.id', '=', 'dream_order_list.order_id')
//                ->join('dream_storages', 'dream_storages.id', '=', 'dream_order_list.storage_id')
//                ->leftJoin('dream_items_data', 'dream_items_data.item_id', '=', 'dream_order_list.item_id')
//                ->join('dream_items', 'dream_items.id', '=', 'dream_order_list.item_id')
//                ->join('dream_units', 'dream_units.id', '=', 'dream_order_list.unit_id')
//                ->join('users', 'users.id', '=', 'dream_orders.user_id')
//                ->leftJoin('dream_suppliers', 'dream_suppliers.id', '=', 'dream_orders.supplier_id')
//                ->select('dream_orders.*', 'users.firstname as customer', 'dream_suppliers.name as supplier',
//                    'dream_order_list.id as order_list_id', 'dream_order_list.amount', 'dream_items_data.name as english_name',
//                    'dream_items.articula_new', 'dream_items.articula_old', 'dream_items.sap_code', 'dream_items.item_name', 'dream_units.unit',
//                    DB::raw('(select sum(dream_transaction_list.amount) from dream_transaction_list
//                                       where dream_transaction_list.id in (select dream_order_transaction_bind.transaction_list_id from dream_order_transaction_bind
//                                       where dream_order_transaction_bind.order_list_id=dream_order_list.id) group by dream_transaction_list.unit_id) as complete')
//                )->where([['dream_orders.state', self::STATE_ACTIVE], ['dream_order_list.storage_id', $request->storage_id], ['dream_orders.order_type', 1]])
//                ->limit($limit)->offset($start)->orderBy($order, $dir)->get();
//
//            $filteredData = DB::table('dream_order_list')
//                ->join('dream_orders', 'dream_orders.id', '=', 'dream_order_list.order_id')
//                ->join('dream_storages', 'dream_storages.id', '=', 'dream_order_list.storage_id')
//                ->leftJoin('dream_items_data', 'dream_items_data.item_id', '=', 'dream_order_list.item_id')
//                ->join('dream_items', 'dream_items.id', '=', 'dream_order_list.item_id')
//                ->join('dream_units', 'dream_units.id', '=', 'dream_order_list.unit_id')
//                ->join('users', 'users.id', '=', 'dream_orders.user_id')
//                ->leftJoin('dream_suppliers', 'dream_suppliers.id', '=', 'dream_orders.supplier_id')
//                ->select('dream_orders.*', 'users.firstname as customer', 'dream_suppliers.name as supplier',
//                    'dream_order_list.id as order_list_id', 'dream_order_list.amount', 'dream_items_data.name as english_name',
//                    'dream_items.articula_new', 'dream_items.articula_old', 'dream_items.sap_code', 'dream_items.item_name', 'dream_units.unit',
//                    DB::raw('(select sum(dream_transaction_list.amount) from dream_transaction_list
//                                       where dream_transaction_list.id in (select dream_order_transaction_bind.transaction_list_id from dream_order_transaction_bind
//                                       where dream_order_transaction_bind.order_list_id=dream_order_list.id) group by dream_transaction_list.unit_id) as complete')
//                )->where([['dream_orders.state', self::STATE_ACTIVE], ['dream_order_list.storage_id', $request->storage_id], ['dream_orders.order_type', 1]])
//                ->get();
//            $totalFiltered = count($filteredData);
//        }else{
//            $search = $request->input('search.value');
//            $items = DB::table('dream_order_list')
//                ->join('dream_orders', 'dream_orders.id', '=', 'dream_order_list.order_id')
//                ->join('dream_storages', 'dream_storages.id', '=', 'dream_order_list.storage_id')
//                ->leftJoin('dream_items_data', 'dream_items_data.item_id', '=', 'dream_order_list.item_id')
//                ->join('dream_items', 'dream_items.id', '=', 'dream_order_list.item_id')
//                ->join('dream_units', 'dream_units.id', '=', 'dream_order_list.unit_id')
//                ->join('users', 'users.id', '=', 'dream_orders.user_id')
//                ->leftJoin('dream_suppliers', 'dream_suppliers.id', '=', 'dream_orders.supplier_id')
//                ->select('dream_orders.*', 'users.firstname as customer', 'dream_suppliers.name as supplier',
//                    'dream_order_list.id as order_list_id', 'dream_order_list.amount', 'dream_items_data.name as english_name',
//                    'dream_items.articula_new', 'dream_items.articula_old', 'dream_items.sap_code', 'dream_items.item_name', 'dream_units.unit',
//                    DB::raw('(select sum(dream_transaction_list.amount) from dream_transaction_list
//                                       where dream_transaction_list.id in (select dream_order_transaction_bind.transaction_list_id from dream_order_transaction_bind
//                                       where dream_order_transaction_bind.order_list_id=dream_order_list.id) group by dream_transaction_list.unit_id) as complete')
//                )
//                ->where(function ($query) use($search){
//                    $query->orWhere('users.firstname', 'like', "%$search%")->orWhere('dream_orders.create_time', 'like', "%$search%")
//                        ->orWhere('dream_suppliers.name', 'like', "%$search%")->orWhere('dream_orders.invoice_number', 'like', "%$search%")->orWhere('dream_orders.container_info', 'like', "%$search%")
//                        ->orWhere('dream_orders.delivery_time', 'like', "%$search%")->orWhere('dream_orders.id', 'like', "%$search%")
//                        ->orWhere('dream_items.articula_new', 'like', "%$search%")->orWhere('dream_items.articula_old', 'like', "%$search%")->orWhere('dream_items.sap_code', 'like', "%$search%")
//                        ->orWhere('dream_items.item_name', 'like', "%$search%");
//                })
//                ->where([['dream_orders.state', self::STATE_ACTIVE], ['dream_order_list.storage_id', $request->storage_id], ['dream_orders.order_type', 1]])
//                ->limit($limit)->offset($start)->orderBy($order, $dir)->get();
//
//            $filteredData = DB::table('dream_order_list')
//                ->join('dream_orders', 'dream_orders.id', '=', 'dream_order_list.order_id')
//                ->join('dream_storages', 'dream_storages.id', '=', 'dream_order_list.storage_id')
//                ->leftJoin('dream_items_data', 'dream_items_data.item_id', '=', 'dream_order_list.item_id')
//                ->join('dream_items', 'dream_items.id', '=', 'dream_order_list.item_id')
//                ->join('dream_units', 'dream_units.id', '=', 'dream_order_list.unit_id')
//                ->join('users', 'users.id', '=', 'dream_orders.user_id')
//                ->leftJoin('dream_suppliers', 'dream_suppliers.id', '=', 'dream_orders.supplier_id')
//                ->select('dream_orders.*', 'users.firstname as customer', 'dream_suppliers.name as supplier',
//                    'dream_order_list.id as order_list_id', 'dream_order_list.amount', 'dream_items_data.name as english_name',
//                    'dream_items.articula_new', 'dream_items.articula_old', 'dream_items.sap_code', 'dream_items.item_name', 'dream_units.unit',
//                    DB::raw('(select sum(dream_transaction_list.amount) from dream_transaction_list
//                                       where dream_transaction_list.id in (select dream_order_transaction_bind.transaction_list_id from dream_order_transaction_bind
//                                       where dream_order_transaction_bind.order_list_id=dream_order_list.id) group by dream_transaction_list.unit_id) as complete')
//                )
//                ->where(function ($query) use($search){
//                    $query->orWhere('users.firstname', 'like', "%$search%")->orWhere('dream_orders.create_time', 'like', "%$search%")
//                        ->orWhere('dream_suppliers.name', 'like', "%$search%")->orWhere('dream_orders.invoice_number', 'like', "%$search%")->orWhere('dream_orders.container_info', 'like', "%$search%")
//                        ->orWhere('dream_orders.delivery_time', 'like', "%$search%")->orWhere('dream_orders.id', 'like', "%$search%")
//                        ->orWhere('dream_items.articula_new', 'like', "%$search%")->orWhere('dream_items.articula_old', 'like', "%$search%")->orWhere('dream_items.sap_code', 'like', "%$search%")
//                        ->orWhere('dream_items.item_name', 'like', "%$search%");
//                })
//                ->where([['dream_orders.state', self::STATE_ACTIVE], ['dream_order_list.storage_id', $request->storage_id], ['dream_orders.order_type', 1]])
//                ->get();
//
//            $totalFiltered = count($filteredData);
//        }
//
//        $data = array();
//        if (!empty($items)){
//            foreach ($items as $item)
//            {
//                $diff = $item->amount - $item->complete;
//                $nestedData['DT_RowId'] = "order_row_".$item->order_list_id;
//                $nestedData['order_list_id'] = $item->order_list_id;
//                $nestedData['group_row'] = "<p style='color: black; text-align: left; margin: 0; padding: 5px'><b>№:</b> ".$item->id." <b>| Поставщик:</b> ".$item->supplier."</p>";
//                $nestedData['articula_new'] = $item->articula_new;
//                $nestedData['english_name'] = $item->english_name;
//                $nestedData['sap_code'] = $item->sap_code;
//                $nestedData['item_name'] = $item->item_name;
//                $nestedData['amount'] = $item->amount;
//                $nestedData['complete'] = $item->complete;
//                if ($item->complete>=$item->amount){
//                    if (isset($input_fields[$item->order_list_id])){
//                        $input_value = $input_fields[$item->order_list_id];
//                        $nestedData['input_field'] = "<input type='number' style=' width: 100px' id='$item->order_list_id' value='$input_value' placeholder='0' disabled>";
//                    }else{
//                        $nestedData['input_field'] = "<input type='number' style=' width: 100px' id='$item->order_list_id' value='' placeholder='0' disabled>";
//                    }
//                    $nestedData['checkbox'] = "<input style='margin-right: 5px' type='checkbox' disabled>";
//                }else{
//                    if (isset($input_fields[$item->order_list_id])){
//                        $input_value = $input_fields[$item->order_list_id];
//                        $nestedData['input_field'] = "<input type='number' style=' width: 100px' name='brought_item_amount[]' id='$item->order_list_id' onkeyup=\"save_value_in_session_for_buffer(this)\" value='$input_value' placeholder='0'>";
//                    }else{
//                        $nestedData['input_field'] = "<input type='number' style=' width: 100px' name='brought_item_amount[]' id='$item->order_list_id' onkeyup=\"save_value_in_session_for_buffer(this)\" value='' placeholder='0'>";
//                    }
//                    $nestedData['checkbox'] = "<input style='margin-right: 5px' type='checkbox' name='order_list_item' id='checkbox_$item->order_list_id' onchange=\"set_value_input('".$item->order_list_id."','".$diff."')\">";
//                }
//                $nestedData['unit'] = $item->unit;
//                $nestedData['customer'] = $item->customer;
//                $nestedData['create_time'] = str_limit($item->create_time, 10, '');
//                $nestedData['invoice_number'] = $item->invoice_number;
//                $nestedData['delivery_time'] = Carbon::createFromFormat('Y-m-d H:i:s', $item->create_time)->addDays($item->delivery_time)->format('Y-m-d');
//                $data[] = $nestedData;
//            }
//        }
//        $json_data = array(
//            "draw"            => intval($request->input('draw')),
//            "recordsTotal"    => intval($totalData),
//            "recordsFiltered" => intval($totalFiltered),
//            "data"            => $data
//        );
//        echo json_encode($json_data);
//    }
//
//    public function save_input_items_on_session_for_buffer(Request $request){
//        $arr = array();
//        $input_id = $request->id;
//        $input_value = $request->input_value;
//        if (empty($input_value) || $input_value==null){
//            $arr = array_add($arr, $input_id, 0);
//        }else{
//            $arr = array_add($arr, $input_id, $input_value);
//        }
//
//        if (session()->get('input_fields_buffer_storage')!=null){
//            $old = session()->get('input_fields_buffer_storage');
//            foreach ($old as $key=>$value){
//                $arr = array_add($arr, $key, $value);
//            }
//
//        }
//
//        session()->put('input_fields_buffer_storage', $arr);
//        return json_encode($arr);
//    }
//
//    public function save_brought_items_to_buffer(){
//        if (Gate::denies('save', new DreamOrder)){
//            return \response()->json(['errors'=>array('нет доступа!')], 500);
//        }
//        $input = Input::all();
//
//        try{
//            $response = DB::transaction(function () use($input){
//
//                $items = session()->get('input_fields_buffer_storage');
//                $storage_id = $input['storage_id'];
//                $description = $input['order_description'];
//                $transaction = [
//                    'user_id' => Auth::user()->id,
//                    'description' => $description
//                ];
//                $transaction_id = DreamTransaction::create($transaction)->id;
//
//                $input_to_storage = [
//                    'transaction_id'=>$transaction_id,
//                    'storage_id'=>$storage_id,
//                    'operation_id'=>self::INPUT
//                ];
//
//                DB::table('dream_transaction_storage_bind')->insert($input_to_storage);
//
//                foreach ($items as $list_id=>$amount){
//                    $list_item = DB::table('dream_order_list')->where('id', $list_id)->get()->first();
//                    if ($amount>0 && !is_null($list_item)){
//                        $transaction_list_data = [
//                            'item_id' => $list_item->item_id,
//                            'transaction_id' => $transaction_id,
//                            'amount' => $amount,
//                            'unit_id' => $list_item->unit_id
//                        ];
//                        $tr_list_id = DB::table('dream_transaction_list')->insertGetId($transaction_list_data);
//
//                        $order_transaction = [
//                            'transaction_list_id'=>$tr_list_id,
//                            'order_list_id'=>$list_item->id
//                        ];
//
//                        DB::table('dream_order_transaction_bind')->insert($order_transaction);
//                    }
//                }
//
//                return $items;
//            });
//            session()->flash('input_fields_buffer_storage');
//
//            return json_encode($response);
//        }catch (\Exception $e){
//            return response()->json(['errors'=>array('невозможно добавить')], 500);
//        }
//    }
//
//    public function inputToBuffer(){
//        return view('index', ['page'=>'input_output_buffer_storage', 'storages'=>DreamStorage::where('type_id', '=', 3)->get()]);
//    }

    public function excelExport(Request $request){
        $items = DB::table('dream_order_list')
            ->join('dream_orders', 'dream_orders.id', '=', 'dream_order_list.order_id')
            ->join('dream_items', 'dream_items.id', '=', 'dream_order_list.item_id')
            ->leftJoin('dream_items_data', 'dream_items_data.item_id', '=', 'dream_order_list.item_id')
            ->select('dream_items_data.*', 'dream_items.articula_new', 'dream_orders.invoice_number', 'dream_orders.create_time', 'dream_order_list.amount')
            ->where('dream_order_list.order_id', $request->order_id)->get();

        $supplier = DB::table('dream_orders')
            ->leftJoin('dream_suppliers', 'dream_suppliers.id', '=', 'dream_orders.supplier_id')->select('dream_suppliers.*')
            ->where('dream_orders.id', $request->order_id)->get()->first();

        Excel::create('Filename', function($excel) use ($items, $supplier){
            $excel->sheet('CI', function($sheet) use ($items, $supplier){
                $sheet->setStyle(array(
                    'font' => array(
                        'name' => 'Times new Roman',
                    )
                ));
                $sheet->setColumnFormat(array(
                    'C' => '0'
                ));
                $sheet->getStyle('G')->getAlignment()->setWrapText(true);
                $sheet->loadView('pages.excel.commercialInvoice', array('items' => $items, 'order' => $items->first(), 'supplier'=>$supplier));
            });
            $excel->sheet('PL', function($sheet) use ($items, $supplier){
                $sheet->setStyle(array(
                    'font' => array(
                        'name' => 'Times new Roman',
                    )
                ));
                $sheet->setColumnFormat(array(
                    'C' => '0'
                ));
                $sheet->getStyle('G')->getAlignment()->setWrapText(true);
                $sheet->loadView('pages.excel.packingList', array('items' => $items, 'order' => $items->first(), 'supplier'=>$supplier));
            });
        })->export('xls');
    }

    public function onTheWay(Request $request){
        try{
            $columns = array(
                0 => 'articula_new',
                1 => 'item_name',
                2 => 'sap_code',
                3 => 'supplier',
                4 => 'invoice_number',
                5 => 'unit',
                6 => 'order_amount',
                7 => 'onTheWay',
                8 => 'inputed',
                9 => 'sup_remaining',
                10 => 'shipment_time',
                11 => 'delivery_time',
                12 => 'user_name'
            );

            $filterColumns = array(
                'filter_by_from_storage' => 'dream_orders.supplier_id',
                'filter_by_to_storage' => 'dream_storages.id',
                5 => 'invoice_number',
                6 => 'order_invoice_number',
                7 => 'container_number',
                8 => 'container_type_name',
                9 => 'create_time',
                'filter_by_user' => 'users.id',
                11 => 'delivery_time'
            );

            $totalData = DB::table('logistics')->get()->count();
            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');
            $filter_by_type = $request->filter_by_type;
            $filterArr = json_decode($request->filterArr);
            $filter_str = '';
            if (!is_null($filterArr)){
                foreach ($filterArr as $key=>$values){
                    if (!empty($values)){
                        $filter_str.=" and $filterColumns[$key] in (";
                        foreach ($values as $value){
                            $filter_str.=$value.',';
                        }
                        $filter_str = substr($filter_str, 0, strlen($filter_str)-1).')';
                    }
                }
            }

            $query_text = "
                select SQL_CALC_FOUND_ROWS  dream_order_list.id, dream_order_list.amount as order_amount, ROUND(IFNULL(inputed.inputed, 0), 3) as inputed, dream_items.articula_new, dream_items.item_name, dream_items.sap_code,
                dream_units.unit, dream_orders.invoice_number, dream_storages.name as supplier, SUBSTRING(dream_orders.create_time, 1, 16) as create_time, users.firstname as user_name,
                ROUND(o.remaining, 3) as sup_remaining, dream_units.unit, SUBSTRING(logistics.delivery_time, 1, 10) as delivery_time, SUBSTRING(dream_orders.shipment_time, 1, 10) as shipment_time,
                case
                when (dream_order_list.amount - IFNULL(inputed.inputed, 0)) >= 0 then
                ROUND((dream_order_list.amount - o.remaining - IFNULL(inputed.inputed, 0)), 3)
                else
                0
                end onTheWay
                from dream_orders
                inner join dream_order_list on dream_order_list.order_id = dream_orders.id
                inner join logistics_list on logistics_list.order_list_id = dream_order_list.id
                inner join logistics on logistics.id = logistics_list.logistics_id
                inner join dream_storages on dream_storages.id = dream_orders.supplier_id
                left join storage_info on storage_info.storage_id = dream_orders.supplier_id
                inner join dream_items on dream_items.id = dream_order_list.item_id
                inner join dream_units on dream_units.id = dream_order_list.unit_id
                inner join users on users.id = dream_orders.user_id
                left join (
                select sum(dream_transaction_list.amount) as inputed, transaction_order_bind.order_list_id from transaction_order_bind
                inner join dream_transaction_list on dream_transaction_list.id = transaction_order_bind.transaction_list_id
                inner join dream_transaction_storage_bind on dream_transaction_storage_bind.transaction_id = dream_transaction_list.transaction_id
                inner join dream_storages on dream_storages.id = dream_transaction_storage_bind.storage_id
                where dream_storages.type_id = 1 and dream_transaction_storage_bind.operation_id = 1
                group by transaction_order_bind.order_list_id, dream_transaction_list.item_id, dream_transaction_list.unit_id
                ) as inputed on inputed.order_list_id = dream_order_list.id
                left join (
                select sum(dream_transaction_list.amount*dream_transaction_types.operation) as remaining, transaction_order_bind.order_list_id, dream_storages.id as storage_id
                from transaction_order_bind
                inner join dream_transaction_list on dream_transaction_list.id = transaction_order_bind.transaction_list_id
                inner join dream_transaction_storage_bind on dream_transaction_storage_bind.transaction_id = dream_transaction_list.transaction_id
                inner join dream_transaction_types on dream_transaction_types.id = dream_transaction_storage_bind.operation_id
                inner join dream_storages on dream_storages.id = dream_transaction_storage_bind.storage_id
                where dream_storages.type_id != 1
                group by transaction_order_bind.order_list_id, dream_transaction_list.item_id, dream_transaction_list.unit_id, dream_storages.id
                ) as o on o.order_list_id = dream_order_list.id and o.storage_id = dream_orders.supplier_id
                where dream_orders.state_id = 1 and storage_info.type_id = $filter_by_type
            ";
            $search_text = '';
            if (!empty($request->input('search.value'))){
                $search = $request->input('search.value');
                $search_text .= "
                     and ( dream_items.articula_new like '%$search%' || dream_items.item_name like '%$search%' || dream_items.sap_code like '%$search%' || dream_orders.invoice_number like '%$search%'
                     || logistics.delivery_time like '%$search%' || users.firstname like '%$search%' || dream_storages.name like '%$search%' )
                    ";
            }

            $query_text.=$search_text.$filter_str." having (dream_order_list.amount - IFNULL(inputed, 0)) > 0 order by $order $dir limit $limit offset $start;";
            $items = DB::select($query_text);
            $totalCount = DB::select('select found_rows() as total_count')[0];

            $json_data = array(
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => intval($totalData),
                "recordsFiltered" => intval($totalCount->total_count),
                "data"            => $items
            );

            return json_encode($json_data);
        }catch (\Exception $exception){
            dd($exception);
            return \response()->json(['errors'=>array($exception)], 500);
        }
    }

    public function historyPage()
    {
        if (Gate::denies('viewOrderPrice', new DreamOrder)){
            abort(403);
        }
        $fromStorages = DreamStorage::whereIn('type_id', array(self::BUFFER, 7))->get();
        $toStorages = DreamStorage::whereIn('type_id', array(self::BUFFER, 1))->get();
        $users = DB::table('logistics')
            ->join('users', 'users.id', '=', 'logistics.user_id')
            ->select('users.*')->groupBy('users.id')->get();
        $states = [
            1 => 'открытый',
            2 => 'закрытый'
        ];
        return view('index', ['page'=>'orderHistory', 'toStorages'=>$toStorages, 'fromStorages'=>$fromStorages, 'users'=>$users, 'states'=>$states]);
    }
}

<?php

namespace App\Http\Controllers;

use App\DreamItem;
use App\DreamOrder;
use App\DreamSupplier;
use App\DreamTransaction;
use App\DreamRequest;
use App\DreamUnit;
use App\Dreamstorage;
use Validator;
use PDF;
use Carbon\Carbon;
use function foo\func;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class DreamStorageController extends Controller
{
    const OUTPUT = 2;
    const SUPER_ADMIN = 1;
    const DEFECT = 3;
    const RETURN_ITEM = 4;
    const INPUT = 1;
    const STATE_ACTIVE = 1;
    const STATE_INACTIVE = 0;
    const STATE_CLOSED = 2;
    const PRODUCTION_LINE_ID=2;
    const WAREHOUSE=1;
    const PRODUCTION_LINE_PRODUCT_UNIT=1;
    const POLUFABRIKAT = 26;
    const RADIATOR = 18;
    const RADIATOR_TSEX = 18;
    const ZAGATOVKA = 26;
    const REQUEST = 1;
    const REQUESTOR = 3;
    const LOCAL = 1;
    const RADIATOR_STORAGE = 6;

    public function storageInputOutputOperation(){
        $isAdmin = false;
        if (DB::table('user_role')->where('user_id', Auth::user()->id)->get()->first()->role_id==self::SUPER_ADMIN){
                $storages = DreamStorage::where('type_id', self::WAREHOUSE)->get();
            $isAdmin = true;
        }else{
            $storages = DB::table('dream_storages')
                ->join('dream_storage_user_bind', 'dream_storage_user_bind.storage_id', '=', 'dream_storages.id')
                ->select('dream_storages.*')->where([['dream_storage_user_bind.user_id', Auth::user()->id], ['dream_storages.type_id', self::WAREHOUSE]])->get();
        }
        $production_lines = DreamStorage::where('type_id', self::PRODUCTION_LINE_ID)->get();
        $suppliers = DreamStorage::where([['type_id', 7], ['state', self::STATE_ACTIVE]])->get();
        return view('index', ['page'=>'input_output_operation', 'storages'=>$storages, 'production_lines'=>$production_lines, 'suppliers'=>$suppliers, 'isAdmin'=>$isAdmin]);
    }

    public function getOperationsByStorage(Request $request){
        if (DB::table('user_role')->where('user_id', Auth::user()->id)->get()->first()->role_id==self::SUPER_ADMIN){
            $operation_types = DB::table('dream_transaction_types')->get();
        }else{
            $operation_types = DB::table('dream_transaction_type_storage_bind')
                ->join('dream_storages', 'dream_storages.id', '=', 'dream_transaction_type_storage_bind.storage_id')
                ->join('dream_transaction_types', 'dream_transaction_types.id', '=', 'dream_transaction_type_storage_bind.transaction_type_id')
                ->where('dream_storages.id', $request->storage_id)->select('dream_transaction_types.*')->get();
        }
        return json_encode($operation_types);
    }

    public function getFilterItemsByStorage(Request $request){
        $storage_id = $request->storage_id;
        $invoices = DB::table('logistics')
            ->join('dream_storages', 'dream_storages.id', '=', 'logistics.from_storage_id')
            ->select('logistics.invoice_number', 'dream_storages.name as storage_name', 'dream_storages.id as storage_id')->where('logistics.to_storage_id', $storage_id)
            ->groupBy('logistics.id')->get();

        $storages = DB::table('logistics')
            ->join('dream_storages', 'dream_storages.id', '=', 'logistics.from_storage_id')
            ->select('dream_storages.name as storage_name', 'dream_storages.id as storage_id')->where('logistics.to_storage_id', $storage_id)
            ->groupBy('dream_storages.id')->get();
        $data = [
            'invoices'=>$invoices,
            'storages'=>$storages
        ];
        return json_encode($data);
    }

    public function lineInputOutputOperation(){
        if (DB::table('user_role')->where('user_id', Auth::user()->id)->get()->first()->role_id==self::SUPER_ADMIN){
            $lines = DreamStorage::where('type_id', self::PRODUCTION_LINE_ID)->get();
            $storages = DreamStorage::where('type_id', self::WAREHOUSE)->get();
        }else{
            $lines = DB::table('dream_storages')
                ->join('dream_storage_user_bind', 'dream_storage_user_bind.storage_id', '=', 'dream_storages.id')
                ->select('dream_storages.*')->where([['dream_storage_user_bind.user_id', Auth::user()->id], ['dream_storages.type_id', self::PRODUCTION_LINE_ID]])->get();
            $storages = DreamStorage::where('type_id', self::WAREHOUSE)->get();
        }
        return view('index', ['page'=>'line_input_output_operation', 'lines'=>$lines, 'storages'=>$storages]);
    }

    public function getMovingItems(Request $request){
        $items = [];
        if (!is_null($request->storage_id)){
            $storage_id = $request->storage_id;
            $supplier_id = $request->supplier_id;

            if ($supplier_id==0 || is_null($supplier_id)){
                $items = DB::table('dream_transaction_list')
                    ->join('dream_transaction_storage_bind', 'dream_transaction_storage_bind.transaction_id', '=', 'dream_transaction_list.transaction_id')
                    ->join('dream_items', 'dream_items.id', '=', 'dream_transaction_list.item_id')
                    ->where('dream_transaction_storage_bind.storage_id', $storage_id)
                    ->select('dream_items.*')->groupBy('dream_items.id')->get();
            }else{
                $items = DB::table('dream_transaction_list')
                    ->join('dream_transaction_storage_bind', 'dream_transaction_storage_bind.transaction_id', '=', 'dream_transaction_list.transaction_id')
                    ->join('dream_items', 'dream_items.id', '=', 'dream_transaction_list.item_id')
                    ->join('dream_storage_item_bind', 'dream_storage_item_bind.item_id', '=', 'dream_items.id')
                    ->where([['dream_transaction_storage_bind.storage_id', $storage_id], ['dream_storage_item_bind.storage_id', $supplier_id]])
                    ->select('dream_items.*')->groupBy('dream_items.id')->get();
            }
        }
        return json_encode($items);
    }

//  correct
    public function add_item(Request $request){
        $validateNames = array(
            'items'=>'элементы'
        );
        $validator = Validator::make($request->all(), [
            'items' => 'required',
        ]);

        $validator->setAttributeNames($validateNames);
        try{
            $storage_user = DB::table('dream_storage_user_bind')->where('user_id', Auth::user()->id)->get();
            if (Auth::user()->roles->first()->id!=self::SUPER_ADMIN){
                if (is_null($storage_user)){
                    return response()->json(['errors'=>array('у вас нет доступа!')], 500);
                }
            }

            $arr = [];
            if (!is_null($storage_user) || Auth::user()->roles->first()->id){
                $operation_type = DB::table('dream_transaction_types')->where('id', $request->operation_id)->get()->first();
                if (is_null($operation_type)){
                    return response()->json(['errors'=>array('невозможно добавить!')], 500);
                }

                $storage_id = $request->storage_id;
                $item_id = $request->item_id;
                $amount = $request->amount;
                $item = DB::table('dream_items')
                    ->join('dream_storage_item_bind', 'dream_storage_item_bind.item_id', '=', 'dream_items.id')
                    ->join('dream_units', 'dream_units.id', '=', 'dream_storage_item_bind.unit_id')
                    ->where([['dream_storage_item_bind.item_id', $item_id]])->get()->first();
                if (!is_null($item)){
                    $data = [
                        'articula_new'=>$item->articula_new,
                        'sap_code'=>$item->sap_code,
                        'item_name'=>$item->item_name,
                        'unit'=>$item->unit,
                        'unit_id'=>$item->unit_id,
                        'storage_id'=>$item->storage_id,
                        'amount'=>$amount,
                        'remaining'=>$this->get_item_remaining($storage_id, $item->item_id),
                        'item_id'=>$item->item_id
                    ];
                    array_push($arr, $data);
                }
            }

            return json_encode($arr);
        }catch(\Exception $e){
            return response()->json(['errors'=>'невозможно добавить!', $e], 500);
        }
    }

    public function get_item_remaining($storage_id, $item_id)
    {
        if (is_null($storage_id) || $storage_id==0){
            return 0;
        }
        $total = DB::select("select sum(operation*amount) as total from dream_transaction_list ".
            "left join dream_transaction_storage_bind on dream_transaction_list.transaction_id=dream_transaction_storage_bind.transaction_id and dream_transaction_storage_bind.storage_id=$storage_id " .
            "left join dream_transaction_types on dream_transaction_types.id=dream_transaction_storage_bind.operation_id where item_id=$item_id and dream_transaction_list.delete_time is null")[0];
        if ($total->total == null){
            $total->total = 0;
        }
        return round($total->total, 3);
    }

//    correct
    public function save_inputs_outputs_items(Request $request){
        $validateNames = array(
            'items'=>'элементы'
        );
        $storage_user = DB::table('dream_storage_user_bind')->where('user_id', Auth::user()->id)->get();
        if (Auth::user()->roles->first()->id!=self::SUPER_ADMIN){
            if (is_null($storage_user)){
                return response()->json(['errors'=>array('у вас нет доступа!')], 500);
            }
            if (Gate::denies('allow_input_output_operation', new DreamTransaction)){
                return response()->json(['errors'=>array('у вас нет доступа!')], 500);
            }
        }
        $operation_type = DB::table('dream_transaction_types')->where('id', $request->operation_id)->get()->first();
        if (is_null($operation_type)){
            return response()->json(['errors'=>array('невозможно добавить!')], 500);
        }
        $validator = Validator::make($request->all(), [
            'items' => 'required',
        ]);

        $validator->setAttributeNames($validateNames);

        if ($operation_type->operation<0){
            if (is_null($request->supplier_id) && is_null($request->description)){
                return response()->json(['errors'=>array('выберите поставщика или напишите примечание!')], 500);
            }
        }

        if ($validator->fails()) {
            return response()->json(['errors'=>array($validator->errors()->all())], 500);
        }
        try{
            $items = DB::transaction(function () use($request, $operation_type){
                $items = json_decode($request->items);
                $desc = $request->description;
                $user_id = Auth::user()->id;
                $supplier = '';
                $storage_id = $request->storage_id;
                $operation_id = $request->operation_id;
                if (!is_null($request->supplier_id)){
                    if (!is_null(DreamStorage::find($request->supplier_id))){
                        $supplier = DreamStorage::find($request->supplier_id)->name;
                    }
                }

                $transaction_id = $this->create_transaction($items, $supplier.'<br>'.$desc, $user_id);
                $input_to_storage = [
                    'transaction_id'=>$transaction_id,
                    'storage_id'=>$storage_id,
                    'operation_id'=>$operation_id
                ];
                DB::table('dream_transaction_storage_bind')->insert(array($input_to_storage));

                return $items;
            });
            return json_encode($items);
        }
        catch (\Exception $e){
            return response()->json(['errors'=>array('ошибка при выпольнении транзакции!', $e)], 500);
        }
    }

    public static function create_transaction($items, $description, $user_id){
        $transaction = [
            'user_id' => $user_id,
            'description' => $description
        ];
        $transaction_id = DreamTransaction::create($transaction)->id;

        foreach ($items as $item){
            $transaction_list_data = [
                'item_id' => $item->item_id,
                'transaction_id' => $transaction_id,
                'amount' => $item->amount,
                'unit_id' => $item->unit_id
            ];

            DB::table('dream_transaction_list')->insert($transaction_list_data);
        }
        return $transaction_id;
    }

//    linelinelineline
    public function getLineItems(Request $request){
        $items = [];
        if (!is_null($request->storage_id)){
            $storage_id = $request->storage_id;

            $items = DB::table('dream_items')
                ->join('dream_storage_item_bind', 'dream_storage_item_bind.item_id', '=', 'dream_items.id')
                ->whereIn('dream_storage_item_bind.storage_id', array($storage_id))->select('dream_items.*')->groupBy('dream_items.id')->get();
        }
        return json_encode($items);
    }

    public function collect_line_item_input_output(Request $request){
        $validateNames = array(
            'items'=>'элементы'
        );
        try{
            $storage_user = DB::table('dream_storage_user_bind')->where('user_id', Auth::user()->id)->get();
            if (Auth::user()->roles->first()->id!=self::SUPER_ADMIN){
                if (is_null($storage_user)){
                    return response()->json(['errors'=>array('у вас нет доступа!')], 500);
                }
            }

            $arr = [];
            if (!is_null($storage_user) || Auth::user()->roles->first()->id){
                $storage_id = $request->storage_id;
                $item_id = $request->item_id;
                $amount = $request->amount;
                $item = DB::table('dream_items')
                    ->join('dream_storage_item_bind', 'dream_storage_item_bind.item_id', '=', 'dream_items.id')
                    ->join('dream_units', 'dream_units.id', '=', 'dream_storage_item_bind.unit_id')
                    ->where([['dream_storage_item_bind.item_id', $item_id], ['dream_storage_item_bind.storage_id', $storage_id]])->get()->first();
                if (!is_null($item)){
                    $data = [
                        'articula_new'=>$item->articula_new,
                        'sap_code'=>$item->sap_code,
                        'item_name'=>$item->item_name,
                        'unit'=>$item->unit,
                        'unit_id'=>$item->unit_id,
                        'storage_id'=>$item->storage_id,
                        'amount'=>$amount,
                        'remaining'=>$this->get_item_remaining($storage_id, $item->item_id),
                        'item_id'=>$item->item_id
                    ];
                    array_push($arr, $data);
                }
            }

            return json_encode($arr);
        }catch(\Exception $e){
            return response()->json(['errors'=>'невозможно добавить!', $e], 500);
        }
    }
//linelineline
    public function save_line_inputs_outputs_items(Request $request){
        $validateNames = array(
            'items'=>'элементы'
        );
        $storage_user = DB::table('dream_storage_user_bind')->where('user_id', Auth::user()->id)->get();
        if (Auth::user()->roles->first()->id!=self::SUPER_ADMIN){
            if (is_null($storage_user)){
                return response()->json(['errors'=>array('у вас нет доступа!')], 500);
            }
            if (Gate::denies('allow_input_output_operation', new DreamTransaction)){
                return response()->json(['errors'=>array('у вас нет доступа!')], 500);
            }
        }
        $operation_type = DB::table('dream_transaction_types')->where('id', $request->operation_id)->get()->first();
        if (is_null($operation_type)){
            return response()->json(['errors'=>array('невозможно добавить!')], 500);
        }
        $validator = Validator::make($request->all(), [
            'items' => 'required'
        ]);
        $validator->setAttributeNames($validateNames);

        if ($operation_type->operation<0){
            if (is_null($request->supplier_id) && is_null($request->description)){
                return response()->json(['errors'=>array('выберите поставщика или напишите примечание!')], 500);
            }
        }
        if ($validator->fails()) {
            return response()->json(['errors'=>array($validator->errors()->all())], 500);
        }
        try{
            $items = DB::transaction(function () use($request, $storage_user, $operation_type){
                $items = json_decode($request->items);
                $desc = $request->description;
                $user_id = Auth::user()->id;

                $storage_id = $request->storage_id;
                $operation_id = $request->operation_id;
                $data = [
                    'user_id'=>$user_id,
                    'description'=>$desc
                ];

                $model_transaction_id = DreamTransaction::create($data)->id;
                $model_store_data = [
                    'transaction_id'=>$model_transaction_id,
                    'storage_id'=>$storage_id,
                    'operation_id'=>$operation_id
                ];
                DB::table('dream_transaction_storage_bind')->insert(array($model_store_data));

                if ($operation_id!=self::INPUT){
                    if ($operation_type->operation > 0){
                        $child_transaction_id = DreamTransaction::create($data)->id;
                        $child_store_data = [
                            'transaction_id'=>$child_transaction_id,
                            'storage_id'=>$storage_id,
                            'operation_id'=>self::OUTPUT
                        ];
                        DB::table('dream_transaction_storage_bind')->insert(array($child_store_data));
                    }else{
                        $child_transaction_id = null;
                    }
                    foreach ($items as $item){
                        $this->inputOutputTransaction($item, $model_transaction_id, $child_transaction_id);
                    }
                }else{
                    foreach ($items as $item){
                        $this->inputOutputTransaction($item, $model_transaction_id, null);
                    }
                }
                return $items;
            });
            return json_encode($items);
        }
        catch (\Exception $e){
            return response()->json(['errors'=>array('ошибка при выпольнении транзакции!', $e)], 500);
        }
    }

//  correct
    public function show_all_storage_items(){
        $storages = Dreamstorage::all();
        return view('index', ['page'=>'all_storage_items', 'storages'=>$storages,]);
    }

    public function getStorageItems(Request $request){
        $columns = array(
            0 => 'dream_items.id',
            1 => 'storages',
            2 => 'dream_items.articula_new',
            3 => 'dream_items.articula_old',
            4 => 'dream_items.sap_code',
            5 => 'dream_items.item_name',
        );
        $allProduct = DreamItem::all();
        $totalData = DreamItem::count();
        $totalFiltered = count($allProduct);
        $start = $request->input('start');
        if ($request->input('length')==-1){
            $limit = $totalData;
        }else{
            $limit = $request->input('length');
        }
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if (empty($request->input('search.value'))){
            $items = DB::table('dream_items')->leftJoin('dream_storage_item_bind', 'dream_storage_item_bind.item_id', '=', 'dream_items.id')
                ->leftJoin('dream_storages', 'dream_storages.id', '=', 'dream_storage_item_bind.storage_id')
                ->select('dream_items.*', 'dream_storage_item_bind.unit_id', DB::raw("group_concat(dream_storages.name) as storages"))
                ->limit($limit)->offset($start)
                ->groupBy('dream_items.id', 'dream_storage_item_bind.unit_id')->orderBy($order, $dir)->get();
        }else{
            $search = $request->input('search.value');
            $items = DB::table('dream_items')->leftJoin('dream_storage_item_bind', 'dream_storage_item_bind.item_id', '=', 'dream_items.id')
                ->leftJoin('dream_storages', 'dream_storages.id', '=', 'dream_storage_item_bind.storage_id')
                ->select('dream_items.*', 'dream_storage_item_bind.unit_id', DB::raw("group_concat(dream_storages.name) as storages"))
                ->where(function ($query) use ($search){
                    $query->orWhere('dream_items.articula_new', 'like', "%$search%")->orWhere('dream_items.item_name', 'like', "%$search%")->orWhere('dream_storages.name', 'like', "%$search%")
                        ->orWhere('dream_items.sap_code', 'like', "%$search%");
                })
                ->limit($limit)->offset($start)
                ->groupBy('dream_items.id', 'dream_storage_item_bind.unit_id')->orderBy($order, $dir)->get();

            $totalFiltered = DB::table('dream_items')->leftJoin('dream_storage_item_bind', 'dream_storage_item_bind.item_id', '=', 'dream_items.id')
                ->leftJoin('dream_storages', 'dream_storages.id', '=', 'dream_storage_item_bind.storage_id')
                ->select('dream_items.*', 'dream_storage_item_bind.unit_id', DB::raw("group_concat(dream_storages.name) as storages"))
                ->where(function ($query) use ($search){
                    $query->orWhere('dream_items.articula_new', 'like', "%$search%")->orWhere('dream_items.item_name', 'like', "%$search%")->orWhere('dream_storages.name', 'like', "%$search%")
                        ->orWhere('dream_items.sap_code', 'like', "%$search%");
                })
                ->groupBy('dream_storage_item_bind.unit_id')->count();

        }
        $data = array();
        if (!empty($items)){
            foreach ($items as $item)
            {
                $item_units = DB::table('dream_item_unit_bind')->join('dream_units', 'dream_units.id', '=', 'dream_item_unit_bind.unit_id')
                    ->where('dream_item_unit_bind.item_id', $item->id)->select('dream_units.*')->get();
                $units = "";
                foreach ($item_units as $unit){
                    if ($item->unit_id!=null){
                        if ($unit->id==$item->unit_id){
                            $units.="<input type='radio' name='unit$item->id' value='$unit->id' onchange='update_unit($item->id, $unit->id)' checked><span>$unit->unit</span><br>";
                        }else{
                            $units.="<input type='radio' name='unit$item->id' value='$unit->id' onchange='update_unit($item->id, $unit->id)'><span>$unit->unit</span><br>";
                        }
                    }else{
                        $units.="<input type='radio' name='unit$item->id' value='$unit->id' onchange='update_unit($item->id, $unit->id)' checked><span>$unit->unit</span><br>";
                    }
                }
                $exist_in_this_storage = DB::table('dream_storage_item_bind')->where([['dream_storage_item_bind.storage_id', $request->storage_id], ['dream_storage_item_bind.item_id', $item->id]])->count();
                $nestedData['DT_RowId'] = "item_".$item->id;
                if ($exist_in_this_storage>0){
                    $nestedData['binding_checkbox'] = "<input type='checkbox' name='unit[$item->id]' value='$item->id' onclick='save_item($item->id)' checked>";
                }else{
                    $nestedData['binding_checkbox'] = "<input type='checkbox' name='unit[$item->id]' value='$item->id' onclick='save_item($item->id)'>";
                }
                $nestedData['storages'] = $item->storages;
                $nestedData['articula_new'] = $item->articula_new;
                $nestedData['articula_old'] = $item->articula_old;
                $nestedData['item_name'] = $item->item_name;
                $nestedData['sap_code'] = $item->sap_code;
                $nestedData['item_units'] = $units;
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

//    correct
    public function save_items_storage(Request $request){
        if (Gate::denies('BINDING_STORAGE_ITEM', new DreamStorage)){
            return \response()->json(['errors'=>array('нет доступа!')], 500);
        }
        try{
            $storage_id = $request->storage_id;
            if ($storage_id!=0){
                $unit_id = $request->unit_id;
                $item_id = $request->item_id;
                $item = DB::table('dream_storage_item_bind')->where([['item_id', $item_id], ['storage_id', $storage_id]])->first();
                if (empty($item)){
                    $data = [
                        'storage_id' => $storage_id,
                        'item_id' => $item_id,
                        'unit_id' => $unit_id,
                    ];
                    DB::table('dream_storage_item_bind')->insert($data);
                }else{
                    DB::table('dream_storage_item_bind')->where([['item_id', $item_id], ['storage_id', $storage_id]])->delete();
                }
                $all_storage = DB::table('dream_storage_item_bind')->join('dream_storages', 'dream_storages.id', '=', 'dream_storage_item_bind.storage_id')
                    ->select('dream_storages.*')->where([['dream_storage_item_bind.item_id', $item_id], ['dream_storages.type_id', self::WAREHOUSE]])->get();
                return response()->json(['storage_name'=>$all_storage]);
            }else{
                return \response()->json(['errors'=>array('выберите склада!')], 500);
            }
        }catch (\Exception $e){
            return \response()->json(['errors'=>array('невозможно сохранить!')], 500);
        }
    }

    //    correct
    public function update_unit(Request $request){
        if (Gate::denies('BINDING_STORAGE_ITEM', new DreamStorage)){
            return \response()->json(['errors'=>array('нет доступа!')], 500);
        }
        if ($request->storage_id==0){
            return \response()->json(['errors'=>array('выберите склада!')], 500);
        }
        try{
            $has_transaction = DB::table('dream_transaction_list')->where('item_id', $request->item_id)->count();
            if ($has_transaction>0){
                return \response()->json(['errors'=>array('не возможно изменить, уже используется!')], 500);
            }
            $items = DB::table('dream_storage_item_bind')->where('item_id', $request->item_id)->get();
            $is_contain_item = DB::table('dream_storage_item_bind')->where([['item_id', $request->item_id], ['storage_id', $request->storage_id]])->first();
            if (!empty($items) && !is_null($is_contain_item)){
                $data = [
                    'unit_id' => $request->unit_id
                ];
                foreach ($items as $item){
                    DB::table('dream_storage_item_bind')->where([['item_id', $request->item_id], ['storage_id', $item->storage_id]])->update($data);
                }
            }
            $items = DB::table('dream_storage_item_bind')->where('item_id', $request->item_id)->get();
            return json_encode($items);
        }catch(\Exception $e){
            return response()->json(array('error'=>true, 'data'=>'невозможно сохранить'), 500);
        }
    }

//  correct
    public function add_transaction_description(){
        $input = Input::all();
        $transaction_data = DreamTransaction::find($input['tr_id']);
        if ($transaction_data->user_id!=Auth::user()->id){
            return \response()->json(['errors'=>array('вы не можете изменить!')], 500);
        }
        try{
            DreamTransaction::find($input['tr_id'])->update(['description'=>$input['desc']]);
        }catch(\Exception $e){
            return \response()->json(['errors'=>array('невозможно сохранить')], 500);
        }
        return json_encode($input['desc']);
    }

//    correct
    public function import_storage_items(){
        if (Gate::denies('BINDING_STORAGE_ITEM', new DreamStorage)){
            return redirect()->back()->with('status', 'нет доступа!');
        }
        $storages = Dreamstorage::all();
        $storage_items = DB::select("SELECT dream_items.articula_new, dream_units.unit FROM dream_items
          inner join dream_storage_item_bind on dream_storage_item_bind.item_id=dream_items.id
          inner join dream_units on dream_units.id=dream_storage_item_bind.unit_id");

        $a_items = DB::select('select dream_items.articula_new, group_concat(dream_units.unit) as units from dream_items
          inner join dream_item_unit_bind on dream_item_unit_bind.item_id=dream_items.id
          inner join dream_units on dream_item_unit_bind.unit_id=dream_units.id group by dream_items.articula_new');
        $units = DreamUnit::all();
        return view('index', ['page'=>'import_storage_items', 'storages'=>$storages, 'items'=>$a_items, 'storage_items'=>$storage_items, 'units'=>$units]);
    }

//    correct
    public function upload_import_storage_items_file(Request $request){
        if ($request->hasFile('import_storage_items_file')){
            $path = $request->file('import_storage_items_file')->getRealPath();
            $data = \Excel::load($path)->get();
            if($data->count()){
//                $request->file('import_storage_items_file')->move($_SERVER['DOCUMENT_ROOT'].'/assets/exampleFiles/', 'import_storage_items_file.xlsx');
//                session()->put('import_storage_items_file_address', $_SERVER['DOCUMENT_ROOT'].'/assets/exampleFiles/import_storage_items_file.xlsx');
                foreach ($data as $key => $value) {
                    $arr[] = ['articula_new' => $value->articula_new, 'unit' => $value->unit];
                }
                if(!empty($arr)){
                    session()->put('import_storage_items_file', $arr);
                }
                else{
                    session()->put('import_storage_items_file', '');
                }
            }
        }
        else{
            return redirect()->back();
        }
        return redirect()->back();
    }

//    correct
    public function import_validate_storage_items_file(){
        if (!session()->has('import_storage_items_file')){
            return redirect()->back()->with('status', 'not data');
        }else{
            $storage_id = Input::all()['storage_id'];
            DB::transaction(function () use($storage_id){
                foreach (session()->get('import_storage_items_file') as $item){
                    $item_id = DreamItem::where('articula_new', $item['articula_new'])->first()->id;
                    $unit_id = DreamUnit::where('unit', $item['unit'])->first()->id;
                    $count = DB::table('dream_storage_item_bind')->where([['storage_id', $storage_id], ['item_id', $item_id], ['unit_id', $unit_id]])->count();
                    $item_unit_bind = DB::table('dream_item_unit_bind')->where([['item_id',$item_id], ['unit_id',$unit_id]])->count();
                    if ($item_unit_bind==0){
                        $data = [
                            'item_id'=>$item_id,
                            'unit_id'=>$unit_id
                        ];
                        DB::table('dream_item_unit_bind')->insert($data);
                    }
                    if ($count==0){
                        $data = [
                            'storage_id' => $storage_id,
                            'item_id' => $item_id,
                            'unit_id' => $unit_id
                        ];
                        DB::table('dream_storage_item_bind')->insert($data);
                    }
                }
            });
//            File::move(session()->get('import_storage_items_file_address'), $_SERVER['DOCUMENT_ROOT'].'/assets/last_import_storage_items_file/correct_last_import_storage_items_file_address.xlsx');
            session()->forget('import_storage_items_file');
            return redirect()->back()->with('status', 'saved');
        }
    }


//    production line logic
    public function show_production_line(){
        $all_lines = DB::table('dream_storages')
            ->join('dream_storage_types', 'dream_storage_types.id', '=', 'dream_storages.type_id')
            ->where('type_id', self::PRODUCTION_LINE_ID)->select('dream_storages.*', 'dream_storage_types.name as type_name')->get();
        return view('index', ['page'=>'production_line', 'all_lines'=>$all_lines]);
    }

    public function show_production_line_items_list(){
        try{
            $input = Input::all();
            $line_items = DB::table('dream_storage_item_bind')
                ->join('dream_items', 'dream_items.id', '=', 'dream_storage_item_bind.item_id')
                ->where('dream_storage_item_bind.storage_id', $input['line_id'])
                ->select('dream_storage_item_bind.*', 'dream_items.articula_new', 'dream_items.articula_old', 'dream_items.sap_code', 'dream_items.item_name')->get();
            $line = DB::table('dream_storages')->where('id', $input['line_id'])->first();
            $data = [
                'all_items'=>$line_items,
                'line'=>$line
            ];
            return json_encode($data);
        }catch(\Exception $e){
            return response()->json(['errors'=>array('невозможно загрузить')], 500);
        }
    }

    public function create_line(Request $request){
        $request->validate([
            'name'=>'required|unique:dream_storages',
        ]);

        if (Gate::denies('save', new DreamItem)){
            return redirect()->back()->with('status', 'нет доступа!');
        }
        try{
            $data = [
                'name'=>$request->name,
                'type_id'=>self::PRODUCTION_LINE_ID
            ];

            DreamStorage::create($data);
            return redirect()->back()->with('status', 'сохранено');
        }catch (\Exception $e){
            return redirect()->back()->with('status', 'невозможно сохранить');
        }
    }

    public function update_line(Request $request){
        if (Gate::denies('save', new DreamItem)){
            return redirect()->back()->with('status', 'нет доступа!');
        }
        $data = [
            'name'=>$request->edit_name,
        ];
        DB::table('dream_storages')->where('id', $request->edit_line_id)->update($data);
        return redirect()->back()->with('status', 'сохранено');
    }

    public function get_production_line_items(){
        $input = Input::all()['line_id'];
        $items = DB::select("select dream_items.* from dream_items where id not in (select item_id from dream_storage_item_bind where storage_id=$input) and id in (select parent_id from dream_item_bind)");
        return json_encode($items);
    }

    public function add_production_line_item_list(){
        if (Gate::denies('save', new DreamItem)){
            return \response()->json(['errors'=>array('нет доступа!')], 500);
        }
        $input = Input::all();
        if (json_decode($input['items'])==null){
            return \response()->json(['errors'=>array('невозможно добавить!')], 500);
        }
        try{
            $result = DB::transaction(function () use($input){
                $line_id = $input['line_id'];
                $items = json_decode($input['items']);
                foreach ($items as $key=>$item){
                    $data = [
                        'storage_id'=>$line_id,
                        'item_id'=>$item,
                        'unit_id'=>self::PRODUCTION_LINE_PRODUCT_UNIT
                    ];

                    DB::table('dream_storage_item_bind')->insert($data);
                }
                return $line_id;
            });
            return json_encode($result);

        }catch(\Exception $e){
            return response()->json(['errors'=>array('невозможно добавить')], 500);
        }
    }

    public function delete_line($id){
        if (Gate::denies('save', new DreamItem)){
            return redirect()->back()->with('status', 'нет доступа!');
        }
        $items_count = DB::table('dream_storage_item_bind')->where('storage_id', $id)->count();
        if ($items_count>0){
            return redirect()->back()->with('status', 'невозможно удалить!');
        }else{
            DreamStorage::find($id)->delete();
        }
        return redirect()->back()->with('status', 'удален');
    }

    public function delete_production_list_item(){
        if (Gate::denies('delete', new DreamItem)){
            return \response()->json(['errors'=>array('нет доступа!')], 500);
        }
        try{
            $input = Input::all();
            DB::table('dream_storage_item_bind')->where('id', $input['list_item_id'])->delete();
            return json_encode($input['list_item_id']);
        }catch (\Exception $e){
            return response()->json(['errors'=>array('невозможно удалить')], 500);
        }
    }

    public function upload_production_line_items_file(Request $request){
        if (Gate::denies('save', new DreamItem)){
            return \response()->json(['errors'=>array('нет доступа!')], 500);
        }
        try{
            $result = DB::transaction(function () use($request){
                $arr = [];
                $bad_arr = [];
                if($request->hasFile('line_item_file'))
                {
                    $line_id = $request->file_line_id;
                    $path = $request->file('line_item_file')->getRealPath();
                    $data = \Excel::load($path)->get();
                    foreach ($data as $value){
                        if ($value->articula_new!=''){
                            $item = DB::table('dream_items')->where('dream_items.articula_new', $value->articula_new)->first();
                            if($item!=null){
                                $already_has = DB::table('dream_storage_item_bind')->where([['storage_id', $line_id], ['item_id', $item->id]])->count();
                                if ($already_has==0){
                                    $data = [
                                        'storage_id'=>$line_id,
                                        'item_id'=>$item->id,
                                        'unit_id'=>self::PRODUCTION_LINE_PRODUCT_UNIT
                                    ];

                                    DB::table('dream_storage_item_bind')->insert($data);
                                    array_push($arr, $item);
                                }
                            }else{
                                array_push($bad_arr, $value->articula_new);
                            }
                        }
                    }
                }
                return $bad_arr;
            });
            return json_encode($result);
        }catch (\Exception $e){
            return response()->json(['errors'=>array('невозможно загрузить')], 500);
        }
    }

    public function delete_all_line_items(){
        $line_id = Input::all()['line_id'];
        DB::table('dream_storage_item_bind')->where('storage_id', $line_id)->delete();
        return json_encode($line_id);
    }

    public function showInsideInvoices(){
        $lines = DB::table('dream_storages')->whereIn('type_id', array(self::PRODUCTION_LINE_ID, 4))->get();
        if (DB::table('user_role')->where('user_id', Auth::user()->id)->get()->first()->role_id==self::SUPER_ADMIN || DB::table('user_role')->where('user_id', Auth::user()->id)->get()->first()->role_id==self::REQUESTOR){
            $storages = DreamStorage::where('type_id', self::WAREHOUSE)->get();
        }else{
            $storages = DB::table('dream_storages')
                ->join('dream_storage_user_bind', 'dream_storage_user_bind.storage_id', '=', 'dream_storages.id')
                ->select('dream_storages.*')->where([['dream_storage_user_bind.user_id', Auth::user()->id], ['dream_storages.type_id', self::WAREHOUSE]])->get();
        }
        return view('index', ['page'=>'insideInvoices', 'lines'=>$lines, 'storages'=>$storages]);
    }

    public function getInsideInvoices(Request $request){
        $data = array();
        if (!is_null($request->storage_id)){
            $fromStorage = DreamStorage::find($request->storage_id);
            $items = DB::table('dream_transaction_request_bind')
                ->join('dream_request_list', 'dream_request_list.id', '=', 'dream_transaction_request_bind.request_list_id')
                ->join('dream_requests', 'dream_requests.id', '=', 'dream_request_list.request_id')
                ->join('dream_transaction_list', 'dream_transaction_list.id', '=', 'dream_transaction_request_bind.transaction_list_id')
                ->join('dream_transaction_storage_bind', 'dream_transaction_storage_bind.transaction_id', '=', 'dream_transaction_list.transaction_id')
                ->join('dream_transactions', 'dream_transactions.id', '=', 'dream_transaction_list.transaction_id')
                ->join('dream_items', 'dream_items.id', '=', 'dream_transaction_list.item_id')
                ->join('dream_units', 'dream_units.id', '=', 'dream_transaction_list.unit_id')
                ->whereBetween('dream_transactions.create_time', array($request->currentDate.' 00:00:00', $request->currentDate.' 23:59:59'))
                ->where([['dream_transaction_storage_bind.storage_id', $request->line_id], ['dream_transaction_storage_bind.operation_id', self::INPUT], ['dream_requests.to_storage_id', $fromStorage->id]])
                ->whereNull('dream_transaction_list.delete_time')
                ->select('dream_units.unit as unit_name', 'dream_items.articula_new', 'dream_items.item_name', 'dream_items.sap_code',
                    DB::raw('sum(dream_transaction_list.amount) as amount')
                )->groupBy('dream_transaction_list.item_id', 'dream_transaction_list.unit_id')->get();

            if (!empty($items)){
                foreach ($items as $key=>$item)
                {
                    $nestedData['key'] = ++$key;
                    $nestedData['articula_new'] = $item->articula_new;
                    $nestedData['item_name'] = $item->item_name;
                    $nestedData['sap_code'] = $item->sap_code;
                    $nestedData['amount'] = $item->amount;
                    $nestedData['unit'] = $item->unit_name;
                    $data[] = $nestedData;
                }
            }
        }

        $json_data = array(
            "data" => $data
        );

        return json_encode($json_data);
    }

    public function getInsideModelsInvoices(Request $request){
        $data = array();
        if (!is_null($request->storage_id)) {
            $fromStorage = DreamStorage::find($request->storage_id);
            $items = DB::table('dream_requests')
                ->join('dream_items', 'dream_items.id', '=', 'dream_requests.model_id')
                ->whereBetween('dream_requests.create_time', array($request->currentDate.' 00:00:00', $request->currentDate.' 23:59:59'))
                ->where([['dream_requests.from_storage_id', $request->line_id], ['dream_requests.to_storage_id', $fromStorage->id]])
                ->select('dream_items.articula_new', 'dream_items.item_name', 'dream_items.sap_code', DB::raw('sum(dream_requests.amount) as amount')
                )->groupBy('dream_requests.model_id')->get();

            if (!empty($items)){
                foreach ($items as $key=>$item)
                {
                    $nestedData['key'] = ++$key;
                    $nestedData['articula_new'] = $item->articula_new;
                    $nestedData['item_name'] = $item->item_name;
                    $nestedData['sap_code'] = $item->sap_code;
                    $nestedData['amount'] = $item->amount;
                    $nestedData['unit'] = 'шт';
                    $data[] = $nestedData;
                }
            }
        }

        $json_data = array(
            "data" => $data
        );

        return json_encode($json_data);
    }

    public function printInsideInvoices(Request $request)
    {
        $storage_id = $request->storage_id;
        if (!is_null($storage_id)) {
            $toStorage = DreamStorage::find($storage_id);
            $managers_of_lines = [
                '1'=>'Содиков Б.',
                '2'=>'Бийменов М.',
                '3'=>'Набихожаев К.',
                '4'=>'Султонов О.',
                '8'=>'Мирзахмедов М.',
                '19'=>'Алибаев К.',
                '20'=>'Алибаев К.',
                '17'=>'___________________________',
                '6'=>'___________________________',
                '22'=>'Алибаев К.',
                '23'=>'Мирзаахмедов М.',
                '26'=>'Хужаназаров Ж.',
                '18'=>'Хусниддинов С.',
                '36'=>'Акрамов Б.',
                '34'=>'Акрамов Б.',
                '42'=>'Акрамов Б.',
                '162'=>'Акрамов Б.',
                '163'=>'Акрамов Б.',
            ];

            $managers_of_warehouse = [
                '4' => 'Султонов О.',
                '1' => 'Содиков Б.',
                '2' => 'Бийминов М.',
                '8' => 'Мирзахмедов М'
            ];

            $production_line = DreamStorage::find($request->line_id);

            $models = DB::table('dream_requests')
                ->join('dream_items', 'dream_items.id', '=', 'dream_requests.model_id')
                ->whereBetween('dream_requests.create_time', array($request->currentDate.' 00:00:00', $request->currentDate.' 23:59:59'))
                ->where([['dream_requests.from_storage_id', $request->line_id],['dream_requests.type_id', self::REQUEST], ['dream_requests.to_storage_id', $toStorage->id]])
                ->select('dream_items.articula_new', 'dream_items.item_name', 'dream_items.sap_code', DB::raw('sum(dream_requests.amount) as amount')
                )
                ->whereRaw('(select count(dream_transaction_request_bind.transaction_list_id) as isAccepted from dream_request_list
                inner join dream_transaction_request_bind on dream_transaction_request_bind.request_list_id=dream_request_list.id
                where dream_request_list.request_id=dream_requests.id
                ) > 0')
                ->groupBy('dream_requests.model_id')->get();
            $items = DB::table('dream_transaction_request_bind')
                ->join('dream_request_list', 'dream_request_list.id', '=', 'dream_transaction_request_bind.request_list_id')
                ->join('dream_requests', 'dream_requests.id', '=', 'dream_request_list.request_id')
                ->join('dream_transaction_list', 'dream_transaction_list.id', '=', 'dream_transaction_request_bind.transaction_list_id')
                ->join('dream_transaction_storage_bind', 'dream_transaction_storage_bind.transaction_id', '=', 'dream_transaction_list.transaction_id')
                ->join('dream_transactions', 'dream_transactions.id', '=', 'dream_transaction_list.transaction_id')
                ->join('dream_items', 'dream_items.id', '=', 'dream_transaction_list.item_id')
                ->join('dream_units', 'dream_units.id', '=', 'dream_transaction_list.unit_id')
                ->whereBetween('dream_transactions.create_time', array($request->currentDate.' 00:00:00', $request->currentDate.' 23:59:59'))
                ->where([['dream_transaction_storage_bind.storage_id', $request->line_id], ['dream_transaction_storage_bind.operation_id', self::INPUT], ['dream_requests.to_storage_id', $toStorage->id],['dream_requests.type_id', self::REQUEST]])
                ->whereNull('dream_transaction_list.delete_time')
                ->select('dream_units.unit as unit_name', 'dream_items.articula_new', 'dream_items.item_name', 'dream_items.sap_code',
                    DB::raw('sum(dream_transaction_list.amount) as amount')
                )->groupBy('dream_transaction_list.item_id', 'dream_transaction_list.unit_id')->get();

            $numberOfRequestForLine = DB::table('dream_requests')->where([['to_storage_id', $toStorage->id], ['dream_requests.create_time', '<=', $request->currentDate.' 00:00:00']])
                ->select(DB::raw('DATE(dream_requests.create_time) as date'))
                ->groupBy('date')->get()->count();

//            $ids = DB::table('dream_requests')
//                ->join('dream_request_list', 'dream_request_list.request_id', '=', 'dream_requests.id')
//                ->join('dream_transaction_request_bind', 'dream_transaction_request_bind.request_list_id', '=', 'dream_request_list.id')
//                ->where([['dream_requests.to_storage_id', $toStorage->id], ['dream_requests.create_time', '>=', $request->currentDate.' 00:00:00'],
//                    ['dream_requests.create_time', '<=', $request->currentDate.' 23:59:00'], ['dream_requests.from_storage_id', $request->line_id], ['dream_requests.type_id', self::REQUEST]])
//                ->select(DB::raw('group_concat(dream_requests.id) as ids'))
//                ->groupBy('dream_requests.id')
//                ->get();
            $ids = DB::select("
                select dream_requests.id from dream_requests
                inner join dream_request_list on dream_request_list.request_id = dream_requests.id
                inner join dream_transaction_request_bind on dream_transaction_request_bind.request_list_id = dream_request_list.id
                inner join dream_transaction_list on dream_transaction_list.id = dream_transaction_request_bind.transaction_list_id
                inner join dream_transactions on dream_transactions.id = dream_transaction_list.transaction_id
                where dream_requests.to_storage_id = $toStorage->id and dream_transactions.create_time between '$request->currentDate 00:00:00' and '$request->currentDate 23:59:00'
                and dream_requests.from_storage_id = $request->line_id and dream_requests.type_id = ".self::REQUEST."
                group by dream_requests.id
            ");
            $str = '';
            foreach ($ids as $id){
                $str .= $id->id.',';
            }
            $x = $request->invoiceNumber;
//            if (!is_null($request->invoiceNumber)){
//                $x = $request->invoiceNumber;
//            }else{
//                $insideInvoiceNumber = DB::select("
//                    select
//                    (
//                        select count(distinct ds1.id, date(dt.create_time)) from dream_transactions dt
//                        inner join dream_transaction_storage_bind dtsb on dt.id = dtsb.transaction_id
//                        inner join dream_storages ds on dtsb.storage_id = ds.id
//                        inner join dream_storage_types dst on ds.type_id = dst.id
//                        inner join dream_transaction_types dtt on dtsb.operation_id = dtt.id
//                        inner join dream_transaction_list dtl on dtl.transaction_id and dtl.delete_time is null
//                        inner join dream_transaction_storage_bind dtsb1 on dt.id = dtsb1.transaction_id
//                        inner join dream_storages ds1 on dtsb1.storage_id = ds1.id
//                        inner join dream_storage_types dst1 on ds1.type_id = dst1.id
//                        inner join dream_transaction_types dtt1 on dtsb1.operation_id = dtt1.id
//                        where dst.name = \"warehouse\" and dtt.operation = -1 and (dst1.name = \"production_line\" or dst1.name = \"other\") and dtt1.operation = 1 and ds.id = $request->storage_id and date(dt.create_time) < date('$request->currentDate') and year(dt.create_time) = year(now())
//                    )
//                    +
//                    (
//                        select count(distinct ds1.id, date(dt.create_time)) from dream_transactions dt
//                        inner join dream_transaction_storage_bind dtsb on dt.id = dtsb.transaction_id
//                        inner join dream_storages ds on dtsb.storage_id = ds.id
//                        inner join dream_storage_types dst on ds.type_id = dst.id
//                        inner join dream_transaction_types dtt on dtsb.operation_id = dtt.id
//                        inner join dream_transaction_list dtl on dtl.transaction_id and dtl.delete_time is null
//                        inner join dream_transaction_storage_bind dtsb1 on dt.id = dtsb1.transaction_id
//                        inner join dream_storages ds1 on dtsb1.storage_id = ds1.id
//                        inner join dream_storage_types dst1 on ds1.type_id = dst1.id
//                        inner join dream_transaction_types dtt1 on dtsb1.operation_id = dtt1.id
//                        where dst.name = \"warehouse\" and dtt.operation = -1 and (dst1.name = \"production_line\" or dst1.name = \"other\") and dtt1.operation = 1 and ds.id = $request->storage_id and date(dt.create_time) = date('$request->currentDate') and year(dt.create_time) = year(now()) and ds1.id < $request->line_id
//                    )
//                    as invoiceNumber
//                ");

//
//                if (!empty($insideInvoiceNumber[0]->invoiceNumber)){
//                    $x = $insideInvoiceNumber[0]->invoiceNumber+1;
//
//                }else{
//                    $x = 0;
//                }
//            }
            $pages = $this->breakToPages($items, $models);
            $date = Carbon::now()->format('Y-m-d H:i:s');
            $data = [
                'ids' => $str,
                'models' => $models,
                'pages' => $pages,
                'created_date' => substr($request->currentDate, 0, 10),
                'created_time' => substr($date, 11, 5),
                'production_line' => $production_line->name,
                'line_id' => $request->line_id,
                'storage_name' => $toStorage->name,
                'request_id' => $numberOfRequestForLine,
                'manager_of_line' => (isset($managers_of_lines[$production_line->id])) ? $managers_of_lines[$production_line->id] : null,
                'manager_of_warehouse' => (isset($managers_of_warehouse[$toStorage->id])) ? $managers_of_warehouse[$toStorage->id] : '____________________',
                'invoiceNumber' => $x,
                'storage_type' => $production_line->type_id
            ];
            $pdf = PDF::loadView('pdf.insideInvoicePdf', $data);
//        $pdf->setEncryption('1','sherzod',array('copy'));
            return $pdf->stream('insideInvoicePdf.pdf', array("Attachment" => false));
        }

        return null;
    }

    public function breakToPages($items, $models){
        $arrayCount = count($items);
        $currentPage = 0;
        $rowCount = 0;
        $pages = [];
        $shouldBreak = false;
        if (!is_null($models)){
            foreach ($models as $model){
                $rowCount += ceil((float)(iconv_strlen($model->item_name, 'UTF-8')/68));
            }
        }
        for ($i=0; $i<$arrayCount; $i++){
            $rowCount += ceil((float)(iconv_strlen($items[$i]->item_name, 'UTF-8')/68));
            if ($currentPage==0){
                if ($rowCount>34){
                    $shouldBreak = true;
                }
            }else{
                if ($rowCount>44){
                    $shouldBreak = true;
                }
            }
            if ($shouldBreak){
                $rowCount = 0;
                $i-=1;
                $currentPage++;
                $shouldBreak = false;
                continue;
            }else{
                $pages[$currentPage][] = $items[$i];
            }
        }
        return $pages;
    }



    public function inputOutputTransaction($item, $model_transaction_id, $child_transaction_id){
        $result = DB::transaction(function () use($item, $model_transaction_id, $child_transaction_id){

            if (!is_null($child_transaction_id)){
                $item_children = DB::table('dream_item_bind')->join('dream_storage_item_bind', function ($join){
                    $join->on('dream_storage_item_bind.item_id', '=', 'dream_item_bind.child_id');
                    $join->on('dream_storage_item_bind.unit_id', '=', 'dream_item_bind.unit_id');
                })->select('dream_storage_item_bind.item_id', 'dream_storage_item_bind.unit_id', 'dream_item_bind.quantity')
                    ->where('dream_item_bind.parent_id', $item->item_id)->groupBy('dream_storage_item_bind.item_id', 'dream_storage_item_bind.unit_id', 'dream_item_bind.quantity')->get();

                foreach ($item_children as $child){
                    $this->insertLineTransactionItem($child->item_id, $child->unit_id, ($child->quantity*$item->amount), $child_transaction_id);
                }
            }

            $this->insertLineTransactionItem($item->item_id, $item->unit_id, $item->amount, $model_transaction_id);
            return $item;
        });
        return $result;
    }

    public function createLineTransaction($transaction_id, $line_id, $operation){
        $bind_data = [
            'transaction_id'=>$transaction_id,
            'storage_id'=>$line_id,
            'operation_id'=>$operation
        ];
        DB::table('dream_transaction_storage_bind')->insert($bind_data);
    }

    public function insertLineTransactionItem($item_id, $unit_id, $amount, $transaction_id){
        $data = [
            'transaction_id'=>$transaction_id,
            'item_id'=>$item_id,
            'unit_id'=>$unit_id,
            'amount'=>$amount
        ];
        DB::table('dream_transaction_list')->insert($data);

    }
}

<?php

namespace App\Http\Controllers;

use App\DreamOrder;
use App\DreamStorage;
use App\DreamSupplier;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Input;
use Arkadedigital\CountryList\CountryList;

class DreamSupplierController extends Controller
{
    public function show_suppliers(){
        $list = new CountryList();
        $countries =  $list->getList('ru');
        $sup_type = array(1=>'местный', 2=>'импорт');
        return view('index', ['page'=>'all_suppliers', 'countries'=>$countries, 'sup_types'=>$sup_type]);
    }

    public function getSuppliers(Request $request){
        $hasPerm = true;
        $columns = array(
            0 => 'id',
            1 => 'state',
            2 => 'name',
            3 => 'address',
            4 => 'phone',
            5 => 'website',
            6 => 'storage_info.type_id',
            7 => 'country'
        );
        $sup_type = array(1=>'местный', 2=>'импорт');
        $totalData = DreamStorage::where('type_id', 7)->get()->count();
        if ($request->input('length')==-1){
            $limit = $totalData;
        }else{
            $limit = $request->input('length');
        }
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if (Gate::denies('delete', new DreamOrder)){
            $hasPerm = false;
        }
        $query_text = "select dream_storages.*, storage_info.address, storage_info.phone, storage_info.website, storage_info.country, storage_info.type_id as sup_type from dream_storages" .
            " left join storage_info on storage_info.storage_id=dream_storages.id where dream_storages.type_id=7";
        $search_text = '';
        if (!empty($request->input('search.value'))){
            $search = $request->input('search.value');
            $search_text .= "
                     and ( dream_storages.name like '%$search%' || storage_info.address like '%$search%' || storage_info.phone like '%$search%' || storage_info.website like '%$search%' || 
                     storage_info.country like '%$search%')";
        }


        $query_text.=$search_text." order by $order $dir limit $limit offset $start;";
        $items = DB::select($query_text);
        $totalCount = DB::select('select found_rows() as total_count')[0];
        $data = array();
        if (!empty($items)){
            foreach ($items as $item)
            {
                $nestedData['DT_RowId'] = "row_".$item->id;
                $nestedData['id'] = $item->id;
                if ($item->state==1){
                    $nestedData['is_active'] = "<span class='label label-warning' style='font-size: 14px; padding-top: 0;'>активный</span>";
                }else{
                    $nestedData['is_active'] = "<span class='label label-danger' style='font-size: 14px; padding-top: 0;'>не активный</span>";
                }
                $nestedData['supplier_id'] = $item->id;
                $nestedData['name'] = $item->name;
                $nestedData['address'] = $item->address;
                $nestedData['phone_number'] = $item->phone;
                $nestedData['website'] = $item->website;
                $nestedData['supplier_type'] = (isset($sup_type[$item->sup_type]))?$sup_type[$item->sup_type]:'';
                $nestedData['country'] = $item->country;
                $nestedData['buttons'] = "<button type='button' class='btn btn-info' data-toggle='modal' data-backdrop='static' data-keyboard='false' style='font-size: 12px' data-target='#show_detail' onclick=\"show_detail($item->id)\"><i class='fa fa-info-circle'></i></button> <button type='button' class='btn btn-warning' data-toggle='modal' data-backdrop='static' data-keyboard='false' style='font-size: 12px' data-target='#show_edit_modal' onclick=\"edit_supplier('".$item->id."')\"><i class='fa fa-edit'></i></button><button type='submit' class='btn btn-danger' style='font-size: 12px' onclick=\"delete_supplier('".$item->id."')\"><i class=\"fa fa-trash\"></i></button>";
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalCount->total_count),
            "data"            => $data,
            "hasPerm"         => $hasPerm
        );

        return json_encode($json_data);
    }

    public function editSupplier(Request $request){
        $supplier = DreamStorage::find($request->supplier_id);
        $list = new CountryList();
        $sup_type = array(1=>'местный', 2=>'импорт');
        $countries =  $list->getList('ru');
        $data = [
            'supplier'=>$supplier,
            'countries'=>$countries,
            'supplier_types'=>$sup_type,

        ];
        return json_encode($data);
    }

    public function show_supplier_items_list(){
        $hasPerm = true;
        if (Gate::denies('delete', new DreamOrder)){
            $hasPerm = false;
        }
        $input = Input::all();
        $supplier_items = DB::table('dream_storage_item_bind')->join('dream_storages', 'dream_storages.id', '=', 'dream_storage_item_bind.storage_id')
            ->join('dream_items', 'dream_items.id', '=', 'dream_storage_item_bind.item_id')->where('dream_storage_item_bind.storage_id', $input['supplier_id'])
            ->select('dream_storage_item_bind.id', 'dream_items.articula_new', 'dream_items.articula_old', 'dream_items.sap_code', 'dream_items.item_name')->get();
        $data = array();
        if (!empty($supplier_items)){
            foreach ($supplier_items as $item)
            {
                $nestedData['DT_RowId'] = "row_".$item->id;
                $nestedData['articula_new'] = $item->articula_new;
                $nestedData['articula_old'] = $item->articula_old;
                $nestedData['sap_code'] = $item->sap_code;
                $nestedData['item_name'] = $item->item_name;
                if ($hasPerm){
                    $nestedData['delete_item'] = "<button type='button' class='btn btn-danger' onclick=\"delete_list_item('".$item->id."')\"><i class='fa fa-trash'></i></button>";
                }else{
                    $nestedData['delete_item'] = "";
                }
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "data" => $data
        );
        return json_encode($json_data);
    }

    public function createSupplier(Request $request){
        if (Gate::denies('delete', new DreamOrder)){
            return response()->json(['errors'=>array('нет доступа!')], 500);
        }

        $request->validate([
            'supplier_type'=>'required',
            'country'=>'required',
            'supplier_name'=>'required',
            'address'=>'required',
            'phone'=>'required'
        ]);

        try{

            $data = [
                'name'=>$request->supplier_name,
                'type_id'=>7
            ];

            $str_id = DreamStorage::create($data)->id;

            $data = [
                'address'=>$request->address,
                'phone'=>$request->phone,
                'website'=>$request->website,
                'type_id'=>1,
                'country'=>$request->country,
                'storage_id'=>$str_id
            ];

            DB::table('storage_info')->insert($data);


            return json_encode($data);
        }catch (\Exception $e){
            return \response()->json(['errors'=>array('возникла ошибка!')], 500);
        }
    }

    public function updateSupplier(Request $request){
        if (Gate::denies('delete', new DreamOrder)){
            return response()->json(['errors'=>array('нет доступа!')], 500);
        }
        try{
            DB::table('dream_storages')->where('id', $request->supplier_id)->update(['name'=>$request->supplier_name]);
            $data = [
                'address'=>$request->address,
                'phone'=>$request->phone,
                'website'=>$request->website,
                'country'=>$request->country,
                'type_id'=>$request->supplier_type,
                'storage_id'=>$request->supplier_id
            ];
            $hasInfo = DB::table('storage_info')->where('storage_id', $request->supplier_id)->get()->first();
            if (is_null($hasInfo)){
                DB::table('storage_info')->insert($data);
            }else{
                DB::table('storage_info')->where('storage_id', $request->supplier_id)->update($data);
            }
            return json_encode($hasInfo);
        }catch (\Exception $e){
            return \response()->json(['errors'=>array('возникла ошибка!', $e)], 500);
        }
    }

    public function get_fit_items(){
        $input = Input::all()['supplier_id'];
        $items = DB::select("select dream_items.* from dream_items 
        inner join dream_storage_item_bind as s_i on s_i.item_id=dream_items.id
        where dream_items.id not in (select dream_storage_item_bind.item_id from dream_storage_item_bind where dream_storage_item_bind.storage_id=$input)
        group by dream_items.id");
        return json_encode($items);
    }

    public function add_supplier_item_list(){
        if (Gate::denies('save', new DreamOrder)){
            return \response()->json(['errors'=>array('нет доступа!')], 500);
        }
        $input = Input::all();
        try{
            $result = DB::transaction(function () use($input){
                $sup_id = $input['supplier_id'];
                $items = json_decode($input['items']);
                foreach ($items as $item){
                    $unit_id = DB::table('dream_storage_item_bind')->where([['item_id', $item]])->get()->first();
                    $sup_item = DB::table('dream_storage_item_bind')->where([['storage_id', $sup_id], ['item_id', $item]])->get()->first();
                    if (is_null($sup_item)){
                        if (!is_null($unit_id)){
                            $data = [
                                'storage_id'=>$sup_id,
                                'item_id'=>$item,
                                'unit_id'=>$unit_id->unit_id
                            ];
                        }else{
                            return \response()->json(['errors'=>array('ошибка!')], 500);
                        }

                        DB::table('dream_storage_item_bind')->insert($data);
                    }
                }
                return $sup_id;
            });
            return json_encode($result);

        }catch(\Exception $e){
            return \response()->json(['errors'=>array('ошибка!')], 500);
        }
    }

    public function deleteSupplier(Request $request){
        if (Gate::denies('delete', new DreamOrder)){
            return \response()->json(['errors'=>array('нет доступа!')], 500);
        }
        try{
            DreamStorage::find($request->supplier_id)->update(['state'=>2]);

            return json_encode($request->supplier_id);
        }catch (\Exception $e){
            return \response()->json(['errors'=>array('возникла ошибка!')], 500);
        }
    }

    public function delete_supplier_list_item(){
        if (Gate::denies('delete', new DreamOrder)){
            return \response()->json(['errors'=>array('нет доступа!')], 500);
        }
        $input = Input::all()['item_list_id'];
        DB::table('dream_storage_item_bind')->where('id', $input)->delete();
        return json_encode($input);
    }

    public function upload_supplier_items_file(Request $request){
        if (Gate::denies('save', new DreamOrder)){
            return \response()->json(['errors'=>array('нет доступа!')], 500);
        }
        try{
            $result = DB::transaction(function () use($request){
                $arr = [];
                $bad_arr = [];
                if($request->hasFile('supplier_item_file'))
                {
                    $sup_id = $request->file_supplier_id;
                    $path = $request->file('supplier_item_file')->getRealPath();
                    $data = \Excel::load($path)->get();
                    foreach ($data as $value){
                        if ($value->articula_new!=''){
                            $item = DB::table('dream_items')->where('dream_items.articula_new', $value->articula_new)->first();
                            if($item!=null){
                                $already_has = DB::table('dream_storage_item_bind')->where([['storage_id', $sup_id], ['item_id', $item->id]])->get()->first();
                                $bindToStorage = DB::table('dream_storage_item_bind')->where([['item_id', $item->id]])->get()->first();
                                if (is_null($already_has) && $this->isDuplicate($value->articula_new, $arr) && !is_null($bindToStorage)){
                                    $data = [
                                        'storage_id'=>$sup_id,
                                        'item_id'=>$item->id,
                                        'unit_id'=>$bindToStorage->unit_id
                                    ];

                                    DB::table('dream_storage_item_bind')->insert($data);
                                    array_push($arr, $item);
                                }elseif(is_null($bindToStorage)){
                                    array_push($bad_arr, ['articula_new'=>$value->articula_new, 'error'=>'добавте склад к этому артикулу']);
                                }
                            }else {
                                array_push($bad_arr, ['articula_new'=>$value->articula_new, 'error'=>'этот артикул в базе не существует']);
                            }
                        }
                    }
                }
                return $bad_arr;
            });
            return json_encode($result);
        }catch (\Exception $e){
            return response()->json(['errors'=>array('невозможно добавить', $e)], 500);
        }
    }

    public function delete_all_supplier_items(){
        if (Gate::denies('delete', new DreamOrder)){
            return \response()->json(['errors'=>array('нет доступа!')], 500);
        }
        $supplier_id = Input::all()['supplier_id'];
        DB::table('dream_storage_item_bind')->where('storage_id', $supplier_id)->delete();
        return json_encode($supplier_id);
    }

    public function isDuplicate($articula, $arr){
        if (empty($arr)){
            return true;
        }
        foreach ($arr as $item){
            if ($articula==$item->articula_new){
                return false;
            }
        }
        return true;
    }
}

<?php

namespace App\Http\Controllers;

use App\DreamLogistics;
use App\DreamOrder;
use App\DreamSupplier;
use App\DreamTransaction;
use App\DreamStorage;
use App\Role;
use Carbon\Carbon;
use Symfony\Component\Translation\Tests\Writer\BackupDumper;
use Validator;
use function foo\func;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class DreamOrderController extends Controller
{
    const OUTPUT = 2;
    const SUPER_ADMIN = 1;
    const DEFECT = 3;
    const RETURN_ITEM = 4;
    const INPUT = 1;
    const STATE_ACTIVE = 1;
    const STATE_INACTIVE = 0;
    const STATE_CLOSED = 2;
    const PRODUCTION_LINE_ID=2;
    const LOCAL_WAREHOUSE = 1;
    const LOCAL = 1;
    const IMPORT = 2;
    const ORDER = 1;

    public function orders_page(){
        if (Gate::denies('read', new DreamOrder)){
            abort('404');
        }
        $currencies = DB::table('dream_currencies')->get();
        $users = DB::table('dream_orders')
            ->join('users', 'users.id', '=', 'dream_orders.user_id')
            ->select('users.*')->groupBy('users.id')->get();
        $suppliers = DreamStorage::where([['type_id', 7], ['state', 1]])->get();
        $states = DB::table('dream_order_states')->get();
        return view('index', ['page'=>'all_orders', 'currencies'=>$currencies, 'users'=>$users, 'suppliers'=>$suppliers, 'states'=>$states]);
    }
    // 917788965 timur

    /**
     * @param Request $request
     */
    public function all_orders(Request $request){
        $all_orders = DreamOrder::all();
        foreach ($all_orders as $order){
            $items_count = DB::table('dream_order_list')->where('order_id', $order->id)->count();
            if ($items_count==0){
                DreamOrder::find($order->id)->delete();
            }
        }

        $columns = array(
            0 => 'id',
            1 => 'state_name',
            2 => 'percent',
            3 => 'shipment_time',
            4 => 'user_name',
            5 => 'storage_name',
            6 => 'type_name',
            7 => 'invoice_number',
            8 => 'total_price',
            9 => 'deposit',
            10 => 'before_shipment',
            11 => 'due_time',
            12 => 'description',
            13 => 'create_time'
        );

        $filterColumns = array(
            0 => 'id',
            'filter_by_state' => 'dream_order_states.id',
            2 => 'percent',
            3 => 'create_time',
            'filter_by_user' => 'users.id',
            'filter_by_supplier' => 'dream_storages.id',
            6 => 'invoice_number',
            7 => 'shipment_time',
            8 => 'description'
        );
        $totalData = DB::table('dream_orders')->get()->count();
        if ($request->input('length')==-1){
            $limit = $totalData;
        }else{
            $limit = $request->input('length');
        }
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $filterArr = json_decode($request->filterArr);

        $filter_str = '';
        if (!is_null($filterArr)){
            foreach ($filterArr as $key=>$values){
                if (!empty($values)){
                    $filter_str.=" and $filterColumns[$key] in (";
                    foreach ($values as $value){
                        $filter_str.=$value.',';
                    }
                    $filter_str = substr($filter_str, 0, strlen($filter_str)-1).')';
                }
            }
        }

        if ($request->start_date && $request->end_date){
            $filter_str.=" and dream_orders.shipment_time between date('$request->start_date') and date('$request->end_date')";
        }

        if ($request->filter_by_type != 0){
            $filter_str.=" and dst.id=$request->filter_by_type";
        }
        $query_text = "
            SELECT SQL_CALC_FOUND_ROWS  dream_orders.id, dream_orders.*, users.firstname as user_name, dream_storages.name as storage_name, dream_order_states.name as state_name,
            (sum(order_transaction.amount)/(
            select sum(dream_order_list.amount) as amount from dream_order_list where dream_order_list.order_id=dream_orders.id group by dream_order_list.order_id
            )) as percent, dream_billings.deposit, dream_billings.before_shipment, dream_billings.due_time, dream_currencies.name as currency_name, dst.name as type_name, price.total_price

            FROM dream_orders
            left join dream_billings on dream_billings.id=dream_orders.billing_id
            left join dream_currencies on dream_currencies.id=dream_billings.currency_id
            inner join users on users.id=dream_orders.user_id
            inner join dream_storages on dream_storages.id=dream_orders.supplier_id
            left join storage_info si on si.storage_id=dream_storages.id
            left join dream_supplier_types dst on dst.id=si.type_id
            inner join dream_order_states on dream_order_states.id=dream_orders.state_id
            left join (
            select dream_transaction_list.amount as amount, dream_order_list.order_id as order_id from dream_transaction_list
            inner join dream_transaction_storage_bind on dream_transaction_storage_bind.transaction_id=dream_transaction_list.transaction_id
            inner join dream_storages on dream_storages.id=dream_transaction_storage_bind.storage_id
            inner join transaction_order_bind on transaction_order_bind.transaction_list_id=dream_transaction_list.id
            inner join dream_order_list on dream_order_list.id=transaction_order_bind.order_list_id
            where dream_storages.type_id=1 and dream_transaction_list.delete_time is null
            ) as order_transaction on order_transaction.order_id=dream_orders.id
            left join (
            select ROUND(sum(price_per_unit * l.amount), 3) as total_price, l.order_id from dream_order_list l
            group by l.order_id
            ) as price on price.order_id = dream_orders.id
            where dream_storages.type_id=7 and dream_orders.delete_time is null
        ".$filter_str;

        $search_text = '';
        if (!empty($request->input('search.value'))){
            $search = $request->input('search.value');
            $search_text .= "
                     and ( users.firstname like '%$search%' || dream_orders.id like '%$search%' || dream_orders.invoice_number like '%$search%' || dream_orders.create_time like '%$search%' ||
                     dream_orders.shipment_time like '%$search%' || dream_order_states.name like '%$search%' || dream_storages.name like '%$search%' || dream_orders.description like '%$search%')
                    ";
        }


        $query_text.=$search_text." group by dream_orders.id, dst.id order by $order $dir limit $limit offset $start;";
        $items = DB::select($query_text);
        $totalCount = DB::select('select found_rows() as total_count')[0];
        $data = array();
        if (!empty($items)){
            foreach ($items as $item)
            {
                $nestedData['DT_RowId'] = "order_row_".$item->id;
                $nestedData['id'] = $item->id;
                $nestedData['state_name'] = ($item->state_id==self::STATE_ACTIVE)?"<span class='label label-primary' style='font-size: 14px; padding-top: 0;'>".$item->state_name."</span>":"<span class='label label-danger' style='font-size: 14px; padding-top: 0;'>".$item->state_name."</span>";
                if ($item->percent>=1){
                    $nestedData['percent'] = "<div class='progress progress-striped' style='height: 22px; padding: 0; margin: 0; background-color: #eababa'><div class='progress-bar progress-bar-danger' role='progressbar' aria-valuenow='100' aria-valuemin='0' aria-valuemax='0' style='width:100%; color: black'>".(round($item->percent, 3)*100)."<span>%</span></div></div>";
                }else if ($item->percent>0 && $item->percent<1){
                    $nestedData['percent'] = "<div class='progress progress-striped' style='height: 22px; padding: 0; margin: 0; background-color: #eababa'><p style='position: absolute; color: black; font-weight: bold;'>".(round($item->percent, 3)*100)." %</p><div class='progress-bar progress-bar-stripped' role='progressbar' aria-valuenow='".(round($item->percent, 3)*100)."' aria-valuemin='0' aria-valuemax='0' style='width:".(round($item->percent, 3)*100)."%; text-align: center'></div></div>";
                }else{
                    $nestedData['percent'] = "<div class='progress progress-striped' style='height: 22px; padding: 0; margin: 0; background-color: #eababa'><p style='position: absolute; color: black; font-weight: bold;'>0 %</p><div class='progress-bar progress-bar-stripped' role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='0' style='width:".(round($item->percent, 3)*100)."%; text-align: center'></div></div>";
                }
                $nestedData['create_time'] = Carbon::createFromFormat('Y-m-d H:i:s', $item->create_time)->format('Y-m-d H:i');
                $nestedData['user_name'] = $item->user_name;
                $nestedData['storage_name'] = $item->storage_name;
                $nestedData['invoice_number'] = $item->invoice_number;
                $nestedData['deposit'] = $item->deposit;
                $nestedData['before_shipment'] = $item->before_shipment;
                $nestedData['due_time'] = $item->due_time;
                $nestedData['type'] = $item->type_name;
                $nestedData['total_price'] = $item->total_price;
                $nestedData['currency_name'] = $item->currency_name;
                $nestedData['shipment_time'] = (is_null($item->shipment_time))?null:Carbon::createFromFormat('Y-m-d H:i:s', $item->shipment_time)->format('Y-m-d');
                $nestedData['description'] = $item->description;
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalCount->total_count),
            "data"            => $data
        );

        return json_encode($json_data);
    }

    public function showOrderList(Request $request){
        $data = array();
        $total = 0;
        $allow = true;
        if (Gate::denies('delete', new DreamOrder)){
            $allow = false;
        }
        try{
            $user_id = Auth::user()->id;
            $role_id = DB::table('user_role')
                ->where('user_role.user_id', $user_id)->first()->role_id;
            $order_id = $request->order_id;
            $order = DB::table('dream_orders')
                ->leftJoin('dream_storages', 'dream_storages.id', '=', 'dream_orders.supplier_id')
                ->leftJoin('dream_billings', 'dream_billings.id', '=', 'dream_orders.billing_id')
                ->leftJoin('dream_currencies', 'dream_currencies.id', '=', 'dream_billings.currency_id')
                ->select('dream_orders.*', 'dream_storages.name as supplier_name', DB::raw('IFNULL(dream_currencies.name, \' \') as currency'))
                ->where([['dream_orders.id', $order_id]])->get()->first();
            $order_data = [
                'supplier_name'=>(is_null($order->supplier_name))?'нет поставщика':$order->supplier_name,
                'order_id'=>$order->id,
                'invoice_number'=>(is_null($order->invoice_number))?'нет invoice num':$order->invoice_number,
                'delivery_time'=>Carbon::createFromFormat('Y-m-d H:i:s', $order->create_time)->addDays($order->shipment_time)->format('Y-m-d'),
                'orderState'=>$order->state_id,
                'supplier_id'=>$order->supplier_id,
            ];
            $items = DB::table('dream_order_list')
                ->leftJoin('dream_items_data', 'dream_items_data.item_id', '=', 'dream_order_list.item_id')
                ->join('dream_items', 'dream_items.id', '=', 'dream_order_list.item_id')
                ->join('dream_units', 'dream_units.id', '=', 'dream_order_list.unit_id')
                ->leftJoin(
                    DB::raw('(
                    select sum(dream_transaction_list.amount) as amount, o_l.id as order_list_id from dream_transaction_list
                    inner join dream_transaction_storage_bind on dream_transaction_storage_bind.transaction_id=dream_transaction_list.transaction_id
                    inner join dream_storages on dream_storages.id=dream_transaction_storage_bind.storage_id
                    inner join transaction_order_bind on transaction_order_bind.transaction_list_id=dream_transaction_list.id
                    inner join dream_order_list as o_l on o_l.id=transaction_order_bind.order_list_id
                    where dream_storages.type_id=1 and dream_transaction_list.delete_time is null group by o_l.id
                    ) as order_transaction'), 'order_transaction.order_list_id', '=', 'dream_order_list.id')
                ->select('dream_items.articula_new', 'dream_items.articula_old', 'dream_items.sap_code', 'dream_items.item_name', 'dream_units.unit', 'dream_units.id as unit_id',
                    'dream_order_list.*', 'dream_items_data.name as english_name', 'order_transaction.amount as complete'
                    )
                ->where('dream_order_list.order_id', $order_id)->get();
            $total_price = 0;
            foreach ($items as $key=>$item){
                $total += $item->amount;
                $total_price += $item->price_per_unit*round($item->amount, 4);
                $nested['list_id'] = $item->id;
                $nested['articula_new'] = $item->articula_new;
                $nested['english_name'] = $item->english_name;
                $nested['sap_code'] = $item->sap_code;
                $nested['item_name'] = "<p style='color: black' title='".$item->item_name."'>".str_limit($item->item_name, 150)."</p>";
                $nested['unit'] = $item->unit;
                $nested['unit_id'] = $item->unit_id;
                $nested['amount'] = round($item->amount, 4);
                $nested['gross_weight'] = $item->gross_weight;
                if (Gate::denies('viewOrderPrice', new DreamOrder)){
                    $nested['price_per_unit'] = null;
                    $nested['total_price'] = null;
                    $nested['currency'] = null;
                }else{
                    $nested['price_per_unit'] = $item->price_per_unit;
                    $nested['total_price'] = round($item->price_per_unit*round($item->amount, 4), 2);
                    $nested['currency'] = $order->currency;
                }

                $nested['complete'] = round($item->complete, 4);
//                $nested['complete'] = $this->get_amount($item->id, $item->item_id);
                $nested['item_id'] = $item->item_id;
                $nested['hasPerm'] = $allow;
                if ($allow){
                    if ($order->state_id==self::STATE_ACTIVE){
                        $nested['buttons'] = "<button type='button' data-toggle='modal' data-backdrop='static' data-keyboard='false' data-target='#edit_amount_modal' class='btn btn-info' onclick='set_list_data(".$item->id.")'><i class='fa fa-edit'></i></button>" .
                            "<button type='button' class='btn btn-danger' onclick=\"delete_item($item->id)\" ><i class='fa fa-trash'></i></button>";
                    }else{
                        $nested['buttons'] = "";
                    }
                }else{
                    $nested['buttons'] = "";
                }
                $data[] = $nested;
            }

            $dat = array(
                'items' => $items,
                'data' => $data,
                'hasPerm' => $allow,
                'order' => $order_data,
                'total' => $total,
                'total_price' => round($total_price, 3)
            );
            return json_encode($dat);
        }catch(\Exception $e){
            return response()->json(['errors'=>array('невозможно загрузит!')], 500);
        }
    }

    public function get_orders_belongs_storage(Request $request){
//        session()->flash('input_fields');

        try{
            $storage_id = $request->storage_id;
            $columns = array(
                0 => 'order_id',
                1 => 'articula_new',
                2 => 'sap_code',
                3 => 'item_name',
                4 => 'supplier_name',
                5 => 'invoice_number',
                6 => 'container_number',
                7 => 'customer',
                8 => 'create_time',
                9 => 'delivery_time',
                10 => 'amount',
                11 => 'complete',
                12 => 'input_field',
                13 => 'unit'
            );

            if ($request->input('length')==-1){
                $limit = '';
            }else{
                $limit = ' limit '.$request->input('length');
            }
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');

            $filterText = '';
            if (!is_null($request->suppliers_id)){
                $filterText .= " and logistics.from_storage_id=$request->suppliers_id ";
            }

            $invoices = '';
            if (!is_null($request->invoices)){
                foreach ($request->invoices as $invoice){
                    $invoices .= "'".$invoice."',";
                }
                $invoices = substr($invoices, 0, strlen($invoices)-1);
                $filterText .= " and logistics.invoice_number in ($invoices) ";
            }

            $query_text = "
                SELECT SQL_CALC_FOUND_ROWS logistics_list.id,
                dream_orders.id as order_id, logistics.*, users.firstname as customer, dream_storages.name as supplier_name, logistics_list.id as logistics_list_id, logistics_list.amount,
                dream_items.articula_new, dream_items.sap_code, dream_items.item_name, dream_units.unit,
                sum(trl.amount) as complete, ROUND((IFNULL(logistics_list.amount, 0) - IFNULL(sum(trl.amount), 0)), 3) as diff
                FROM logistics
                inner join logistics_list on logistics_list.logistics_id=logistics.id
                inner join dream_order_list on dream_order_list.id=logistics_list.order_list_id
                inner join dream_items on dream_items.id=dream_order_list.item_id
                inner join dream_units on dream_units.id=dream_order_list.unit_id
                inner join dream_orders on dream_orders.id=dream_order_list.order_id
                inner join dream_storages on dream_storages.id=dream_orders.supplier_id
                inner join users on users.id=logistics.user_id
                left join (
                select transaction_order_bind.logistics_list_id, dream_transaction_list.amount from transaction_order_bind
                inner join dream_transaction_list on dream_transaction_list.id=transaction_order_bind.transaction_list_id
                inner join dream_transaction_storage_bind on dream_transaction_storage_bind.transaction_id=dream_transaction_list.transaction_id
                where dream_transaction_storage_bind.operation_id=1 and dream_transaction_list.delete_time is null
                ) as trl on trl.logistics_list_id=logistics_list.id
                where logistics.to_storage_id=$storage_id and logistics.state_id=1
            ";

            $search_text = '';
            if (!empty($request->input('search.value'))){
                $search = $request->input('search.value');
                $search_text .= "
                     and ( dream_items.articula_new like \"%$search%\" || logistics.invoice_number like \"%$search%\" || dream_orders.create_time like \"%$search%\" || dream_items.sap_code like \"%$search%\" ||
                     dream_items.item_name like \"%$search%\" || logistics.delivery_time like \"%$search%\" || users.firstname like \"%$search%\" || dream_orders.id like \"%$search%\"  || logistics.container_number like \"%$search%\"  )
                    ";
            }
            $query_text.=$filterText.$search_text." group by logistics_list.id having diff > 0 order by $order $dir $limit offset $start;";
            $items = DB::select($query_text);
            $totalCount = DB::select('select found_rows() as total_count')[0];
            $activeOrdersCount = DB::table('logistics')
                ->where([['logistics.state_id', 1], ['logistics.to_storage_id', $storage_id]])->get()->count();

            $data = array();
            if (!empty($items)){
                foreach ($items as $item)
                {
                    $diff = round($item->amount, 3) - round($item->complete, 3);
                    $maxInpt = $diff+50;
                    $nestedData['DT_RowId'] = "order_row_".$item->logistics_list_id;
                    $nestedData['logistics_list_id'] = $item->logistics_list_id;
                    $nestedData['order_id'] = $item->id;
                    $nestedData['articula_new'] = $item->articula_new;
                    $nestedData['sap_code'] = $item->sap_code;
                    $nestedData['item_name'] = $item->item_name;
                    $nestedData['invoice_number'] = $item->invoice_number;
                    $nestedData['customer'] = $item->customer;
                    $nestedData['supplier_name'] = $item->supplier_name;
                    $nestedData['container_number'] = $item->container_number;
                    $nestedData['create_time'] = str_limit($item->create_time, 16, '');
                    $nestedData['delivery_time'] = str_limit($item->delivery_time, 16, '');
                    $nestedData['amount'] = round($item->amount, 3);
                    $nestedData['complete'] = round($item->complete, 3);
                    $nestedData['remaining'] = $diff;
                    if ($item->complete>=$item->amount){
                        $nestedData['input_field'] = "";
                    }else{
                        $nestedData['input_field'] = "<input type='number' name='brought_item_amount[]' max='$maxInpt' id='$item->logistics_list_id' onkeyup=\"if(parseFloat(this.value) >= parseFloat(this.max)) this.value = this.max; save_value_in_session(this);\" style=' width: 100px' value='' placeholder='0'>"."<input style='margin-left: 15px; margin-top: 5px' type='checkbox' id='checkbox$item->logistics_list_id' onclick=\"fillInput('".$item->logistics_list_id."', '".$diff."')\" value='$diff'>";
                    }
                    $nestedData['unit'] = $item->unit;
                    $data[] = $nestedData;
                }
            }
            $json_data = array(
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => intval($totalCount->total_count),
                "recordsFiltered" => intval($totalCount->total_count),
                "data"            => $data,
                "activeOrders"    => $activeOrdersCount
            );
            return json_encode($json_data);
        }catch (\Exception $e){
            return \response()->json(['errors'=>array($e)], 500);
        }
    }

    public function create_order(){
        if (Gate::denies('save', new DreamOrder)){
            abort('404');
        }
        $suppliers = DreamStorage::whereIn('type_id', array(7))->where('state',1)->get();
        $currencies = DB::table('dream_currencies')->get();
        return view('index', ['page'=>'create_order', 'suppliers'=>$suppliers, 'currencies'=>$currencies]);
    }

    public function add_item_to_exist_order(Request $request){
        if (Gate::denies('save', new DreamOrder)){
            return \response()->json(['errors'=>array('нет доступа!')], 500);
        }
        try{
            $auth_id = Auth::user()->id;
            $order = DreamOrder::find($request->order_id);
            $unit_id = DB::table('dream_storage_item_bind')->where([['item_id', $request->item_id]])->first()->unit_id;
            $exist_item = DB::table('dream_order_list')->where([['item_id', $request->item_id], ['order_id', $request->order_id]])->first();
            if (!is_null($exist_item)){
                return \response()->json(['errors'=>array('существует!')], 500);
            }
            if ($auth_id==$order->user_id){
                if ($request->amount>0){
                    $data = [
                        'order_id'=>$request->order_id,
                        'item_id'=>$request->item_id,
                        'amount'=>$request->amount,
                        'price_per_unit'=>(is_null($request->price_per_unit))?0:$request->price_per_unit,
                        'unit_id'=>$unit_id
                    ];
                    $order_list_id = DB::table('dream_order_list')->insertGetId($data);
                    $transaction_id = DB::table('dream_order_list')
                        ->join('transaction_order_bind', 'transaction_order_bind.order_list_id', '=', 'dream_order_list.id')
                        ->join('dream_transaction_list', 'dream_transaction_list.id', '=', 'transaction_order_bind.transaction_list_id')
                        ->select('dream_transaction_list.transaction_id')->where('dream_order_list.order_id', $request->order_id)
                        ->whereNull('dream_transaction_list.delete_time')->get()->first()->transaction_id;
                    $data1 = [
                        'transaction_id' => $transaction_id,
                        'item_id' => $request->item_id,
                        'unit_id' => $unit_id,
                        'amount' => $request->amount
                    ];
                    $tr_list_id = DB::table('dream_transaction_list')->insertGetId($data1);

                    DB::table('transaction_order_bind')->insert(['order_list_id'=>$order_list_id, 'transaction_list_id'=>$tr_list_id, 'logistics_list_id'=>null]);
                }
            }else{
                return \response()->json(['errors'=>array('вам нельзя!')], 500);
            }
            return response()->json($request->order_id);
        }catch(\Exception $e){
            return \response()->json(['errors'=>array('ошибка!', $e)], 500);
        }
    }

    public function get_item_remaining($storage_id, $item_id)
    {
        $total = DB::select("select sum(operation*amount) as total from dream_transaction_list ".
            "left join dream_transaction_storage_bind on dream_transaction_list.transaction_id=dream_transaction_storage_bind.transaction_id and dream_transaction_storage_bind.storage_id=$storage_id " .
            "left join dream_transaction_types on dream_transaction_types.id=dream_transaction_storage_bind.operation_id where item_id=$item_id ")[0];
        if ($total->total == null){
            $total->total = 0;
        }
        return $total->total;
    }

    public function get_amount($order_list_id, $item_id)
    {
        $total = DB::select("select sum(amount) as total from dream_transaction_list right join dream_order_transaction_bind on dream_transaction_list.id=dream_order_transaction_bind.transaction_list_id ".
            "inner join dream_transaction_storage_bind on dream_transaction_storage_bind.transaction_id=dream_transaction_list.transaction_id " .
            "inner join dream_storages on dream_storages.id=dream_transaction_storage_bind.storage_id " .
            "where dream_order_transaction_bind.order_list_id=$order_list_id and dream_storages.type_id=1 and dream_transaction_storage_bind.operation_id=1 and dream_transaction_list.item_id=$item_id")[0];
        if ($total->total == null){
            $total->total = 0;
        }
        return $total->total;
    }

    public function save_order(Request $request){
        if (Gate::denies('save', new DreamOrder)){
            return \response()->json(['errors'=>array('нет доступа!')], 500);
        }
        $validateNames = array(
            'items'=>'элементы',
            'delivery_time'=>'дата прибытие',
            'supplier_id'=>'поставщик',
            'description'=>'примечание',
            'deposit'=>'депозит',
            'before_shipment'=>'перед отправкой',
            'due_time'=>'конец'
        );
        if (is_null($request->invoice_number)){
            $validator = Validator::make($request->all(), [
                'items' => 'required',
//                'delivery_time' => 'required',
                'supplier_id' => 'required',
//                'deposit' => 'required',
//                'due_time' => 'required',
//                'before_shipment' => 'required',
//                'currency' => 'required',
            ]);
        }else{
            $validator = Validator::make($request->all(), [
                'items' => 'required',
//                'delivery_time' => 'required',
                'supplier_id' => 'required',
                'invoice_number' => 'unique:dream_orders',
//                'deposit' => 'required',
//                'due_time' => 'required',
//                'before_shipment' => 'required',
//                'currency' => 'required',
            ]);
        }

        $validator->setAttributeNames($validateNames);

        if ($validator->fails()) {
            return response()->json(['errors'=>array($validator->errors()->all())], 500);
        }

        $validItems = json_decode($request->items);

        if (count($validItems->data)<=0){
            return response()->json(['errors'=>array('нет данные!')], 500);
        }

        try{
            $res = DB::transaction(function () use($request){
                $items = json_decode($request->items);
                $user_id = Auth::user()->id;

                $billing_data = [
                    'deposit'=>$request->deposit,
                    'before_shipment'=>$request->before_shipment,
                    'due_time'=>$request->due_time,
                    'currency_id'=>$request->currency,
                ];

                $billing_id = DB::table('dream_billings')->insertGetId($billing_data);
                if (is_null($request->invoice_number)){
                    $invoice_number = $request->supplier_id.Carbon::now()->format('ymd');
                }else{
                    $invoice_number = $request->invoice_number;
                }
                $order_data = [
                    'user_id'=>$user_id,
                    'description'=>$request->description,
                    'invoice_number'=>$invoice_number,
                    'supplier_id'=>$request->supplier_id,
                    'shipment_time'=>$request->shipment_time,
                    'billing_id'=>$billing_id
                ];
                $order_id = DreamOrder::create($order_data)->id;
                if (is_null($request->invoice_number)){
                    DreamOrder::find($order_id)->update(['invoice_number'=>$request->supplier_id.Carbon::now()->format('md').$order_id]);
                }
                $transaction = [
                    'user_id' => Auth::user()->id,
                    'description' => $request->description
                ];
                $transaction_id = DreamTransaction::create($transaction)->id;
                $output_from_storage = [
                    'transaction_id'=>$transaction_id,
                    'storage_id'=>$request->supplier_id,
                    'operation_id'=>self::INPUT
                ];
                DB::table('dream_transaction_storage_bind')->insert($output_from_storage);
                if (!empty($items)){
                    foreach ($items->data as $item){
                        if ((float)$item->amount>0 && !is_null($item->amount)){

                            $list_data = [
                                'order_id'=>$order_id,
                                'item_id'=>$item->item_id,
                                'unit_id'=>$item->unit_id,
                                'amount'=>(float)$item->amount,
                                'price_per_unit'=>(float)$item->price_per_unit,
                                'gross_weight' => (float)$item->weight_kg,
                            ];
                            $order_list_id = DB::table('dream_order_list')->insertGetId($list_data);

                            $data = [
                                'transaction_id' => $transaction_id,
                                'item_id' => $item->item_id,
                                'unit_id' => $item->unit_id,
                                'amount' => $item->amount
                            ];
                            $tr_list_id = DB::table('dream_transaction_list')->insertGetId($data);

                            DB::table('transaction_order_bind')->insert(['order_list_id'=>$order_list_id, 'transaction_list_id'=>$tr_list_id, 'logistics_list_id'=>null]);
                        }
                    }
                }

                return $items;
            });
            return json_encode($res);

        }catch(\Exception $e){
            return \response()->json(['errors'=>array($e)], 500);
        }
    }

    public function editOrder(Request $request){
        try{
            $order = DB::table('dream_orders')
                ->where('dream_orders.id', $request->order_id)->select('dream_orders.*')
                ->get()->first();
            return json_encode($order);
        }catch (\Exception $e){
            return \response()->json(['errors'=>array($e)], 500);
        }
    }

    public function editOrderList(Request $request){
        try{
            $order = DB::table('dream_order_list')
                ->where('dream_order_list.id', $request->order_list_id)->get()->first();
            return json_encode($order);
        }catch (\Exception $e){

            return \response()->json(['errors'=>array($e)], 500);
        }
    }

    public function updateOrder(Request $request){
        if (Gate::denies('save', new DreamOrder)){
            return \response()->json(['errors'=>array('нет доступа!')], 500);
        }

        $validateNames = array(
            'edit_invoice_number'=>'invoice номер'
        );
        $validator = Validator::make($request->all(), [
            'edit_invoice_number' => 'required',
        ]);
        $validator->setAttributeNames($validateNames);
        if ($validator->fails()) {
            return response()->json(['errors'=>array($validator->errors()->all())], 500);
        }

        try{
            $res = DB::transaction(function () use ($request){
                $user_id = Auth::user()->id;
                $data = [
                    'invoice_number'=>$request->edit_invoice_number,
                    'description'=>$request->description,
                    'shipment_time'=>$request->shipment_time
                ];
                $order = DreamOrder::find($request->order_id);
                if ($order->user_id==$user_id){
                    DreamOrder::find($request->order_id)->update($data);
//                    $billing_data = [
//                        'deposit'=>$request->deposit,
//                        'due_time'=>$request->due_time,
//                        'before_shipment'=>$request->before_shipment,
//                        'currency_id'=>$request->currency,
//                    ];
//                    $billing = DB::table('dream_billings')->where('id', $order->billing_id)->update($billing_data);
                }else{
                    return \response()->json(['errors'=>array('вы не можете изменить!')], 500);
                }
                return $order;
            });
            return $res;
        }catch(\Exception $e){
            return \response()->json(['errors'=>array('не возможно изменить!')], 500);
        }
    }

    public function check_is_correct($items){
        foreach ($items as $item){
            if ($item->amount>0){
                return true;
            }
        }
        return false;
    }

    public function save_brought_items(Request $request){
        if (Gate::denies('allow_input_output_operation', new DreamTransaction)){
            return response()->json(array('error'=>true, 'data'=>'нет доступа!'), 200);
        }

        try{
            $response = DB::transaction(function () use($request){
                $storage_id = $request->storage_id;
                $description = $request->order_description;
                $arr = array();
                $items = json_decode($request->items, true);
                foreach ($items as $key=>$value){
                    $logistics_list = DB::table('logistics_list')
                        ->join('logistics', 'logistics.id', '=', 'logistics_list.logistics_id')
                        ->join('dream_order_list', 'dream_order_list.id', '=', 'logistics_list.order_list_id')
                        ->select('logistics.from_storage_id', 'logistics_list.order_list_id', 'dream_order_list.item_id', 'dream_order_list.unit_id')
                        ->where('logistics_list.id', $key)->get()->first();
                    if (is_null($logistics_list)){
                        return response()->json(array('error'=>true, 'data'=>'пустие данные!'), 200);
                    }
                    $data = [
                        'logistics_list_id'=>$key,
                        'amount'=>$value,
                        'order_list_id'=>$logistics_list->order_list_id,
                        'item_id'=>$logistics_list->item_id,
                        'unit_id'=>$logistics_list->unit_id
                    ];
                    $arr[$logistics_list->from_storage_id][] = $data;
                }
                foreach ($arr as $from_storage_id=>$lists){
                    $transaction = [
                        'user_id' => Auth::user()->id,
                        'description' => $description
                    ];
                    $transaction_id = DreamTransaction::create($transaction)->id;

                    $input_to_storage = [
                        'transaction_id'=>$transaction_id,
                        'storage_id'=>$storage_id,
                        'operation_id'=>self::INPUT
                    ];

                    DB::table('dream_transaction_storage_bind')->insert($input_to_storage);

//                    $output_from_storage = [
//                        'transaction_id'=>$transaction_id,
//                        'storage_id'=>$from_storage_id,
//                        'operation_id'=>self::OUTPUT
//                    ];
//
//                    DB::table('dream_transaction_storage_bind')->insert($output_from_storage);

                    foreach ($lists as $list){
                        if ($list['amount']>0){
                            $transaction_list_data = [
                                'item_id' => $list['item_id'],
                                'transaction_id' => $transaction_id,
                                'amount' => $list['amount'],
                                'unit_id' => $list['unit_id']
                            ];
                            $tr_list_id = DB::table('dream_transaction_list')->insertGetId($transaction_list_data);

                            $order_transaction = [
                                'transaction_list_id'=>$tr_list_id,
                                'order_list_id'=>$list['order_list_id'],
                                'logistics_list_id'=>$list['logistics_list_id']
                            ];

                            DB::table('transaction_order_bind')->insert($order_transaction);

                        }
                    }
                }

                return $arr;
            });
//            session()->flash('input_fields');

            return json_encode($response);
        }catch (\Exception $e){
            return response()->json(['errors'=>array('невозможно добавить', $e)], 500);
        }
    }

    public function outputFromStorage($transaction_id, $from_storage_id){
        $hasTransactionStorageBind = DB::table('dream_transaction_storage_bind')->where([['transaction_id', $transaction_id], ['storage_id', $from_storage_id], ['operation_id', self::OUTPUT]])->get();
        if (count($hasTransactionStorageBind)==0){
            DB::table('dream_transaction_storage_bind')->insert(['transaction_id'=>$transaction_id, 'storage_id'=>$from_storage_id, 'operation_id'=>self::OUTPUT]);
        }
    }

    public function updateOrderList(Request $request){
        $allow = true;
        if (Gate::denies('delete', new DreamOrder)){
            $allow = false;
        }
//        $participateInTransaction = DB::table('transaction_order_bind')->where([['order_list_id', $request->order_list_id], ['logistics_list_id', null]])->get()->first();
//        if (!is_null($participateInTransaction)){
//            return \response()->json(['errors'=>array('участвует в транзакции!')], 500);
//        }
        $list_id = $request->order_list_id;
        $amount = $request->amount;
        $price = $request->price;
        try{
            if ($amount!=0){
                $order_id = DB::table('dream_order_list')->where('id', $list_id)->first()->order_id;
                $order = DreamOrder::find($order_id);
                if ($allow && $order->state_id==self::STATE_ACTIVE){
                    if (is_null($request->price)){
                        DB::table('dream_order_list')->where('id', $list_id)->update(['amount'=>$amount]);
                    }else{
                        DB::table('dream_order_list')->where('id', $list_id)->update(['amount'=>$amount, 'price_per_unit'=>$price]);
                    }
                    $tr_list_id = DB::table('transaction_order_bind')->where([['order_list_id', $list_id]])->whereNull('transaction_order_bind.logistics_list_id')->first();
                    if (!is_null($tr_list_id)){
                        DB::table('dream_transaction_list')->where('id', $tr_list_id->transaction_list_id)->update(['amount'=>$amount]);
                    }
                }else{
                    return \response()->json(['errors'=>array('вы неможете обнавить!')], 500);
                }
            }else{
                return \response()->json(['errors'=>array('недопустимое значение!!')], 500);
            }
            return json_encode($order_id);
        }catch (\Exception $exception){
            return \response()->json(['errors'=>array('неможете обнавить!', $exception)], 500);
        }
    }

    public function delete_order_list_item(Request $request){
        $allow = true;
        if (Gate::denies('delete', new DreamOrder)){
            $allow = false;
        }
        //check into logistics
        $role_id = DB::table('user_role')
            ->where('user_role.user_id', Auth::user()->id)->first()->role_id;
        $list_id = $request->order_list_id;
        try{
            $order_id = DB::table('dream_order_list')->where('id', $list_id)->first()->order_id;
            if ($allow){
                $tr_binds = DB::table('transaction_order_bind')->where([['transaction_order_bind.order_list_id', $list_id]])->get();
                foreach ($tr_binds as $bind){
                    DB::table('dream_transaction_list')->where('id', $bind->transaction_list_id)->delete();
                    if (!is_null($bind->logistics_list_id)){
                        DB::table('logistics_list')->where('id', $bind->logistics_list_id)->delete();
                    }
                }
                DB::table('dream_order_list')->where('id', $list_id)->delete();
            }else{
                return \response()->json(['errors'=>array('вы неможете удалить!')], 500);
            }
            return json_encode($order_id);
        }catch (\Exception $exception){
            return \response()->json(['errors'=>array('вы неможете удалить!', $exception)], 500);
        }
    }

    public function delete_order(Request $request){
        if (Gate::denies('delete', new DreamOrder)){
            return \response()->json(['errors'=>array('нет доступа!')], 500);
        }

        try{
            $tr_item_count = DB::table('logistics_list')->join('dream_order_list', 'dream_order_list.id', '=', 'logistics_list.order_list_id')
                ->where('dream_order_list.order_id', $request->order_id)->count();
            if ($tr_item_count>0){
                return \response()->json(['errors'=>array('невозможно удалить, участвует в логистики!')], 500);
            }
            else{
                $order = DreamOrder::find($request->order_id);

                if ($order->user_id==Auth::user()->id){
                    DreamOrder::find($request->order_id)->update(['delete_time'=>Carbon::now()->format('Y-m-d'), 'invoice_number'=>$order->invoice_number.'-notActual']);
                    $items = DB::table('dream_order_list')
                        ->join('transaction_order_bind', 'transaction_order_bind.order_list_id', '=', 'dream_order_list.id')
                        ->select('transaction_order_bind.*')->where('dream_order_list.order_id', $request->order_id)->get();
                    foreach ($items as $item){
                        DB::table('dream_transaction_list')->where('dream_transaction_list.id', $item->transaction_list_id)->update(['delete_time'=>Carbon::now()->format('Y-m-d')]);
                    }
                }else{
                    return \response()->json(['errors'=>array('вы неможете удалить!')], 500);
                }
            }
            return json_encode($tr_item_count);
        }catch (\Exception $exception){
            return \response()->json(['errors'=>array('вы неможете удалить!')], 500);
        }
    }

    public function close_order(Request $request){
        if (Gate::denies('save', new DreamOrder)){
            return \response()->json(['errors'=>array('нет доступа!')], 500);
        }
        $data = [
            'state_id'=>self::STATE_CLOSED
        ];
        $author_id = DreamOrder::find($request->order_id)->user_id;
        if ($author_id==Auth::user()->id){
            DB::table('dream_orders')->where('id', $request->order_id)->update($data);
        }else{
            return \response()->json(['errors'=>array('вы неможете удалить!')], 500);
        }
        return json_encode($author_id);
    }

    public function get_supplier_items(Request $request){
        $supplier_id = $request->supplier_id;
        $data = array();
        $items = DB::table('dream_storage_item_bind')
            ->join('dream_items', 'dream_items.id', '=', 'dream_storage_item_bind.item_id')
            ->leftJoin('dream_items_data', 'dream_items_data.item_id', '=', 'dream_storage_item_bind.item_id')
            ->join('dream_units', 'dream_units.id', '=', 'dream_storage_item_bind.unit_id')
            ->select('dream_items.*', 'dream_units.unit as unit_name', 'dream_units.id as unit_id', 'dream_items_data.name as english_name')
            ->where('dream_storage_item_bind.storage_id', $supplier_id)->get();

        foreach ($items as $key=>$item){
            $price_per_unit = $this->isShownBefore($item->id, $supplier_id);;
            $nestedData['DT_RowId'] = $item->id;
            $nested['error'] = 0;
            $nested['item_id'] = $item->id;
            $nested['articula_new'] = $item->articula_new;
            $nested['english_name'] = $item->english_name;
            $nested['item_name'] = $item->item_name;
            $nested['sap_code'] = $item->sap_code;
            $nested['unit'] = $item->unit_name;
            $nested['unit_id'] = $item->unit_id;
            $nested['amount'] = 0;
            $nested['weight_kg'] = 0;
            $nested['weight'] = "<input required type='number' width='100' name='item_wieght[]' class='form-control' onkeyup=\"save_item_weight(this)\" style='height: 26px; width: 100px' placeholder='0'>";
            $nested['amount_input'] = "<input type='number' width='100' name='item_amount[]' value='' class='form-control' onkeyup=\"save_item_amount(this)\" style='height: 26px; width: 100px' placeholder='0'>";
            $nested['price_per_unit'] = $price_per_unit;
            $nested['total_price'] = $price_per_unit*0;
            $nested['price_per_unit_input'] = "<input type='number' width='100' name='item_price[]' value='$price_per_unit' class='form-control' onkeyup=\"save_price_value(this)\" onfocus=\"this.select()\" style='height: 26px; width: 100px' placeholder='0'>";;
            $nested['delete_btn'] = "<button type='button' class='btn btn-danger' onclick=\"delete_item(this)\"><i class='fa fa-trash'></i></button>";
            $data[] = $nested;
        }

        $json_data = array(
            "data" => $data,
            "lastBilling" => $this->billingWasSetBefore($supplier_id)
        );

        return json_encode($json_data);
    }

    public function get_supplier_items_by_model(Request $request){
        $supplier_id = $request->supplier_id;
        $model_id = $request->model_id;
        $model_amount = $request->model_amount;
        $validateNames = array(
            'model_amount'=>'кол-во'
        );
        $validator = Validator::make($request->all(), [
            'model_amount' => 'required'
        ]);
        $validator->setAttributeNames($validateNames);
        if ($validator->fails()) {
            return response()->json(['errors'=>array($validator->errors()->all())], 500);
        }

        try{
            $data = array();
            $items = DB::table('dream_item_bind')
                ->leftJoin('dream_storage_item_bind', 'dream_storage_item_bind.item_id', '=', 'dream_item_bind.child_id')
                ->join('dream_items', 'dream_items.id', '=', 'dream_storage_item_bind.item_id')
                ->leftJoin('dream_items_data', 'dream_items_data.item_id', '=', 'dream_storage_item_bind.item_id')
                ->leftJoin('dream_units', 'dream_units.id', '=', 'dream_storage_item_bind.unit_id')
                ->select('dream_items.*', 'dream_units.unit as unit_name', 'dream_units.id as unit_id', 'dream_items_data.name as english_name', 'dream_item_bind.quantity', 'dream_storage_item_bind.item_id')
                ->where([['dream_item_bind.parent_id', $model_id]])->groupBy('dream_items.id', 'dream_units.id', 'dream_items_data.id', 'dream_item_bind.quantity')->get();
            foreach ($items as $key=>$item){
                $item_amount = $item->quantity*$model_amount;
                $price_per_unit = $this->isShownBefore($item->id, $supplier_id);
                $nestedData['DT_RowId'] = $item->id;
                if (is_null($item->item_id)){
                    $nested['error'] = 1;
                }else{
                    $nested['error'] = 0;
                }
                $nested['item_id'] = $item->id;
                $nested['articula_new'] = $item->articula_new;
                $nested['english_name'] = $item->english_name;
                $nested['item_name'] = $item->item_name;
                $nested['sap_code'] = $item->sap_code;
                $nested['unit'] = $item->unit_name;
                $nested['unit_id'] = $item->unit_id;
                $nested['amount'] = $item_amount;
                $nested['amount_input'] = "<input type='number' width='100' name='item_amount[]' value='$item_amount' class='form-control' onkeyup=\"save_item_amount(this)\" style='height: 26px; width: 100px' placeholder='0'>";
                $nested['price_per_unit'] = $price_per_unit;
                $nested['weight_kg'] = 0;
                $nested['weight'] = "<input required type='number' width='100' name='item_wieght[]' class='form-control' onkeyup=\"save_item_weight(this)\" style='height: 26px; width: 100px' placeholder='0'>";
                $nested['total_price'] = $price_per_unit*$item_amount;
                $nested['price_per_unit_input'] = "<input type='number' width='100' name='item_price[]' value='$price_per_unit' class='form-control' onkeyup=\"save_price_value(this)\" onfocus=\"this.select()\" style='height: 26px; width: 100px' placeholder='0'>";;
                $nested['delete_btn'] = "<button type='button' class='btn btn-danger' onclick=\"delete_item(this)\"><i class='fa fa-trash'></i></button>";
                $data[] = $nested;
            }
            $json_data = array(
                "data" => $data,
                "lastBilling" => $this->billingWasSetBefore($supplier_id)
            );

            return json_encode($json_data);
        }catch (\Exception $exception){
            dd($exception);
            return response()->json(['errors'=>array($exception)], 500);
        }
    }

    public function getSupplierModels(Request $request){
        $models = DB::table('dream_storage_item_bind')
            ->join('dream_item_bind', 'dream_item_bind.parent_id', '=', 'dream_storage_item_bind.item_id')
            ->join('dream_items', 'dream_items.id', '=', 'dream_item_bind.parent_id')
            ->select('dream_items.*')->where('dream_storage_item_bind.storage_id', $request->supplier_id)->groupBy('dream_items.id')->get();
        return json_encode($models);
    }

    public function isShownBefore($item_id, $supplier_id){
        $item = DB::table('dream_orders')
            ->join('dream_order_list', 'dream_order_list.order_id', '=', 'dream_orders.id')
            ->where([['dream_orders.supplier_id', $supplier_id], ['dream_order_list.item_id', $item_id]])
            ->select('dream_orders.id', 'dream_orders.supplier_id', 'dream_order_list.price_per_unit')
            ->orderBy('dream_orders.id', 'desc')->limit(1)->get();

        return empty($item)?0:(isset($item->first()->price_per_unit)?$item->first()->price_per_unit:0);
    }

    public function billingWasSetBefore($supplier_id){
        if ($supplier_id!=0){
            $lastBilling = DB::table('dream_orders')
                ->join('dream_billings', 'dream_billings.id', '=', 'dream_orders.billing_id')
                ->select('dream_billings.deposit', 'dream_billings.due_time', 'dream_billings.before_shipment', 'dream_billings.currency_id')
                ->where('dream_orders.supplier_id', $supplier_id)->orderBy('dream_orders.id', 'desc')->limit(1)->get();
            return empty($lastBilling)?null:$lastBilling->first();
        }else{
            return null;
        }
    }

    public function getNotConsistedItems(Request $request){
        $order_id = $request->order_id;
        $supplier_id = DreamOrder::find($order_id)->supplier_id;
        $items = DB::select("
            select dream_items.* from dream_storage_item_bind
            inner join dream_items on dream_items.id=dream_storage_item_bind.item_id
            where dream_storage_item_bind.item_id not in (select dream_order_list.item_id from dream_order_list where dream_order_list.order_id=$order_id)
            and dream_storage_item_bind.storage_id=$supplier_id
            group by dream_items.id
        ");
        return json_encode($items);
    }

    public function upload_order_file(Request $request){
        try{
            $data = array();
            $validateNames = array(
                'file_supplier_id'=>'поставщик'
            );
            $validator = Validator::make($request->all(), [
                'file_supplier_id' => 'required'
            ]);
            $validator->setAttributeNames($validateNames);
            if ($validator->fails()) {
                return response()->json(['errors'=>array($validator->errors()->all())], 500);
            }
            $supplier_id = $request->file_supplier_id;

            if($request->hasFile('orderFile'))
            {
                $path = $request->file('orderFile')->getRealPath();
                //dd($path);
                $arr = \Excel::load($path)->get();
                //dd($arr);
                foreach ($arr as $key=>$value){
                    if ($value->articula_new!=null && $value->amount!=null && is_numeric($value->amount) ){
                        $item = DB::table('dream_items')
                            ->join('dream_storage_item_bind', 'dream_storage_item_bind.item_id', '=', 'dream_items.id')
                            ->join('dream_units', 'dream_units.id', '=', 'dream_storage_item_bind.unit_id')
                            ->leftJoin('dream_items_data', 'dream_items_data.item_id', '=', 'dream_items.id')
                            ->select('dream_items.*', 'dream_units.unit as unit_name', 'dream_units.id as unit_id', 'dream_items_data.name as english_name')
                            ->where([['dream_items.articula_new', '=', trim($value->articula_new)], ['dream_storage_item_bind.storage_id', '=', $supplier_id]])->get()->first();

                        if (is_null($item)){
                            $nestedData['DT_RowId'] = 'row'.$key;
                            if ($this->hasDuplicate($value->articula_new, $arr)) {
                                $nested['error'] = 2;
                            }else {
                                $nested['error'] = 1;
                            }
                            $nested['item_id'] = null;
                            $nested['articula_new'] = $value->articula_new;
                            $nested['english_name'] = null;
                            $nested['item_name'] = null;
                            $nested['sap_code'] = null;
                            $nested['unit'] = null;
                            $nested['unit_id'] = null;
                            $nested['amount'] = null;
                            $nested['weight']=null;
                            $nested['weight_kg']=null;

                            $nested['amount_input'] = "";
                            $nested['price_per_unit'] = null;
                            $nested['price_per_unit_input'] = "";
                            $nested['total_price'] = "";
                            $nested['delete_btn'] = "<button type='button' class='btn btn-danger' onclick=\"delete_item(this)\"><i class='fa fa-trash'></i></button>";
                        }else{
                            $price_per_unit = (empty($value->price_per_unit))?$this->isShownBefore($item->id, $supplier_id):$value->price_per_unit;
                            $nestedData['DT_RowId'] = $item->id;
                            if ($this->hasDuplicate($value->articula_new, $arr)) {
                                $nested['error'] = 2;
                            }else {
                                if ($item->unit_name!=$value->unit){
                                    $nested['error'] = 3;
                                }else{
                                    $nested['error'] = 0;
                                }
                            }
                            $nested['item_id'] = $item->id;
                            $nested['articula_new'] = $item->articula_new;
                            $nested['english_name'] = $item->english_name;
                            $nested['item_name'] = $item->item_name;
                            $nested['sap_code'] = $item->sap_code;
                            $nested['unit'] = $item->unit_name;
                            $nested['unit_id'] = $item->unit_id;
                            $nested['amount'] = $value->amount;
                            $nested['amount_input'] = "<input type='number' width='100' name='item_amount[]' value='$value->amount' class='form-control' onkeyup=\"save_item_amount(this)\" style='height: 26px; width: 100px' placeholder='0'>";
                            $nested['price_per_unit'] = $price_per_unit;
                            $nested['weight'] = "<input type='number' width='100' name='item_weight[]' value='$value->gross_weight' class='form-control' onkeyup=\"save_item_weight(this)\" style='height: 26px; width: 100px' placeholder='0'>";
                            $nested['weight_kg'] = $value->gross_weight;
                            $nested['total_price'] = $price_per_unit*$value->amount;
                            $nested['price_per_unit_input'] = "<input type='number' width='100' name='item_price[]' value='$price_per_unit' class='form-control' onkeyup=\"save_price_value(this)\" onfocus=\"this.select()\" style='height: 26px; width: 100px' placeholder='0'>";;
                            $nested['delete_btn'] = "<button type='button' class='btn btn-danger' onclick=\"delete_item(this)\"><i class='fa fa-trash'></i></button>";
                        }
                        $data[] = $nested;

                    }
                }
            }

            $json_data = array(
                "data" => $data,
                "lastBilling" => $this->billingWasSetBefore($supplier_id)
            );

            return json_encode($json_data);
        }catch(\Exception $e){
            return \response()->json(['errors'=>array($e)], 500);
        }
    }

    public function hasDuplicate($articula, $array){
        $count=0;
        foreach ($array as $item){
            if ($articula==$item->articula_new){
                $count++;
            }
        }
        if ($count>=2){
            return true;
        }else{
            return false;
        }
    }

    public function getOrderPrice(Request $request){
        $sup_items = DB::table('dream_storage_item_bind')
            ->join('dream_items', 'dream_items.id', '=', 'dream_storage_item_bind.item_id')
            ->join('dream_units', 'dream_units.id', '=', 'dream_storage_item_bind.unit_id')
            ->join('dream_storages', 'dream_storages.id', '=', 'dream_storage_item_bind.storage_id')
            ->select('dream_items.*', 'dream_units.unit as unit_name', 'dream_storages.name as storage_name', 'dream_storage_item_bind.item_id', 'dream_storage_item_bind.storage_id',
                'dream_storage_item_bind.unit_id')
            ->where([['dream_storages.type_id', 7]])->groupBy('dream_storage_item_bind.item_id', 'dream_storage_item_bind.unit_id', 'dream_storage_item_bind.storage_id')->get();
        $data = array();
        foreach ($sup_items as $item){
            $maxId = DB::table('dream_order_list')
                ->join('dream_orders', 'dream_orders.id', '=', 'dream_order_list.order_id')
                ->where([['dream_order_list.item_id', $item->item_id], ['dream_orders.supplier_id', $item->storage_id]])->max('dream_order_list.id');
            $lastPrice = DB::table('dream_order_list')->where('id', $maxId)->first();
            $nested['item_id'] = $item->id;
            $nested['articula_new'] = $item->articula_new;
            $nested['item_name'] = $item->item_name;
            $nested['sap_code'] = $item->sap_code;
            $nested['unit'] = $item->unit_name;
            $nested['unit_id'] = $item->unit_id;
            $nested['last_price'] = (!isset($lastPrice->price_per_unit))?0:$lastPrice->price_per_unit;
            $data[] = $nested;
        }
        dd($data);

        $json_data = array(
            "data" => $data
        );

        return json_encode($json_data);
    }

    public function historyOfOrder(Request $request){
        try{
            $columns = array(
                0 => 'articula_new',
                1 => 'sap_code',
                2 => 'item_name',
                3 => 'supplier',
                4 => 'supplier_type',
                5 => 'shipment_time',
                6 => 'delivery_time',
                7 => 'input_date',
                8 => 'order_invoice',
                9 => 'log_invoice',
                10 => 'container_number',
                11 => 'price_per_unit',
                12 => 'currency',
                13 => 'order_amount',
                14 => 'log_amount',
                15 => 'way_amount',
                16 => 'input_sum',
                17 => 'inputed',
                18 => 'sup_remaining',
                19 => 'unit',
                20 => 'to_storage'
            );

            $filterColumns = array(
                'filter_by_from_storage' => 'o.supplier_id',
                'filter_by_to_storage' => 'to_str.id',
                5 => 'invoice_number',
                6 => 'order_invoice_number',
                7 => 'container_number',
                8 => 'container_type_name',
                9 => 'create_time',
                'filter_by_user' => 'o.user_id',
                11 => 'delivery_time'
            );

            $totalData = DB::table('logistics')->get()->count();
            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');
            $filterArr = json_decode($request->filterArr);
            $filter_str = '';
            if (!is_null($filterArr)){
                foreach ($filterArr as $key=>$values){
                    if (!empty($values)){
                        $filter_str.=" and $filterColumns[$key] in (";
                        foreach ($values as $value){
                            $filter_str.=$value.',';
                        }
                        $filter_str = substr($filter_str, 0, strlen($filter_str)-1).')';
                    }
                }
            }

            if (Gate::denies('viewOrderPrice', new DreamOrder)){
                $x = "null as currency, null as price_per_unit";
            }else{
                $x = "dc.name as currency, ol.price_per_unit";
            }

            if ($request->upload_start_date && $request->upload_end_date){
                $filter_str.=" and delivery_time between date('$request->upload_start_date') and date('$request->upload_end_date')";
            }

            if ($request->input_start_date && $request->input_end_date){
                $filter_str.=" and inp.create_time between date('$request->input_start_date') and date('$request->input_end_date')";
            }

            if ($request->order_start_date && $request->order_end_date){
                $filter_str.=" and o.shipment_time between date('$request->order_start_date') and date('$request->order_end_date')";
            }
            if ($request->filter_by_type){
                $filter_str.=" and  si.type_id = ".$request->filter_by_type;
            }

            $query_text = "
                select
                SQL_CALC_FOUND_ROWS inp.id,
                di.articula_new, di.sap_code, concat('<p title=\"', di.item_name, '\">', di.item_name, '</p>') as item_name, du.unit, sup.name as supplier, o.invoice_number as order_invoice, l.invoice_number as log_invoice,
                $x, ol.amount as order_amount, ll.amount as log_amount, ROUND((IFNULL(logs.total, 0) - IFNULL(inp1.inputed, 0)), 3) as way_amount, IFNULL(ROUND(inp1.inputed, 3), 0) as input_sum,
                IFNULL(ROUND(inp.amount, 3), 0) as inputed, IFNULL(ROUND((ol.amount - logs.total), 3), 0) as sup_remaining, substring(inp.create_time, 1, 10) as input_date, substring(l.delivery_time, 1, 10) as delivery_time,
                substring(l.create_time, 1, 10) as log_created, substring(o.create_time, 1, 10) as order_created, to_str.name as to_storage, substring(o.shipment_time, 1, 10) as shipment_time, l.container_number, l.state_id,
                dst.name as supplier_type
                from dream_orders as o
                inner join dream_storages sup on sup.id = o.supplier_id
                left join storage_info si on si.storage_id = sup.id
                left join dream_supplier_types dst on dst.id=si.type_id
                inner join dream_order_list ol on ol.order_id = o.id
                inner join dream_billings db on db.id = o.billing_id
                inner join dream_currencies dc on dc.id = db.currency_id
                inner join dream_items di on di.id = ol.item_id
                inner join dream_units du on du.id = ol.unit_id
                left join logistics_list ll on ll.order_list_id = ol.id
                left join logistics l on l.id = ll.logistics_id
                left join dream_storages to_str on to_str.id = l.to_storage_id
                left join (
                    select tob.logistics_list_id, dtl.amount, dt.create_time, dtl.id from transaction_order_bind tob
                    inner join dream_transaction_list dtl on dtl.id = tob.transaction_list_id
                    inner join dream_transaction_storage_bind dtsb on dtsb.transaction_id = dtl.transaction_id
                    inner join dream_transactions dt on dt.id = dtl.transaction_id
                    inner join dream_storages ds on ds.id = dtsb.storage_id
                    where dtsb.operation_id = 1 and ds.type_id = 1
                ) as inp on inp.logistics_list_id = ll.id
                left join (
                    select tob.order_list_id, sum(dtl.amount) as inputed from transaction_order_bind tob
                    inner join dream_transaction_list dtl on dtl.id = tob.transaction_list_id
                    inner join dream_transaction_storage_bind dtsb on dtsb.transaction_id = dtl.transaction_id
                    inner join dream_transactions dt on dt.id = dtl.transaction_id
                    inner join dream_storages ds on ds.id = dtsb.storage_id
                    where dtsb.operation_id = 1 and ds.type_id = 1
                    group by tob.order_list_id
                ) as inp1 on inp1.order_list_id = ol.id
                left join (
                    select logistics_list.order_list_id, sum(logistics_list.amount) as total from logistics_list
                    group by logistics_list.order_list_id
                ) as logs on logs.order_list_id = ol.id
                where date(o.create_time)>date('2020-02-04')
            ";

            $search_text = '';
            if (!empty($request->input('search.value'))){
                $search = $request->input('search.value');
                $search_text .= "
                     and ( di.articula_new like '%$search%' || di.item_name like '%$search%' || di.sap_code like '%$search%' || o.invoice_number like '%$search%' || l.invoice_number like '%$search%'
                     || l.delivery_time like '%$search%' || sup.name like '%$search%' || l.container_number like '%$search%')
                    ";
            }

            $query_text.=$search_text.$filter_str." order by $order $dir limit $limit offset $start;";
            $items = DB::select($query_text);
            $totalCount = DB::select('select found_rows() as total_count')[0];

            $data = [];
            foreach ($items as $item){
                if ($item->inputed == 0 && $item->state_id == 2){
                    continue;
                }
                $data[] = $item;
            }
            $json_data = array(
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => intval($totalData),
                "recordsFiltered" => intval($totalCount->total_count),
                "data"            => $data
            );

            return json_encode($json_data);
        }catch (\Exception $exception){
            dd($exception);
            return \response()->json(['errors'=>array($exception)], 500);
        }
    }
}

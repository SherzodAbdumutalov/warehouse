<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DreamLogistics extends Model
{
    protected $table = 'dream_logistics';
    protected $fillable = ['invoice_number', 'truck_number', 'user_id', 'delivery_time', 'description'];
    public $timestamps = false;
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DreamItem extends Model
{
    //

    protected $table = 'dream_items';
    protected $fillable = ['articula_new', 'articula_old', 'sap_code', 'item_name', 'item_model',];

    public $timestamps = false;

    public function dream_unit(){
        return $this->belongsToMany("App\DreamUnit", 'dream_item_unit_bind', 'item_id', 'unit_id');
    }

    public function checkForCycle($parent_id, $id){

        if ($parent_id==$id){
            return true;
        }

        $parents = DB::table("dream_item_bind")->where("child_id", $parent_id)->get();

        foreach ($parents as $parent){

            if ($this->checkForCycle($parent->parent_id, $id)){
                return true;
            }

        }

        return false;

    }

    public function transactions(){
        return $this->belongsToMany('App\DreamTransaction', 'dream_transaction_list', 'transaction_id', 'item_id');
    }

    public function storage(){
        return $this->belongsToMany('App\DreamStorage', 'dream_storage_item_bind', 'storage_id', 'item_id');
    }

    public function dream_plan(){
        return $this->belongsTo('App\DreamMonthlyPlan', 'item_id');
    }

    public static function insertIgnore($array){
        $a = new static();
        if($a->timestamps){
            $now = \Carbon\Carbon::now();
            $array['created_at'] = $now;
            $array['updated_at'] = $now;
        }
        DB::insert('INSERT IGNORE INTO '.$a->table.' ('.implode(',',array_keys($array)).
            ') values (?'.str_repeat(',?',count($array) - 1).')',array_values($array));
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DreamStorage extends Model
{
    protected $table = 'dream_storages';
    protected $fillable = ['name', 'type_id', 'state'];
    public $timestamps = false;
    public function transactions(){
        return $this->hasMany('App\DreamTransaction', 'storage_id');
    }

    public function users(){
        return $this->belongsToMany('App\User', 'dream_storage_user_bind', 'user_id', 'storage_id');
    }

    public function item(){
        return $this->belongsToMany('App\DreamItem', 'dream_storage_item_bind', 'storage_id', 'item_id');
    }

    public function transaction_requests(){
        return $this->hasMany('App\DreamRequest', 'storage_id');
    }
}

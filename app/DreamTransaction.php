<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DreamTransaction extends Model
{
    protected $table = 'dream_transactions';
    protected $fillable = ['storage_id', 'operation_id', 'user_id', 'create_time', 'update_time', 'description', 'is_inventory'];
    public $timestamps = false;

    public function items(){
        return $this->belongsToMany('App\DreamItem', 'dream_transaction_list', 'transaction_id', 'item_id');
    }

    public function storage(){
        return $this->belongsTo('App\DreamStorage', 'storage_id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }
}

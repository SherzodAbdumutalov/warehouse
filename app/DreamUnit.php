<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DreamUnit extends Model
{
    //

    protected $fillable = ['unit'];
    protected $table = 'dream_units';

    public function dream_item(){
        return $this->belongsToMany('App\DreamItem', 'dream_item_unit_bind', 'item_id', 'unit_id');
    }
}

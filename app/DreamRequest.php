<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DreamRequest extends Model
{
    //
    protected $table = 'dream_requests';
    protected $fillable = ['from_storage_id', 'to_storage_id', 'user_id', 'description', 'state', 'model_id', 'amount', 'type_id'];
    public $timestamps = false;

    public function storage(){
        return $this->belongsTo('App\DreamStorage', 'to_storage_id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function isBelongToManyStorage($articula_new, $all_items){
        $count = 0;
        foreach ($all_items as $item){
            if ($articula_new==$item['articula_new']){
                $count++;
            }
        }

        return $count;
    }
}

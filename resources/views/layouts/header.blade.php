{{--@section('additional_css')--}}
{{--    <style>--}}


{{--    </style>--}}
{{--@endsection--}}

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid" style="padding-bottom: 1px; padding-left: 0; padding-right: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li><a href="{{ route('home') }}"><i class="fa fa-home"></i> <b>Главная</b></a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-chain-broken"></i> <b>План </b>
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ route('show_plan') }}"><i class="fa fa-line-chart"></i> <b>Месячный план </b></a></li>
                        <li><a href="{{ route('show_daily_plan') }}"><i class="fa fa-line-chart"></i> <b>Дневной план</b></a></li>
                    </ul>
                </li>
                <li><a href="{{ route('showTransaction') }}"><i class="fa fa-history"></i> <b>История операций</b> </a></li>
                <li><a href="{{ route('show_remaining') }}"><i class="fas fa-boxes"></i> <b>Остатки</b></a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fas fa-warehouse"></i> <b>Инвентарь </b>
                        <sup class="badge badge-red bg-important isExistNotification"></sup>
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ route('storageInputOutputOperation') }}"><i class="fas fa-dolly-flatbed"></i> <b> Операции на складе</b>
                                <sup class="badge badge-red bg-important isExistNotification"></sup></a>
                        </li>
                        <li><a href="{{ route('lineInputOutputOperation') }}"><i class="fas fa-dolly"></i> <b>Операции на линии</b> </a></li>
{{--                        <li><a href="{{ route('movingBetweenStorages') }}"><i class="fas fa-arrows-alt"></i> Межскладские перемещение</a></li>--}}

                        <li class="dropdownlist_ali"><a href="{{ route('show_requests_page') }}"><i class="fas fa-clipboard-list"></i><b>Заявки</b> <span class="caret" ></span></a>
                            <div id="leveled_list_item"  ><a href="{{ route('request') }}"> <i class="fas fa-clipboard-list"></i> <b>Создать заявку</b></a></div>
                        </li>



                <li><a href="{{ route('showInsideInvoices') }}"><i class="fas fa-clipboard-check"></i> <b>Накладные</b></a></li>

                        <li class="dropdownlist_ali"><a href="{{ route('returns') }}"><i class="fas fa-backward"></i> <b>Возврат на склад</b></a>
                            <div id="leveled_list_item"  ><a href="{{ route('create_returns') }}"> <i class="fas fa-clipboard-list"></i> <b>Создать возврат</b></a></div>
                        </li>

                        <li><a href="{{ route('all_storage_items') }}"><i class="fas fa-tags"></i> <b>Распределение по складам</b></a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-chain-broken"></i> <b>Брак</b>
                        <sup class="badge badge-red bg-important notAcceptedDefectiveRequest"></sup>
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ route('defectiveInputOutputOperation') }}"><i class="fas fa-dolly-flatbed"></i>  <b>Операции на складе</b>
                                <sup class="badge badge-red bg-important notAcceptedDefectiveRequest"></sup></a></li>


                        <li  class="dropdownlist_ali"><a href="{{ route('defective') }}"><i class=" fas fa-file-alt"></i> <b>Акты</b></a>
                            <div id="leveled_list_item"  ><a href="{{ route('create_defective') }}"> <i class="fas fa-clipboard-list"></i> <b>Составление акта</b></a></div>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fas fa-truck-moving"></i> <b>Снабжение </b>
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">

                        <li class="dropdownlist_ali"><a href="{{ route('orders_page') }}"><i class="fa fa-list"></i> <b>Заказы</b> <span class="caret" ></span></a>
                            <div id="leveled_list_item"  ><a href="{{ route('create_order') }}"> <i class="fas fa-clipboard-list"></i> <b>Создать заказ</b></a></div>
                        </li>


                        {{--<li><a href="{{ route('inputToBuffer') }}"><i class="fas fa-truck-loading"></i> Приход в базу</a></li>--}}
                        {{--<li><a href="{{ route('create_order') }}"><i class="fa fa-plus"></i> Новый заказ</a></li>--}}
                        {{--<li><a href="{{ route('createLogistics') }}"><i class="fa fa-train"></i> Создание логистики</a></li>--}}


                        <li class="dropdownlist_ali"> <a href="{{ route('logistics') }}"><i class="fas fa-route"></i> <b>Логистика</b>  <span class="caret" ></span></a>
                            <div id="leveled_list_item"  ><a href="{{ route('createLogistics') }}"> <i class="fas fa-clipboard-list"></i> <b>Создать  логистика</b></a></div>
                        </li>

                        <li> <a href="{{ route('show_suppliers') }}"><i class="far fa-handshake"></i> <b>Поставщики</b></a></li>
                        <li><a href="{{ route('historyPage') }}"><i class="fas fa-history"></i> <b>История заказов</b></a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fas fa-tools"></i> <b>Тех. раздел</b>
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ route('items') }}"><i class="fas fa-sitemap"></i><b> Все детали и нормы расхода</b></a></li>
                        <li><a href="{{ route('normArchive') }}"><i class="fas fa-history"></i> <b>Изменения в норме и деталях</b></a></li>

                        {{--<li><a href="{{ route('show_production_line') }}">Сборочные линии</a></li>--}}
                    </ul>
                </li>
                <li><a href="{{ route('users') }}"><i class="fa fa-users"></i> <b>Пользователи</b></a></li>

            </ul>
            <ul class="nav navbar-nav navbar-right">
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>
                @else
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user"></i>
                            {{ \Auth::user()->firstname }}
                            <sup class="badge badge-red bg-important isExistNotification"></sup>
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ route('storageInputOutputOperation') }}?id=byRequest"><i class="fa fa-file"></i> <b>заявка</b> <sup class="badge badge-red bg-important notAcceptedRequestsAmount"></sup></a></li>
                            <li><a href="{{ route('storageInputOutputOperation') }}?id=byReturns"><i class="fa fa-file"></i> <b>возврат</b> <sup class="badge badge-red bg-important notAcceptedReturnRequestsAmount"></sup></a></li>
                            <li><a href="{{ route('storageInputOutputOperation') }}?id=byMovingRequest"><i class="fa fa-file"></i> <b>перемещение</b> <sup class="badge badge-red bg-important notAcceptedMovingRequestsAmount"></sup></a></li>
                            <li><a href="{{ route('storageInputOutputOperation') }}?id=byOrder"><i class="fa fa-file"></i> <b>заказ</b> <sup class="badge badge-red bg-important activeOrdersCount"></sup></a></li>
                            <li><a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <i class="fa fa-sign-out"></i> <b>выйти из системы</b>
                                </a>
                            </li>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                            <li>
                                <a href="{{ route('changePasswordPage') }}">
                                    <i class="fa fa-gear"></i> <b>настройка</b>
                                </a>
                            </li>
                        </ul>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>

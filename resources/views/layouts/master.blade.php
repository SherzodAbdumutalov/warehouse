<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap -->
    <link rel="shortcut icon" href="{{ asset('assets/favicon.ico') }}">
    <link rel="stylesheet" href="{{ asset('assets') }}/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/css/lib/chosen/chosen.min.css">
    <script src="{{ asset('assets') }}/js/jquery.min.js"></script>
    <script src="{{ asset('assets') }}/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="{{ asset('assets') }}/css/bootstrap-multiselect.css" type="text/css">
    <script type="text/javascript" src="{{ asset('assets') }}/js/bootstrap-multiselect.js"></script>

    {{--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" type="text/css">--}}
    <link rel="stylesheet" href="{{ asset('assets') }}/DataTables/DataTables-1.10.18/css/jquery.dataTables.min.css" type="text/css">
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" type="text/css">--}}
    <link rel="stylesheet" href="{{ asset('assets') }}/DataTables/Buttons-1.5.6/css/buttons.dataTables.min.css" type="text/css">
    {{--<link rel="stylesheet" href="https://cdn.datatables.net/scroller/2.0.0/css/scroller.dataTables.min.css" type="text/css">--}}
    <link rel="stylesheet" href="{{ asset('assets') }}/DataTables/Scroller-2.0.0/css/scroller.dataTables.min.css" type="text/css">
    {{--<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>--}}
    <link rel="stylesheet" href="{{ asset('assets') }}/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/css/style.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/css/jquery.multiselect.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/css/helper.css" type="text/css">
    {{--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">--}}
    <link rel="stylesheet" href="{{ asset('assets') }}/css/myStyle.css">

    {{--datepicker--}}
    {{--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>--}}
    @yield('additional_css')
    <style>
        .dropdown:hover .dropdown-menu {
            display: block;
        }
        #preloader {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: transparent;
            z-index: 99999999;
            width: 100%;
            height: auto;
        }
        .dropdownlist_ali:hover #leveled_list_item{
            display: block;
            position: absolute;
            border: 1px solid #999999;
            margin-left: 7px;
            right: 0;
            color: black;
            background-color: whitesmoke;
            z-index: 999;
            padding: 3px;
            transform: translate(100%, -100%);
        }
        #leveled_list_item{
            display: none;
        }

        #status {
            width: 250px;
            height: 250px;
            position: absolute;
            left: 50%;
            top: 50%;
            background-image: url('{{ asset('assets') }}/gif/Spinner.gif');
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            margin: -100px 0 0 -100px;
        }

        input::-webkit-outer-spin-button, input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        input {
            -moz-appearance: textfield;
        }

        #loading {
            background: url('{{ asset('assets') }}/gif/Spinner.gif') no-repeat center center;
            position: absolute;
            height: 100%;
            width: 100%;
            z-index: 999999;
        }

        input::-webkit-clear-button { display: none; }


        .dataTables_processing {
            top: 20%!important;
            height: 100px!important;
            z-index: 11000 !important;
        }
    </style>
    <script>
        function showImage(event, _this){
            var d = new Date();
            e = event || window.event;
            var myImg = document.querySelector("#imagePanel");
            var realWidth = myImg.naturalWidth;
            var realHeight = myImg.naturalHeight;
            var pageX = e.pageX;
            var pageY = e.pageY;
            if (pageX === undefined) {
                pageX = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
                pageY = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
            }
            $('#imagePanel').attr('src', '{{ asset('assets') }}/itemsPhoto/'+$(_this).data('id')+'.png?'+d.getTime());
            if ((pageY+realHeight)>=1000) {
                $('#imagePanel').css({top: (pageY-200), left:pageX+20, transition: 2});
            }else{
                $('#imagePanel').css({top: pageY, left:pageX+20, transition: 2});
            }
            $('#imagePanel').show();
        }

        function hideImage(event){
            $('#imagePanel').hide();
        }

        function showUserImage(event, _this){
            var d = new Date();
            e = event || window.event;
            var myImg = document.querySelector("#userImagePanel");
            var realWidth = myImg.naturalWidth;
            var realHeight = myImg.naturalHeight;
            var pageX = e.pageX;
            var pageY = e.pageY;
            if (pageX === undefined) {
                pageX = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
                pageY = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
            }
            $('#userImagePanel').attr('src', '{{ asset('assets') }}/usersPhoto/'+$(_this).data('id')+'.png?'+d.getTime());
            if ((pageY+realHeight)>=1000) {
                $('#userImagePanel').css({top: (pageY-200), left:pageX+20, transition: 2});
            }else{
                $('#userImagePanel').css({top: pageY, left:pageX+20, transition: 2});
            }
            $('#userImagePanel').show();
        }

        function hideUserImage(event){
            $('#userImagePanel').hide();
        }
    </script>
</head>
<body>
<div id="loading"></div>

<div id="preloader">
    <div id="status"></div>
</div>
@yield('header')
<img src="" alt="" class="img img-rounded" style="position: absolute; display: none; z-index: 9999999; border: 1px solid black; max-width: 400px; max-height: 400px;" id="imagePanel">
<img src="" alt="" class="img img-rounded" style="position: absolute; display: none; z-index: 9999999; border: 1px solid black; max-width: 400px; max-height: 400px;" id="userImagePanel">
<div class="container-fluid" style="margin-top: 55px">
    <div class="row">
        @yield('content')
    </div>
</div>
{{--<script src="https://code.jquery.com/jquery-3.3.1.js"></script>--}}

{{--<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>--}}
<script src="{{ asset('assets') }}/DataTables/DataTables-1.10.18/js/jquery.dataTables.min.js"></script>
{{--<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>--}}
<script src="{{ asset('assets') }}/DataTables/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>
{{--<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>--}}
<script src="{{ asset('assets') }}/DataTables/Buttons-1.5.6/js/buttons.flash.min.js"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>--}}
<script src="{{ asset('assets') }}/DataTables/JSZip-2.5.0/jszip.min.js"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>--}}
<script src="{{ asset('assets') }}/DataTables/pdfmake-0.1.36/pdfmake.min.js"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>--}}
<script src="{{ asset('assets') }}/DataTables/pdfmake-0.1.36/vfs_fonts.js"></script>
{{--<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>--}}
<script src="{{ asset('assets') }}/DataTables/Buttons-1.5.6/js/buttons.html5.min.js"></script>
{{--<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>--}}
<script src="{{ asset('assets') }}/DataTables/Buttons-1.5.6/js/buttons.print.min.js"></script>
{{--<script src="https://cdn.datatables.net/scroller/2.0.0/js/dataTables.scroller.min.js"></script>--}}
<script src="{{ asset('assets') }}/DataTables/Scroller-2.0.0/js/dataTables.scroller.min.js"></script>
<script>
    var url = window.location;

    $('ul.nav a').filter(function() {
        return this.href == url;
    }).parent().parent().parent().addClass('active');

    $('ul.nav a').filter(function() {
        return this.href == url;
    }).parent().addClass('active');
</script>
@yield('additionalLibrary')
<script src="{{ asset('assets') }}/js/lib/chosen/chosen.jquery.min.js"></script>
<script src="{{ asset('assets') }}/js/jquery.multiselect.js"></script>
<script>
    jQuery(document).ready(function() {
        jQuery(".standardSelect").chosen({
            disable_search_threshold: 10,
            placeholder_text_single: "выбрать",
            no_results_text: "Упс, ничего не найдено!",
            width: "100%",
            search_contains:true,
        });

        $('li.dropdown a').on('click', function (event) {
            $(this).parent().toggleClass('open');
        });
        $('input[type=search]').attr('placeholder', 'Поиск');
        $("input").focus(function() { $(this).select(); } );
        $(document).on('keydown', function(event) {
            if (event.key == "Escape") {
                jQuery(".modal").modal('hide');
            }
        });
        getNotAcceptedRequestsAmount();
        $('input[type=number]').on('focus', function (e) {
            $(this).on('mousewheel.disableScroll', function (e) {
                e.preventDefault()
            })
        });
        $('input[type=number]').on('blur', function (e) {
            $(this).off('mousewheel.disableScroll')
        })

        $('input[type=number]').on('keydown', function (e) {
            if(!((e.keyCode > 95 && e.keyCode < 106)
                || (e.keyCode > 47 && e.keyCode < 58)
                || e.keyCode == 8 || e.keyCode == 190 || e.keyCode ==37 || e.keyCode == 39 || e.keyCode == 110 || e.keyCode == 86 || e.keyCode == 17)) {
                return false;
            }
        });

        $('input[type=number]').on('keyup', function (e) {
            var x = $(this).val();
            if ($(this).val()>=1000000){
                $(this).val(x);
                alert('больще 1000000 недопустимое значение!!')
            }
        })
    });

    $(window).on('load', function() {
        $('#status').fadeOut();
        $('#preloader').delay(350).fadeOut('slow');
        $('body').delay(350).css({'overflow':'visible'});
    });

    $('#loading').hide();

    function getNotAcceptedRequestsAmount() {
        $.ajax({
            type:'get',
            url:"{{route('getNotAcceptedRequestsAmount')}}",
            success:function(item){
                var data = JSON.parse(item);
                console.log(data.requests)
                if (data.existRequest == null) {
                    $('sup.isExistNotification').hide();
                }else{
                    $('sup.isExistNotification').show().html('<b>!</b>');
                }
                if (data.requests == null) {
                    $('sup.notAcceptedRequestsAmount').hide();
                }else{
                    $('sup.notAcceptedRequestsAmount').show().html(data.requests)
                }
                if (data.returns == null) {
                    $('sup.notAcceptedReturnRequestsAmount').hide();
                }else{
                    $('sup.notAcceptedReturnRequestsAmount').show().html(data.returns)
                }
                if (data.moving_requests == null) {
                    $('sup.notAcceptedMovingRequestsAmount').hide();
                }else{
                    $('sup.notAcceptedMovingRequestsAmount').show().html(data.moving_requests)
                }
                if (data.defective_requests == null) {
                    $('sup.notAcceptedDefectiveRequest').hide();
                }else{
                    $('sup.notAcceptedDefectiveRequest').show().html(data.defective_requests)
                }
                if (data.ordersCount == null) {
                    $('sup.activeOrdersCount').hide();
                }else{
                    $('sup.activeOrdersCount').show().html(data.ordersCount)
                }

            },
            error: function (request, status, error) {
                var json = $.parseJSON(request.responseText)
                $('.alert-warning').show();
                $('.alert-warning ul').empty();
                $.each(json.errors, function (index, value) {
                    $('.alert-warning ul').append('<li>'+value+'</li>');
                })
            }
        });
    }
</script>
</body>
</html>

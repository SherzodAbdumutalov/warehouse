@extends('layouts.master')

@section('header')
    @include('layouts.header')
@endsection
@section('content')
    @include('pages.'.$page)
@endsection
@section('additional_css')
    <style>
        tr.group,
        tr.group:hover {
            background-color: #ddd !important;
        }

        tr.group:hover > td{
            background-color: #ddd !important;
        }

    </style>
@endsection

<div class="panel panel-primary">

    <div class="panel-heading" style="padding: 1px 5px">
        создание заявки
    </div>
    <div class="panel-body">
        <div class="alert-info" style="display: none">
            <ul></ul>
        </div>

        <div class="clearfix">
            <form action="">
                <div class="col-md-2">
                    <label for="to_storage_id" style="font-weight: bold">склады</label>
                    <select name="to_storage_id" data-placeholder="storage select" id="to_storage_id" class="standardSelect" {{ (session()->has('transaction_request_list'))?'disabled':'' }} onchange="get_storage_items()">
                        <option value="0" >--все склады--</option>
                        @foreach($storages as $storage)
                            <option value="{{ $storage->id }}" >{{ $storage->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                    <label for="from_storage_id" style="font-weight: bold">получатель</label>
                    <select name="from_storage_id" data-placeholder="production line select" id="from_storage_id" class="standardSelect">
                        <option value="0">не выбран</option>
                        @foreach($production_lines as $production_line)
                            <option value="{{ $production_line->id }}" >{{ $production_line->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-md-5">
                    <label for="item_id" style="font-weight: bold">единицы</label>
                    <select name="item_id" data-placeholder="выбрать деталь..." id="item_id" class="standardSelect ">
                    </select>
                </div>

                <div class="col-md-1">
                    <label for="amount" style="font-weight: bold">кол-во</label><br>
                    <input type="number" name="amount" min="0.0001" step="0.0001" id="amount" style="width: 100px">
                </div>
                <div class="col-md-1">
                    <label for="withoutNorm" style="font-weight: bold">без нормы</label><br>
                    <input type="checkbox" name="withoutNorm" id="withoutNorm" value="">
                </div>
                <div class="col-md-1">
                    <button class="btn btn-warning " type="button" id="validate" style="margin-top: 30px; padding: 3px 15px" onclick="create_request_list()"><i class="fa fa-plus"></i> добавить</button>
                </div>
            </form>
        </div>
        <div class="col-md-12 clearfix" style="margin-top: 15px; background-color: whitesmoke">
            <div class="col-md-4">
                <div class="col-md-3" style="background-color: blue; height: 20px; "></div>
                <div class="col-md-9">конфликтующая ед. изм.</div>
            </div>
            <div class="col-md-4">
                <div class="col-md-3" style="background-color: green; height: 20px; "></div>
                <div class="col-md-9">дублирующие</div>
            </div>
            <div class="col-md-4">
                <div class="col-md-3" style="background-color: red; height: 20px; "></div>
                <div class="col-md-9">недостаточно средств</div>
            </div>
        </div>
        <div class="col-md-12 clearfix" style="margin-top: 5px; background-color: whitesmoke">
            <p id="model_name" style="display: none"></p>
        </div>

        <table id="request_list_table" class="table table-hover table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>артикул</th>
                <th>SAP код</th>
                <th>склад</th>
                <th>наименование</th>
                <th>кол-во</th>
                <th>остаток</th>
                <th>ост. на линии</th>
                <th>ед. изм.</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
            <tr>
                <th>артикул</th>
                <th>SAP код</th>
                <th>склад</th>
                <th>наименование</th>
                <th>кол-во</th>
                <th>остаток</th>
                <th>ост. на линии</th>
                <th>ед. изм.</th>
                <th></th>
            </tr>
            </tfoot>
        </table>
    </div>

    <div class="modal fade" id="edit_amount" role="dialog">
        <div class="modal-dialog modal-sm" role="document" style="width: 25%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" style="font-size: 24px" id="close_modal_update_amount">&times;</button>
                    изменить кол-во элемента
                </div>
                <div class="modal-body">
                    <p id="element_name"></p>
                    <form>
                        <input type="hidden" name="item_key" id="item_key" value="">
                        <label for="edit_amount_input">кол-во</label>
                        <input type="number" id="edit_amount_input" min="0" name="edit_amount_input" class="form-control" value="">
                    </form>
                    <div class="col-md-12">
                        <label for="replace_items">взаимозаминяющие</label>
                        <select name="replace_items" id="replace_items" data-placeholder="выбрать..." class="standardSelect">
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="update_amount()"><i class="fa fa-edit"></i></button>
                </div>
            </div>
        </div>
    </div>
    
    <div class="panel-footer clearfix">
        <form >
            <div class="col-md-5">
                <label for="description" style="font-weight: bold">Примечание</label>
                <input type="text" name="description" id="description" style="resize: none" maxlength="50" class="form-control" placeholder="вводите текст... (макс 50 символов)">
            </div>

            <div class="col-md-3">
                <div style="float: left; padding-top: 30px">
                    <button class="btn btn-warning" type="button" id="save_request_btn" style="font-size: 12px;" onclick="save_request()" disabled><i class="glyphicon glyphicon-floppy-save"></i> сохранить</button>
                    <button class="btn btn-danger" type="button" style="font-size: 12px;" onclick="clear_request_session()"><i class="glyphicon glyphicon-remove-circle"></i> очистить</button>
                </div>
            </div>
        </form>
    </div>
</div>
@section('additionalLibrary')
    {{--dataTables library--}}
    <script>
        $(document).ready(function() {

            if (sessionStorage.getItem('chosen_to_storage_id_for_request')!=null) {
                $('#to_storage_id').val(sessionStorage.getItem('chosen_to_storage_id_for_request')).trigger('chosen:updated');
                $('#to_storage_id').attr('disabled', true).trigger("chosen:updated")
            }

            $('input[name=amount]').on('keydown', function (e) {
                if(e.keyCode==13){
                    create_request_list()
                }
            });

            $('input[name=edit_amount_input]').on('keydown', function (e) {
                if(e.keyCode==13){
                    update_amount()
                }
            });
            get_storage_items();
            fetch_table();
        } );

        $('#replace_items').on('change', function () {
            if ($('#replace_items').val() != 0) {
                $('#item_id').val($('#replace_items').val()).trigger("chosen:updated");
                $('#amount').val($('#edit_amount_input').val());
            }else{
                $('#amount').val('');
            }
        });

        $('#from_storage_id').on('change', function () {
            change_from_storage_remaining();
        });

        function create_request_list() {
            $('.alert-info').hide();
            var to_storage_id = $('#to_storage_id').val();
            var from_storage_id = $('#from_storage_id').val();
            var item_id = $('#item_id').val();
            var item_name = $( "#item_id option:selected" ).text();
            console.log(item_name)
            var amount = $('#amount').val();
            var withoutNorm = 0;
            if ($('#withoutNorm').is(":checked")){
                withoutNorm = 1
            }else{
                withoutNorm = 0

            }
            if (amount>0){
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'post',
                    url:"{{route('add_request')}}",
                    data:{amount:amount, item_id:item_id, to_storage_id:to_storage_id, withoutNorm:withoutNorm, from_storage_id:from_storage_id},
                    success: function (data) {
                        var items = [];
                        sessionStorage.setItem('chosen_to_storage_id_for_request', to_storage_id);
                        $('#to_storage_id').attr('disabled', true).trigger("chosen:updated");
                        if($.parseJSON(data)['has_child'] && withoutNorm !== 1){
                            if (sessionStorage.getItem('request_items')!=null) {
                                if (confirm('удалить список! Потому что у этого есть норма!')) {
                                    sessionStorage.removeItem('request_items');
                                    sessionStorage.setItem('request_items', data);
                                    sessionStorage.setItem('model_id', item_id);
                                    sessionStorage.setItem('model_amount', amount);
                                    sessionStorage.setItem('model_name', item_name);
                                    fetch_table();
                                }
                            }else{
                                sessionStorage.removeItem('request_items');
                                sessionStorage.setItem('request_items', data);
                                sessionStorage.setItem('model_id', item_id);
                                sessionStorage.setItem('model_amount', amount);
                                sessionStorage.setItem('model_name', item_name);
                                fetch_table();
                            }

                        }else{
                            if (!is_exist(JSON.parse(data)['data'])){
                                if (JSON.parse(sessionStorage.getItem('request_items'))!=null){
                                    items = JSON.parse(sessionStorage.getItem('request_items'));
                                    $.merge(items['data'], JSON.parse(data)['data']);
                                    sessionStorage.setItem('request_items', JSON.stringify(items));
                                }else{
                                    sessionStorage.setItem('request_items', data);
                                }

                                fetch_table();
                                $('.alert-info').show();
                                $('.alert-info ul').empty().append('<li>добавлен</li>');
                            } else {
                                $('.alert-info').show();
                                $('.alert-info ul').empty().append('<li>уже есть</li>');
                            }
                        }
                        get_storage_items();
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        console.log(json);
                        $('.alert-info').show();
                        $('.alert-info ul').empty();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').append('<li>'+value+'</li>');
                        })
                    }
                })
            } else {
                $('#close_edit_amount_modal').click()
                $('.alert-info').show();
                $('.alert-info ul').empty().append('<li>0 или пустое значение нельзя</li>');
            }
        }

        function is_exist(items) {
            x = false;
            var old_items = $.parseJSON(sessionStorage.getItem('request_items'));
            if (old_items!=null){
                old_items['data'].forEach(function (data, key){
                    items.forEach(function (item, index) {
                        if(data.item_id==item.item_id){
                            x = true
                        }
                    })
                })
            }
            return x
        }

        function fetch_table() {
            var positionY = sessionStorage.getItem('create_request_table_position');
            $('#request_list_table').DataTable().clear();
            var deny_to_save = true;
            var items = [];
            if (sessionStorage.getItem('request_items')!=null) {
                items = JSON.parse(sessionStorage.getItem('request_items'));
            }
            var groupColumn = 2;
            $('#request_list_table').DataTable().destroy();
            $('#request_list_table').DataTable( {
                data : items.data,
                order: [[ groupColumn, 'asc' ]],
                columnDefs: [
                    { orderable: false, targets: [0, 1, 2, 3, 4, 5, 6, 7, 8] }, { visible: false, targets: [groupColumn] }
                ],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "_TOTAL_ записей",
                    "infoEmpty": "нет записей",
                    "infoFiltered": "((<b>отфильтровано из _MAX_ записей</b>))",
                    "search": "<i class='fa fa-search' style='float: left'></i>"
                },
                columns:[
                    {'width':'5%', 'data':'articula_new'},
                    {'width':'5%', 'data':'sap_code'},
                    {'width':'5%', 'data':'storage'},
                    {'width':'25%', 'data':'item_name'},
                    {'width':'5%', 'data':'amount'},
                    {'width':'5%', 'data':'remaining'},
                    {'width':'5%', 'data':'remaining_of_from_storage'},
                    {'width':'3%', 'data':'unit'},
                    {'width':'6%', 'data':'edit_btn'},
                ],
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr('id', dataIndex);
                    if (has_duplicate(data.item_id)) {
                        $(row).find('td:eq(0)').css('background-color','green').css('color', 'black');
                    }
                    if (has_deficit_item(data)){
                        $(row).find('td:eq(4)').css('background-color','red').css('color', 'white');
                    }else{
                        $(row).find('td:eq(4)').css('background-color', '');
                    }
                    if (data.error==1) {
                        $(row).find('td:eq(6)').css('background-color','blue').css('color', 'white');
                        deny_to_save = false;
                    }

                    if (data.error==2) {
                        $(row).css({'background-color':'grey'}, {'color':'white'});
                        deny_to_save = false;
                    }
                    console.log(deny_to_save)
                },
                paging: false,
                drawCallback: function ( settings ) {
                    var api = this.api();
                    var rows = api.rows( {page:'current'} ).nodes();
                    var last=null;

                    api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                        if ( last !== group ) {
                            $(rows).eq( i ).before(
                                '<tr class="group"><td colspan="8">'+group+'</td></tr>'
                            );

                            last = group;
                        }
                    } );
                    if (deny_to_save) {
                        $('#save_request_btn').attr('disabled', false).show();
                    }else{
                        $('#save_request_btn').attr('disabled', true).hide();
                    }
                    if (sessionStorage.getItem('model_name')!=null) {
                        $('#model_name').show();
                        $('#model_name').text(sessionStorage.getItem('model_name')).css('color', 'black')
                    }else{
                        $('#model_name').hide();
                        $('#model_name').text('')
                    }
                    if (positionY!=null) {
                        $('.dataTables_scrollBody').scrollTop(positionY);
                    }
                },
                scrollY: "60vh",
                scrollCollapse: true
            });
        }

        function get_storage_items(){
            var to_storage_id = $('#to_storage_id').val();
            var item_id = sessionStorage.getItem('last_item_id_in_request');
            var model_id = sessionStorage.getItem('model_id');
            $('#item_id').append('<option value="" >выбрать...</option>');
            $.ajax({
                type:'get',
                url:"{{route('requestItemsByStorage')}}",
                data:{to_storage_id:to_storage_id, model_id:model_id},
                success: function (data) {
                    var items = JSON.parse(data);
                    $('#item_id').empty().trigger("chosen:updated");
                    if (items.length>0){
                        items.forEach(function (data, index) {
                            if (item_id==data.id){
                                if (data.sap_code == null) {
                                    $('#item_id').append('<option value="'+data.id+'" selected>'+data.articula_new+' '+data.item_name+'</option>')
                                }else{
                                    $('#item_id').append('<option value="'+data.id+'" selected>'+data.articula_new+' '+data.sap_code+' '+data.item_name+'</option>')
                                }
                            } else {
                                if (data.sap_code == null) {
                                    $('#item_id').append('<option value="'+data.id+'">'+data.articula_new+' '+data.item_name+'</option>')
                                }else{
                                    $('#item_id').append('<option value="'+data.id+'">'+data.articula_new+' '+data.sap_code+' '+data.item_name+'</option>')
                                }
                            }
                        });
                        $('#item_id').trigger("chosen:updated");
                    }
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText)
                    console.log(json)
                    $('#loading').hide()
                    $('.alert-info').show();
                    $('.alert-info ul').empty();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').append('<li>'+value+'</li>');
                    })
                }
            });
        }

        function has_error(all_items) {
            var t = true;
            $.each(all_items,function(index, item){
                if (item.error == '1' || item.error == '2'){
                    t = false
                }
            });
            return t
        }

        function has_deficit_item(req_item){
            if(Number(req_item.remaining)<Number(req_item.amount)){
                return true
            }else{
                return false
            }
        }

        function has_duplicate(item_id){
            var count = 0;
            var has_dublicate = false;
            if (sessionStorage.getItem('request_items')!=null) {
                var items = $.parseJSON(sessionStorage.getItem('request_items'))['data'];
                if (items.length>0) {
                    for (var i=0; i<items.length; i++){
                        if (items[i].item_id==item_id){
                            count++
                        }
                    }
                    if (count>=2) {
                        has_dublicate = true
                    }else{
                        has_dublicate = false
                    }
                }
            }
            return has_dublicate
        }

        function change_from_storage_remaining() {
            var items = sessionStorage.getItem('request_items');
            var from_storage_id = $('#from_storage_id').val();
            $.ajax({
                type:'get',
                url:"{{route('changeFromStorageRemaining')}}",
                data:{items:items, from_storage_id:from_storage_id},
                success: function (data) {
                    sessionStorage.setItem('request_items', data)
                    fetch_table();
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText)
                    console.log(json);
                    $('#loading').hide();
                    $('.alert-info').show();
                    $('.alert-info ul').empty();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').append('<li>'+value+'</li>');
                    })
                }
            })
        }

        function set_data(e, amount, item_id) {
            $('#item_key').val($(e).closest('tr').attr('id'));
            $('#edit_amount_input').val(amount);
            // $('#element_name').text($('#row_'+item_id).find('td:eq(2)').text());
            $('#replace_items').empty().trigger("chosen:updated");
            var model_id = sessionStorage.getItem('model_id');
            $.ajax({
                type:'get',
                url:"{{route('replaceItems')}}",
                data:{item_id:item_id, model_id:model_id},
                success: function (data) {
                    var items = JSON.parse(data);
                    if (items.length>0){
                        $('#replace_items').append('<option value="0">без взаимозаменяющых</option>')
                        items.forEach(function (data) {
                            $('#replace_items').append('<option value="'+data.id+'">'+data.item_name+'</option>')
                        });
                        $('#replace_items').trigger("chosen:updated");
                    }
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText)
                    console.log(json);
                    $('#loading').hide();
                    $('.alert-info').show();
                    $('.alert-info ul').empty();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').append('<li>'+value+'</li>');
                    })
                }
            })

        }
        
        function update_amount() {
            sessionStorage.setItem('create_request_table_position', $('.dataTables_scrollBody').scrollTop());
            var key = $('#item_key').val();
            var edit_amount = $('#edit_amount_input').val();
            var update_arr = $.parseJSON(sessionStorage.getItem('request_items'));
            if ($('#replace_items').val() == null || $('#replace_items').val() == 0) {
                update_arr['data'][key].amount = edit_amount;
                sessionStorage.setItem('request_items', JSON.stringify(update_arr));
                fetch_table();
            }else{
                update_arr['data'].splice(key, 1);
                sessionStorage.setItem('request_items', JSON.stringify(update_arr));
                create_request_list();
            }
            $('#close_modal_update_amount').click();
        }

        function delete_item(e) {
            sessionStorage.setItem('create_request_table_position', $('.dataTables_scrollBody').scrollTop());
            var items = $.parseJSON(sessionStorage.getItem('request_items'));
            var key = $(e).closest('tr').attr('id');
            items['data'].splice(key, 1);
            sessionStorage.setItem('request_items', JSON.stringify(items));
            if (items['data'].length==0){
                sessionStorage.removeItem('model_id');
                sessionStorage.removeItem('model_name');
                sessionStorage.removeItem('model_amount');
            }
            fetch_table();
        }

        function clear_request_session() {
            sessionStorage.removeItem('request_items');
            sessionStorage.removeItem('model_id');
            sessionStorage.removeItem('model_name');
            sessionStorage.removeItem('model_amount');
            sessionStorage.removeItem('chosen_to_storage_id_for_request');
            $('#to_storage_id').attr('disabled', false).trigger("chosen:updated");
            fetch_table();
            get_storage_items();
        }

        function save_request() {
            $('#save_request_btn').hide();
            if (confirm('сохранить?')) {
                console.log(JSON.parse(sessionStorage.getItem('request_items')))
                var items = JSON.stringify(JSON.parse(sessionStorage.getItem('request_items'))['data']);
                var desc = $('#description').val();
                var from_storage_id = $('#from_storage_id').val();
                var model_id = sessionStorage.getItem('model_id');
                var model_amount = sessionStorage.getItem('model_amount');
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'post',
                    url:"{{route('register_request')}}",
                    data:{items:items, description:desc, from_storage_id:from_storage_id, model_id:model_id, model_amount:model_amount},
                    success: function (data) {
                        $('#save_request_btn').show();
                        $('#description').val('');
                        sessionStorage.removeItem('model_id');
                        sessionStorage.removeItem('model_name');
                        sessionStorage.removeItem('model_amount');
                        sessionStorage.removeItem('request_items');
                        sessionStorage.removeItem('chosen_to_storage_id_for_request');
                        $('#to_storage_id').attr('disabled', false).trigger("chosen:updated");
                        fetch_table();
                        $('.alert-info').show();
                        $('.alert-info ul').empty();
                        $('.alert-info ul').append('<li>сохранен</li>');
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText)
                        console.log(json)
                        $('#loading').hide()
                        $('.alert-info').show();
                        $('.alert-info ul').empty();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').append('<li>'+value+'</li>');
                        })
                    }
                })
            }else{
                $('#save_request_btn').show();
            }
        }

        function remove_items(storage_name) {
            sessionStorage.setItem('create_request_table_position', $('.dataTables_scrollBody').scrollTop());
            var items = $.parseJSON(sessionStorage.getItem('request_items'));
            var i = items['data'].length;
            while (i--) {
                if (items['data'][i].storage_name==storage_name){
                    items['data'].splice(i, 1);
                }
            }
            sessionStorage.setItem('request_items', JSON.stringify(items));
            if (items['data'].length==0){
                sessionStorage.removeItem('model_id');
                sessionStorage.removeItem('model_name');
                sessionStorage.removeItem('model_amount');
                sessionStorage.removeItem('chosen_to_storage_id_for_request');
                $('#to_storage_id').attr('disabled', false);
            }
            fetch_table()
        }
    </script>
@endsection
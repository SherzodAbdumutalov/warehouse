@section('additional_css')
    <style>

    </style>
@endsection
<div class="panel panel-primary">

    <div class="panel-heading" style="padding: 1px 5px">
        общие накладные для отчета
    </div>

    <div class="panel-body">
        <div class="alert alert-info" style="display:none">
            <ul>

            </ul>
        </div>
        <div class="clearfix">
            <form action="{{ route('printInsideInvoices') }}" method="post" >
                {{ csrf_field() }}
                <div class="col-md-2">
                    <label for="storage_id">склад</label>
                    <select name="storage_id" data-placeholder="выбрать склад" id="storage_id" class="standardSelect">
                        @foreach($storages as $storage)
                            <option value="{{ $storage->id }}">{{ $storage->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-1" style="margin-right: 30px">
                    <label for="currentDate">дата</label>
                    <input type="date" value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}" id="currentDate" name="currentDate" onkeydown="return false" />
                </div>
                <div class="col-md-4">
                    <label for="line_id">линия</label>
                    <select name="line_id" data-placeholder="выбрать линию" id="line_id" class="standardSelect">
                        @foreach($lines as $line)
                            <option value="{{ $line->id }}">{{ $line->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-1">
                    <label for="invoiceNumber">№</label>
                    <input type="text" class="form-control" name="invoiceNumber" id="invoiceNumber"  style="height: 24px">
                </div>
                <div class="col-md-1" style="margin-top: 30px">
                    <button type="submit" class="btn btn-info"><i class="fa fa-print"></i></button>
                </div>
            </form>
        </div>
        <table id="invoices_models_table" class="display table table-hover table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>№</th>
                <th style="padding-left: 0">артикул</th>
                <th style="padding-left: 0">SAP</th>
                <th style="padding-left: 0">наименование</th>
                <th style="padding-left: 0">кол-во</th>
                <th style="padding-left: 0">ед. изм.</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
            <tr>
                <th>№</th>
                <th style="padding-left: 0">артикул</th>
                <th style="padding-left: 0">SAP</th>
                <th style="padding-left: 0">наименование</th>
                <th style="padding-left: 0">кол-во</th>
                <th style="padding-left: 0">ед. изм.</th>
            </tr>
            </tfoot>
        </table>

        <table id="invoices_items_table" class="display table table-hover table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>№</th>
                <th style="padding-left: 0">артикул</th>
                <th style="padding-left: 0">SAP</th>
                <th style="padding-left: 0">наименование</th>
                <th style="padding-left: 0">кол-во</th>
                <th style="padding-left: 0">ед. изм.</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
            <tr>
                <th>№</th>
                <th style="padding-left: 0">артикул</th>
                <th style="padding-left: 0">SAP</th>
                <th style="padding-left: 0">наименование</th>
                <th style="padding-left: 0">кол-во</th>
                <th style="padding-left: 0">ед. изм.</th>
            </tr>
            </tfoot>
        </table>
    </div>
</div>
@section('additionalLibrary')
    {{--dataTables library--}}
    <script>
        $(document).ready(function() {
            fetchData();
            fetchModelData();
        } );
    </script>
    <script>

        $('#currentDate').on('change',function () {
            fetchData();
            fetchModelData();
        })
        $( "#line_id" ).change(function () {
            fetchData();
            fetchModelData();
        });
        $( "#storage_id" ).change(function () {
            fetchData();
            fetchModelData();
        });
        function fetchData() {
            var storage_id = $('#storage_id').val();
            var line_id = $('#line_id').val();
            var currentDate = $('#currentDate').val();
            $('#invoices_items_table').DataTable().destroy();
            $('#invoices_items_table').DataTable( {
                progress:true,
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "{{ route('getInsideInvoices')}}",
                    type: "get",
                    data:{line_id:line_id, currentDate:currentDate, storage_id:storage_id},
                    dataSrc: function (data) {
                        console.log(data);
                        return data.data;
                    },
                    error: function (xhr, error, thrown) {
                        console.log(xhr)
                        console.log(error)
                        console.log(thrown)
                    }
                },
                columnDefs: [
                    { orderable: false, targets: [0, 1, 2, 3, 4, 5] }
                ],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "_TOTAL_ записей",
                    "infoEmpty": "",
                    "infoFiltered": "((<b>отфильтровано из _MAX_ записей</b>))",
                    "search": "<i class='fa fa-search' style='float: left'></i>"
                },
                order: false,
                paging: false,
                bPaginate: false,
                columns: [
                    { "width": "1%", "data":"key" },
                    { "width": "8%", "data":"articula_new" },
                    { "width": "8%", "data":"sap_code" },
                    { "width": "36%", "data":"item_name" },
                    { "width": "5%", "data":"amount" },
                    { "width": "5%", "data":"unit" }
                ],
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5]
                        },
                        title:'order list items',
                        messageTop: 'все элементы заказа',
                        text: '<i class="fa fa-file-excel-o"></i>'

                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5]
                        },
                        title:'order list items',
                        messageTop: 'все элементы заказа',
                        text: '<i class="fa fa-file-pdf-o"></i>'

                    },
                ],
                displayLength: 25,
                scrollY: "50vh",
                scrollCollapse: true,
            });
        }

        function fetchModelData() {
            var line_id = $('#line_id').val();
            var currentDate = $('#currentDate').val();
            var storage_id = $('#storage_id').val();
            $('#invoices_models_table').DataTable().destroy();
            $('#invoices_models_table').DataTable( {
                progress:true,
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "{{ route('getInsideModelsInvoices')}}",
                    type: "get",
                    data:{line_id:line_id, currentDate:currentDate, storage_id:storage_id},
                    dataSrc: function (data) {
                        console.log(data);
                        return data.data;
                    },
                    error: function (xhr, error, thrown) {
                        console.log(xhr)
                        console.log(error)
                        console.log(thrown)
                    }
                },
                columnDefs: [
                    { orderable: false, targets: [0, 1, 2, 3, 4, 5] }
                ],
                order: false,
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "_TOTAL_ записей",
                    "infoEmpty": "",
                    "infoFiltered": "((<b>отфильтровано из _MAX_ записей</b>))",
                    "search": "<i class='fa fa-search' style='float: left'></i>"
                },
                paging: false,
                bPaginate: false,
                columns: [
                    { "width": "1%", "data":"key" },
                    { "width": "8%", "data":"articula_new" },
                    { "width": "8%", "data":"sap_code" },
                    { "width": "36%", "data":"item_name" },
                    { "width": "5%", "data":"amount" },
                    { "width": "5%", "data":"unit" }
                ],
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5]
                        },
                        title:'order list items',
                        messageTop: 'все элементы заказа',
                        text: '<i class="fa fa-file-excel-o"></i>'

                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5]
                        },
                        title:'order list items',
                        messageTop: 'все элементы заказа',
                        text: '<i class="fa fa-file-pdf-o"></i>'

                    },
                ],
                displayLength: 25,
                scrollY: "50vh",
                scrollCollapse: true,
            });
        }
    </script>
@endsection
<div class="panel panel-primary">

    <div class="panel-heading" style="padding-top: 10px">
        <h5 style="color: whitesmoke">Admin</h5>
    </div>
    <div class="panel-body">
        <div class="col-md-2">
            <select name="storages" id="storages_selector" class="standardSelect" data-placeholder="выберите склад" onchange="get_value_of_select()">
                <option value=""></option>
                @foreach($all_storages as $storage)
                    <option value="{{ $storage->id }}">{{ $storage->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-1" style="width: 100px">
            <button class="btn btn-danger" style="font-size: 11px">удалить все</button>
        </div>
        <div class="col-md-1">
            <a href="{{ route('show_import_transactions_page') }}" class="btn btn-warning" style="font-size: 11px" id="import_btn" >импортировать</a>
        </div>
    </div>
    <div class="panel-body">
        <table id="example" class="table table-bordered" style="width:100%">
            <thead>
            <tr>
                <th>№</th>
                <th>операция</th>
                <th>автор</th>
                <th>дата</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

</div>

<script>
    $(document).ready(function() {
        $('#example').DataTable();
        $('#import_btn').attr("disabled", true)
        $('#storages_selector').trigger('chosen:updated')
    } );
</script>
<script>
    function get_value_of_select(){
        var storage_id = $('#storages_selector option:selected').val()
        var storage_name = $('#storages_selector option:selected').text()

        $('#import_btn').text('импортировать в '+storage_name)
        $('#import_btn').attr('href', '{{ \Illuminate\Support\Facades\URL::route('show_import_transactions_page') }}?id='+storage_id)
        $.ajax({
            url:'{{ route('get_transactions_by_storage') }}',
            type: 'get',
            data: {storage_id:storage_id},
            success: function (data) {
                var transactions = $.parseJSON(data)
                $('#import_btn').attr("disabled", false)
                $('#example').DataTable().clear().draw()
                transactions.forEach(function (item) {
                    $('#example').DataTable().row.add([
                        item.id,
                        item.operation_id,
                        item.user_name,
                        item.create_time
                    ]).draw(false)
                })
            },
            error:function (request, status, error) {
                var json = $.parseJSON(request)
                console.log(json)
            }
        })
    }
</script>
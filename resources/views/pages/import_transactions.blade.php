@extends('layouts.master')
@section('additional_css')

@endsection
@section('content')
    <div class="panel panel-primary">

        <div class="panel-heading" style="padding-top: 10px">
        </div>
        <div class="panel-body">
            <div class="col-md-12 clearfix" style="margin-bottom: 10px">
                <form action="{{ route('upload_inventarisation_file') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="col-md-2">
                        <label for="storage_id" style="font-weight: bold">Склад</label>
                        <select name="storage_id" id="storage_id" class="standardSelect" >
                            @foreach($storages as $storage)
                                <option value="{{ $storage->id }}" {{ (session()->get('inventarization_items.storage_id')!=null)?(session()->get('inventarization_items.storage_id')==$storage->id)?'selected':'':'' }}>{{ $storage->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label for="inventarization_file">Файл</label>
                        <input type="file" class="form-control" name="inventarization_file" id="inventarization_file">
                    </div>
                    <div class="col-md-2" style="padding-top: 20px">
                        <span style="font-weight: bold; font-size: 18px">шаблон *.xls:</span><a href="{{ asset('assets/template/inventarization.xlsx') }}"> <i style="font-size: 18px; font-weight: bold" class="glyphicon glyphicon-download"></i> </a>
                    </div>
                    <div class="col-md-2">
                        <i class="fa fa-calendar"></i> инвентиризация дата: <input type="date" id="date" name="date" onkeydown="return false" style="font-size: 11px" value="{{ (session()->get('inventarization_items.date')!=null)?session()->get('inventarization_items.date'):\Carbon\Carbon::now()->format('Y-m-d') }}"/>
                    </div>
                    <div class="col-md-2" style="margin-top: 30px">
                        <button type="submit" name="upload_file" class="btn btn-warning">загрузить</button>
                    </div>
                </form>
            </div>
            {{--<div class="col-md-3">--}}
                {{--<div class="col-md-6" style="height: 25px; width: 100px; background-color: yellow"></div>--}}
                {{--<div class="col-md-6"><p style="display: block; float: left">пустая ячейка</p></div>--}}
            {{--</div>--}}
            {{--<div class="col-md-3">--}}
                {{--<div class="col-md-6" style="height: 25px; width: 100px; background-color: red"></div>--}}
                {{--<div class="col-md-6"><p style="display: block; float: left">not exist in base</p></div>--}}
            {{--</div>--}}
            {{--<div class="col-md-3">--}}
                {{--<div class="col-md-6" style="height: 25px; width: 100px; background-color: green"></div>--}}
                {{--<div class="col-md-6"><p style="display: block; float: left">дубликат в файле</p></div>--}}
            {{--</div>--}}
            {{--<div class="col-md-3">--}}
                {{--<div class="col-md-6" style="height: 25px; width: 100px; background-color: blue"></div>--}}
                {{--<div class="col-md-6"><p style="display: block; float: left">ошибка в формате</p></div>--}}
            {{--</div>--}}
            {{--<div class="col-md-3">--}}
                {{--<div class="col-md-6" style="height: 25px; width: 100px; background-color: #1f3346"></div>--}}
                {{--<div class="col-md-6"><p style="display: block; float: left">not exist in this storage</p></div>--}}
            {{--</div>--}}
            {{--<div class="col-md-3">--}}
                {{--<div class="col-md-6" style="height: 25px; width: 100px; background-color: #a8daf3"></div>--}}
                {{--<div class="col-md-6"><p style="display: block; float: left">not equal storage unit</p></div>--}}
            {{--</div>--}}

            <table id="inventorization_items_table" class="table table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>артикул нов.</th>
                    <th>кол-во</th>
                    <th>единица измерения</th>
                </tr>
                </thead>
                <tbody>
                @if(session()->has('inventarization_items.items'))
                    @foreach(session()->get('inventarization_items.items') as $arr)
                        <tr>
                            <td>{{ $arr['articula_new'] }}</td>
                            <td>{{ $arr['quantity'] }}</td>
                            <td>{{ $arr['unit'] }}</td>
                            <td>{{ $arr['storage'] }}</td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
            <form action="{{ route('import_transaction') }}" method="post">
                {{ csrf_field() }}
                @if (\Session::has('btn'))
                    @if(!is_null(\Session::get('btn')))
                        {!! \Session::get('btn') !!}
                    @endif
                @endif
            </form>
        </div>
        <div class="panel-footer">

        </div>
    </div>

    <script>
        $(document).ready(function() {
            var groupColumn = 3;
            $('input:file').change(
                function(){
                    if ($(this).val()) {
                        $('input:submit').attr('disabled',false);
                    }
                }
            );
            $('#inventorization_items_table').DataTable({
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "_TOTAL_ записей",
                    "infoEmpty": "нет записей",
                    "infoFiltered": "(_MAX_)",
                    "search": "<i class='fa fa-search' style='float: left'></i>"
                },
                columnDefs: [
                    { orderable: false, targets: [0,1,2] },
                    { visible: false, targets: groupColumn }
                ],
                order: [[ groupColumn, 'asc' ]],
                drawCallback: function ( settings ) {
                    var api = this.api();
                    var rows = api.rows( {page:'current'} ).nodes();
                    var last=null;

                    api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                        if ( last !== group ) {
                            $(rows).eq( i ).before(
                                '<tr class="group" style="background-color: #00aa88"><td colspan="4">'+group+'</td></tr>'
                            );

                            last = group;
                        }
                    } );
                },
                paging: false,
                bPaginate: false,
                scrollY: "65vh"
            });
        } );
    </script>
@endsection
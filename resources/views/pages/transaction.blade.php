@section('additional_css')
    <style>

        .proc p {
            overflow: hidden;
            text-overflow: ellipsis;
            max-width: 420px;
        }

        table tbody td {
            min-width: 130px;
        }
        .date-filter-input-fields{
            display: flex;
            justify-content: space-between;
        }

        .chosen-container{
            font-size: 11px!important;
        }

        .chosen-search-input{
            height: 20px!important;
        }
        .tableheads:hover {
            background-color: grey;
            border: 1px solid #353564;
            color: black;
            padding: 16px 32px;
            text-align: center;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            transition-duration: 0.2s;
        }


    </style>
@endsection
<div class="panel panel-primary">
    <input type="hidden" id="selectedFilter">
    <div class="panel-heading" style="padding: 1px 5px">
        <i class="fa fa-history"></i> История операций
    </div>
    <div class="panel-body">
        <div class="alert alert-info" style="display: none">
            <ul></ul>
        </div>
        <div class="col-md-3 date-filter-input-fields" style="padding-right: 0">
            <div style="display: inline-block;">
            <i class="fa fa-calendar"></i> с: <input type="date" value="" id="start" name="start_date" onkeydown="return false" style="font-size: 10px"/>
                по: <input type="date" value="" id="end" name="end_date" onkeydown="return false" style="font-size: 10px"/>
            </div>
            </div>
        <div class="col-md-2" style="padding-right: 0">
            <select name="filter[]" id="filter_by_storage" class="standardSelect" data-placeholder="склады (фильтр)" multiple>

            </select>
        </div>
        <div class="col-md-2" style="padding-right: 0">
            <select name="filter[]" id="filter_by_operation" data-placeholder="операции (фильтр)" class="standardSelect" multiple>

            </select>
        </div>
        <div class="col-md-1" style="padding-right: 0">
            <select name="filter[]" id="filter_by_from_storage" data-placeholder="отправители (фильтр)" class="standardSelect" multiple>
            </select>
        </div>
        <div class="col-md-1" style="padding-right: 0">
            <select name="filter[]" id="filter_by_to_storage" data-placeholder="получатели (фильтр)" class="standardSelect" multiple>

            </select>
        </div>
        <div class="col-md-1" style="padding-right: 0">
            <select name="filter[]" id="filter_by_user" data-placeholder="авторы (фильтр)" class="standardSelect" multiple>

            </select>
        </div>
        <div class="col-md-2" style="padding-right: 0">
            <select name="filter[]" id="filter_by_supplier" class="standardSelect" data-placeholder="поставщики (фильтр)" multiple>

            </select>
        </div>

        <table id="transaction_table" class="nowrap table table-hover table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th class="tableheads">артикул</th>
                <th class="tableheads">SAP код</th>
                <th class="tableheads">наименование</th>
                <th class="tableheads">кол-во</th>
                <th class="tableheads">ед. изм.</th>
                <th class="tableheads">цена за ед.</th>
                <th class="tableheads">цена</th>
                <th class="tableheads">валюта</th>
                <th class="tableheads">склад/линии</th>
                <th class="tableheads">операции</th>
                <th class="tableheads">дата</th>
                <th class="tableheads">получатель</th>
                <th class="tableheads">отправитель</th>
                <th class="tableheads">invoice заказа</th>
                <th class="tableheads">invoice лог.</th>
                <th class="tableheads">№ транспорта</th>
                <th class="tableheads">поставщик</th>
                <th class="tableheads">автор</th>
                <th class="tableheads">примечание</th>
                <th class="tableheads">№ запроса</th>
                <th class="tableheads">№ заказа</th>
                <th class="tableheads"></th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
            <tr>
                <th>артикул</th>
                <th>SAP код</th>
                <th>наименование</th>
                <th>кол-во</th>
                <th>ед. изм.</th>
                <th>цена за ед.</th>
                <th>цена</th>
                <th>валюта</th>
                <th>склад/линии</th>
                <th>операции</th>
                <th>дата</th>
                <th>получатель</th>
                <th>отправитель</th>
                <th>invoice заказа</th>
                <th>invoice лог.</th>
                <th>№ транспорта</th>
                <th>поставщик</th>
                <th>автор</th>
                <th>примечание</th>
                <th>№ запроса</th>
                <th>№ заказа</th>
                <th></th>
            </tr>
            </tfoot>
        </table>
    </div>
</div>
<div id="edit_amount_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm" style="width: 20%">
        <form id="update_item_amount" method="post">
            {{ csrf_field() }}
            <div class="modal-content" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" style="font-size: 28px"  id="close_edit_amount_modal">&times;</button>
                    <h4 class="modal-title" id="edit_modal">изменить</h4>
                </div>
                <div class="modal-body">
                    <label for="amount">кол-во</label>
                    <input type="number" name="amount" step="0.00001" min="0" id="amount" onkeyup="if(parseFloat(this.value) >= parseFloat(this.max)) this.value = this.max;" class="form-control">
                    <span id="maxValueOfOrder"></span>
                    <input type="hidden" name="dream_transaction_list_id" id="dream_transaction_list_id" >
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-warning" >Сохранить</button>
                </div>
            </div>
        </form>
    </div>
</div>
@section('additionalLibrary')

    <script>
        $(document).ready(function() {
            var now = new Date();

            var day = ("0" + now.getDate()).slice(-2);
            var month = ("0" + (now.getMonth() + 1)).slice(-2);

            var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
            if (sessionStorage.getItem('startDate')!=null) {
                $('#start').val(sessionStorage.getItem('startDate'));
            }else{
                $('#start').val(today)
            }
            if (sessionStorage.getItem('endDate')!=null) {
                $('#end').val(sessionStorage.getItem('endDate'));
            }else{
                $('#end').val(today)
            }
            $('#filter_by_storage').empty().trigger("chosen:updated");
            filterOfFilters();
            $('#transaction_table').DataTable().destroy();
            fetch_transaction_data();
            $('a[data-toggle="tab"]').on('shown.bs.tab', function () {
                $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust().draw();
            } );
            $('#loading').hide()
        });

        $('#start').on('change',function () {
            sessionStorage.setItem('startDate', $('#start').val());
            if ($('#start').val()>$('#end').val()) {
                $('#end').val($('#start').val());
                sessionStorage.setItem('endDate', $('#end').val());
            }
            $('#filter_by_storage').empty().trigger("chosen:updated");
            filterOfFilters();
            $('#transaction_table').DataTable().destroy();
            fetch_transaction_data();
        });

        $('#end').on('change',function () {
            sessionStorage.setItem('endDate', $('#end').val());
            $('#transaction_table').DataTable().destroy();
            $('#filter_by_storage').empty().trigger("chosen:updated");
            filterOfFilters();
            fetch_transaction_data();
        });
        $('.standardSelect').on('change',function () {
            $('#transaction_table').DataTable().destroy();
            fetch_transaction_data();
        });

        $('#filter_by_storage').on('change', function () {
            var trigger = false;
            filterOfFilters(trigger);
        });

        function filterOfFilters(trigger = true) {
            var start_date = $('#start').val();
            var end_date = $('#end').val();
            var storages = $('#filter_by_storage').val();
            $.ajax({
                url: "{{ route('filterOfFilters') }}",
                type: "get",
                data: { start_date:start_date, end_date:end_date, storages:storages},
                success: function (data) {
                    console.log(JSON.parse(data))
                    var items = $.parseJSON(data);
                    if (trigger) {
                        $.each(items.storages, function (key, value) {
                            $('#filter_by_storage').append('<option value="'+value.id+'">'+value.name+'</option>');
                        });
                        $('#filter_by_storage').trigger("chosen:updated");
                    }

                    $('#filter_by_from_storage').empty();
                    $('#filter_by_from_storage').append('<option value="-1">EMPTY</option>');
                    $.each(items.sender, function (key, value) {
                        $('#filter_by_from_storage').append('<option value="'+value.id+'">'+value.name+'</option>');
                    });
                    $('#filter_by_from_storage').trigger("chosen:updated");
                    $('#filter_by_to_storage').empty();
                    $('#filter_by_to_storage').append('<option value="-1">EMPTY</option>');
                    $.each(items.acceptor, function (key, value) {
                        $('#filter_by_to_storage').append('<option value="'+value.id+'">'+value.name+'</option>');
                    });
                    $('#filter_by_to_storage').trigger("chosen:updated");
                    $('#filter_by_user').empty();
                    $.each(items.users, function (key, value) {
                        $('#filter_by_user').append('<option value="'+value.id+'">'+value.firstname+'</option>');
                    });
                    $('#filter_by_user').trigger("chosen:updated");
                    $('#filter_by_supplier').empty();
                    $.each(items.suppliers, function (key, value) {
                        $('#filter_by_supplier').append('<option value="'+value.id+'">'+value.name+'</option>');
                    });
                    $('#filter_by_supplier').trigger("chosen:updated");
                    $('#filter_by_operation').empty();
                    $.each(items.operations, function (key, value) {
                        $('#filter_by_operation').append('<option value="'+value.id+'">'+value.name+'</option>');
                    });
                    $('#filter_by_operation').trigger("chosen:updated");

                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    console.log(json)
                    $('.alert-warning').show();
                    $.each(json.errors, function (index, value) {
                        $('.alert-warning ul').empty().append('<li>'+value+'</li>');
                    })
                }
            });
        }

        function fetch_transaction_data() {
            var filterArr = {};
            var filterText = '';
            $('.standardSelect').each(function (index, value) {
                var parentKey = $(this).attr('id');
                filterArr[parentKey] = [];
                if ($(this).val()!=null) {
                    $.each($(this).val(), function (i, val) {
                        filterArr[parentKey].push(val);
                    });
                }
                if ($(this).find('option:selected').text()!='') {
                    filterText += $(this).data('placeholder')+':'+$(this).find('option:selected').text()+', ';
                }
            });
            var start_date = $('#start').val();
            var end_date = $('#end').val();
            var oTable = $('#transaction_table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "{{ route('getTransactions') }}",
                    type: "post",
                    data: {
                        start_date:start_date,
                        end_date:end_date,
                        filterArr: JSON.stringify(filterArr)
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText)
                        console.log(json)
                    }
                },
                "columnDefs": [
                    {
                        orderable: false,
                        targets: [21]
                    },
                    {
                        "targets": [ 5,6,7 ],
                        "visible": false,
                        "searchable": false
                    }
                ],
                columns: [
                    { "data": "articula_new", 'width':'6%' },
                    { "data": "sap_code", 'width':'7%' },
                    { "data": "item_name", 'width':'220px' },
                    { "data": "amount", 'width':'5%' },
                    { "data": "unit_name", 'width':'2%' },
                    { "data": "price_per_unit", 'width':'2%' },
                    { "data": "price", 'width':'2%' },
                    { "data": "currency", 'width':'2%' },
                    { "data": "storage_name", 'width':'10%' },
                    { "data": "operation_name", 'width':'5%' },
                    { "data": "create_time", 'width':'7%' },
                    { "data": "destination", 'width':'6%' },
                    { "data": "from_destination", 'width':'6%' },
                    { "data": "invoice_number", 'width':'7%' },
                    { "data": "logistics_invoice", 'width':'7%' },
                    { "data": "container_number", 'width':'7%' },
                    { "data": "supplier_name", 'width':'7%' },
                    { "data": "user_name", 'width':'5%' },
                    { "data": "description", 'width':'10%' },
                    { "data": "request_id", 'width':'3%' },
                    { "data": "order_id", 'width':'3%' },
                    { "data": "buttons", 'width':'6%' }
                ],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "_TOTAL_ записей",
                    "infoEmpty": "данные отсутствует",
                    "infoFiltered": "(отфильтровано из _MAX_ записей)",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                "order": [ [10, 'desc'] ],
                "displayLength": 500,
                lengthMenu:[[500, 1000, 5000], [500, 1000, 5000]],
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>',
                        title: function () {
                            return 'с '+$('#start').val()+' до '+$('#end').val() +' | '+ filterText
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>'
                    },
                ],
                createdRow: function( row, data, dataIndex ) {
                    $( row ).find('td:eq(2)').addClass("proc");
                },
                drawCallback: function (data) {
                    $(oTable.column(3).footer()).text(' итог: '+data.json.total);
                },
                "sScrollY": "65vh",
                "sScrollX": "100%",
            } );
            var table = $('#transaction_table').DataTable();
            $('#transaction_table tbody').on( 'click', 'tr', function () {
                $(this).removeClass('selected');

                if ( $(this).hasClass('selected') ) {
                    $(this).removeClass('selected');
                }
                else {
                    table.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            } );
        }

        function setItemData(id){
            // $('#edit_modal').html(item.articula_new+' '+item.item_name)
            // $('#dream_transaction_list_id').val(id)
            // $('#amount').val(amount);
            $('#dream_transaction_list_id').val(id);
            $.ajax({
                url: "{{ route('edit_transaction_list') }}",
                type: "get",
                data: { list_id:id},
                success: function (data) {
                    console.log(JSON.parse(data))
                    $('#amount').val(JSON.parse(data).amount);
                    if (JSON.parse(data).logistics_amount!=null) {
                        $('#amount').attr('max', JSON.parse(data).logistics_amount);
                        $('#maxValueOfOrder').text('макс. кол-во логистики:('+JSON.parse(data).logistics_amount+')').css('font-weight', 'bold');
                    }else{
                        $('#amount').attr('max', '');
                        $('#maxValueOfOrder').text('').css('font-weight', 'bold');
                    }
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    console.log(json)
                    $('.alert-warning').show();
                    $.each(json.errors, function (index, value) {
                        $('.alert-warning ul').empty().append('<li>'+value+'</li>');
                    })
                }
            });
        }

        $('#update_item_amount').on('submit', function (event) {
            event.preventDefault();
            var amount = $('#amount').val()
            if (amount>0){
                var dream_transaction_list_id = $('#dream_transaction_list_id').val()
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'POST',
                    url:"{{route('update_transaction_list_amount')}}",
                    data:{amount:amount, dream_transaction_list_id:dream_transaction_list_id},
                    success: function (data) {
                        $('#transaction_table').DataTable().destroy()
                        fetch_transaction_data()
                        $('.alert-info').show();
                        $('.alert-info ul').empty().append('<li>обнавлен</li>');
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText)
                        console.log(json)
                        $('.alert-info').show();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').empty().append('<li>'+value+'</li>');
                        })
                    }
                })
            } else {
                alert('недопустимое значение!!')
            }
        });

        function delete_item(transaction_list_id) {
            if (confirm('удалить')) {
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'post',
                    url:"{{route('delete_transaction_list_item')}}",
                    data:{transaction_list_id:transaction_list_id},
                    success: function (data) {
                        $('#transaction_table').DataTable().destroy()
                        fetch_transaction_data()
                        $('.alert-info').show();
                        $('.alert-info ul').empty().append('<li>удален</li>');
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText)
                        console.log(json)
                        $('.alert-info').show();
                        $('.alert-info ul').empty()
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').append('<li>'+value+'</li>');
                        })
                    }
                })
            }
        }
    </script>
@endsection

@section('additional_css')
    <style>
        .modal-body {
            max-height: calc(100vh - 200px);
            overflow-y: auto;
            padding-top: 0;
            margin-top: 0;
        }

        .modal-dialog {
            width: calc(100% - 100px);
        }
        input[type=checkbox]
        {
            /* Double-sized Checkboxes */
            -ms-transform: scale(2); /* IE */
            -moz-transform: scale(2); /* FF */
            -webkit-transform: scale(2); /* Safari and Chrome */
            -o-transform: scale(2); /* Opera */
            padding: 10px;
        }

        .table > tbody > tr > td{
            font-size: 12px;
            line-height: 20px;
        }

        .table > thead > tr > th {
            font-weight: bold;
        }
    </style>
@endsection
<div class="panel panel-primary">
    <div class="panel-heading" style="padding: 1px 5px">
        приход заказа
    </div>
    <div class="panel-body">
        <div class="alert alert-info" style="display:none">
            <ul>

            </ul>
        </div>
        <div class="col-md-12" style="padding-left: 0">
            <div class="col-md-2">
                <select name="storage" data-placeholder="выбрать склад" id="storage_id" class="standardSelect">
                    @foreach($storages as $storage)
                        <option value="{{ $storage->id }}">{{ $storage->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <table id="orders_table" class="table table-hover table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>group</th>
                <th>артикул</th>
                <th>SAP код</th>
                <th>наименование</th>
                <th>item name</th>
                <th>заказчик</th>
                <th>invoice заказа</th>
                <th>дата</th>
                <th>кол-во в заказе</th>
                <th>прибыло</th>
                <th></th>
                <th>ед. изм.</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
            <tr>
                <th>group</th>
                <th>артикул</th>
                <th>SAP код</th>
                <th>наименование</th>
                <th>item name</th>
                <th>заказчик</th>
                <th>invoice заказа</th>
                <th>дата</th>
                <th>кол-во в заказе</th>
                <th>прибыло</th>
                <th></th>
                <th>ед. изм.</th>
                <th></th>
            </tr>
            </tfoot>
        </table>
        <form action="" method="post">
            {{ csrf_field() }}
            <div class="col-md-4">
                <label for="order_description">вводите примечание</label>
                <input name="order_description" id="order_description" maxlength="50" style="resize: none; height: 26px" placeholder="вводите текст... (макс 50 символов)" class="form-control">
            </div>
            <div class="col-md-8">
                <button type="button" class="btn btn-warning" style="float: right; margin-top: 30px" id="save_brought_item_btn" onclick="save_brought_item()"><i class="fa fa-save"></i> сохранить изменение</button>
            </div>
        </form>
    </div>
</div>
@section('additionalLibrary')
    {{--dataTables library--}}
    <script>
        $(document).ready(function() {
            fetch_orders_table();
            $('#storage_id').on('change', function () {
                fetch_orders_table();
            })
        } );
    </script>
    <script>
        //    orderorderorder

        function fetch_orders_table() {
            var storage_id = $('#storage_id').val();
            $('#orders_table').DataTable().destroy();
            var groupColumn = 0;
            $('#orders_table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    "url": "{{ route('get_orders_belongs_buffer_storage') }}",
                    "type": "post",
                    data:{storage_id:storage_id},
                    "dataSrc": function (data) {
                        $('#ordersCount').text(data.recordsTotal);
                        return data.data;
                    }
                },
                "createdRow": function ( row, data, index ) {
                    if (data.amount <= data.complete) {
                        $(row).css({'background-color':'#A9A9A9'}, {'color':'white'});
                    }
                },
                columnDefs:[
                    { orderable: false, targets: [0,1,2,3,4,5,6,7,8,9,10,11,12] },
                    { visible: false, targets: groupColumn }
                ],
                pageLength: 100,
                lengthMenu:[[100, 500, 1000], [100, 500, 1000]],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение от _START_ до _END_ из _TOTAL_ деталей",
                    "infoEmpty": "",
                    "infoFiltered": "(отфильтровано из _TOTAL_ заказов)",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                "columns": [
                    { "data": "group_row" },
                    { "data": "articula_new", "width":"5%" },
                    { "data": "sap_code", "width":"5%" },
                    { "data": "item_name", "width":"29%" },
                    { "data": "english_name", "width":"20%" },
                    { "data": "customer", "width":"7%" },
                    { "data": "invoice_number", "width":"7%" },
                    { "data": "delivery_time", "width":"5%" },
                    { "data": "amount", "width":"6%" },
                    { "data": "complete", "width":"4%" },
                    { "data": "input_field", "width":"5%" },
                    { "data": "unit", "width":"4%" },
                    { "data": "checkbox" },
                ],
                scrollY: "65vh",
                drawCallback: function ( settings ) {
                    var api = this.api();
                    var rows = api.rows( {page:'current'} ).nodes();
                    var last=null;

                    api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                        if ( last !== group ) {
                            $(rows).eq( i ).before(
                                '<tr class="group"><td colspan="13">'+group+'</td></tr>'
                            );

                            last = group;
                        }
                    } );
                    $('input[type=number]').on('focus', function (e) {
                        $(this).on('mousewheel.disableScroll', function (e) {
                            e.preventDefault()
                        })
                    })
                    $('input[type=number]').on('blur', function (e) {
                        $(this).off('mousewheel.disableScroll')
                    })
                    $('input[type=number]').on('keydown', function (e) {
                        if(!((e.keyCode > 95 && e.keyCode < 106)
                            || (e.keyCode > 47 && e.keyCode < 58)
                            || e.keyCode == 8 || e.keyCode == 190 || e.keyCode ==37 || e.keyCode == 39 || e.keyCode == 110 || e.keyCode == 86 || e.keyCode == 17)) {
                            return false;
                        }
                    })
                },
            });
        }

        function save_brought_item() {
            if (confirm('сохранить?')) {
                var storage_id = $('#storage_id').val();
                var order_description = $('#order_description').val();
                var obj = {};
                $('input[name^=brought_item_amount]').each(function () {
                    obj[$(this).attr('id')] = parseFloat($(this).val());
                });
                var obj_stringify = JSON.stringify(obj);
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type: 'post',
                    url: "{{route('save_brought_items_to_buffer')}}",
                    data: {items: obj_stringify, storage_id:storage_id, order_description:order_description},
                    success: function (data) {
                        fetch_orders_table();
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        console.log(json);
                        $('.alert-info ul').empty();
                        $('.alert-info').show();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').append('<li>'+value+'</li>');
                        })
                    }
                })
            }
        }

        function set_value_input(list_id, amount) {
            if(document.getElementById('checkbox_'+list_id).checked){
                document.getElementById(list_id).value = amount
            }else{
                document.getElementById(list_id).value = 0
            }
            save_input_items_on_session_for_buffer(list_id, document.getElementById(list_id).value);
        }

        function save_value_in_session_for_buffer(e) {
            var id = e.id;
            var input_value = e.value;
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type: 'post',
                url: "{{route('save_input_items_on_session_for_buffer')}}",
                data: {id:id, input_value:input_value},
                success: function (data) {
                    console.log(data);
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    console.log(json);
                    $('.alert-info ul').empty();
                    $('.alert-info').show();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').append('<li>'+value+'</li>');
                    })
                }
            })
        }

        function save_input_items_on_session_for_buffer(id, input_value) {
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type: 'post',
                url: "{{route('save_input_items_on_session_for_buffer')}}",
                data: {id:id, input_value:input_value},
                success: function (data) {
                    console.log(data);
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    console.log(json);
                    $('.alert-info ul').empty();
                    $('.alert-info').show();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').append('<li>'+value+'</li>');
                    })
                }
            })
        }
    </script>
@endsection
@section('additional_css')
    <style>
        .modal-body {
            max-height: calc(100vh - 200px);
            overflow-y: auto;
            padding-top: 0;
            margin-top: 0;
        }

        .modal-dialog {
            width: calc(100% - 100px);
        }
        input[type=checkbox]
        {
            /* Double-sized Checkboxes */
            -ms-transform: scale(2); /* IE */
            -moz-transform: scale(2); /* FF */
            -webkit-transform: scale(2); /* Safari and Chrome */
            -o-transform: scale(2); /* Opera */
            padding: 10px;
        }

        .table > tbody > tr > td{
            font-size: 12px;
            line-height: 20px;
        }

        .table > thead > tr > th {
            font-weight: bold;
        }

        #defectives_table tbody tr{
            cursor: pointer;
        }
    </style>
@endsection
<div class="panel panel-primary">

    <div class="panel-heading" style="padding: 1px 5px">
         Управление складом
    </div>
    <div class="panel-body">
        <div class="alert alert-info" style="display:none">
            <ul>

            </ul>
        </div>
        <div class="col-md-12 clearfix" style="margin-bottom: 10px; padding-left: 0">
            <div class="col-md-2" style="padding-left: 0">
                <select name="storage_id" id="storage_id" class="standardSelect" >
                    @foreach($storages as $storage)
                        <option value="{{ $storage->id }}">{{ $storage->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <ul class="nav nav-tabs" id="operation_tab">
            <li class="active"><a data-toggle="tab" href="#byRequest"><i class="fas fa-clipboard-list"></i> приход по актам <span class="badge"></span></a></li>
            <li><a data-toggle="tab" href="#byHand"><i class="fa fa-hand-paper-o"></i> ручная операция</a></li>
        </ul>
        <div class="tab-content">
            <div id="byHand" class="tab-pane fade">
                <div class="col-md-2">
                    <label for="operation"><b>операции</b></label>
                    <select name="operation" id="operation" class="standardSelect" >
                        {{--@foreach($operation_types as $type)--}}
                            {{--<option value="{{ $type->id }}" data-operation="{{ $type->operation }}">{{ $type->name }}</option>--}}
                        {{--@endforeach--}}
                    </select>
                </div>
                <div class="col-md-2" id="supplier_div">
                    <label for="supplier" style="font-weight: bold">получатель</label>
                    <select name="supplier" id="supplier" class="standardSelect">
                        <option value="0">без получателя</option>
                        @foreach($suppliers as $supplier)
                            <option value="{{ $supplier->id }}" >{{ $supplier->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="item"><b>единицы</b></label>
                    <select name="item" data-placeholder="item select" id="item" class="standardSelect chosen-choices ">
                    </select>
                </div>

                <div class="col-md-1">
                    <label for="amount"><b>кол-во</b></label><br>
                    <input type="number" name="amount" min="0" id="amount" class="form-control" style="height: 26px; display: block">
                </div>
                <div class="col-md-1">
                    <button class="btn btn-warning" type="button" style="margin-top: 30px; padding: 3px 15px" onclick="add_item()"><i class="fa fa-plus"></i> добавить</button>
                </div>

                <table id="selected_items_table" class="table table-hover table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th style="padding-left: 0">артикул</th>
                        <th style="padding-left: 0">SAP код</th>
                        <th style="padding-left: 0">наименование</th>
                        <th style="padding-left: 0">кол-во</th>
                        <th style="padding-left: 0">остаток</th>
                        <th style="padding-left: 0">ед. изм.</th>
                        <th style="padding-left: 0"></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th style="padding-left: 0">артикул</th>
                        <th style="padding-left: 0">SAP код</th>
                        <th style="padding-left: 0">наименование</th>
                        <th style="padding-left: 0">кол-во</th>
                        <th style="padding-left: 0">остаток</th>
                        <th style="padding-left: 0">ед. изм.</th>
                        <th style="padding-left: 0"></th>
                    </tr>
                    </tfoot>
                </table>

                <div class="col-md-4">
                    <label for="description" style="font-size: 16px; font-weight: bold">описаниe</label>
                    <input name="description" id="description" maxlength="50" style="resize: none; height: 26px" placeholder="вводите текст... (макс 50 символов)" class="form-control">
                </div>
                <div class="col-md-3">
                    <div style="float: left; padding-top: 30px">
                        <button class="btn btn-warning " type="submit" style="font-size: 12px; float: left" id="save_in_out" onclick="save_items()"><i class="glyphicon glyphicon-floppy-save" ></i> сохранить</button>
                        &nbsp;<button class="btn btn-danger " type="submit" style="font-size: 12px; float: right" onclick="clear_sessionStorage()"><i class="glyphicon glyphicon-remove-circle"></i> очистить</button>
                    </div>
                </div>
            </div>
            <div id="byRequest" class="tab-pane fade in active">
                <table id="defectives_table" class="table table-hover table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>автор</th>
                        <th>дата запроса</th>
                        <th>отправитель</th>
                        <th>примечание</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>№</th>
                        <th>автор</th>
                        <th>дата запроса</th>
                        <th>отправитель</th>
                        <th>примечание</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>


{{--edit amount--}}
<div id="edit_amount_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm" style="width: 20%">
        <div class="modal-content" >
            <div class="modal-header">
                изменить кол-во
                <button type="button" class="close" data-dismiss="modal" style="font-size: 38px" id="close_modal">&times;</button>
            </div>
            <div class="modal-body">
                <label for="edit_amount">кол-во</label>
                <input type="number" name="amount" id="edit_amount" step="0.0001" class="form-control">
                <input type="hidden" name="key" id="edit_key" value="">
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-warning" onclick="save_amount()"><i class="fa fa-save"></i> Сохранить</button>
            </div>
        </div>
    </div>
</div>

<div id="show_defective_detail" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <p style="color: red; font-weight: bold; display: inline-block" id="accepted"></p>
                <button type="button" class="close" data-dismiss="modal" id="close_detail_modal"><span style="font-size: 24px">&times;</span></button>
            </div>
            <div class="alert alert-danger" style="display:none">
                <ul>

                </ul>
            </div>
            <div class="modal-body">
                <table id="detail_table" class="table table-hover table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>артикул</th>
                        <th>стр арт.</th>
                        <th>наименование</th>
                        <th>SAP код</th>
                        <th>кол-во</th>
                        <th>остаток</th>
                        <th>ед. изм.</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>артикул</th>
                        <th>стр арт.</th>
                        <th>наименование</th>
                        <th>SAP код</th>
                        <th id="total">кол-во</th>
                        <th>остаток</th>
                        <th>ед. изм.</th>
                    </tr>
                    </tfoot>
                </table>
                <form>
                    <input type="hidden" name="defective_id_for_accept" id="defective_id_for_accept" value="">
                    <button class="btn btn-warning" type="button" id="accept_btn" style="display: none; float: right" onclick="accept_defective()"><i class="fas fa-clipboard-check"></i> принять</button>
                </form>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>
@section('additionalLibrary')
    {{--dataTables library--}}
    <script>
        $(document).ready(function() {
            $('input[name=amount]').on('keydown', function (e) {
                if(e.keyCode==13){
                    add_item();
                }
            });

            fetch_defectives_table();
            $('a[data-toggle="tab"]').on('shown.bs.tab', function () {
                $('#selected_items_table').DataTable().columns.adjust().draw();
                $('#defectives_table').DataTable().columns.adjust().draw();
            } );
            getDefectiveStorageRemainingItems();
            getOperationsByStorage();
            $('#selected_items_table').DataTable( {
                columnDefs: [
                    { orderable: false, targets: [0, 1, 2, 3, 4, 5, 6] }
                ],
                order: false,
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение от _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "",
                    "infoFiltered": "(отфильтровано из _MAX_ записей)",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                paging: false,
                bPaginate: false,
                "columns": [
                    { "width": "8%" },
                    { "width": "8%" },
                    { "width": "50%" },
                    { "width": "5%" },
                    { "width": "5%" },
                    { "width": "8%" },
                    { "width": "5%" },
                ],
                scrollY: "60vh",
                scrollCollapse: true
            });

            draw_table();

            $('#defectives_table').on('dblclick', 'tr', function (e) {
                var id = $(this).data('id');
                $('#show_defective_detail').modal("show");
                show_defective_detail(id);
            });
            $('#defectives_table tbody').on( 'click', 'tr', function () {
                $(this).removeClass('selected');

                if ( $(this).hasClass('selected') ) {
                    $(this).removeClass('selected');
                }
                else {
                    $('#defectives_table').DataTable().$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            } );
        } );
    </script>
    <script>
        $('#storage_id').on('change', function () {
           getDefectiveStorageRemainingItems();
           fetch_defectives_table();
           getOperationsByStorage();
        });

        $('#supplier').on('change', function () {
            getDefectiveStorageRemainingItems();
        });

        function getOperationsByStorage() {
            var storage_id = $('#storage_id').val();
            $('#operation').empty().trigger("chosen:updated");
            $.ajax({
                url: "{{ route('getOperationsByStorage') }}",
                type: "get",
                data: {storage_id:storage_id},
                success: function(data){
                    $.each(JSON.parse(data), function (index, value) {
                        $('#operation').append('<option value="'+value.id+'" data-operation="'+value.operation+'">'+value.name+'</option>');
                    });
                    $('#operation').trigger("chosen:updated");
                },
                error: function (request, status, error) {
                    $('#loading').hide();
                    var json = $.parseJSON(request.responseText);
                    console.log(json);
                    $('.alert-info').show();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').empty().append('<li>'+value+'</li>');
                    })
                }
            })
        }

        function getDefectiveStorageRemainingItems() {
            var storage_id = $('#storage_id').val();
            var supplier_id = $('#supplier').val();
            $('#item').empty().trigger("chosen:updated");
            $.ajax({
                url: "{{ route('getDefectiveStorageRemainingItems') }}",
                type: "get",
                data: {storage_id:storage_id, supplier_id:supplier_id},
                success: function(data){
                    $.each(JSON.parse(data), function (index, value) {
                        $('#item').append('<option value="'+value.id+'">'+value.articula_new+' '+value.item_name+' '+value.sap_code+'</option>');
                    })
                    $('#item').trigger("chosen:updated");
                },
                error: function (request, status, error) {
                    $('#loading').hide();
                    var json = $.parseJSON(request.responseText);
                    console.log(json);
                    $('.alert-info').show();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').empty().append('<li>'+value+'</li>');
                    })
                }
            })
        }

        function add_item() {
            $('#loading').show();
            var storage_id = $('#storage_id').val();
            var operation = $('#operation').val();
            var supplier_id = $('#supplier').val();
            sessionStorage.setItem('last_selected_operation_id_for_defective', operation);
            sessionStorage.setItem('last_selected_storage_id_for_defective', storage_id);
            sessionStorage.setItem('last_selected_supplier_id_for_defective', supplier_id);
            var item_id = $('#item').val();
            var str_amount = $('#amount').val();
            var amount = parseFloat(str_amount.replace(',', '.'));
            if(amount>0){
                $('#operation').prop('disabled', true).trigger("chosen:updated");
                $('#from_storage_id').prop('disabled', true).trigger("chosen:updated");
                $('#supplier').prop('disabled', true).trigger("chosen:updated");
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "{{ route('collect_defective_item_input_output') }}",
                    type: "post",
                    data: {item_id:item_id, amount:amount, operation_id:operation, supplier_id:supplier_id, storage_id:storage_id},
                    success: function(data){
                        $('#amount').val('');
                        $('#loading').hide();
                        var items = $.parseJSON(data);
                        items.forEach(function (value, index) {
                            if(is_exist(value.item_id, value.amount)){
                                draw_table();
                            }else{
                                collect_to_array(value);
                            }
                        })
                    },
                    error: function (request, status, error) {
                        $('#loading').hide();
                        var json = $.parseJSON(request.responseText);
                        console.log(json);
                        $('.alert-info').show();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').empty().append('<li>'+value+'</li>');
                        })
                    }
                })
            }else{
                $('#loading').hide();
                alert('вводите кол-во!')
            }
        }

        function draw_table() {
            $('#save_in_out').prop('disabled', false).show();
            var operation = $('#operation').find(':selected').data('operation');
            console.log(operation)
            $('#selected_items_table').DataTable().clear().draw(false);
            var items = $.parseJSON(sessionStorage.getItem('defective_storage_input_output_items'));
            if (items!=null){
                items.forEach(function (value, key) {
                    if(value.amount>value.remaining){
                        $('#save_in_out').prop('disabled', true).hide();
                    }
                    var trow = $('#selected_items_table').DataTable().row.add([
                        value.articula_new,
                        value.sap_code,
                        value.item_name,
                        parseFloat(value.amount),
                        value.remaining,
                        value.unit,
                        '<button type="button" class="btn btn-warning " data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#edit_amount_modal" onclick="set_data_for_edit('+key+','+value.amount+')" style="font-size: 11px"><i class="fa fa-edit"></i></button> <button type="button" class="btn btn-danger " onclick="delete_item('+key+')" style="font-size: 11px" ><i class="fa fa-trash"></i></button>'
                    ]).node();
                    trow.id = 'row_'+key;
                    $('#selected_items_table').DataTable().draw(false);
                    if (Number(value.remaining)<Number(value.amount) && (operation==-1)){
                        $(trow).find('td:eq(4)').css('background-color', 'red');
                    }else {
                        $(trow).find('td:eq(4)').css('background-color', '');
                    }
                })
            }else{
                $('#save_in_out').prop('disabled', true).hide();
            }
            $('#loading').hide();
        }

        function collect_to_array(item) {
            var old_items = [];
            if(sessionStorage.getItem('defective_storage_input_output_items')!=null){
                old_items = $.parseJSON(sessionStorage.getItem('defective_storage_input_output_items'))
            }
            var data = {
                'item_id':item.item_id,
                'articula_new':item.articula_new,
                'sap_code':item.sap_code,
                'item_name':item.item_name,
                'amount':parseFloat(item.amount),
                'remaining':item.remaining,
                'unit':item.unit,
                'unit_id':item.unit_id,
                'storage_id':item.storage_id,
            };
            old_items.push(data);
            sessionStorage.setItem('defective_storage_input_output_items', JSON.stringify(old_items));
            draw_table();
        }

        function clear_sessionStorage() {
            $('#operation').prop('disabled', false).trigger("chosen:updated");
            $('#storage_id').prop('disabled', false).trigger("chosen:updated");
            $('#supplier').prop('disabled', false).trigger("chosen:updated");
            sessionStorage.removeItem('defective_storage_input_output_items');
            sessionStorage.removeItem('last_selected_operation_id_for_defective');
            sessionStorage.removeItem('last_selected_storage_id_for_defective');
            sessionStorage.removeItem('last_selected_supplier_id_for_defective');
            $('#supplier').val(0).trigger('chosen:updated');
            draw_table();
            getDefectiveStorageRemainingItems();
        }

        function is_exist(item_id, amount) {
            var items = $.parseJSON(sessionStorage.getItem('defective_storage_input_output_items'));
            if(items!=null){
                for (var i=0; i<items.length; i++){
                    if(item_id==items[i].item_id){
                        items[i].amount+=parseFloat(amount);
                        sessionStorage.setItem('defective_storage_input_output_items', JSON.stringify(items));
                        return true;
                    }
                }
            }
            return false;
        }

        function delete_item(key) {
            var items = $.parseJSON(sessionStorage.getItem('defective_storage_input_output_items'));
            items.splice(key, 1);
            sessionStorage.setItem('defective_storage_input_output_items', JSON.stringify(items));
            draw_table()
        }

        function set_data_for_edit(key, amount) {
            $('#edit_amount').val(amount);
            $('#edit_key').val(key);
        }

        function save_amount() {
            var amount = $('#edit_amount').val();
            var key = $('#edit_key').val();
            var items = $.parseJSON(sessionStorage.getItem('defective_storage_input_output_items'));
            if(amount>0){
                items[key]['amount']=amount;
                sessionStorage.setItem('defective_storage_input_output_items', JSON.stringify(items));
                draw_table();
                $('#close_modal').click();
            }else{
                alert('недопустимое значение!!');
            }
        }

        function save_items() {
            if (confirm('сохранить?')) {
                $('#save_in_out').hide();
                var items = sessionStorage.getItem('defective_storage_input_output_items');
                var desc = $('#description').val();
                var operation_id = $('#operation').val();
                var supplier_id = $('#supplier').val();
                var storage_id = $('#storage_id').val();
                console.log(supplier_id);
                if (items!=null){
                    $.ajax({
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        type:'POST',
                        url:"{{route('save_defective_inputs_outputs_items')}}",
                        data:{items:items, description:desc, operation_id:operation_id, supplier_id:supplier_id, storage_id:storage_id},
                        success: function (data) {
                            console.log($.parseJSON(data));
                            $('#save_in_out').show();
                            $('#description').val('');
                            clear_sessionStorage();
                            $('.alert-info').show();
                            $('.alert-info ul').empty().append('<li>сохранено</li>');
                        },
                        error: function (request, status, error) {
                            $('#loading').hide();
                            var json = $.parseJSON(request.responseText);
                            console.log(json);
                            $('.alert-info').show();
                            $.each(json.errors, function (index, value) {
                                $('.alert-info ul').empty().append('<li>'+value+'</li>');
                            })
                        }
                    })
                }else{
                    $('.alert-info').show();
                    $('.alert-info ul').empty().append('<li>внесите данные!</li>');
                }
            }
        }

        function allow_save() {
            var op = $('#operation').attr('data-operation');
            console.log(op);
            var items = $.parseJSON(sessionStorage.getItem('defective_storage_input_output_items'));
            var i = 0;
            if(op>0){
                for(i=0; i<items.length; i++){
                    if(items[i].amount>items[i].remaining_of_sender){
                        return false
                    }
                }
            }else{
                for(i=0; i<items.length; i++){
                    if(items[i].amount>items[i].remaining){
                        return false
                    }
                }
            }
            return true
        }
        //    defectivedefective
        function fetch_defectives_table() {
            var storage_id = $('#storage_id').val();
            $("#defectives_table").DataTable().destroy();
            var table = $('#defectives_table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    "url": "{{ route('getDefectivesForAccept') }}",
                    "type": "post",
                    "data":{storage_id:storage_id}
                },
                columns: [
                    { "width": "4%", "data": "id" },
                    { "width": "10%", "data": "applier" },
                    { "width": "8%", "data": "create_time" },
                    { "width": "15%", "data": "from_storage" },
                    { "width": "15%", "data": "description" },
                ],
                stateSave:true,
                order: [[ 2, "desc" ]],
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr('data-id', data.id);
                },
                pageLength: 100,
                lengthMenu:[[100, 500, 1000], [100, 500, 1000]],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение от _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "",
                    "infoFiltered": "(отфильтровано из _MAX_ записей)",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                drawCallback: function (data) {
                    this.api().columns([1,3]).every( function () {
                        var column = this;
                        var select = $('<select><option value="">--все--</option></select>')
                            .appendTo( $(column.footer()).empty() )
                            .on( 'change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val, true, false )
                                    .draw();
                            } );
                        if (data.json.filteredData.length != 0) {
                            for (var i=0; i<data.json.filteredData[column[0]].length; i++){
                                if (typeof data.json.filters[column[0]] !== "undefined") {
                                    select.append( '<option value="'+data.json.filteredData[column[0]][i]+'" selected>'+data.json.filteredData[column[0]][i]+'</option>' )
                                }else {
                                    select.append( '<option value="'+data.json.filteredData[column[0]][i]+'">'+data.json.filteredData[column[0]][i]+'</option>')
                                }
                            }
                        }

                    } );
                },
                scrollY: "50vh",
                scrollCollapse: true
            });

        }

        function show_defective_detail(defective_id) {
            $('.alert-danger').hide();
            $('#loading').show();
            $('input[name=defective_id_for_accept]').val(defective_id);
            $('#show_defective_detail').on('shown.bs.modal', function() {
                fetch_detail_table();
                $('#loading').hide();
            });
        }

        function fetch_detail_table() {
            var defective_id = $('#defective_id_for_accept').val();
            $('#detail_table').DataTable().destroy();
            var table = $('#detail_table').DataTable({
                ajax: {
                    "url": "{{ route('showDefectiveListItemsForAccept') }}",
                    "type": "get",
                    "data": {defective_id:defective_id},
                    "dataSrc": function (data) {
                        if (data['allowToAccept']) {
                            $('#accept_btn').show();
                        } else {
                            $('#accept_btn').show();
                        }
                        if (data['requestData'].state == 1){
                            $('#accepted').text('№'+defective_id).css('color', 'blue');
                        } else {
                            $('#accepted').text('№'+defective_id).css('color', 'red');
                        }
                        $(table.column(4).footer()).text(' итог: '+data['total']);
                        return data.data;
                    }
                },
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "_TOTAL_ записей",
                    "infoEmpty": "нет записей",
                    "infoFiltered": "(_MAX_)",
                    "search": "<i class='fa fa-search' style='float: left'></i>"
                },
                columns:[
                    {'data':'articula_new', 'width':'10%'},
                    {'data':'articula_old', 'width':'10%'},
                    {'data':'item_name', 'width':'30%'},
                    {'data':'sap_code', 'width':'10%'},
                    {'data':'amount', 'width':'6%'},
                    {'data':'remaining', 'width':'10%'},
                    {'data':'unit', 'width':'6%'}
                ],
                paging: false,
                bPaginate: false,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5, 6]
                        },
                        text: '<i class="fa fa-file-excel-o"></i>'

                    },
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5, 6]
                        },
                        text: '<i class="fa fa-file-excel-o"></i>'

                    }
                ],
                scrollY: "50vh",

            });
        }

        function accept_defective() {
            $('#accept_btn').attr('disabled', true).hide();
            var defective_id = $('#defective_id_for_accept').val();
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type: 'post',
                url: "{{route('accept_defective_items')}}",
                data: {defective_id:defective_id},
                success: function (data) {
                    $('#accept_btn').attr('disabled', false).hide();
                    $('#close_detail_modal').click();
                    fetch_detail_table();
                    fetch_defectives_table();
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    console.log(json)
                    $('.alert-danger').show();
                    $('.alert-danger ul').empty().append('<li>'+json.data+'</li>');
                }
            })
        }
    </script>
@endsection
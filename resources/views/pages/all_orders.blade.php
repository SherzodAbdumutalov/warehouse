@section('additional_css')
    <style>
        #orders_table tbody tr{
            cursor: pointer;
        }
        #all-orders-header-filters{
            display: flex;
            justify-content: space-between;
            flex-wrap: wrap;
        }
        #date-filter-header-all-orders{
            display: flex;
            width: 500px;
            justify-content: space-between;
        }
        tr.uncompleted-order-status td:nth-child(6){
            background-color: rgba(204,255,0,0.6)!important;
        }
        tr.null-order-status td:nth-child(6){
            background-color: rgba(255,0,0,0.2)!important;
        }

    </style>
@endsection
<div class="panel panel-primary" style="padding-bottom: 1px">
    <div class="panel-heading" style="padding: 1px 5px">
        Все заказы
    </div>
    <form action="{{ route('excelExport') }}" name="excelExportForm" id="excelExportForm" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{--<input type="hidden" name="order_id" id="order_id" value="">--}}
    </form>
    <div class="alert alert-danger" style="display:none">
        <ul></ul>
    </div>
    <div class="panel-body" style="overflow-x: auto">
        <div id="all-orders-header-filters">
            <a href="{{ route('create_order') }}" class="btn btn-info " style="font-size: 15px; float: left"><i class="fa fa-plus"></i> создать</a>
            <div class="col-md-2" id="date-filter-header-all-orders">
                <span>дата заказа</span><br>
                <span><i class="fa fa-calendar"></i> с:</span> <input type="date" value="" id="start" name="start_date" onkeydown="return false" style="font-size: 11px"/>

                <span class="ml-4"><i class="fa fa-calendar"></i> по:</span> <input type="date" value="" id="end" name="end_date" onkeydown="return false" style="font-size: 11px"/>
            </div>
            <div class="col-md-2">
                <select name="filter[]" id="filter_by_state" class="standardSelect multipleSelect" data-placeholder="статус (фильтр)" multiple>
                    @foreach($states as $item)
                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-1">
                <select name="filter[]" id="filter_by_user" data-placeholder="автор (фильтр)" class="standardSelect multipleSelect" multiple>
                    @foreach($users as $user)
                        <option value="{{ $user->id }}">{{ $user->firstname }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2" >
                <select name="filter[]" id="filter_by_supplier" class="standardSelect multipleSelect" data-placeholder="поставщики (фильтр)" multiple>
                    @foreach($suppliers as $supplier)
                        <option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-1" >
                <select name="filter_by_supplier_type" id="filter_by_supplier_type" class="standardSelect" data-placeholder="тип поставщика (фильтр)">
                    <option value="0">все типы</option>
                    <option value="2">импорт</option>
                    <option value="1">местный</option>
                </select>
            </div>
        </div>
        <table id="orders_table" class="table nowrap table-hover table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>№</th>
                <th>статус</th>
                <th>прогресс</th>
                <th>дата заказа</th>
                <th>заказчик</th>
                <th>поставщик</th>
                <th>тип пост.</th>
                <th>invoice заказа</th>
                <th>сумма заказа</th>
                <th>депозит</th>
                <th>перед отпр.</th>
                <th>дата оплаты</th>
                <th>примечание</th>
                <th>создано</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
            <tr>
                <th>№</th>
                <th>статус</th>
                <th>прогресс</th>
                <th>дата заказа</th>
                <th>заказчик</th>
                <th>поставщик</th>
                <th>тип пост.</th>
                <th>invoice заказа</th>
                <th>сумма заказа</th>
                <th>депозит</th>
                <th>перед отпр.</th>
                <th>дата оплаты</th>
                <th>примечание</th>
                <th>создано</th>
            </tr>
            </tfoot>
        </table>

    </div>


    <div id="show_detail" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" id="close_detail_modal"><span style="font-size: 24px">&times;</span></button>
                    <p id="header_text" style="display: block; color: black; margin: 0; padding: 0"></p>
                </div>
                <div class="modal-body" style="padding-top: 10px">
                    <div class="alert alert-info" style="display:none">
                        <ul>

                        </ul>
                    </div>
                    <form action="">
                        <input type="hidden" name="order_id" id="order_id" value="">
                        <div class="col-md-4" style="padding-left: 0;">
                            <select name="notContainedItems" id="notContainedItems" data-placeholder="выбрать деталь..." class="standardSelect">
                            </select>
                        </div>
                        <div class="col-md-1">
                            <input type="number" class="form-control" id="amount" name="amount" placeholder="кол-во" min="0" style="height: 28px">
                        </div>
                        <div class="col-md-1">
                            <input type="number" class="form-control" id="price" name="price" placeholder="цена" min="0" style="height: 28px">
                        </div>
                        <div class="col-md-1">
                            <button type="button" class="btn btn-info " id="btnAdd" onclick="add_new_item_to_order()"><i class="fa fa-plus"></i> добавить</button>
                        </div>
                    </form>
                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#edit_order">
                        изменить заказ
                    </button>
                    <table id="detail_table" class="nowrap table table-hover table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>артикул</th>
                            <th>SAP код</th>
                            <th>наименование</th>
                            <th>item name</th>
                            <th>кол-во</th>
                            <th>приход</th>
                            <th>ед. изм.</th>
                            <th>цена за ед.(без НДС)</th>
                            <th>сумма</th>
                            <th>валюта</th>
                            <th>Gross weight(kg)</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>артикул</th>
                            <th>SAP код</th>
                            <th>наименование</th>
                            <th>item name</th>
                            <th id="total_amount">кол-во</th>
                            <th>приход</th>
                            <th>ед. изм.</th>
                            <th>цена за ед.(без НДС)</th>
                            <th>сумма</th>
                            <th>валюта</th>
                            <th>Gross weight(kg)</th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="modal-footer">
                    {{--<button type="button" class="btn btn-danger" style="float: right" id="delete_order" onclick="delete_order_func()" disabled><i class="fa fa-trash"></i> отменит заказ</button>--}}
                    <button type="button" class="btn btn-success" style="float: right; margin-right: 5px" onclick="close_order_func()" id="close_order" disabled><i class="fa fa-close"></i> закрыть заказ</button>
                </div>
            </div>

        </div>
    </div>

    {{--edit amount--}}
    <div id="edit_amount_modal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm" style="width: 20%">
            <form id="update_item_amount" method="post">
                {{ csrf_field() }}
                <div class="modal-content" >
                    <div class="modal-header">
                        изменить
                        <button type="button" class="close" data-dismiss="modal" style="font-size: 24px" id="close_edit_amount_modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-warning" style="display: none">
                            <ul></ul>
                        </div>
                        <label for="edit_amount">кол-во</label>
                        <input type="number" name="edit_amount" id="edit_amount" class="form-control">
                        <label for="edit_price">цена</label>
                        <input type="number" name="edit_price" id="edit_price" class="form-control">
                        <input type="hidden" name="hidden_list_id" id="hidden_list_id" class="form-control">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-warning " onclick="update_order_list()"><i class="fa fa-save"></i> Сохранить</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div id="edit_order" class="modal fade" role="dialog">
        <div class="modal-dialog"  style="width: 30%;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <p style="color: #000; font-size: 12px; display: inline-block; padding: 0; margin: 0" id="orderInfoHeader"></p>
                    <button type="button" class="close" data-dismiss="modal" id="close_edit_order_modal"><span style="font-size: 24px; padding: 0; margin: 0">&times;</span></button>
                </div>
                <div class="alert alert-success" style="display:none; color: black">
                    <ul>

                    </ul>
                </div>
                <form action="" method="post">
                    <div class="modal-body" style="height: 50vh;">
                        <input type="hidden" name="edit_order_id" id="edit_order_id">
                        <label for="edit_invoice_number">invoice number</label>
                        <input type="text" name="edit_invoice_number" id="edit_invoice_number" class="form-control" >
                        <label for="shipment_time">Дата заказа</label>
                        <input type="date" name="shipment_time" id="shipment_time" class="form-control" >
                        {{--<label for="container_info">контейнер</label>--}}
                        {{--<input type="text" name="container_info" id="container_info" class="form-control" >--}}
                        {{--<label for="deposit">депозит</label>--}}
                        {{--<input type="number" name="deposit" id="deposit" class="form-control" >--}}
                        {{--<label for="before_shipment">перед отправкой</label>--}}
                        {{--<input type="number" name="before_shipment" id="before_shipment" class="form-control" >--}}
                        {{--<div class="col-md-12 clearfix" style="padding-left: 1px; padding-right: 1px">--}}
                            {{--<div class="col-md-4">--}}
                                {{--<label for="due_time" style="margin-bottom: 0">время оплаты</label>--}}
                                {{--<input type="date" name="due_time" id="due_time" class="form-control" >--}}
                            {{--</div>--}}
                            {{--<div class="col-md-8">--}}
                                {{--<label for="currency" style="margin-bottom: 0">валюта</label>--}}
                                {{--<select name="currency" id="currency" class="standardSelect">--}}
                                    {{--@foreach($currencies as $currency)--}}
                                        {{--<option value="{{ $currency->id }}">{{ $currency->name }}</option>--}}
                                    {{--@endforeach--}}
                                {{--</select>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="col-md-12">
                            <label for="description">Примечание</label>
                            <input type="text" name="description" id="description" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" onclick="update_order()"><i class="fa fa-save"></i> сохранить</button>
                    </div>
                </form>
            </div>

        </div>
    </div>

</div>
@section('additionalLibrary')
    <script>
        $(document).ready(function() {
            var now = new Date();

            var day = ("0" + now.getDate()).slice(-2);
            var month = ("0" + (now.getMonth() + 1)).slice(-2);

            var today = now.getFullYear()+"-"+(month)+"-"+(day);
            // $('#start').val(today);
            $('#end').val(today);
            $('#loading').hide();
            fetch_data();

            var url_text = window.location.href;
            var url = new URL(url_text);
            var id = url.searchParams.get('id');
            var oTable = $('#orders_table').dataTable();
            if (id!=null){
                oTable.fnFilter( id );
            }else {
                oTable.fnFilter('');
            }

            $('#orders_table').on('dblclick', 'tr', function (e) {
                var id = $(this).data('id');
                $('#show_detail').modal("show");
                show_detail(id);
            });

            $('#orders_table tbody').on( 'click', 'tr', function () {
                $(this).removeClass('selected');

                if ( $(this).hasClass('selected') ) {
                    $(this).removeClass('selected');
                }
                else {
                    $('#orders_table').DataTable().$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            } );
        } );

        $('.standardSelect').on('change',function () {
            $('#orders_table').DataTable().destroy();
            fetch_data();
        });

        $('#start').on('change',function () {
            $('#orders_table').DataTable().destroy();
            fetch_data();
        });

        $('#end').on('change',function () {
            $('#orders_table').DataTable().destroy();
            fetch_data();
        });

        function fetch_data() {
            var start_date = $('#start').val();
            var end_date = $('#end').val();
            var filterArr = {};
            var filterText = '';
            console.log(start_date);
            console.log(end_date);
            $('.multipleSelect').each(function (index, value) {
                var parentKey = $(this).attr('id');
                filterArr[parentKey] = [];
                console.log($(this).val())
                if ($(this).val()!=null) {
                    $.each($(this).val(), function (i, val) {
                        filterArr[parentKey].push(val);
                    });
                }
                if ($(this).find('option:selected').text()!='') {
                    filterText += $(this).data('placeholder')+':'+$(this).find('option:selected').text()+', ';
                }
            });
            var filter_by_type = $('#filter_by_supplier_type').val();
            $('#orders_table').DataTable().destroy();
            var table = $('#orders_table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "{{ route('all_orders')}}",
                    type: "post",
                    data: {filterArr: JSON.stringify(filterArr), start_date:start_date, end_date:end_date, filter_by_type:filter_by_type}
                },
                stateSave: true,
                pageLength:100,
                lengthMenu:[[100, 500, 1000], [100, 500, 1000]],
                "columns": [
                    { "data":"id" },
                    { "data":"state_name" },
                    { "data":"percent" },
                    { "data":"shipment_time" },
                    { "data":"user_name" },
                    { "data":"storage_name"},
                    { "data":"type"},
                    { "data":"invoice_number" },
                    { "data":"total_price" },
                    { "data":"deposit" },
                    { "data":"before_shipment" },
                    { "data":"due_time" },
                    { "data":"description" },
                    { "data":"create_time" }
                ],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение от _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "нет записей",
                    "infoFiltered": "(<b>отфильтровано из _MAX_ записей</b>)",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr('data-id', data.id);
                },
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>'
                    },
                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>'
                    },
                ],
                order: [[ 0, "desc" ]],
                scrollY: "70vh",
                scrollX: true
            });
        }

        function fetch_detail_table() {
            $('#detail_table').DataTable().destroy();
            var order_id = $('#order_id').val();
            var table = $('#detail_table').DataTable({
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    "url": "{{ route('showOrderList') }}",
                    "type": "get",
                    "data": {order_id:order_id},
                    "dataSrc": function (data) {
                        $('#header_text').html('<b>№: </b>'+data['order'].order_id+' <b>поставщик: </b>'+data['order'].supplier_name+' <b>invoice: </b>'+data['order'].invoice_number);
                        if (data['data'].length == 0) {
                            $('#delete_order').hide();
                            $('#close_order').hide();
                            $('#notContainedItems').hide().prop('disabled', true).trigger('chosen:updated');
                            $('#amount').hide().prop('disabled', true);
                            $('#price').hide().prop('disabled', true);
                            $('#btnAdd').hide().prop('disabled', true);
                        }else{
                            if (!data['hasPerm']) {
                                $('#delete_order').hide();
                                $('#close_order').hide();
                                $('#notContainedItems').hide().prop('disabled', true).trigger('chosen:updated');
                                $('#amount').hide().prop('disabled', true);
                                $('#price').hide().prop('disabled', true);
                                $('#btnAdd').hide().prop('disabled', true);

                            }else{
                                if (data['order'].orderState == 1){
                                    $('#closed').text('заказ открыт').css('color', 'red');
                                    $('#delete_order').show().attr('disabled', false);
                                    $('#close_order').show().attr('disabled', false);
                                    $('#notContainedItems').prop('disabled', false).trigger('chosen:updated');
                                    $('#amount').show().prop('disabled', false);
                                    $('#price').show().prop('disabled', false);
                                    $('#btnAdd').show().prop('disabled', false);

                                } else if(data['order'].orderState == 2) {
                                    $('#closed').text('заявка закрыт').css('color', 'blue');
                                    $('#delete_order').hide();
                                    $('#close_order').hide();
                                    $('#notContainedItems').hide().prop('disabled', true).trigger('chosen:updated');
                                    $('#amount').hide().prop('disabled', true);
                                    $('#price').hide().prop('disabled', true);
                                    $('#btnAdd').hide().prop('disabled', true);

                                }
                            }
                        }

                        $('#loading').hide();
                        $(table.column(4).footer()).text(' итог: '+data['total']);
                        $(table.column(8).footer()).text(' итог: '+data['total_price']);
                        return data['data'];
                    }
                },
                columns:[
                    {'data':'articula_new'},
                    {'data':'sap_code'},
                    {'data':'item_name'},
                    {'data':'english_name'},
                    {'data':'amount'},
                    {'data':'complete'},
                    {'data':'unit'},
                    {'data':'price_per_unit'},
                    {'data':'total_price'},
                    {'data':'currency'},
                    {'data': 'gross_weight'},
                    {'data':'buttons'},

                ],
                paging: false,
                bPaginate: false,
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "_TOTAL_ записей",
                    "infoEmpty": "нет записей",
                    "infoFiltered": "(<b>отфильтровано из _MAX_ записей</b>)",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                        },
                        title:'order list items',
                        messageTop: 'все элементы заказа',
                        text: '<i class="fa fa-file-excel-o"></i>'

                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8]
                        },
                        title:'order list items',
                        messageTop: 'все элементы заказа',
                        text: '<i class="fa fa-file-pdf-o"></i>'

                    },
                ],
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr('data-id', data.id);

                    if(data['complete'] === data['amount']){
                        $(row).attr("class", "completed-order-status")
                    }
                    else if(data['complete']){
                        $(row).attr("class", "uncompleted-order-status")
                    }
                    else {
                        $(row).attr("class", "null-order-status")
                    }

                },
                scrollY: "50vh"
            });
        }

        function show_detail(order_id) {
            $('#order_id').val(order_id);
            $('#loading').show();
            $('.alert-info').hide();
        }

        $('#show_detail').on('shown.bs.modal', function() {
            fetch_detail_table();
            getNotConsistedItems();
        });

        let amountAccapted=null;

        function set_list_data(list_id) {
            $('#hidden_list_id').val(list_id);
            $.ajax({
                url: "{{ route('edit_order_list') }}",
                type: "get",
                data: { order_list_id:list_id},
                success: function (data) {
                    console.log(JSON.parse(data))
                    amountAccapted = JSON.parse(data).amount
                    $('#edit_amount').val(JSON.parse(data).amount);
                    $('#edit_price').val(JSON.parse(data).price);
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    console.log(json)
                    $('.alert-warning').show();
                    $.each(json.errors, function (index, value) {
                        $('.alert-warning ul').empty().append('<li>'+value+'</li>');
                    })
                }
            });
        }

        function add_new_item_to_order() {
            var order_id = $('#order_id').val();
            var item_id = $('#notContainedItems').val();
            var amount = $('#amount').val();
            var price = $('#price').val();
            if (item_id != 0 && amount != '' && amount > 0){
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "{{ route('add_item_to_exist_order') }}",
                    type: "post",
                    data: { order_id:order_id, item_id:item_id, amount:amount, price_per_unit:price},
                    success: function (data) {
                        getNotConsistedItems();
                        fetch_detail_table();
                        $('.alert-info').hide();
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        $('#close_edit_amount_modal').click()
                        console.log(json)
                        $('.alert-info').show();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').empty().append('<li>'+value+'</li>');
                        })
                    }
                })
            } else {
                $('.alert-info').show();
                $('.alert-info ul').empty().append('<li>выберите деталь и заполните кол-во или недопустимое значение!</li>');
            }
        }

        function update_order_list() {
            event.preventDefault();
            var list_id = $('#hidden_list_id').val();
            var amount = $('#edit_amount').val();
            var price = $('#edit_price').val();
            if (amount>0 && price>=0 && amount >= amountAccapted) {
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'POST',
                    url:"{{route('updateOrderList')}}",
                    data:{amount:amount, order_list_id:list_id, price:price},
                    success: function (order_id) {
                        fetch_data();
                        fetch_detail_table();
                        $('#close_edit_amount_modal').click();
                        $('.alert-info').show();
                        $('.alert-info').empty().append('сохранено');
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        console.log(json);
                        $('#close_edit_amount_modal').click()
                        console.log(json);
                        $('.alert-info').show();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info').empty().append(value);
                        })
                    }
                })
            }else{
                alert('недопустимое значение!')
            }

        }

        function delete_item(order_list_id){
            console.log(order_list_id)
            if (confirm('удалить?')){
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'POST',
                    url:"{{route('delete_order_list_item')}}",
                    data:{order_list_id:order_list_id},
                    success: function (order_id) {
                        fetch_detail_table();
                        fetch_data();
                        $('.alert-info').show();
                        $('.alert-info').empty().append('удален');
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        console.log(json)
                        $('.alert-info').show();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info').empty().append(value);
                        })
                    }
                })
            }
        }

        function update_order() {
            event.preventDefault()
            var order_id = $('#order_id').val();
            console.log(order_id)
            var invoice_number = $('#edit_invoice_number').val();
            console.log(invoice_number)
            var shipment_time = $('#shipment_time').val();
            // var delivery_time = $('#delivery_time').val();
            // var container_info = $('#container_info').val();
            var description = $('#description').val();
            // var deposit = $('#deposit').val();
            // var before_shipment = $('#before_shipment').val();
            // var due_time = $('#due_time').val();
            // var currency = $('#currency').val();
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type:'POST',
                url:"{{route('updateOrder')}}",
                data:{order_id:order_id, edit_invoice_number:invoice_number, description:description, shipment_time:shipment_time},
                success: function (data) {
                    fetch_data();
                    $('#close_edit_order_modal').click();
                    $('.alert-info').show().empty().append('сохранено');
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    $('.alert-success').show();
                    $.each(json.errors, function (index, value) {
                        $('.alert-success').empty().append(value);
                    })
                }
            })
        }

        $('#edit_order').on('shown.bs.modal', function () {
            set_order_value();
        });

        function set_order_value() {
            var order_id = $('#order_id').val();
            $.ajax({
                url:"{{ route('editOrder') }}",
                type:"get",
                data:{order_id:order_id},
                success:function (data) {
                    var item = JSON.parse(data);
                    console.log(item.shipment_time)
                    $('#orderInfoHeader').text('№:'+item.id);
                    $('#edit_invoice_number').val(item.invoice_number);
                    $('#shipment_time').val(item.shipment_time.substring(0, 10));
                    // $('#delivery_time').val(item.delivery_time);
                    // $('#container_info').val(item.container_info);
                    // $('#deposit').val(item.deposit);
                    // $('#description').val(item.description);
                    // $('#edit_order_id').val(item.id);
                    // $('#before_shipment').val(item.before_shipment);
                    // if (item.due_time != null) {
                    //     $('#due_time').val(item.due_time);
                    // }else{
                    //     var date = new Date();
                    //     $('#due_time').val(date.toISOString().substr(0, 10));
                    // }
                    // var date1 = new Date(item.create_time);
                    // $('#due_time').attr('min', date1.toISOString().substr(0, 10));
                    // $('#currency').val(item.currency_id).trigger("chosen:updated");
                },
                error:function () {

                }
            });

        }

        function delete_order_func(){
            if (confirm('удалить?')){
                var order_id = $('#order_id').val();
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'POST',
                    url:"{{route('delete_order')}}",
                    data:{order_id:order_id},
                    success: function (order) {
                        fetch_data();
                        $('#close_detail_modal').click();
                        $('.alert-info').show();
                        $('.alert-info ul').empty().append('удален');
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        console.log(json)
                        $('.alert-info').show();
                        $('.alert-info ul').empty()
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').append('<li>'+value+'</li>');
                        })
                    }
                })
            }
        }

        function close_order_func(){
            if (confirm('закрыт')){
                var order_id = $('#order_id').val()
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'POST',
                    url:"{{route('close_order')}}",
                    data:{order_id:order_id},
                    success: function (order) {
                        fetch_data();
                        $('#close_detail_modal').click();
                        $('.alert-info').show().empty().append('закрыт');
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        console.log(json)
                        $('.alert-info').show();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info').empty().append(value);
                        })
                    }
                })
            }
        }

        function getNotConsistedItems(){
            var order_id = $('#order_id').val();
            if (order_id != null){
                $('#notContainedItems').append('<option value="" >выбрать...</option>');
                $.ajax({
                    url: "{{ route('getNotConsistedItems') }}",
                    type: "get",
                    data: {order_id:order_id},
                    success: function(data){
                        $('#notContainedItems').empty().trigger("chosen:updated");
                        var items = JSON.parse(data);
                        if (items.length>0) {
                            $.each(items, function (index, subItemObj) {
                                $('#notContainedItems').append('<option value="'+subItemObj.id+'">'+subItemObj.articula_new+' '+subItemObj.item_name+'</option>')
                            });
                        }
                        $('#notContainedItems').trigger("chosen:updated");
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        console.log(json)
                        $('.alert-info').show();
                        $('.alert-info ul').empty()
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').append('<li>'+value+'</li>');
                        })
                    }
                })
            }else{
                $('#notContainedItems').empty();
                $('#notContainedItems').trigger("chosen:updated");
            }
        }

        function isExist(item_id, items){
            if(items!=null){
                for (var i=0; i<items.length; i++){
                    if(item_id==items[i].item_id){
                        return true
                    }
                }
            }
            return false
        }

        function excelExport(order_id) {
            $('#order_id').val(order_id);
            $('#excelExportForm').submit();
        }
    </script>
@endsection

@section('additional_css')
    <style>

    </style>
@endsection
<div class="panel panel-primary">
    <div class="panel-heading" style="padding: 1px 5px">
        Сборочные линии
    </div>

    <div class="panel-body">
        <div class="alert alert-danger" style="display:none">

        </div>
        <button type="button" class="btn btn-info " data-toggle="modal" style="font-size: 12px" data-target="#show_create_modal">добавить</button>
        <table id="production_lines_table" class="table table-hover table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>№</th>
                <th>название</th>
                <th>тип</th>
                <th>детали</th>
                <th>изм.</th>
                <th>удал.</th>
            </tr>
            </thead>
            <tbody>
            @foreach($all_lines as $key=>$line)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td><p style="font-weight: bold; color: black">{{ $line->name }}</p></td>
                    <td><p style="font-weight: bold; color: black">{{ $line->type_name }}</p></td>
                    <td><button type="button" class="btn btn-info " data-toggle="modal" style="font-size: 12px" data-target="#show_detail" onclick="show_detail({{ $line->id }})">детали</button></td>
                    <td><button type="button" class="btn btn-warning " data-toggle="modal" style="font-size: 12px" data-target="#show_edit_modal{{ $line->id }}">изменить</button></td>
                    <td>
                        <form action="{{ route('delete_line', ['id'=>$line->id]) }}" method="post">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger " style="font-size: 12px" onclick="return confirm('удалить?')">удалить</button>
                        </form>
                    </td>
                </tr>

                {{--edit supplier--}}
                <div id="show_edit_modal{{ $line->id }}" class="modal fade" role="dialog">
                    <div class="modal-dialog" style="width: 50%">
                        <form  action="{{ route('update_line') }}" method="post">
                            {{ csrf_field() }}
                            <div class="modal-content" >
                                <div class="modal-header">
                                    изменить
                                    <button type="button" class="close" data-dismiss="modal" style="font-size: 24px">&times;</button>
                                </div>
                                <div class="modal-body" style="height: 400px">
                                    <label for="edit_name">название</label>
                                    <input type="text" name="edit_name" id="edit_name" class="form-control" value="{{ $line->name }}">
                                    <input type="hidden" name="edit_line_id" id="edit_line_id" value="{{ $line->id }}">
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-warning ">Сохранить</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            @endforeach
            </tbody>
        </table>
    </div>


    <div id="show_detail" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <p id="selected_row_name" style="display: inline-block"></p>
                    <button type="button" class="close" data-dismiss="modal" id="close_detail_modal"><span style="font-size: 24px">&times;</span></button>
                </div>
                <div class="modal-body" style="min-height: 500px">
                    <div class="alert alert-info" style="display:none">
                        <ul>

                        </ul>
                    </div>
                    {{--<button type="button" class="btn btn-danger " onclick="delete_all_items()">удалить всё</button>--}}
                    {{--<br>--}}
                    {{--<br>--}}

                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#byHand">ручная привязка</a></li>
                        <li><a data-toggle="tab" href="#byFile">привязка по файлу</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="byHand" class="tab-pane  fade in active" style="padding-top: 10px">
                            <div class="col-md-11">
                                <select name="items" id="items" class="standardSelect" multiple data-placeholder="добавит элемент">
                                </select>
                            </div>
                            <div class="col-md-1">
                                <input type="hidden" name="hidden_line_id" id="hidden_line_id">
                                <button type="button" class="btn btn-warning " onclick="create_item()">добавить</button>
                            </div>
                        </div>
                        <div id="byFile" class="tab-pane  fade" style="padding-top: 10px">
                            <div class="col-md-12">
                                <form action="" id="import_line_item_file">
                                    {{ csrf_field() }}
                                    <div class="col-md-6">
                                        <input type="hidden" name="file_line_id" id="file_line_id">
                                        <label for="line_item_file">file</label>
                                        <input type="file" name="line_item_file" id="line_item_file" class="form-control">
                                    </div>
                                    <div class="col-md-1">
                                        <button class="btn btn-warning" type="submit" style="margin-top: 30px; padding: 3px 15px">добавить</button>
                                    </div>
                                    <div class="col-md-1" style="padding-top: 15px">
                                        <a href="{{ asset('assets') }}/template/postovshik.xlsx" download style="font-size: 36px;"><span class="glyphicon glyphicon-download">&nbsp;шаблон</span></a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <table id="detail_table" class="table table-hover table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>нов. арт</th>
                            <th>стр арт</th>
                            <th>SAP код</th>
                            <th>наименование</th>
                            <th>удал</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>

{{--edit supplier--}}
<div id="show_create_modal" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 50%">
        <form action="{{ route('create_line') }}" method="post">
            {{ csrf_field() }}
            <div class="modal-content" >
                <div class="modal-header">
                    создать
                    <button type="button" class="close" data-dismiss="modal" style="font-size: 24px">&times;</button>
                </div>
                <div class="modal-body" style="min-height: 100px">
                    <label for="name">название</label>
                    <input type="text" name="name" id="name" class="form-control" required>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-warning ">Сохранить</button>
                </div>
            </div>
        </form>
    </div>
</div>
@section('additionalLibrary')
    <script>
        $(document).ready(function() {
            $('#loading').hide()
            $('#production_lines_table').DataTable({
                columnDefs:[
                    { orderable: false, targets: [3, 4, 5] }
                ],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "_PAGE_ - ая страница из _PAGES_",
                    "infoEmpty": "",
                    "infoFiltered": "((<b>отфильтровано из _MAX_ записей</b>))",
                    "search": "<i class='fa fa-search' style='float: left'></i>"
                },
                "columns": [
                    { "width": "4%" },
                    { "width": "65%" },
                    { "width": "10%" },
                    { "width": "7%" },
                    { "width": "7%" },
                    { "width": "7%" }
                ]
            });

            // get_all_requests()

            $('#detail_table').DataTable({
                columnDefs:[
                    { orderable: false, targets: [4] }
                ],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "_PAGE_ - ая страница из _PAGES_",
                    "infoEmpty": "",
                    "infoFiltered": "((<b>отфильтровано из _MAX_ записей</b>))",
                    "search": "<i class='fa fa-search' style='float: left'></i>"
                },
                bInfo: false,
                paging: false,
                bPaginate: false,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3]
                        },
                        title:'order list items',
                        messageTop: 'все элементы заказа'

                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3]
                        },
                        title:'order list items',
                        messageTop: 'все элементы заказа'
                    },

                ],
                columns: [
                    { "width": "15%" },
                    { "width": "15%" },
                    { "width": "10%" },
                    { "width": "50%" },
                    { "width": "10%" }
                ]
            });
        } );

        function get_items(line_id) {
            $.ajax({
                url: '{{ route('get_production_line_items') }}',
                type: 'get',
                data: {line_id:line_id},
                success: function (data) {
                    var items = $.parseJSON(data)
                    $('#items').empty()
                    items.forEach(function (value, key) {
                        $('#items').append('<option value="'+value.id+'">'+value.articula_new+' '+value.articula_old+' '+value.item_name+'</option>')
                    })
                    $('#items').trigger("chosen:updated");
                },
                error: function (request, status, error) {
                    console.log(request)
                    if (request!="undefined"){
                        var json = $.parseJSON(request.responseText)
                        $('.alert-info').show()
                        $('.alert-info ul').empty()
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').append('<li>'+value+'</li>')
                        })
                    }
                }
            })
        }

        function show_detail(line_id) {
            $('#hidden_line_id').val(line_id)
            $('#file_line_id').val(line_id)

            $('#loading').show()
            $('.alert-info').hide()
            $('#detail_table').DataTable().clear().draw(false)

            get_items(line_id)
            $.ajax({
                type:'get',
                url:"{{route('show_production_line_items_list')}}",
                data:{line_id:line_id},
                success: function (data) {
                    var items = $.parseJSON(data)['all_items']
                    $('#selected_row_name').html($.parseJSON(data)['line']['name'])
                    draw_table(items)
                    $('#loading').hide()
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText)
                    $('.alert-info').show()
                    $('.alert-info ul').empty()
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').append('<li>'+value+'</li>')
                    })
                }
            })
        }

        function draw_table(items){
            items.forEach(function (data) {
                var trow = $('#detail_table').DataTable().row.add([
                    data.articula_new,
                    data.articula_old,
                    data.sap_code,
                    data.item_name,
                    '<button type="button" class="btn btn-danger " onclick="delete_list_item('+data.id+')">удалить</button>'
                ]).node()
                trow.id = 'row_'+data.id
                $('#detail_table').DataTable().draw(false)
            })
        }

        function create_item() {
            var line_id = $('#hidden_line_id').val()
            var items = $('#items').val()
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type:'POST',
                url:"{{route('add_production_line_item_list')}}",
                data:{line_id:line_id, items:JSON.stringify(items)},
                success: function (data) {
                    show_detail(line_id)
                    $('.alert-info').show();
                    $('.alert-info').empty().append('сохранено');
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    $('.alert-info').show();
                    $('.alert-info ul').empty();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').append('<li>'+value+'</li>');
                    })
                }
            })
        }

        function delete_list_item(list_item_id){
            var line_id = $('#hidden_line_id').val()
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type:'post',
                url:"{{route('delete_production_list_item')}}",
                data:{list_item_id:list_item_id},
                success: function (data) {
                    show_detail(line_id)
                    $('.alert-info').show();
                    $('.alert-info').empty().append('удален');
                },
                error: function (request, status, error) {
                    if (request!="undefined") {
                        var json = $.parseJSON(request.responseText);
                        $('.alert-info').show();
                        $('.alert-info ul').empty();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').append('<li>'+value+'</li>');
                        })
                    }
                }
            })
        }

        $('#import_line_item_file').on('submit', function(event){
            $('#loading').show()
            event.preventDefault();
            $.ajax({
                url:"{{ route('upload_production_line_items_file') }}",
                method:"POST",
                data:new FormData(this),
                dataType:'JSON',
                contentType: false,
                cache: false,
                processData: false,
                success:function(data)
                {
                    var incorrect_articuls = data
                    var line_id = $('#hidden_line_id').val()
                    show_detail(line_id)
                    document.getElementById("import_line_item_file").reset()
                    if (incorrect_articuls.length>0){
                        incorrect_articuls.forEach(function (value) {
                            $('#detail_table').DataTable().row.add([
                                value,
                                null,
                                null,
                                null,
                                null
                            ])
                            $('#detail_table').DataTable().draw(false)
                        })
                    }
                },
                error: function (request) {
                    $('#loading').hide()
                    var json = $.parseJSON(request.responseText)
                    $('.alert-info').show()
                    $('.alert-info ul').empty()
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').append('<li>'+value+'</li>')
                    })
                }
            })
        });

        function delete_all_items() {
            var line_id = $('#hidden_line_id').val()
            if (confirm('удалить всё?')) {
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'post',
                    url: '{{ route('delete_all_line_items') }}',
                    data:{line_id:line_id},
                    success: function (data) {
                        show_detail(line_id)
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        console.log(request)
                        $('.alert-info').show();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').empty().append('<li>'+value+'</li>');
                        })
                    }
                })
            }
        }
    </script>
@endsection
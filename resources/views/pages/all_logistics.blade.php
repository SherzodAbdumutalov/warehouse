@section('additional_css')
    <style>
        .proc p {
            overflow: hidden;
            text-overflow: ellipsis;
            max-width: 420px;
            color: black;
        }
        .progress{
            margin-bottom: 5px;
            background-color: #efe1e1;
        }

        table tbody td {
            min-width: 130px;
        }

        button{
            margin-top: 5px;
        }
        #logistics_table tbody tr{
            cursor: pointer;
        }
        tr.uncompleted-logistics-status td:nth-child(7){
            background-color: rgba(204,255,0,0.6)!important;
        }
        tr.null-logistics-status td:nth-child(7){
            background-color: rgba(255,0,0,0.2)!important;
        }
    </style>
@endsection
<form action="{{ route('excelExport') }}" name="excelExportForm" id="excelExportForm" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="order_id" id="order_id" value="">
</form>
<div class="panel panel-primary" style="padding-bottom: 1px">
    <div class="panel-heading" style="padding: 1px 5px">
        Вся логистика
    </div>
    <div class="alert alert-danger" style="display:none">

    </div>
    <div class="panel-body" style="overflow-x: auto">
        <div class="col-md-12 clearfix" style="padding-left: 0">
            <a href="{{ route('createLogistics') }}" class="btn btn-info" style="font-size: 12px; float: left"><i class="fa fa-plus"></i> создать</a>

            <div class="col-md-2" style="float: right" id="statusDiv">
                <select name="stateFilter" id="stateFilter" class="standardSelect" data-placeholder="статус (фильтр)">
                    <option value="0">статус фильтр</option>
                    @foreach($states as $key=>$item)
                        <option value="{{ $key }}">{{ $item }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2" style="float: right">
                <select name="filter[]" id="filter_by_user" data-placeholder="автор (фильтр)" class=" multipleChosen" multiple>
                    @foreach($users as $user)
                        <option value="{{ $user->id }}">{{ $user->firstname }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2" style="float: right; display: none" id="storageDiv">
                <select name="filter[]" id="filter_by_to_storage" class=" multipleChosen" data-placeholder="на склад (фильтр)" multiple>
                    @foreach($toStorages as $storage)
                        <option value="{{ $storage->id }}">{{ $storage->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2" style="float: right; display: none" id="supplierDiv">
                <select name="filter_by_type" id="filter_by_type" class="standardSelect" data-placeholder="на склад (фильтр)">
                    <option value="2">Импорт поставщик</option>
                    <option value="1">Местный поставщик</option>
                </select>
            </div>
            <div class="col-md-2" style="float: right">
                <select name="filter[]" id="filter_by_from_storage" class=" multipleChosen" data-placeholder="со склада (фильтр)" multiple>
                    @foreach($fromStorages as $storage)
                        <option value="{{ $storage->id }}">{{ $storage->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-3" style="float: right; display: none; text-align: right" id="allLogDiv">
                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#l_collapseExample" aria-expanded="false" aria-controls="l_collapseExample">
                    дата фильтр
                </button>
                <div class="collapse" id="l_collapseExample">
                    <div class="card card-body" style="position: relative; text-align: left">
                        <span>дата отгрузки</span><br>
                        <span><i class="fa fa-calendar"></i> от:</span> <input type="date" value="" id="l_upload_start" name="l_upload_start_date" onkeydown="return false" style="font-size: 11px"/>
                        <span><i class="fa fa-calendar"></i> до:</span> <input type="date" value="" id="l_upload_end" name="l_upload_end_date" onkeydown="return false" style="font-size: 11px"/>
                        <br><span>дата заказа</span><br>
                        <span><i class="fa fa-calendar"></i> от:</span> <input type="date" value="" id="l_order_start" name="l_order_start_date" onkeydown="return false" style="font-size: 11px"/>
                        <span><i class="fa fa-calendar"></i> до:</span> <input type="date" value="" id="l_order_end" name="l_order_end_date" onkeydown="return false" style="font-size: 11px"/>
                        <button class="btn btn-danger" type="button" style="float: right" id="l_resetDateFilter">сброс</button>
                    </div>
                </div>
            </div>

            {{--<div class="col-md-3" style="float: right; display: none; text-align: right" id="historyDiv">--}}
                {{--<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">--}}
                    {{--дата фильтр--}}
                {{--</button>--}}
                {{--<div class="collapse" id="collapseExample">--}}
                    {{--<div class="card card-body" style="position: relative; text-align: left">--}}
                        {{--<span>дата отгрузки</span><br>--}}
                        {{--<span><i class="fa fa-calendar"></i> от:</span> <input type="date" value="" id="upload_start" name="upload_start_date" onkeydown="return false" style="font-size: 11px"/>--}}
                        {{--<span><i class="fa fa-calendar"></i> до:</span> <input type="date" value="" id="upload_end" name="upload_end_date" onkeydown="return false" style="font-size: 11px"/>--}}
                        {{--<br><span>дата прихода</span><br>--}}
                        {{--<span><i class="fa fa-calendar"></i> от:</span> <input type="date" value="" id="input_start" name="input_start_date" onkeydown="return false" style="font-size: 11px"/>--}}
                        {{--<span><i class="fa fa-calendar"></i> до:</span> <input type="date" value="" id="input_end" name="input_end_date" onkeydown="return false" style="font-size: 11px"/>--}}
                        {{--<br><span>дата заказа</span><br>--}}
                        {{--<span><i class="fa fa-calendar"></i> от:</span> <input type="date" value="" id="order_start" name="order_start_date" onkeydown="return false" style="font-size: 11px"/>--}}
                        {{--<span><i class="fa fa-calendar"></i> до:</span> <input type="date" value="" id="order_end" name="order_end_date" onkeydown="return false" style="font-size: 11px"/>--}}
                        {{--<button class="btn btn-danger" type="button" style="float: right" id="resetDateFilter">сброс</button>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>

        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#logistics">Логистика</a></li>
            <li><a data-toggle="tab" href="#onTheWay">В пути</a></li>
            {{--<li><a data-toggle="tab" href="#history">История заказов</a></li>--}}
        </ul>

        <div class="tab-content">
            <div id="logistics" class="tab-pane fade in active">
                <table id="logistics_table" class="table table-hover table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>статус</th>
                        <th>прогресс</th>
                        <th>дата отгрузки</th>
                        <th>дата заказа</th>
                        <th>отправитель</th>
                        <th>получатель</th>
                        <th>invoice логист.</th>
                        <th>invoice заказа</th>
                        <th>№ транспорта</th>
                        <th>тип транспорта</th>
                        <th>автор</th>
                        <th>создано</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>id</th>
                        <th>статус</th>
                        <th>прогресс</th>
                        <th>дата отгрузки</th>
                        <th>дата заказа</th>
                        <th>отправитель</th>
                        <th>получатель</th>
                        <th>invoice логист.</th>
                        <th>invoice заказа</th>
                        <th>№ транспорта</th>
                        <th>тип транспорта</th>
                        <th>автор</th>
                        <th>создано</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <div id="onTheWay" class="tab-pane fade">
                <table id="onTheWay_table" class="table table-hover table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>артикул</th>
                        <th>наименования</th>
                        <th>SAP</th>
                        <th>поставщик</th>
                        <th>invoce зак.</th>
                        <th>ед. изм.</th>
                        <th>кол-во в зак.</th>
                        <th>в пути</th>
                        <th>прибыло</th>
                        <th>ост. пост-ка</th>
                        <th>дата заказа</th>
                        <th>дата отгрузки</th>
                        <th>автор</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>артикул</th>
                        <th>наименования</th>
                        <th>SAP</th>
                        <th>поставщик</th>
                        <th>invoice заказа</th>
                        <th>ед. изм.</th>
                        <th>кол-во в зак</th>
                        <th>в пути</th>
                        <th>прибыло</th>
                        <th>ост. пост-ка</th>
                        <th>дата заказа</th>
                        <th>дата отгрузки</th>
                        <th>автор</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            {{--<div id="history" class="tab-pane fade">--}}
                {{--<table id="history_table" class="nowrap table table-hover table-bordered" cellspacing="0" width="100%">--}}
                    {{--<thead>--}}
                    {{--<tr>--}}
                        {{--<th>артикул</th>--}}
                        {{--<th>SAP</th>--}}
                        {{--<th>наименования</th>--}}
                        {{--<th>поставщик</th>--}}
                        {{--<th>дата заказа</th>--}}
                        {{--<th>дата отгрузки</th>--}}
                        {{--<th>дата прихода</th>--}}
                        {{--<th>invoice заказа</th>--}}
                        {{--<th>invoice лог.</th>--}}
                        {{--<th>№ транспорта</th>--}}
                        {{--<th>ед. изм.</th>--}}
                        {{--<th>цена за ед.</th>--}}
                        {{--<th>валюта</th>--}}
                        {{--<th>кол-во в зак.</th>--}}
                        {{--<th>кол-во в лог.</th>--}}
                        {{--<th>в пути</th>--}}
                        {{--<th>прибыло</th>--}}
                        {{--<th>приход</th>--}}
                        {{--<th>ост. пост-ка</th>--}}
                        {{--<th>склад</th>--}}
                    {{--</tr>--}}
                    {{--</thead>--}}
                    {{--<tbody>--}}
                    {{--</tbody>--}}
                    {{--<tfoot>--}}
                    {{--<tr>--}}
                        {{--<th>артикул</th>--}}
                        {{--<th>SAP</th>--}}
                        {{--<th>наименования</th>--}}
                        {{--<th>поставщик</th>--}}
                        {{--<th>дата заказа</th>--}}
                        {{--<th>дата отгрузки</th>--}}
                        {{--<th>дата прихода</th>--}}
                        {{--<th>invoice зак.</th>--}}
                        {{--<th>invoice лог.</th>--}}
                        {{--<th>№ транспорта</th>--}}
                        {{--<th>ед. изм.</th>--}}
                        {{--<th>цена за ед.</th>--}}
                        {{--<th>валюта</th>--}}
                        {{--<th>кол-во в зак.</th>--}}
                        {{--<th>кол-во в лог.</th>--}}
                        {{--<th>в пути</th>--}}
                        {{--<th>прибыло</th>--}}
                        {{--<th>приход</th>--}}
                        {{--<th>ост. пост-ка</th>--}}
                        {{--<th>склад</th>--}}
                    {{--</tr>--}}
                    {{--</tfoot>--}}
                {{--</table>--}}
            {{--</div>--}}
        </div>
    </div>

    {{--edit amount--}}
    <div id="edit_amount_modal" class="modal fade" role="dialog" style="z-index: 999999">
        <div class="modal-dialog modal-sm" style="width: 20%">
            <form id="update_item_amount" method="post">
                {{ csrf_field() }}
                <div class="modal-content" >
                    <div class="modal-header">
                        изменить
                        <button type="button" class="close" data-dismiss="modal" style="font-size: 24px" id="close_edit_amount_modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-warning" style="display: none">
                            <ul></ul>
                        </div>
                        <label for="edit_amount">кол-во</label>
                        <input type="number" name="edit_amount" id="edit_amount" class="form-control" onkeyup="if(parseFloat(this.value) >= parseFloat(this.max)) this.value = this.max;">
                        <span id="maxValueOfOrder"></span>
                        <input type="hidden" name="hidden_edit_logistics_list_id" id="hidden_edit_logistics_list_id" class="form-control">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-warning " onclick="update_logistics_list()"><i class="fa fa-save"></i> Сохранить</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div id="updateLogistics" class="modal fade" role="dialog" style="z-index: 999999">
        <div class="modal-dialog"  style="width: 30%;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <p style="color: #000; font-size: 12px; display: inline-block; padding: 0; margin: 0" id="logisticsInfoHeader"></p>
                    <button type="button" class="close" data-dismiss="modal" id="close_edit_log_modal"><span style="font-size: 24px; padding: 0; margin: 0">&times;</span></button>
                </div>
                <div class="alert alert-success" style="display:none; color: black">
                    <ul>

                    </ul>
                </div>
                <form action="" method="post">
                    <div class="modal-body" style="height: 50vh;">
                        <label for="edit_invoice_number">invoice number</label>
                        <input type="text" name="edit_invoice_number" id="edit_invoice_number" class="form-control" >
                        <label for="delivery_time">дата отгрузки</label>
                        <input type="date" name="delivery_time" id="delivery_time" class="form-control" >
                        <label for="truck_number">контейнер</label>
                        <input type="text" name="truck_number" id="truck_number" class="form-control" >
                        <label for="price">Logistics price</label>
                        <input type="number" name="logistics_price" id="logistics_price" class="form-control">
                        <label for="unit">Price unit</label>
                        <select name="price_unit" id="price_unit" class="standardSelect" data-placeholder="выбрать">
                            @foreach($currencies as $currency)
                                <option value="{{ $currency->id }}">{{ $currency->name }}</option>
                            @endforeach
                        </select>
                        <div class="col-md-12">
                            <label for="description">Примечание</label>
                            <input type="text" name="description" id="description" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" onclick="update_logistics()"><i class="fa fa-save"></i> сохранить</button>
                    </div>
                </form>
            </div>

        </div>
    </div>

    <div id="show_detail" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" id="close_detail_modal"><span style="font-size: 24px">&times;</span></button>
                    <p id="logisticsHeaderInfo" style="display: block; color: black; margin: 0; padding: 0"></p>
                </div>
                <div class="modal-body">
                    <div class="alert alert-info" style="display:none">
                        <ul>

                        </ul>
                    </div>
                    {{--<form action="">--}}
                        {{--<div class="col-md-4" style="padding-left: 0;">--}}
                            {{--<select name="notContainedItems" id="notContainedItems" data-placeholder="выбрать деталь..." class="standardSelect">--}}
                            {{--</select>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-4" style="padding-left: 0;">--}}
                            {{--<select name="currentItemInvoice" id="currentItemInvoice" data-placeholder="выбрать invoice..." class="standardSelect">--}}
                            {{--</select>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-1">--}}
                            {{--<input type="number" class="form-control" id="amount" name="amount" placeholder="кол-во" min="0" style="height: 28px">--}}
                        {{--</div>--}}
                        {{--<div class="col-md-1">--}}
                            {{--<button type="button" class="btn btn-warning " id="btnAdd" onclick="add_new_item_to_logistics()"><i class="fa fa-plus"></i> добавить</button>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                    <div style="display: flex">
                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#updateLogistics">
                            изменить логистику
                        </button>
                        <div style="margin-left: 10px">
                            <button id="pop-up-date-input-trigger" class="btn btn-primary" style="z-index: 9;">Print</button>
                            <span id="pop-up-date-input" style="top: 0; z-index: 999; background-color: whitesmoke; box-shadow: 0px 0px 23px 2px rgba(0,0,0,0.75); display: flex; flex-direction: column; width: 200px; border: 1px solid #999; padding: 10px; position: absolute;">
                            <input type="date" id="input-date-logistics" />
                                <div style="display: flex; justify-content: space-between;">
                                    <button type="button" class="btn btn-primary" style="width: 40%" id="print-detail-in-format">Ok</button>
                                    <button id="pop-up-date-input-close" class="btn btn-danger" style="width: 40%">Close</button>
                                </div>

                        </span>
                        </div>


                    </div>
                    <input type="hidden" id="logistics_id">
                    <table id="detail_table" class="table table-hover table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>артикул</th>
                            <th>SAP код</th>
                            <th>наименование</th>
                            <th>item name</th>
                            <th>invoice №</th>
                            <th>кол-во</th>
                            <th>приход</th>
                            <th>ед. изм.</th>

                            <th>cost per item(no VAT)</th>
                            <th>total price</th>
                            <th>gross weight(kg)</th>
                            <th>gross weight per unit(kg)</th>
                            <th>logistics cost</th>
                            <th>logistics cost per unit</th>
                            <th>price unit</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>артикул</th>
                            <th>SAP код</th>
                            <th>наименование</th>
                            <th>item name</th>
                            <th>invoice №</th>
                            <th id="total_amount">кол-во</th>
                            <th>приход</th>
                            <th>ед. изм.</th>

                            <th>cost per item(no VAT)</th>
                            <th>total price</th>
                            <th>gross weight(kg)</th>
                            <th>gross weight per unit(kg)</th>
                            <th>logistics cost</th>
                            <th>logistics cost per unit</th>
                            <th>price unit</th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" onclick="saveArrivedLogistics()" id="saveLogisticsBtn" style="display: none" disabled>прибыл <i class="fa fa-save"></i></button>
                    <button type="button" class="btn btn-danger" onclick="closeLogistics()" id="closeLogisticsBtn" style="display: none" disabled>закрыть <i class="fa fa-close"></i></button>
                    {{--<button type="button" class="btn btn-danger" onclick="delete_logistics_func()" id="deleteLogisticsBtn" style="display: none" disabled>удалить <i class="fa fa-trash"></i></button>--}}
                </div>
            </div>

        </div>
    </div>

</div>
@section('additionalLibrary')
    <script>
        $(document).ready(function() {
            var now = new Date();
            $('#pop-up-date-input').hide();

            var day = ("0" + now.getDate()).slice(-2);
            var month = ("0" + (now.getMonth() + 1)).slice(-2);

            var today = now.getFullYear()+"-"+(month)+"-"+(day);
            // $('#start').val(today);
            $('#input-date-logistics').val(today);
            $('#upload_end').val(today);
            $('#input_end').val(today);
            $('#order_end').val(today);

            $('#l_upload_end').val(today);
            $('#l_order_end').val(today);

            $('#storageDiv').show();
            $('#allLogDiv').show();

            $('#filter_by_from_storage').multiselect({
                search   : true,
                onOptionClick: function( element, option ) {
                    $('#logistics_table').DataTable().destroy();
                    fetch_data();
                    fetch_onTheWay_data();
                    // fetch_history_data();
                },
                maxHeight: 400,
                texts    : {
                    placeholder: 'Поставщики/База',
                    search     : 'Search States'
                },
            });
            $('#filter_by_to_storage').multiselect({
                search   : true,
                onOptionClick: function( element, option ) {
                    $('#logistics_table').DataTable().destroy();
                    fetch_data();
                    // fetch_history_data();
                },
                maxHeight: 400,
                texts    : {
                    placeholder: 'Склады',
                    search     : 'Search States'
                },
            });
            $('#filter_by_user').multiselect({
                search   : true,
                onOptionClick: function( element, option ) {
                    $('#logistics_table').DataTable().destroy();
                    fetch_data();
                    fetch_onTheWay_data();
                },
                maxHeight: 400,
                texts    : {
                    placeholder: 'Автор',
                    search     : 'Search States'
                },
            });
            $('#loading').hide();
            fetch_data();
            fetch_onTheWay_data();
            // fetch_history_data();
            $('#logistics').on('show.bs.tab', function(){
                alert('New tab will be visible now!');
            });

            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // resetHistoryDatefilter();
                resetAllLogDateFilter();
                $('select[multiple]').multiselect( 'reset' );
                fetch_data();
                fetch_onTheWay_data();
                // fetch_history_data();
                var target = $(e.target).attr("href");
                if (target == '#onTheWay') {
                    $('#allLogDiv').hide();
                    $('#supplierDiv').show();
                    $('#storageDiv').hide();
                    $('#statusDiv').hide();
                    // $('#historyDiv').hide();
                }
                else if(target == '#logistics'){
                    $('#allLogDiv').show();
                    // $('#historyDiv').hide();
                    $('#storageDiv').show();
                    $('#statusDiv').show();
                    $('#supplierDiv').hide();
                }
                else if(target == '#history'){
                    $('#allLogDiv').hide();
                    $('#statusDiv').hide();
                    $('#supplierDiv').hide();
                    // $('#historyDiv').show();
                }else{
                    $('#allLogDiv').hide();
                    // $('#historyDiv').hide();
                    $('#storageDiv').hide();
                    $('#statusDiv').hide();
                    $('#supplierDiv').hide();
                }
                $('#logistics_table').DataTable().columns.adjust().draw();
                $('#onTheWay_table').DataTable().columns.adjust().draw();
            } );
            $('#logistics_table').on('dblclick', 'tr', function (e) {
                var id = $(this).data('id');
                console.log(id)
                $('#show_detail').modal("show");
                show_detail(id);
            });

            $('#logistics_table tbody').on( 'click', 'tr', function () {
                $(this).removeClass('selected');

                if ( $(this).hasClass('selected') ) {
                    $(this).removeClass('selected');
                }
                else {
                    $('#logistics_table').DataTable().$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            } );
        } );

        $('#pop-up-date-input-trigger').on('click', function (){
            $('#pop-up-date-input').show();
        })

        $('#pop-up-date-input-close').on('click', function (){
            $('#pop-up-date-input').hide();
        })

        $('#print-detail-in-format').on('click', function (){
            var logistics_id = $('#logistics_id').val();
            var logistics_date = new Date($('#input-date-logistics').val());
            console.log(logistics_id);

            window.open("{{ route('printFormattedLogistics') }}/"+logistics_id+"/"+logistics_date.getFullYear()+'-'+(logistics_date.getMonth()+1)+'-'+logistics_date.getDate(), '_blank')

            {{--$.ajax({--}}
            {{--    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},--}}
            {{--    "url": "{{ route('printFormattedLogistics') }}/"+logistics_id,--}}
            {{--    "type": "post",--}}
            {{--    "data": {--}}
            {{--        logistics_id:logistics_id,--}}
            {{--    },--}}
            {{--    "success": function (data){--}}
            {{--        console.log(data);--}}
            {{--    }--}}
            {{--})--}}

        })

        $('#filter_by_type').on('change', function () {
            fetch_onTheWay_data();
        });
        $('.standardSelect').on('change',function () {
            $('#logistics_table').DataTable().destroy();
            fetch_data();
        });

        // $('#upload_start').on('change',function () {
        //     $('#history_table').DataTable().destroy();
        //     fetch_history_data();
        // });
        // $('#upload_end').on('change',function () {
        //     $('#history_table').DataTable().destroy();
        //     fetch_history_data();
        // });
        //
        // $('#order_start').on('change',function () {
        //     $('#history_table').DataTable().destroy();
        //     fetch_history_data();
        // });
        // $('#order_end').on('change',function () {
        //     $('#history_table').DataTable().destroy();
        //     fetch_history_data();
        // });
        //
        // $('#input_start').on('change',function () {
        //     $('#history_table').DataTable().destroy();
        //     fetch_history_data();
        // });
        // $('#input_end').on('change',function () {
        //     $('#history_table').DataTable().destroy();
        //     fetch_history_data();
        // });

        $('#l_upload_start').on('change',function () {
            $('#logistics_table').DataTable().destroy();
            fetch_data();
        });
        $('#l_upload_end').on('change',function () {
            $('#logistics_table').DataTable().destroy();
            fetch_data();
        });

        $('#l_order_start').on('change',function () {
            $('#logistics_table').DataTable().destroy();
            fetch_data();
        });
        $('#l_order_end').on('change',function () {
            $('#logistics_table').DataTable().destroy();
            fetch_data();
        });

        // $('#resetDateFilter').click(function () {
        //     resetHistoryDatefilter();
        // });

        // function resetHistoryDatefilter() {
        //     var now = new Date();
        //
        //     var day = ("0" + now.getDate()).slice(-2);
        //     var month = ("0" + (now.getMonth() + 1)).slice(-2);
        //
        //     var today = now.getFullYear()+"-"+(month)+"-"+(day);
        //     $('#upload_start').val('');
        //     $('#input_start').val('');
        //     $('#order_start').val('');
        //     $('#upload_end').val(today);
        //     $('#input_end').val(today);
        //     $('#order_end').val(today);
        //     $('#history_table').DataTable().destroy();
        //     fetch_history_data();
        // }

        $('#l_resetDateFilter').click(function () {
            resetAllLogDateFilter();
        });

        function  resetAllLogDateFilter() {
            var now = new Date();

            var day = ("0" + now.getDate()).slice(-2);
            var month = ("0" + (now.getMonth() + 1)).slice(-2);

            var today = now.getFullYear()+"-"+(month)+"-"+(day);
            $('#l_upload_start').val('');
            $('#l_order_start').val('');
            $('#l_upload_end').val(today);
            $('#l_order_end').val(today);
            $('#logistics_table').DataTable().destroy();
            fetch_data();
        }
        function fetch_data() {
            var l_upload_start = $('#l_upload_start').val();
            var l_upload_end = $('#l_upload_end').val();
            var l_order_start = $('#l_order_start').val();
            var l_order_end = $('#l_order_end').val();
            var filterArr = {};
            $('.multipleChosen').each(function (index, value) {
                var parentKey = $(this).attr('id');
                filterArr[parentKey] = [];
                if ($(this).val()!=null) {
                    $.each($(this).val(), function (i, val) {
                        filterArr[parentKey].push(val);
                    });
                }
            });
            var stateFilter = $('#stateFilter').val();
            console.log(filterArr)
            $('#logistics_table').DataTable().destroy();
            var table = $('#logistics_table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "{{ route('allLogistics')}}",
                    type: "post",
                    data: {filterArr: JSON.stringify(filterArr), stateFilter:stateFilter, l_upload_start:l_upload_start, l_order_start:l_order_start, l_upload_end:l_upload_end, l_order_end:l_order_end}
                },
                pageLength:100,
                order: [[ 12, "desc" ]],
                lengthMenu:[[100, 500, 1000], [100, 500, 1000]],
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
                        },
                        title:'бум лист',
                        text: '<i class="fa fa-file-excel-o"></i>'

                    },
                ],
                "columns": [
                    { "data":"id" },
                    { "data":"state_name" },
                    { "data":"percent" },
                    { "data":"delivery_time" },
                    { "data":"shipment_time" },
                    { "data":"from_storage_name" },
                    { "data":"to_storage_name" },
                    { "data":"logistics_invoice_number"},
                    { "data":"order_invoice_number"},
                    { "data":"container_number" },
                    { "data":"container_type" },
                    { "data":"user_name" },
                    { "data":"create_time" }
                ],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение от _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "нет записей",
                    "infoFiltered": "(<b>отфильтровано из _TOTAL_ записей</b>)",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr('data-id', data.id);
                },
                scrollY: "70vh",
                scrollCollapse: true,
                scrollX: true
            });
        }

        function saveArrivedLogistics() {
            $('#saveLogisticsBtn').hide().attr('disabled', true);
            $('#closeLogisticsBtn').hide().attr('disabled', true);
            $('#deleteLogisticsBtn').hide().attr('disabled', true);
            var logistics_id = $('#logistics_id').val();
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: "{{ route('saveArrivedLogistics') }}",
                type: "post",
                data: { logistics_id:logistics_id},
                success: function (data) {
                    console.log(data);
                    fetch_data();
                    fetch_detail_table();
                    $('.alert-info').hide();
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    console.log(json)
                    $('.alert-info').show();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').empty().append('<li>'+value+'</li>');
                    })
                }
            })
        }

        function show_detail(logistics_id) {
            $('#saveLogisticsBtn').hide().attr('disabled', true);
            $('#closeLogisticsBtn').hide().attr('disabled', true);
            $('#deleteLogisticsBtn').hide().attr('disabled', true);
            $('#logistics_id').val(logistics_id);
            $('#close_logistics_id').val(logistics_id);
            $('#loading').show();
            $('.alert-info').hide();
        }

        $('#show_detail').on('shown.bs.modal', function() {
            fetch_detail_table();
            // getNotConsistedItems();
        });

        $('#updateLogistics').on('shown.bs.modal', function () {
            set_logistics_value();
        });

        function fetch_detail_table() {
            $('#saveLogisticsBtn').hide().attr('disabled', true);
            $('#closeLogisticsBtn').hide().attr('disabled', true);
            $('#deleteLogisticsBtn').hide().attr('disabled', true);
            $('#detail_table').DataTable().destroy();
            var logistics_id = $('#logistics_id').val();
            console.log(logistics_id)
            var table = $('#detail_table').DataTable({
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    "url": "{{ route('showLogisticsList') }}",
                    "type": "get",
                    "data": {logistics_id:logistics_id},
                    "dataSrc": function (data) {
                        $(table.column(10).footer()).text("Total: "+data['total_gross_weight']);
                        $(table.column(12).footer()).text("Total: "+data['logistics_info'].logistics_price);
                        $('#loading').hide();
                        $('#logisticsHeaderInfo').text('№ '+data['logistics_info'].id+'| invoice № '+data['logistics_info'].invoice_number+'| фура/контейнер № '+data['logistics_info'].container_number);
                        return data['data'];
                    },

                },
                columnDefs:[
                    { orderable: false, targets: [6, 8] }
                ],

                columns:[
                    {'data':'articula_new', 'width':'6%'},
                    {'data':'sap_code', 'width':'6%'},
                    {'data':'item_name', 'width':'25%'},
                    {'data':'english_name', 'width':'25%'},
                    {'data':'order_invoice', 'width':'5%'},
                    {'data':'amount', 'width':'4%'},
                    {'data':'complete', 'width':'4%'},
                    {'data':'unit_name', 'width':'4%'},
                    {'data': 'price_per_unit'},
                    {'data': 'total_price'},
                    {'data': 'gross_weight'},
                    {'data': 'gross_weight_per_unit'},
                    {'data': 'logistics_price'},
                    {'data': 'logistics_price_per_unit'},
                    {'data': 'logistics_price_unit'},
                    {'data':'buttons', 'width':'2%'},
                ],
                scrollX: '50vh',
                paging: false,
                bPaginate: false,
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "_TOTAL_ записей",
                    "infoEmpty": "нет записей",
                    "infoFiltered": "(<b>отфильтровано из _MAX_ записей</b>)",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 ]
                        },
                        title:'order list items',
                        messageTop: 'все элементы заказа',
                        text: '<i class="fa fa-file-excel-o"></i>'

                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4]
                        },
                        title:'order list items',
                        messageTop: 'все элементы заказа',
                        text: '<i class="fa fa-file-pdf-o"></i>'

                    },
                ],
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr('data-id', data.id);
                    if(data['amount'] === data['complete']){
                        $(row).attr("class", "completed-logistics-status")
                    }
                    else if(data['complete']){
                        $(row).attr("class", "uncompleted-logistics-status")
                    }
                    else{
                        $(row).attr("class", "null-logistics-status");
                        $(row).find('td').eq(6).text("0");
                    }
                },

                scrollCollapse: true,
                scrollY: "65vh",
                initComplete: function (data) {
                    if (data.json.allowToSave) {
                        $('#saveLogisticsBtn').show().attr('disabled', false);
                    }
                    if (data.json.allowToClose) {
                        $('#closeLogisticsBtn').show().attr('disabled', false);
                    }
                    if (data.json.allowToDelete) {
                        $('#deleteLogisticsBtn').show().attr('disabled', false);
                    }
                }
            });
        }

        function set_list_data(list_id) {
            $('#hidden_edit_logistics_list_id').val(list_id);
            $.ajax({
                url: "{{ route('edit_logistics_list') }}",
                type: "get",
                data: { logistics_list_id:list_id},
                success: function (data) {
                    console.log(JSON.parse(data))
                    $('#edit_amount').val(JSON.parse(data).amount);
                    $('#edit_amount').attr('max', JSON.parse(data).remaining);
                    $('#maxValueOfOrder').text('макс. кол-во логистики:('+JSON.parse(data).remaining+')').css('font-weight', 'bold');
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    console.log(json)
                    $('.alert-warning').show();
                    $.each(json.errors, function (index, value) {
                        $('.alert-warning ul').empty().append('<li>'+value+'</li>');
                    })
                }
            });
        }

        function delete_item(logistics_list_id){
            console.log(logistics_list_id)
            if (confirm('удалить?')){
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'POST',
                    url:"{{route('deleteLogisticsListItem')}}",
                    data:{logistics_list_id:logistics_list_id},
                    success: function (logistics) {
                        console.log(logistics)
                        fetch_detail_table();
                        fetch_data();
                        $('.alert-info').show();
                        $('.alert-info ul').empty().append('удален');
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        console.log(json)
                        $('.alert-info').show();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info').empty().append(value);
                        })
                    }
                })
            }
        }

        function update_logistics_list() {
            var amount = $('#edit_amount').val();
            var log_list_id = $('#hidden_edit_logistics_list_id').val();
            event.preventDefault();
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type:'POST',
                url:"{{route('updateLogisticsList')}}",
                data:{amount:amount, log_list_id:log_list_id},
                success: function (data) {
                    fetch_data();
                    fetch_detail_table();
                    $('#close_edit_amount_modal').click();
                    $('.alert-info').show().empty().append('сохранено');
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    console.log(json)
                    $('.alert-info').show();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info').empty().append(value);
                    })
                }
            })
        }

        function set_logistics_value() {
            var logistics_id = $('#logistics_id').val();
            $.ajax({
                url:"{{ route('editLogistics') }}",
                type:"get",
                data:{logistics_id:logistics_id},
                success:function (data) {
                    var item = JSON.parse(data);
                    console.log(item)
                    $('#logisticsInfoHeader').text('№:'+item.id+' | invoice №:'+item.invoice_number);
                    $('#edit_invoice_number').val(item.invoice_number);
                    $('#delivery_time').val(item.delivery_time.substring(0,10));
                    $('#truck_number').val(item.container_number);
                    $('#description').val(item.description);
                    $('#logistics_price').val(item.logistics_price);
                    $('#price_unit').val(item.price_unit);
                },
                error:function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    console.log(json)
                    $('.alert-info').show();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info').empty().append(value);
                    })
                }
            });

        }

        function update_logistics() {
            var logistics_id = $('#logistics_id').val();
            var invoice_number = $('#edit_invoice_number').val();
            var delivery_time = $('#delivery_time').val();
            var truck_number = $('#truck_number').val();
            var description = $('#description').val();
            var logistics_price = $('#logistics_price').val();
            var price_unit = $('#price_unit').val();

            console.log(logistics_price);

            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url:"{{ route('updateLogistics') }}",
                type:"post",
                data:{logistics_id:logistics_id, logistics_price: logistics_price, price_unit: price_unit,invoice_number:invoice_number, delivery_time:delivery_time, truck_number:truck_number, description:description},
                success:function (data) {
                    var item = JSON.parse(data);
                    $('#close_edit_log_modal').click();
                    fetch_data();
                    fetch_detail_table()
                    $('.alert-info').show().empty().append('изменено');
                },
                error:function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    console.log(json)
                    $('.alert-info').show();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info').empty().append(value);
                    })
                }
            });
        }

        function delete_logistics_func(){
            var logistics_id = $('#logistics_id').val();
            if (confirm('удалить?')){
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'POST',
                    url:"{{route('deleteLogistics')}}",
                    data:{logistics_id:logistics_id},
                    success: function (logistics) {
                        $('#close_detail_modal').click();
                        fetch_data();
                        $('.alert-info').show().empty().append('удален');
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        console.log(json);
                        $('.alert-info').show();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info').empty().append(value);
                        })
                    }
                })
            }
        }

        function closeLogistics(){
            var logistics_id = $('#logistics_id').val();
            if (confirm('закрыт')){
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'POST',
                    url:"{{route('closeLogistics')}}",
                    data:{logistics_id:logistics_id},
                    success: function (logistics) {
                        fetch_data();
                        fetch_detail_table();
                        $('#close_detail_modal').click();
                        $('.alert-info').show().empty().append('закрыт');
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        $('.alert-info').show();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info').empty().append(value);
                        })
                    }
                })
            }
        }

        function isExist(item_id, items){
            if(items!=null){
                for (var i=0; i<items.length; i++){
                    if(item_id==items[i].item_id){
                        return true
                    }
                }
            }
            return false
        }

        function excelExport(order_id) {
            $('#order_id').val(order_id);
            $('#excelExportForm').submit();
        }

        $('#notContainedItems').on('change', function () {
            var logistics_id = $('#logistics_id').val();
            var item_id = $('#notContainedItems').val();
            $.ajax({
                url: "{{ route('getCurrentItemInvoice') }}",
                type: "get",
                data: {logistics_id:logistics_id, item_id:item_id},
                success: function(data){
                    $('#currentItemInvoice').empty().trigger("chosen:updated");
                    var items = JSON.parse(data);
                    if (items.length>0) {
                        $.each(items, function (index, subItemObj) {
                            $('#currentItemInvoice').append('<option value="'+subItemObj.id+'">'+subItemObj.articula_new+' '+subItemObj.item_name+'</option>')
                        });
                    }
                    $('#currentItemInvoice').trigger("chosen:updated");
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    console.log(json)
                    $('.alert-info').show();
                    $('.alert-info ul').empty()
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').append('<li>'+value+'</li>');
                    })
                }
            })
        });

        function getNotConsistedItems(){
            var logistics_id = $('#logistics_id').val();
            if (logistics_id != null){
                $('#notContainedItems').append('<option value="" >выбрать...</option>');
                $.ajax({
                    url: "{{ route('getNotConsistedItemsOfLogistics') }}",
                    type: "get",
                    data: {logistics_id:logistics_id},
                    success: function(data){
                        $('#notContainedItems').empty().trigger("chosen:updated");
                        var items = JSON.parse(data);
                        if (items.length>0) {
                            $.each(items, function (index, subItemObj) {
                                $('#notContainedItems').append('<option value="'+subItemObj.id+'">'+subItemObj.articula_new+' '+subItemObj.item_name+'</option>')
                            });
                        }
                        $('#notContainedItems').trigger("chosen:updated");
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        console.log(json)
                        $('.alert-info').show();
                        $('.alert-info ul').empty()
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').append('<li>'+value+'</li>');
                        })
                    }
                })
            }else{
                $('#notContainedItems').empty();
                $('#notContainedItems').trigger("chosen:updated");
            }
        }

        function fetch_onTheWay_data() {
            var filterArr = {};
            $('.multipleChosen').each(function (index, value) {
                var parentKey = $(this).attr('id');
                filterArr[parentKey] = [];
                if ($(this).val()!=null) {
                    $.each($(this).val(), function (i, val) {
                        filterArr[parentKey].push(val);
                    });
                }
            });
            var stateFilter = $('#stateFilter').val();
            var filter_by_type = $('#filter_by_type').val();
            console.log(filterArr)
            $('#onTheWay_table').DataTable().destroy();
            var table = $('#onTheWay_table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "{{ route('onTheWay')}}",
                    type: "post",
                    data: {filterArr: JSON.stringify(filterArr), stateFilter:stateFilter, filter_by_type:filter_by_type}
                },
                pageLength:100,
                order: [[ 11, "desc" ]],
                lengthMenu:[[100, 500, 1000], [100, 500, 1000]],
                "columns": [
                    { "data":"articula_new" },
                    { "data":"item_name" },
                    { "data":"sap_code" },
                    { "data":"supplier" },
                    { "data":"invoice_number" },
                    { "data":"unit" },
                    { "data":"order_amount" },
                    { "data":"onTheWay"},
                    { "data":"inputed"},
                    { "data":"sup_remaining" },
                    { "data":"delivery_time" },
                    { "data":"shipment_time" },
                    { "data":"user_name" }
                ],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение от _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "нет записей",
                    "infoFiltered": "(<b>отфильтровано из _TOTAL_ записей</b>)",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0,1,2,3,4,5,6,7,8,9,10]
                        },
                        title:'бум лист',
                        text: '<i class="fa fa-file-excel-o"></i>'

                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: [ 0,1,2,3,4,5,6,7,8,9,10]
                        },
                        title:'бум лист',
                        text: '<i class="fa fa-file-pdf-o"></i>'

                    },
                ],

                scrollY: "70vh",
                scrollCollapse: true,
                scrollX: true
            });
        }

        {{--function fetch_history_data() {--}}
            {{--var upload_start_date = $('#upload_start').val();--}}
            {{--var upload_end_date = $('#upload_end').val();--}}
            {{--var input_start_date = $('#input_start').val();--}}
            {{--var input_end_date = $('#input_end').val();--}}
            {{--var order_start_date = $('#order_start').val();--}}
            {{--var order_end_date = $('#order_end').val();--}}

            {{--var filterArr = {};--}}
            {{--$('.multipleChosen').each(function (index, value) {--}}
                {{--var parentKey = $(this).attr('id');--}}
                {{--filterArr[parentKey] = [];--}}
                {{--if ($(this).val()!=null) {--}}
                    {{--$.each($(this).val(), function (i, val) {--}}
                        {{--filterArr[parentKey].push(val);--}}
                    {{--});--}}
                {{--}--}}
            {{--});--}}
            {{--var stateFilter = $('#stateFilter').val();--}}
            {{--console.log(filterArr)--}}
            {{--$('#history_table').DataTable().destroy();--}}
            {{--var table = $('#history_table').DataTable({--}}
                {{--processing: true,--}}
                {{--serverSide: true,--}}
                {{--ajax: {--}}
                    {{--headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},--}}
                    {{--url: "{{ route('historyOfOrder')}}",--}}
                    {{--type: "post",--}}
                    {{--data: {filterArr: JSON.stringify(filterArr), upload_start_date:upload_start_date, upload_end_date:upload_end_date, input_start_date:input_start_date, input_end_date:input_end_date,--}}
                        {{--order_start_date:order_start_date, order_end_date:order_end_date}--}}
                {{--},--}}
                {{--pageLength:100,--}}
                {{--order: [[ 4, "desc" ]],--}}
                {{--lengthMenu:[[100, 500, 1000, 5000], [100, 500, 1000, 5000]],--}}
                {{--"columns": [--}}
                    {{--{ "data":"articula_new" },--}}
                    {{--{ "data":"sap_code" },--}}
                    {{--{ "data":"item_name", "width":"220px" },--}}
                    {{--{ "data":"supplier" },--}}
                    {{--{ "data":"shipment_time" },--}}
                    {{--{ "data":"delivery_time" },--}}
                    {{--{ "data":"input_date" },--}}
                    {{--{ "data":"order_invoice" },--}}
                    {{--{ "data":"log_invoice" },--}}
                    {{--{ "data":"container_number" },--}}
                    {{--{ "data":"unit" },--}}
                    {{--{ "data":"price_per_unit" },--}}
                    {{--{ "data":"currency" },--}}
                    {{--{ "data":"order_amount" },--}}
                    {{--{ "data":"log_amount" },--}}
                    {{--{ "data":"way_amount"},--}}
                    {{--{ "data":"input_sum"},--}}
                    {{--{ "data":"inputed"},--}}
                    {{--{ "data":"sup_remaining" },--}}
                    {{--{ "data":"to_storage" }--}}
                {{--],--}}
                {{--createdRow: function( row, data, dataIndex ) {--}}
                    {{--$( row ).find('td:eq(2)').addClass("proc");--}}
                {{--},--}}
                {{--language: {--}}
                    {{--"lengthMenu": "_MENU_",--}}
                    {{--"zeroRecords": "ничего не найдено",--}}
                    {{--"info": "Отображение от _START_ до _END_ из _TOTAL_ записей",--}}
                    {{--"infoEmpty": "нет записей",--}}
                    {{--"infoFiltered": "(<b>отфильтровано из _TOTAL_ записей</b>)",--}}
                    {{--"search": "<i class='fa fa-search' style='float: left'></i>",--}}
                    {{--"paginate": {--}}
                        {{--"previous": "<i class='fa fa-angle-left'></i>",--}}
                        {{--"next": "<i class='fa fa-angle-right'></i>",--}}
                    {{--}--}}
                {{--},--}}
                {{--dom: 'Blfrtip',--}}
                {{--buttons: [--}}
                    {{--{--}}
                        {{--extend: 'excelHtml5',--}}
                        {{--exportOptions: {--}}
                            {{--columns: [ 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17]--}}
                        {{--},--}}
                        {{--title:'бум лист',--}}
                        {{--text: '<i class="fa fa-file-excel-o"></i>'--}}

                    {{--},--}}
                    {{--{--}}
                        {{--extend: 'pdfHtml5',--}}
                        {{--exportOptions: {--}}
                            {{--columns: [ 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17]--}}
                        {{--},--}}
                        {{--title:'бум лист',--}}
                        {{--text: '<i class="fa fa-file-pdf-o"></i>'--}}

                    {{--},--}}
                {{--],--}}
                {{--scrollY: "70vh",--}}
                {{--scrollCollapse: true,--}}
                {{--scrollX: true--}}
            {{--});--}}
        {{--}--}}
    </script>
@endsection

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<table>
    <tbody style="border: 10px solid #000000">
    <tr>
        <td colspan="8" style="border: 1px solid #000000" height="30"><h3 style="text-align: center; vertical-align: middle">{{ (!is_null($supplier))?$supplier->name:'' }}</h3></td>
    </tr>
    <tr>
        <td colspan="8" style="border: 1px solid #000000; vertical-align: top" height="30"><p style="text-align: center">{{ (!is_null($supplier))?$supplier->address:'' }}</p></td>
    </tr>
    <tr>
        <td colspan="8" style="border: 1px solid #000000"></td>
    </tr>
    <tr>
        <td colspan="8" style="border: 5px solid #000000; vertical-align: middle" height="30"><h3 style="text-align: center">COMMERCIAL INVOICE</h3></td>
    </tr>
    <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
    <tr><td colspan="5" style="border-top: 5px solid #000000; border-left: 5px solid #000000; border-right: 5px solid #000000; vertical-align: top" height="30"></td>
        <td style="border-top: 5px solid #000000; border-right: 5px solid #000000" colspan="3"></td><td></td><td></td>
    </tr>
    <tr>
        <td colspan="5" style="border-left: 5px solid #000000; border-right: 5px solid #000000; vertical-align: top">Consignee:</td>
        <td style="border-right: 5px solid #000000" colspan="3">Invoice number: {{ (!is_null($order))?str_limit($order->create_time, 10, ''):'' }}</td><td></td><td></td>
    </tr>
    <tr>
        <td colspan="5"  style="border-left: 5px solid #000000; border-right: 5px solid #000000">JV  "QUALITY ELECTRONICS" LLC</td>
        <td colspan="3"  style="border-right: 5px solid #000000" >Date: {{ (!is_null($order))?str_limit($order->create_time, 10, ''):'' }}</td>
    </tr>
    <tr>
        <td colspan="5"  style=" border-left: 5px solid #000000; border-right: 5px solid #000000">Address: 2 MAKHTAMKULI STREET, YASHNABOD</td>
        <td colspan="3" style="border-right: 5px solid #000000" ></td>
    </tr>
    <tr>
        <td colspan="5" style="border-left: 5px solid #000000; border-right: 5px solid #000000" >DISTRICT, TASHKENT, UZBEKISTAN</td>
        <td colspan="3" style="border-right: 5px solid #000000; " ></td>
    </tr>
    <tr><td colspan="5" style="border-bottom: 5px solid #000000; border-left: 5px solid #000000; border-right: 5px solid #000000; vertical-align: top" height="30"></td>
        <td style="border-bottom: 5px solid #000000; border-right: 5px solid #000000" colspan="3"></td><td></td><td></td>
    </tr>
    <tr>
        <td style="border: 5px solid #000000; text-align: center; vertical-align: middle; font-weight: bold" height="30" width="8">№</td>
        <td style="border: 5px solid #000000; text-align: center; vertical-align: middle;  font-weight: bold" width="28">Description</td>
        <td style="border: 5px solid #000000; text-align: center; vertical-align: middle;  font-weight: bold" width="18">Part Number</td>
        <td style="border: 5px solid #000000; text-align: center; vertical-align: middle;  font-weight: bold" width="18">HS CODE</td>
        <td style="border: 5px solid #000000; text-align: center; vertical-align: middle;  font-weight: bold" width="10">Unit</td>
        <td style="border: 5px solid #000000; text-align: center; vertical-align: middle;  font-weight: bold" width="12">Q-ty</td>
        <td style="border: 5px solid #000000; text-align: center; vertical-align: middle;  font-weight: bold" width="12">Unit price (USD)</td>
        <td style="border: 5px solid #000000; text-align: center; vertical-align: middle;  font-weight: bold" width="12">Price (USD)</td>
    </tr>
    @foreach($items as $key=>$item)
        <tr>
            <td style="border: 5px solid #000000; text-align: center; vertical-align: middle;" >{{ ++$key  }}</td>
            <td style="border: 5px solid #000000; text-align: center; vertical-align: middle;" >{{ $item->name }}</td>
            <td style="border: 5px solid #000000; text-align: center; vertical-align: middle;" >{{ $item->articula_new }}</td>
            <td style="border: 5px solid #000000; text-align: center; vertical-align: middle;" >{{ $item->hs_code }}</td>
            <td style="border: 5px solid #000000; text-align: center; vertical-align: middle;" >{{ $item->unit }}</td>
            <td style="border: 5px solid #000000; text-align: center; vertical-align: middle;" >{{ $item->amount }}</td>
            <td style="border: 5px solid #000000; text-align: center; vertical-align: middle;" ></td>
            <td style="border: 5px solid #000000; text-align: center; vertical-align: middle;" ></td>
        </tr>
    @endforeach
    <tr>
        <td colspan="7"  style="border: 5px solid #000000; font-weight: bold; text-align: right" >Total: </td>
        <td style="border: 5px solid #000000; font-weight: bold" ></td>
    </tr>
    <tr>
        <td colspan="8" style="border: 1px solid #000000; font-weight: bold" >Delivery Terms: CPT-TASHKENT</td>
    </tr>
    <tr>
        <td colspan="8" style="border: 1px solid #000000; font-weight: bold">Say Total as: </td>
    </tr>
    </tbody>
</table>
</body>
</html>
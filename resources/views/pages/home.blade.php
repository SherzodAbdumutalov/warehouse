@section('additional_css')
<style>

    *{
        font-family: 'Oswald', sans-serif;
    }

    html, body{
        margin: 0;
        padding: 0;
        width: 100%;
        height: 100%;
    }



    .llbtn {
        color: #fff;
        padding: 10px 20px;
        font-size: 11px;
        letter-spacing: 3px;
        font-weight: 500;
        background-color: #daa37f;
        border-radius: 0;
        text-align: center;
        text-transform: uppercase;
    }

    .llbtn--opacity {
        animation: opacity 3s;
    }
    .ttsect {
        padding: 130px 0;
    }
    .uucol--inbl{
        display: inline-block;
        float: none;
    }
    .pprow--center{
        text-align: center;

    }
    .pprow--small {
        max-width: 960px;
        margin:0 auto;
    }

    .pprow--margin{
        margin-top: 65px;
    }
    .ssbanner{
        background-image: url("./assets/backhomeimg.jpg");
        /*background-color:#000;*/
        background-position: center;
        background-size: cover;
        background-attachment: fixed;
        display: table;
        width: 100%;
        height: 100vh;
        position: relative;
        opacity: 0.8;

    }

    .ssbanner__overlay {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        display: table;
        height: 100%;
        text-align:center;
        background-color: rgba(0,0,0,0.4);
        animation: banner-in 2s ;
    }

    .ssbanner__container {
        transform: translateY(-23%);
        display: table-cell;
        vertical-align: middle;
    }

    .ssbanner__scroll-down {
        position: absolute;
        bottom: 15px;
        left: 50%;
        margin-left: -12px;
        cursor: pointer;
        animation: opacity 3s;
    }

    .ssbanner__title {
        font-size: 86px;
        color: #000000;
        margin-top: 5px;
        margin-bottom: 20px;
        letter-spacing: 1px;
        font-weight: 500;
        animation: opacity 3s;
    }

    .ssbanner__text {
        font-size: 24px;
        color: #000000;
        margin: 0;
        margin-bottom: 18px;
        animation: opacity 3s;
    }
    .ttsect--great {
        background-image: url("./assets/imgbaraka.jpg");
        background-position: center;
        background-size: cover;
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-color:#fff;
    }


    .ttsect--best {
        background-image: url('https://www.uzdaily.uz/storage/img/ict_telecom/artel-sales-office.jpg');
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
        padding: 160px 0;
        background-color:#000;
    }
    .fffooter {
        display: table;
        width: 100%;

        bottom: 0;
        left: 0;
        z-index: -1;
    }

    .fffooter__half {
        width: 50%;
        display: table-cell;
        padding: 100px 140px;
    }

    .fffooter__half-1 {
        background-image: url('https://www.uzdaily.uz/storage/img/ict_telecom/artel-sales-office.jpg');
        margin-left: 100px;
        background-repeat: no-repeat;
        background-size: cover;
    }

    .fffooter__half-2 {
        background-color: #000;
    }

    .fffooter__big-title {
        margin-top: 0;
        text-transform: uppercase;
        line-height: 40px;
        font-size: 30px;
        margin-bottom: 25px;
        color: red;
    }



</style>
@endsection
<div>
        @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
            <div class="ssbanner">

                <div class="ssbanner__overlay">
                    <div class="ssbanner__container">
                        <h1 class="ssbanner__title"><b>Dream WMS</b></h1>
                        <p class="ssbanner__text"><b>Dream Warehouse Management System</b></p>
                    </div>
                    <img class="ssbanner__scroll-down" src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCAxMjkgMTI5IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAxMjkgMTI5IiB3aWR0aD0iMjRweCIgaGVpZ2h0PSIyNHB4Ij4KICA8Zz4KICAgIDxwYXRoIGQ9Im0xMjEuMywzNC42Yy0xLjYtMS42LTQuMi0xLjYtNS44LDBsLTUxLDUxLjEtNTEuMS01MS4xYy0xLjYtMS42LTQuMi0xLjYtNS44LDAtMS42LDEuNi0xLjYsNC4yIDAsNS44bDUzLjksNTMuOWMwLjgsMC44IDEuOCwxLjIgMi45LDEuMiAxLDAgMi4xLTAuNCAyLjktMS4ybDUzLjktNTMuOWMxLjctMS42IDEuNy00LjIgMC4xLTUuOHoiIGZpbGw9IiNGRkZGRkYiLz4KICA8L2c+Cjwvc3ZnPgo=" />
                </div>
            </div>
            <div class="ttsect ttsect--great">
                <div class="container">
                    <div class="pprow">
                        <div class="uucol-md-5 uucol-sm-7 uucol-sm-offset-4 uucol-md-offset-6">

                        </div>
                    </div>
                </div>
            </div>

            <div class="pprow pprow--margin pprow--center">
                <a href="#" class="llbtn">Move to the top</a>
            </div>
<div class="ttsect-t-fffooter"></div>

<footer class="fffooter">
    <div class="fffooter__half fffooter__half-1">
        <h2 class="fffooter__big-title"></h2>
        <h3 class="ffoter__title"></h3>
        <p class="fffooter__desc"></p>

    </div>
    <div class="fffooter__half fffooter__half-2">
        <h2 class="fffooter__big-title">CONTACT</h2>
        <h3 class="fffooter__title">Phone</h3>
        <p class="fffooter__desc">+(998) 99-704-2009</p>
        <h3 class="fffooter__title">Mail</h3>
        <p class="fffooter__desc">abduakhadovalisher2000@mail.ru</p>
        <h3 class="fffooter__title">Address</h3>
        <p class="fffooter__desc">Artel<br>
            Mirobod district,Tashkent
        </p>
        <a href="https://www.mediapark.uz/products/category/145" class="llbtn llbtn--opacity" target="_blank">Кондиционеры магазин</a>
    </div>
</footer>
</div>

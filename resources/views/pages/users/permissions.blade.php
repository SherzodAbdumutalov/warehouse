@extends('layouts.master')
@section('content')
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4>Контроль прав</h4>
        </div>
        <div class="panel-body">
            <form action="{{ route('save_perm') }}" method="post" class="form-group">
                {{ csrf_field() }}
                <table class="table table-bordered" style="font-size: 11px">
                    <thead>
                    <tr>
                        <th style="background-color: #00aff0">Права\Роли</th>
                        @foreach($roles as $role)
                            <th>{{ $role->name }}</th>
                        @endforeach
                    </tr>
                    </thead>
                    <tbody>
                    @if(!$permissions->isEmpty())
                        @foreach($permissions as $perm)
                            <tr>
                                <td>{{ $perm->name }}</td>

                                @foreach($roles as $role)
                                    <td>
                                        @if($role->hasPermission($perm->name))
                                            <input type="checkbox" name="{{ $role->id }}[]" value="{{ $perm->id }}" checked>
                                        @else
                                            <input type="checkbox" name="{{ $role->id }}[]" value="{{ $perm->id }}" >
                                        @endif
                                    </td>
                                @endforeach
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                <button class="btn btn-warning" type="submit">сохранить</button>
            </form>
        </div>
    </div>
@endsection
@section('additional_css')
    <style>
        .table tbody tr{
            cursor: pointer;
        }
        p{
            margin: 0;
        }
    </style>
@endsection
<div class="modal fade" id="createUser" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close_create_user_modal">
                    <span aria-hidden="true" style="font-size: 32px">&times;</span>
                </button>
                <h3 class="modal-title" id="createUserLabel">добавление пользователя</h3>
            </div>
            <form method="post" id="createUserForm" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="alert alert-info" style="display:none">
                        <ul>

                        </ul>
                    </div>
                    <div class="col-md-12 clearfix">
                        <div class="col-md-3">
                            <label for="firstname"> Имя</label>
                            <input type="text" id="firstname" name="firstname" class="form-control">
                            <label for="surname"> Фамилия</label>
                            <input type="text" id="surname" name="surname" class="form-control">
                            <label for="phone_number"> Тел</label>
                            <input type="text" id="phone_number" name="phone_number" class="form-control">
                            <label for="email"> Email</label>
                            <input type="email" id="email" name="email" class="form-control">
                        </div>
                        <div class="col-md-3">
                            <label for="role_id">Выберите роль</label>
                            <select id="role_id" name="role_id" data-placeholder="" class="standardSelect">
                                @foreach($roles as $role)
                                    <option value="{{ $role->id }}" >{{ $role->name }}</option>
                                @endforeach
                            </select>
                            <div class="clearfix" id="storageDiv" style="display: none">
                                <label for="storage">склады</label>
                                <select id="storage" name="storage[]" data-placeholder="выбрат" class="standardSelect" multiple>
                                    @foreach($storages as $storage)
                                        <option value="{{ $storage->id }}" >{{ $storage->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label for="name"> Логин</label>
                            <input type="text" id="name" name="name" class="form-control">
                            <label for="password"> Пароль</label>
                            <input type="password" id="password" name="password" class="form-control">
                        </div>
                        <div class="col-md-3">
                            <img src="https://www.svgimages.com/svg-image/s5/man-passportsize-silhouette-icon-256x256.png" alt="" id="userPhoto" width="150" height="150" class="img img-rounded">
                            <br>
                            <input type="file" name="userPhotoInput" id="userPhotoInput" accept="image/x-png/x-jpg">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    @if($has_perm)
                        <input type="submit" name="save_user_form" id="save_user_form" style="float: right;" class="btn btn-primary" value="сохранить">
                    @endif
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="userInfo" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close_user_info_modal">
                    <span aria-hidden="true" style="font-size: 32px">&times;</span>
                </button>
                <h3 class="modal-title" id="editUserLabel">изменить пользователя</h3>
            </div>
            <form method="post" id="updateUserForm" enctype="multipart/form-data">
                <input type="hidden" name="user_hidden_id" id="user_hidden_id">
                <div class="modal-body">
                    <div class="alert alert-info" style="display:none">
                        <ul>

                        </ul>
                    </div>
                    <div class="col-md-12 clearfix">
                        <div class="col-md-3">
                            <label for="edit_firstname"> Имя</label>
                            <input type="text" id="edit_firstname" name="edit_firstname" class="form-control" disabled>
                            <label for="edit_surname"> Фамилия</label>
                            <input type="text" id="edit_surname" name="edit_surname" class="form-control" disabled>
                            <label for="edit_phone_number"> Тел</label>
                            <input type="text" id="edit_phone_number" name="edit_phone_number" class="form-control" disabled>
                            <label for="edit_email"> Email</label>
                            <input type="email" id="edit_email" name="edit_email" class="form-control" disabled>
                        </div>
                        <div class="col-md-3">
                            <label for="edit_role_id">Выберите роль</label>
                            <select id="edit_role_id" name="edit_role_id" data-placeholder="" class="standardSelect" disabled>
                                @foreach($roles as $role)
                                    <option value="{{ $role->id }}" >{{ $role->name }}</option>
                                @endforeach
                            </select>
                            <div class="clearfix" id="edit_storageDiv" style="">
                                <label for="edit_storage">склады</label>
                                <select id="edit_storage" name="edit_storage[]" data-placeholder="выбрат" class="standardSelect" multiple disabled>
                                    @foreach($storages as $storage)
                                        <option value="{{ $storage->id }}" >{{ $storage->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label for="edit_name"> Логин</label>
                            <input type="text" id="edit_name" name="edit_name" class="form-control" disabled>
                            <label for="edit_password" style="display: none" id="edit_password_text"> Пароль</label>
                            <input type="password" id="edit_password" name="edit_password" class="form-control" disabled style="display: none">
                        </div>
                        <div class="col-md-3">
                            <img src="https://www.svgimages.com/svg-image/s5/man-passportsize-silhouette-icon-256x256.png" alt="" id="edit_userPhoto" width="150" height="150" class="img img-rounded">
                            <br>
                            <input type="file" name="edit_userPhotoInput" id="edit_userPhotoInput" accept="image/x-png/x-jpg" disabled>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    @if($has_perm)
                        <button type="submit" name="edit_user_form" id="edit_user_form" style="" class="btn btn-warning"><i class='fa fa-save'></i> сохранить</button>
                        <button type="button" class="btn btn-danger" onclick="deactivate_user()" id="deactivate_btn"><i class="fas fa-close"></i> деактивировать</button>
                    @endif
                </div>
            </form>
        </div>
    </div>
</div>
<div class="panel panel-primary">

    <div class="panel-heading" style="padding: 1px 5px">
        пользователи
    </div>
    <div class="panel-body">
        <div class="alert alert-danger" style="display:none">
            <ul>

            </ul>
        </div>
        @if($has_perm)
            <button type="button" class="btn btn-primary " data-toggle="modal" data-target="#createUser"><i class="fa fa-plus"></i> добавить</button>
            <a href="{{ route('permissions') }}" class="btn btn-warning"><i class="fa fa-check"></i> роли/доступы</a>
            <a href="{{ route('inventarization') }}" class="btn btn-info"><i class="fas fa-file-import"></i> инвентаризация</a>
        @endif
        <table class=" table table-bordered table-hover" id="users_table" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>статус</th>
                <th>имя</th>
                <th>фамилия</th>
                <th>логин</th>
                <th>email</th>
                <th>тел</th>
                <th>роль</th>
                <th>склад</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
            <tr>
                <th>статус</th>
                <th>имя</th>
                <th>фамилия</th>
                <th>логин</th>
                <th>email</th>
                <th>тел</th>
                <th>роль</th>
                <th>склад</th>
            </tr>
            </tfoot>
        </table>

    </div>

</div>
@section('additionalLibrary')
    <script>
        $(document).ready(function () {
            fetch_data();
            $('#storage').hide();
            $('#edit_storage').hide();

            $('#users_table').on('dblclick', 'tr', function (e) {
                var id = $(this).data('id');
                $('#userInfo').modal("show");
                show_user_info(id);
            });

            $('#users_table tbody').on( 'click', 'tr', function () {
                if ( $(this).hasClass('selected') ) {
                    // do nothing
                }
                else {
                    $('#users_table').DataTable().$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            });

            $('#role_id').on('change', function (e) {
                var op_val = $('#role_id').val();
                if (op_val==2){
                    $('#storageDiv').show();
                } else {
                    $('#storageDiv').hide();
                }
            });
            $('#edit_role_id').on('change', function (e) {
                var op_val = $('#edit_role_id').val();
                if (op_val==2){
                    $('#edit_storageDiv').show();
                } else {
                    $('#edit_storageDiv').hide();
                }
            });

            $("#userPhotoInput").change(function(){
                readURL(this);
            });
            $("#edit_userPhotoInput").change(function(){
                readURL1(this);
            });
        });


        function readURL(input) {
            var url = input.value;
            var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
            if (input.files && input.files[0]&& (ext == "png" || ext == "jpg" || ext == "jpeg")) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#userPhoto').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }else{
                $('#userPhotoInput').attr('src', 'https://www.svgimages.com/svg-image/s5/man-passportsize-silhouette-icon-256x256.png');
            }
        }

        function readURL1(input) {
            var url = input.value;
            var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
            if (input.files && input.files[0]&& (ext == "png" || ext == "jpg" || ext == "jpeg")) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#edit_userPhoto').attr('src', e.target.result).show();
                };

                reader.readAsDataURL(input.files[0]);
            }else{
                $('#edit_userPhoto').attr('src', '/assets/no_preview.png');
            }
        }

        function fetch_data() {
            $('#users_table').DataTable().destroy();
            $('#users_table').DataTable({
                processing: true,
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    "url": "{{ route('getUsers') }}",
                    "type": "get",
                },
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение _START_ для _END_ из _TOTAL_ записей",
                    "infoEmpty": "данные отсутствует",
                    "infoFiltered": "(<b>отфильтровано из _MAX_ записей</b>)",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr('data-id', data.id);
                },
                columns: [
                    { "data": "is_active", "width":"4%" },
                    { "data": "firstname" },
                    { "data": "surname" },
                    { "data": "name" },
                    { "data": "email" },
                    { "data": "phone" },
                    { "data": "role_name" },
                    { "data": "storage_name" },
                ],
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5, 6, 7]
                        },
                        title:'споисок пользователей',
                        text: '<i class="fa fa-file-excel-o"></i>'

                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5, 6, 7]
                        },
                        title:'споисок пользователей',
                        text: '<i class="fa fa-file-pdf-o"></i>'

                    },
                ],
                paging: false,
                bPaginate: false,
                scrollY: "65vh"
            });
        }

        function show_user_info(id) {
            $('#loading').show();
            $('.alert-danger').hide();
            $('.alert-info').hide();
            $('.alert-success').hide();
            $.ajax({
                type:'get',
                url:'{{ route('getUserValues') }}',
                data:{ user_id:id },
                success: function (data) {
                    var d = new Date();
                    $('#user_hidden_id').val(id);
                    var item = JSON.parse(data);
                    console.log(item)
                    $('#edit_firstname').val(item['user'].firstname);
                    $('#edit_surname').val(item['user'].surname);
                    $('#edit_name').val(item['user'].name);
                    $('#edit_phone_number').val(item['user'].phone_number);
                    $('#edit_role_id').val(item['user'].role_id).trigger('chosen:updated');
                    $('#edit_email').val(item['user'].email);
                    $('#edit_storage').val(item['user_storage']).trigger('chosen:updated');
                    if (item['user'].is_active) {
                        $('#deactivate_btn').removeClass().addClass('btn btn-danger').text('деактивировать');
                    }else{
                        $('#deactivate_btn').removeClass().addClass('btn btn-info').text('активировать');
                    }
                    console.log(item['path']);
                    if (item['path']!=null) {
                        $('#edit_userPhoto').attr('src', item['path']+"?"+d.getTime());
                    }
                    if (item['hasPerm']) {
                        $('#edit_firstname').attr('disabled', false);
                        $('#edit_surname').attr('disabled', false);
                        $('#edit_name').attr('disabled', false);
                        $('#edit_phone_number').attr('disabled', false);
                        $('#edit_role_id').attr('disabled', false).trigger('chosen:updated');
                        $('#edit_email').attr('disabled', false);
                        $('#edit_storage').attr('disabled', false).trigger('chosen:updated');
                        $('#edit_userPhotoInput').attr('disabled', false).show();
                        $('#edit_password_text').show();
                        $('#edit_password').attr('disabled', false).show();
                        $('#update_user_button').show();
                    }
                    if (item['user'].role_id==2) {
                        $('#edit_storageDiv').show();
                    }else{
                        $('#edit_storage').val('').trigger('chosen:updated');
                        $('#edit_storageDiv').hide();
                    }
                    $('#loading').hide()
                },
                error: function (request, status, error) {
                    alert('произошла ошибка');
                    console.log(error)
                }
            })
        }

        $('#createUserForm').on('submit', function(event){
            event.preventDefault();
            $('.alert-info').hide();
            if (confirm('создать?')) {
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'POST',
                    url:"{{route('createUser')}}",
                    contentType: false,
                    cache: false,
                    processData: false,
                    data:new FormData(this),
                    success:function(item){
                        fetch_data();
                        $('#close_create_user_modal').click();
                        $('.alert-danger').show();
                        $('.alert-danger ul').empty().append('<li>добавлен</li>');
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        console.log(json)
                        $('.alert-info').show();
                        $('.alert-info ul').empty()
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').append('<li>'+value+'</li>');
                        })
                    }
                });
            }
        });

        $('#updateUserForm').on('submit', function(event){
            event.preventDefault();
            $('.alert-info').hide();
            var user_id = $("#user_hidden_id").val();
            console.log(user_id)
            if (confirm('изменить?')) {
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'POST',
                    url:"{{route('updateUser')}}",
                    contentType: false,
                    cache: false,
                    processData: false,
                    data:new FormData(this),
                    success:function(item){
                        $('#edit_userPhoto').val('');
                        data = jQuery.parseJSON(item);
                        console.log(data)
                        show_user_info(user_id);
                        fetch_data();
                        $('#close_user_info_modal').click();
                        $('.alert-danger').show();
                        $('.alert-danger ul').empty().append('<li>обнавлено</li>');
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        console.log(json)
                        $('.alert-info').show();
                        $('.alert-info ul').empty()
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').append('<li>'+value+'</li>');
                        })
                    }
                });
            }
        });

        function deactivate_user() {
            var user_id = $('#user_hidden_id').val();
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type:'POST',
                url:"{{route('deleteUser')}}",
                data: {user_id:user_id},
                success: function(item){
                    fetch_data();
                    $('#close_user_info_modal').click();
                    $('.alert-danger').show();
                    $('.alert-danger ul').empty().append('<li>деактивирован</li>');
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    console.log(json)
                    $('.alert-info').show();
                    $('.alert-info ul').empty()
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').append('<li>'+value+'</li>');
                    })
                }
            });
        }
    </script>
@endsection
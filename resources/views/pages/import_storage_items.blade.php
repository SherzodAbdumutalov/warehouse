<div class="panel panel-primary">
    <div class="panel-body">

        <div class="row">
            {!! Form::open(array('route' => 'upload_import_storage_items_file','method'=>'POST','files'=>'true')) !!}

            <div class="col-md-6">
                <div class="form-group">
                    <div class="col-md-6">
                        {!! Form::file('import_storage_items_file', array('class' => 'form-control')) !!}
                        {!! $errors->first('import_storage_items_file', '<p class="alert alert-danger">:message</p>') !!}
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2 text-left">
                        {!! Form::submit('загрузить',['class'=>'btn btn-primary' , 'disabled'=>'true', 'onclick'=>'disabledCheckbox()']) !!}
                    </div>
                </div>
            </div>
            {!! Form::close() !!}

            <div class="col-md-6">
                <form action="{{ route('import_validate_storage_items_file') }}" method="post">
                    {{ csrf_field() }}
                    <div class="col-md-6">
                        <label for="storage_id">выберите склад:</label>
                        <select name="storage_id" id="storage_id" class="standardSelect" onchange="on_change_select()">
                            @foreach($storages as $storage)
                                <option value="{{ $storage->id }}">{{ $storage->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6">
                        <button type="button" class="btn btn-success" onclick="validate_import_storage_items_file()">проверить</button>
                        <button type="submit" class="btn btn-warning" id="import_btn" disabled >импортировать</button>
                        <br>
                        <br>
                        <label for="allow_import">разрешит импортировать</label>
                        <input type="checkbox" name="allow_import" value="1" id="allow_import" onchange="validate_import_storage_items_file()" >
                    </div>
                </form>

            </div>
            <br/>
        </div>

        <div class="panel-body">
            <div class="col-md-3">
                <div class="col-md-6" style="height: 25px; width: 100px; background-color: yellow"></div>
                <div class="col-md-6"><p style="display: block; float: left">пустая ячейка</p></div>
            </div>
            <div class="col-md-3">
                <div class="col-md-6" style="height: 25px; width: 100px; background-color: red"></div>
                <div class="col-md-6"><p style="display: block; float: left">not exist in base</p></div>
            </div>
            <div class="col-md-3">
                <div class="col-md-6" style="height: 25px; width: 100px; background-color: green"></div>
                <div class="col-md-6"><p style="display: block; float: left">дубликат в файле</p></div>
            </div>
            <div class="col-md-3">
                <div class="col-md-6" style="height: 25px; width: 100px; background-color: blue"></div>
                <div class="col-md-6"><p style="display: block; float: left">ошибка в формате</p></div>
            </div>
            <div class="col-md-3">
                <div class="col-md-6" style="height: 25px; width: 100px; background-color: #a8daf3"></div>
                <div class="col-md-6"><p style="display: block; float: left">already exist storage</p></div>
            </div>
            <div class="col-md-3">
                <div class="col-md-6" style="height: 25px; width: 100px; background-color: #00aa88"></div>
                <div class="col-md-6"><p style="display: block; float: left">already exist in current storage</p></div>
            </div>
            <div class="col-md-3">
                <div class="col-md-6" style="height: 25px; width: 100px; background-color: #4cae4c"></div>
                <div class="col-md-6"><p style="display: block; float: left">not equal storage unit</p></div>
            </div>
            <div class="col-md-3">
                <div class="col-md-6" style="height: 25px; width: 100px; background-color: #1d68a7"></div>
                <div class="col-md-6"><p style="display: block; float: left">not exist unit into articul's units</p></div>
            </div>
        </div>
        <br>

    </div>
    <div class="panel-body">
        <table id="import_storage_items_table" class="table table-bordered" style="width:100%">
            <thead>
            <tr>
                <th>артикул нов.</th>
                <th>единица измерения</th>
            </tr>
            </thead>
            <tbody>
            @if(session()->has('import_storage_items_file'))
                @foreach(session()->get('import_storage_items_file') as $arr)
                    <tr>
                        <td>{{ $arr['articula_new'] }}</td>
                        <td>{{ $arr['unit'] }}</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>

</div>
@section('additionalLibrary')
    <script>
        $(document).ready(function() {

            $('input:file').change(
                function(){
                    if ($(this).val()) {
                        $('input:submit').attr('disabled',false);
                    }
                }
            );

            get_storage_items()
        } );

        function validate_import_storage_items_file() {
            var allowImport = false
            var isCorrect = true;

            var import_items = <?php if (session()->has('import_storage_items_file')) {
                echo json_encode(session()->get('import_storage_items_file'));
            } else {
                echo '1';
            } ?>;

            var all_items = <?php echo json_encode($items); ?>;
            var units = <?php echo json_encode($units); ?>;
            var storage_items = <?php echo json_encode($storage_items); ?>;
            var importing_storage = localStorage.getItem('w_all_items')

            console.log(storage_items)

            var table = document.getElementById('import_storage_items_table')
            var all_items_arr = []
            var all_units_arr = []
            for (var j = 0; j < all_items.length; j++) {
                all_items_arr.push(all_items[j].articula_new)
            }

            for (var j = 0; j < units.length; j++) {
                all_units_arr.push(units[j].unit)
            }

            for (var i = 0; i < import_items.length; i++) {
                var trr = table.getElementsByTagName('tr')[i + 1]

                if (import_items[i].unit == null) {
                    isCorrect = false
                    trr.getElementsByTagName('td')[1].style.backgroundColor = 'yellow'
                } else {
                    if (all_units_arr.includes(import_items[i].unit)){
                        trr.getElementsByTagName('td')[1].style.backgroundColor = 'transparent'
                    } else {
                        isCorrect = false
                        trr.getElementsByTagName('td')[1].style.backgroundColor = 'blue'
                    }
                }

                if (import_items[i].articula_new+'' == null) {
                    isCorrect = false
                    trr.getElementsByTagName('td')[0].style.backgroundColor = 'yellow'
                } else {
                    if (!all_items_arr.includes(import_items[i].articula_new+'')) {
                        isCorrect = false
                        trr.getElementsByTagName('td')[0].style.backgroundColor = 'red'
                    }
                    else {
                        if (!isDuplicat(import_items[i].articula_new, import_items)){
                            isCorrect = false
                            trr.getElementsByTagName('td')[0].style.backgroundColor = 'green'
                        } else {
                            if (is_already_exist(import_items[i].articula_new+'', JSON.parse(importing_storage))){
                                trr.getElementsByTagName('td')[0].style.backgroundColor = '#00aa88'
                            } else {
                                var x = contains_other_storage(import_items[i].articula_new+'', import_items[i].unit+'', storage_items)
                                if (x==1) {
                                    trr.getElementsByTagName('td')[0].style.backgroundColor = '#a8daf3'
                                }
                                else if (x==-1){
                                    isCorrect = false
                                    if (!isExistUnit(import_items[i].articula_new+'', import_items[i].unit, all_items)){
                                        trr.getElementsByTagName('td')[1].style.backgroundColor = '#1d68a7'
                                    } else {
                                        trr.getElementsByTagName('td')[1].style.backgroundColor = '#4cae4c'
                                    }
                                }
                                else if (x==0) {
                                    trr.getElementsByTagName('td')[0].style.backgroundColor = 'transparent'
                                }
                            }
                        }
                    }

                }
            }



            if ($('#allow_import').checked){
                if (isCorrect){
                    $('#import_btn').attr('disabled', false);
                }
            }else{
                if (isCorrect){
                    $('#import_btn').attr('disabled', false);
                }
            }
        }

        function isExistUnit(articula_new, unit, arr) {
            for (var i=0; i<arr.length; i++){
                if (articula_new==arr[i].articula_new+''){
                    var unit_arr = arr[i].units.split(',')
                    if (unit_arr.includes(unit)){
                        return true
                    } else {
                        return false
                    }
                }
            }
        }

        function is_already_exist(articula_new, all) {
            // for (var i=0; i<all.length; i++){
            //     if (articula_new==all[i].articula_new){
            //         return true
            //     }
            // }
            return true
        }

        function isDuplicat(articula_new, all_import_artiucla_new) {
            var count = 0
            for (var i=0; i<all_import_artiucla_new.length; i++){
                if (articula_new+''==all_import_artiucla_new[i].articula_new+''){
                    count++
                }
            }
            if (count>=2){
                return false
            } else {
                return true
            }
        }

        function contains_other_storage(articula_new, unit, storages_item) {
            for (var i=0; i<storages_item.length; i++){
                if (articula_new==storages_item[i].articula_new){
                    if (unit==storages_item[i].unit){
                        return 1
                    } else {
                        return -1
                    }
                }
            }
            return 0
        }

        function on_change_select() {
            get_storage_items()
        }

        function get_storage_items(){
            var to_storage_id = $('#to_storage_id').val();
            var item_id = sessionStorage.getItem('last_item_id_in_request');
            var model_id = sessionStorage.getItem('model_id');
            $('#item_id').append('<option value="" >выбрать...</option>');
            $.ajax({
                type:'get',
                url:"<?php echo e(route('requestItemsByStorage')); ?>",
                data:{to_storage_id:to_storage_id, model_id:model_id},
                success: function (data) {
                    var items = JSON.parse(data);
                    $('#item_id').empty().trigger("chosen:updated");
                    if (items.length>0){
                        items.forEach(function (data, index) {
                            if (item_id==data.id){
                                if (data.sap_code == null) {
                                    $('#item_id').append('<option value="'+data.id+'" selected>'+data.articula_new+' '+data.item_name+'</option>')
                                }else{
                                    $('#item_id').append('<option value="'+data.id+'" selected>'+data.articula_new+' '+data.sap_code+' '+data.item_name+'</option>')
                                }
                            } else {
                                if (data.sap_code == null) {
                                    $('#item_id').append('<option value="'+data.id+'">'+data.articula_new+' '+data.item_name+'</option>')
                                }else{
                                    $('#item_id').append('<option value="'+data.id+'">'+data.articula_new+' '+data.sap_code+' '+data.item_name+'</option>')
                                }
                            }
                        });
                        $('#item_id').trigger("chosen:updated");
                    }
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText)
                    console.log(json)
                    $('#loading').hide()
                    $('.alert-info').show();
                    $('.alert-info ul').empty();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').append('<li>'+value+'</li>');
                    })
                }
            });
        }

        {{--function get_storage_items() {--}}
            {{--var storage_id = $('#storage_id').val();--}}
            {{--$.ajax({--}}
                {{--type:'get',--}}
                {{--url:'{{ \Illuminate\Support\Facades\URL::route('get_storage_items_with_units') }}',--}}
                {{--data:{storage_id:storage_id},--}}
                {{--success:function (data) {--}}
                    {{--// console.log(data)--}}
                    {{--localStorage.setItem('w_all_items', JSON.stringify(data))--}}
                {{--}--}}
            {{--})--}}
        {{--}--}}
    </script>
@endsection
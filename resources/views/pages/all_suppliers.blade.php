    @section('additional_css')
    <style>
        .chosen-container{
            width: 100%!important;
        }
        .table tbody tr{
            cursor: pointer;
        }
    </style>
@endsection
<div class="panel panel-primary">
    <div class="panel-heading" style="padding: 1px 5px">
        Все фирмы
    </div>

    <div class="panel-body">
        <div class="alert alert-danger" style="display:none">
            <ul></ul>
        </div>
        <button type="button" class="btn btn-info " data-toggle="modal" data-backdrop="static" data-keyboard="false" style="font-size: 12px; display: none" data-target="#show_create_modal" id="create_supplier_btn"><i class="fa fa-plus"></i> создать</button>
        <table id="suppliers_table" class="table table-hover table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>№</th>
                <th>статус</th>
                <th>поставщик</th>
                <th>адрес</th>
                <th>телефон</th>
                <th>веб сайт</th>
                <th>тип поставщика</th>
                <th>страна</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
            <tfoot>
            <tr>
                <th>№</th>
                <th>статус</th>
                <th>поставщик</th>
                <th>адрес</th>
                <th>телефон</th>
                <th>веб сайт</th>
                <th>тип поставщика</th>
                <th>страна</th>
            </tr>
            </tfoot>
        </table>
    </div>

    <div id="show_detail" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <p style="display: inline-block; color: #000;" id="supplier_info"></p>
                    <button type="button" class="close" data-dismiss="modal" id="close_detail_modal"><span style="font-size: 24px">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-info" style="display:none">
                        <ul>

                        </ul>
                    </div>
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#byHand">ручная привязка</a></li>
                        <li><a data-toggle="tab" href="#supplier_edit_tab">изменить поставщика</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="supplier_edit_tab" class="tab-pane fade" style="padding-top: 10px">
                            <form>
                                <div class="col-md-6">
                                    <label for="edit_supplier_type">тип поставшика</label>
                                    <select name="edit_supplier_type" id="edit_supplier_type" class="standardSelect">
                                    </select>
                                    <label for="edit_phone">телефон поставщика</label>
                                    <input type="text" name="edit_phone" id="edit_phone" class="form-control" value="">
                                </div>
                                <div class="col-md-6">
                                    <label for="edit_country">страна</label>
                                    <select name="edit_country" id="edit_country" class="standardSelect">
                                    </select>
                                    <label for="edit_website">вебсайт поставщика</label>
                                    <input type="text" name="edit_website" id="edit_website" class="form-control" value="">
                                </div>

                                <div class="clearfix" style="padding: 0 15px">
                                    <label for="edit_supplier_name">имя поставщика</label>
                                    <input type="text" name="edit_supplier_name" id="edit_supplier_name" class="form-control" value="">
                                    <label for="edit_address">адресс поставщика</label>
                                    <input type="text" name="edit_address" id="edit_address" class="form-control" value="">
                                    <input type="hidden" name="edit_supplier_id" id="edit_supplier_id" value="">
                                    <button type="button" class="btn btn-warning" onclick="update_supplier()" style="float: right; margin-top: 10px; display: none" id="update_supplier_btn">Сохранить</button>
                                </div>
                            </form>
                        </div>
                        <div id="byHand" class="tab-pane fade in active" style="padding-top: 10px">
                            <div class="clearfix" id="panelOfAdding" style="display: none">
                                <div class="col-md-1" style="width: 20px">
                                    <button class="btn btn-info btn-circle" type="button" id="openCloseBtn" style="width: 30px; height: 30px"><i class="fa fa-plus"></i></button>
                                </div>
                                <div class="col-md-11">
                                    <div class="clearfix" id="add_by_hand" style="">
                                        <div class="col-md-11">
                                            <select name="items" id="items" class="standardSelect" multiple data-placeholder="добавить элемент">
                                            </select>
                                        </div>
                                        <div class="col-md-1">
                                            <input type="hidden" name="hidden_supplier_id" id="hidden_supplier_id">
                                            <button type="button" class="btn btn-warning " onclick="create_item()">добавить</button>
                                        </div>
                                    </div>
                                    <div class="clearfix" id="add_by_file" style="display: none">
                                        <div class="alert alert-warning" style="display: none">
                                            <ul></ul>
                                        </div>
                                        <form action="" id="import_supplier_item_file">
                                            {{ csrf_field() }}
                                            <div class="col-md-6">
                                                <input type="hidden" name="file_supplier_id" id="file_supplier_id">
                                                <input type="file" name="supplier_item_file" id="supplier_item_file" class="form-control">
                                            </div>
                                            <div class="col-md-1">
                                                <button class="btn btn-warning" type="submit" style="padding: 3px 15px">добавить</button>
                                            </div>
                                            <div class="col-md-1">
                                                <a href="{{ asset('assets') }}/template/postovshik.xlsx" download><span class="glyphicon glyphicon-download" style="font-size: 24px">&nbsp;шаблон</span></a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <table id="detail_table" class="table table-hover table-bordered" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>артикул</th>
                                    <th>стр. артикул</th>
                                    <th>SAP код</th>
                                    <th>наименование</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>артикул</th>
                                    <th>стр. артикул</th>
                                    <th>SAP код</th>
                                    <th>наименование</th>
                                    <th></th>
                                </tr>
                                </tfoot>
                            </table>
                            <button type="button" class="btn btn-danger " onclick="delete_all_items()" style="float: right; margin-top: 10px; display: none" id="delete_items_btn">удалить всё</button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" style="float: right; margin-top: 10px; display: none" id="delete_supplier_btn" onclick="delete_supplier()">удалить поставщика</button>
                </div>
            </div>

        </div>
    </div>
</div>

{{--edit supplier--}}
<div id="show_create_modal" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 50%">
        <form>
            <div class="modal-content" >
                <div class="modal-header">
                    создать
                    <button type="button" class="close" data-dismiss="modal" style="font-size: 24px">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="col-md-6">
                        <label for="supplier_type">тип поставщика</label>
                        <select name="supplier_type" id="supplier_type" class="standardSelect">
                            @foreach($sup_types as $key=>$sup_type)
                                <option value="{{ $key }}" >{{ $sup_type }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label for="country">страна</label>
                        <select name="country" id="country" class="standardSelect">
                            @foreach($countries as $key=>$country)
                                <option value="{{ $country }}" {{ ($key=='UZ')?'selected':'' }}>{{ $country }}</option>
                            @endforeach
                        </select>
                    </div>

                    <br>
                    <br>
                    <label for="name">имя поставщика</label>
                    <input type="text" name="supplier_name" id="supplier_name" class="form-control" required>
                    <label for="address">адрес</label>
                    <input type="text" name="address" id="address" class="form-control" required>
                    <label for="phone">телефон</label>
                    <input type="text" name="phone" id="phone" class="form-control" required>
                    <label for="website">веб сайт</label>
                    <input type="text" name="website" id="website" class="form-control">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" onclick="create_supplier()">Сохранить</button>
                </div>
            </div>
        </form>
    </div>
</div>
@section('additionalLibrary')
    <script>
        $(document).ready(function() {
            $(".standardSelect").chosen({
                search_contains: true,
                enable_split_word_search: true
            }).trigger('chosen:update');
            $('#loading').hide();
            fetch_suppliers_table();
            $('#suppliers_table').on('dblclick', 'tr', function (e) {
                var id = $(this).data('id');
                $('#hidden_supplier_id').val(id);
                $('#show_detail').modal("show");
                show_detail(id);
            });
            $('#suppliers_table tbody').on( 'click', 'tr', function () {
                if ( $(this).hasClass('selected') ) {
                    // do nothing
                }
                else {
                    $('#suppliers_table').DataTable().$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            });
            $('a[data-toggle="tab"]').on('shown.bs.tab', function () {
                $('#detail_table').DataTable().columns.adjust().draw();
                $('.alert-danger').hide();
                $('.alert-info').hide();
                $('.alert-success').hide();
            });

            $('#openCloseBtn').click(function () {
                $('#add_by_hand').toggle();
                $('#add_by_file').toggle();
                if ($('#add_by_hand').is(':visible')) {
                    $('#add_by_file i').removeClass('fa fa-plus').addClass('fa fa-minus');
                }else{
                    $('#add_by_file i').removeClass('fa fa-minus').addClass('fa fa-plus');
                }
            });
        } );

        function get_items(supplier_id) {
            $.ajax({
                url: '{{ route('get_fit_items') }}',
                type: 'get',
                data: {supplier_id:supplier_id},
                success: function (data) {
                    var items = $.parseJSON(data);
                    $('#items').empty();
                    items.forEach(function (value, key) {
                        $('#items').append('<option value="'+value.id+'">'+value.articula_new+' '+value.articula_old+' '+value.item_name+'</option>');
                    })
                    $('#items').trigger("chosen:updated");
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    $('.alert-info').show();
                    $('.alert-info ul').empty()
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').append('<li>'+value+'</li>');
                    })
                }
            })
        }

        function fetch_suppliers_table() {
            $('#suppliers_table').DataTable().destroy();
            $('#suppliers_table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    type:'get',
                    url:"{{route('getSuppliers')}}"
                },
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение от _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "нет записей",
                    "infoFiltered": "(<b>отфильтровано из _MAX_ записей</b>)",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr('data-id', data.id);
                },
                pageLength: 100,
                lengthMenu:[[100, 500, 1000, -1], [100, 500, 1000, "All"]],
                stateSave: true,
                "columns": [
                    { "data": "supplier_id", "width":"3%" },
                    { "data": "is_active", "width":"3%" },
                    { "data": "name", "width":"30%" },
                    { "data": "address", "width":"24%" },
                    { "data": "phone_number", "width":"10%" },
                    { "data": "website", "width":"10%" },
                    { "data": "supplier_type", "width":"10%" },
                    { "data": "country", "width":"10%" },
                ],
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5, 6]
                        },
                        title:'бум лист',
                        text: '<i class="fa fa-file-excel-o"></i>'

                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5, 6]
                        },
                        title:'бум лист',
                        text: '<i class="fa fa-file-pdf-o"></i>'

                    },
                ],
                initComplete: function (data) {
                    if (data.json.hasPerm) {
                        $('#create_supplier_btn').show();
                        $('#delete_supplier_btn').show();
                        $('#delete_items_btn').show();
                        $('#update_supplier_btn').show();
                        $('#panelOfAdding').show();
                    }
                },
                scrollY: "60vh",
                scrollCollapse: true
            });
        }

        function edit_supplier() {
            var supplier_id = $('#hidden_supplier_id').val();
            $.ajax({
                type:'get',
                url:"{{route('editSupplier')}}",
                data:{supplier_id:supplier_id},
                success: function (data) {
                    var item = $.parseJSON(data);
                    $('#edit_supplier_name').val(item.supplier.name);
                    $('#edit_address').val(item.supplier.address);
                    $('#edit_phone').val(item.supplier.phone);
                    $('#edit_website').val(item.supplier.website);
                    var countries = item.countries;
                    $('#edit_supplier_type').empty();
                    $.each(item.supplier_types, function (index, value) {
                        if (index === item.supplier.type) {
                            $('#edit_supplier_type').append('<option value="'+index+'" selected>'+value+'</option>')
                        }else{
                            $('#edit_supplier_type').append('<option value="'+index+'">'+value+'</option>')
                        }
                    });
                    $('#edit_supplier_type').trigger("chosen:updated");
                    $('#edit_country').empty();
                    $.each(countries, function (index, value) {
                        if (value === item.supplier.country) {
                            $('#edit_country').append('<option value="'+value+'" selected>'+value+'</option>')
                        }else{
                            $('#edit_country').append('<option value="'+value+'">'+value+'</option>')
                        }
                    });
                    $('#edit_country').trigger("chosen:updated");
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    $('.alert-danger').show();
                    $('.alert-danger ul').empty()
                    $.each(json.errors, function (index, value) {
                        $('.alert-danger ul').append('<li>'+value+'</li>');
                    })
                }
            })
        }

        function update_supplier() {
            if (confirm('изменить')) {
                var supplier_id = $('#hidden_supplier_id').val();
                var supplier_name = $('#edit_supplier_name').val();
                var phone = $('#edit_phone').val();
                var address = $('#edit_address').val();
                var website = $('#edit_website').val();
                var supplier_type = $('#edit_supplier_type').val();
                var country = $('#edit_country').val();
                console.log(supplier_id)
                console.log(supplier_type)
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'POST',
                    url:"{{route('updateSupplier')}}",
                    data:{supplier_id:supplier_id, supplier_name:supplier_name, phone:phone, address:address, website:website, supplier_type:supplier_type, country:country},
                    success: function (data) {
                        console.log(data)
                        fetch_suppliers_table();
                        $('.alert-info').show();
                        $('.alert-info ul').empty().append('<li>обнавлен</li>');
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        console.log(json)
                        $('.alert-info').show();
                        $('.alert-info ul').empty()
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').append('<li>'+value+'</li>');
                        })
                    }
                })
            }
        }

        function delete_supplier() {
            var supplier_id = $('#hidden_supplier_id').val();
            console.log(supplier_id)
            if (confirm('Удалить?')) {
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'POST',
                    url:"{{route('deleteSupplier')}}",
                    data:{supplier_id:supplier_id},
                    success: function (data) {
                        fetch_suppliers_table();
                        $('.alert-danger').show();
                        $('.alert-danger ul').empty().append('<li>удален</li>');
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        console.log(json)
                        $('.alert-danger').show();
                        $('.alert-danger ul').empty()
                        $.each(json.errors, function (index, value) {
                            $('.alert-danger ul').append('<li>'+value+'</li>');
                        })
                    }
                })
            }
        }

        function show_detail(supplier_id) {
            // $('#hidden_supplier_id').val(supplier_id);
            $('#file_supplier_id').val(supplier_id);
            var x = $('#row_'+supplier_id);
            $('#supplier_info').text(' №: '+x.find('td:eq(0)').text()+' поставщик: '+x.find('td:eq(2)').text()+' страна: '+x.find('td:eq(3)').text());
            $('#loading').show();
            $('.alert-info').hide();
            get_items(supplier_id);
        }

        $('#show_detail').on('shown.bs.modal', function() {
            fetch_detail_table();
            edit_supplier()
        });

        function fetch_detail_table() {
            var supplier_id = $('#hidden_supplier_id').val();
            $('#detail_table').DataTable().destroy();
            $('#detail_table').DataTable({
                processing: true,
                ajax:{
                    type:'get',
                    url:"{{route('show_supplier_items_list')}}",
                    data:{supplier_id:supplier_id}
                },
                columnDefs:[
                    { orderable: false, targets: [4] }
                ],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение от _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "",
                    "infoFiltered": "(<b>отфильтровано из _MAX_ записей</b>)",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                pageLength: 100,
                lengthMenu:[[100, 500, 1000, -1], [100, 500, 1000, "All"]],
                columns: [
                    { "data": "articula_new" },
                    { "data": "articula_old" },
                    { "data": "sap_code" },
                    { "data": "item_name" },
                    { "data": "delete_item" }
                ],
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3]
                        },
                        title:'бум лист',
                        text: '<i class="fa fa-file-excel-o"></i>'

                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3]
                        },
                        title:'бум лист',
                        text: '<i class="fa fa-file-pdf-o"></i>'

                    },
                ],
                scrollY: '40vh',
            });
            $('#loading').hide();
        }

        function create_item() {
            if (confirm('сохранить')) {
                var supplier_id = $('#hidden_supplier_id').val();
                var items = $('#items').val();

                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'POST',
                    url:"{{route('add_supplier_item_list')}}",
                    data:{supplier_id:supplier_id, items:JSON.stringify(items)},
                    success: function (data) {
                        $('#items').val('').trigger('chosen:updated');
                        fetch_detail_table();
                        $('.alert-info').show();
                        $('.alert-info ul').empty().append('<li>сохранено</li>');
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        console.log(json)
                        $('.alert-info').show();
                        $('.alert-info ul').empty()
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').append('<li>'+value+'</li>');
                        })
                    }
                })
            }
        }

        function delete_list_item(item_list_id){
            if (confirm('удалить')) {
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'POST',
                    url:"{{route('delete_supplier_list_item')}}",
                    data:{item_list_id:item_list_id},
                    success: function () {
                        fetch_detail_table();
                        $('.alert-info').show();
                        $('.alert-info ul').empty().append('<li>удален</li>');
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        $('.alert-info').show();
                        $('.alert-info ul').empty()
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').append('<li>'+value+'</li>');
                        })
                    }
                })
            }
        }

        function create_supplier() {
            var supplier_name = $('#supplier_name').val();
            var phone = $('#phone').val();
            var address = $('#address').val();
            var website = $('#website').val();
            var supplier_type = $('#supplier_type').val();
            var country = $('#country').val();
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type:'POST',
                url:"{{route('createSupplier')}}",
                data:{supplier_name:supplier_name, phone:phone, address:address, website:website, supplier_type:supplier_type, country:country},
                success: function (data) {
                    fetch_suppliers_table();
                    $('.alert-danger').show();
                    $('.alert-danger ul').empty().append('<li>сохранен</li>');
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    console.log(json)
                    $('.alert-danger').show();
                    $('.alert-danger ul').empty()
                    $.each(json.errors, function (index, value) {
                        $('.alert-danger ul').append('<li>'+value+'</li>');
                    })
                }
            })
        }

        $('#import_supplier_item_file').on('submit', function(event){
            $('#loading').show()
            event.preventDefault();
            $.ajax({
                url:"{{ route('upload_supplier_items_file') }}",
                method:"POST",
                data:new FormData(this),
                dataType:'JSON',
                contentType: false,
                cache: false,
                processData: false,
                success:function(data)
                {
                    console.log(data)
                    var badItems = data;
                    console.log(badItems)
                    fetch_detail_table();
                    $('.alert-warning').show();
                    $('.alert-warning ul').empty();
                    if (badItems.length>0) {
                        $.each(badItems, function (index, value) {
                            $('.alert-warning ul').append('<li style="color: black">'+value.articula_new+'-'+value.error+'</li>')
                        });
                    }
                    document.getElementById("import_supplier_item_file").reset()

                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    console.log(json)
                    $('.alert-info').show();
                    $('.alert-info ul').empty()
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').append('<li>'+value+'</li>');
                    })
                }
            })
        });

        function delete_all_items() {
            var supplier_id = $('#hidden_supplier_id').val()
            if (confirm('удалить всё?')) {
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'post',
                    url: '{{ route('delete_all_supplier_items') }}',
                    data:{supplier_id:supplier_id},
                    success: function (data) {
                        fetch_detail_table();
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        $('.alert-info').show();
                        $('.alert-info ul').empty()
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').append('<li>'+value+'</li>');
                        })
                    }
                })
            }
        }
    </script>
@endsection

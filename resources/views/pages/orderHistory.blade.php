@section('additional_css')
    <style>
        .proc p {
            overflow: hidden;
            text-overflow: ellipsis;
            max-width: 420px;
            color: black;
        }
        .progress{
            margin-bottom: 5px;
            background-color: #efe1e1;
        }

        table tbody td {
            min-width: 130px;
        }

        button{
            margin-top: 5px;
        }
        #logistics_table tbody tr{
            cursor: pointer;
        }
    </style>
@endsection
<form action="{{ route('excelExport') }}" name="excelExportForm" id="excelExportForm" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="order_id" id="order_id" value="">
</form>
<div class="panel panel-primary" style="padding-bottom: 1px">
    <div class="panel-heading" style="padding: 1px 5px">
        История заказов
    </div>
    <div class="alert alert-danger" style="display:none">

    </div>
    <div class="panel-body" style="overflow-x: auto">
        <div class="col-md-12 clearfix" style="padding-left: 0">

            <div class="col-md-1" style="float: right">
                <select name="filter_by_supplier_type" id="filter_by_supplier_type" class="standardSelect" data-placeholder="тип поставщика (фильтр)">
                    <option value="0">все типы</option>
                    <option value="2">импорт</option>
                    <option value="1">местный</option>
                </select>
            </div>
            {{--<div class="col-md-2" style="float: right" id="statusDiv">--}}
                {{--<select name="stateFilter" id="stateFilter" class="standardSelect" data-placeholder="статус (фильтр)">--}}
                    {{--<option value="0">статус фильтр</option>--}}
                    {{--@foreach($states as $key=>$item)--}}
                        {{--<option value="{{ $key }}">{{ $item }}</option>--}}
                    {{--@endforeach--}}
                {{--</select>--}}
            {{--</div>--}}
            <div class="col-md-2" style="float: right; display: none" id="storageDiv">
                <select name="filter[]" id="filter_by_to_storage" class=" multipleChosen" data-placeholder="на склад (фильтр)" multiple>
                    @foreach($toStorages as $storage)
                        <option value="{{ $storage->id }}">{{ $storage->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2" style="float: right; display: none" id="supplierDiv">
                <select name="filter_by_type" id="filter_by_type" class="standardSelect" data-placeholder="на склад (фильтр)">
                    <option value="2">Импорт поставщик</option>
                    <option value="1">Местный поставщик</option>
                </select>
            </div>
            <div class="col-md-2" style="float: right">
                <select name="filter[]" id="filter_by_from_storage" class=" multipleChosen" data-placeholder="со склада (фильтр)" multiple>
                    @foreach($fromStorages as $storage)
                        <option value="{{ $storage->id }}">{{ $storage->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-3" style="float: right; text-align: right">
                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    дата фильтр
                </button>
                <div class="collapse" id="collapseExample">
                    <div class="card card-body" style="position: relative; text-align: left">
                        <span>дата отгрузки</span><br>
                        <span><i class="fa fa-calendar"></i> от:</span> <input type="date" value="" id="upload_start" name="upload_start_date" onkeydown="return false" style="font-size: 11px"/>
                        <span><i class="fa fa-calendar"></i> до:</span> <input type="date" value="" id="upload_end" name="upload_end_date" onkeydown="return false" style="font-size: 11px"/>
                        <br><span>дата прихода</span><br>
                        <span><i class="fa fa-calendar"></i> от:</span> <input type="date" value="" id="input_start" name="input_start_date" onkeydown="return false" style="font-size: 11px"/>
                        <span><i class="fa fa-calendar"></i> до:</span> <input type="date" value="" id="input_end" name="input_end_date" onkeydown="return false" style="font-size: 11px"/>
                        <br><span>дата заказа</span><br>
                        <span><i class="fa fa-calendar"></i> от:</span> <input type="date" value="" id="order_start" name="order_start_date" onkeydown="return false" style="font-size: 11px"/>
                        <span><i class="fa fa-calendar"></i> до:</span> <input type="date" value="" id="order_end" name="order_end_date" onkeydown="return false" style="font-size: 11px"/>
                        <button class="btn btn-danger" type="button" style="float: right" id="resetDateFilter">сброс</button>
                    </div>
                </div>
            </div>


            <table id="history_table" class="nowrap table table-hover table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>артикул</th>
                    <th>SAP</th>
                    <th>наименования</th>
                    <th>поставщик</th>
                    <th>тип пост.</th>
                    <th>дата заказа</th>
                    <th>дата отгрузки</th>
                    <th>дата прихода</th>
                    <th>invoice заказа</th>
                    <th>invoice лог.</th>
                    <th>№ транспорта</th>
                    <th>цена за ед.</th>
                    <th>валюта</th>
                    <th>кол-во в зак.</th>
                    <th>кол-во в лог.</th>
                    <th>в пути</th>
                    <th>прибыло</th>
                    <th>приход</th>
                    <th>ост. пост-ка</th>
                    <th>ед. изм.</th>
                    <th>склад</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                <tr>
                    <th>артикул</th>
                    <th>SAP</th>
                    <th>наименования</th>
                    <th>поставщик</th>
                    <th>тип пост.</th>
                    <th>дата заказа</th>
                    <th>дата отгрузки</th>
                    <th>дата прихода</th>
                    <th>invoice заказа</th>
                    <th>invoice лог.</th>
                    <th>№ транспорта</th>
                    <th>цена за ед.</th>
                    <th>валюта</th>
                    <th>кол-во в зак.</th>
                    <th>кол-во в лог.</th>
                    <th>в пути</th>
                    <th>прибыло</th>
                    <th>приход</th>
                    <th>ост. пост-ка</th>
                    <th>ед. изм.</th>
                    <th>склад</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>

    {{--edit amount--}}
    <div id="edit_amount_modal" class="modal fade" role="dialog" style="z-index: 999999">
        <div class="modal-dialog modal-sm" style="width: 20%">
            <form id="update_item_amount" method="post">
                {{ csrf_field() }}
                <div class="modal-content" >
                    <div class="modal-header">
                        изменить
                        <button type="button" class="close" data-dismiss="modal" style="font-size: 24px" id="close_edit_amount_modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-warning" style="display: none">
                            <ul></ul>
                        </div>
                        <label for="edit_amount">кол-во</label>
                        <input type="number" name="edit_amount" id="edit_amount" class="form-control" onkeyup="if(parseFloat(this.value) >= parseFloat(this.max)) this.value = this.max;">
                        <span id="maxValueOfOrder"></span>
                        <input type="hidden" name="hidden_edit_logistics_list_id" id="hidden_edit_logistics_list_id" class="form-control">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-warning " onclick="update_logistics_list()"><i class="fa fa-save"></i> Сохранить</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div id="updateLogistics" class="modal fade" role="dialog" style="z-index: 999999">
        <div class="modal-dialog"  style="width: 30%;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <p style="color: #000; font-size: 12px; display: inline-block; padding: 0; margin: 0" id="logisticsInfoHeader"></p>
                    <button type="button" class="close" data-dismiss="modal" id="close_edit_log_modal"><span style="font-size: 24px; padding: 0; margin: 0">&times;</span></button>
                </div>
                <div class="alert alert-success" style="display:none; color: black">
                    <ul>

                    </ul>
                </div>
                <form action="" method="post">
                    <div class="modal-body" style="height: 50vh;">
                        <label for="edit_invoice_number">invoice number</label>
                        <input type="text" name="edit_invoice_number" id="edit_invoice_number" class="form-control" >
                        <label for="delivery_time">дата отгрузки</label>
                        <input type="date" name="delivery_time" id="delivery_time" class="form-control" >
                        <label for="truck_number">контейнер</label>
                        <input type="text" name="truck_number" id="truck_number" class="form-control" >
                        <div class="col-md-12">
                            <label for="description">Примечание</label>
                            <input type="text" name="description" id="description" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" onclick="update_logistics()"><i class="fa fa-save"></i> сохранить</button>
                    </div>
                </form>
            </div>

        </div>
    </div>

    <div id="show_detail" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" id="close_detail_modal"><span style="font-size: 24px">&times;</span></button>
                    <p id="logisticsHeaderInfo" style="display: block; color: black; margin: 0; padding: 0"></p>
                </div>
                <div class="modal-body">
                    <div class="alert alert-info" style="display:none">
                        <ul>

                        </ul>
                    </div>
                    {{--<form action="">--}}
                    {{--<div class="col-md-4" style="padding-left: 0;">--}}
                    {{--<select name="notContainedItems" id="notContainedItems" data-placeholder="выбрать деталь..." class="standardSelect">--}}
                    {{--</select>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-4" style="padding-left: 0;">--}}
                    {{--<select name="currentItemInvoice" id="currentItemInvoice" data-placeholder="выбрать invoice..." class="standardSelect">--}}
                    {{--</select>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-1">--}}
                    {{--<input type="number" class="form-control" id="amount" name="amount" placeholder="кол-во" min="0" style="height: 28px">--}}
                    {{--</div>--}}
                    {{--<div class="col-md-1">--}}
                    {{--<button type="button" class="btn btn-warning " id="btnAdd" onclick="add_new_item_to_logistics()"><i class="fa fa-plus"></i> добавить</button>--}}
                    {{--</div>--}}
                    {{--</form>--}}
                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#updateLogistics">
                        изменить логистику
                    </button>
                    <input type="hidden" id="logistics_id">
                    <table id="detail_table" class="table table-hover table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>артикул</th>
                            <th>SAP код</th>
                            <th>наименование</th>
                            <th>item name</th>
                            <th>invoice №</th>
                            <th>кол-во</th>
                            <th>приход</th>
                            <th>ед. изм.</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>артикул</th>
                            <th>SAP код</th>
                            <th>наименование</th>
                            <th>item name</th>
                            <th>invoice №</th>
                            <th id="total_amount">кол-во</th>
                            <th>приход</th>
                            <th>ед. изм.</th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" onclick="saveArrivedLogistics()" id="saveLogisticsBtn" style="display: none" disabled>прибыл <i class="fa fa-save"></i></button>
                    <button type="button" class="btn btn-danger" onclick="closeLogistics()" id="closeLogisticsBtn" style="display: none" disabled>закрыть <i class="fa fa-close"></i></button>
                    {{--<button type="button" class="btn btn-danger" onclick="delete_logistics_func()" id="deleteLogisticsBtn" style="display: none" disabled>удалить <i class="fa fa-trash"></i></button>--}}
                </div>
            </div>

        </div>
    </div>

</div>
@section('additionalLibrary')
    <script>
        $(document).ready(function() {
            var now = new Date();

            var day = ("0" + now.getDate()).slice(-2);
            var month = ("0" + (now.getMonth() + 1)).slice(-2);

            var today = now.getFullYear()+"-"+(month)+"-"+(day);
            // $('#start').val(today);
            $('#upload_end').val(today);
            $('#input_end').val(today);
            $('#order_end').val(today);

            $('#l_upload_end').val(today);
            $('#l_order_end').val(today);

            $('#storageDiv').show();
            $('#allLogDiv').show();

            $('#filter_by_from_storage').multiselect({
                search   : true,
                onOptionClick: function( element, option ) {
                    $('#logistics_table').DataTable().destroy();
                    fetch_history_data();
                },
                maxHeight: 400,
                texts    : {
                    placeholder: 'Поставщики/База',
                    search     : 'Search States'
                },
            });
            $('#filter_by_to_storage').multiselect({
                search   : true,
                onOptionClick: function( element, option ) {
                    $('#logistics_table').DataTable().destroy();
                    fetch_history_data();
                },
                maxHeight: 400,
                texts    : {
                    placeholder: 'Склады',
                    search     : 'Search States'
                },
            });

            $('#loading').hide();
            fetch_history_data();
            $('#logistics').on('show.bs.tab', function(){
                alert('New tab will be visible now!');
            });

        } );

        $('#upload_start').on('change',function () {
            $('#history_table').DataTable().destroy();
            fetch_history_data();
        });
        $('#upload_end').on('change',function () {
            $('#history_table').DataTable().destroy();
            fetch_history_data();
        });

        $('#order_start').on('change',function () {
            $('#history_table').DataTable().destroy();
            fetch_history_data();
        });
        $('#order_end').on('change',function () {
            $('#history_table').DataTable().destroy();
            fetch_history_data();
        });

        $('#input_start').on('change',function () {
            $('#history_table').DataTable().destroy();
            fetch_history_data();
        });
        $('#input_end').on('change',function () {
            $('#history_table').DataTable().destroy();
            fetch_history_data();
        });

        $('#resetDateFilter').click(function () {
            resetHistoryDatefilter();
        });

        function resetHistoryDatefilter() {
            var now = new Date();

            var day = ("0" + now.getDate()).slice(-2);
            var month = ("0" + (now.getMonth() + 1)).slice(-2);

            var today = now.getFullYear()+"-"+(month)+"-"+(day);
            $('#upload_start').val('');
            $('#input_start').val('');
            $('#order_start').val('');
            $('#upload_end').val(today);
            $('#input_end').val(today);
            $('#order_end').val(today);
            $('#history_table').DataTable().destroy();
            fetch_history_data();
        }

        $('#l_resetDateFilter').click(function () {
            resetAllLogDateFilter();
        });

        function  resetAllLogDateFilter() {
            var now = new Date();

            var day = ("0" + now.getDate()).slice(-2);
            var month = ("0" + (now.getMonth() + 1)).slice(-2);

            var today = now.getFullYear()+"-"+(month)+"-"+(day);
            $('#l_upload_start').val('');
            $('#l_order_start').val('');
            $('#l_upload_end').val(today);
            $('#l_order_end').val(today);
            $('#logistics_table').DataTable().destroy();
            fetch_data();
        }

        $('.standardSelect').on('change',function () {
            $('#history_table').DataTable().destroy();
            fetch_history_data();
        });

        function fetch_history_data() {
            var upload_start_date = $('#upload_start').val();
            var upload_end_date = $('#upload_end').val();
            var input_start_date = $('#input_start').val();
            var input_end_date = $('#input_end').val();
            var order_start_date = $('#order_start').val();
            var order_end_date = $('#order_end').val();
            var filter_by_type = $('#filter_by_supplier_type').val();

            var filterArr = {};
            $('.multipleChosen').each(function (index, value) {
                var parentKey = $(this).attr('id');
                filterArr[parentKey] = [];
                if ($(this).val()!=null) {
                    $.each($(this).val(), function (i, val) {
                        filterArr[parentKey].push(val);
                    });
                }
            });
            // var stateFilter = $('#stateFilter').val();
            console.log(filterArr)
            $('#history_table').DataTable().destroy();
            var table = $('#history_table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "{{ route('historyOfOrder')}}",
                    type: "post",
                    data: {filterArr: JSON.stringify(filterArr), upload_start_date:upload_start_date, upload_end_date:upload_end_date, input_start_date:input_start_date, input_end_date:input_end_date,
                        order_start_date:order_start_date, order_end_date:order_end_date, filter_by_type:filter_by_type}
                },
                pageLength:100,
                order: [[ 4, "desc" ]],
                lengthMenu:[[100, 500, 1000, 5000], [100, 500, 1000, 5000]],
                "columns": [
                    { "data":"articula_new" },
                    { "data":"sap_code" },
                    { "data":"item_name", "width":"220px" },
                    { "data":"supplier" },
                    { "data":"supplier_type" },
                    { "data":"shipment_time" },
                    { "data":"delivery_time" },
                    { "data":"input_date" },
                    { "data":"order_invoice" },
                    { "data":"log_invoice" },
                    { "data":"container_number" },
                    { "data":"price_per_unit" },
                    { "data":"currency" },
                    { "data":"order_amount" },
                    { "data":"log_amount" },
                    { "data":"way_amount"},
                    { "data":"input_sum"},
                    { "data":"inputed"},
                    { "data":"sup_remaining" },
                    { "data":"unit" },
                    { "data":"to_storage" }
                ],
                createdRow: function( row, data, dataIndex ) {
                    $( row ).find('td:eq(2)').addClass("proc");
                },
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение от _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "нет записей",
                    "infoFiltered": "(<b>отфильтровано из _TOTAL_ записей</b>)",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17]
                        },
                        title:'бум лист',
                        text: '<i class="fa fa-file-excel-o"></i>'

                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: [ 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17]
                        },
                        title:'бум лист',
                        text: '<i class="fa fa-file-pdf-o"></i>'

                    },
                ],
                scrollY: "70vh",
                scrollCollapse: true,
                scrollX: true
            });
        }
    </script>
@endsection
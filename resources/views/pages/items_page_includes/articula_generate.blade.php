@extends('layouts.master')
@section('additional_css')
    <style>
        .form-control{
            font-size: 54px;
            height: auto;
        }
    </style>
@endsection
@section('content')

    <div class="panel panel-primary">

        <div class="panel-heading" style="padding-top: 10px">

        </div>

        <div class="panel-body">

            <div class="col-md-2"></div>
            <div class="col-md-10">
                <form action="{{ route('generate_articula') }}" method="post">
                    {{ csrf_field() }}
                    <div class="col-md-1" style="padding: 0; margin: 0 0 0 15px">
                        <input type="number" min="1" max="9" class="form-control" name="cat_num[]" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" required>
                    </div>
                    <div class="col-md-1" style="padding: 0; margin: 0">
                        <input type="number" min="0"  max="9" class="form-control" name="cat_num[]" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" required>
                    </div>
                    <div class="col-md-1" style="padding: 0; margin: 0">
                        <input type="number" min="0"  max="9" class="form-control" name="cat_num[]" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" required>
                    </div>
                    <div class="col-md-1" style="padding: 0; margin: 0">
                        <input type="number" min="0"  max="9" class="form-control" name="cat_num[]"  onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" required>
                    </div>
                    <div class="col-md-1" style="padding: 0; margin: 0">
                        <input type="number" min="0"  max="9" class="form-control" name="cat_num[]"  onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" required>
                    </div>
                    <div class="col-md-1" style="padding: 0; margin: 0">
                        <input type="number" min="0" max="9" class="form-control" name="cat_num[]"  onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" required>
                    </div>
                    <div class="col-md-1" style="padding: 0; margin: 0">
                        <input type="number" min="0" max="9" class="form-control" name="cat_num[]"  onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" required>
                    </div>
                    <div class="col-md-2" style="padding: 0; margin: 20px 0 5px 10px;">
                        <p style="font-size: 36px; height: auto; font-weight: bold">-XXXXX</p>
                    </div>
                    <div class="col-md-1"  style="padding: 0; margin: 30px 0 5px 10px;">
                        <button type="submit" class="btn btn-warning">генерировать</button>
                    </div>
                </form>
            </div>

            <p style="margin-top: 150px; text-align: center; font-weight: bold; color: #00aff0; font-size: 72px; letter-spacing: 5px">
                @if(session()->has('articula_new'))
                    @if(session()->get('articula_new')==null)
                        'нет свободных артикул'
                    @else
                        {{ session()->get('articula_new') }}
                    @endif
                @endif
            </p>
        </div>
    </div>
    <script>
        function maxLengthCheck(object) {
            if (object.value.length > object.max.length)
                object.value = object.value.slice(0, object.max.length)
        }

        function isNumeric (evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode (key);
            var regex = /[0-9]|\./;
            if ( !regex.test(key) ) {
                theEvent.returnValue = false;
                if(theEvent.preventDefault) theEvent.preventDefault();
            }
        }
    </script>
@endsection
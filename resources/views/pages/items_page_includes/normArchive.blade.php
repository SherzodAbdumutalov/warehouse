@section('additional_css')

    <style>

        table tbody td {
            min-width: 130px;
        }
        .selectMultiple {
            width: 240px;
            position: relative;
        }


    </style>
@endsection
<div class="alert alert-danger" style="display:none">
    <ul>

    </ul>
</div>
<div class="panel panel-primary">
    <div class="panel-heading"> История изменении в норме расходах </div>
    <div class="panel-body" style="overflow-x: auto">

        <table id="archive_table" class="table table-bordered table-hover" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>готовый продукт</th>
                <th>деталь</th>
                <th>деталь измн.</th>
                <th>кол-во</th>
                <th>кол-во измн.</th>
                <th>название</th>
                <th>название измн.</th>
                <th>дата измн.</th>
                <th>статус</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
            <tfoot>
            <tr>
                <th>готовый продукт</th>
                <th>деталь</th>
                <th>деталь измн.</th>
                <th>кол-во</th>
                <th>кол-во измн.</th>
                <th>название</th>
                <th>название измн.</th>
                <th>дата измн.</th>
                <th>статус</th>
            </tr>
            </tfoot>
        </table>
    </div>
</div>
@section('additionalLibrary')
    {{--ready window for work--}}
    <script>
        $(document).ready(function() {

            fetch_date();
        });

        $('#filter-action-process').on('change', function (){

            let filterss = $('#filter-action-process').val();

            console.log(filterss);
        });

        function fetch_date() {
            $("#archive_table").DataTable().destroy();
            var table = $('#archive_table').DataTable( {
                "processing": true,
                serverSide: true,
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    "url": "{{ route('getNormArchive') }}",
                    "type": "post",
                    "dataType": "json"
                },
                columns: [
                    { "data": "model_name", "width":"23%" },
                    { "data": "old_name", "width":"23%" },
                    { "data": "new_name", "width":"23%" },
                    { "data": "old_amount", "width":"8%" },
                    { "data": "new_amount", "width":"8%" },
                    { "data": "old_item_name", "width":"23%" },
                    { "data": "new_item_name", "width":"23%" },
                    { "data": "changed_time", "width":"7%" },
                    { "data": "status", "width":"8%"},
                ],
                order: [[7, 'desc']],
                pageLength: 100,
                lengthMenu:[[100, 500, 1000, -1], [100, 500, 1000, "All"]],
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5,6,7]
                        },
                        title:'бум лист',
                        text: '<i class="fa fa-file-excel-o"></i>'

                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5]
                        },
                        title:'бум лист',
                        text: '<i class="fa fa-file-pdf-o"></i>'

                    },
                ],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение с _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "данные отсутствует",
                    "infoFiltered": "",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                scrollY: "70vh",
            } );
        }
        // $('#Texizmennorm').on('change', function (){
        //     fetch_date();
        // })

    </script>
@endsection

@section('additional_css')
    {{--<link rel="stylesheet" href="{{ asset('assets') }}/css/style.css">--}}
    <style>
        table.dataTable thead th, table.dataTable thead td{
            padding: 1px 18px!important;
        }
        table.dataTable tbody td{
            padding: 1px 18px!important;
        }
        table.dataTable tr.group,
        table.dataTable tr.group:hover {
            background-color: #ddd !important;
        }
    </style>
@endsection
<div class="panel-control" id="forItemCreate">
    <table  id="example1" class="display nowrap table table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th></th>
            <th></th>
            <th>вфц</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>импорт</td>
            <td><input type="checkbox" name="checkbox_data[]" value="0_1" ></td>
            <td>Статус</td>
        </tr>
        <tr>
            <td>местный</td>
            <td><input type="checkbox" name="checkbox_data[]" value="0_2" ></td>
            <td>Статус</td>
        </tr>
        <tr>
            <td>собственное производство</td>
            <td><input type="checkbox" name="checkbox_data[]" value="0_3" ></td>
            <td>Статус</td>
        </tr>
        <tr>
            <td>Радиаторный цех</td>
            <td><input type="checkbox" name="checkbox_data[]" value="0_4" ></td>
            <td>Статус</td>
        </tr>
        <tr>
            <td>сырье</td>
            <td><input type="checkbox" name="checkbox_data[]" value="1_0" ></td>
            <td>номенклатура</td>
        </tr>
        <tr>
            <td>загатовка</td>
            <td><input type="checkbox" name="checkbox_data[]" value="1_1" ></td>
            <td>номенклатура</td>
        </tr>
        <tr>
            <td>деталь</td>
            <td><input type="checkbox" name="checkbox_data[]" value="1_2" ></td>
            <td>номенклатура</td>
        </tr>
        <tr>
            <td>сборачная единица 1 уровня</td>
            <td><input type="checkbox" name="checkbox_data[]" value="1_3" ></td>
            <td>номенклатура</td>
        </tr>
        <tr>
            <td>комплектная единица</td>
            <td><input type="checkbox" name="checkbox_data[]" value="1_4" ></td>
            <td>номенклатура</td>
        </tr>
        <tr>
            <td>без идентификации</td>
            <td><input type="checkbox" name="checkbox_data[]" value="2_0" ></td>
            <td>идентификация</td>
        </tr>
        <tr>
            <td>наружный блок</td>
            <td><input type="checkbox" name="checkbox_data[]" value="2_1" ></td>
            <td>идентификация</td>
        </tr>
        <tr>
            <td>внутренный блок</td>
            <td><input type="checkbox" name="checkbox_data[]" value="2_2" ></td>
            <td>идентификация</td>
        </tr>
        <tr>
            <td>коммерческий наружный блок</td>
            <td><input type="checkbox" name="checkbox_data[]" value="2_3" ></td>
            <td>идентификация</td>
        </tr>
        <tr>
            <td>коммерческий внутренный блок</td>
            <td><input type="checkbox" name="checkbox_data[]" value="2_4" ></td>
            <td>идентификация</td>
        </tr>
        <tr>
            <td>Сырье, заготовка, не установленные детали и сборочные еденицы, испытания</td>
            <td><input type="checkbox" name="checkbox_data[]" value="3_00" ></td>
            <td>Операция</td>
        </tr>
        <tr>
            <td>Резка, гибка медных трубок</td>
            <td><input type="checkbox" name="checkbox_data[]" value="3_01" ></td>
            <td>Операция</td>
        </tr>
        <tr>
            <td>Резка алюминевой фольги</td>
            <td><input type="checkbox" name="checkbox_data[]" value="3_02" ></td>
            <td>Операция</td>
        </tr>
        <tr>
            <td>Сжатие фольги и развальцовка медной трубки</td>
            <td><input type="checkbox" name="checkbox_data[]" value="3_03" ></td>
            <td>Операция</td>
        </tr>
        <tr>
            <td>Резка, гибка и развольцовка медной трубки</td>
            <td><input type="checkbox" name="checkbox_data[]" value="3_04" ></td>
            <td>Операция</td>
        </tr>
        <tr>
            <td>Резка медной трубки</td>
            <td><input type="checkbox" name="checkbox_data[]" value="3_05" ></td>
            <td>Операция</td>
        </tr>
        <tr>
            <td>Гибка медной трубки</td>
            <td><input type="checkbox" name="checkbox_data[]" value="3_06" ></td>
            <td>Операция</td>
        </tr>
        <tr>
            <td>Формирования Т образной медной трубки</td>
            <td><input type="checkbox" name="checkbox_data[]" value="3_07" ></td>
            <td>Операция</td>
        </tr>
        <tr>
            <td>Термообработка медной трубки</td>
            <td><input type="checkbox" name="checkbox_data[]" value="3_08" ></td>
            <td>Операция</td>
        </tr>
        <tr>
            <td>Формирования Y образной медной трубки</td>
            <td><input type="checkbox" name="checkbox_data[]" value="3_09" ></td>
            <td>Операция</td>
        </tr>
        <tr>
            <td>Чистка медной трубки</td>
            <td><input type="checkbox" name="checkbox_data[]" value="3_10" ></td>
            <td>Операция</td>
        </tr>
        <tr>
            <td>Развальцовка медной трубки</td>
            <td><input type="checkbox" name="checkbox_data[]" value="3_11" ></td>
            <td>Операция</td>
        </tr>
        <tr>
            <td>Испытательние на герметичность</td>
            <td><input type="checkbox" name="checkbox_data[]" value="3_12" ></td>
            <td>Операция</td>
        </tr>
        <tr>
            <td>Установка кол-воцевого припоя на U образную медную трубку</td>
            <td><input type="checkbox" name="checkbox_data[]" value="3_13" ></td>
            <td>Операция</td>
        </tr>
        <tr>
            <td>Пайка автоматическая</td>
            <td><input type="checkbox" name="checkbox_data[]" value="3_14" ></td>
            <td>Операция</td>
        </tr>
        <tr>
            <td>Пайка ручная</td>
            <td><input type="checkbox" name="checkbox_data[]" value="3_15" ></td>
            <td>Операция</td>
        </tr>
        <tr>
            <td>Скрутка медной трубки</td>
            <td><input type="checkbox" name="checkbox_data[]" value="3_16" ></td>
            <td>Операция</td>
        </tr>
        <tr>
            <td>Скрутка медной трубки</td>
            <td><input type="checkbox" name="checkbox_data[]" value="3_17" ></td>
            <td>Операция</td>
        </tr>
        <tr>
            <td>Сборочная единица 1 уровня (статус "собственное производство - 3)</td>
            <td><input type="checkbox" name="checkbox_data[]" value="3_18" ></td>
            <td>Операция</td>
        </tr>
        <tr>
            <td>без комплектующих</td>
            <td><input type="checkbox" name="checkbox_data[]" value="4_00" ></td>
            <td>материал</td>
        </tr>
        <tr>
            <td>Алюминевые изделия</td>
            <td><input type="checkbox" name="checkbox_data[]" value="4_01" ></td>
            <td>материал</td>
        </tr>
        <tr>
            <td>Медные изделия</td>
            <td><input type="checkbox" name="checkbox_data[]" value="4_02" ></td>
            <td>материал</td>
        </tr>
        <tr>
            <td>Газ</td>
            <td><input type="checkbox" name="checkbox_data[]" value="4_03" ></td>
            <td>материал</td>
        </tr>
        <tr>
            <td>Сборные алюминий и медь</td>
            <td><input type="checkbox" name="checkbox_data[]" value="4_04" ></td>
            <td>материал</td>
        </tr>
        <tr>
            <td>Металлические части</td>
            <td><input type="checkbox" name="checkbox_data[]" value="4_05" ></td>
            <td>материал</td>
        </tr>
        <tr>
            <td>Резинотехнические</td>
            <td><input type="checkbox" name="checkbox_data[]" value="4_06" ></td>
            <td>материал</td>
        </tr>
        <tr>
            <td>Полимерные</td>
            <td><input type="checkbox" name="checkbox_data[]" value="4_07" ></td>
            <td>материал</td>
        </tr>
        <tr>
            <td>Электрические части</td>
            <td><input type="checkbox" name="checkbox_data[]" value="4_08" ></td>
            <td>материал</td>
        </tr>
        <tr>
            <td>Термоизоляция, шумоизоляция и виброизоляция</td>
            <td><input type="checkbox" name="checkbox_data[]" value="4_09" ></td>
            <td>материал</td>
        </tr>
        <tr>
            <td>Метизы</td>
            <td><input type="checkbox" name="checkbox_data[]" value="4_10" ></td>
            <td>материал</td>
        </tr>
        <tr>
            <td>Упаковочные материалы и наклейки</td>
            <td><input type="checkbox" name="checkbox_data[]" value="4_11" ></td>
            <td>материал</td>
        </tr>
        <tr>
            <td>Прочее</td>
            <td><input type="checkbox" name="checkbox_data[]" value="4_12" ></td>
            <td>материал</td>
        </tr>
        </tbody>
    </table>
</div>

<script>
    $(document).ready(function() {
        var groupColumn = 2;

        $('#example1').DataTable( {
            order: [[ groupColumn, 'desc' ]],
            language: {
                // sLoadingRecords : '<span style="width:100%;"><img src="http://www.snacklocal.com/images/ajaxload.gif"></span>',
                "lengthMenu": "_MENU_",
                "zeroRecords": "ничего не найдено!",
                "info": "_PAGE_ - ая страница из _PAGES_",
                "infoEmpty": "",
                "infoFiltered": "((<b>отфильтровано из _MAX_ записей</b>))",
                "search": "<i class='fa fa-search' style='float: left'></i>"
            },
            columnDefs: [
                { orderable: false, targets: [0, 1, 2] }, { visible: false, targets: [groupColumn] }
            ],
            paging: false,
            bInfo: false,
            scrollY:        "400px",
            scrollX:        true,
            scrollCollapse: true,
            fixedColumns:   {
                heightMatch: 'none'
            },

            drawCallback: function ( settings ) {
                var api = this.api();
                var rows = api.rows( {page:'current'} ).nodes();
                var last=null;

                api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                    if ( last !== group ) {
                        $(rows).eq( i ).before(
                            '<tr class="group"><td colspan="9" style="background-color: #0ea432">'+group+'</td></tr>'
                        );

                        last = group;
                    }
                } );
            }
        });

        $('#example tbody').on( 'click', 'tr.group', function () {
            var currentOrder = table.order()[0];
            if ( currentOrder[0] === groupColumn && currentOrder[1] === 'asc' ) {
                table.order( [ groupColumn, 'desc' ] ).draw();
            }
            else {
                table.order( [ groupColumn, 'asc' ] ).draw();
            }
        } );
    } );
</script>
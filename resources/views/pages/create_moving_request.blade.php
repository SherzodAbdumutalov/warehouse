@section('additional_css')
    <style>
        .modal-body {
            max-height: calc(100vh - 200px);
            overflow-y: auto;
            padding-top: 0;
            margin-top: 0;
        }

        .modal-dialog {
            width: calc(100% - 100px);
        }
        input[type=checkbox]
        {
            /* Double-sized Checkboxes */
            -ms-transform: scale(2); /* IE */
            -moz-transform: scale(2); /* FF */
            -webkit-transform: scale(2); /* Safari and Chrome */
            -o-transform: scale(2); /* Opera */
            padding: 10px;
        }

        .table > tbody > tr > td{
            font-size: 12px;
            line-height: 20px;
        }

        .table > thead > tr > th {
            font-weight: bold;
        }
    </style>
@endsection
<div class="panel panel-primary">

    <div class="panel-heading" style="padding: 1px 5px">
        запрос на перемещение
    </div>
    <div class="panel-body">
        <div class="alert alert-info" style="display:none">
            <ul>

            </ul>
        </div>
        <div class="col-md-2" style="padding-left: 0">
            <label for="from_storage_id" style="font-weight: bold">Отправитель</label>
            <select name="from_storage_id" id="from_storage_id" class="standardSelect" >
                @foreach($lines as $line)
                    <option value="{{ $line->id }}">{{ $line->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-2" style="padding-left: 0">
            <label for="to_storage_id" style="font-weight: bold">Получатель</label>
            <select name="to_storage_id" id="to_storage_id" class="standardSelect" >
                @foreach($storages as $storage)
                    <option value="{{ $storage->id }}">{{ $storage->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-6">
            <label for="item"><b>единицы</b></label>
            <select name="item" data-placeholder="item select" id="item" class="standardSelect chosen-choices ">
            </select>
        </div>

        <div class="col-md-1">
            <label for="amount"><b>кол-во</b></label><br>
            <input type="number" name="amount" min="0" id="amount" class="form-control" style="height: 26px; display: block">
        </div>
        <div class="col-md-1">
            <button class="btn btn-warning" type="button" style="margin-top: 30px; padding: 3px 15px" onclick="add_item()"><i class="fa fa-plus"></i> добавить</button>
        </div>

        <table id="moving_request_list_table" class="table table-hover table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th style="padding-left: 0">артикул</th>
                <th style="padding-left: 0">sap</th>
                <th style="padding-left: 0">наименование</th>
                <th style="padding-left: 0">кол-во</th>
                <th style="padding-left: 0">остаток</th>
                <th style="padding-left: 0">ед. изм.</th>
                <th style="padding-left: 0"></th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
            <tr>
                <th style="padding-left: 0">артикул</th>
                <th style="padding-left: 0">sap</th>
                <th style="padding-left: 0">наименование</th>
                <th style="padding-left: 0">кол-во</th>
                <th style="padding-left: 0">остаток</th>
                <th style="padding-left: 0">ед. изм.</th>
                <th style="padding-left: 0"></th>
            </tr>
            </tfoot>
        </table>

        <div class="col-md-4">
            <label for="description" style="font-size: 16px; font-weight: bold">описаниe</label>
            <input name="description" id="description" maxlength="50" style="resize: none; height: 26px" placeholder="вводите текст... (макс 50 символов)" class="form-control">
        </div>
        <div class="col-md-3">
            <div style="float: left; padding-top: 30px">
                <button class="btn btn-warning " type="submit" style="font-size: 12px; float: left" id="save_moving_request" onclick="save_moving_request()"><i class="glyphicon glyphicon-floppy-save" ></i> сохранить</button>
                &nbsp;<button class="btn btn-danger " type="submit" style="font-size: 12px; float: right" onclick="clear_sessionStorage()"><i class="glyphicon glyphicon-remove-circle"></i> очистить</button>
            </div>
        </div>
    </div>
</div>

{{--edit amount--}}
<div id="edit_amount_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm" style="width: 20%">
        <div class="modal-content" >
            <div class="modal-header">
                изменить кол-во
                <button type="button" class="close" data-dismiss="modal" style="font-size: 38px" id="close_modal">&times;</button>
            </div>
            <div class="modal-body">
                <label for="edit_amount">кол-во</label>
                <input type="number" name="amount" id="edit_amount" step="0.0001" class="form-control">
                <input type="hidden" name="key" id="edit_key" value="">
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-warning" onclick="save_amount()"><i class="fa fa-save"></i> Сохранить</button>
            </div>
        </div>
    </div>
</div>
@section('additionalLibrary')
    {{--dataTables library--}}
    <script>
        $(document).ready(function() {
            if(sessionStorage.getItem('moving_request_items')!=null){
                $('#from_storage_id').prop('disabled', true).trigger('chosen:updated');
                $('#to_storage_id').prop('disabled', true).trigger('chosen:updated');
            }else{
                $('#from_storage_id').prop('disabled', false).trigger("chosen:updated");
                $('#to_storage_id').prop('disabled', false).trigger("chosen:updated");
            }

            if(sessionStorage.getItem('last_selected_from_storage_id_moving_request')!=null){
                $('#from_storage_id').val(sessionStorage.getItem('last_selected_from_storage_id_moving_request')).trigger('chosen:updated')
            }

            if(sessionStorage.getItem('last_selected_to_storage_id_moving_request')!=null){
                $('#to_storage_id').val(sessionStorage.getItem('last_selected_to_storage_id_moving_request')).trigger('chosen:updated')
            }
            
            $('#moving_request_list_table').DataTable( {
                columnDefs: [
                    { orderable: false, targets: [0, 1, 2, 3, 4, 5, 6] }
                ],
                order: false,
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение от _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "",
                    "infoFiltered": "(отфильтровано из _MAX_ записей)",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                paging: false,
                bPaginate: false,
                "columns": [
                    { "width": "8%" },
                    { "width": "8%" },
                    { "width": "50%" },
                    { "width": "5%" },
                    { "width": "5%" },
                    { "width": "8%" },
                    { "width": "5%" },
                ],
                scrollY: "50vh",
                scrollCollapse: true
            });

            $('input[name=amount]').on('keydown', function (e) {
                if(e.keyCode==13){
                    add_item();
                }
            });
            getStorageMovingRequestItems();
            draw_table();
        } );
    </script>
    <script>
        $('#from_storage_id').on('change', function () {
            getStorageMovingRequestItems();
        });
        $('#to_storage_id').on('change', function () {
            getStorageMovingRequestItems();
        });
        function getStorageMovingRequestItems() {
            var from_storage_id = $('#from_storage_id').val();
            var to_storage_id = $('#to_storage_id').val();
            $.ajax({
                url: "{{ route('getStorageMovingRequestItems') }}",
                type: "get",
                data: {from_storage_id:from_storage_id, to_storage_id:to_storage_id},
                success: function(data){
                    $('#item').empty().trigger("chosen:updated");
                    $.each(JSON.parse(data), function (index, value) {
                        $('#item').append('<option value="'+value.id+'">'+value.articula_new+' '+value.item_name+' '+value.sap_code+'</option>');
                    })
                    $('#item').trigger("chosen:updated");
                },
                error: function (request, status, error) {
                    $('#loading').hide();
                    var json = $.parseJSON(request.responseText);
                    console.log(json);
                    $('.alert-info').show();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').empty().append('<li>'+value+'</li>');
                    })
                }
            })
        }

        function add_item() {
            $('#loading').show();
            var from_storage_id = $('#from_storage_id').val();
            var to_storage_id = $('#to_storage_id').val();
            sessionStorage.setItem('last_selected_from_storage_id_moving_request', from_storage_id);
            sessionStorage.setItem('last_selected_to_storage_id_moving_request', to_storage_id);
            var item_id = $('#item').val();
            console.log(item_id);
            var str_amount = $('#amount').val();
            var amount = parseFloat(str_amount.replace(',', '.'));
            if(amount>0){
                $('#from_storage_id').prop('disabled', true).trigger("chosen:updated");
                $('#to_storage_id').prop('disabled', true).trigger("chosen:updated");
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "{{ route('collectMovingRequestItems') }}",
                    type: "post",
                    data: {item_id:item_id, amount:amount, from_storage_id:from_storage_id, to_storage_id:to_storage_id},
                    success: function(data){
                        $('#amount').val('');
                        $('#loading').hide();
                        console.log(data)
                        var items = $.parseJSON(data);
                        items.forEach(function (value, index) {
                            if(is_exist(value.item_id, value.amount)){
                                draw_table();
                            }else{
                                collect_to_array(value);
                            }
                        })
                    },
                    error: function (request, status, error) {
                        $('#loading').hide();
                        var json = $.parseJSON(request.responseText);
                        console.log(json);
                        $('.alert-info').show();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').empty().append('<li>'+value+'</li>');
                        })
                    }
                })
            }else{
                $('#loading').hide();
                alert('вводите кол-во!')
            }
        }

        function draw_table() {
            $('#save_moving_request').prop('disabled', false).show();
            $('#moving_request_list_table').DataTable().clear().draw(false);
            var items = $.parseJSON(sessionStorage.getItem('moving_request_items'));
            if (items!=null){
                items.forEach(function (value, key) {
                    if(value.amount>value.remaining){
                        $('#save_moving_request').prop('disabled', true).hide();
                    }
                    var trow = $('#moving_request_list_table').DataTable().row.add([
                        value.articula_new,
                        value.sap_code,
                        value.item_name,
                        parseFloat(value.amount),
                        value.remaining,
                        value.unit,
                        '<button type="button" class="btn btn-warning " data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#edit_amount_modal" onclick="set_data_for_edit('+key+','+value.amount+')" style="font-size: 11px"><i class="fa fa-edit"></i></button> <button type="button" class="btn btn-danger " onclick="delete_item('+key+')" style="font-size: 11px" ><i class="fa fa-trash"></i></button>'
                    ]).node();
                    trow.id = 'row_'+key;
                    $('#moving_request_list_table').DataTable().draw(false);
                    if (Number(value.remaining)<Number(value.amount)){
                        $(trow).find('td:eq(4)').css('background-color', 'red');
                    }else {
                        $(trow).find('td:eq(4)').css('background-color', '');
                    }
                    $(trow).find('td:eq(5)').css('background-color', '');
                })
            }else{
                $('#save_moving_request').prop('disabled', true).hide();
            }
            $('#loading').hide();
        }

        function collect_to_array(item) {
            var old_items = [];
            if(sessionStorage.getItem('moving_request_items')!=null){
                old_items = $.parseJSON(sessionStorage.getItem('moving_request_items'))
            }
            var data = {
                'item_id':item.item_id,
                'articula_new':item.articula_new,
                'sap_code':item.sap_code,
                'item_name':item.item_name,
                'amount':parseFloat(item.amount),
                'remaining':item.remaining,
                'unit':item.unit,
                'unit_id':item.unit_id,
                'storage_id':item.storage_id,
            };
            old_items.push(data);
            sessionStorage.setItem('moving_request_items', JSON.stringify(old_items));
            draw_table();
        }

        function clear_sessionStorage() {
            sessionStorage.removeItem('moving_request_items');
            sessionStorage.removeItem('last_selected_from_storage_id_moving_request');
            sessionStorage.removeItem('last_selected_to_storage_id_moving_request');
            $('#from_storage_id').prop('disabled', false).trigger("chosen:updated");
            $('#to_storage_id').prop('disabled', false).trigger("chosen:updated");
            draw_table();
            getStorageMovingRequestItems();
        }

        function is_exist(item_id, amount) {
            var items = $.parseJSON(sessionStorage.getItem('moving_request_items'));
            if(items!=null){
                for (var i=0; i<items.length; i++){
                    if(item_id==items[i].item_id){
                        items[i].amount+=parseFloat(amount);
                        sessionStorage.setItem('moving_request_items', JSON.stringify(items));
                        return true;
                    }
                }
            }
            return false;
        }

        function delete_item(key) {
            var items = $.parseJSON(sessionStorage.getItem('moving_request_items'));
            items.splice(key, 1);
            sessionStorage.setItem('moving_request_items', JSON.stringify(items));
            draw_table()
        }

        function set_data_for_edit(key, amount) {
            $('#edit_amount').val(amount);
            $('#edit_key').val(key);
        }

        function save_amount() {
            var amount = $('#edit_amount').val();
            var key = $('#edit_key').val();
            var items = $.parseJSON(sessionStorage.getItem('moving_request_items'));
            if(amount>0){
                items[key]['amount']=amount;
                sessionStorage.setItem('moving_request_items', JSON.stringify(items));
                draw_table();
                $('#close_modal').click();
            }else{
                alert('недопустимое значение!!');
            }
        }

        function save_moving_request() {
            $('#save_moving_request').hide();
            if (confirm('сохранить?')) {
                var items = sessionStorage.getItem('moving_request_items');
                var desc = $('#description').val();
                var from_storage_id = $('#from_storage_id').val();
                var to_storage_id = $('#to_storage_id').val();
                if (items.length>0){
                    $.ajax({
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        type:'POST',
                        url:"{{route('saveMovingRequestItems')}}",
                        data:{items:items, description:desc, from_storage_id:from_storage_id, to_storage_id:to_storage_id},
                        success: function (data) {
                            $('#save_moving_request').show();
                            $('#description').val('');
                            draw_table();
                            clear_sessionStorage();
                            $('.alert-info').show();
                            $('.alert-info ul').empty().append('<li>сохранено</li>');
                        },
                        error: function (request, status, error) {
                            $('#loading').hide();
                            var json = $.parseJSON(request.responseText);
                            console.log(json);
                            $('.alert-info').show();
                            $.each(json.errors, function (index, value) {
                                $('.alert-info ul').empty().append('<li>'+value+'</li>');
                            })
                        }
                    })
                }
            }else{
                $('#save_moving_request').show();
            }
        }

        function allow_save() {
            var items = $.parseJSON(sessionStorage.getItem('moving_request_items'));
            var i = 0;
            for (i = 0; i < items.length; i++) {
                if (items[i].amount > items[i].remaining) {
                    return false
                }
            }
            return true
        }

    </script>
@endsection
<div class="container">

    <div class="panel panel-primary">

        <div class="panel-heading">
            @php
                if($parentData->id!=0)
                    $item = \DB::select("select *from dream_items where id=$parentData->id");
                    if(!empty($item))
                        echo '<h4><b>'.$item[0]->item_name.'</b></h4>';
            @endphp
        </div>

        <div class="panel-body">

            {!! Form::open(array('route' => 'upload_child_items','method'=>'POST','files'=>'true')) !!}

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12">

                    <div class="form-group">

                        {!! Form::label('','',['class'=>'col-md-3']) !!}

                        <div class="col-md-7">

                            {!! Form::file('sample_file', array('class' => 'form-control')) !!}

                            {!! $errors->first('sample_file', '<p class="alert alert-danger">:message</p>') !!}

                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2 text-left">

                            {!! Form::submit('загрузить',['class'=>'btn btn-primary' , 'disabled'=>'true', 'onclick'=>'disabledCheckbox()']) !!}

                        </div>
                    </div>

                </div>
                <br/>

            </div>

            {!! Form::close() !!}

            <div class="panel-body col-md-12">
                <div class="col-md-10">
                    <div class="col-md-6">
                        <div class="col-md-6" style="height: 25px; width: 100px; background-color: yellow"></div>
                        <div class="col-md-6"><p style="display: block; float: left">пустая ячейка</p></div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-4" style="height: 25px; width: 100px; background-color: #00aa88"></div>
                        <div class="col-md-8"><p style="display: block; float: left">не существующий элемент в базе</p></div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-4" style="height: 25px; width: 100px; background-color: #0E566C"></div>
                        <div class="col-md-8"><p style="display: block; float: left">невозможно добавить</p></div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-6" style="height: 25px; width: 100px; background-color: #0ea432"></div>
                        <div class="col-md-6"><p style="display: block; float: left">не совпадение</p></div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-6" style="height: 25px; width: 100px; background-color: green"></div>
                        <div class="col-md-6"><p style="display: block; float: left">дубликат в файле</p></div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-4" style="height: 25px; width: 100px; background-color: blue"></div>
                        <div class="col-md-8"><p style="display: block; float: left">ошибка в формате</p></div>
                    </div>
                </div>
                <div class="col-md-2">
                    <br>
                    <span style="font-weight: bold; font-size: 18px">шаблон *.xls:</span><a href="{{ asset('assets/template/child.xlsx') }}"> <i style="font-size: 18px; font-weight: bold" class="glyphicon glyphicon-download"></i> </a>
                </div>
            </div>
            <div class="col-md-12">
                <button type="submit" style="display: block; float: left; margin-right: 5px" class="btn btn-info" onclick="validateExcelForm()" >проверить</button>
                <form action="{{ route('import_child_items') }}" method="post" enctype="multipart/form-data">
                    <input type="hidden" value="{{ $parentData->id }}" name="parentId">
                    {{ csrf_field() }}
                    <button type="submit" style="display: block;" class="btn btn-success" id="import" disabled>импортировать</button>
                </form>
            </div>
        </div>
        <?php

        function isChecked($name){
            if(session()->has($name)){
                if(session()->get($name)=="on"){
                    return true;
                }
            }
            return false;
        }
        ?>

        <div class="panel-body">
            <table class="table table-bordered" id="excelTable">
                <thead>
                    <tr>
                        <th>нов. артикул
                        </th>
                        <th>кол-во
                        </th>
                        <th>ед. измерения
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @if(\Session::has('import_child_item'))
                        @php
                            $data = \Session::get('import_child_item');
                            $units = \Session::get('import_child_item_units');
                            $allItem = \Session::get('allItem');
                            $correctUnits = [];
                        @endphp

                        @foreach($data as $key=>$item)
                            @php
                                $correctUnit = \App\DreamItem::where('articula_new', $item['articula_new'])->get();
                                if (!is_null($correctUnit->first())){
                                    foreach ($correctUnit as $correct){
                                        array_push($correctUnits, $correct->dream_unit->toArray());
                                    }
                                }
                                else{
                                    array_push($correctUnits, null);
                                }
                            @endphp
                            <tr data-toggle="modal"  data-id="{{ $key }}" data-target="#orderModal">
                                <td id="{{ $key }}articula_new">{{ $item['articula_new'] }}</td>
                                <td id="{{ $key }}quantity">{{ $item['quantity'] }}</td>
                                <td id="{{ $key }}units">{{ $item['units'] }}</td>
                            </tr>
                        @endforeach

                    @else
                        @php
                            $data = '';
                            $correctUnits = '';
                            $allItem = '';
                        @endphp
                    @endif

                </tbody>

            </table>

        </div>

    </div>

</div>

<script>

    $(document).ready(function(){
        $('input:file').change(
            function(){
                if ($(this).val()) {
                    $('input:submit').attr('disabled',false);
                }
            }
        );
    });

    function validateExcelForm() {

        var excelData = {!! json_encode($data) !!};
        var allData = {!! json_encode($allItem) !!};
        var units = {!! json_encode($correctUnits) !!};
        var parentId = {!! $parentData->articula_new !!};
        var checkboxes = ['articula_new', 'quantity', 'units'];
        var arr = [];
        var arr1 = [];

        $("table td").css("background-color", "transparent")

        var isValid = true;

        for (var a=0; a<excelData.length; a++){
            arr.push(excelData[a]['articula_new'])
        }

        for (var a=0; a<allData.length; a++){
            arr1.push(allData[a]['articula_new'])
        }

        for(var c=0; c<checkboxes.length; c++){
            for(var ex=0; ex<excelData.length; ex++){
                var tableCell = document.getElementById(ex+checkboxes[c]);


                if(!checkForEmpty(excelData[ex][checkboxes[c]])){

                    isValid = false;
                    tableCell.style.backgroundColor = "yellow"

                }


            }
        }

        for(var ex=0; ex<excelData.length; ex++){

            var correctUnits = []
            var tableCell = document.getElementById(ex+"articula_new");
            var tableCellQ = document.getElementById(ex+"quantity");
            var tableCellUnit = document.getElementById(ex+"units");

            if (excelData[ex]['articula_new']==parentId){
                var tableCell = document.getElementById(ex+"articula_new");
                tableCell.style.backgroundColor = "#0E566C"
                isValid = false;
            }

            else{
                if (units[ex]!=null){
                    for (var cu=0; cu<units[ex].length; cu++){
                        correctUnits.push(units[ex][cu]['unit'])
                    }
                } else {
                    correctUnits.push(null)
                }

                if(!unitExists(excelData[ex]['units'], correctUnits) || correctUnits==null){
                    isValid = false;
                    tableCellUnit.style.backgroundColor = "blue"
                    if (checkForDuplicat(arr, excelData[ex]['articula_new'])==3){
                        isValid = false;
                        tableCell.style.backgroundColor = "green"
                    }

                    else if (checkForExist(arr1, excelData[ex]['articula_new'])==2){
                        isValid = false;
                        tableCell.style.backgroundColor = "#00aa88"
                    }
                }

                else{

                    if (checkForDuplicat(arr, excelData[ex]['articula_new'])==3){
                        isValid = false;
                        tableCell.style.backgroundColor = "green"
                    }

                    else if (checkForExist(arr1, excelData[ex]['articula_new'])==2){
                        isValid = false;
                        tableCell.style.backgroundColor = "#00aa88"
                    }
                    else
                    {
                        if (excelData[ex]['units']!=null){
                            var quantity = excelData[ex]['quantity']+''
                            var unit = excelData[ex]['units']+''
                            if (quantity.split('/').length != unit.split("/").length){
                                isValid = false;

                                tableCellQ.style.backgroundColor = "#0ea432"
                                tableCellUnit.style.backgroundColor = "#0ea432"
                            }
                        }
                    }

                    var quantArr = quantity.split('/')
                    for (var ii=0; ii<quantArr.length; ii++){
                        if (!$.isNumeric(quantArr[ii])){
                            isValid = false;
                            tableCellQ.style.backgroundColor = "#0E566C"
                        }
                    }
                }
            }

        }

        if(isValid){
            var btn = document.getElementById("import")
            btn.disabled = "";
        }

    }


    function checkForEmpty(excelData){

        if(excelData==null){

            return false;

        }else{

            return true;

        }

    }

    function checkForDuplicat(excelData, data){
        var count = 0;
        for (var j=0; j<excelData.length; j++){
            if(data==excelData[j] && data!=null){
                count++;
            }
        }
        if (count>1){
            return 3;
        }

    }

    function checkForExist(arr, data){
        var count = 0;
        for (var j=0; j<arr.length; j++){
            if(data==arr[j]){
                count++;
            }
        }

        if (count==0){
            return 2;
        }

    }

    function unitExists(excelUnit, units){

        if (excelUnit!=null) {

            var parsedUnits = excelUnit.split("/")
            for (var i = 0; i < parsedUnits.length; i++) {
                if (!units.includes(parsedUnits[i].trim())) {

                    return false;

                }
            }
            return true;
        }else {
            return true;
        }
    }


</script>
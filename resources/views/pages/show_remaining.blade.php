@section('additional_css')
    <style>
        p{
            margin: 0;
        }
        .chosen-container{
            font-size: 11px!important;
        }

        .chosen-search-input{
            height: 20px!important;
        }
        #date{
            margin-top: 19px;
            padding: 2px;
        }

        label{
            margin-bottom: 0;
        }
    </style>
@endsection
<div class="panel panel-primary">
    <input type="hidden" id="selectedFilter">
    <div class="panel-heading" style="padding: 1px 5px">
        <strong> Остатки на складах/линиях </strong>
    </div>
    <div class="panel-body">
        <div class="col-md-2 mr-3">
            <i class="fa fa-calendar"></i>
            <input type="date" id="date" name="date" value="{{ (session()->has('remainingTable.date'))?session()->get('remainingTable.date'):\Illuminate\Support\Carbon::now()->format('Y-m-d') }}" style="font-size: 11px">
        </div>
        <div class="col-md-2">
            <label for="storage">склады (фильтр)</label>
            <select name="storage[]" data-placeholder="выбрать" id="storage" class="standardSelect" multiple disabled>
                @foreach($storages as $storage)
                    <option value="{{ $storage->id }}">{{ $storage->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-3" style="padding-left: 0">
            <label for="models_id">готовые продукты (фильтр)</label>
            <select name="models_id" data-placeholder="выбрать" id="models_id" class="standardSelect" multiple disabled>
                @foreach($models as $model)
                    <option value="{{ $model->id }}">{{ $model->articula_new }} {{ $model->item_name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-3" style="padding-left: 0">
            <label for="suppliers">поставщики (фильтр)</label>
            <select name="suppliers" data-placeholder="выбрать" id="suppliers" class="standardSelect" multiple disabled>
                @foreach($suppliers as $supplier)
                    <option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-2">
            <label for="type_id_of_items">идентификация (фильтр)</label>
            <select name="type_of_items" data-placeholder="выбрать" id="type_id_of_items" class="standardSelect" disabled>
                <option value="0">без идентификации</option>
                <option value="1">наружный блок сплит</option>
                <option value="2">внутренный блок сплит</option>
                <option value="3">наружный блок коммерческий</option>
                <option value="4">внутренный блок коммерческий</option>
                <option value="5">комплектация коммерческий</option>
            </select>
        </div>
        <table id="remaining_table" class="table table-bordered table-hover" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>артикул</th>
                <th>SAP код</th>
                <th>наименование</th>
                <th>инвен.</th>
                <th id="remaining_place">остаток</th>
                <th>ед. изм.</th>
                <th>склад/линии</th>
                {{--<th>invoice №</th>--}}
            </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
            <tr>
                <th>артикул</th>
                <th>SAP код</th>
                <th>наименование</th>
                <th>инвен.</th>
                <th id="remaining_place">остаток</th>
                <th>ед. изм.</th>
                <th>склад/линии</th>
                {{--<th>invoice №</th>--}}
            </tr>
            </tfoot>
        </table>
    </div>
</div>
@section('additionalLibrary')
    <script>
        $(document).ready(function() {
            fetch_data()
            console.log(sessionStorage.getItem('selectedFilters'))
        });

        $("input[name=date]").on("change", function (e) {
            e.preventDefault();
            $("input[name=date]").attr("disabled", true);
            fetch_data();
        });
        $("#storage").on("change", function (e) {
            fetch_data();
        });
        $("#suppliers").on("change", function (e) {
            fetch_data();
        });
        $("#models_id").on("change", function (e) {
            fetch_data();
        });

        $("#type_id_of_items").on("change", function (e) {
            fetch_data();
        });

        function fetch_data() {
            $('#storage').attr('disabled', true).trigger('chosen:updated');
            $('#models_id').attr('disabled', true).trigger('chosen:updated');
            $('#suppliers').attr('disabled', true).trigger('chosen:updated');
            $('#type_id_of_items').attr('disabled', true).trigger('chosen:updated');
            var models_id = $('#models_id').val();
            var suppliers = $('#suppliers').val();
            var storages = $('#storage').val();
            var filterText = '';
            console.log(storages)
            var type_id = $('#type_id_of_items').val();
            $('#remaining_table').DataTable().destroy();
            $("input[name=date]").attr("disabled", true);
            var date = $("input[name=date]").val();
            $("input[name=date]").attr("disabled", false);
            if ($('#storage').find('option:selected').text()!='') {
                filterText += $('#storage').find('option:selected').text();
            }
            var table = $('#remaining_table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url:"{{ route('show_remaining_by_storage') }}",
                    type: "post",
                    data: {
                        date:date, suppliers_id:suppliers,models_id:models_id, type_id:type_id, storages:storages
                    },
                    dataSrc: function (data) {
                        $(table.column(4).footer()).text(' итог: '+data['total']);
                        return data.data;
                    }
                },
                "columnDefs": [
                    {
                        "targets": [ 3 ],
                        "visible": false,
                        "searchable": false
                    }
                ],
                columns: [
                    { data: 'articula_new', width: "8%"},
                    { data: 'sap_code', width: "8%"},
                    { data: 'item_name',width: "30%",},
                    { data: 'last_remaining'},
                    { data: 'remaining'},
                    { data: 'unit'},
                    { data: 'storage_name',width: "15%"},
                    // { data: 'invoices_numbers',width: "20%"}
                ],
                order:[[1, 'desc']],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение от _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "",
                    "infoFiltered": "((<b>отфильтровано из _TOTAL_ записей</b>))",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                pageLength:100,
                lengthMenu:[[100, 500, 1000, -1], [100, 500, 1000, "All"]],
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>',
                        title: function(){
                            return $('#date').val()+' | '+filterText;
                        },
                    },
                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>'
                    },
                ],
                initComplete: function () {
                    $('#storage').attr('disabled', false).trigger('chosen:updated');
                    $('#models_id').attr('disabled', false).trigger('chosen:updated');
                    $('#suppliers').attr('disabled', false).trigger('chosen:updated');
                    $('#type_id_of_items').attr('disabled', false).trigger('chosen:updated');
                },
                "scrollY": "65vh",
            });

        }
    </script>
@endsection

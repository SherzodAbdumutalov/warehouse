@section('additional_css')
    <style>
        #requests_table tbody tr{
            cursor: pointer;
        }
        #all-orders-header-filters{
            display: flex;
            justify-content: space-between;
            flex-wrap: wrap;
        }
        #date-filter-header-all-orders{
            display: flex;
            width: 500px;
            justify-content: space-between;
        }
    </style>
@endsection
<div class="panel panel-primary">
    <div class="panel-heading" style="padding: 1px 5px">
        Все заявки
    </div>
    <div class="alert alert-danger" style="display: none;">
        <ul>

        </ul>
    </div>
    <div class="panel-body">
        <div id="all-orders-header-filters">
            <a href="{{ route('request') }}" class="btn btn-info " style="font-size: 15px"><i class="fa fa-plus"></i>создать</a>
            <div class="col-md-2" id="date-filter-header-all-orders">
                <span>дата заявки</span><br>
                <span><i class="fa fa-calendar"></i> с:</span> <input type="date" value="" id="start" name="start_date" onkeydown="return false" style="font-size: 11px"/>

                <span class="ml-4"><i class="fa fa-calendar"></i> по:</span> <input type="date" value="" id="end" name="end_date" onkeydown="return false" style="font-size: 11px"/>
            </div>
        </div>

        <table id="requests_table" class="table table-hover table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>№</th>
                <th>статус</th>
                <th>прогресс</th>
                <th>дата</th>
                <th>отправитель</th>
                <th width="100px">артикул</th>
                <th>наименование</th>
                <th width="70px">кол.</th>
                <th>получатель</th>
                <th>примечание</th>
                <th>автор</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
            <tr>
                <th>№</th>
                <th>статус</th>
                <th>прогресс</th>
                <th>дата</th>
                <th>отправитель</th>
                <th width="100px">артикул</th>
                <th>наименование</th>
                <th width="70px">кол.</th>
                <th>получатель</th>
                <th>примечание</th>
                <th>автор</th>
            </tr>
            </tfoot>
        </table>
    </div>


    <div id="show_detail" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <p style="color: red; font-weight: bold; display: inline-block" id="accepted"></p>|
                    <p style="font-weight: bold; display: inline-block" id="model_info"></p>
                    <button type="button" class="close" data-dismiss="modal" id="close_detail_modal"><span style="font-size: 24px">&times;</span></button>
                </div>
                <div class="alert alert-info" style="display: none; line-height: 12px; padding: 0">
                    <ul>

                    </ul>
                </div>
                <div class="modal-body">
                    <ul class="nav nav-tabs" id="operation_tab">
                        <li class="active"><a data-toggle="tab" href="#detailTab"><i class="fas fa-clipboard-list"></i> детали<span class="badge"></span></a></li>
                        <li><a data-toggle="tab" href="#historyTab"><i class="fas fa-history"></i> история</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="detailTab" class="tab-pane fade in active">
                            <a href="" id="export_to_pdf" target="_blank" hidden>pdf</a>
                            <table id="detail_table" class="table table-hover table-bordered" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>артикул</th>
                                    <th>стр арт.</th>
                                    <th>наименование</th>
                                    <th>SAP код</th>
                                    <th>кол-во</th>
                                    <th>расход</th>
                                    <th></th>
                                    <th>остаток</th>
                                    <th>ед. изм.</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>артикул</th>
                                    <th>стр арт.</th>
                                    <th>наименование</th>
                                    <th>SAP код</th>
                                    <th id="total">кол-во</th>
                                    <th>расход</th>
                                    <th></th>
                                    <th>остаток</th>
                                    <th>ед. изм.</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div id="historyTab" class="tab-pane fade">


                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <form >
                        <input type="hidden" name="request_id" id="request_id" value="0">
                        <button type="button" class="btn btn-warning"  onclick="close_request_function()" id="close_request" style="display: none">закрыть заявку</button>
                        {{--<button type="button" class="btn btn-danger"  onclick="delete_request_function()" id="delete_request" style="display: none">удалить заявку</button>--}}
                    </form>
                </div>
            </div>

        </div>
    </div>


    {{--edit--}}
    <div id="edit_request_modal" class="modal fade" role="dialog">
        <div class="modal-dialog"  style="width: 30%">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    изменить
                    <button type="button" class="close" data-dismiss="modal" id="close_detail_modal"><span style="font-size: 24px">&times;</span></button>
                </div>
                <div class="alert alert-success" style="display:none; color: black">
                    <ul>

                    </ul>
                </div>
                <form action="" method="post">
                    <div class="modal-body">
                        <input type="hidden" name="edit_request_id" id="edit_request_id">
                        <label for="description" style="font-size: 25px; font-weight: bold">примечание</label>
                        <textarea name="description" id="description"  rows="10" style="resize: none" class="form-control"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" onclick="update_request()"><i class="fa fa-save"></i> сохранить</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
@section('additionalLibrary')
    <script>
        $(document).ready(function() {
            var now = new Date();

            var day = ("0" + now.getDate()).slice(-2);
            var month = ("0" + (now.getMonth() + 1)).slice(-2);

            var today = now.getFullYear()+"-"+(month)+"-"+(day);
            $('#end').val(today);
            //$('#start').val(today);
            var url_text = window.location.href;
            var url = new URL(url_text);
            var id = url.searchParams.get('id');
            $('#loading').hide();
            fetch_data();
            console.log(id)
            var oTable = $('#requests_table').dataTable();
            if (id!=null){
                oTable.fnFilter( id );
            }else {
                oTable.fnFilter('');
            }
            $('table.display').DataTable();
            $('a[data-toggle="tab"]').on('shown.bs.tab', function () {
                $('table.history').DataTable().columns.adjust().draw();
                $('#detail_table').DataTable().columns.adjust().draw();
            } );

            $('#requests_table').on('dblclick', 'tr', function (e) {
                var id = $(this).data('id');
                $('#show_detail').modal("show");
                show_detail(id);
            });
            $('#requests_table tbody').on( 'click', 'tr', function () {
                $(this).removeClass('selected');

                if ( $(this).hasClass('selected') ) {
                    $(this).removeClass('selected');
                }
                else {
                    $('#requests_table').DataTable().$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            } );
        } );

        $('#end').on('change', ()=>{
            fetch_data()
        })
        $('#start').on('change', ()=>{
            fetch_data()
        })



        function fetch_data() {
            $("#requests_table").DataTable().destroy();
            let sDate = $('#start').val();
            let eDate = $('#end').val();
            console.log(sDate, eDate);
            var table = $('#requests_table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    "url": "{{ route('get_requests') }}",
                    "type": "post",
                    "data": {
                            start: sDate,
                            end: eDate,
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText)
                        console.log(json)
                        $('.alert-info').show();
                        $('.alert-info ul').empty();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').append('<li>'+value+'</li>');
                        })
                    }
                },
                columns: [
                    { "width": "3%", "data": "id" },
                    { "width": "3%", "data": "state" },
                    { "width": "10%", "data": "progress" },
                    { "width": "8%", "data": "create_time" },
                    { "width": "6%", "data": "to_storage" },
                    { "width": "5%", "data": "articula_new" },
                    { "width": "25%", "data": "item_name" },
                    { "width": "3%", "data": "amount" },
                    { "width": "10%", "data": "from_storage" },
                    { "width": "10%", "data": "description" },
                    { "width": "6%", "data": "applier" },
                ],
                pageLength:100,
                lengthMenu:[[100, 500, 1000], [100, 500, 1000]],
                stateSave:true,
                order: [[ 3, "desc" ]],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение от _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "",
                    "infoFiltered": "(отфильтровано из _MAX_ записей)",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr('data-id', data.id);
                },
                drawCallback: function (data) {
                    this.api().columns([1,4,8,10]).every( function () {
                        var column = this;
                        var select = $('<select><option value="">--все--</option></select>')
                            .appendTo( $(column.footer()).empty() )
                            .on( 'change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val, true, false )
                                    .draw();
                            } );

                        for (var i=0; i<data.json.filteredData[column[0][0]].length; i++){
                            if (data.json.filters[column[0][0]] === data.json.filteredData[column[0][0]][i]['name']) {
                                select.append( '<option value="'+data.json.filteredData[column[0][0]][i]['name']+'" selected>'+data.json.filteredData[column[0][0]][i]['name']+'</option>' )
                            }else {
                                select.append( '<option value="'+data.json.filteredData[column[0][0]][i]['name']+'">'+data.json.filteredData[column[0][0]][i]['name']+'</option>' )
                            }
                        }
                    } );
                },
                scrollY: "70vh",
            });
        }

        function show_detail(request_id) {
            $('#export_to_pdf').attr('href', '{{ \Illuminate\Support\Facades\URL::route('pdf_remaining_generate') }}/'+request_id);
            $('#loading').show();
            $('#request_id').val(request_id);
            createTables();
            $('#delete_request').hide();
            $('#close_request').hide();
            $('#show_detail').on('shown.bs.modal', function() {
                fetch_detail_table();
                setTimeout(function () {
                    $('table.history').DataTable().destroy();
                    fetch_history_table();
                }, 1000)
            });
        }

        function fetch_detail_table() {
            var request_id = $('#request_id').val();
            $('#detail_table').DataTable().destroy();
            var table = $('#detail_table').DataTable({
                ajax: {
                    "url": "{{ route('get_request_list_items') }}",
                    "type": "get",
                    "data": {request_id:request_id},
                    "dataSrc": function (data) {
                        if (!data['hasPerm']) {
                            $('#delete_request').hide();
                            $('#close_request').hide();
                        }else{
                            if (data['requestData'].state==1){
                                $('#delete_request').show();
                                $('#close_request').show();
                            } else {
                                $('#delete_request').hide();
                                $('#close_request').hide();
                            }
                        }
                        if (data['requestData'].state==1){
                            $('#accepted').text('№'+request_id+' заявка открыта').css('color', 'blue');
                        } else {
                            $('#accepted').text('№'+request_id+' заявка закрыта').css('color', 'red');
                        }
                        if (data['model'] != null) {
                            var desc = '';
                            if (data['model'].to_storage==null) {
                                if (data['model'].description==null) {
                                    desc = 'Назначение не известно';
                                }else{
                                    desc = data['model'].description;
                                }
                            }else{
                                desc = data['model'].to_storage;
                            }
                            if (data['model'].articula_new == null) {
                                $('#model_info').show().html('Назначение: '+desc+' | Дата: '+data['model'].create_time).css('color', 'black');
                            }else{
                                $('#model_info').show().html('модель: '+data['model'].articula_new+' '+data['model'].item_name+' <b>|</b> кол-во '+data['model'].amount+' <b>|</b> Назначение: '+desc+' | Дата: '+data['model'].create_time).css('color', 'black');
                            }
                        } else {
                            $('#model_info').hide();
                        }
                        $('#loading').hide();
                        $(table.column(4).footer()).text(' итог: '+data['total']);

                        return data.data;
                    }
                },
                columnDefs:[
                    { orderable: false, targets: [6] },
                    {
                        "targets": [ 1 ],
                        "visible": false,
                        "searchable": false
                    },
                ],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "_TOTAL_ записей",
                    "infoEmpty": "нет записей",
                    "infoFiltered": "(_MAX_)",
                    "search": "<i class='fa fa-search' style='float: left'></i>"
                },
                columns:[
                    {'data':'articula_new', 'width':'10%'},
                    {'data':'articula_old', 'width':'10%'},
                    {'data':'item_name', 'width':'30%'},
                    {'data':'sap_code', 'width':'10%'},
                    {'data':'amount', 'width':'6%'},
                    {'data':'output', 'width':'6%'},
                    {'data':'input_field', 'width':'10%'},
                    {'data':'remaining', 'width':'10%'},
                    {'data':'unit', 'width':'6%'}
                ],
                paging: false,
                bPaginate: false,
                dom: 'Blfrtip',
                buttons: [
                    {
                        text: '<i class="glyphicon glyphicon-print" style="font-size: 18px"></i>',
                        action: function ( e, dt, node, config ) {
                            window.open(document.getElementById('export_to_pdf').href, '_blank');
                        }

                    },
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0, 2, 3, 4, 5]
                        },
                        text: '<i class="fa fa-file-excel-o"></i>'

                    }
                ],
                scrollY: "55vh",
                scrollCollapse: true

            });
        }

        function createTables() {
            var request_id = $('#request_id').val();
            $('#historyTab').empty();
            $.ajax({
                "url": "{{ route('get_accepted_request_list_history') }}",
                "type": "get",
                "data": {request_id:request_id},
                success: function (data) {
                    var items = $.parseJSON(data);
                    console.log(items)
                    $.each(items, function( index, value ) {
                        drawTable(value, index);
                    });
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText)
                    console.log(json)
                    $('.alert-info').show();
                    $('.alert-info ul').empty();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').append('<li>'+value+'</li>');
                    })
                }

            });
        }

        function drawTable(items, key) {
            var total = 0;
            var accepted_time = '';
            var str = '';
            $.each(items, function (index, item) {
                total += item.output;
                accepted_time = item.accepted_time.substr(0, 16);
                str += '<tr><td>'+item.articula_new+'</td><td>'+item.articula_old+'</td><td>'+item.item_name+'</td><td>'+item.sap_code+'</td><td>'+item.output+'</td><td>'+item.unit+'</td></tr>'
            });
            $('#historyTab').append('<p style="display: block; float: left; margin-top: 5px; color: black">'+accepted_time+'</p>&nbsp;<a href="" id="export_to_pdf'+key+'" class="btn btn-info" style="margin-bottom: 0; margin-top: 5px" target="_blank" hidden><i class="fa fa-print"></i></a>' +
                '<table class="table table-hover table-bordered history" cellspacing="0" width="100%">' +
                '<thead><tr><th>артикул</th><th>стр арт.</th><th>наименование</th><th>SAP код</th><th>расход</th><th>ед. изм.</th></tr></thead><tbody>'+str+'</tbody>' +
                '<tfoot><tr><th>артикул</th><th>стр арт.</th><th>наименование</th><th>SAP код</th><th> итого: '+total+'</th><th>ед. изм.</th></tr></tfoot></table>')

            $('#export_to_pdf'+key).attr('href', '{{ \Illuminate\Support\Facades\URL::route('pdf_remaining_generate_by_history') }}/'+key);
        }

        function fetch_history_table() {
            $('table.history').DataTable({
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "_TOTAL_ записей",
                    "infoEmpty": "нет записей",
                    "infoFiltered": "(_MAX_)",
                    "search": "<i class='fa fa-search' style='float: left'></i>"
                },
                columns:[
                    {'width':'10%'},
                    {'width':'10%'},
                    {'width':'30%'},
                    {'width':'10%'},
                    {'width':'6%'},
                    {'width':'6%'}
                ],
                paging: false,
                bPaginate: false,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5]
                        },
                        text: '<i class="fa fa-file-excel-o"></i>'

                    }
                ],
                scrollY: "50vh",
                scrollCollapse: true
            });
        }

        function delete_request_function(){
            if (confirm('удалить')) {
                var request_id = $('#request_id').val()
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'POST',
                    url:"{{route('delete_request')}}",
                    data:{request_id:request_id},
                    success: function (data) {
                        $('#close_detail_modal').click()
                        fetch_data()
                        $('.alert-danger').show()
                        $('.alert-danger ul').empty().append('<li>удален</li>')
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText)
                        console.log(json)
                        $('.alert-info').show();
                        $('.alert-info ul').empty();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').append('<li>'+value+'</li>');
                        })
                    }
                })
            }
        }

        function close_request_function() {
            if (confirm('закрыть')) {
                var request_id = $('#request_id').val()
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'POST',
                    url:"{{route('close_request')}}",
                    data:{request_id:request_id},
                    success: function (data) {
                        $('#close_detail_modal').click()
                        fetch_data()
                        $('.alert-danger').show()
                        $('.alert-danger ul').empty().append('<li>закрыт</li>')
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText)
                        console.log(json)
                        $('.alert-info').show();
                        $('.alert-info ul').empty();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').append('<li>'+value+'</li>');
                        })
                    }
                })
            }
        }

        function set_data_for_edit_request(request_id, description) {
            $('#edit_request_id').val(request_id)
            $('textarea#description').val(description)
        }

        function update_request() {
            var request_id = $('#edit_request_id').val()
            var desc = $('textarea#description').val()
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: "{{ route('update_request') }}",
                type: "post",
                data: {request_id:request_id, description:desc},
                success: function (data) {
                    fetch_data()
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    $('.alert-danger').show();
                    $.each(json.errors, function (index, value) {
                        $('.alert-danger ul').empty().append('<li>'+value+'</li>');
                    })
                }
            })
        }

    </script>
@endsection

@section('additional_css')
    <style>
        input[type=checkbox]
        {
            /* Double-sized Checkboxes */
            -ms-transform: scale(2); /* IE */
            -moz-transform: scale(2); /* FF */
            -webkit-transform: scale(1.5); /* Safari and Chrome */
            -o-transform: scale(2); /* Opera */
            margin-left: 5px;
        }

    </style>
@endsection
<div class="panel panel-primary">

    <div class="panel-heading clearfix" style="padding: 1px 5px">
        распределение по складам
    </div>

    <div class="panel-body">
        <div class="alert alert-info" style="display: none">
            <ul>

            </ul>
        </div>
        <div class="col-md-12 clearfix">
            <div class="col-md-2">
                <select name="storage_id" id="storage_id" class="standardSelect">
                    <option value="0">--выбрать склад--</option>
                    @foreach($storages as $storage)
                        <option value="{{ $storage->id }}">{{ $storage->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-1">
                <a href="{{ route('import_storage_items') }}" class="btn btn-warning" style="font-size: 11px" target="_blank">импортировать</a>
            </div>
        </div>

        <table id="storage_items" class="table table-bordered table-hover" cellspacing="0" width="100%" disabled>
            <thead>
            <tr>
                <th></th>
                <th>склад</th>
                <th>артикул</th>
                <th>стр. артикул</th>
                <th>SAP код</th>
                <th>наименование</th>
                <th style="padding-left: 0"></th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>
@section('additionalLibrary')

    <script>
        $(document).ready(function() {
            fetch_data()
        } );

        $('#storage_id').on("change", function () {
            fetch_data()
        })

        function fetch_data() {
            $('#storage_items').DataTable().destroy()

            var storage_id = $('#storage_id').val()
            $('#storage_items').DataTable( {
                processing: true,
                serverSide:true,
                columnDefs:[
                    { orderable: false, targets: [0, 6] },
                ],
                stateSave: true,
                order:[[2, 'asc']],
                "ajax": {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "{{ route('getStorageItems') }}",
                    type: "post",
                    data: {
                        storage_id:storage_id
                    }
                },
                pageLength: 100,
                lengthMenu:[[100, 500, 1000, -1], [100, 500, 1000, "All"]],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "_TOTAL_ записей",
                    "infoEmpty": "данные отсутствует",
                    "infoFiltered": "(<b>отфильтровано из _MAX_ записей</b>)",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 1, 2, 3, 4, 5]
                        },
                        title:'бум лист',
                        text: '<i class="fa fa-file-excel-o"></i>'


                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: [ 1, 2, 3, 4, 5]
                        },
                        title:'бум лист',
                        text: '<i class="fa fa-file-pdf-o"></i>'

                    },
                ],
                columns: [
                    { data: 'binding_checkbox', width:'3%' },
                    { data: 'storages', width:'10%' },
                    { data: 'articula_new', width:'10%' },
                    { data: 'articula_old', width:'10%' },
                    { data: 'sap_code', width:'10%' },
                    { data: 'item_name', width:'50%' },
                    { data: 'item_units', width:'5%' },
                ],
                scrollY: "70vh",
            } );
        }

        function save_item(item_id){
            var storage_id = $('#storage_id').val()
            var unit_id = $("input[name='unit"+item_id+"']:checked").val();
            console.log(unit_id)
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type:'POST',
                url:"{{route('save_storage_item')}}",
                data:{item_id:item_id, storage_id:storage_id, unit_id:unit_id},
                success:function(data){
                    fetch_data()
                },
                error: function (request, status, error) {
                    fetch_data()
                    var json = $.parseJSON(request.responseText)
                    console.log(json)
                    $('.alert-info').show();
                    $('.alert-info ul').empty()
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').append('<li>'+value+'</li>');
                    })
                }
            });
        }

        function update_unit(item_id, unit_id){
            var storage_id = $('#storage_id').val()
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type:'POST',
                url:"{{route('update_unit')}}",
                data:{item_id:item_id, unit_id:unit_id, storage_id:storage_id},
                success:function(data){
                    fetch_data()
                },
                error: function (request, status, error) {
                    fetch_data()
                    var json = $.parseJSON(request.responseText)
                    console.log(json)
                    $('.alert-info').show();
                    $('.alert-info ul').empty()
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').append('<li>'+value+'</li>');
                    })
                }
            });
        }
    </script>
    {{--dataTables library--}}

@endsection
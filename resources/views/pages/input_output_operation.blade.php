@section('additional_css')
    <style>
        .modal-body {
            max-height: calc(100vh - 200px);
            overflow-y: auto;
            padding-top: 0;
            margin-top: 0;
        }

        .modal-dialog {
            width: calc(100% - 100px);
        }
        input[type=checkbox]
        {
            /* Double-sized Checkboxes */
            -ms-transform: scale(2); /* IE */
            -moz-transform: scale(2); /* FF */
            -webkit-transform: scale(1.5); /* Safari and Chrome */
            -o-transform: scale(2); /* Opera */
           padding: 10px;
            width: 15px;







        }

        .table > tbody > tr > td{
            font-size: 12px;
            line-height: 20px;
        }

        .table > thead > tr > th {
            font-weight: bold;
        }

        #requests_table tbody tr{
            cursor: pointer;
        }

        #moving_request_table tbody tr{
            cursor: pointer;
        }

        #returns_table tbody tr{
            cursor: pointer;
        }
    </style>
@endsection
<div class="panel panel-primary">

    <div class="panel-heading" style="padding: 1px 5px">
        Управление складом
    </div>
    <div class="panel-body">
        <div class="alert alert-info" style="display:none">
            <ul>

            </ul>
        </div>
        <div class="col-md-12 clearfix" style="margin-bottom: 10px; padding-left: 0">
            <div class="col-md-2" style="padding-left: 0">
                <select name="storage_id" id="storage_id" class="standardSelect" >
                    @foreach($storages as $storage)
                        <option value="{{ $storage->id }}">{{ $storage->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div id="tabs">
            <ul class="nav nav-tabs" id="operation_tab">
                <li><a data-toggle="tab" href="#byRequest" data-title="hello"><i class="fas fa-clipboard-list"></i> расход по заявкам <span class="badge" id="requestsCount"></span></a></li>
                <li><a data-toggle="tab" href="#byOrder"><i class="fas fa-file-text"></i> приход по заказам <span class="badge" id="ordersCount"></span></a></li>
                <li><a data-toggle="tab" href="#byReturns"><i class="fa fa-backward"></i> Возврат <span class="badge" id="returnsCount"></span></a></li>
                <li><a data-toggle="tab" href="#byMovingRequest"><i class="fas fa-clipboard-list"></i> приходы по производству <span class="badge" id="movingRequestsCount"></span></a></li>
                <li><a data-toggle="tab" href="#byHand"><i class="fa fa-hand-paper-o"></i> ручная операция</a></li>
            </ul>

            <div class="tab-content">
                <div id="byRequest" class="tab-pane fade">
                    <table id="requests_table" class="table table-hover table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>прогресс</th>
                            <th>автор</th>
                            <th>дата заявки</th>
                            <th>артикул</th>
                            <th>наименование</th>
                            <th>кол-во</th>
                            <th>назначение</th>
                            <th>примечание</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>№</th>
                            <th>прогресс</th>
                            <th>автор</th>
                            <th>дата заявки</th>
                            <th>артикул</th>
                            <th>наименование</th>
                            <th>кол-во</th>
                            <th>с</th>
                            <th>примечание</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <div id="byOrder" class="tab-pane fade">
                    <div class="col-md-2" style="padding-left: 0">
                        <span>поставщик/база (фильтр)</span>
                        <select name="filterBySupplier" data-placeholder="выбрать" id="filterBySupplier" class="standardSelect">
                            <option value=""></option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <span>invoice логистики (Фильтр)</span>
                        <select name="filter_by_invoice_numbers" id="filter_by_invoice_numbers" class="standardSelect" data-placeholder="выбрать" multiple>
                        </select>
                    </div>
                    <table id="orders_table" class="table table-hover table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th width="40px">id</th>
                            <th width="90px">артикул</th>
                            <th>SAP</th>
                            <th>наименование</th>
                            <th>поставщик</th>
                            <th width="100px">inv-ce лог.</th>
                            <th width="8%">Транспорт</th>
                            <th width="5%">автор</th>
                            <th>создано</th>
                            <th>дата приб.</th>
                            <th width="4%">кол.</th>
                            <th width="6%">прибыл</th>
                            <th width="10%"></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th width="40px">id</th>
                            <th width="90px">артикул</th>
                            <th>SAP</th>
                            <th>наименование</th>
                            <th>поставщик</th>
                            <th width="100px">inv-ce лог.</th>
                            <th width="8%">Транспорт</th>
                            <th width="5%">автор</th>
                            <th>создано</th>
                            <th>дата приб.</th>
                            <th width="4%">кол.</th>
                            <th width="6%">прибыл</th>
                            <th width="10%"></th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                    <form action="" method="post">
                        {{ csrf_field() }}
                        <div class="col-md-4">
                            <label for="order_description">вводите примечание</label>
                            <input name="order_description" id="order_description" maxlength="150" style="resize: none; height: 26px" placeholder="вводите текст... (макс 50 символов)" class="form-control">
                        </div>
                        <div class="col-md-8">
                            <button type="button" class="btn btn-warning" style="float: right; margin-top: 30px" id="save_brought_item_btn" onclick="save_brought_item()"><i class="fa fa-save"></i> сохранить изменение</button>
                        </div>
                    </form>
                </div>
                <div id="byReturns" class="tab-pane fade">
                    <table id="returns_table" class="table table-hover table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>автор</th>
                            <th>дата запроса</th>
                            <th>отправитель</th>
                            <th>примечание</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>№</th>
                            <th>автор</th>
                            <th>дата запроса</th>
                            <th>отправитель</th>
                            <th>примечание</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <div id="byMovingRequest" class="tab-pane fade">
                    <table id="moving_request_table" class="table table-hover table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>автор</th>
                            <th>дата запроса</th>
                            <th>отправитель</th>
                            <th>примечание</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>№</th>
                            <th>автор</th>
                            <th>дата запроса</th>
                            <th>отправитель</th>
                            <th>примечание</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <div id="byHand" class="tab-pane fade">
                    <div class="col-md-2">
                        <label for="operation"><b>операции</b></label>
                        <select name="operation" id="operation" class="standardSelect" >
                            {{--@foreach($operation_types as $type)--}}
                                {{--<option value="{{ $type->id }}" data-operation="{{ $type->operation }}">{{ $type->name }}</option>--}}
                            {{--@endforeach--}}
                        </select>
                    </div>
                    <div class="col-md-2" id="supplier_div">
                        <label for="supplier" style="font-weight: bold">поставщик</label>
                        <select name="supplier" id="supplier" class="standardSelect">
                            <option value="">без поставщика</option>
                            @foreach($suppliers as $supplier)
                                <option value="{{ $supplier->id }}" >{{ $supplier->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label for="item"><b>единицы</b></label>
                        <select name="item" data-placeholder="item select" id="item" class="standardSelect chosen-choices ">
                        </select>
                    </div>

                    <div class="col-md-1">
                        <label for="amount"><b>кол-во</b></label><br>
                        <input type="number" name="amount" min="0" id="amount" class="form-control" style="height: 26px; display: block">
                    </div>
                    <div class="col-md-1">
                        <button class="btn btn-warning" type="button" style="margin-top: 30px; padding: 3px 15px" onclick="add_item()"><i class="fa fa-plus"></i> добавить</button>
                    </div>

                    <table id="selected_items_table" class="table table-hover table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th style="padding-left: 0">артикул</th>
                            <th style="padding-left: 0">sap</th>
                            <th style="padding-left: 0">наименование</th>
                            <th id="total_of_selected_items" style="padding-left: 0">кол-во</th>
                            <th style="padding-left: 0">остаток</th>
                            <th style="padding-left: 0">ед. изм.</th>
                            <th style="padding-left: 0"></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th style="padding-left: 0">артикул</th>
                            <th style="padding-left: 0">sap</th>
                            <th style="padding-left: 0">наименование</th>
                            <th style="padding-left: 0">кол-во</th>
                            <th style="padding-left: 0">остаток</th>
                            <th style="padding-left: 0">ед. изм.</th>
                            <th style="padding-left: 0"></th>
                        </tr>
                        </tfoot>
                    </table>

                    <div class="col-md-4">
                        <label for="description" style="font-size: 16px; font-weight: bold">Примечание</label>
                        <input name="description" id="description" maxlength="50" style="resize: none; height: 26px" placeholder="вводите текст... (макс 50 символов)" class="form-control">
                    </div>
                    <div class="col-md-3">
                        <div style="float: left; padding-top: 30px">
                            <button class="btn btn-warning " type="submit" style="font-size: 12px; float: left" id="save_in_out" onclick="save_items()"><i class="glyphicon glyphicon-floppy-save" ></i> сохранить</button>
                            &nbsp;<button class="btn btn-danger " type="submit" style="font-size: 12px; float: right" onclick="clear_sessionStorage()"><i class="glyphicon glyphicon-remove-circle"></i> очистить</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{--edit amount--}}
<div id="edit_amount_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm" style="width: 20%">
        <div class="modal-content" >
            <div class="modal-header">
                изменить кол-во
                <button type="button" class="close" data-dismiss="modal" style="font-size: 38px" id="close_modal">&times;</button>
            </div>
            <div class="modal-body">
                <label for="edit_amount">кол-во</label>
                <input type="number" name="amount" id="edit_amount" step="0.0001" class="form-control">
                <input type="hidden" name="key" id="edit_key" value="">
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-warning" onclick="save_amount()"><i class="fa fa-save"></i> Сохранить</button>
            </div>
        </div>
    </div>
</div>

<div id="show_request_detail" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <p style="color: red; font-weight: bold; display: inline-block" id="accepted"></p>|
                <p style="font-weight: bold; display: inline-block" id="model_info"></p>
                <button type="button" class="close" data-dismiss="modal" id="close_request_detail_modal"><span style="font-size: 24px">&times;</span></button>
            </div>
            <div class="alert alert-danger" style="display:none">
                <ul>

                </ul>
            </div>
            <div class="modal-body">
                <ul class="nav nav-tabs" id="operation_tab">
                    <li class="active"><a data-toggle="tab" href="#detailTab"><i class="fas fa-clipboard-list"></i> детали<span class="badge"></span></a></li>
                    <li><a data-toggle="tab" href="#historyTab"><i class="fas fa-history"></i> история</a></li>
                </ul>
                <div class="tab-content">
                    <div id="detailTab" class="tab-pane fade in active">
                        <a href="" id="export_to_pdf" target="_blank" hidden>pdf</a>
                        <table id="request_list_table" class="table table-hover table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>артикул</th>
                                <th>стр арт.</th>
                                <th>наименование</th>
                                <th>SAP код</th>
                                <th>кол-во</th>
                                <th>расход</th>
                                <th></th>
                                <th>остаток</th>
                                <th>ед. изм.</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>артикул</th>
                                <th>стр арт.</th>
                                <th>наименование</th>
                                <th>SAP код</th>
                                <th id="total">кол-во</th>
                                <th>расход</th>
                                <th></th>
                                <th>остаток</th>
                                <th>ед. изм.</th>
                            </tr>
                            </tfoot>
                        </table>
                        <form>
                            <input type="hidden" name="request_id_for_accept" id="request_id_for_accept" value="">
                            <button class="btn btn-warning" type="button" id="accept_request_btn" style="display: none; float: right" onclick="accept_request()"><i class="fas fa-clipboard-check"></i> принять</button>
                        </form>
                    </div>
                    <div id="historyTab" class="tab-pane fade">


                    </div>
                </div>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>

<div id="show_returns_detail" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <p style="color: red; font-weight: bold; display: inline-block" id="returnsAccepted"></p>|
                <button type="button" class="close" data-dismiss="modal" id="close_returns_detail_modal"><span style="font-size: 24px">&times;</span></button>
            </div>
            <div class="alert alert-danger" style="display:none">
                <ul>

                </ul>
            </div>
            <div class="modal-body">
                {{--<a href="" id="returns_export_to_pdf" target="_blank" hidden>pdf</a>--}}
                <table id="returns_list_table" class="table table-hover table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>артикул</th>
                        <th>стр арт.</th>
                        <th>наименование</th>
                        <th>SAP код</th>
                        <th>кол-во</th>
                        <th>остаток</th>
                        <th>ед. изм.</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>артикул</th>
                        <th>стр арт.</th>
                        <th>наименование</th>
                        <th>SAP код</th>
                        <th id="returns_total">кол-во</th>
                        <th>остаток</th>
                        <th>ед. изм.</th>
                    </tr>
                    </tfoot>
                </table>
                <form>
                    <input type="hidden" name="returns_id_for_accept" id="returns_id_for_accept" value="">
                    <button class="btn btn-warning" type="button" id="returns_accept_btn" style="display: none; float: right" onclick="accept_returns()"><i class="fas fa-clipboard-check"></i> принять</button>
                </form>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>
<div id="show_moving_request_detail" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <p style="color: red; font-weight: bold; display: inline-block" id="accepted"></p>
                <button type="button" class="close" data-dismiss="modal" id="close_detail_modal"><span style="font-size: 24px">&times;</span></button>
            </div>
            <div class="alert alert-danger" style="display:none">
                <ul>

                </ul>
            </div>
            <div class="modal-body">
                <form action="{{route('acceptMovingRequestItems')}}" method="post">
                    @csrf
                <table id="moving_request_detail_table" class="table table-hover table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>артикул</th>
                        <th>стр арт.</th>
                        <th>наименование</th>
                        <th>SAP код</th>
                        <th>кол-во</th>
                        <th>accapted num</th>
                        <th>остаток</th>
                        <th>ед. изм.</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>артикул</th>
                        <th>стр арт.</th>
                        <th>наименование</th>
                        <th>SAP код</th>
                        <th id="total">кол-во</th>
                        <th></th>
                        <th>остаток</th>
                        <th>ед. изм.</th>
                    </tr>
                    </tfoot>
                </table>


                    <input type="hidden" name="moving_request_id_for_accept" id="moving_request_id_for_accept" value="">
                    <button class="btn btn-warning" type="submit" id="accept_moving_request_btn" style="display: none; float: right" ><i class="fas fa-clipboard-check"></i> принять</button>
                </form>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>
@section('additionalLibrary')
    {{--dataTables library--}}
    <script>
        $(document).ready(function() {
            var url_text = window.location.href;
            var url = new URL(url_text);
            var id = url.searchParams.get('id');

            if(id){
                var hash = '#'+id;
                $('a[href="' + hash +'"]').parent('li').addClass('active');
                $('#tabs .tab-content '+hash).addClass(' in active');
            } else {
                $('#tabs .tab-content div:first').addClass(' in active');
                $('#tabs ul li:first').addClass('active');
            }

            $('a[data-toggle="tab"]').on('shown.bs.tab', function () {
                $('table.history').DataTable().columns.adjust().draw();
                $('#request_list_table').DataTable().columns.adjust().draw();
            } );
            if(sessionStorage.getItem('storage_input_output_items')!=null){
                $('#operation').prop('disabled', true).trigger('chosen:updated');
                $('#supplier').prop('disabled', true).trigger('chosen:updated');
            }else{
                $('#operation').prop('disabled', false).trigger("chosen:updated");
                $('#supplier').prop('disabled', false).trigger("chosen:updated");
            }

            if(sessionStorage.getItem('last_selected_operation_id')!=null){
                $('#operation').val(sessionStorage.getItem('last_selected_operation_id')).trigger('chosen:updated')
            }

            if(sessionStorage.getItem('last_selected_supplier_id')!=null){
                $('#supplier').val(sessionStorage.getItem('last_selected_supplier_id')).trigger('chosen:updated')
            }
            $('a[data-toggle="tab"]').on('shown.bs.tab', function () {
                $('.alert-info').hide();
                $('.alert-danger').hide();
                $('#orders_table').DataTable().columns.adjust().draw(false);
                $('#returns_table').DataTable().columns.adjust().draw(false);
                $('#requests_table').DataTable().columns.adjust().draw();
                $('#selected_items_table').DataTable().columns.adjust().draw();
                $('#moving_request_table').DataTable().columns.adjust().draw();
            } );
            fetch_moving_request_table();
            fetch_request_table();
            fetch_orders_table();
            fetch_returns_table();
            $('#selected_items_table').DataTable( {
                columnDefs: [
                    { orderable: false, targets: [0, 1, 2, 3, 4, 5, 6] }
                ],
                responsive: true,
                order: false,
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение от _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "",
                    "infoFiltered": "(отфильтровано из _MAX_ записей)",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                paging: false,
                bPaginate: false,
                "columns": [
                    { "width": "8%" },
                    { "width": "8%" },
                    { "width": "50%" },
                    { "width": "5%" },
                    { "width": "5%" },
                    { "width": "5%" },
                    { "width": "5%" },
                ],
                scrollY: "50vh",
                scrollCollapse: true
            });

            $('input[name=amount]').on('keydown', function (e) {
                if(e.keyCode==13){
                    add_item();
                }
            });
            getFilterItemsByStorage();
            draw_table();
            getMovingItems();
            getOperationsByStorage();
            $('#moving_request_table').on('dblclick', 'tr', function (e) {
                var id = $(this).data('id');
                $('#show_moving_request_detail').modal("show");
                show_moving_request_detail(id);
            });
            $('#moving_request_table tbody').on( 'click', 'tr', function () {
                $(this).removeClass('selected');

                if ( $(this).hasClass('selected') ) {
                    $(this).removeClass('selected');
                }
                else {
                    $('#moving_request_table').DataTable().$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            } );

            $('#returns_table').on('dblclick', 'tr', function (e) {
                var id = $(this).data('id');
                $('#show_returns_detail').modal("show");
                show_returns_detail(id);
            });
            $('#returns_table tbody').on( 'click', 'tr', function () {
                $(this).removeClass('selected');

                if ( $(this).hasClass('selected') ) {
                    $(this).removeClass('selected');
                }
                else {
                    $('#returns_table').DataTable().$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            } );

            $('#requests_table').on('dblclick', 'tr', function (e) {
                var id = $(this).data('id');
                $('#show_request_detail').modal("show");
                show_request_detail(id);
            });

            if(window.innerWidth < 700){
                $('#requests_table').on('click', 'tr', function (e) {
                    var id = $(this).data('id');
                    $('#show_request_detail').modal("show");
                    show_request_detail(id);
                });
            }

            $('#requests_table tbody').on( 'click', 'tr', function () {
                $(this).removeClass('selected');

                if ( $(this).hasClass('selected') ) {
                    $(this).removeClass('selected');
                }
                else {
                    $('#requests_table').DataTable().$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            } );


            $('#filterBySupplier').on('change', function () {
                fetch_orders_table();
            })
            $('#filter_by_invoice_numbers').on('change', function () {
                fetch_orders_table();
            })
        } );
    </script>
    <script>
        $('#storage_id').on('change', function () {
            getFilterItemsByStorage();

            sessionStorage.removeItem('logistics_input_list');
            getOperationsByStorage();
            fetch_moving_request_table();
            getMovingItems();
            fetch_request_table();
            fetch_orders_table();
            fetch_returns_table();
        });
        $('#operation').on('change', function () {
            getMovingItems();
        });

        $('#supplier').on('change', function () {
            getMovingItems();
        });

        function getFilterItemsByStorage() {
            $('#filter_by_invoice_numbers').empty().trigger("chosen:updated");
            $('#filterBySupplier').empty().trigger("chosen:updated");
            var storage_id = $('#storage_id').val();
            $.ajax({
                url: "{{ route('getFilterItemsByStorage') }}",
                type: "get",
                data: {storage_id:storage_id},
                success: function(data){
                    $('#filterBySupplier').append('<option value=""></option>');
                    $.each(JSON.parse(data).storages, function (index, value) {
                        $('#filterBySupplier').append('<option value="'+value.storage_id+'">'+value.storage_name+'</option>');
                    });
                    $.each(JSON.parse(data).invoices, function (index, value) {
                        $('#filter_by_invoice_numbers').append('<option value="'+value.invoice_number+'">'+value.invoice_number+'</option>');
                    });
                    $('#filterBySupplier').trigger("chosen:updated");
                    $('#filter_by_invoice_numbers').trigger("chosen:updated");
                },
                error: function (request, status, error) {
                    $('#loading').hide();
                    var json = $.parseJSON(request.responseText);
                    console.log(json);
                    $('.alert-info').show();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').empty().append('<li>'+value+'</li>');
                    })
                }
            })
        }

        function getOperationsByStorage() {
            var storage_id = $('#storage_id').val();
            $('#operation').empty().trigger("chosen:updated");
            $.ajax({
                url: "{{ route('getOperationsByStorage') }}",
                type: "get",
                data: {storage_id:storage_id},
                success: function(data){
                    $.each(JSON.parse(data), function (index, value) {
                        $('#operation').append('<option value="'+value.id+'" data-operation="'+value.operation+'">'+value.name+'</option>');
                    });
                    $('#operation').trigger("chosen:updated");
                },
                error: function (request, status, error) {
                    $('#loading').hide();
                    var json = $.parseJSON(request.responseText);
                    console.log(json);
                    $('.alert-info').show();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').empty().append('<li>'+value+'</li>');
                    })
                }
            })
        }

        function getMovingItems() {
            var storage_id = $('#storage_id').val();
            var supplier_id = $('#supplier').val();
            var operation = $('#operation').val();
            console.log(operation);
            console.log(supplier_id);
            $('#item').empty().trigger("chosen:updated");
            $.ajax({
                url: "{{ route('getMovingItems') }}",
                type: "get",
                data: {operation_id:operation, supplier_id:supplier_id, storage_id:storage_id},
                success: function(data){
                    $.each(JSON.parse(data), function (index, value) {
                        $('#item').append('<option value="'+value.id+'">'+value.articula_new+' '+value.item_name+' '+value.sap_code+'</option>');
                    })
                    $('#item').trigger("chosen:updated");
                },
                error: function (request, status, error) {
                    $('#loading').hide();
                    var json = $.parseJSON(request.responseText);
                    console.log(json);
                    $('.alert-info').show();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').empty().append('<li>'+value+'</li>');
                    })
                }
            })
        }

        function add_item() {
            $('#loading').show();
            var storage_id = $('#storage_id').val();
            var operation = $('#operation').val();
            var supplier_id = $('#supplier').val();
            sessionStorage.setItem('last_selected_operation_id', operation);
            sessionStorage.setItem('last_selected_supplier_id', supplier_id);
            var item_id = $('#item').val();
            var str_amount = $('#amount').val();
            var amount = parseFloat(str_amount.replace(',', '.'));
            if(amount>0){
                $('#operation').prop('disabled', true).trigger("chosen:updated");
                $('#supplier').prop('disabled', true).trigger("chosen:updated");
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "{{ route('collect_item_input_output') }}",
                    type: "post",
                    data: {item_id:item_id, amount:amount, operation_id:operation, supplier_id:supplier_id, storage_id:storage_id},
                    success: function(data){
                        $('#amount').val('');
                        $('#loading').hide();
                        var items = $.parseJSON(data);
                        items.forEach(function (value, index) {
                            if(is_exist(value.item_id, value.amount)){
                                draw_table();
                            }else{
                                collect_to_array(value);
                            }
                        })
                    },
                    error: function (request, status, error) {
                        $('#loading').hide();
                        var json = $.parseJSON(request.responseText);
                        console.log(json);
                        $('.alert-info').show();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').empty().append('<li>'+value+'</li>');
                        })
                    }
                })
            }else{
                $('#loading').hide();
                alert('вводите кол-во!')
            }
        }

        function draw_table() {
            $('#save_in_out').prop('disabled', false).show();
            var operation = $('#operation').find(':selected').data('operation');
            var sum = 0;
            console.log(operation)
            $('#selected_items_table').DataTable().clear().draw(false);
            var items = $.parseJSON(sessionStorage.getItem('storage_input_output_items'));
            if (items!=null){
                items.forEach(function (value, key) {
                    if ((operation == -1)){
                        if(value.amount>value.remaining){
                            $('#save_in_out').prop('disabled', true).hide();
                        }
                    } else {
                        $('#save_in_out').prop('disabled', false).show();
                    }
                    var trow = $('#selected_items_table').DataTable().row.add([
                        value.articula_new,
                        value.sap_code,
                        value.item_name,
                        parseFloat(value.amount),
                        value.remaining,
                        value.unit,
                        '<button type="button" class="btn btn-warning " data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#edit_amount_modal" onclick="set_data_for_edit('+key+','+value.amount+')" style="font-size: 11px"><i class="fa fa-edit"></i></button> <button type="button" class="btn btn-danger " onclick="delete_item('+key+')" style="font-size: 11px" ><i class="fa fa-trash"></i></button>'
                    ]).node();
                    sum+=parseFloat(value.amount);
                    trow.id = 'row_'+key;
                    $('#selected_items_table').DataTable().draw(false);
                    if (Number(value.remaining)<Number(value.amount) && (operation==-1)){
                        $(trow).find('td:eq(4)').css('background-color', 'red');
                    }else {
                        $(trow).find('td:eq(4)').css('background-color', '');
                    }
                })
            }else{
                $('#save_in_out').prop('disabled', true).hide();
            }
            $('#loading').hide();
            $($('#selected_items_table').DataTable().column(3).footer()).text(' итог: '+sum);
            console.log(sum)
        }

        function collect_to_array(item) {
            var old_items = [];
            if(sessionStorage.getItem('storage_input_output_items')!=null){
                old_items = $.parseJSON(sessionStorage.getItem('storage_input_output_items'))
            }
            var data = {
                'item_id':item.item_id,
                'articula_new':item.articula_new,
                'sap_code':item.sap_code,
                'item_name':item.item_name,
                'amount':parseFloat(item.amount),
                'remaining':item.remaining,
                'unit':item.unit,
                'unit_id':item.unit_id,
                'storage_id':item.storage_id,
            };
            old_items.push(data);
            sessionStorage.setItem('storage_input_output_items', JSON.stringify(old_items));
            draw_table();
        }

        function clear_sessionStorage() {
            $('.alert-info').hide();
            $('.alert-danger').hide();
            $('#operation').prop('disabled', false).trigger("chosen:updated");
            $('#supplier').prop('disabled', false).trigger("chosen:updated");
            sessionStorage.removeItem('storage_input_output_items');
            sessionStorage.removeItem('last_selected_operation_id');
            sessionStorage.removeItem('last_selected_supplier_id');
            $('#supplier').val('').trigger('chosen:updated');
            draw_table();
            getMovingItems();
        }

        function is_exist(item_id, amount) {
            var items = $.parseJSON(sessionStorage.getItem('storage_input_output_items'));
            if(items!=null){
                for (var i=0; i<items.length; i++){
                    if(item_id==items[i].item_id){
                        items[i].amount+=parseFloat(amount);
                        sessionStorage.setItem('storage_input_output_items', JSON.stringify(items));
                        return true;
                    }
                }
            }
            return false;
        }

        function delete_item(key) {
            var items = $.parseJSON(sessionStorage.getItem('storage_input_output_items'));
            items.splice(key, 1);
            sessionStorage.setItem('storage_input_output_items', JSON.stringify(items));
            draw_table()
        }

        function set_data_for_edit(key, amount) {
            $('#edit_amount').val(amount);
            $('#edit_key').val(key);
        }

        function save_amount() {
            var amount = $('#edit_amount').val();
            var key = $('#edit_key').val();
            var items = $.parseJSON(sessionStorage.getItem('storage_input_output_items'));
            if(amount>0){
                items[key]['amount']=amount;
                sessionStorage.setItem('storage_input_output_items', JSON.stringify(items));
                draw_table();
                $('#close_modal').click();
            }else{
                alert('недопустимое значение!!');
            }
        }

        function save_items() {
            var items = sessionStorage.getItem('storage_input_output_items');
            var desc = $('#description').val();
            var operation_id = $('#operation').val();
            var supplier_id = $('#supplier').val();
            var storage_id = $('#storage_id').val();
            console.log(supplier_id);
            if (items!=null){
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'POST',
                    url:"{{route('save_inputs_outputs_items')}}",
                    data:{items:items, description:desc, operation_id:operation_id, supplier_id:supplier_id, storage_id:storage_id},
                    success: function (data) {
                        console.log($.parseJSON(data));

                        $('#description').val('');
                        clear_sessionStorage();
                        $('.alert-info').show();
                        $('.alert-info ul').empty().append('<li>сохранено</li>');
                    },
                    error: function (request, status, error) {
                        $('#loading').hide();
                        var json = $.parseJSON(request.responseText);
                        console.log(json);
                        $('.alert-info').show();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').empty().append('<li>'+value+'</li>');
                        })
                    }
                })
            }else{
                $('.alert-info').show();
                $('.alert-info ul').empty().append('<li>внесите данные!</li>');
            }
        }

        function allow_save() {
            var op = $('#operation').attr('data-operation');
            console.log(op);
            var items = $.parseJSON(sessionStorage.getItem('storage_input_output_items'));
            var i = 0;
            if(op>0){
                for(i=0; i<items.length; i++){
                    if(items[i].amount>items[i].remaining_of_sender){
                        return false
                    }
                }
            }else{
                for(i=0; i<items.length; i++){
                    if(items[i].amount>items[i].remaining){
                        return false
                    }
                }
            }
            return true
        }
    //    orderorderorder

        function fetch_orders_table() {
            console.log(sessionStorage.getItem('logistics_input_list'))
            var storage_id = $('#storage_id').val();
            var suppliers_id = $('#filterBySupplier').val();
            var invoices = $('#filter_by_invoice_numbers').val();
            console.log(invoices)
            $('#orders_table').DataTable().destroy();
            $('#orders_table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    "url": "{{ route('get_orders_belongs_storage') }}",
                    "type": "post",
                    "data": {storage_id:storage_id, invoices:invoices, suppliers_id:suppliers_id},
                    "dataSrc": function (data) {
                        $('#ordersCount').text(data.activeOrders);
                        return data.data;
                    }
                },
                "createdRow": function ( row, data, index ) {
                    if (data.amount <= data.complete) {
                        $(row).css({'background-color':'#A9A9A9'}, {'color':'white'});
                    }
                    $( row ).find('td:eq(12)').attr('nowrap', 'nowrap');
                },
                columnDefs:[
                    { orderable: false, targets: [12] },
                    {
                        "targets": [ 9 ],
                        "visible": false,
                        "searchable": false
                    },
                ],
                order: [[ 0, "desc" ]],
                lengthMenu:[[100, 500, 1000], [100, 500, 1000]],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение от _START_ до _END_ из _TOTAL_ деталей",
                    "infoEmpty": "",
                    "infoFiltered": "(отфильтровано из _MAX_ заказов)",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                "columns": [
                    { "data": "order_id" },
                    { "data": "articula_new" },
                    { "data": "sap_code" },
                    { "data": "item_name", "width":"20%" },
                    { "data": "supplier_name" },
                    { "data": "invoice_number" },
                    { "data": "container_number" },
                    { "data": "customer" },
                    { "data": "create_time" },
                    { "data": "delivery_time" },
                    { "data": "amount" },
                    { "data": "complete" },
                    { "data": "input_field" },
                    { "data": "unit" },
                ],
                scrollY: "65vh",
                scrollCollapse: true,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13]
                        },
                        text: '<i class="fa fa-file-excel-o"></i>'
                    },
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5]
                        },
                        text: '<i class="fa fa-file-excel-o"></i>'

                    }
                ],
                drawCallback: function ( settings ) {
                    $('input[type=number]').on('focus', function (e) {
                        $(this).on('mousewheel.disableScroll', function (e) {
                            e.preventDefault()
                        })
                    })
                    $('input[type=number]').on('blur', function (e) {
                        $(this).off('mousewheel.disableScroll')
                    })
                    $('input[type=number]').on('keydown', function (e) {
                        if(!((e.keyCode > 95 && e.keyCode < 106)
                            || (e.keyCode > 47 && e.keyCode < 58)
                            || e.keyCode == 8 || e.keyCode == 190 || e.keyCode ==37 || e.keyCode == 39 || e.keyCode == 110 || e.keyCode == 86 || e.keyCode == 17)) {
                            return false;
                        }
                    });

                    var x = [];
                    if (sessionStorage.getItem('logistics_input_list') != null) {
                        x = JSON.parse(sessionStorage.getItem('logistics_input_list'));
                    }

                    $.each(settings.json.data, function (index, value) {
                        if (value.logistics_list_id in x) {
                            if (x[value.logistics_list_id]>=value.remaining) {
                                console.log(x[value.logistics_list_id])
                                $('#checkbox'+value.logistics_list_id).prop('checked', true);
                            }else{
                                $('#checkbox'+value.logistics_list_id).prop('checked', false);
                            }
                            $('#'+value.logistics_list_id).val(x[value.logistics_list_id]);
                        }
                    });
                }
            });
        }

        function save_brought_item() {
            $('.alert-info').hide();
            $('.alert-danger').hide();
            if (confirm('сохранить?')) {
                var items = sessionStorage.getItem('logistics_input_list');
                sessionStorage.removeItem('logistics_input_list');
                $('#save_brought_item_btn').hide();
                var storage_id = $('#storage_id').val();
                var order_description = $('#order_description').val();
                if(storage_id==''){
                    alert('вы не состоитест ни в одном складе!');
                }else {
                    // var obj = {};
                    // $('input[name^=brought_item_amount]').each(function () {
                    //     obj[$(this).attr('id')] = parseFloat($(this).val());
                    // });
                    // var obj_stringify = JSON.stringify(obj);
                    $.ajax({
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        type: 'post',
                        url: "{{route('save_brought_items')}}",
                        data: {items:items, storage_id:storage_id, order_description:order_description},
                        success: function (data) {
                            console.log(data)
                            $('#save_brought_item_btn').show();
                            fetch_orders_table();
                            getNotAcceptedRequestsAmount();
                            $('.alert-info').show();
                            $('.alert-info ul').empty().append('<li>сохранено</li>');
                        },
                        error: function (request, status, error) {
                            var json = $.parseJSON(request.responseText);
                            console.log(json);
                            $('.alert-info').show();
                            $('.alert-info ul').empty().append('<li>'+json.data+'</li>');
                        }
                    })
                }
            }
        }

        function save_value_in_session(e) {
            var id = e.id;
            var input_value = e.value;
            var old = {};
            if (sessionStorage.getItem('logistics_input_list')!=null) {
                old = JSON.parse(sessionStorage.getItem('logistics_input_list'));
            }
            console.log(old)
            old[id] = input_value;
            console.log(old)
            sessionStorage.setItem('logistics_input_list', JSON.stringify(old));
        }

        function fillInput(id, value) {
            if ($('#checkbox'+id).prop("checked")) {
                $('#'+id).val(value);

                var old = {};
                if (sessionStorage.getItem('logistics_input_list')!=null) {
                    old = JSON.parse(sessionStorage.getItem('logistics_input_list'));
                }
                console.log(old)
                old[id] = value;
                console.log(old)
                sessionStorage.setItem('logistics_input_list', JSON.stringify(old));
            }else{
                $('#'+id).val('');

                var old = {};
                if (sessionStorage.getItem('logistics_input_list')!=null) {
                    old = JSON.parse(sessionStorage.getItem('logistics_input_list'));
                }
                delete old[id];
                console.log(old)
                sessionStorage.setItem('logistics_input_list', JSON.stringify(old));
            }
        }

    //    requestrequestrequestrequestrequestrequest
        function fetch_request_table() {
            var storage_id = $('#storage_id').val();
            $("#requests_table").DataTable().destroy();
            var table = $('#requests_table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    "url": "{{ route('get_requests_belongs_storage') }}",
                    "type": "post",
                    "data":{storage_id:storage_id},
                    "dataSrc": function (data) {
                        $('#requestsCount').text(data.recordsTotal);
                        return data.data;
                    }
                },
                columns: [
                    { "width": "4%", "data": "id" },
                    { "width": "10%", "data": "progress" },
                    { "width": "10%", "data": "applier" },
                    { "width": "8%", "data": "create_time" },
                    { "width": "8%", "data": "articula_new" },
                    { "width": "20%", "data": "item_name" },
                    { "width": "5%", "data": "amount" },
                    { "width": "15%", "data": "from_storage" },
                    { "width": "15%", "data": "description" },
                ],
                columnDefs:[
                    { orderable: false, targets: [8] }
                ],
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr('data-id', data.id);
                },
                stateSave:true,
                order: [[ 3, "desc" ]],
                pageLength: 100,
                lengthMenu:[[100, 500, 1000], [100, 500, 1000]],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение от _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "",
                    "infoFiltered": "(отфильтровано из _MAX_ записей)",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                drawCallback: function (data) {
                    this.api().columns([2,7]).every( function () {
                        var column = this;
                        var select = $('<select><option value="">--все--</option></select>')
                            .appendTo( $(column.footer()).empty() )
                            .on( 'change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val, true, false )
                                    .draw();
                            } );
                        if (data.json.filteredData.length != 0) {
                            for (var i=0; i<data.json.filteredData[column[0]].length; i++){
                                if (typeof data.json.filters[column[0]] !== "undefined") {
                                    select.append( '<option value="'+data.json.filteredData[column[0]][i]+'" selected>'+data.json.filteredData[column[0]][i]+'</option>' )
                                }else {
                                    select.append( '<option value="'+data.json.filteredData[column[0]][i]+'">'+data.json.filteredData[column[0]][i]+'</option>')
                                }
                            }
                        }

                    } );
                },
                scrollY: "50vh",
                scrollCollapse: true
            });
        }

        function show_request_detail(request_id) {
            $('.alert-info').hide();
            $('.alert-danger').hide();
            $('#export_to_pdf').attr('href', '{{ \Illuminate\Support\Facades\URL::route('pdf_remaining_generate') }}/'+request_id);
            $('.alert-danger').hide();
             $('#loading').show();
            $('input[name=request_id_for_accept]').val(request_id);
            createTables();
            $('#show_request_detail').on('shown.bs.modal', function() {
                fetch_request_detail();
                $('table.history').DataTable().destroy();
                fetch_history_table();
                $('#loading').hide();
            });
        }

        function fetch_request_detail() {
            var request_id = $('#request_id_for_accept').val();
            $('#request_list_table').DataTable().destroy();
            var table = $('#request_list_table').DataTable({
                ajax: {
                    "url": "{{ route('get_request_list_items_by_storage') }}",
                    "type": "get",
                    "data": {request_id:request_id},
                    "dataSrc": function (data) {
                        if (data['allowToAccept']) {
                            $('#accept_request_btn').show();
                        } else {
                            $('#accept_request_btn').hide();
                        }

                        if (!data['hasPerm']) {
                            $('#delete_request').hide()
                        }
                        if (data['requestData'].state == 1){
                            $('#accepted').text('№'+request_id+' заявка открыта').css('color', 'blue');
                        } else {
                            $('#accepted').text('№'+request_id+' заявка закрыта').css('color', 'red');
                        }
                        if (data['model'] != null) {
                            $('#model_info').show().html('модель: '+data['model'].articula_new+' '+data['model'].item_name+' <b>|</b> кол-во '+data['model'].amount).css('color', 'black');
                        } else {
                            $('#model_info').hide();
                        }
                        $(table.column(4).footer()).text(' итог: '+data['total']);
                        return data.data;
                    }
                },
                "createdRow": function ( row, data, index ) {
                    if (data.diff === 0 || data.remaining_num <= 0) {
                        $(row).css({'background-color':'#A9A9A9'}, {'color':'white'});
                    }
                },
                columnDefs:[
                    { orderable: false, targets: [6] }
                ],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "_TOTAL_ записей",
                    "infoEmpty": "нет записей",
                    "infoFiltered": "(_MAX_)",
                    "search": "<i class='fa fa-search' style='float: left'></i>"
                },
                columns:[
                    {'data':'articula_new', 'width':'10%'},
                    {'data':'articula_old', 'width':'10%'},
                    {'data':'item_name', 'width':'30%'},
                    {'data':'sap_code', 'width':'10%'},
                    {'data':'amount', 'width':'6%'},
                    {'data':'output', 'width':'6%'},
                    {'data':'input_field', 'width':'10%'},
                    {'data':'remaining', 'width':'10%'},
                    {'data':'unit', 'width':'6%'}
                ],
                paging: false,
                bPaginate: false,
                dom: 'Blfrtip',
                buttons: [
                    {
                        text: '<i class="glyphicon glyphicon-print" style="font-size: 18px"></i>',
                        action: function ( e, dt, node, config ) {
                            window.open(document.getElementById('export_to_pdf').href, '_blank');
                        }

                    },
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5]
                        },
                        text: '<i class="fa fa-file-excel-o"></i>'

                    }
                ],
                scrollY: "50vh",
                scrollCollapse: true,
                drawCallback: function () {
                    $('input[type=number]').on('focus', function (e) {
                        $(this).on('mousewheel.disableScroll', function (e) {
                            e.preventDefault()
                        })
                    })
                    $('input[type=number]').on('blur', function (e) {
                        $(this).off('mousewheel.disableScroll')
                    })

                    $('input[type=number]').on('keydown', function (e) {
                        if(!((e.keyCode > 95 && e.keyCode < 106)
                            || (e.keyCode > 47 && e.keyCode < 58)
                            || e.keyCode == 8 || e.keyCode == 190 || e.keyCode ==37 || e.keyCode == 39 || e.keyCode == 110 || e.keyCode == 86 || e.keyCode == 17)) {
                            return false;
                        }
                    })
                }
            });
        }

        function createTables() {
            var request_id = $('#request_id_for_accept').val();
            $('#historyTab').empty();
            $.ajax({
                "url": "{{ route('get_accepted_request_list_history') }}",
                "type": "get",
                "data": {request_id:request_id},
                success: function (data) {
                    var items = $.parseJSON(data);
                    $.each(items, function( index, value ) {
                        drawTable(value, index);
                    });
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText)
                    console.log(json)
                    $('.alert-info').show();
                    $('.alert-info ul').empty();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').append('<li>'+value+'</li>');
                    })
                }

            });
        }

        function drawTable(items, key) {
            var total = 0;
            var accepted_time = '';
            var str = '';
            $.each(items, function (index, item) {
                total += item.output;
                accepted_time = item.accepted_time.substr(0, 16);
                str += '<tr><td>'+item.articula_new+'</td><td>'+item.articula_old+'</td><td>'+item.item_name+'</td><td>'+item.sap_code+'</td><td>'+item.output+'</td><td>'+item.unit+'</td></tr>'
            });
            $('#historyTab').append('<p style="display: block; float: left; margin-top: 5px; color: black">'+accepted_time+'</p>&nbsp;<a href="" id="export_to_pdf'+key+'" class="btn btn-info" style="margin-bottom: 0; margin-top: 5px" target="_blank" hidden><i class="fa fa-print"></i></a>' +
                '<table class="table table-hover table-bordered history" cellspacing="0" width="100%">' +
                '<thead><tr><th>артикул</th><th>стр арт.</th><th>наименование</th><th>SAP код</th><th>расход</th><th>ед. изм.</th></tr></thead><tbody>'+str+'</tbody>' +
                '<tfoot><tr><th>артикул</th><th>стр арт.</th><th>наименование</th><th>SAP код</th><th> итого: '+total+'</th><th>ед. изм.</th></tr></tfoot></table>')

            $('#export_to_pdf'+key).attr('href', '{{ \Illuminate\Support\Facades\URL::route('pdf_remaining_generate_by_history') }}/'+key);
        }

        function fetch_history_table() {
            $('table.history').DataTable({
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "_TOTAL_ записей",
                    "infoEmpty": "нет записей",
                    "infoFiltered": "(_MAX_)",
                    "search": "<i class='fa fa-search' style='float: left'></i>"
                },
                columns: [
                    {'width': '10%'},
                    {'width': '10%'},
                    {'width': '30%'},
                    {'width': '10%'},
                    {'width': '6%'},
                    {'width': '6%'}
                ],
                paging: false,
                bPaginate: false,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        },
                        text: '<i class="fa fa-file-excel-o"></i>'

                    }
                ],
                scrollY: "50vh",
                scrollCollapse: true
            });
        }

        function accept_request() {
            $('#accept_request_btn').attr('disabled', true).hide();
            if (confirm('сохранить?')) {
                $('#accept_request_btn').attr('disabled', true).hide();
                var request_id = $('#request_id_for_accept').val();
                var obj = {};
                $('input[name^=outputs]').each(function () {
                    if (parseFloat($(this).val())>0) {
                        obj[$(this).attr('id')] = parseFloat($(this).val());
                    }
                });
                var obj_stringify = JSON.stringify(obj);
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type: 'post',
                    url: "{{route('accept_request')}}",
                    data: {request_id:request_id, outputs:obj_stringify},
                    success: function (data) {
                        $('#close_request_detail_modal').click();
                        fetch_request_detail();
                        createTables();
                        fetch_request_table();
                        getNotAcceptedRequestsAmount();
                        $('.alert-danger').show();
                        $('.alert-danger ul').empty().append('<li>принято</li>');
                        $('#accept_request_btn').attr('disabled', false).show();

                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        console.log(json)
                        $('.alert-danger').show();
                        $('.alert-danger ul').empty().append('<li>'+json.data+'</li>');
                    }
                })
            }
        }


        //    returnsreturns
        function fetch_returns_table() {
            var storage_id = $('#storage_id').val();
            $("#returns_table").DataTable().destroy();
            var table = $('#returns_table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    "url": "{{ route('getReturnsForAccept') }}",
                    "type": "post",
                    "data":{storage_id:storage_id},
                    "dataSrc": function (data) {
                        $('#returnsCount').text(data.recordsTotal);
                        return data.data;
                    }
                },
                columns: [
                    { "width": "4%", "data": "id" },
                    { "width": "10%", "data": "applier" },
                    { "width": "8%", "data": "create_time" },
                    { "width": "15%", "data": "from_storage" },
                    { "width": "15%", "data": "description" },
                ],
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr('data-id', data.id);
                },
                stateSave:true,
                order: [[ 2, "desc" ]],
                pageLength: 100,
                lengthMenu:[[100, 500, 1000], [100, 500, 1000]],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение от _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "",
                    "infoFiltered": "(отфильтровано из _MAX_ записей)",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                drawCallback: function (data) {
                    this.api().columns([1,3]).every( function () {
                        var column = this;
                        var select = $('<select><option value="">--все--</option></select>')
                            .appendTo( $(column.footer()).empty() )
                            .on( 'change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val, true, false )
                                    .draw();
                            } );
                        if (data.json.filteredData.length != 0) {
                            for (var i=0; i<data.json.filteredData[column[0]].length; i++){
                                if (typeof data.json.filters[column[0]] !== "undefined") {
                                    select.append( '<option value="'+data.json.filteredData[column[0]][i]+'" selected>'+data.json.filteredData[column[0]][i]+'</option>' )
                                }else {
                                    select.append( '<option value="'+data.json.filteredData[column[0]][i]+'">'+data.json.filteredData[column[0]][i]+'</option>')
                                }
                            }
                        }

                    } );
                },
                scrollY: "50vh",
                scrollCollapse: true
            });
            $('#returns_table tbody').on( 'click', 'tr', function () {
                $(this).removeClass('selected');

                if ( $(this).hasClass('selected') ) {
                    $(this).removeClass('selected');
                }
                else {
                    table.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            } );
        }

        function show_returns_detail(returns_id) {
            $('.alert-info').hide();
            $('.alert-danger').hide();
            $('#returns_export_to_pdf').attr('href', '{{ \Illuminate\Support\Facades\URL::route('pdf_remaining_generate') }}/'+returns_id);
            $('.alert-danger').hide();
            $('#loading').show();
            $('input[name=returns_id_for_accept]').val(returns_id);
            $('#show_returns_detail').on('shown.bs.modal', function() {
                fetch_returns_detail();
                $('#loading').hide();
            });
        }

        function fetch_returns_detail() {
            var returns_id = $('#returns_id_for_accept').val();
            $('#returns_list_table').DataTable().destroy();
            var table = $('#returns_list_table').DataTable({
                ajax: {
                    "url": "{{ route('showReturnsListItemsForAccept') }}",
                    "type": "get",
                    "data": {returns_id:returns_id},
                    "dataSrc": function (data) {
                        if (data['allowToAccept']) {
                            $('#returns_accept_btn').show();
                        } else {
                            $('#returns_accept_btn').show();
                        }

                        if (!data['hasPerm']) {
                            $('#delete_returns').hide()
                        }
                        if (data['requestData'].state == 1){
                            $('#returnsAccepted').text('№ '+returns_id).css('color', 'blue');
                        } else {
                            $('#returnsAccepted').text('№ '+returns_id).css('color', 'red');
                        }
                        $(table.column(4).footer()).text(' итог: '+data['total']);
                        return data.data;
                    }
                },
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "_TOTAL_ записей",
                    "infoEmpty": "нет записей",
                    "infoFiltered": "(_MAX_)",
                    "search": "<i class='fa fa-search' style='float: left'></i>"
                },
                columns:[
                    {'data':'articula_new', 'width':'10%'},
                    {'data':'articula_old', 'width':'10%'},
                    {'data':'item_name', 'width':'30%'},
                    {'data':'sap_code', 'width':'10%'},
                    {'data':'amount', 'width':'6%'},
                    {'data':'remaining', 'width':'10%'},
                    {'data':'unit', 'width':'6%'}
                ],
                paging: false,
                bPaginate: false,
                dom: 'Blfrtip',
                buttons: [
                    // {
                    //     text: '<i class="glyphicon glyphicon-print" style="font-size: 18px"></i>',
                    //     action: function ( e, dt, node, config ) {
                    //         window.open(document.getElementById('export_to_pdf').href, '_blank');
                    //     }
                    //
                    // },
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5]
                        },
                        text: '<i class="fa fa-file-excel-o"></i>'

                    }
                ],
                scrollY: "50vh",
                scrollCollapse: true,
                drawCallback: function () {
                    $('input[type=number]').on('focus', function (e) {
                        $(this).on('mousewheel.disableScroll', function (e) {
                            e.preventDefault()
                        })
                    })
                    $('input[type=number]').on('blur', function (e) {
                        $(this).off('mousewheel.disableScroll')
                    })

                    $('input[type=number]').on('keydown', function (e) {
                        if(!((e.keyCode > 95 && e.keyCode < 106)
                            || (e.keyCode > 47 && e.keyCode < 58)
                            || e.keyCode == 8 || e.keyCode == 190 || e.keyCode ==37 || e.keyCode == 39 || e.keyCode == 110 || e.keyCode == 86 || e.keyCode == 17)) {
                            return false;
                        }
                    })
                }
            });
        }

        function accept_returns() {
            $('#returns_accept_btn').attr('disabled', true).hide();
            if (confirm('сохранить?')) {
                $('#returns_accept_btn').attr('disabled', true).hide();
                var returns_id = $('#returns_id_for_accept').val();
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type: 'post',
                    url: "{{route('accept_returns_items')}}",
                    data: {returns_id:returns_id},
                    success: function (data) {
                        $('#returns_accept_btn').attr('disabled', false).show();
                        $('#close_returns_detail_modal').click();
                        fetch_returns_detail();
                        fetch_returns_table();
                        getNotAcceptedRequestsAmount();
                        $('.alert-info').show();
                        $('.alert-info ul').empty().append('<li>принято</li>');
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        console.log(json)
                        $('.alert-danger').show();
                        $('.alert-danger ul').empty().append('<li>'+json.data+'</li>');
                    }
                })
            }
        }
        //    moving request moving request
        function fetch_moving_request_table() {
            var storage_id = $('#storage_id').val();
            $("#moving_request_table").DataTable().destroy();
            var table = $('#moving_request_table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    "url": "{{ route('getMovingRequestsForAccept') }}",
                    "type": "post",
                    "data":{storage_id:storage_id},
                    "dataSrc": function (data) {
                        $('#movingRequestsCount').text(data.recordsTotal);
                        return data.data;
                    }
                },
                columns: [
                    { "width": "4%", "data": "id" },
                    { "width": "10%", "data": "applier" },
                    { "width": "8%", "data": "create_time" },
                    { "width": "15%", "data": "from_storage" },
                    { "width": "15%", "data": "description" },
                ],
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr('data-id', data.id);
                },
                stateSave:true,
                order: [[ 2, "desc" ]],
                pageLength: 100,
                lengthMenu:[[100, 500, 1000], [100, 500, 1000]],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение от _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "",
                    "infoFiltered": "(отфильтровано из _MAX_ записей)",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                drawCallback: function (data) {
                    this.api().columns([1,3]).every( function () {
                        var column = this;
                        var select = $('<select><option value="">--все--</option></select>')
                            .appendTo( $(column.footer()).empty() )
                            .on( 'change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val, true, false )
                                    .draw();
                            } );
                        if (data.json.filteredData.length != 0) {
                            for (var i=0; i<data.json.filteredData[column[0]].length; i++){
                                if (typeof data.json.filters[column[0]] !== "undefined") {
                                    select.append( '<option value="'+data.json.filteredData[column[0]][i]+'" selected>'+data.json.filteredData[column[0]][i]+'</option>' )
                                }else {
                                    select.append( '<option value="'+data.json.filteredData[column[0]][i]+'">'+data.json.filteredData[column[0]][i]+'</option>')
                                }
                            }
                        }

                    } );
                },
                scrollY: "50vh",
                scrollCollapse: true
            });
        }

        function show_moving_request_detail(moving_request_id) {
            $('.alert-info').hide();
            $('.alert-danger').hide();
            $('#loading').show();
            $('input[name=moving_request_id_for_accept]').val(moving_request_id);
            $('#show_moving_request_detail').on('shown.bs.modal', function() {
                fetch_moving_request_detail_table();
                $('#loading').hide();
            });
        }

        function fetch_moving_request_detail_table() {
            var moving_request_id = $('#moving_request_id_for_accept').val();
            $('#moving_request_detail_table').DataTable().destroy();
            var table = $('#moving_request_detail_table').DataTable({
                ajax: {
                    "url": "{{ route('showMovingRequestListItemsForAccept') }}",
                    "type": "get",
                    "data": {moving_request_id:moving_request_id},
                    "dataSrc": function (data) {
                        if (data['allowToAccept']) {
                            $('#accept_moving_request_btn').show();
                        } else {
                            $('#accept_moving_request_btn').hide();
                        }
                        if (data['requestData'].state == 1){
                            $('#accepted').text('№'+moving_request_id).css('color', 'blue');
                        } else {
                            $('#accepted').text('№'+moving_request_id).css('color', 'red');
                        }
                        $(table.column(4).footer()).text(' итог: '+data['total']);
                        return data.data;
                    }
                },
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "_TOTAL_ записей",
                    "infoEmpty": "нет записей",
                    "infoFiltered": "(_MAX_)",
                    "search": "<i class='fa fa-search' style='float: left'></i>"
                },
                columns:[
                    {'data':'articula_new', 'width':'10%'},
                    {'data':'articula_old', 'width':'10%'},
                    {'data':'item_name', 'width':'30%'},
                    {'data':'sap_code', 'width':'10%'},
                    {'data':'amount', 'width':'6%'},
                    {'data': 'input_edit', 'width': '6%'},
                    {'data':'remaining', 'width':'6%'},
                    {'data':'unit', 'width':'4%'}
                ],
                paging: false,
                bPaginate: false,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 6, 7]
                        },
                        text: '<i class="fa fa-file-excel-o"></i>'

                    },
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 6, 7]
                        },
                        text: '<i class="fa fa-file-excel-o"></i>'

                    }
                ],
                scrollY: "50vh",

            });
        }

        function accept_moving_request() {
            $('#accept_moving_request_btn').attr('disabled', true).hide();
            if (confirm('принять')) {
                $('#accept_moving_request_btn').attr('disabled', true).hide();
                var moving_request_id = $('#moving_request_id_for_accept').val();
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type: 'post',
                    url: "{{route('acceptMovingRequestItems')}}",
                    data: {moving_request_id:moving_request_id},
                    success: function (data) {
                        $('#accept_moving_request_btn').attr('disabled', false).show();
                        $('#close_detail_modal').click();
                        fetch_moving_request_detail_table();
                        fetch_moving_request_table();
                        getNotAcceptedRequestsAmount();
                        $('.alert-info').show();
                        $('.alert-info ul').empty().append('<li>принято</li>');
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        console.log(json)
                        $('.alert-danger').show();
                        $('.alert-danger ul').empty().append('<li>'+json.data+'</li>');
                    }
                })
            }
        }
    </script>
@endsection

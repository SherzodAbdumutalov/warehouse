<div class="container">

    <div class="panel panel-primary">

        <div class="panel-heading">
        @php
        if($parentId!=0)
            $item = \DB::select("select *from dream_items where id=$parentId");
            if(!empty($item))
                echo $item[0]->item_name;
        @endphp
        </div>

        <div class="panel-body">

            {!! Form::open(array('route' => 'upload.file','method'=>'POST','files'=>'true')) !!}

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12">

                    <div class="form-group">

                        {!! Form::label('','',['class'=>'col-md-3']) !!}

                        <div class="col-md-7">

                            {!! Form::file('sample_file', array('class' => 'form-control')) !!}

                            {!! $errors->first('sample_file', '<p class="alert alert-danger">:message</p>') !!}

                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2 text-left">

                            {!! Form::submit('загрузить',['class'=>'btn btn-primary' , 'disabled'=>'true']) !!}

                        </div>
                    </div>

                </div>
                <br/>

            </div>

            {!! Form::close() !!}

        </div>
        <?php

        function isChecked($name){

            if(session()->has($name)){

                if(session()->get($name)=="on"){

                    return true;

                }

            }

            return false;
        }

        ?>

        <div class="panel-body ">
            <div class="col-md-6">
                <div class="col-md-6" style="height: 25px; width: 100px; background-color: yellow"></div>
                <div class="col-md-6"><p style="display: block; float: left">пустая ячейка</p></div>
            </div>
            <div class="col-md-6">
                <div class="col-md-6" style="height: 25px; width: 100px; background-color: red"></div>
                <div class="col-md-6"><p style="display: block; float: left">дупликат в базе</p></div>
            </div>
            <div class="col-md-6">
                <div class="col-md-6" style="height: 25px; width: 100px; background-color: green"></div>
                <div class="col-md-6"><p style="display: block; float: left">дубликат в файле</p></div>
            </div>
            <div class="col-md-6">
                <div class="col-md-4" style="height: 25px; width: 100px; background-color: blue"></div>
                <div class="col-md-8"><p style="display: block; float: left">ошибка в формате</p></div>
            </div>

            <div class="col-md-2">
                <br>
                <span style="font-weight: bold; font-size: 18px">шаблон *.xls:</span><a href="{{ asset('assets/template/parent.xlsx') }}"> <i style="font-size: 18px; font-weight: bold" class="glyphicon glyphicon-download"></i> </a>
            </div>
        </div>

        @php
            $data = \Session::get('import_parent_item');
            $units = \Session::get('import_parent_item_units');
            $allData = \Session::get('allItem');
        @endphp
        <div class="panel-body">
            <button type="submit" style="display: block; float: left; margin-right: 5px" class="btn btn-info" onclick="validateExcelForm()" id="validateSubmit" {{ (empty($data))?'disabled':'' }}>проверить</button>
            <form action="{{ route('import.file') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <button type="submit" style="display: block;" class="btn btn-success" id="import" disabled>импортировать</button>
            </form>
            <label for="allow_import">всё равно импортировать</label>
            <input type="checkbox" name="allow_import" value="1" id="allow_import" onchange="validateExcelForm()" disabled>
            <form action="{{ route('set_required') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <table class="table table-bordered" id="excelTable">

                    <thead>
                        <tr>
                            <th>новый артикул
                                <input type="checkbox" class="chk" value="articula_new"  checked disabled/>
                            </th>
                            <th>старый артикул
                                <input type="checkbox" class="chk" value="articula_old" id="articula_old"  disabled/>
                            </th>
                            <th>SAP код
                                <input type="checkbox" class="chk" value="sap_code" id="sap_code"  disabled/>
                            </th>
                            <th>наименование</th>
                            <th>ед. измерения</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(\Session::has('import_parent_item'))
                    <script>

                        $('#articula_old').attr('disabled',false);
                        $('#sap_code').attr('disabled',false);

                    </script>

                    @foreach($data as $key=>$item)
                        <tr data-toggle="modal"  data-id="{{ $key }}" data-target="#orderModal">
                            <td id="{{ $key }}articula_new">{{ $item['articula_new'] }}</td>
                            <td id="{{ $key }}articula_old">{{ $item['articula_old'] }}</td>
                            <td id="{{ $key }}sap_code">{{ $item['sap_code'] }}</td>
                            <td>{{ $item['item_name'] }}</td>
                            <td id="{{ $key }}unit">{{ $item['unit'] }}</td>
                        </tr>
                    @endforeach

                    @else
                    @php
                    $data = '';
                    $units = '';
                    $checkboxes = '';
                    $allData = '';
                    @endphp
                    @endif
                    </tbody>

                </table>

            </form>
        </div>

    </div>

</div>

<script>

    function validateExcelForm() {

        var excelData = {!! json_encode($data) !!};
        var allData = {!! json_encode($allData) !!};
        var units = {!! json_encode($units) !!};
        var checkboxes = getCheckboxValues();

        $("table td").css("background-color", "transparent")

        console.log(excelData)
        var isValid = true;
        var isValid_articula = true;

        var x = document.getElementById("excelTable").getElementsByTagName("td")

        for(var c=0; c<checkboxes.length; c++){

            for(var ex=0; ex<excelData.length; ex++){

                var tableCell = document.getElementById(ex+checkboxes[c])
                var tableCellUnit = document.getElementById(ex+"unit")
                var arrayData = []
                var arrayExcelData = []
                var count = 0


                if(!checkForEmpty(excelData[ex][checkboxes[c]])){
                    isValid = false;
                    tableCell.style.backgroundColor = "yellow"
                }

                if(!unitExists(excelData[ex]['unit'], units)){
                    isValid = false;
                    tableCellUnit.style.backgroundColor = "blue"
                }

                for(var j=0; j<allData.length; j++){
                    arrayData.push(allData[j][checkboxes[c]])
                }
                for(var j=0; j<excelData.length; j++){
                    arrayExcelData.push(excelData[j][checkboxes[c]])
                }

                for (var j=0; j<arrayExcelData.length; j++){
                    if (excelData[ex][checkboxes[c]]==arrayExcelData[j]){
                        count+=1
                    }
                }
                console.log(count)
                if (arrayData.includes(excelData[ex][checkboxes[c]]+'') && count>1){
                    isValid = false;
                    tableCell.style.backgroundColor = "black"
                }

                else if (arrayData.includes(excelData[ex][checkboxes[c]]+'')){
                    document.getElementById("allow_import").disabled = false
                    isValid_articula = false;
                    tableCell.style.backgroundColor = "red"
                }

                else if (count>1){
                    isValid = false;
                    tableCell.style.backgroundColor = "green"
                }
            }
        }

        if (document.getElementById("allow_import").checked) {
            isValid_articula=true
        }
        console.log(isValid)
        if (isValid_articula){
            if(isValid){
                var btn = document.getElementById("import")
                btn.disabled = "";
            }
        } else{
            if(isValid){
                var btn = document.getElementById("import")
                btn.disabled = "true";
            }
        }

    }

    function getCheckboxValues(){
        var chkArray = [];

        $(".chk:checked").each(function() {
            chkArray.push($(this).val());
        });

        return chkArray
    }



    function checkForEmpty(excelData){

        if(excelData===null){

            return false;

        }else{

            return true;

        }

    }

    function unitExists(excelUnit, units){

        if (excelUnit!=null) {
            var parsedUnits = excelUnit.split("/")
            for (var i = 0; i < parsedUnits.length; i++) {
                if (!units.includes(parsedUnits[i])) {

                    return false;

                }
            }

            return true;
        }else{
            return true;
        }
    }

</script>
<script>
var excelData = {!! json_encode($data) !!};
$(document).ready(
    function(){
        $('input:file').change(
            function(){
                if ($(this).val()) {
                    $('input:submit').attr('disabled',false);
                }
            }
        );
    });
</script>
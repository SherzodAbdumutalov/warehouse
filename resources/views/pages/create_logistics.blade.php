@section('additional_css')
    <style>
        input[type=checkbox]
        {
            /* Double-sized Checkboxes */
            -ms-transform: scale(2); /* IE */
            -moz-transform: scale(2); /* FF */
            -webkit-transform: scale(2); /* Safari and Chrome */
            -o-transform: scale(2); /* Opera */
            padding: 10px;
        }

    </style>
@endsection
<div class="panel panel-primary">

    <div class="panel-heading" style="padding: 1px 5px">
        создание логистики
    </div>

    <div class="panel-body">
        <div class="alert alert-info" style="display:none">
            <ul>

            </ul>
        </div>
        <div class="col-md-2" style="padding-left: 0">
            <span>С поставщиков/база</span>
            <select name="fromStorage" data-placeholder="выбрать" id="fromStorage" class="standardSelect">
                @foreach($storages as $storage)
                    <option value="{{ $storage->id }}">{{ $storage->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-2" style="padding-left: 0">
            <span>В склад/базу</span>
            <select name="toStorage" data-placeholder="" id="toStorage" class="standardSelect">
                @foreach($storages1 as $storage)
                    <option value="{{ $storage->id }}">{{ $storage->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-2">
            <span>invoice заказа (Фильтр)</span>
            <select name="invoice_numbers" id="invoice_numbers" class="standardSelect" data-placeholder="выбрать" multiple>
                @foreach($orders as $order)
                    <option value="{{ $order->invoice_number }}">{{ $order->invoice_number }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-2">
            <span>Logistics price</span>
            <input name="logistics_price" id="logistics_price" type="number" style="resize: none" class="form-control" placeholder="Logistics price">
        </div>
        <div class="col-md-2">
            <span>Price unit</span>
            <select name="price_unit" id="price_unit" class="standardSelect" data-placeholder="выбрать">
                @foreach($currencies as $currency)
                    <option value="{{ $currency->id }}">{{ $currency->name }}</option>
                @endforeach
            </select>
        </div>
        <table id="orders_items_table" class="display table table-hover table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th style="padding-left: 0">id</th>
                <th style="padding-left: 0">артикул</th>
                <th style="padding-left: 0">SAP код</th>
                <th style="padding-left: 0">наименование</th>
                <th style="padding-left: 0">item name</th>
                <th style="padding-left: 0">invoice</th>
                <th style="padding-left: 0">остаток</th>
                <th style="padding-left: 0">кол-во &nbsp;&nbsp;&nbsp;<input type="checkbox" name="checkAllCheckbox" id="checkAllCheckbox"></th>
                <th style="padding-left: 0">ед. изм.</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
            <tr>
                <th style="padding-left: 0">id</th>
                <th style="padding-left: 0">артикул</th>
                <th style="padding-left: 0">SAP код</th>
                <th style="padding-left: 0">наименование</th>
                <th style="padding-left: 0">item name</th>
                <th style="padding-left: 0">invoice</th>
                <th style="padding-left: 0">остаток</th>
                <th style="padding-left: 0"></th>
                <th style="padding-left: 0">ед. изм.</th>
            </tr>
            </tfoot>
        </table>
        <div class="col-md-4" style="padding-left: 0">
            <span>вводите примечание... (макс. 50 символов)</span>
            <input name="description" id="description" style="resize: none" maxlength="50" class="form-control" placeholder="вводите примечание... (макс. 50 символов)">
        </div>
        <div class="col-md-2">
            <span>invoice логистики</span>
            <input type="text" name="invoice_number" id="invoice_number" class="form-control" placeholder="invoice логистики" maxlength="30" style="height: 30px">
        </div>
        <div class="col-md-2">
            <span>дата отгрузки</span>
            <input type="date" name="delivery_time" value="{{ \Illuminate\Support\Carbon::now()->format('Y-m-d') }}" id="delivery_time" class="form-control" placeholder="время доставки в днях" style="height: 30px" autocomplete="off">
        </div>
        <div class="col-md-2">
            <span>№ транспорта</span>
            <input type="text" name="container_info" id="container_info" class="form-control" placeholder="№ фура/контейнер" maxlength="15" style="height: 30px">
        </div>
        <div class="col-md-2" style="padding-top: 20px">
            <button class="btn btn-warning" type="submit" style="font-size: 12px; float: left; margin-right: 5px" onclick="save_logistics_items()" id="save_order_btn"><i class="glyphicon glyphicon-floppy-save" ></i> сохранить</button>
            <button class="btn btn-danger" type="submit" style="font-size: 12px; float: left" onclick="clear_sessionStorage()"><i class="glyphicon glyphicon-remove-circle"></i> очистить</button>

        </div>

    </div>
</div>
@section('additionalLibrary')
    {{--dataTables library--}}
    <script>
        $(document).ready(function() {
            var url_text = window.location.href;
            var url = new URL(url_text);
            var supplier_id = url.searchParams.get('supplier_id');
            var invoice_number = url.searchParams.get('invoice_number');
            console.log(invoice_number);
            console.log(invoice_number);

            if (supplier_id!=null) {
                $("#fromStorage").val(supplier_id);
                sessionStorage.removeItem('logistics_list');
                sessionStorage.setItem('last_fromStorage', JSON.stringify($("#fromStorage").val()));
            }
            if (invoice_number!=null) {
                $("#invoice_numbers").val(invoice_number);
                sessionStorage.removeItem('logistics_list');
                sessionStorage.setItem('last_invoice_numbers', JSON.stringify($("#invoice_numbers").val()));
            }
            // sessionStorage.removeItem('logistics_list')
            console.log(sessionStorage.getItem('logistics_list'))
            set_last_attributes();
            setTimeout(function () {
                fetchData();
            }, 500)
        } );
    </script>
    <script>
        $( "#fromStorage" ).change(function () {
            $('#checkAllCheckbox').prop('checked', false);
            sessionStorage.removeItem('logistics_list');
            sessionStorage.setItem('last_fromStorage', JSON.stringify($("#fromStorage").val()));
            $('.alert-info').hide();
            if ($("#fromStorage").val() == null) {
                sessionStorage.removeItem('last_fromStorage');
            }
            fetchData();
        });

        $( "#toStorage" ).change(function () {
            $('#checkAllCheckbox').prop('checked', false);
            sessionStorage.removeItem('logistics_list');
            sessionStorage.setItem('last_toStorage', JSON.stringify($("#toStorage").val()));
            $('.alert-info').hide();
            if ($("#toStorage").val() == null) {
                sessionStorage.removeItem('last_toStorage');
            }
            fetchData();
        });

        $( "#invoice_numbers" ).change(function () {
            $('#checkAllCheckbox').prop('checked', false);
            sessionStorage.removeItem('logistics_list');
            console.log($("#invoice_numbers").val());
            sessionStorage.setItem('last_invoice_numbers', JSON.stringify($("#invoice_numbers").val()));
            $('.alert-info').hide();
            if ($("#invoice_numbers").val() == null) {
                sessionStorage.removeItem('last_invoice_numbers');
            }
            fetchData();
        });

        function save_value_in_session(e) {
            $($('#orders_items_table').DataTable().column(7).footer()).text(' итог: 0');
            var id = e.id;
            var input_value = e.value;
            var old = {};
            if (sessionStorage.getItem('logistics_list')!=null) {
                old = JSON.parse(sessionStorage.getItem('logistics_list'));
            }
            if (input_value>0) {
                old[id] = input_value;
            }else{
                delete old[id];
                $('#checkbox'+id).prop('checked', false);
            }
            if (e.max == input_value) {
                $('#checkbox'+id).prop('checked', true);
            }
            console.log(old)
            sessionStorage.setItem('logistics_list', JSON.stringify(old));

            var sum = 0;
            if (old!='{}') {
                console.log(old)
                $.each(old, function (index, value) {
                    console.log(value)
                    sum+=parseFloat(value);
                    $($('#orders_items_table').DataTable().column(7).footer()).text(' итог: '+sum);
                });
            }
        }

        function fillInput(id, value) {
            $($('#orders_items_table').DataTable().column(7).footer()).text(' итог: 0');
            if ($('#checkbox'+id).prop("checked")) {
                $('#'+id).val(value)

                var old = {};
                if (sessionStorage.getItem('logistics_list')!=null) {
                    old = JSON.parse(sessionStorage.getItem('logistics_list'));
                }
                console.log(old)
                old[id] = value;
                console.log(old)
                sessionStorage.setItem('logistics_list', JSON.stringify(old));
            }else{
                $('#'+id).val('')

                var old = {};
                if (sessionStorage.getItem('logistics_list')!=null) {
                    old = JSON.parse(sessionStorage.getItem('logistics_list'));
                }
                delete old[id];
                console.log(old)
                sessionStorage.setItem('logistics_list', JSON.stringify(old));
            }
            var sum = 0;
            if (old!='{}') {
                console.log(old)
                $.each(old, function (index, value) {
                    console.log(value)
                    sum+=parseFloat(value);
                    $($('#orders_items_table').DataTable().column(7).footer()).text(' итог: '+sum);
                });
            }

        }

        $('#checkAllCheckbox').on('click', function () {
            sessionStorage.removeItem('logistics_list');
            fetchData();
        });

        function set_last_attributes(){
            if(sessionStorage.getItem('last_invoice_numbers')!=null){
                $('#invoice_numbers').val(JSON.parse(sessionStorage.getItem('last_invoice_numbers')));
            }
            if(sessionStorage.getItem('last_fromStorage')!=null){
                console.log(sessionStorage.getItem('last_fromStorage'))
                if (sessionStorage.getItem('last_fromStorage')!='null') {
                    $('#fromStorage').val(JSON.parse(sessionStorage.getItem('last_fromStorage')));
                }
            }
            if(sessionStorage.getItem('last_toStorage')!=null){
                $('#toStorage').val(JSON.parse(sessionStorage.getItem('last_toStorage')));
            }
        }

        function fetchData() {
            var invoice_numbers = $('#invoice_numbers').val();
            var fromStorage = $('#fromStorage').val();
            var toStorage = $('#toStorage').val();
            var hasError = false;
            $('#orders_items_table').DataTable().destroy();
            $('#orders_items_table').DataTable( {
                processing: true,
                serverSide: true,
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'post',
                    url: '{{ route('getItemsOfBase') }}',
                    data: {invoice_numbers:invoice_numbers, fromStorage:fromStorage, toStorage:toStorage},
                    "dataSrc": function (data) {
                        return data.data;
                    }
                },
                columnDefs:[
                    {orderable:false, targets:[7]}
                ],
                stateSave: true,
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение от _START_ до _END_ из _TOTAL_ деталей",
                    "infoEmpty": "",
                    "infoFiltered": "(отфильтровано из _MAX_ заказов)",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                columns: [
                    { "width": "6%", "data":"order_list_id" },
                    { "width": "6%", "data":"articula_new" },
                    { "width": "6%", "data":"sap_code" },
                    { "width": "25%", "data":"item_name" },
                    { "width": "25%", "data":"english_name" },
                    { "width": "6%", "data":"invoice_number" },
                    { "width": "4%", "data":"remaining" },
                    { "width": "8%", "data":"input_field" },
                    { "width": "4%", "data":"unit_name" },
                ],
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr('id', dataIndex);
                },
                pageLength: 100,
                lengthMenu:[[100, 500, 1000], [100, 500, 1000]],
                drawCallback: function ( settings ) {
                    $($('#orders_items_table').DataTable().column(7).footer()).text(' итог: 0');

                    $('input[type=number]').on('focus', function (e) {
                        $(this).on('mousewheel.disableScroll', function (e) {
                            e.preventDefault()
                        })
                    });
                    $('input[type=number]').on('blur', function (e) {
                        $(this).off('mousewheel.disableScroll')
                    });
                    $('input[type=number]').on('keydown', function (e) {
                        if(!((e.keyCode > 95 && e.keyCode < 106)
                            || (e.keyCode > 47 && e.keyCode < 58)
                            || e.keyCode == 8 || e.keyCode == 190 || e.keyCode ==37 || e.keyCode == 39 || e.keyCode == 110 || e.keyCode == 86 || e.keyCode == 17)) {
                            return false;
                        }
                    });
                    var x = [];
                    if (sessionStorage.getItem('logistics_list')!=null) {
                        x = JSON.parse(sessionStorage.getItem('logistics_list'));
                    }
                    console.log(x)
                    $.each(settings.json.data, function (index, value) {
                        if ($('#checkAllCheckbox').prop('checked')==true) {
                            $('#checkbox'+value.order_list_id).prop('checked', true);
                            $('#'+value.order_list_id).val(value.remaining);

                            var old = {};
                            if (sessionStorage.getItem('logistics_list')!=null) {
                                old = JSON.parse(sessionStorage.getItem('logistics_list'));
                            }
                            console.log(old)
                            old[value.order_list_id] = value.remaining;
                            console.log(old)
                            sessionStorage.setItem('logistics_list', JSON.stringify(old));
                        }else{
                            if (value.order_list_id in x) {
                                if (x[value.order_list_id]>=value.remaining) {
                                    $('#checkbox'+value.order_list_id).prop('checked', true);
                                }else{
                                    $('#checkbox'+value.order_list_id).prop('checked', false);
                                }
                                $('#'+value.order_list_id).val(x[value.order_list_id]);
                            }
                        }
                    });

                    var x1 = [];
                    if (sessionStorage.getItem('logistics_list')!=null) {
                        x1 = JSON.parse(sessionStorage.getItem('logistics_list'));
                    }
                    var sum = 0;
                    if (x1.length!=0) {
                        console.log(x1)
                        $.each(x1, function (index, value) {
                            console.log(value)
                            sum+=parseFloat(value);
                            $($('#orders_items_table').DataTable().column(7).footer()).text(' итог: '+sum);
                        });
                    }
                },
                scrollY: "50vh",
                scrollCollapse: true,
                initComplete: function (settings, json) {
                    if (hasError) {
                        $('#save_order_btn').attr('disabled', true);
                    }else{
                        $('#save_order_btn').attr('disabled', false);
                    }
                }
            });
        }

        function isEmpty(obj) {
            for(var key in obj) {
                if(obj.hasOwnProperty(key))
                    return false;
            }
            return true;
        }

        function save_logistics_items() {
            if (confirm('сохранить?')) {
                var log_list = sessionStorage.getItem('logistics_list');
                sessionStorage.removeItem('logistics_list');
                sessionStorage.removeItem('last_fromStorage');
                var desc = $('#description').val();
                var container_info = $('#container_info').val();
                var delivery_time = $('#delivery_time').val();
                var invoice_number = $('#invoice_number').val();
                var toStorage = $('#toStorage').val();
                console.log(toStorage)
                var fromStorage = $('#fromStorage').val();
                var logistics_price = $('#logistics_price').val();
                var price_unit = $('#price_unit').val();

                console.log(log_list);
                if (log_list!=null || !isEmpty(log_list)) {
                    $('#save_order_btn').hide();

                    $.ajax({
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        type:'post',
                        url:"{{route('saveLogistics')}}",
                        data:{description:desc, logistics_price: logistics_price, price_unit: price_unit, invoice_number:invoice_number, container_info:container_info, delivery_time:delivery_time, toStorage:toStorage, fromStorage:fromStorage, logistics_list:log_list},
                        success: function (data) {
                            $('#checkAllCheckbox').prop('checked', false);
                            $('#save_order_btn').show();
                            $('#description').val('');
                            $('#delivery_time').val('');
                            $('#invoice_number').val('');
                            $('#container_info').val('');
                            $('.alert-info').show();
                            $('.alert-info ul').empty().append('<li>сохранено</li>');
                            fetchData();
                        },
                        error: function (request, status, error) {
                            var json = $.parseJSON(request.responseText);
                            console.log(json)
                            $('.alert-info ul').empty();
                            $('.alert-info').show();
                            $.each(json.errors, function (index, value) {
                                $('.alert-info ul').append('<li>'+value+'</li>');
                            })
                        }
                    })
                }else{
                    $('.alert-info').show();
                    $('.alert-info ul').empty().append('<li>пустое значение</li>');
                }
            }else{
                $('#save_order_btn').show();
            }
        }

        function clear_sessionStorage() {
            $('#checkAllCheckbox').prop('checked', false);
            $('#invoice_number').val('');
            $('#container_info').val('');
            sessionStorage.removeItem('logistics_list');
            sessionStorage.removeItem('last_fromStorage');
            sessionStorage.removeItem('last_toStorage');
            fetchData();
            $('.alert-info').hide();
        }

    </script>
@endsection

@section('additional_css')
    <style>
        .table tbody tr{
            cursor: pointer;
        }
    </style>
@endsection
<div class="panel panel-primary">

    <div class="panel-heading" style="padding: 1px 5px">
        Дневной план производства
    </div>
    <div class="panel-body">
        {{--<label for="plan_date">выберите дату <i class="fa fa-calendar"></i></label>--}}
        <div class="col-md-12 clearfix" style="padding: 0; margin-bottom: 5px">
            <div class="col-md-3" style="padding: 0">
                <button id="prevBtn"><span class="fa fa-arrow-left"></span></button>
                <input type="date" id="plan_date" name="plan_date" value="">
                <button id="nextBtn"><span class="fa fa-arrow-right"></span></button>
            </div>
            <div class="col-md-2" style="display: none" id="plansFilters">
                <select name="plan_storage_id" id="plan_storage_id" class="standardSelect">
                    @foreach($plan_storages as $storage)
                        <option value="{{ $storage->id }}">{{ $storage->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-3" style=" display: none" id="statisticsFilters">
                <select name="storage_id" id="storage_id" class="standardSelect" multiple data-placeholder="выберите склад/поставщик">
                    @foreach($storages as $storage)
                        <option value="{{ $storage->id }}" >{{ $storage->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <ul class="nav nav-tabs" id="planTabs">
            <li class="active"><a data-toggle="tab" href="#plans"><i class="fa fa-line-chart"></i> План</a></li>
            <li><a data-toggle="tab" href="#statistics" ><i class="glyphicon glyphicon-stats"></i> Детальный расчет</a></li>
        </ul>

        <div class="tab-content">
            <div id="plans" class="tab-pane active">
                <table id="plan_table" class="nowrap table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>наименования</th>
                            <th>артикул</th>
                            <th>SAP код</th>
                            <th>годный ост.</th>
                            <th>производено</th>
                            <th>план произ.</th>
                            <th>ост. в начале</th>
                            <th>ост. под конец</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                    <tfoot>
                        <tr>
                            <th>наименования</th>
                            <th>артикул</th>
                            <th>SAP код</th>
                            <th>годный ост.</th>
                            <th>производено</th>
                            <th>план произ.</th>
                            <th>ост. в начале</th>
                            <th>ост. под конец</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div id="statistics" class="tab-pane fade">
                <div id="planStatisticsDiv">
                    <table id="plan_statistics_table" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>наименования</th>
                            <th>артикул</th>
                            <th>SAP код</th>
                            <th>ост. начало</th>
                            <th>расход скл.</th>
                            <th>расход по плану</th>
                            <th>расчетный ост.</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                        <tfoot>
                        <tr>
                            <th>наименования</th>
                            <th>артикул</th>
                            <th>SAP код</th>
                            <th>ост. начало</th>
                            <th>расход скл.</th>
                            <th>расход по плану</th>
                            <th>расчетный ост.</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="plan_by_model_modal" role="dialog" >
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" style="font-size: 28px" id="close_modal">&times;</button>
                <h4 class="modal-title" id="itemInfoHeader"></h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-info" style="display: none">
                    <ul></ul>
                </div>
                <div class="panel-default">
                    <input type="hidden" name="itemIdOfShownModal" id="itemIdOfShownModal">
                    <div class="col-md-3">
                        <label for="production_count">производство</label>
                        <input type="number" min="0" max="2147483647" name="production_count" value="" id="production_count" class="form-control" placeholder="0" style="height: 27px" required disabled>
                    </div>
                    <div class="col-md-3">
                        <button type="button" class="btn btn-info" style="margin-top: 30px; font-size: 12px" onclick="save_plan()"><span class="glyphicon glyphicon-floppy-save"></span></button>
                    </div>
                </div>
                <table id="item_norm_table" class="nowrap table table-bordered table-hover" width="100%">
                    <thead>
                    <tr>
                        <th>артикул</th>
                        <th>SAP code</th>
                        <th>наименование</th>
                        <th>норма расход</th>
                        <th>расход по плану</th>
                        <th>ост. под конец</th>
                        <th>ед. изм.</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>артикул</th>
                        <th>SAP code</th>
                        <th>наименование</th>
                        <th>норма расход</th>
                        <th>расход по плану</th>
                        <th>ост. под конец</th>
                        <th>ед. изм.</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>

    </div>
</div>
@section('additionalLibrary')
    <script>
        $(document).ready(function() {
            $('#plansFilters').show();
            var date = new Date();
            var currentDate = date.toISOString().slice(0,10);
            $('#plan_date').val(currentDate);
            fetch_plan_data();
            fetch_plan_statistics_data();
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                $('select[multiple]').multiselect( 'reset' );
                fetch_plan_data();
                fetch_plan_statistics_data();
                var target = $(e.target).attr("href");
                if (target == '#plans') {
                    $('#plansFilters').show();
                    $('#statisticsFilters').hide();
                }else{
                    $('#statisticsFilters').show();
                    $('#plansFilters').hide();
                }
                $('#plan_statistics_table').DataTable().columns.adjust().draw(false);
                $('#plan_table').DataTable().columns.adjust().draw();
            } );

            $('#plan_table').on('dblclick', 'tr', function (e) {
                var id = $(this).data('id');
                $('#itemIdOfShownModal').val(id);
                $('#plan_by_model_modal').modal("show");
            });
        });

        $('#nextBtn').on('click', function () {
            var dateTXT = $('#plan_date').val();
            var x = new Date(dateTXT);
            x.setDate(x.getDate()+1)
            $('#plan_date').val(x.toISOString().slice(0,10));
            fetch_plan_data();
            fetch_plan_statistics_data();
        });

        $('#prevBtn').on('click', function () {
            var dateTXT = $('#plan_date').val();
            var x = new Date(dateTXT);
            x.setDate(x.getDate()-1)
            $('#plan_date').val(x.toISOString().slice(0,10));
            fetch_plan_data();
            fetch_plan_statistics_data();
        });

        $('input[name="plan_date"]').on("change", function () {
            fetch_plan_data();
            fetch_plan_statistics_data();
        });

        $('#storage_id').on("change", function () {
            fetch_plan_statistics_data();
        });
        $('#plan_storage_id').on("change", function () {
            fetch_plan_data();
        });

        function fetch_plan_data() {
            var plan_storage_id = $('#plan_storage_id').val();
            var plan_date = $('input[name="plan_date"]').val();
            $('#plan_table').DataTable().destroy();
            $('#plan_table').DataTable( {
                processing: true,
                serverSide: true,
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "{{ route('get_daily_plan') }}",
                    type: "post",
                    data: { plan_date:plan_date, plan_storage_id:plan_storage_id},
                },
                stateSave: true,
                "displayLength": 100,
                pageLength: 100,
                lengthMenu:[[100, 500, 1000, -1], [100, 500, 1000, "All"]],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение от _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "данные отсутствует",
                    "infoFiltered": "",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                columns: [
                    {data:"item_name"},
                    {data:"articula_new"},
                    {data:"sap_code"},
                    {data:"production_count"},
                    {data:"remaining_at_the_beginning_of_model"},
                    {data:"produced"},
                    {data:"remaining"},
                    {data:"predictedCount"}
                ],
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr('data-id', data.id);
                },
                scrollY: "60vh",
                scrollCollapse: true,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>'
                    },
                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>'
                    },
                ],
            })
        }

        function fetch_plan_statistics_data() {
            var storage_id = $('#storage_id').val();
            var plan_date = $('input[name="plan_date"]').val();
            $('#plan_statistics_table').DataTable().destroy();
            $('#plan_statistics_table').DataTable( {
                processing: true,
                serverSide: true,
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "{{ route('getDailyPlanStatistics') }}",
                    type: "post",
                    data: { plan_date:plan_date, storage_id:storage_id }
                },
                "displayLength": 100,
                pageLength: 100,
                lengthMenu:[[100, 500, 1000, -1], [100, 500, 1000, "All"]],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение от _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "данные отсутствует",
                    "infoFiltered": "",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                columns: [
                    {data:"item_name", width:"40%"},
                    {data:"articula_new"},
                    {data:"sap_code"},
                    {data:"remaining_at_the_beginning"},
                    {data:"outputCount"},
                    {data:"consumption"},
                    {data:"remaining_at_the_end"}
                ],
                scrollY: "58vh",
                scrollCollapse: true,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>'
                    },
                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>'
                    },
                ],
            })
        }

        function show_detail() {
            $('.alert-danger').hide();
            $('.alert-info').hide();
            $('.alert-success').hide();
            $('#loading').show();
            set_value();
            fetch_norm_data();
        }

        function set_value(){
            var Year = $('#plan_date').val();
            var Month = $('#plan_date').val();
            var day = $('#plan_date').val();
            var monthly_plan_id = $('#itemIdOfShownModal').val();
            var month = Year.substring(0,4)*12 + parseInt(Month.substring(5,7));
            var planDay = parseInt(day.substring(8,10));
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: "{{ route('set_daily_plan_values') }}",
                type: "post",
                data: { monthly_plan_id:monthly_plan_id, month:month, planDay:planDay},
                success: function (item) {
                    console.log(item)
                    if (item.daily_data!=null) {
                        $('#production_count').val(item.daily_data.production_count);
                    }
                    $('#production_count').attr('disabled', false);
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    console.log(json)
                    $('.alert-info').show();
                    $('.alert-info ul').empty();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').append('<li>'+value+'</li>');
                    })
                    $('#loading').hide()

                }
            })
        }

        $('#plan_by_model_modal').on('shown.bs.modal', function(e) {
            $('#production_count').val('');
            show_detail();
        });
        function fetch_norm_data() {
            var production_count = $('#production_count').val();
            var Year = $('#plan_date').val();
            var Month = $('#plan_date').val();
            var day = $('#plan_date').val();
            var monthly_plan_id = $('#itemIdOfShownModal').val();
            var month_id = Year.substring(0,4)*12 + parseInt(Month.substring(5,7));
            var planDay = parseInt(day.substring(8,10));
            var plan_date = $('#plan_date').val();

            $('#item_norm_table').DataTable().destroy();
            $('#item_norm_table').DataTable( {
                processing: true,
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    "url": "{{ route('showPlanAddModal') }}",
                    "type": "post",
                    "data":{monthly_plan_id:monthly_plan_id, month_id:month_id, production_count:production_count, planDay:planDay, plan_date:plan_date},
                },
                columns: [
                    { "data": "articula_new"},
                    { "data": "sap_code"},
                    { "data": "item_name"},
                    { "data": "consumption_by_norm"},
                    { "data": "total_consumption_by_plan"},
                    { "data": "remaining_at_the_end"},
                    { "data": "unit"}
                ],
                order: [[1, 'asc']],
                stateSave: true,
                paging: false,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5]
                        },
                        title:'бум лист',
                        text: '<i class="fa fa-file-excel-o"></i>'

                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5]
                        },
                        title:'бум лист',
                        text: '<i class="fa fa-file-pdf-o"></i>'

                    },
                ],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение _START_ для _END_ из _TOTAL_ записей",
                    "infoEmpty": "данные отсутствует",
                    "infoFiltered": "",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                scrollY: "50vh",
                initComplete: function (data) {
                    $('#itemInfoHeader').text($('#plan_date').val()+' | '+data.json.item['articula_new']+' '+data.json.item['item_name']);
                }
            } );
            $('#loading').hide();
        }

        $('#production_count').on("input", function () {
            fetch_norm_data();
        });

        function save_plan() {
            var date = $('#plan_date').val();
            var monthly_plan_id = $('#itemIdOfShownModal').val();
            var day = parseInt(date.substring(8,10));
            var production_count = $('input[name=production_count]').val();
            if (production_count>0){
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "{{ route('save_daily_plan') }}",
                    type: "post",
                    data: { day:day, monthly_plan_id:monthly_plan_id, production_count:production_count},
                    success: function (data) {
                        fetch_plan_data();
                        fetch_plan_statistics_data();
                        $('#close_modal').click();
                        $('#loading').hide();
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        console.log(json)
                        $('.alert-info').show();
                        $('.alert-info ul').empty();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').append('<li>'+value+'</li>');
                        })
                        $('#loading').hide()

                    }
                })
            } else {
                $('.alert-info').show();
                $('.alert-info ul').empty().append('<li>0 и null нельзя</li>')
            }
        }
    </script>
@endsection

@section('additional_css')
    <style>
        .table tbody tr{
            cursor: pointer;
        }
        tr.detail-red-cell td:nth-child(6){
            background-color: red!important;
            margin: 3px;
            color: white!important;
        }
    </style>
@endsection
<div class="panel panel-primary">

    <div class="panel-heading" style="padding: 1px 5px">
        Месячный план производства
    </div>
    <div class="panel-body">
        {{--<label for="plan_date">выберите дату <i class="fa fa-calendar"></i></label>--}}
        <div class="col-md-12 clearfix" style="padding: 0; margin-bottom: 5px">
            <div class="col-md-2" style="padding: 0; width: 250px">
                <button id="prevBtn"><span class="fa fa-arrow-left"></span></button>
                <input type="month" id="plan_date" name="plan_date" value="{{ (session()->has('plan_date'))?session()->get('plan_date'):\Illuminate\Support\Carbon::now()->format('Y-m') }}">
                <button id="nextBtn"><span class="fa fa-arrow-right"></span></button>
            </div>
            <div class="col-md-2" style="display: none" id="plansFilters">
                <select name="plan_storage_id" id="plan_storage_id" class="standardSelect" disabled>
                    @foreach($plan_storages as $storage)
                        <option value="{{ $storage->id }}">{{ $storage->name }}</option>
                    @endforeach
                </select>
            </div>
            <a href="{{ route('show_import_plan_page') }}" class="btn btn-info"><i class="fas fa-file-import"></i> импортировать план</a>
            <div class="col-md-3" style="display: none" id="statisticsFilters">
                <select name="storage_id" id="storage_id" class="standardSelect" multiple data-placeholder="выберите склад/поставщик" disabled>
                    @foreach($storages as $storage)
                        <option value="{{ $storage->id }}" >{{ $storage->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <ul class="nav nav-tabs" id="planTabs">
            <li class="active"><a data-toggle="tab" href="#plans"><i class="fa fa-line-chart"></i> План</a></li>
            <li><a data-toggle="tab" href="#statistics" ><i class="glyphicon glyphicon-stats"></i> Детальный расчет</a></li>
        </ul>

        <div class="tab-content">
            <div id="plans" class="tab-pane active">
                <table id="plan_table" class="nowrap table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>наименования</th>
                            <th>артикул</th>
                            <th>SAP код</th>
                            <th>ост. в начале</th>
                            <th>план произ.</th>
                            <th>продажа</th>
                            <th>ост. под конец</th>
                            <th>годный остаток</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                    <tfoot>
                    <tr>
                        <th>наименования</th>
                        <th>артикул</th>
                        <th>SAP код</th>
                        <th>Итог: </th>
                        <th>Итог: </th>
                        <th>Итог: </th>
                        <th>Итог: </th>
                        <th>Итог: </th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <div id="statistics" class="tab-pane fade">
                <div id="planStatisticsDiv">
                    <table id="plan_statistics_table" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>наименования</th>
                            <th>артикул</th>
                            <th>SAP код</th>
                            <th>ост. в начале</th>
                            <th>заказ</th>
                            <th>приход скл.</th>
                            <th>расход скл.</th>
                            <th>расход по плану</th>
                            <th>годный ост.</th>
                            <th>расчетный ост.</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                        <tfoot>
                        <tr>
                            <th>наименования</th>
                            <th>артикул</th>
                            <th>SAP код</th>
                            <th>ост. в начале</th>
                            <th>заказ</th>
                            <th>приход скл.</th>
                            <th>расход скл.</th>
                            <th>расход по плану</th>
                            <th>годный ост.</th>
                            <th>расчетный ост.</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="plan_by_model_modal" role="dialog" >
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" style="font-size: 28px" id="close_modal">&times;</button>
                <h4 class="modal-title" id="itemInfoHeader"></h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-info" style="display: none">
                    <ul></ul>
                </div>
                <div class="panel-default">
                    <input type="hidden" name="itemIdOfShownModal" id="itemIdOfShownModal">
                    <div class="col-md-3">
                        <label for="production_count">производство</label>
                        <input type="number" min="0" max="2147483647" name="production_count" value="" id="production_count" class="form-control" placeholder="0" style="height: 27px" required disabled>
                    </div>

                    <div class="col-md-3">
                        <label for="sales_count">продажа</label>
                        <input type="number" min="0" max="2147483647" name="sales_count" value="" id="sales_count" class="form-control" placeholder="0" style="height: 27px" required disabled>
                    </div>
                    <div class="col-md-3">
                        <button type="button" class="btn btn-info" style="margin-top: 30px; font-size: 12px" onclick="save_plan()"><span class="glyphicon glyphicon-floppy-save"></span></button>
                    </div>
                </div>
                <table id="item_norm_table" class="nowrap table table-bordered table-hover" width="100%">
                    <thead>
                    <tr>
                        <th>артикул</th>
                        <th>SAP code</th>
                        <th>наименование</th>
                        <th>норма расход</th>
                        <th>расход по плану</th>
                        <th>ост. под конец</th>
                        <th>ед. изм.</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>артикул</th>
                        <th>SAP code</th>
                        <th>наименование</th>
                        <th>норма расход</th>
                        <th>расход по плану</th>
                        <th>ост. под конец</th>
                        <th>ед. изм.</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>

    </div>
</div>
@section('additionalLibrary')
    <script>
        $(document).ready(function() {
            $('#storage_id').attr('disabled', true).trigger('chosen:updated');
            $('#plan_storage_id').attr('disabled', true).trigger('chosen:updated');
            $('#plansFilters').show();
            fetch_plan_data();
            fetch_plan_statistics_data();
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                $('select[multiple]').multiselect( 'reset' );
                fetch_plan_data();
                fetch_plan_statistics_data();
                var target = $(e.target).attr("href");
                if (target == '#plans') {
                    $('#plansFilters').show();
                    $('#statisticsFilters').hide();
                }else{
                    $('#statisticsFilters').show();
                    $('#plansFilters').hide();
                }
                $('#plan_statistics_table').DataTable().columns.adjust().draw(false);
                $('#plan_table').DataTable().columns.adjust().draw();
            } );


            $('#plan_table').on('dblclick', 'tr', function (e) {
                var id = $(this).data('id');
                $('#itemIdOfShownModal').val(id);
                $('#plan_by_model_modal').modal("show");
            });
        });

        $('#nextBtn').on('click', function () {
            var monthTxt = '';
            var dateTXT = $('#plan_date').val();
            var x = new Date(dateTXT);
            var currentMonth = x.getMonth()+1;
            var currentYear = x.getFullYear();
            if (currentMonth<12) {
                currentMonth = currentMonth + 1;
            }else{
                currentMonth = 1;
                currentYear = currentYear + 1;
            }
            if (currentMonth<10) {
                monthTxt = '0'+currentMonth;
            }else{
                monthTxt = currentMonth;
            }
            $('#plan_date').val(currentYear+'-'+monthTxt);
            fetch_plan_data();
            fetch_plan_statistics_data();
        });

        $('#prevBtn').on('click', function () {
            var monthTxt = '';
            var dateTXT = $('#plan_date').val();
            var x = new Date(dateTXT);
            var currentMonth = x.getMonth()+1;
            var currentYear = x.getFullYear();
            if (currentMonth>1) {
                currentMonth = currentMonth - 1;
            }else{
                currentMonth = 12;
                currentYear = currentYear - 1;
            }
            if (currentMonth<10) {
                monthTxt = '0'+currentMonth;
            }else{
                monthTxt = currentMonth;
            }
            $('#plan_date').val(currentYear+'-'+monthTxt);
            fetch_plan_data();
            fetch_plan_statistics_data();
        });

        $('input[name="plan_date"]').on("change", function () {
            fetch_plan_data();
            fetch_plan_statistics_data();
        });

        $('#storage_id').on("change", function () {
            fetch_plan_statistics_data();
        });

        $('#plan_storage_id').on("change", function () {
            fetch_plan_data();
        });

        function fetch_plan_data() {
            var plan_date = $('input[name="plan_date"]').val();
            var plan_storage_id = $('#plan_storage_id').val();
            $('#plan_table').DataTable().destroy();
            var tab = $('#plan_table').DataTable( {
                processing: true,
                serverSide: true,
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "{{ route('get_plan_by_month') }}",
                    type: "post",
                    data: { plan_date:plan_date, plan_storage_id:plan_storage_id},
                    dataSrc: function (data) {
                        $(tab.column(3).footer()).text(' Total: '+data['total_production']);
                        $(tab.column(4).footer()).text(' Total: '+data['total_sale']);
                        $(tab.column(5).footer()).text(' Total: '+data['total_start_remaining']);
                        $(tab.column(7).footer()).text(' Total: '+data['total_end_remaining']);
                        $(tab.column(6).footer()).text(' Total: '+data['total_remaining']);
                        return data.data;
                    },
                },

                stateSave: true,
                "displayLength": 100,
                pageLength: 100,
                lengthMenu:[[100, 500, 1000, -1], [100, 500, 1000, "All"]],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение от _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "данные отсутствует",
                    "infoFiltered": "",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                columns: [
                    {data:"item_name", width: "14%"},
                    {data:"articula_new", width: "12%"},
                    {data:"sap_code", width: "12%"},
                    {data:"production_count", width: "12%"},
                    {data:"sales_count", width: "12%"},
                    {data:"remaining_at_the_beginning", width: "12%"},
                    {data:"remaining", width: "12%"},
                    {data:"predictedCount", width: "14%"}
                ],
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr('data-id', data.id);
                },
                scrollY: "60vh",
                scrollCollapse: true,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>'
                    },
                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>'
                    },
                ],
                initComplete: function () {
                    $('#storage_id').attr('disabled', false).trigger('chosen:updated');
                    $('#plan_storage_id').attr('disabled', false).trigger('chosen:updated');
                }
            })
        }

        function fetch_plan_statistics_data() {
            var storage_id = $('#storage_id').val();
            console.log(storage_id)
            var plan_date = $('input[name="plan_date"]').val();
            $('#plan_statistics_table').DataTable().destroy();
            $('#plan_statistics_table').DataTable( {
                processing: true,
                serverSide: true,
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "{{ route('get_plan_statistics') }}",
                    type: "post",
                    data: { plan_date:plan_date, storage_id:storage_id }
                },
                "displayLength": 100,
                pageLength: 100,
                lengthMenu:[[100, 500, 1000, -1], [100, 500, 1000, "All"]],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение от _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "данные отсутствует",
                    "infoFiltered": "",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                columns: [
                    {data:"item_name", width:"40%"},
                    {data:"articula_new"},
                    {data:"sap_code"},
                    {data:"remaining_at_the_beginning"},
                    {data:"orderCount"},
                    {data:"inputCount"},
                    {data:"outputCount"},
                    {data:"consumption"},
                    {data:"remaining"},
                    {data:"remaining_at_the_end"}
                ],
                scrollY: "58vh",
                scrollCollapse: true,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        text: '<i class="fa fa-file-excel-o"></i>'
                    },
                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>'
                    },
                ],
                initComplete: function () {
                    $('#storage_id').attr('disabled', false).trigger('chosen:updated');
                    $('#plan_storage_id').attr('disabled', false).trigger('chosen:updated');
                }
            })
        }

        function show_detail() {
            $('.alert-danger').hide();
            $('.alert-info').hide();
            $('.alert-success').hide();
            $('#loading').show();
            set_value();
            fetch_norm_data();
        }

        function set_value(){
            var Year = $('#plan_date').val();
            var Month = $('#plan_date').val();
            var item_id = $('#itemIdOfShownModal').val();
            var month_id = Year.substring(0,4)*12 + parseInt(Month.substring(5,7));
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: "{{ route('set_plan_values') }}",
                type: "post",
                data: { item_id:item_id, month_id:month_id},
                success: function (item) {
                    if (item.month_data!=null) {
                        $('#production_count').val(item.month_data.production_count);
                        $('#sales_count').val(item.month_data.sales_count);
                    }
                    $('#production_count').attr('disabled', false);
                    $('#sales_count').attr('disabled', false);
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    console.log(json)
                    $('.alert-info').show();
                    $('.alert-info ul').empty();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').append('<li>'+value+'</li>');
                    })
                    $('#loading').hide()

                }
            })
        }

        $('#plan_by_model_modal').on('shown.bs.modal', function(e) {
            $('#production_count').val('');
            $('#sales_count').val('');
            show_detail();
        });
        function fetch_norm_data() {
            var production_count = $('#production_count').val();
            var plan_date = $('#plan_date').val();
            var Year = $('#plan_date').val();
            var Month = $('#plan_date').val();
            var item_id = $('#itemIdOfShownModal').val();
            var month_id = Year.substring(0,4)*12 + parseInt(Month.substring(5,7));
            $('#item_norm_table').DataTable().destroy();
            $('#item_norm_table').DataTable( {
                processing: true,
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    "url": "{{ route('plan_show_detail') }}",
                    "type": "post",
                    "data":{item_id:item_id, month_id:month_id, production_count:production_count, plan_date:plan_date},
                },

                columns: [
                    { "data": "articula_new"},
                    { "data": "sap_code"},
                    { "data": "item_name"},
                    { "data": "consumption_by_norm"},
                    { "data": "total_consumption_by_plan"},
                    { "data": "remaining_at_the_end"},
                    { "data": "unit"}
                ],
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr('data-id', data.id);
                    if(data["remaining_at_the_end"] <=0){
                        $(row).attr("class", "detail-red-cell")
                    }
                },
                order: [[1, 'asc']],
                stateSave: true,
                paging: false,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5]
                        },
                        title:'бум лист',
                        text: '<i class="fa fa-file-excel-o"></i>'

                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5]
                        },
                        title:'бум лист',
                        text: '<i class="fa fa-file-pdf-o"></i>'

                    },
                ],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение _START_ для _END_ из _TOTAL_ записей",
                    "infoEmpty": "данные отсутствует",
                    "infoFiltered": "",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                scrollY: "50vh",
                initComplete: function (data) {
                    $('#itemInfoHeader').text($('#plan_date').val()+' | '+data.json.item['articula_new']+' '+data.json.item['item_name']);
                }
            } );
            $('#loading').hide();
        }

        $('#production_count').on("input", function () {
            fetch_norm_data();
        });

        function save_plan() {
            var Year = $('#plan_date').val();
            var Month = $('#plan_date').val();
            var item_id = $('#itemIdOfShownModal').val();
            var month_id = Year.substring(0,4)*12 + parseInt(Month.substring(5,7));
            var production_count = $('input[name=production_count]').val();
            var sales_count = $('input[name=sales_count]').val();
            if (production_count>=0){
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "{{ route('save_monthly_plan') }}",
                    type: "post",
                    data: { item_id:item_id, month_id:month_id, production_count:production_count, sales_count:sales_count},
                    success: function (data) {
                        fetch_plan_data();
                        fetch_plan_statistics_data();
                        $('#close_modal').click();
                        $('#loading').hide();
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        console.log(json)
                        $('.alert-info').show();
                        $('.alert-info ul').empty();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').append('<li>'+value+'</li>');
                        })
                        $('#loading').hide()

                    }
                })
            } else {
                $('.alert-info').show();
                $('.alert-info ul').empty().append('<li>0 и null нельзя</li>')
            }
        }
    </script>
@endsection

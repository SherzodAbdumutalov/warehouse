@section('additional_css')
    <style>
        .table tbody tr{
            cursor: pointer;
        }
    </style>
@endsection
<div class="panel panel-primary">

    <div class="panel-heading" style="padding: 1px 5px">
        Месячный план производства
    </div>
    <div class="panel-body">
        <div class="col-md-12 clearfix" style="padding: 0; margin-bottom: 5px">
            <form action="{{ route('upload_plan_file') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="col-md-2" style="padding: 0; width: 250px">
                    <button type="button" id="prevBtn"><span class="fa fa-arrow-left"></span></button>
                    <input type="month" id="plan_date" name="plan_date" value="{{ (session()->get('plan_items.plan_date')==null)?\Illuminate\Support\Carbon::now()->format('Y-m'):session()->get('plan_items.plan_date') }}">
                    <button type="button" id="nextBtn"><span class="fa fa-arrow-right"></span></button>
                </div>
                <div class="col-md-2">
                    <select name="storage_id" id="plan_storage_id" class="standardSelect">
                        @foreach($plan_storages as $storage)
                            <option value="{{ $storage->id }}" {{ (session()->get('plan_items.storage_id')!=null)?(session()->get('plan_items.storage_id')==$storage->id)?'selected':'':'' }}>{{ $storage->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6">
                    <input type="file" class="form-control" name="plan_file" id="plan_file">
                </div>
                <div class="col-md-1">
                    <button type="submit" name="upload_file" class="btn btn-warning">загрузить</button>
                </div>
                <div class="col-md-1">
                    <a href="{{ asset('assets') }}/template/planFile.xlsx" download style="font-size: 18px; "><span class="glyphicon glyphicon-download" style="font-size: 18px">&nbsp;шаблон</span></a>
                </div>
            </form>
        </div>
        <table id="plan_table" class="table table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>артикул</th>
                <th>план произ.</th>
                <th>продажа</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @if(session()->has('plan_items.items'))
                @foreach(session()->get('plan_items.items') as $arr)
                    <tr>
                        <td>{{ $arr['articula_new'] }}</td>
                        <td>{{ $arr['production_count'] }}</td>
                        <td>{{ $arr['sales_count'] }}</td>
                        <td>{{ $arr['storage'] }}</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        <form action="{{ route('import_plan') }}" method="post">
            {{ csrf_field() }}
            @if (\Session::has('plan_btn'))
                @if(!is_null(\Session::get('plan_btn')))
                    {!! \Session::get('plan_btn') !!}
                @endif
            @endif
        </form>
    </div>
</div>
@section('additionalLibrary')
    <script>
        $(document).ready(function() {
            $('#plan_table').DataTable({
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "_TOTAL_ записей",
                    "infoEmpty": "нет записей",
                    "infoFiltered": "(_MAX_)",
                    "search": "<i class='fa fa-search' style='float: left'></i>"
                },
                paging: false,
                bPaginate: false,
                scrollY: "65vh"
            });
        });

        $('#nextBtn').on('click', function () {
            var monthTxt = '';
            var dateTXT = $('#plan_date').val();
            var x = new Date(dateTXT);
            var currentMonth = x.getMonth()+1;
            var currentYear = x.getFullYear();
            if (currentMonth<12) {
                currentMonth = currentMonth + 1;
            }else{
                currentMonth = 1;
                currentYear = currentYear + 1;
            }
            if (currentMonth<10) {
                monthTxt = '0'+currentMonth;
            }else{
                monthTxt = currentMonth;
            }
            $('#plan_date').val(currentYear+'-'+monthTxt);
        });

        $('#prevBtn').on('click', function () {
            var monthTxt = '';
            var dateTXT = $('#plan_date').val();
            var x = new Date(dateTXT);
            var currentMonth = x.getMonth()+1;
            var currentYear = x.getFullYear();
            if (currentMonth>1) {
                currentMonth = currentMonth - 1;
            }else{
                currentMonth = 12;
                currentYear = currentYear - 1;
            }
            if (currentMonth<10) {
                monthTxt = '0'+currentMonth;
            }else{
                monthTxt = currentMonth;
            }
            $('#plan_date').val(currentYear+'-'+monthTxt);
        });

    </script>
@endsection
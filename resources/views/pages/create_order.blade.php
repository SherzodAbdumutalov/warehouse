@section('additional_css')
    <style>

    </style>
@endsection
<div class="panel panel-primary">

    <div class="panel-heading" style="padding: 1px 5px">
        создание заказа
    </div>

    <div class="panel-body">
        <div class="alert alert-info" style="display:none">
            <ul>

            </ul>
        </div>
        <div class="clearfix">
            <div class="col-md-12 clearfix" style="padding-left: 0; margin-bottom: 5px">
                <div class="col-md-4" style="padding-left: 0">
                    <span>выбрать поставщика</span>
                    <select name="supplier_id" data-placeholder="выбрать поставщика" id="supplier_id" class="standardSelect chosen-choices" onchange="get_supplier_items()">
                        <option value="0"></option>
                        @foreach($suppliers as $supplier)
                            <option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                    <span>депозит (%)</span>
                    <input type="number" name="deposit" id="deposit" class="form-control" value="" placeholder="депозит (%)">
                </div>
                <div class="col-md-2">
                    <span>перед отправкой (%)</span>
                    <input type="number" name="before_shipment" id="before_shipment" class="form-control" value="" placeholder="перед отправкой (%)">
                </div>
                <div class="col-md-1">
                    <span>валюта</span>
                    <select name="currency" id="currency" class="standardSelect" data-placeholder="валюта">
                        @foreach($currencies as $currency)
                            <option value="{{ $currency->id }}">{{ $currency->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                    <span>дата оплаты</span>
                    <input type="date" name="due_time" id="due_time" class="form-control" value="" placeholder="дата оплаты" min="">
                </div>
            </div>
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#home">ручное добавление</a></li>
                <li><a data-toggle="tab" href="#menu1">файл импорт</a></li>
            </ul>
            <div class="tab-content" style="padding-top: 6px">
                <div id="home" class="tab-pane fade in active">
                    <div class="col-md-4" style="padding-left: 0">
                        <select name="supplier_model_id" data-placeholder="выбрать готовый продукт" id="supplier_model_id" class="standardSelect">
                        </select>
                    </div>

                    <div class="col-md-1">
                        <input type="number" name="model_amount" min="1" id="model_amount" class="form-control" style="height: 26px; display: block" placeholder="кол-во">
                    </div>
                    <div class="col-md-1">
                        <button class="btn btn-warning" type="button" style="padding: 3px 15px" onclick="get_supplier_items_by_model()"><i class="fa fa-plus"></i> добавить</button>
                    </div>
                </div>
                <div id="menu1" class="tab-pane fade">
                    <div class="col-md-2" style="padding-left: 0">
                        <form method="post" name="import_order_file" id="import_order_file" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="file_supplier_id" id="file_supplier_id">
                            <input type="file" name="orderFile" class="form-control" style="width: 70%; float: left">
                            <button type="submit" class="btn btn-info" style="float: right">импорт <i class="fas fa-file-import"></i></button>
                        </form>
                    </div>
                    <div class="col-md-1">
                        <a href="{{ asset('assets') }}/template/order.xlsx" download style="font-size: 18px; "><span class="glyphicon glyphicon-download" style="font-size: 18px">&nbsp;шаблон</span></a>
                    </div>

                    <div class="col-md-2">
                        <div class="col-md-12" style="background-color: red; height: 20px; "></div>
                        <div class="col-md-12">не соответствующий поставщик</div>
                    </div>
                    <div class="col-md-2">
                        <div class="col-md-12" style="background-color: green; height: 20px; "></div>
                        <div class="col-md-12">дубликат артикул</div>
                    </div>
                    <div class="col-md-2">
                        <div class="col-md-12" style="background-color: blue; height: 20px; "></div>
                        <div class="col-md-12">конфликтующая ед. изм.</div>
                    </div>
                    <div class="col-md-2">
                        <div class="col-md-12" style="background-color: #2a9055; height: 20px; "></div>
                        <div class="col-md-12">несуществующий артикул</div>
                    </div>

                </div>
            </div>

        </div>

        <table id="supplier_items_table" class="display table table-hover table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th style="padding-left: 0">артикул</th>
                <th style="padding-left: 0">SAP код</th>
                <th style="padding-left: 0">наименование</th>
                <th style="padding-left: 0">item name</th>
                <th style="padding-left: 0">кол-во</th>
                <th style="padding-left: 0">ед. изм.</th>
                <th style="padding-left: 0">цена за ед.(без НДС)</th>
                <th style="padding-left: 0">сумма</th>
                <th>Weight</th>
                <th style="padding-left: 0"></th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
            <tr>
                <th style="padding-left: 0">артикул</th>
                <th style="padding-left: 0">SAP код</th>
                <th style="padding-left: 0">наименование</th>
                <th style="padding-left: 0">item name</th>
                <th style="padding-left: 0"></th>
                <th style="padding-left: 0">ед. изм.</th>
                <th style="padding-left: 0">цена за ед.(без НДС)</th>
                <th style="padding-left: 0">сумма</th>
                <th>Weight</th>
                <th style="padding-left: 0"></th>
            </tr>
            </tfoot>
        </table>

        <div class="col-md-3" style="padding-left: 0">
            <span>вводите примечание... (макс. 50 символов)</span>
            <input name="description" id="description" style="resize: none" maxlength="50" class="form-control" placeholder="вводите примечание... (макс. 50 символов)">
        </div>
        <div class="col-md-2">
            <span>invoice №</span>
            <input type="text" name="invoice_number" id="invoice_number" class="form-control" placeholder="invoice №" maxlength="25" style="height: 30px">
        </div>
        <div class="col-md-2">
            <span>дата заказа</span>
            <input type="date" name="shipment_time" id="shipment_time" value="{{ \Illuminate\Support\Carbon::now()->format('Y-m-d') }}" class="form-control" placeholder="время доставки в днях" style="height: 30px" >
        </div>
        <div class="col-md-2" style="margin-top: 20px">
            <button class="btn btn-warning" type="submit" style="font-size: 12px; float: left; margin-right: 5px" onclick="save_items()" id="save_order_btn"><i class="glyphicon glyphicon-floppy-save" ></i> сохранить</button>
            <button class="btn btn-danger" type="submit" style="font-size: 12px; float: left" onclick="clear_sessionStorage()"><i class="glyphicon glyphicon-remove-circle"></i> очистить</button>
        </div>
    </div>
</div>
@section('additionalLibrary')
    {{--dataTables library--}}
    <script>
        $(document).ready(function() {
            var date = new Date();
            if(sessionStorage.getItem('supplier_items')!=null){
                $('#supplier_id').prop('disabled', true).trigger('chosen:updated')
            }else{
                $('#supplier_id').prop('disabled', false).trigger("chosen:updated")
            }
            fetchData();
            set_last_attributes();
            get_supplier_models();
        } );
    </script>
    <script>

        $('#deposit').on('keyup', function () {
            var deposit = $(this).val();
            var before_shipment = $('#before_shipment').val();
            console.log(before_shipment)
            if (deposit=='' || deposit==null) {
                deposit = 0;
            }
            if (before_shipment=='' || before_shipment==null) {
                before_shipment = 0;
            }
            console.log(parseFloat(deposit) + parseFloat(before_shipment))
            if (parseFloat(deposit) + parseFloat(before_shipment) > 100) {
                console.log(parseFloat(deposit) + parseFloat(before_shipment));
                $(this).val(100 - before_shipment);
            }
        });

        $('#before_shipment').on('keyup', function () {
            var before_shipment = $(this).val();
            var deposit = $('#deposit').val();
            console.log(deposit)
            if (parseFloat(deposit) + parseFloat(before_shipment) > 100) {
                console.log(parseFloat(deposit) + parseFloat(before_shipment));
                $(this).val(100 - deposit);
            }
        });

        $( "#supplier_id" ).change(function () {
            $('.alert-info').hide();
            $('#file_supplier_id').val($('#supplier_id').val());
            var items = sessionStorage.getItem('supplier_items');
            console.log(items)
            if (items != null) {
                if (confirm('удалит список?')) {
                    get_supplier_items();
                }
            }else{
                get_supplier_items();
            }
            get_supplier_models();
        });

        function set_last_attributes(){
            if(sessionStorage.getItem('last_supplier_id')!=null){
                $('#supplier_id').val(JSON.parse(sessionStorage.getItem('last_supplier_id'))).trigger("chosen:updated");
                $('#file_supplier_id').val(JSON.parse(sessionStorage.getItem('last_supplier_id')));
            }
            if (sessionStorage.getItem('lastBilling') !== 'null' && sessionStorage.getItem('lastBilling') != null) {
                console.log(sessionStorage.getItem('lastBilling'));
                $('#deposit').val(JSON.parse(sessionStorage.getItem('lastBilling')).deposit);
                $('#before_shipment').val(JSON.parse(sessionStorage.getItem('lastBilling')).before_shipment);
                $('#due_time').val(JSON.parse(sessionStorage.getItem('lastBilling')).due_time);
                $('#currency').val(JSON.parse(sessionStorage.getItem('lastBilling')).currency_id).trigger("chosen:updated");
            }
        }

        function fetchData() {
            var items = [];
            if (sessionStorage.getItem('supplier_items')!=null) {
                items = JSON.parse(sessionStorage.getItem('supplier_items'));
                $('#save_order_btn').show().attr('disabled', false);
            }else{
                $('#save_order_btn').hide().attr('disabled', true);
            }
            console.log(items)
            $('#supplier_items_table').DataTable().destroy();
            $('#supplier_items_table').DataTable( {
                progress:true,
                data : items.data,
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "_TOTAL_ записей",
                    "infoEmpty": "",
                    "infoFiltered": "((<b>отфильтровано из _MAX_ записей</b>))",
                    "search": "<i class='fa fa-search' style='float: left'></i>"
                },
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr('id', dataIndex);
                    $( row ).find('td:eq(7)').text(data.price_per_unit*data.amount);
                    if (data.error!=0) {
                        $('#save_order_btn').hide().attr('disabled', true);
                        if (data.error==1) {
                            $(row).find('td:eq(0)').css('background-color','red').css('color', 'white');
                        }
                        if (data.error==2){
                            $(row).find('td:eq(0)').css('background-color','green').css('color', 'white');
                        }
                        if (data.error==3){
                            $(row).find('td:eq(5)').css('background-color','blue').css('color', 'white');
                        }
                    }
                },
                paging: false,
                columns: [
                    { "data":"articula_new" },
                    { "data":"sap_code" },
                    { "data":"item_name" },
                    { "data":"english_name" },
                    { "data":"amount_input" },
                    { "data":"unit" },
                    { "data":"price_per_unit_input" },
                    { "data":"total_price" },
                    { "data": "weight" },
                    { "data":"delete_btn" },

                ],
                scrollY: "70vh",
                scrollCollapse: true,
                drawCallback: function ( settings ) {
                    $('input[type=number]').on('focus', function (e) {
                        $(this).on('mousewheel.disableScroll', function (e) {
                            e.preventDefault()
                        })
                    });
                    $('input[type=number]').on('blur', function (e) {
                        $(this).off('mousewheel.disableScroll')
                    });
                    $('input[type=number]').on('keydown', function (e) {
                        if(!((e.keyCode > 95 && e.keyCode < 106)
                            || (e.keyCode > 47 && e.keyCode < 58)
                            || e.keyCode == 8 || e.keyCode == 190 || e.keyCode ==37 || e.keyCode == 39 || e.keyCode == 110 || e.keyCode == 86 || e.keyCode == 17)) {
                            return false;
                        }
                    });
                    var x = [];
                    if (sessionStorage.getItem('supplier_items')!=null) {
                        x = JSON.parse(sessionStorage.getItem('supplier_items'));
                    }
                    console.log(x.length)
                    var sum = 0;
                    if (x.length!=0) {
                        $.each(x.data, function (index, value) {
                            sum+=parseFloat(value.amount);
                            $($('#supplier_items_table').DataTable().column(4).footer()).text(' итог: '+sum);
                        });
                    }
                },
                initComplete: function (items) {
                    var theTotal = 0;
                    $("#supplier_items_table td:nth-child(8)").each(function () {
                        theTotal += parseFloat($(this).text());
                    });
                    $($('#supplier_items_table').DataTable().column(7).footer()).text(' итог: '+theTotal.toFixed(2));
                }
            });
        }

        function get_supplier_items() {
            var supplier_id = $('#supplier_id').val();
            if (supplier_id != null && supplier_id>0){
                $.ajax({
                    url: '{{ route('get_supplier_items') }}',
                    type: 'get',
                    data: {supplier_id:supplier_id},
                    success: function (item) {
                        if (item!=null) {
                            if (JSON.parse(item).data.length > 0) {
                                sessionStorage.setItem('last_supplier_id', JSON.stringify($("#supplier_id").val()));
                                sessionStorage.setItem('supplier_items', item);
                                sessionStorage.setItem('lastBilling', JSON.stringify(JSON.parse(item).lastBilling));
                                $('#supplier_id').prop('disabled', true).trigger("chosen:updated");

                                fetchData();
                                if (JSON.parse(item).lastBilling!=null) {
                                    if (JSON.parse(item).lastBilling.deposit != null && JSON.parse(item).lastBilling.currency_id != null) {
                                        $('#deposit').val(JSON.parse(item).lastBilling.deposit);
                                        $('#before_shipment').val(JSON.parse(item).lastBilling.before_shipment);
                                        $('#currency').val(JSON.parse(item).lastBilling.currency_id).trigger("chosen:updated");
                                    }else{
                                        $('#deposit').val('');
                                        $('#before_shipment').val('');
                                        $('#currency').val('').trigger("chosen:updated");
                                    }
                                }
                            }else{
                                $('.alert-info ul').empty();
                                $('.alert-info').show();
                                $('.alert-info ul').append('<li>пустой</li>');
                            }
                        }
                        $('#loading').hide();

                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        console.log(json)
                        $('.alert-info ul').empty();
                        $('.alert-info').show();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').append('<li>'+value+'</li>');
                        })
                    }
                })
            }
        }

        function get_supplier_items_by_model() {
            var supplier_id = $('#supplier_id').val();
            var model_id = $('#supplier_model_id').val();
            var model_amount = $('#model_amount').val();
            console.log(supplier_id)
            if (supplier_id != null && supplier_id>0){
                $.ajax({
                    url: '{{ route('get_supplier_items_by_model') }}',
                    type: 'get',
                    data: {supplier_id:supplier_id, model_id:model_id, model_amount:model_amount},
                    success: function (item) {
                        if (JSON.parse(item).data.length > 0) {
                            sessionStorage.setItem('supplier_items', item);
                            sessionStorage.setItem('lastBilling', JSON.stringify(JSON.parse(item).lastBilling));
                            $('#supplier_id').prop('disabled', true).trigger("chosen:updated");

                            fetchData();

                            get_supplier_not_exist_items();
                            if (JSON.parse(item).lastBilling!=null) {
                                if (JSON.parse(item).lastBilling.deposit != null && JSON.parse(item).lastBilling.currency_id != null) {
                                    $('#deposit').val(JSON.parse(item).lastBilling.deposit);
                                    $('#before_shipment').val(JSON.parse(item).lastBilling.before_shipment);
                                    $('#currency').val(JSON.parse(item).lastBilling.currency_id).trigger("chosen:updated");
                                }else{
                                    $('#deposit').val('');
                                    $('#before_shipment').val('');
                                    $('#currency').val('').trigger("chosen:updated");
                                }
                            }
                        }else{
                            $('.alert-info ul').empty();
                            $('.alert-info').show();
                            $('.alert-info ul').append('<li>пустой</li>');
                        }
                        $('#loading').hide();

                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        console.log(json)
                        $('.alert-info ul').empty();
                        $('.alert-info').show();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').append('<li>'+value+'</li>');
                        })
                    }
                })
            }
        }

        function get_supplier_not_exist_items(){
            var supplier_id = $('#supplier').val();
            var items = sessionStorage.getItem('supplier_items');
            if (supplier_id != null && items != null){
                $.ajax({
                    url: "{{ route('get_supplier_items') }}",
                    type: "get",
                    data: {supplier_id:supplier_id},
                    success: function(data){
                        $('#supplier_model_id').empty().trigger("chosen:updated");
                        $.each(JSON.parse(data).data, function (index, subItemObj) {
                            if (!isExist(subItemObj.item_id)) {
                                $('#supplier_model_id').append('<option value="'+subItemObj.item_id+'">'+subItemObj.articula_new+' '+subItemObj.item_name+'</option>')
                            }
                        });
                        $('#supplier_model_id').trigger("chosen:updated");
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        console.log(json)
                        $('.alert-info').show();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').empty().append('<li>'+value+'</li>');
                        })
                    }
                })
            }
        }

        function clear_sessionStorage() {
            $('#supplier_id').val('').prop('selected', true).prop('disabled', false).trigger("chosen:updated");

            sessionStorage.removeItem('supplier_items');
            sessionStorage.removeItem('last_supplier_id');
            sessionStorage.removeItem('lastBilling');
            get_supplier_not_exist_items();
            $('#supplier_items_table').DataTable().clear();
            fetchData();
            $('#supplier_model_id').empty().trigger("chosen:updated");
            $('.alert-info').hide();
        }

        function delete_item(e) {
            var id = $(e).closest('tr').attr('id');
            var items = JSON.parse(sessionStorage.getItem('supplier_items')).data;
            items.splice(id, 1);
            var data = {
                'data':items
            };
            sessionStorage.setItem('supplier_items', JSON.stringify(data));
            fetchData();
            get_supplier_not_exist_items();
        }

        function save_items() {
            $('#save_order_btn').hide();
            if (confirm('сохранить?')) {
                var items = sessionStorage.getItem('supplier_items');
                var desc = $('#description').val();
                var shipment_time = $('#shipment_time').val();
                var deposit = $('#deposit').val();
                var before_shipment = $('#before_shipment').val();
                var due_time = $('#due_time').val();
                var currency = $('#currency').val();
                var supplier_id = $('#supplier_id').val();
                var invoice_number = $('#invoice_number').val();
                if(items!=null){
                    $.ajax({
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        type:'post',
                        url:"{{route('save_order')}}",
                        data:{items:items, description:desc, supplier_id:supplier_id, invoice_number:invoice_number, shipment_time:shipment_time, deposit:deposit, before_shipment:before_shipment, due_time:due_time, currency:currency},
                        success: function (data) {
                            $('#save_order_btn').show();
                            $('#description').val('');
                            $('#delivery_time').val('');
                            $('#invoice_number').val('');
                            $('#container_info').val('');
                            $('#deposit').val('');
                            var date = new Date();
                            $('#due_time').val(date.toISOString().substr(0, 10));
                            $('#before_shipment').val('');
                            clear_sessionStorage();
                            $('.alert-info').show();
                            $('.alert-info ul').empty().append('<li>сохранено</li>');
                            fetchData();
                            sessionStorage.setItem('last_fromStorage', JSON.stringify($('#supplier_id').val()));
                            window.location.replace("{{ route('createLogistics') }}"+'?supplier_id='+supplier_id+'&&invoice_number='+invoice_number);
                        },
                        error: function (request, status, error) {
                            $('#save_order_btn').show();
                            var json = $.parseJSON(request.responseText);
                            console.log(json)
                            $('.alert-info ul').empty();
                            $('.alert-info').show();
                            $.each(json.errors, function (index, value) {
                                $('.alert-info ul').append('<li>'+value+'</li>');
                            })
                        }
                    })
                }else{
                    alert('внесите данные')
                }
            }else{
                $('#save_order_btn').show();
            }
        }

        function save_item_weight(e){
            var items = JSON.parse(sessionStorage.getItem('supplier_items')).data;

            var id = $(e).closest('tr').attr('id');
            items[id].weight = "<input type='number' name='item_weight[]' id='"+id+"' value='"+$(e).val()+"' onkeyup=\"save_item_weight(this)\" class='form-control item_amount' style='height: 26px; width: 100px' placeholder='0'>";
            items[id].weight_kg = +$(e).val();

            console.log(e);

            var data = {
                'data':items
            };

            sessionStorage.setItem('supplier_items', JSON.stringify(data));
            var x = [];
            if (sessionStorage.getItem('supplier_items')!=null) {
                x = JSON.parse(sessionStorage.getItem('supplier_items'));
            }

            console.log(x);
            var sum = 0;
            $.each(x.data, function (index, value) {
                sum+=parseFloat(value.weight_kg);
                console.log(sum);
                $($('#supplier_items_table').DataTable().column(8).footer()).text(' итог: '+sum);
            });
        }

        function save_item_amount(e) {
            var items = JSON.parse(sessionStorage.getItem('supplier_items')).data;
            var id = $(e).closest('tr').attr('id');
            items[id].amount_input = "<input type='number' name='item_amount[]' id='"+id+"' value='"+$(e).val()+"' onkeyup=\"save_item_amount(this)\" class='form-control item_amount' style='height: 26px; width: 100px' placeholder='0'>";
            items[id].amount = $(e).val();
            var data = {
                'data':items
            };
            sessionStorage.setItem('supplier_items', JSON.stringify(data));
            var x = [];
            if (sessionStorage.getItem('supplier_items')!=null) {
                x = JSON.parse(sessionStorage.getItem('supplier_items'));
            }
            console.log(x)
            var sum = 0;
            $.each(x.data, function (index, value) {
                sum+=parseFloat(value.amount);
                $($('#supplier_items_table').DataTable().column(4).footer()).text(' итог: '+sum);
            });
            $('#supplier_items_table tbody #'+id).find('td:eq(7)').text($(e).val()*items[id].price_per_unit);
            var theTotal = 0;
            $("#supplier_items_table td:nth-child(8)").each(function () {
                theTotal += parseFloat($(this).text());
            });
            $($('#supplier_items_table').DataTable().column(7).footer()).text(' итог: '+theTotal.toFixed(2));
        }

        function save_price_value(e) {
            var items = JSON.parse(sessionStorage.getItem('supplier_items')).data;
            var id = $(e).closest('tr').attr('id');
            items[id].price_per_unit_input = "<input type='number' name='item_price[]' id='"+id+"' value='"+$(e).val()+"' onkeyup=\"save_price_value(this)\" onfocus=\"this.select()\" class='form-control item_price' style='height: 26px; width: 100px' placeholder='0'>";
            items[id].price_per_unit = $(e).val();
            var data = {
                'data':items
            };
            sessionStorage.setItem('supplier_items', JSON.stringify(data));
            var theTotal = 0;
            $('#supplier_items_table tbody #'+id).find('td:eq(7)').text($(e).val()*items[id].amount);
            $("td:nth-child(8)").each(function () {
                console.log($(this).text())
                theTotal += parseFloat($(this).text());
            });
            $($('#supplier_items_table').DataTable().column(7).footer()).text(' итог: '+theTotal.toFixed(2));
        }

        function isExist(item_id){
            var items = JSON.parse(sessionStorage.getItem('supplier_items')).data;
            if(items!=null){
                for (var i=0; i<items.length; i++){
                    if(item_id === items[i].item_id){
                        return true
                    }
                }
            }
            return false
        }

        $('#import_order_file').on('submit', function(event){
            $('#loading').show()
            event.preventDefault();
            var supplier_id = $('#supplier_id').val()
            if (supplier_id!=null) {
                $.ajax({
                    url:"{{ route('upload_order_file') }}",
                    method:"POST",
                    data:new FormData(this),
                    dataType:'JSON',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success:function(data)
                    {
                        console.log(data);
                        if (data.data.length > 0) {
                            sessionStorage.setItem('supplier_items', JSON.stringify(data));
                            document.getElementById("import_order_file").reset();
                            fetchData();
                        }
                        $('#loading').hide();
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        console.log(json);
                        $('.alert-info').show();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').empty().append('<li>'+value+'</li>');
                        })
                        $('#loading').hide()

                    }
                })
            }else{
                alert('выберите поставщика!')
            }
        });

        function get_supplier_models(){
            var supplier_id = $('#supplier_id').val();
            $.ajax({
                url:"{{ route('getSupplierModels') }}",
                method:"get",
                data:{supplier_id:supplier_id},
                success:function(data)
                {
                    var items = JSON.parse(data);
                    $('#supplier_model_id').empty().trigger("chosen:updated");
                    $.each(items, function (index, value) {
                        $('#supplier_model_id').append('<option value="'+value.id+'">'+value.articula_new+' '+value.item_name+'</option>')
                    });
                    $('#supplier_model_id').trigger("chosen:updated");
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    console.log(json);
                    $('.alert-info').show();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').empty().append('<li>'+value+'</li>');
                    })
                    $('#loading').hide()

                }
            });
        }
    </script>
@endsection

@section('additional_css')
    <style>
        .table tbody tr{
            cursor: pointer;
        }
        p{
            margin: 0;
        }
        tr.different-units-in-measure td:nth-child(6){
            background-color: blue;
            color: white;
        }
        tr.different-units-in-measure td:nth-child(8){
            background-color: blue;
            color: white;
        }
        tr.no-storage-detail-item td:nth-child(7){
            background-color: brown;
            color: white;
        }
    </style>

@endsection
@php
    if (session()->has('filtered_items')){
        $items = session()->get('filtered_items');
    }else{
        //$items = $all_items;
    }
@endphp
<div class="alert alert-danger" style="display:none">
    <ul>

    </ul>
</div>
<!-- Modal create parent item-->
<div class="modal fade" id="createItem" role="dialog">
    <div class="modal-dialog modal-sm" style="width: 50%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="font-size: 32px">&times;</span>
                </button>
                <h3 class="modal-title" id="createItemLabel">создание новой единицы</h3>
            </div>
            <form method="post" id="createItemForm" enctype="multipart/form-data">
            <div class="modal-body">
                <div class="alert alert-info" style="display:none">
                    <ul>

                    </ul>
                </div>

                <div class="col-md-6">
                    <label for="articula_new"> нов. артикул</label>
                    <input type="text" id="articula_new" name="articula_new" class="form-control" />

                    <label for="articula_old"> стр. артикул</label>
                    <input type="text" id="articula_old" name="articula_old" class="form-control" />

                    <label for="sap_code"> SAП код</label>
                    <input type="text" id="sap_code" name="sap_code" class="form-control" />

                    <label for="item_name"> наименование</label>
                    <input type="text" id="item_name" name="item_name" class="form-control" />
                </div>
                <div class="col-md-6">
                    <label for="units">Выберите ед. измерения</label>
                    <select id="units" name="units[]" data-placeholder="единицы измерения" multiple class="standardSelect">
                        @foreach($units as $unit)
                            <option value="{{ $unit->id }}" >{{ $unit->unit }}</option>
                        @endforeach
                    </select>
                    <label for="item_file">фото единицы (*.png)</label>
                    <input type="file" name="item_file" id="item_file" placeholder="выберите файл" class="form-control">
                    <img src="" id="item_photo" name="item_photo" width="300" height="200" style="margin-top: 10px" alt="" accept="image/x-png">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="close_create_modal">отмена</button>
                <input type="submit" name="save_item_form" id="save_item_form" style="float: right;" class="btn btn-primary" value="сохранить">
                {{--<button type="button" class="btn btn-primary" onclick="save_item()">создать</button>--}}
            </div>
            </form>
        </div>
    </div>
</div>


<!-- filter by checkbox-->
<div class="modal fade" id="filterByCheckbox" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="padding: 0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close_filter_modal">
                    <span aria-hidden="true" style="font-size: 32px">&times;</span>
                </button>
                <h3 class="modal-title" id="filterByCheckboxLabel">Фильтр</h3>
            </div>

            <div class="modal-body" style="padding-top: 4px; padding-right: 4px">
                <form action="{{ route('filter_items_by_checkbox') }}" method="post" id="filter_modal">
                    {{ csrf_field() }}
                    @include('pages.items_page_includes.filter_modul')
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" onclick="filter_items()" id="filter_items_btn">фильтр</button>
            </div>
        </div>
    </div>
</div>

{{--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>--}}

<div id="ItemInfo" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="margin: 0; padding: 5px">
                <button type="button" class="close" data-dismiss="modal" style="font-size: 23px; border-radius: 1px" onclick="clearNavigation()" id="itemInfoModalCloseBtn">&times;</button>
                <h4 class="modal-title" id="navigationPart">navigation</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-info" style="display:none">
                    <ul>

                    </ul>
                </div>
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#itemNormTabs">Нормы</a></li>
                    <li><a data-toggle="tab" href="#rawOfModel">Детальная норма</a></li>
                    <li><a data-toggle="tab" href="#itemSpecificationTabs">Спецификация</a></li>
                    <li><a data-toggle="tab" href="#itemModelsTabs">Г.П.</a></li>
                    <li><a data-toggle="tab" href="#itemSuppliersTabs">Поставщики</a></li>
                </ul>

                <div class="tab-content">
                    <div id="itemNormTabs" class="tab-pane fade in active">
                        <h4 class="modal-title" id="itemNameOfNorm">Нормы расхода:</h4>
                        <a href="/items/child_item_import_page/" class="btn btn-warning" style="margin-right: 5px; display: none" target="_blank" id="showNormImportModul"><i class="glyphicon glyphicon-import"></i> Импорт</a>
                        <button type="button" id="addItemToNormBtn" class="btn btn-primary" data-toggle="modal" data-target="#addToNormModal" style="margin-right: 5px; display: none"><i class="fa fa-plus"></i> Добавить</button>
                        <button type="button" class="btn btn-danger" style="display: none" onclick="delete_norm()" id="deleteNormEntirely"><i class="fa fa-trash"></i> удалить норму</button>
                        <table id="item_norm_table" class="table table-bordered table-hover" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>артикул</th>
                                <th>старый артикул</th>
                                <th>SAP код</th>
                                <th>наименование</th>
                                <th>кол-во</th>
                                <th>ед. изм.</th>
                                <th>склад</th>
                                <th>ед.изм. скл.</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                            <tr>
                                <th>артикул</th>
                                <th>старый артикул</th>
                                <th>SAP код</th>
                                <th>наименование</th>
                                <th>кол-во</th>
                                <th>ед. изм.</th>
                                <th>склад</th>
                                <th>ед.изм. скл.</th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div id="itemSpecificationTabs" class="tab-pane fade">
                        <form method="post" id="upload_form" enctype="multipart/form-data">
                            <input type="hidden" name="itemIdOfShownModal" id="itemIdOfShownModal">

                            <div class="col-md-12 clearfix">
                                <div class="col-md-3">
                                    <label for="edit_articula_new">нов. артикул</label>
                                    <input type="text" name="edit_articula_new" value="" id="edit_articula_new" class="form-control" disabled/>

                                    <label for="edit_articula_old">стр артикул</label>
                                    <input type="text" name="edit_articula_old" id="edit_articula_old" value="" class="form-control" disabled/>

                                    <label for="edit_sap_code">САП код</label>
                                    <input type="text" name="edit_sap_code" value="" id="edit_sap_code" class="form-control" disabled/>

                                    <label for="edit_item_name">наименование</label>
                                    <input type="text" name="edit_item_name" id="edit_item_name" value="" class="form-control" disabled/>
                                </div>
                                <div class="col-md-3">
                                    <label for="edit_units">ед. изм.</label>
                                    <select data-placeholder="выбрать единицу измерения..." multiple class="standardSelect" id="edit_units" name="units[]">
                                        @foreach($units as $unit)
                                            <option value="{{ $unit->id }}">{{ $unit->unit }}</option>
                                        @endforeach
                                    </select>

                                    <label for="edit_item_weight">вес</label>
                                    <input type="text" name="edit_item_weight" value="" id="edit_item_weight" class="form-control" disabled/>

                                    <label for="edit_item_length">длина</label>
                                    <input type="text" name="edit_item_length" id="edit_item_length" value="" class="form-control" disabled/>
                                </div>
                                <div class="col-md-3">
                                    <label for="edit_item_photo">фото (*.png)</label>
                                    <img src="" alt="photo" width="300" height="200" style="background-color: #00ccff; float: left; position: relative; margin: 0 5px 10px" class="img img-rounded" id="edit_item_photo" >
                                    <input type="file" name="item_photo_upload" width="200" style="float: left" id="item_photo_upload" accept="image/x-png">
                                    <button type="button" class="btn btn-danger" onclick="deleteItemPhoto()" id="delete_photo_btn" style="float: left; display: none"><i class="fa fa-trash"></i></button>
                                </div>
                            </div>
                            <div class="col-md-12 clearfix">
                                <input type="submit" name="upload" id="update_item_button" style="float: right; display: none;" class="btn btn-primary" value="сохранить">
                                {{--<button type="submit" class="btn btn-primary" style="float: right; display: none;" onclick="update_item()" id="update_item_button">сохранить</button>--}}
                            </div>
                        </form>
                    </div>
                    <div id="itemModelsTabs" class="tab-pane fade">
                        <table id="foundModelsTable" class="table table-bordered table-hover" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>артикул</th>
                                <th>старый артикул</th>
                                <th>SAP код</th>
                                <th>наименование</th>
                                <th>кол-во</th>
                                <th>ед. изм.</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div id="itemSuppliersTabs" class="tab-pane fade">
                        <table id="suppliers_table" class="table table-bordered table-hover myTable" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>поставщик</th>
                                <th>адрес</th>
                                <th>телефон</th>
                                <th>веб сайт</th>
                                <th>страна</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                    </div>
                    <div id="rawOfModel" class="tab-pane fade">
                        <table id="rawOfModelsTable" class="table table-bordered table-hover" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>артикул ГП</th>
                                <th>SAP код ГП</th>
                                <th>наименование ГП</th>
                                <th>артикул</th>
                                <th>SAP код</th>
                                <th>наименование</th>
                                <th>кол-во расхода</th>
                                <th>ед. изм.</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type='button' class='btn btn-danger' onclick='delete_item()' id="delete_item_btn" style="float:right;"><i class='fa fa-trash'></i> удалить единицу</button>
            </div>

        </div>
    </div>
</div>


<!-- Modal add child item-->
<div class="modal fade" id="addToNormModal" role="dialog" style="z-index: 9999999999999999999">
    <div class="modal-dialog" style="width: 50%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeAddToNormModal">
                    <span aria-hidden="true" style="font-size: 32px">&times;</span>
                </button>
                <h3 class="modal-title" id="AddToNormModalLabel">Добавление нормы расхода</h3>
            </div>
            <div class="modal-body">
                <div class="alert alert-success" style="display:none">
                    <ul>

                    </ul>
                </div>
                <label for="normItem">выбрать сборочную единицу</label>
                <select id="normItem" name="normItem" data-placeholder="" class="standardSelect" >
                    <option value=""></option>
                </select>
                <div class="col-md-12 clearfix">
                    <div class="col-md-6">
                        <label for="subItemUnits">выбрать ед. измерения</label>
                        <select data-placeholder="" class="standardSelect" id="subItemUnits" name="units[]">
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label for="normItemQuantity">кол-во</label>
                        <input type="number" id="normItemQuantity" name="normItemQuantity" min="0" step="0.0001" class="form-control" placeholder=""/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="addItemToNorm()">добавить</button>
            </div>
        </div>
    </div>
</div>

{{--<!-- Modal update child item-->--}}
<div class="modal fade" id="editNormItemModal" role="dialog">
    <div class="modal-dialog"  style="width: 30%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeEditNormItemModal">
                    <span aria-hidden="true" style="font-size: 32px">&times;</span>
                </button>
                <h3 class="modal-title" id="editNormItemModalLabel">изменить</h3>
            </div>
            <div class="modal-body" style="height: 450px">
                <div class="alert alert-success" style="display:none">
                    <ul>

                    </ul>
                </div>
                <input type="hidden" name="childIdForEditNormItem" id="childIdForEditNormItem" value="">
                <div class="clearfix" id="allNormItemQuantities">

                </div>
                <span>взаимозаменяемые детали</span>
                <select data-placeholder="выбрать" class="standardSelect" id="replacement_items" name="replacement_items">
                    <option value="0"></option>
                    @foreach($all_items as $item)
                        <option value="{{ $item->id }}" >{{ $item->articula_new }} {{ $item->item_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" onclick="updateNormItem()">сохранить</button>
            </div>

        </div>
    </div>
</div>

{{--<!-- Modal update child item-->--}}
<div class="modal fade" id="replaceNormItemModal" role="dialog">
    <div class="modal-dialog"  style="width: 30%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeReplaceNormItemModal">
                    <span aria-hidden="true" style="font-size: 32px">&times;</span>
                </button>
                <h3 class="modal-title" id="editReplaceItemModalLabel">изменить</h3>
            </div>
            <div class="modal-body" style="height: 450px">
                <div class="alert alert-success" style="display:none">
                    <ul>

                    </ul>
                </div>
                <input type="hidden" name="mainItemId" id="mainItemId" value="">

                <span>взаимозаменяемые детали</span>
                <select data-placeholder="выбрать" class="standardSelect" id="replacedItemId" name="replacedItemId">
                    <option value="0"></option>
                    @foreach($all_items as $item)
                        <option value="{{ $item->id }}" >{{ $item->articula_new }} {{ $item->item_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" onclick="replaceNormItem()">сохранить</button>
            </div>

        </div>
    </div>
</div>


<div class="panel panel-primary">
    <div class="panel-heading" style="padding:1px 5px">Все детали</div>
    <div class="panel-body" style="overflow-x: auto">
        <a href="{{ route('show_import_page', ['parentId'=>0]) }}" class="btn btn-warning" onclick="sessionStorage.removeItem('import_parent_item');" target="_blank" style="display: none" id="importItemsBtn"><i class="glyphicon glyphicon-import"></i> импорт</a>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createItem" style="display: none" id="createItemBtn">
            <i class="glyphicon glyphicon-plus"></i> создать
        </button>
        <button type="button" class="btn btn-danger" id="export-all-items-with-detail">
            <i class="glyphicon glyphicon-import"></i>
            import all with norm detail
        </button>
        <button type="button" class="btn btn-warning" id="export-all-items-norms-with-detail">
            <i class="glyphicon glyphicon-import"></i>
            import all norm
        </button>
        {{--<button type="button" class="btn btn-info" data-toggle="modal" data-target="#filterByCheckbox" id="show_filter_modal">Фильтр</button>--}}
        {{--<a href="{{ route('articula_generate') }}" target="_blank" class="btn btn-info" style="float: right; display: none" id="generateArticulBtn">Генератор (артикул)</a>--}}
        <div class="col-md-2" style="float: right">
            <select name="storage[]" data-placeholder="выбрать склад" id="storage" class="standardSelect" multiple>
                @foreach($storages as $storage)
                    <option value="{{ $storage->id }}">{{ $storage->name }}</option>
                @endforeach
            </select>
        </div>
        <table id="all_items_table" class="table table-bordered table-hover" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>артикул</th>
                <th>старый артикул</th>
                <th>SAP код</th>
                <th>наименование</th>
                <th width="100px">ед. изм.</th>
                <th>склады</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
            <tfoot>
            <tr>
                <th>артикул</th>
                <th>старый артикул</th>
                <th>SAP код</th>
                <th>наименование</th>
                <th width="100px">ед. изм.</th>
                <th>склады</th>
            </tr>
            </tfoot>
        </table>
    </div>
</div>
@section('additionalLibrary')
    {{--ready window for work--}}
    <script>
        $(document).ready(function() {
            sessionStorage.removeItem('navigation');
            fetch_all_items_table();
            $('#loading').hide();
            $('#all_items_table').DataTable().search('').draw();

            $('a[data-toggle="tab"]').on('shown.bs.tab', function () {
                $('#foundModelsTable').DataTable().columns.adjust().draw();
                $('#item_norm_table').DataTable().columns.adjust().draw();
                $('#suppliers_table').DataTable().columns.adjust().draw();
                $('#rawOfModelsTable').DataTable().columns.adjust().draw();
                $('.alert-danger').hide();
                $('.alert-info').hide();
                $('.alert-success').hide();
            });

            $("#item_photo_upload").change(function(){
                readURL(this);
            });
            $("#item_file").change(function(){
                readURL1(this);
            });

            $('#all_items_table').on('dblclick', 'tr', function (e) {
                var id = $(this).data('id');
                get_replacement_items(id);
                $('#itemIdOfShownModal').val(id);
                $('#ItemInfo').modal("show");
            });

            $('#item_norm_table').on('dblclick', 'tr', function (e) {
                var id = $(this).data('id');
                $('#itemIdOfShownModal').val(id);
                show_detail();
            });

            $('#all_items_table tbody').on( 'click', 'tr', function () {
                if ( $(this).hasClass('selected') ) {
                    // do nothing
                }
                else {
                    $('#all_items_table').DataTable().$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            });

        });
    </script>
    {{--create parent item--}}
    <script>

        $("#storage").on("change", function (e) {
            fetch_all_items_table();
        });

        $('input[name=searchByDetailId]').on('input', function() {
            fetch_model_data();
            $('#foundModelsTable').DataTable().columns.adjust().draw();
            $('.alert-danger').hide();
            $('.alert-info').hide();
            $('.alert-success').hide();
        });

        $('#export-all-items-with-detail').on('click', function (){
            exportAllItemsWithDetail();
        })

        $('#export-all-items-norms-with-detail').on('click', function (){
            exportAllItemsNorm();
            console.log("Norms export fired!");
        })

        function exportAllItemsNorm(){
            let data = sessionStorage.getItem("coreItems");
            let data1 = JSON.parse(data);

            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: "{{route("exportDetailedItemsNorm")}}",
                type: 'post',
                data: {items: data1},
                success: function (data) {
                    let d = JSON.parse(data);
                    var a = document.createElement('a');
                    var url = d.url;
                    console.log(url);
                    a.href = url;
                    a.download = 'WMS_detail_norm.xlsx';
                    document.body.append(a);
                    a.click();
                    a.remove();
                    window.URL.revokeObjectURL(url);
                },
                error: function (err){
                    console.log(err);
                }
            })

        }

        function exportAllItemsWithDetail(){

            let data = sessionStorage.getItem("coreItems");
            let data1 = JSON.parse(data);
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: "{{route("exportDetailedItems")}}",
                type: 'post',
                data: {items: data1},
                success: function (data) {
                    let d = JSON.parse(data);
                    var a = document.createElement('a');
                    var url = d.url;
                    console.log(url);
                    a.href = url;
                    a.download = 'WMS_buxgalteriya_norm.xlsx';
                    document.body.append(a);
                    a.click();
                    a.remove();
                    window.URL.revokeObjectURL(url);
            },
                error: function (err){
                    console.log(err);
                }
            })
        }

        function readURL(input) {
            var url = input.value;
            var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
            if (input.files && input.files[0]&& (ext == "png")) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#edit_item_photo').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }else{
                $('#edit_item_photo').attr('src', '/assets/no_preview.png');
            }
        }

        function readURL1(input) {
            var url = input.value;
            var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
            if (input.files && input.files[0]&& (ext == "png")) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#item_photo').attr('src', e.target.result).show();
                };

                reader.readAsDataURL(input.files[0]);
            }else{
                $('#item_photo').attr('src', '/assets/no_preview.png');
            }
        }



        function fetch_all_items_table() {

            sessionStorage.removeItem("coreItems");

            var storages = $('#storage').val();
            $("#all_items_table").DataTable().destroy();
            var table = $('#all_items_table').DataTable( {
                "processing": true,
                serverSide: true,
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    "url": "{{ route('getItems') }}",
                    "type": "post",
                    "dataType": "json",
                    data:{
                        storages:storages
                    }
                },
                columns: [
                    { "data": "articula_new", "width": "10%" },
                    { "data": "articula_old", "width": "10%" },
                    { "data": "sap_code", "width": "10%" },
                    { "data": "item_name", "width": "40%" },
                    { "data": "unit", "width": "4%" },
                    { "data": "storages", "width": "10%" }
                ],
                order: [[1, 'asc']],
                stateSave: true,
                pageLength: 100,
                lengthMenu:[[30, 50, 100, 500, 1000, -1], [30, 50, 100, 500, 1000, "All"]],
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5]
                        },
                        title:'бум лист',
                        text: '<i class="fa fa-file-excel-o"></i>'

                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5]
                        },
                        title:'бум лист',
                        text: '<i class="fa fa-file-pdf-o"></i>'

                    },
                ],
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr('data-id', data.id);
                },
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение _START_ для _END_ из _TOTAL_ записей",
                    "infoEmpty": "данные отсутствует",
                    "infoFiltered": "(<b>отфильтровано из _MAX_ записей</b>)",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                drawCallback: function (data) {
                    console.log(data);
                    sessionStorage.setItem("coreItems", JSON.stringify(data.json.data))
                    if (data.json.hasPerm) {
                        $('#importItemsBtn').show();
                        $('#createItemBtn').show();
                        $('#generateArticulBtn').show();
                        $('#export-all-items-with-detail').show();
                        $('#export-all-items-norms-with-detail').show();
                    }else{
                        $('#export-all-items-norms-with-detail').hide();
                        $('#export-all-items-with-detail').hide();
                        $('#importItemsBtn').hide();
                        $('#createItemBtn').hide();
                        $('#generateArticulBtn').hide();
                    }
                },
                scrollY: "60vh",
            } );
        }
        $('#ItemInfo').on('shown.bs.modal', function(e) {
            show_detail();
        });
        $('#ItemInfo').on('hidden.bs.modal', function(e) {
            sessionStorage.removeItem('navigation');
        });
        function show_detail() {
            var item_id = $('#itemIdOfShownModal').val();
            $('.alert-danger').hide();
            $('.alert-info').hide();
            $('.alert-success').hide();
            $('#loading').show();
            $('#itemIdOfShownModal').val(item_id);
            fetch_item_norm_table(item_id);
            fetch_model_data(item_id);
            fetch_suppliers_data(item_id);
            fetch_raw_data(item_id);
            set_item_values(item_id);
        }

        function clearNavigation() {
            sessionStorage.removeItem('navigation');
        }

        function fetch_item_norm_table(item_id) {
            $('#item_photo_upload').val('');
            $("#item_norm_table").DataTable().search('').draw(false);
            $('#showNormImportModul').attr('href', '{{ url('/home/items/child_item_import_page') }}/'+item_id);
            $('#loading').hide();
            $("#item_norm_table").DataTable().destroy();
            $('#item_norm_table').DataTable( {
                processing: true,
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    "url": "{{ route('getItemNormItems') }}",
                    "type": "post",
                    "data":{item_id:item_id},
                },
                columns: [
                    { "data": "articula_new", "width": "10%" },
                    { "data": "articula_old", "width": "10%" },
                    { "data": "sap_code", "width": "10%" },
                    { "data": "item_name", "width": "35%" },
                    { "data": "quantities", "width": "5%" },
                    { "data": "units_name", "width": "4%" },
                    { "data": "storages", "width": "10%" },
                    { "data": "unit", "width": "6%" },
                    { "data": "buttons", "width": "5%" }
                ],
                columnDefs: [
                    { orderable: false, targets: [5, 6, 8] }
                ],
                order: [[1, 'asc']],
                stateSave: true,
                paging: false,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5, 6, 7]
                        },
                        title:'бум лист',
                        text: '<i class="fa fa-file-excel-o"></i>'

                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5, 6, 7]
                        },
                        title:'бум лист',
                        text: '<i class="fa fa-file-pdf-o"></i>'

                    },
                ],
                createdRow: function( row, data, dataIndex ) {
                    $( row ).find('td:eq(6)').attr('nowrap', 'nowrap');
                    $(row).attr('data-id', data.id);

                    data['storages'] ? null : $(row).attr("class", "no-storage-detail-item");

                    if(data['units_name'] !== data['unit']){
                        $(row).attr('class', "different-units-in-measure")
                    }
                },
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение _START_ для _END_ из _TOTAL_ записей",
                    "infoEmpty": "данные отсутствует",
                    "infoFiltered": "(<b>отфильтровано из _MAX_ записей</b>)",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                "initComplete": function( settings, json ) {
                    console.log(json.hasPerm)
                    if (json.hasPerm) {
                        $('#showNormImportModul').show();
                        $('#addItemToNormBtn').show();
                        $('#deleteNormEntirely').show();
                    }else{
                        $('#showNormImportModul').hide();
                        $('#addItemToNormBtn').hide();
                        $('#deleteNormEntirely').hide();
                    }
                    $('#itemNameOfNorm').html(json.parentItem.articula_new+' '+json.parentItem.item_name);
                    createNavigation(json.parentItem.id, json.parentItem.articula_new);
                },
                scrollY: "50vh",
            } );
            var table = $('#item_norm_table').DataTable();

            $('#item_norm_table tbody').on( 'click', 'tr', function () {
                if ( $(this).hasClass('selected') ) {
                    // do nothing
                }
                else {
                    table.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            });
        }

        function fetch_raw_data(item_id) {
            $("#rawOfModelsTable").DataTable().destroy();
            $('#rawOfModelsTable').DataTable( {
                processing: true,
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    "url": "{{ route('getRawOfModel') }}",
                    "type": "post",
                    "data":{item_id:item_id},
                },
                columns: [
                    { "data": "parent_articula", "width": "5%" },
                    { "data": "parent_sap_code", "width": "5%" },
                    { "data": "parent_name", "width": "20%" },
                    { "data": "child_articula", "width": "5%" },
                    { "data": "child_sap_code", "width": "5%" },
                    { "data": "child_name", "width": "20%" },
                    { "data": "consumption", "width": "5%"},
                    { "data": "unit", "width": "5%"}
                ],
                columnDefs: [
                    { orderable: false, targets: [5, 6] }
                ],
                order: [[1, 'asc']],
                stateSave: true,
                paging: false,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5, 6, 7]
                        },
                        title:'бум лист',
                        text: '<i class="fa fa-file-excel-o"></i>'

                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5, 6, 7]
                        },
                        title:'бум лист',
                        text: '<i class="fa fa-file-pdf-o"></i>'

                    },
                ],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение _START_ для _END_ из _TOTAL_ записей",
                    "infoEmpty": "данные отсутствует",
                    "infoFiltered": "(<b>отфильтровано из _MAX_ записей</b>)",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                scrollY: "70vh",
            } );
        }

        function createNavigation(item_id, articula_new) {
            var navArray = [];
            var canPush = true;
            if (sessionStorage.getItem('navigation') != null) {
                navArray = JSON.parse(sessionStorage.getItem('navigation'));
            }
            console.log(navArray);
            var data = {id:item_id, articula_new:articula_new};
            $.each(navArray, function (i, val) {
                if (item_id == val.id) {
                    canPush = false;
                }
            });
            if (canPush) {
                navArray.push(data);
                sessionStorage.setItem('navigation', JSON.stringify(navArray));
                var x = '';
                $.each(navArray, function (i, val) {
                    x+='<a href="javascript:void(0);" onclick="changeByNavigation('+val.id+')">'+val.articula_new+'</a>'+' > ';
                });
                $('#navigationPart').html(x);
            }
        }

        function changeByNavigation(id) {
            var navArray = [];
            if (sessionStorage.getItem('navigation') != null) {
                navArray = JSON.parse(sessionStorage.getItem('navigation'));
                $.each(navArray, function (i, val) {
                    if (id == val.id) {
                        navArray.splice(i, (navArray.length-i));
                        return false;
                    }
                });

                console.log(navArray);
                sessionStorage.setItem('navigation', JSON.stringify(navArray));
                $('#itemIdOfShownModal').val(id);
                show_detail();
            }
        }

        function fetch_model_data(item_id) {
            $("#foundModelsTable").DataTable().destroy();
            $('#foundModelsTable').DataTable( {
                processing: true,
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    "url": "{{ route('searchModelByDetail') }}",
                    "type": "post",
                    "data": {item_id:item_id},
                },
                columns: [
                    { "data": "articula_new", "width": "10%" },
                    { "data": "articula_old", "width": "10%" },
                    { "data": "sap_code", "width": "10%" },
                    { "data": "item_name", "width": "40%" },
                    { "data": "quantity", "width": "4%" },
                    { "data": "unit", "width": "4%" }
                ],
                columnDefs: {orderable: false, target:[0]},
                order: [[0, 'asc']],
                stateSave: true,
                paging: false,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3,4,5]
                        },
                        title:'бум лист',
                        text: '<i class="fa fa-file-excel-o"></i>'

                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3,4,5]
                        },
                        title:'бум лист',
                        text: '<i class="fa fa-file-pdf-o"></i>'

                    },
                ],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение _START_ для _END_ из _TOTAL_ записей",
                    "infoEmpty": "данные отсутствует",
                    "infoFiltered": "(<b>отфильтровано из _MAX_ записей</b>)",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                scrollY: "60vh",
            } );
        }

        function fetch_suppliers_data(item_id) {
            $("#suppliers_table").DataTable().destroy();
            $('#suppliers_table').DataTable( {
                processing: true,
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    "url": "{{ route('getItemSuppliers') }}",
                    "type": "post",
                    "data": {item_id:item_id},
                },
                columns: [
                    { "data": "name", "width": "40%" },
                    { "data": "address", "width": "20%" },
                    { "data": "phone", "width": "10%" },
                    { "data": "website", "width": "10%" },
                    { "data": "country", "width": "10%" },
                ],
                columnDefs: {orderable: false, target:[0]},
                order: [[0, 'asc']],
                stateSave: true,
                paging: false,
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3]
                        },
                        title:'бум лист',
                        text: '<i class="fa fa-file-excel-o"></i>'

                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3]
                        },
                        title:'бум лист',
                        text: '<i class="fa fa-file-pdf-o"></i>'

                    },
                ],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение _START_ для _END_ из _TOTAL_ записей",
                    "infoEmpty": "данные отсутствует",
                    "infoFiltered": "(<b>отфильтровано из _MAX_ записей</b>)",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                scrollY: "60vh",
            } );
        }

        $('#createItemForm').on('submit', function(event){
            event.preventDefault();
            $('.alert-info').hide();
            var articula_new = $("input[name=articula_new]").val();
            var articula_old = $( "input[name=articula_old]" ).val();
            var sap_code = $( "input[name=sap_code]" ).val();
            var item_name = $( "input[name=item_name]" ).val();
            var units = $("#units").val();
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type:'POST',
                url:"{{route('create_item')}}",
                contentType: false,
                cache: false,
                processData: false,
                data:new FormData(this),
                success:function(item){
                    $("#all_items_table").DataTable().page('last').draw('page');
                    data = $.parseJSON(item);
                    document.getElementById("createItemForm").reset();
                    $("#units").val('').trigger('chosen:updated');
                    $('#close_create_modal').click();
                    $('.alert-danger').show();
                    $('.alert-danger ul').empty().append('<li>сохранен</li>');
                    fetch_all_items_table();
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    console.log(json)
                    $('.alert-info').show();
                    $('.alert-info ul').empty();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').append('<li>'+value+'</li>');
                    })
                }
            });
        });

        function save_item() {
            var articula_new = $("input[name=articula_new]").val();
            var articula_old = $( "input[name=articula_old]" ).val();
            var sap_code = $( "input[name=sap_code]" ).val();
            var item_name = $( "input[name=item_name]" ).val();
            var units = $("#units").val();
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type:'POST',
                url:"{{route('create_item')}}",
                data:{articula_new:articula_new, articula_old:articula_old, sap_code:sap_code, item_name:item_name, units:units},
                success:function(item){
                    $("#all_items_table").DataTable().page('last').draw('page');
                    data = $.parseJSON(item);
                    document.getElementById("createItemForm").reset();
                    $("#units").val('').trigger('chosen:updated');
                    $('#close_create_modal').click();
                    $('.alert-danger').show();
                    $('.alert-danger ul').empty().append('<li>сохранен</li>');
                    fetch_all_items_table();
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    console.log(json)
                    $('.alert-info').show();
                    $('.alert-info ul').empty();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').append('<li>'+value+'</li>');
                    })
                }
            });
        }

        function delete_item() {
            var id = $('#itemIdOfShownModal').val();
            $('.alert-danger').hide();
            if (confirm('удалить?')){
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'post',
                    url:'{{ route('delete_item') }}',
                    data:{ item_id:id },
                    success: function (data) {
                        $('.alert-danger').show();
                        $('.alert-danger ul').empty().append('<li>удален</li>');
                        $('#itemInfoModalCloseBtn').click();
                        fetch_all_items_table();
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        console.log(json)
                        $('.alert-danger').show();
                        $('.alert-danger ul').empty();
                        $.each(json.errors, function (index, value) {
                            $('.alert-danger ul').append('<li>'+value+'</li>');
                        })
                    }
                })
            }
        }
        function set_item_values(id) {
            $('#loading').show();
            $('.alert-danger').hide();
            $('.alert-info').hide();
            $('.alert-success').hide();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type:'post',
                url:'{{ route('get_item_values') }}',
                data:{ item_id:id },
                success: function (data) {
                    var d = new Date();
                    var item = JSON.parse(data);
                    $('#edit_articula_new').val(item['data'].articula_new);
                    $('#edit_articula_old').val(item['data'].articula_old);
                    $('#edit_sap_code').val(item['data'].sap_code);
                    $('#edit_item_name').val(item['data'].item_name);
                    $('#edit_units').val(item['units']).trigger('chosen:updated');
                    console.log(item['path'])
                    if (item['path']!=null) {
                        $('#edit_item_photo').attr('src', item['path']+"?"+d.getTime());
                    }else{
                        $('#edit_item_photo').attr('src', '');
                    }
                    if (item['hasPerm']) {
                        $('#edit_articula_new').attr('disabled', false);
                        $('#edit_articula_old').attr('disabled', false);
                        $('#edit_sap_code').attr('disabled', false);
                        $('#edit_item_name').attr('disabled', false);
                        $('#edit_units').attr('disabled', false);
                        $('#update_item_button').show();
                        $('#delete_photo_btn').show();
                    }
                    $('#loading').hide()
                },
                error: function (request, status, error) {
                    alert('произошла ошибка');
                    console.log(error)
                }
            })
        }

        $('#upload_form').on('submit', function(event){
            event.preventDefault();
            $('.alert-info').hide();
            var articula_new = $("input[name=edit_articula_new]").val();
            var articula_old = $("input[name=edit_articula_old]").val();
            var sap_code = $("input[name=edit_sap_code]").val();
            var item_name = $("input[name=edit_item_name]").val();
            var item_id = $("#itemIdOfShownModal").val();
            var units = $('#edit_units').val();
            console.log(item_id)
            if (confirm('изменить?')) {
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'POST',
                    url:"{{route('update_item')}}",
                    contentType: false,
                    cache: false,
                    processData: false,
                    data:new FormData(this),
                    success:function(item){
                        $('#item_photo_upload').val('');
                        data = jQuery.parseJSON(item);
                        console.log(data)
                        set_item_values(item_id);
                        fetch_all_items_table();
                        $('#close_parent_edit_modal').click();
                        $('.alert-info').show();
                        $('.alert-info ul').empty().append('<li>обнавлено</li>');
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        console.log(json)
                        $('.alert-info').show();
                        $('.alert-info ul').empty()
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').append('<li>'+value+'</li>');
                        })
                    }
                });
            }
        });

        function deleteItemPhoto() {
            var item_id = $('#itemIdOfShownModal').val();
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type:'post',
                url:'{{ route('deleteItemPhoto') }}',
                data:{ item_id:item_id },
                success: function (data) {
                    console.log(data)
                    $('#edit_item_photo').attr('src', '');
                    set_item_values(item_id);
                    fetch_all_items_table();
                    fetch_item_norm_table(item_id);
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    console.log(json)
                    $('.alert-info').show();
                    $('.alert-info ul').empty()
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').append('<li>'+value+'</li>');
                    })
                }
            })
        }

        $('#addToNormModal').on('shown.bs.modal', function() {
            get_items_for_adding_to_norm();
        });

        function get_items_for_adding_to_norm() {
            $('#subItemUnits').empty().trigger("chosen:updated");
            $('#normItem').empty().trigger("chosen:updated");
            $('.alert-danger').hide();
            $('.alert-info').hide();
            $('.alert-success').hide();
            var item_id = $('#itemIdOfShownModal').val();
            $.ajax({
                type:'get',
                url:'{{ route('get_items_for_adding_to_norm') }}',
                data:{ item_id:item_id },
                success: function (data) {
                    $('#normItem').append('<option value=""></option>');
                    $.each(JSON.parse(data), function (i, val) {
                        $('#normItem').append('<option value="'+val.id+'">'+val.articula_new+' '+val.sap_code+' '+val.item_name+'</option>')
                    });

                    $('#normItem').trigger("chosen:updated");
                },
                error: function (request, status, error) {
                    alert('произошла ошибка');
                    console.log(error)
                }
            })

        }

        $('#normItem').on('change', function () {
            var item_id = $('#normItem  ').val();
            $('#subItemUnits').empty().trigger("chosen:updated");
            $.ajax({
                type:'get',
                url:'{{ route('get_item_units') }}',
                data:{ item_id:item_id },
                success: function (data) {
                    console.log(data)
                    $.each(JSON.parse(data), function (i, val) {
                        $('#subItemUnits').append('<option value="'+val.id+'">'+val.unit+'</option>')
                    });
                    $('#subItemUnits').trigger("chosen:updated");
                },
                error: function (request, status, error) {
                    alert('произошла ошибка');
                    console.log(error)
                }
            })
        });

        function addItemToNorm() {
            $('.alert-danger').hide();
            $('.alert-info').hide();
            $('.alert-success').hide();
            var parent_id = $("#itemIdOfShownModal").val();
            var item_id = $("#normItem").val();
            var unit = $( "#subItemUnits" ).val();
            var quantity = $( "input[name=normItemQuantity]" ).val();
            if (confirm('добавить?')) {
                if (quantity > 0) {
                    $.ajax({
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        type:'POST',
                        url:'{{route('add_item_to_norm')}}',
                        data:{ parent_id:parent_id, unit:unit, quantity:quantity, item_id:item_id},
                        success:function(data){
                            $("#closeAddToNormModal").click();
                            fetch_item_norm_table(parent_id);
                            $('.alert-info').show();
                            $('.alert-info ul').empty().append('<li>сохранено</li>');
                        },
                        error: function (request, status, error) {
                            var json = $.parseJSON(request.responseText);
                            console.log(json)
                            $('.alert-success').show();
                            $.each(json.errors, function (index, value) {
                                $('.alert-success ul').empty().append('<li>'+value+'</li>');
                            })
                        }
                    });
                }else{
                    alert('недопустимое значение!')
                }
            }
        }

        function remove_norm_item(child_id) {
            $('.alert-danger').hide();
            $('.alert-info').hide();
            $('.alert-success').hide();
            var parent_id = $('#itemIdOfShownModal').val();
            if (confirm('удалить?')){
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'post',
                    url:'{{ route('delete_child_item') }}',
                    data:{ parent_item_id:parent_id, child_item_id:child_id },
                    success: function (data) {
                        fetch_item_norm_table(parent_id);
                        $('.alert-info').show();
                        $('.alert-info ul').empty().append('<li>удален</li>');
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        $('.alert-info').show();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').empty().append('<li>'+value+'</li>');
                        })
                    }
                })
            }
        }

        function edit_item_norm_modal(child_id) {
            $('.alert-danger').hide();
            $('.alert-info').hide();
            $('.alert-success').hide();
            var item_id = $('#itemIdOfShownModal').val();
            $('#childIdForEditNormItem').val(child_id);
            $('#allNormItemQuantities').find('li').remove().end();
            console.log(item_id)
            console.log(child_id)
            $.ajax({
                type:'get',
                url:'{{ route('get_item_child_values') }}',
                data:{ parent_id:item_id, child_id:child_id },
                success: function (data) {
                    var json = JSON.parse(data);
                    var units = json.item;
                    var rep_item = json.rep_item;
                    console.log(json)
                    $.each(units, function (i, val) {
                        $('#allNormItemQuantities').append('<li class="list-group">'+val.unit+': <input type="number" step="0.0001" min="0" name="edit_quantity[]" id="" class="form-control" value="'+val.quantity+'"></li>')
                    });
                    $('#replacement_items').val(rep_item.id).trigger("chosen:updated");
                },
                error: function (request, status, error) {
                    alert('произошла ошибка');
                    console.log(request)
                }
            })
        }

        function updateNormItem() {
            var allowToSave = true;
            $('.alert-danger').hide();
            $('.alert-info').hide();
            $('.alert-success').hide();
            var child_id = $("#childIdForEditNormItem").val();
            var parent_id = $("#itemIdOfShownModal").val();
            var quantities = [];
            $('input[name^="edit_quantity"]').each(function() {
                if (this.value<=0) {
                    allowToSave = false;
                    alert('недопустимое значение');
                }else{
                    quantities.push($(this).val());
                }
            });
            console.log(quantities)
            if (allowToSave) {
                if (confirm('изменить?')) {
                    $.ajax({
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        type:'post',
                        url:"{{route('update_child_item')}}",
                        data:{ parent_id:parent_id, child_id:child_id, quantities:quantities},
                        success:function(data){
                            fetch_item_norm_table(parent_id);
                            $('#closeEditNormItemModal').click();
                            $('.alert-info').show();
                            $('.alert-info ul').empty().append('<li>Обнавлено</li>');
                        },
                        error: function (request, status, error) {
                            var json = $.parseJSON(request.responseText);
                            console.log(json)
                            $('#update_child_item_using_ajax').click()
                            $('.alert-success').show();
                            $.each(json.errors, function (index, value) {
                                $('.alert-success ul').empty().append('<li>'+value+'</li>');
                            })
                        }
                    });

                    add_replacement_item();
                }
            }
        }

        function get_replacement_items(child_id) {
            $('#replacement_items').val('').trigger('chosen:updated');
            var item_id = $('#itemIdOfShownModal').val();
            console.log(child_id);
            console.log(item_id);
            $('#replacement_items').val('').trigger('chosen:update');
            if (item_id!=null) {
                $.ajax({
                    type:'get',
                    url: '{{ route('getReplacementItems') }}',
                    data:{item_id:item_id, child_id:child_id},
                    success: function (data) {
                        var item = JSON.parse(data);
                        if (item['replace_items']!=null) {
                            $('#replacement_items').val(item['replace_items']).trigger('chosen:updated');
                        }
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        console.log(request)
                        $('.alert-info').show();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').empty().append('<li>'+value+'</li>');
                        })
                    }
                })
            }
        }

        function add_replacement_item(){
            var child_id = $("#childIdForEditNormItem").val();
            var parent_id = $("#itemIdOfShownModal").val();
            var selected_item = $('#replacement_items').val();
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type:'post',
                url: '{{ route('add_replacement_items') }}',
                data:{parent_id:parent_id, child_id:child_id, selected_item:selected_item},
                success: function (data) {
                    fetch_item_norm_table(parent_id);
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    console.log(request)
                    $('.alert-info').show();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').empty().append('<li>'+value+'</li>');
                    })
                }
            })
        }

        function delete_replacement_item(id) {
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type:'post',
                url: '{{ route('delete_replacement_items') }}',
                data:{id:id},
                success: function (data) {
                    $('#row_'+id).remove();
                    $('.alert-info').show();
                    $('.alert-info ul').empty().append('<li>'+data.data+'</li>');
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    console.log(request)
                    $('.alert-info').show();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').empty().append('<li>'+value+'</li>');
                    })
                }
            })
        }

        function delete_norm() {
            var item_id = $('#itemIdOfShownModal').val();
            if (confirm('удалить норму?')) {
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'post',
                    url: '{{ route('delete_norm') }}',
                    data:{item_id:item_id},
                    success: function (data) {
                        fetch_item_norm_table(item_id);
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        console.log(request)
                        $('.alert-info').show();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').empty().append('<li>'+value+'</li>');
                        })
                    }
                })
            }
        }

        $( "#show_filter_modal" ).click(function(){
            change_filtering_btn_text();
        });

        $("input:checkbox").on('change', function () {
            change_filtering_btn_text();
        });

        function change_filtering_btn_text() {
            var n = $('input:checked').length;
            if (n>0){
                $('#filter_items_btn').html('фильтр');
            }else{
                $('#filter_items_btn').html('выводить все');
            }
        }

        function filter_items() {
            $('#filter_modal').submit();
        }

        function replaceNormItem() {
            var parent_id = $("#itemIdOfShownModal").val();

            var main_item_id = $('#mainItemId').val();

            var replace_item_id = $('#replacedItemId').val();
            console.log(main_item_id)
            console.log(replace_item_id)
            if (replace_item_id == 0)
            {
                alert('выберите деталь')
            }else{
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'post',
                    url: '{{ route('replaceNormItem') }}',
                    data:{main_item_id:main_item_id, replace_item_id:replace_item_id, parent_id:parent_id},
                    success: function (data) {
                        $('#mainItemId').val(replace_item_id);
                        fetch_item_norm_table(parent_id);
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        console.log(request)
                        $('.alert-info').show();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').empty().append('<li>'+value+'</li>');
                        })
                    }
                })
            }

        }

        function setReplaceItemId(item_id) {
            $('#mainItemId').val(item_id);
        }
    </script>
@endsection

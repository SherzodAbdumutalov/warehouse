@section('additional_css')
    <style>
        .modal-body {
            max-height: calc(100vh - 200px);
            overflow-y: auto;
            padding-top: 0;
            margin-top: 0;
        }

        .modal-dialog {
            width: calc(100% - 100px);
        }
        input[type=checkbox]
        {
            /* Double-sized Checkboxes */
            -ms-transform: scale(2); /* IE */
            -moz-transform: scale(2); /* FF */
            -webkit-transform: scale(2); /* Safari and Chrome */
            -o-transform: scale(2); /* Opera */
            padding: 10px;
        }

        .table > tbody > tr > td{
            font-size: 12px;
            line-height: 20px;
        }

        .table > thead > tr > th {
            font-weight: bold;
        }

        #moving_requests_table tbody tr{
            cursor: pointer;
        }
    </style>
@endsection
<div class="panel panel-primary">

    <div class="panel-heading" style="padding: 1px 5px">
          Операции на линии
    </div>
    <div class="panel-body">
        <div class="alert alert-danger" style="display:none">
            <ul>

            </ul>
        </div>
        <div class="col-md-12 clearfix" style="margin-bottom: 10px; padding-left: 0; margin-left: 0">
            <div class="col-md-2" style="margin-bottom: 0; padding-left: 0; margin-left: 0">
                <select name="storage_id" id="storage_id" class="standardSelect" >
                    @foreach($lines as $line)
                        <option value="{{ $line->id }}">{{ $line->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <ul class="nav nav-tabs" id="operation_tab">
            <li class="active"><a data-toggle="tab" href="#byRequest"><i class="fas fa-clipboard-list"></i> запрос на перемещение<span class="badge"></span></a></li>
            <li><a data-toggle="tab" href="#byHand"><i class="fa fa-hand-paper-o"></i> ручная операция</a></li>
        </ul>
        <div class="tab-content">
            <div id="byHand" class="tab-pane fade">
                <div class="col-md-2">
                    <label for="operation"><b>операции</b></label>
                    <select name="operation" id="operation" class="standardSelect" >
                        {{--@foreach($operation_types as $type)--}}
                            {{--<option value="{{ $type->id }}" data-operation="{{ $type->operation }}">{{ $type->name }}</option>--}}
                        {{--@endforeach--}}
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="item"><b>единицы</b></label>
                    <select name="item" data-placeholder="item select" id="item" class="standardSelect chosen-choices ">
                    </select>
                </div>

                <div class="col-md-1">
                    <label for="amount"><b>кол-во</b></label><br>
                    <input type="number" name="amount" min="0" id="amount" class="form-control" style="height: 26px; display: block">
                </div>
                <div class="col-md-1">
                    <button class="btn btn-warning" type="button" style="margin-top: 30px; padding: 3px 15px" onclick="add_item()"><i class="fa fa-plus"></i> добавить</button>
                </div>

                <table id="line_items_table" class="table table-hover table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th style="padding-left: 0">артикул</th>
                        <th style="padding-left: 0">sap</th>
                        <th style="padding-left: 0">наименование</th>
                        <th style="padding-left: 0">кол-во</th>
                        <th style="padding-left: 0">остаток</th>
                        <th style="padding-left: 0">ед. изм.</th>
                        <th style="padding-left: 0"></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th style="padding-left: 0">артикул</th>
                        <th style="padding-left: 0">sap</th>
                        <th style="padding-left: 0">наименование</th>
                        <th style="padding-left: 0">кол-во</th>
                        <th style="padding-left: 0">остаток</th>
                        <th style="padding-left: 0">ед. изм.</th>
                        <th style="padding-left: 0"></th>
                    </tr>
                    </tfoot>
                </table>

                <div class="col-md-4">
                    <label for="description" style="font-size: 16px; font-weight: bold">описаниe</label>
                    <input name="description" id="description" maxlength="50" style="resize: none; height: 26px" placeholder="вводите текст... (макс 50 символов)" class="form-control">
                </div>
                <div class="col-md-3">
                    <div style="float: left; padding-top: 30px">
                        <button class="btn btn-warning " type="submit" style="font-size: 12px; float: left" id="save_in_out_btn" onclick="save_items()"><i class="glyphicon glyphicon-floppy-save" ></i> сохранить</button>
                        &nbsp;<button class="btn btn-danger " type="submit" style="font-size: 12px; float: right" onclick="clear_sessionStorage()"><i class="glyphicon glyphicon-remove-circle"></i> очистить</button>
                    </div>
                </div>
            </div>
            <div id="byRequest" class="tab-pane fade in active">
                <a href="{{ route('createMovingRequest') }}" class="btn btn-info " style="font-size: 12px; margin-top: 10px"><i class="fa fa-plus"></i> добавить</a>
                <table id="moving_requests_table" class="table table-hover table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>статус</th>
                        <th>дата</th>
                        <th>с </th>
                        <th>склад</th>
                        <th>примечание</th>
                        <th>автор</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>№</th>
                        <th>статус</th>
                        <th>дата</th>
                        <th>с </th>
                        <th>склад</th>
                        <th>примечание</th>
                        <th>автор</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>

        <div id="show_detail" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <p style="color: red; font-weight: bold; display: inline-block" id="accepted"></p>
                        <button type="button" class="close" data-dismiss="modal" id="close_detail_modal"><span style="font-size: 24px">&times;</span></button>
                    </div>
                    <div class="alert alert-info" style="display: none; line-height: 12px; padding: 0">
                        <ul>

                        </ul>
                    </div>
                    <div class="modal-body">
                        <div id="detailTab" class="tab-pane fade in active">
                            <a href="" id="export_to_pdf" target="_blank" hidden>pdf</a>
                            <table id="detail_table" class="table table-hover table-bordered" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>артикул</th>
                                    <th>стр арт.</th>
                                    <th>наименование</th>
                                    <th>SAP код</th>
                                    <th>кол-во</th>
                                    <th>остаток</th>
                                    <th>ед. изм.</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>артикул</th>
                                    <th>стр арт.</th>
                                    <th>наименование</th>
                                    <th>SAP код</th>
                                    <th id="total">кол-во</th>
                                    <th>остаток</th>
                                    <th>ед. изм.</th>
                                    <th></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <form >
                            <input type="hidden" name="moving_request_id" id="moving_request_id" value="0"/>
                            <button type="button" class="btn btn-warning"  onclick="close_moving_request_function()" id="close_moving_request" style="display: none">закрыть</button>
                            {{--<button type="button" class="btn btn-danger"  onclick="delete_moving_request_function()" id="delete_moving_request" style="display: none">удалить заявку</button>--}}
                        </form>
                    </div>
                </div>

            </div>
        </div>

        <div id="show_moving_request_detail_before_storing_modal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <p style="color: blue; font-weight: bold; display: inline-block; font-size: 28px" id="confirmationApply">создать запрос на перемещение в склад?</p>
                        <button type="button" class="close" data-dismiss="modal" onclick="clear_sessionStorage()"><span style="font-size: 24px">&times;</span></button>
                    </div>
                    <div class="alert alert-success" style="display: none; line-height: 12px; padding: 0">
                        <ul>

                        </ul>
                    </div>
                    <div class="modal-body">
                        <table id="moving_request_table_before_storing" class="table table-hover table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th style="padding-left: 0">артикул</th>
                                <th style="padding-left: 0">sap</th>
                                <th style="padding-left: 0">наименование</th>
                                <th style="padding-left: 0">кол-во</th>
                                <th style="padding-left: 0">остаток</th>
                                <th style="padding-left: 0">ед. изм.</th>
                                <th style="padding-left: 0"></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th style="padding-left: 0">артикул</th>
                                <th style="padding-left: 0">sap</th>
                                <th style="padding-left: 0">наименование</th>
                                <th style="padding-left: 0">кол-во</th>
                                <th style="padding-left: 0">остаток</th>
                                <th style="padding-left: 0">ед. изм.</th>
                                <th style="padding-left: 0"></th>
                            </tr>
                            </tfoot>
                        </table>
                        <div class="col-md-4">
                            <label for="moving_request_description">примечания для запроса</label>
                            <input type="text" name="moving_request_description" id="moving_request_description" maxlength="50" style="resize: none; height: 26px" placeholder="вводите текст... (макс 50 символов)" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <form >
                            <button type="button" class="btn btn-warning" onclick="store_moving_request_before_storing()" id="store_moving_request_btn" style="display: none">сохранить</button>
                            <button type="button" data-dismiss="modal" class="btn btn-danger" id="close_moving_request_detail_before_storing_modal" onclick="clear_sessionStorage()" style="">отмена</button>
                            {{--<button type="button" class="btn btn-danger"  onclick="delete_moving_request_function()" id="delete_moving_request" style="display: none">удалить заявку</button>--}}
                        </form>
                    </div>
                </div>

            </div>
        </div>


        {{--edit--}}
        <div id="edit_request_modal" class="modal fade" role="dialog">
            <div class="modal-dialog"  style="width: 30%">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        изменить
                        <button type="button" class="close" data-dismiss="modal" id="close_edit_detail_modal"><span style="font-size: 24px">&times;</span></button>
                    </div>
                    <div class="alert alert-success" style="display:none; color: black">
                        <ul>

                        </ul>
                    </div>
                    <form action="" method="post">
                        <div class="modal-body">
                            <input type="hidden" name="edit_moving_request_id" id="edit_moving_request_id">
                            <label for="description1" style="font-size: 25px; font-weight: bold">примечание</label>
                            <textarea name="description1" id="description1"  rows="10" style="resize: none" class="form-control"></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-warning" onclick="update_moving_request()"><i class="fa fa-save"></i> сохранить</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
        <div id="edit_moving_request_item_amount" class="modal fade" role="dialog">
            <div class="modal-dialog"  style="width: 20%; height: 10%">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        изменить
                        <button type="button" class="close" data-dismiss="modal" id="close_edit_moving_request_item_amount"><span style="font-size: 24px">&times;</span></button>
                    </div>
                    <div class="alert alert-success" style="display:none; color: black">
                        <ul>

                        </ul>
                    </div>
                    <form action="" method="post">
                        <div class="modal-body">
                            <input type="hidden" name="edit_moving_request_list_id" id="edit_moving_request_list_id">
                            <label for="edit_moving_request_list_amount">кол-во</label>
                            <input type="number" name="edit_moving_request_list_amount" id="edit_moving_request_list_amount" class="form-control">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-warning" onclick="update_moving_request_list()"><i class="fa fa-save"></i> сохранить</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

<div id="edit_automatically_created_moving_request_list_amount_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm" style="width: 20%">
        <div class="modal-content" >
            <div class="modal-header">
                изменить кол-во
                <button type="button" class="close" data-dismiss="modal" style="font-size: 38px" id="close_automatically_created_moving_request_list_amount_modal">&times;</button>
            </div>
            <div class="modal-body">
                <label for="edit_automatically_created_moving_request_list_amount">кол-во</label>
                <input type="number" name="amount" id="edit_automatically_created_moving_request_list_amount" step="0.0001" class="form-control">
                <input type="hidden" name="key" id="edit_automatically_created_moving_request_list_key" value="">
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-warning" onclick="save_moving_request_item_amount()"><i class="fa fa-save"></i> Сохранить</button>
            </div>
        </div>
    </div>
</div>

{{--edit amount--}}
<div id="edit_amount_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm" style="width: 20%">
        <div class="modal-content" >
            <div class="modal-header">
                изменить кол-во
                <button type="button" class="close" data-dismiss="modal" style="font-size: 38px" id="close_modal">&times;</button>
            </div>
            <div class="modal-body">
                <label for="edit_amount">кол-во</label>
                <input type="number" name="amount" id="edit_amount" step="0.0001" class="form-control">
                <input type="hidden" name="key" id="edit_key" value="">
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-warning" onclick="save_amount()"><i class="fa fa-save"></i> Сохранить</button>
            </div>
        </div>
    </div>
</div>

<div id="show_moving_request_detail" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <p style="color: red; font-weight: bold; display: inline-block" id="accepted"></p>
                <button type="button" class="close" data-dismiss="modal" id="close_detail_modal"><span style="font-size: 24px">&times;</span></button>
            </div>
            <div class="alert alert-danger" style="display:none">
                <ul>

                </ul>
            </div>
            <div class="modal-body">
                <table id="detail_table" class="table table-hover table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>артикул</th>
                        <th>стр арт.</th>
                        <th>наименование</th>
                        <th>SAP код</th>
                        <th>кол-во</th>
                        <th>остаток</th>
                        <th>ед. изм.</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>артикул</th>
                        <th>стр арт.</th>
                        <th>наименование</th>
                        <th>SAP код</th>
                        <th id="total">кол-во</th>
                        <th>остаток</th>
                        <th>ед. изм.</th>
                    </tr>
                    </tfoot>
                </table>
                <form>
                    <input type="hidden" name="moving_request_id_for_accept" id="moving_request_id_for_accept" value="">
                    <button class="btn btn-warning" type="button" id="accept_btn" style="display: none; float: right" onclick="accept_defective()"><i class="fas fa-clipboard-check"></i> принять</button>
                </form>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>
@section('additionalLibrary')
    {{--dataTables library--}}
    <script>
        $(document).ready(function() {
            fetch_moving_request_table();
            $('a[data-toggle="tab"]').on('shown.bs.tab', function () {
                $('#moving_request_table').DataTable().columns.adjust().draw(false);
                fetch_moving_request_table();
                $('#line_items_table').DataTable().columns.adjust().draw(false);
            } );
            if(sessionStorage.getItem('line_input_output_items')!=null){
                $('#operation').val(sessionStorage.getItem('line_input_output_items'));
                $('#operation').prop('disabled', true).trigger('chosen:updated');
            }else{
                $('#operation').prop('disabled', false).trigger("chosen:updated");
            }

            if(sessionStorage.getItem('last_selected_operation_id')!=null){
                $('#operation').val(sessionStorage.getItem('last_selected_operation_id')).trigger('chosen:updated')
            }

            $('#line_items_table').DataTable( {
                columnDefs: [
                    { orderable: false, targets: [0, 1, 2, 3, 4, 5, 6] }
                ],
                order: false,
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение от _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "",
                    "infoFiltered": "(отфильтровано из _MAX_ записей)",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                paging: false,
                bPaginate: false,
                "columns": [
                    { "width": "8%" },
                    { "width": "8%" },
                    { "width": "50%" },
                    { "width": "5%" },
                    { "width": "5%" },
                    { "width": "5%" },
                    { "width": "5%" },
                ],
                scrollY: "50vh",
                scrollCollapse: true
            });

            var groupColumn = 7;
            $('#moving_request_table_before_storing').DataTable( {
                columnDefs: [
                    { orderable: false, targets: [0, 1, 2, 3, 4, 5, 6] },
                    { visible: false, targets: [groupColumn]}
                ],
                order: [[ groupColumn, 'asc' ]],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение от _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "",
                    "infoFiltered": "(отфильтровано из _MAX_ записей)",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                stateSave: true,
                paging: false,
                bPaginate: false,
                "columns": [
                    { "width": "8%" },
                    { "width": "8%" },
                    { "width": "50%" },
                    { "width": "5%" },
                    { "width": "5%" },
                    { "width": "5%" },
                    { "width": "5%" },
                    { "width": "5%" },
                ],
                drawCallback: function ( settings ) {
                    var api = this.api();
                    var rows = api.rows( {page:'current'} ).nodes();
                    var last=null;

                    api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                        if ( last !== group ) {
                            $(rows).eq( i ).before(
                                '<tr class="group"><td colspan="7">'+group+'</td></tr>'
                            );

                            last = group;
                        }
                    } );
                },
                scrollY: "50vh",
                scrollCollapse: true
            });

            $('input[name=amount]').on('keydown', function (e) {
                if(e.keyCode==13){
                    add_item();
                }
            });
            draw_table();
            getLineItems();
            getOperationsByStorage();
            $('#moving_requests_table').on('dblclick', 'tr', function (e) {
                var id = $(this).data('id');
                $('#show_detail').modal("show");
                show_detail(id);
            });
            $('#moving_requests_table tbody').on( 'click', 'tr', function () {
                $(this).removeClass('selected');

                if ( $(this).hasClass('selected') ) {
                    $(this).removeClass('selected');
                }
                else {
                    $('#moving_requests_table').DataTable().$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            } );
        } );
    </script>
    <script>
        $('#storage_id').on('change', function () {
            getLineItems();
            fetch_moving_request_table();
            getOperationsByStorage();
        });
        $('#operation').on('change', function () {
            getLineItems();
        });

        function getOperationsByStorage() {
            var storage_id = $('#storage_id').val();
            $('#operation').empty().trigger("chosen:updated");
            $.ajax({
                url: "{{ route('getOperationsByStorage') }}",
                type: "get",
                data: {storage_id:storage_id},
                success: function(data){
                    $.each(JSON.parse(data), function (index, value) {
                        $('#operation').append('<option value="'+value.id+'" data-operation="'+value.operation+'">'+value.name+'</option>');
                    });
                    $('#operation').trigger("chosen:updated");
                },
                error: function (request, status, error) {
                    $('#loading').hide();
                    var json = $.parseJSON(request.responseText);
                    console.log(json);
                    $('.alert-info').show();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').empty().append('<li>'+value+'</li>');
                    })
                }
            })
        }

        function getLineItems() {
            var operation = $('#operation').val();
            var storage_id = $('#storage_id').val();
            $.ajax({
                url: "{{ route('getLineItems') }}",
                type: "get",
                data: {operation:operation, storage_id:storage_id},
                success: function(data){
                    $('#item').empty().trigger("chosen:updated");
                    $.each(JSON.parse(data), function (index, value) {
                        $('#item').append('<option value="'+value.id+'">'+value.articula_new+' '+value.item_name+' '+value.sap_code+'</option>');
                    })
                    $('#item').trigger("chosen:updated");
                },
                error: function (request, status, error) {
                    $('#loading').hide();
                    var json = $.parseJSON(request.responseText);
                    console.log(json);
                    $('.alert-info').show();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').empty().append('<li>'+value+'</li>');
                    })
                }
            })
        }

        function add_item() {
            $('#loading').show();
            var storage_id = $('#storage_id').val();
            var operation = $('#operation').val();
            var item_id = $('#item').val();
            var str_amount = $('#amount').val();
            var amount = parseFloat(str_amount.replace(',', '.'));
            if(amount>0){
                $('#operation').prop('disabled', true).trigger("chosen:updated");
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "{{ route('collect_line_item_input_output') }}",
                    type: "post",
                    data: {item_id:item_id, amount:amount, operation:operation, storage_id:storage_id},
                    success: function(data){
                        console.log(data)
                        sessionStorage.setItem('last_selected_operation_id', operation);
                        $('#amount').val('');
                        $('#loading').hide();
                        var items = $.parseJSON(data);
                        items.forEach(function (value, index) {
                            if(is_exist(value.item_id, value.amount)){
                                draw_table();
                            }else{
                                collect_to_array(value);
                            }
                        })
                    },
                    error: function (request, status, error) {
                        $('#loading').hide();
                        var json = $.parseJSON(request.responseText);
                        console.log(json);
                        $('.alert-info').show();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').empty().append('<li>'+value+'</li>');
                        })
                    }
                })
            }else{
                $('#loading').hide();
                alert('вводите кол-во!')
            }
        }

        function draw_table() {
            var operation = $('#operation').find(':selected').data('operation');
            console.log(operation)
            $('#line_items_table').DataTable().clear().draw(false);
            var items = $.parseJSON(sessionStorage.getItem('line_input_output_items'));
            if (items!=null){
                items.forEach(function (value, key) {
                    if ((operation == -1)){
                        if(value.amount>value.remaining){
                            $('#save_in_out').prop('disabled', true).hide();
                        }else{
                            $('#save_in_out').prop('disabled', false).show();
                        }
                    } else {
                        $('#save_in_out').prop('disabled', false).show();
                    }
                    var trow = $('#line_items_table').DataTable().row.add([
                        value.articula_new,
                        value.sap_code,
                        value.item_name,
                        parseFloat(value.amount),
                        value.remaining,
                        value.unit,
                        '<button type="button" class="btn btn-warning " data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#edit_amount_modal" onclick="set_data_for_edit('+key+','+value.amount+')" style="font-size: 11px"><i class="fa fa-edit"></i></button> <button type="button" class="btn btn-danger " onclick="delete_item('+key+')" style="font-size: 11px" ><i class="fa fa-trash"></i></button>'
                    ]).node();
                    trow.id = 'row_'+key;
                    $('#line_items_table').DataTable().draw(false);
                    if (Number(value.remaining)<Number(value.amount) && (operation==-1)){
                        $(trow).find('td:eq(4)').css('background-color', 'red');
                    }else {
                        $(trow).find('td:eq(4)').css('background-color', '');
                    }
                })
            }else{
                $('#save_in_out').prop('disabled', true).hide();
            }
            getCollectedItemsWithStorage();
            $('#loading').hide();
        }

        function getCollectedItemsWithStorage() {
            var from_storage_id = $('#storage_id').val();
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: "{{ route('collectItemsByStorageForMMovingRequest') }}",
                type: "post",
                data: {items:sessionStorage.getItem('line_input_output_items'), storage_id:from_storage_id},
                success: function(data){
                    sessionStorage.setItem('automaticallyCreateMovingRequestItemsList', data);
                    $('#loading').hide();
                },
                error: function (request, status, error) {
                    $('#loading').hide();
                    var json = $.parseJSON(request.responseText);
                    console.log(json);
                    $('.alert-info').show();
                    $.each(json.errors, function (index, value) {
                        $('.alert-info ul').empty().append('<li>'+value+'</li>');
                    })
                }
            })
        }

        function draw_moving_request_table_before_storing() {
            $('#moving_request_table_before_storing').DataTable().clear().draw(false);
            var items = JSON.parse(sessionStorage.getItem('automaticallyCreateMovingRequestItemsList'));
            console.log(items);
            if (items!=null){
                items.forEach(function (value, key) {
                    if ((operation == -1)){
                        if(value.amount>value.remaining){
                            $('#store_moving_request_btn').prop('disabled', true).hide();
                        }else{
                            $('#store_moving_request_btn').prop('disabled', false).show();
                        }
                    } else {
                        $('#store_moving_request_btn').prop('disabled', false).show();
                    }
                    var trow = $('#moving_request_table_before_storing').DataTable().row.add([
                        value.articula_new,
                        value.sap_code,
                        value.item_name,
                        parseFloat(value.amount),
                        value.remaining,
                        value.unit,
                        '<button type="button" class="btn btn-warning " data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#edit_automatically_created_moving_request_list_amount_modal" onclick="set_data_for_edit_moving_request_item('+key+','+value.amount+')" style="font-size: 11px"><i class="fa fa-edit"></i></button> <button type="button" class="btn btn-danger " onclick="delete_moving_request_item('+key+')" style="font-size: 11px" ><i class="fa fa-trash"></i></button>',
                        value.storage_name
                    ]).node();
                    trow.id = 'modal_table_row_'+key;
                    $('#moving_request_table_before_storing').DataTable().draw(false);
                    if (Number(value.remaining)<Number(value.amount)){
                        $('#store_moving_request_btn').hide();
                        $(trow).find('td:eq(4)').css('background-color', 'red');
                    }else {
                        $(trow).find('td:eq(4)').css('background-color', '');
                    }
                })
            }else{
                $('#close_automatically_created_moving_request_list_amount_modal').click();
                $('#store_moving_request_btn').prop('disabled', true).hide();
            }
            $('#loading').hide();
        }

        function collect_to_array(item) {
            var old_items = [];
            if(sessionStorage.getItem('line_input_output_items')!=null){
                old_items = $.parseJSON(sessionStorage.getItem('line_input_output_items'))
            }
            var data = {
                'item_id':item.item_id,
                'articula_new':item.articula_new,
                'sap_code':item.sap_code,
                'item_name':item.item_name,
                'amount':parseFloat(item.amount),
                'remaining':item.remaining,
                'unit':item.unit,
                'unit_id':item.unit_id,
                'storage_id':item.storage_id,
            };
            old_items.push(data);
            sessionStorage.setItem('line_input_output_items', JSON.stringify(old_items));
            draw_table();
        }

        function clear_sessionStorage() {
            $('#operation').prop('disabled', false).trigger("chosen:updated");
            sessionStorage.removeItem('line_input_output_items');
            sessionStorage.removeItem('automaticallyCreateMovingRequestItemsList');
            sessionStorage.removeItem('last_selected_operation_id');
            draw_table();
            draw_moving_request_table_before_storing();
        }

        function is_exist(item_id, amount) {
            var items = $.parseJSON(sessionStorage.getItem('line_input_output_items'));
            if(items!=null){
                for (var i=0; i<items.length; i++){
                    if(item_id==items[i].item_id){
                        items[i].amount+=parseFloat(amount);
                        sessionStorage.setItem('line_input_output_items', JSON.stringify(items));
                        return true;
                    }
                }
            }
            return false;
        }

        function delete_item(key) {
            var items = $.parseJSON(sessionStorage.getItem('line_input_output_items'));
            items.splice(key, 1);
            sessionStorage.setItem('line_input_output_items', JSON.stringify(items));
            draw_table();
        }

        function set_data_for_edit(key, amount) {
            $('#edit_amount').val(amount);
            $('#edit_key').val(key);
        }

        function save_amount() {
            var amount = $('#edit_amount').val();
            var key = $('#edit_key').val();
            var items = $.parseJSON(sessionStorage.getItem('line_input_output_items'));
            if(amount>0){
                items[key]['amount']=amount;
                sessionStorage.setItem('line_input_output_items', JSON.stringify(items));
                draw_table();
                $('#close_modal').click();
            }else{
                alert('недопустимое значение!!');
            }
        }


        function delete_moving_request_item(key) {
            var items = $.parseJSON(sessionStorage.getItem('automaticallyCreateMovingRequestItemsList'));
            items.splice(key, 1);
            sessionStorage.setItem('automaticallyCreateMovingRequestItemsList', JSON.stringify(items));
            draw_moving_request_table_before_storing();
        }

        function set_data_for_edit_moving_request_item(key, amount) {
            $('#edit_automatically_created_moving_request_list_amount').val(amount);
            $('#edit_automatically_created_moving_request_list_key').val(key);
        }

        function save_moving_request_item_amount() {
            var amount = $('#edit_automatically_created_moving_request_list_amount').val();
            var key = $('#edit_automatically_created_moving_request_list_key').val();
            var items = $.parseJSON(sessionStorage.getItem('automaticallyCreateMovingRequestItemsList'));
            if(amount>0){
                items[key]['amount']=amount;
                sessionStorage.setItem('automaticallyCreateMovingRequestItemsList', JSON.stringify(items));
                draw_moving_request_table_before_storing();
                $('#close_automatically_created_moving_request_list_amount_modal').click();
            }else{
                alert('недопустимое значение!!');
            }
        }

        $('#show_moving_request_detail_before_storing_modal').on('shown.bs.modal', function() {
            draw_moving_request_table_before_storing();
            sessionStorage.removeItem('line_input_output_items');
        });
        $('#show_moving_request_detail_before_storing_modal').on('hidden.bs.modal', function() {
            draw_table();
        });
        function save_items() {
            if (confirm('сохранить?')) {
                $('#save_in_out_btn').hide();
                var items = sessionStorage.getItem('line_input_output_items');
                var desc = $('#description').val();
                var operation_id = $('#operation').val();
                var storage_id = $('#storage_id').val();
                if (items!=null){
                    $.ajax({
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        type:'POST',
                        url:"{{route('save_line_inputs_outputs_items')}}",
                        data:{items:items, description:desc, operation_id:operation_id, storage_id:storage_id},
                        success: function (data) {
                            console.log($.parseJSON(data));
                            $('#save_in_out_btn').show();
                            $('#description').val('');
                            draw_table();
                            clear_sessionStorage();
                            // if (operation_id == 8) {
                            //     $('#show_moving_request_detail_before_storing_modal').modal('show');
                            // }else{
                            //     clear_sessionStorage();
                            //     draw_moving_request_table_before_storing();
                            // }
                            // clear_sessionStorage();
                            $('.alert-info').show();
                            $('.alert-info ul').empty().append('<li>производство сохранено</li>');
                        },
                        error: function (request, status, error) {
                            $('#loading').hide();
                            var json = $.parseJSON(request.responseText);
                            console.log(json);
                            $('.alert-info').show();
                            $.each(json.errors, function (index, value) {
                                $('.alert-info ul').empty().append('<li>'+value+'</li>');
                            })
                        }
                    })
                }else{
                    $('.alert-info').show();
                    $('.alert-info ul').empty().append('<li>внесите данные!</li>');
                }
            }
        }

        function store_moving_request_before_storing() {
            if (confirm('сохранить?')) {
                $('#store_moving_request_btn').hide();
                var items = sessionStorage.getItem('automaticallyCreateMovingRequestItemsList');
                var desc = $('#moving_request_description').val();
                var from_storage_id = $('#storage_id').val();
                if (items.length>0){
                    $.ajax({
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        type:'POST',
                        url:"{{route('saveMovingRequestItemsByStorage')}}",
                        data:{items:items, description:desc, from_storage_id:from_storage_id},
                        success: function (data) {
                            $('#close_moving_request_detail_before_storing_modal').click();
                            $('#description').val('');
                            fetch_moving_request_table();
                            clear_sessionStorage();
                            $('.alert-info ul').append('<li>запрос сохранено</li>');
                        },
                        error: function (request, status, error) {
                            $('#loading').hide();
                            var json = $.parseJSON(request.responseText);
                            console.log(json);
                            $('.alert-info').show();
                            $.each(json.errors, function (index, value) {
                                $('.alert-info ul').empty().append('<li>'+value+'</li>');
                            })
                        }
                    })
                }
            }
        }

    //    moving request moving request
        function fetch_moving_request_table() {
            var storage_id = $('#storage_id').val();
            $("#moving_requests_table").DataTable().destroy();
            var table = $('#moving_requests_table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    "url": "{{ route('getMovingRequest') }}",
                    "type": "post",
                    "data":{storage_id:storage_id},
                    "error": function (data) {
                        console.log(data);
                        return data.data;
                    }
                },
                columns: [
                    { "width": "1%", "data": "id" },
                    { "width": "1%", "data": "state" },
                    { "width": "4%", "data": "create_time" },
                    { "width": "10%", "data": "from_storage" },
                    { "width": "8%", "data": "to_storage" },
                    { "width": "10%", "data": "description" },
                    { "width": "1%", "data": "applier" },
                ],
                pageLength:100,
                lengthMenu:[[100, 500, 1000], [100, 500, 1000]],
                columnDefs:[
                    { orderable: false, targets: [6] }
                ],
                stateSave:true,
                order: [[ 2, "desc" ]],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение от _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "",
                    "infoFiltered": "(отфильтровано из _MAX_ записей)",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                createdRow: function( row, data, dataIndex ) {
                    $(row).attr('data-id', data.id);
                },
                drawCallback: function (data) {
                    this.api().columns([1,3,4,6]).every( function () {
                        var column = this;
                        var select = $('<select><option value="">--все--</option></select>')
                            .appendTo( $(column.footer()).empty() )
                            .on( 'change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val, true, false )
                                    .draw();
                            } );
                        for (var i=0; i<data.json.filteredData[column[0]].length; i++){
                            if (typeof data.json.filters[column[0]] !== "undefined") {
                                select.append( '<option value="'+data.json.filteredData[column[0]][i]+'" selected>'+data.json.filteredData[column[0]][i]+'</option>' )
                            }else {
                                select.append( '<option value="'+data.json.filteredData[column[0]][i]+'">'+data.json.filteredData[column[0]][i]+'</option>' )
                            }
                        }
                    } );
                },
                scrollY: "60vh",
                scrollCollapse: true
            });
        }

        function show_detail(moving_request_id) {
            $('#export_to_pdf').attr('href', '{{ \Illuminate\Support\Facades\URL::route('moving_requests_invoice_generate') }}/'+moving_request_id);
            $('#loading').show();
            $('#moving_request_id').val(moving_request_id);
            $('#delete_moving_request').hide();
            $('#close_moving_request').hide();
            $('.alert-info').hide();
            $('.alert-danger').hide();
            $('.alert-success').hide();
            $('#show_detail').on('shown.bs.modal', function() {
                fetch_detail_table();
            });
        }

        function fetch_detail_table() {
            var moving_request_id = $('#moving_request_id').val();
            $('#detail_table').DataTable().destroy();
            var table = $('#detail_table').DataTable({
                ajax: {
                    "url": "{{ route('getMovingRequestList') }}",
                    "type": "get",
                    "data": {moving_request_id:moving_request_id},
                    "dataSrc": function (data) {
                        if (!data['hasPerm']) {
                            $('#delete_moving_request').hide();
                            $('#close_moving_request').hide();
                        }else{
                            if (data['requestData'].state==1){
                                $('#delete_moving_request').show();
                                $('#close_moving_request').show();
                            } else {
                                $('#delete_moving_request').hide();
                                $('#close_moving_request').hide();
                            }
                        }
                        if (data['requestData'].state==1){
                            $('#accepted').text('№'+moving_request_id).css('color', 'blue');
                        } else {
                            $('#accepted').text('№'+moving_request_id).css('color', 'red');
                        }
                        $('#loading').hide();
                        $(table.column(4).footer()).text(' итог: '+data['total']);

                        return data.data;
                    }
                },
                columnDefs: [
                    { orderable: false, targets: [7] }
                ],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "_TOTAL_ записей",
                    "infoEmpty": "нет записей",
                    "infoFiltered": "(_MAX_)",
                    "search": "<i class='fa fa-search' style='float: left'></i>"
                },
                columns:[
                    {'data':'articula_new', 'width':'10%'},
                    {'data':'articula_old', 'width':'10%'},
                    {'data':'item_name', 'width':'30%'},
                    {'data':'sap_code', 'width':'10%'},
                    {'data':'amount', 'width':'6%'},
                    {'data':'remaining', 'width':'10%'},
                    {'data':'unit', 'width':'6%'},
                    {'data':'edit_btn', 'width':'4%'}
                ],
                paging: false,
                bPaginate: false,
                dom: 'Blfrtip',
                buttons: [
                    {
                        text: '<i class="glyphicon glyphicon-print" style="font-size: 18px"></i>',
                        action: function ( e, dt, node, config ) {
                            window.open(document.getElementById('export_to_pdf').href, '_blank');
                        }

                    },
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5, 6]
                        },
                        text: '<i class="fa fa-file-excel-o"></i>'

                    }
                ],
                scrollY: "50vh",

            });
        }

        function delete_moving_request_function(){
            if (confirm('удалить')) {
                var moving_request_id = $('#moving_request_id').val()
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'POST',
                    url:"{{route('deleteMovingRequest')}}",
                    data:{moving_request_id:moving_request_id},
                    success: function (data) {
                        $('#close_detail_modal').click();
                        fetch_moving_request_table();
                        $('.alert-danger').show();
                        $('.alert-danger ul').empty().append('<li>удален</li>')
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText)
                        console.log(json)
                        $('.alert-info').show();
                        $('.alert-info ul').empty();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').append('<li>'+value+'</li>');
                        })
                    }
                })
            }
        }

        function close_moving_request_function() {
            if (confirm('закрыть')) {
                var moving_request_id = $('#moving_request_id').val()
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'POST',
                    url:"{{route('closeMovingRequest')}}",
                    data:{moving_request_id:moving_request_id},
                    success: function (data) {
                        $('#close_detail_modal').click();
                        fetch_moving_request_table();
                        $('.alert-danger').show();
                        $('.alert-danger ul').empty().append('<li>закрыт</li>')
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText)
                        console.log(json)
                        $('.alert-info').show();
                        $('.alert-info ul').empty();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').append('<li>'+value+'</li>');
                        })
                    }
                })
            }
        }

        function edit_moving_request_item_amount(id, amount) {
            $('#edit_moving_request_list_id').val(id);
            $('#edit_moving_request_list_amount').val(amount);
        }

        function set_data_for_edit_request(moving_request_id, description) {
            $('#edit_moving_request_id').val(moving_request_id)
            $('textarea#description').val(description)
        }

        function update_moving_request() {
            var moving_request_id = $('#edit_moving_request_id').val()
            var desc = $('textarea#description').val()
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: "{{ route('updateMovingRequest') }}",
                type: "post",
                data: {moving_request_id:moving_request_id, description:desc},
                success: function (data) {
                    fetch_moving_request_table();
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    $('.alert-danger').show();
                    $.each(json.errors, function (index, value) {
                        $('.alert-danger ul').empty().append('<li>'+value+'</li>');
                    })
                }
            })
        }

        function update_moving_request_list() {
            var moving_request_list_id = $('#edit_moving_request_list_id').val();
            var edit_amount = $('#edit_moving_request_list_amount').val();
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: "{{ route('updateMovingRequestList') }}",
                type: "post",
                data: {moving_request_list_id:moving_request_list_id, edit_amount:edit_amount},
                success: function (data) {
                    $('#close_edit_moving_request_item_amount').click();
                    fetch_detail_table();
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    console.log(json)
                    $('.alert-danger').show();
                    $.each(json.errors, function (index, value) {
                        $('.alert-danger ul').empty().append('<li>'+value+'</li>');
                    })
                }
            })
        }
        function delete_moving_request_list(id) {
            var moving_request_list_id = id;
            if (confirm('удалить?')) {
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "{{ route('deleteMovingRequestList') }}",
                    type: "post",
                    data: {moving_request_list_id:moving_request_list_id},
                    success: function (data) {
                        fetch_detail_table();
                        $('.alert-info').show();
                        $('.alert-info ul').empty().append('<li>удален</li>');
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText);
                        console.log(json)
                        $('.alert-info').show();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').empty().append('<li>'+value+'</li>');
                        })
                    }
                })
            }
        }
    </script>
@endsection
</div>
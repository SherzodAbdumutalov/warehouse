@section('additional_css')
    <style>
        #defective_table tbody tr{
            cursor: pointer;
        }
    </style>
@endsection
<div class="panel panel-primary">
    <div class="panel-heading" style="padding: 1px 5px">
        все акты
    </div>
    <div class="alert alert-danger" style="display: none;">
        <ul>

        </ul>
    </div>
    <div class="panel-body">
        <a href="{{ route('create_defective') }}" class="btn btn-info " style="font-size: 12px"><i class="fa fa-plus"></i> создать</a>
        <table id="defective_table" class="table table-hover table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>№</th>
                <th>статус</th>
                <th>дата запроса</th>
                <th>отправитель</th>
                <th>получатель</th>
                <th>примечание</th>
                <th>автор</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
            <tr>
                <th>№</th>
                <th>статус</th>
                <th>дата запроса</th>
                <th>отправитель</th>
                <th>получатель</th>
                <th>примечание</th>
                <th>автор</th>
            </tr>
            </tfoot>
        </table>
    </div>


    <div id="show_detail" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <p style="color: red; font-weight: bold; display: inline-block" id="accepted"></p>
                    <button type="button" class="close" data-dismiss="modal" id="close_detail_modal"><span style="font-size: 24px">&times;</span></button>
                </div>
                <div class="alert alert-info" style="display: none; line-height: 12px; padding: 0">
                    <ul>

                    </ul>
                </div>
                <div class="modal-body">
                    <div id="detailTab" class="tab-pane fade in active">
                        <a href="" id="export_to_pdf" target="_blank" hidden>pdf</a>
                        <table id="detail_table" class="table table-hover table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>артикул</th>
                                <th>стр арт.</th>
                                <th>наименование</th>
                                <th>SAP код</th>
                                <th>кол-во</th>
                                <th>принято</th>
                                <th>остаток</th>
                                <th>ед. изм.</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>артикул</th>
                                <th>стр арт.</th>
                                <th>наименование</th>
                                <th>SAP код</th>
                                <th id="total">кол-во</th>
                                <th>принято</th>
                                <th>остаток</th>
                                <th>ед. изм.</th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <form >
                        <input type="hidden" name="defective_id" id="defective_id" value="0">
                        <button type="button" class="btn btn-warning"  onclick="close_defective_function()" id="close_defective" style="display: none">закрыть</button>
                        {{--<button type="button" class="btn btn-danger"  onclick="delete_defective_function()" id="delete_defective" style="display: none">удалить заявку</button>--}}
                    </form>
                </div>
            </div>

        </div>
    </div>


    {{--edit--}}
    <div id="edit_defective_modal" class="modal fade" role="dialog">
        <div class="modal-dialog"  style="width: 30%">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    изменить
                    <button type="button" class="close" data-dismiss="modal" id="close_detail_modal"><span style="font-size: 24px">&times;</span></button>
                </div>
                <div class="alert alert-success" style="display:none; color: black">
                    <ul>

                    </ul>
                </div>
                <form action="" method="post">
                    <div class="modal-body">
                        <input type="hidden" name="edit_defective_id" id="edit_defective_id">
                        <label for="description" style="font-size: 25px; font-weight: bold">примечание</label>
                        <textarea name="description" id="description"  rows="10" style="resize: none" class="form-control"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" onclick="update_defective()"><i class="fa fa-save"></i> сохранить</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <div id="edit_defective_item_modal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm" style="width: 20%">
            <div class="modal-content" >
                <div class="modal-header">
                    изменить кол-во
                    <button type="button" class="close" data-dismiss="modal" style="font-size: 38px" id="close_edit_defective_item_amount_modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-success" style="display:none; color: black">
                        <ul>

                        </ul>
                    </div>
                    <label for="edit_list_amount">кол-во</label>
                    <input type="number" name="amount" id="edit_list_amount" step="0.0001" class="form-control">
                    <input type="hidden" name="defective_item_id" id="defective_item_id" value="">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-warning" onclick="update_defective_list()"><i class="fa fa-save"></i> Сохранить</button>
                </div>
            </div>
        </div>
    </div>
</div>
@section('additionalLibrary')
    <script>
        $(document).ready(function() {
            var url_text = window.location.href;
            var url = new URL(url_text);
            var id = url.searchParams.get('id');
            $('#loading').hide();
            console.log(id)
            var oTable = $('#defective_table').dataTable();
            fetch_data();
            if (id!=null){
                oTable.fnFilter( id );
            }
            else {
                oTable.fnFilter('');
            }
            $('table.display').DataTable();

            $('#defective_table').on('dblclick', 'tr', function (e) {
                var id = $(this).data('id');
                $('#show_detail').modal("show");
                show_detail(id);
            });
            $('#defective_table tbody').on( 'click', 'tr', function () {
                $(this).removeClass('selected');

                if ( $(this).hasClass('selected') ) {
                    $(this).removeClass('selected');
                }
                else {
                    $('#defective_table').DataTable().$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            } );
        } );

        function fetch_data() {
            $("#defective_table").DataTable().destroy();
            var table = $('#defective_table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    "url": "{{ route('get_defectives') }}",
                    "type": "post"
                },
                columns: [
                    { "width": "1%", "data": "id" },
                    { "width": "1%", "data": "state" },
                    { "width": "4%", "data": "create_time" },
                    { "width": "10%", "data": "from_storage" },
                    { "width": "4%", "data": "to_storage" },
                    { "width": "10%", "data": "description" },
                    { "width": "1%", "data": "applier" },
                ],
                pageLength:100,
                lengthMenu:[[100, 500, 1000], [100, 500, 1000]],
                stateSave:true,
                order: [[ 2, "desc" ]],
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "Отображение от _START_ до _END_ из _TOTAL_ записей",
                    "infoEmpty": "",
                    "infoFiltered": "(отфильтровано из _MAX_ записей)",
                    "search": "<i class='fa fa-search' style='float: left'></i>",
                    "paginate": {
                        "previous": "<i class='fa fa-angle-left'></i>",
                        "next": "<i class='fa fa-angle-right'></i>",
                    }
                },
                createdRow: function( row, data, dataIndex ) {
                    $( row ).find('td:eq(6)').attr('nowrap', 'nowrap');
                    $(row).attr('data-id', data.id);
                },
                drawCallback: function (data) {
                    this.api().columns([1,3,4,6]).every( function () {
                        var column = this;
                        var select = $('<select><option value="">--все--</option></select>')
                            .appendTo( $(column.footer()).empty() )
                            .on( 'change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val, true, false )
                                    .draw();
                            } );

                        console.log(data.json)
                        for (var i=0; i<data.json.filteredData[column[0][0]].length; i++){
                            if (data.json.filters[column[0][0]] === data.json.filteredData[column[0][0]][i]['name']) {
                                select.append( '<option value="'+data.json.filteredData[column[0][0]][i]['name']+'" selected>'+data.json.filteredData[column[0][0]][i]['name']+'</option>' )
                            }else {
                                select.append( '<option value="'+data.json.filteredData[column[0][0]][i]['name']+'">'+data.json.filteredData[column[0][0]][i]['name']+'</option>' )
                            }
                        }
                    } );
                },
                scrollY: "60vh",
            });

        }

        function show_detail(defective_id) {
            $('#export_to_pdf').attr('href', '{{ \Illuminate\Support\Facades\URL::route('defective_invoice_generate') }}/'+defective_id);
            $('#loading').show();
            $('#defective_id').val(defective_id);
            $('.alert-danger').hide();
            $('.alert-info').hide();
            $('.alert-success').hide();
            $('#show_detail').on('shown.bs.modal', function() {
                fetch_detail_table();
            });
        }

        function fetch_detail_table() {
            var defective_id = $('#defective_id').val();
            $('#detail_table').DataTable().destroy();
            var table = $('#detail_table').DataTable({
                ajax: {
                    "url": "{{ route('get_defective_list') }}",
                    "type": "get",
                    "data": {defective_id:defective_id},
                    "dataSrc": function (data) {
                        console.log(data['hasPerm'])
                        if (!data['hasPerm']) {
                            $('#delete_defective').hide();
                            $('#close_defective').hide();
                        }else{
                            if (data['requestData'].state==1){
                                $('#delete_defective').show();
                                $('#close_defective').show();
                            } else {
                                $('#delete_defective').hide();
                                $('#close_defective').hide();
                            }
                        }

                        if (data['requestData'].state==1){
                            $('#accepted').text('№'+defective_id).css('color', 'blue');
                        } else {
                            $('#accepted').text('№'+defective_id).css('color', 'red');
                        }
                        $('#loading').hide();
                        $(table.column(4).footer()).text(' итог: '+data['total']);

                        return data.data;
                    }
                },
                language: {
                    "lengthMenu": "_MENU_",
                    "zeroRecords": "ничего не найдено",
                    "info": "_TOTAL_ записей",
                    "infoEmpty": "нет записей",
                    "infoFiltered": "(_MAX_)",
                    "search": "<i class='fa fa-search' style='float: left'></i>"
                },
                columnDefs: [
                    { orderable: false, targets: [8] }
                ],
                columns:[
                    {'data':'articula_new', 'width':'10%'},
                    {'data':'articula_old', 'width':'10%'},
                    {'data':'item_name', 'width':'30%'},
                    {'data':'sap_code', 'width':'10%'},
                    {'data':'amount', 'width':'6%'},
                    {'data':'accepted_amount', 'width':'6%'},
                    {'data':'remaining', 'width':'10%'},
                    {'data':'unit', 'width':'6%'},
                    {'data':'edit_btn', 'width':'4%'},
                ],
                paging: false,
                bPaginate: false,
                dom: 'Blfrtip',
                buttons: [
                    {
                        text: '<i class="glyphicon glyphicon-print" style="font-size: 18px"></i>',
                        action: function ( e, dt, node, config ) {
                            window.open(document.getElementById('export_to_pdf').href, '_blank');
                        }

                    },
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [ 0, 1, 2, 3, 4, 5, 6, 7]
                        },
                        text: '<i class="fa fa-file-excel-o"></i>'

                    }
                ],
                scrollY: "50vh",

            });
        }

        function delete_defective_function(){
            if (confirm('удалить')) {
                var defective_id = $('#defective_id').val()
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'POST',
                    url:"{{route('delete_defective')}}",
                    data:{defective_id:defective_id},
                    success: function (data) {
                        $('#close_detail_modal').click();
                        fetch_data();
                        $('.alert-danger').show();
                        $('.alert-danger ul').empty().append('<li>удален</li>');
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText)
                        console.log(json)
                        $('.alert-info').show();
                        $('.alert-info ul').empty();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').append('<li>'+value+'</li>');
                        })
                    }
                })
            }
        }

        function close_defective_function() {
            if (confirm('закрыть')) {
                var defective_id = $('#defective_id').val()
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type:'POST',
                    url:"{{route('close_defective')}}",
                    data:{defective_id:defective_id},
                    success: function (data) {
                        $('#close_detail_modal').click();
                        fetch_data();
                        $('.alert-danger').show();
                        $('.alert-danger ul').empty().append('<li>закрыт</li>');
                    },
                    error: function (request, status, error) {
                        var json = $.parseJSON(request.responseText)
                        console.log(json)
                        $('.alert-info').show();
                        $('.alert-info ul').empty();
                        $.each(json.errors, function (index, value) {
                            $('.alert-info ul').append('<li>'+value+'</li>');
                        })
                    }
                })
            }
        }

        function set_data_for_edit_request(defective_id, description) {
            $('#edit_defective_id').val(defective_id);
            $('textarea#description').val(description);
        }

        function update_defective() {
            var defective_id = $('#edit_defective_id').val()
            var desc = $('textarea#description').val()
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: "{{ route('update_defective') }}",
                type: "post",
                data: {defective_id:defective_id, description:desc},
                success: function (data) {
                    fetch_data();
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    $('.alert-danger').show();
                    $.each(json.errors, function (index, value) {
                        $('.alert-danger ul').empty().append('<li>'+value+'</li>');
                    })
                }
            })
        }

        function update_defective_list() {
            var id = $('#defective_item_id').val();
            var amount = $('#edit_list_amount').val();
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: "{{ route('update_defective_list') }}",
                type: "post",
                data: {defective_list_id:id, amount:amount},
                success: function (data) {
                    $('#close_edit_amount_modal').click();
                    fetch_detail_table();
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    $('.alert-danger').show();
                    $.each(json.errors, function (index, value) {
                        $('.alert-danger ul').empty().append('<li>'+value+'</li>');
                    })
                }
            })
        }

        function set_data_for_edit_defective_item(id, amount) {
            $('#edit_list_amount').val(amount);
            $('#defective_item_id').val(id);
        }

        function remove_defective_item(id) {
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: "{{ route('remove_defective_list') }}",
                type: "post",
                data: {defective_list_id:id},
                success: function (data) {
                    fetch_detail_table();
                    fetch_data();
                },
                error: function (request, status, error) {
                    var json = $.parseJSON(request.responseText);
                    $('.alert-danger').show();
                    $.each(json.errors, function (index, value) {
                        $('.alert-danger ul').empty().append('<li>'+value+'</li>');
                    })
                }
            })
        }
    </script>
@endsection
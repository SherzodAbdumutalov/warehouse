<?php

Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/changePassword','HomeController@showChangePasswordForm')->name('changePasswordPage');
Route::post('/changePassword','HomeController@changePassword')->name('changePassword');
Auth::routes();
Route::post('/import', 'ImportExportController@import')->name('import');
Route::post('/import_uchet', 'ImportExportController@import_uchet')->name('import_uchet');
Route::group(['prefix'=>'home', 'middleware'=>'auth'], function (){

    Route::get('/', 'HomeController@index')->name('home');

    Route::get('/pdf_remaining', 'HomeController@pdf_remaining')->name('pdf_remaining');
    Route::get('/pdf_remaining/generate/{request_id?}', 'HomeController@pdf_remaining_generate')->name('pdf_remaining_generate');
    Route::get('/pdf_remaining/pdf_remaining_generate_by_history/{transaction_id?}', 'HomeController@pdf_remaining_generate_by_history')->name('pdf_remaining_generate_by_history');

    Route::group(['prefix'=>'items'], function(){

        Route::get('/', 'DreamItemController@index')->name('items');
        Route::post('/getItems', 'DreamItemController@getItems')->name('getItems');
        Route::post('/getItemNormItems', 'DreamItemController@getItemNormItems')->name('getItemNormItems');
        Route::post('/getRawOfModel', 'DreamItemController@getRawOfModel')->name('getRawOfModel');

        Route::post('/exportDetailedItems', "DreamItemController@exportDetailedItems")->name("exportDetailedItems");
        Route::post('/exportDetailedItemsNorm',"DreamItemController@exportDetailedItemsNorm")->name("exportDetailedItemsNorm");

        Route::get('/item_import_page/{parentId}', 'ItemImportController@importExportView')->name('show_import_page');
        Route::post('import-file', 'ItemImportController@importFile')->name('import.file');

        Route::post('upload-file', 'ItemImportController@uploadExcelFile')->name('upload.file');

        Route::post('/deleteItemPhoto', 'DreamItemController@deleteItemPhoto')->name('deleteItemPhoto');
        Route::post('set_required', 'DreamItemController@set_required')->name('set_required');
        Route::post('add_replacement_items', 'DreamItemController@add_replacement_items')->name('add_replacement_items');
        Route::post('delete_replacement_items', 'DreamItemController@delete_replacement_items')->name('delete_replacement_items');


        Route::post('/create_item', 'DreamItemController@createItem')->name('create_item');
        Route::post('/get_item_values', 'DreamItemController@getItemValues')->name('get_item_values');
        Route::get('/getReplacementItems', 'DreamItemController@getReplacementItems')->name('getReplacementItems');
        Route::post('/update_item', 'DreamItemController@updateItem')->name('update_item');
        Route::post('/destroy', 'DreamItemController@destroyItem')->name('delete_item');
        Route::post('/delete_file', 'DreamItemController@delete_file')->name('delete_item_file');


        Route::post('/add_item_to_norm', 'DreamItemController@add_item_to_norm')->name('add_item_to_norm');
        Route::get('/get_items_for_adding_to_norm', 'DreamItemController@get_items_for_adding_to_norm')->name('get_items_for_adding_to_norm');
        Route::get('/get_item_units', 'DreamItemController@get_item_units')->name('get_item_units');
        Route::get('/get_item_child_values', 'DreamItemController@get_item_child_values')->name('get_item_child_values');
        Route::post('/updateChildItem', 'DreamItemController@updateChild')->name('update_child_item');
        Route::post('/destroyChild', 'DreamItemController@destroyChildItem')->name('delete_child_item');
        Route::post('/delete_norm', 'DreamItemController@delete_norm')->name('delete_norm');

        Route::post('/filter_items_by_checkbox', 'DreamItemController@filter_items_by_checkbox')->name('filter_items_by_checkbox');

        Route::post('/upload_item_file', 'DreamItemController@upload_item_file')->name('upload_item_file');

        Route::get('/child_item_import_page/{id}', 'ImportExportController@show_child_item_import_page')->name('show_child_item_import_page');
        Route::post('/upload_child_item', 'ImportExportController@uploadExcelFile')->name('upload_child_items');
        Route::post('/import_child_item', 'ImportExportController@importExcelFile')->name('import_child_items');

        Route::get('/show_remaining', 'DreamItemController@show_remaining')->name('show_remaining');
        Route::post('/show_remaining_by_storage', 'DreamItemController@show_remaining_by_storage')->name('show_remaining_by_storage');
        Route::post('/add_parent_item_unit', 'DreamItemController@add_parent_item_unit')->name('add_parent_item_unit');

        Route::post('/add_quantity_for_child_item', 'DreamItemController@add_quantity_for_child_item')->name('add_quantity_for_child_item');
        Route::get('/normArchive', 'DreamItemController@normArchive')->name('normArchive');
        Route::post('/getNormArchive', 'DreamItemController@getNormArchive')->name('getNormArchive');
        Route::post('/replaceNormItem', 'DreamItemController@replaceNormItem')->name('replaceNormItem');

        Route::get('/articula_generate', function(){
            return view('index', ['page'=>'items_page_includes.articula_generate']);
        })->name('articula_generate');

        Route::post('/generate_articula', 'DreamItemController@generate_articula')->name('generate_articula');
        Route::post('/searchModelByDetail', 'DreamItemController@searchModelByDetail')->name('searchModelByDetail');
        Route::post('/getItemSuppliers', 'DreamItemController@getItemSuppliers')->name('getItemSuppliers');
    });


    Route::group(['prefix'=>'storage'], function (){

        Route::get('/getOperationsByStorage', 'DreamStorageController@getOperationsByStorage')->name('getOperationsByStorage');

        Route::get('/getFilterItemsByStorage', 'DreamStorageController@getFilterItemsByStorage')->name('getFilterItemsByStorage');
        Route::get('/storageInputOutputOperation', 'DreamStorageController@storageInputOutputOperation')->name('storageInputOutputOperation');
        Route::get('/lineInputOutputOperation', 'DreamStorageController@lineInputOutputOperation')->name('lineInputOutputOperation');

        Route::get('/getLineItems', 'DreamStorageController@getLineItems')->name('getLineItems');
        Route::post('/collect_line_item_input_output', 'DreamStorageController@collect_line_item_input_output')->name('collect_line_item_input_output');
        Route::post('/save_line_inputs_outputs_items', 'DreamStorageController@save_line_inputs_outputs_items')->name('save_line_inputs_outputs_items');

        Route::get('/getMovingItems', 'DreamStorageController@getMovingItems')->name('getMovingItems');
        Route::post('/saveToSession', 'DreamStorageController@add_item')->name('collect_item_input_output');
        Route::post('/save_inputs_outputs_items', 'DreamStorageController@save_inputs_outputs_items')->name('save_inputs_outputs_items');

        Route::post('/save_items_storage', 'DreamStorageController@save_items_storage')->name('save_storage_item');
        Route::post('/getStorageItems', 'DreamStorageController@getStorageItems')->name('getStorageItems');
        Route::post('/update_unit', 'DreamStorageController@update_unit')->name('update_unit');
        Route::get('all_storage_items', 'DreamStorageController@show_all_storage_items')->name('all_storage_items');

        Route::get('/showInsideInvoices', 'DreamStorageController@showInsideInvoices')->name('showInsideInvoices');
        Route::get('/getInsideInvoices', 'DreamStorageController@getInsideInvoices')->name('getInsideInvoices');
        Route::get('/getInsideModelsInvoices', 'DreamStorageController@getInsideModelsInvoices')->name('getInsideModelsInvoices');
        Route::post('/printInsideInvoices', 'DreamStorageController@printInsideInvoices')->name('printInsideInvoices');

        Route::get('/import_storage_items', 'DreamStorageController@import_storage_items')->name('import_storage_items');
        Route::post('/upload_import_storage_items_file', 'DreamStorageController@upload_import_storage_items_file')->name('upload_import_storage_items_file');
        Route::post('/import_validate_storage_items_file', 'DreamStorageController@import_validate_storage_items_file')->name('import_validate_storage_items_file');

        Route::group(['prefix'=>'production_line'], function (){
            Route::get('/all_production_line', 'DreamStorageController@show_production_line')->name('show_production_line');
            Route::get('/show_production_line_items_list', 'DreamStorageController@show_production_line_items_list')->name('show_production_line_items_list');
            Route::get('/get_production_line_items', 'DreamStorageController@get_production_line_items')->name('get_production_line_items');
            Route::post('/update_line', 'DreamStorageController@update_line')->name('update_line');
            Route::post('/delete_line/{id}', 'DreamStorageController@delete_line')->name('delete_line');
            Route::post('/create_line', 'DreamStorageController@create_line')->name('create_line');
            Route::post('/delete_production_list_item', 'DreamStorageController@delete_production_list_item')->name('delete_production_list_item');
            Route::post('/add_production_line_item_list', 'DreamStorageController@add_production_line_item_list')->name('add_production_line_item_list');
            Route::post('/upload_production_line_items_file', 'DreamStorageController@upload_production_line_items_file')->name('upload_production_line_items_file');
            Route::post('/delete_all_line_items', 'DreamStorageController@delete_all_line_items')->name('delete_all_line_items');

        });

    });

    Route::group(['prefix'=>'transaction'], function(){
        Route::post('/getTransactions', 'DreamTransactionController@getTransactions')->name('getTransactions');
        Route::get('/transaction', 'DreamTransactionController@showTransaction')->name('showTransaction');
        Route::post('/getTransactionDetail', 'DreamTransactionController@getTransactionDetail')->name('getTransactionDetail');

        Route::post('/getTransactionsStatistics', 'DreamTransactionController@getTransactionsStatistics')->name('getTransactionsStatistics');
        Route::get('/getStatisticsColumns', 'DreamTransactionController@getStatisticsColumns')->name('getStatisticsColumns');

        Route::get('/edit_transaction_list', 'DreamTransactionController@edit_transaction_list')->name('edit_transaction_list');
        Route::post('/add_transaction_description', 'DreamTransactionController@add_transaction_description')->name('add_transaction_description');
        Route::post('/delete_transaction_list_item', 'DreamTransactionController@delete_transaction_list_item')->name('delete_transaction_list_item');

        Route::post('/delete_transaction', 'DreamTransactionController@delete_transaction')->name('delete_transaction');
        Route::post('/update_transaction_list_amount', 'DreamTransactionController@update_transaction_list_amount')->name('update_transaction_list_amount');
        Route::post('/clear_session_data', 'DreamTransactionController@clear_session_data')->name('clear_session_data');

        Route::post('/upload_transaction_list_file', 'DreamTransactionController@upload_transaction_list_file')->name('upload_transaction_list_file');

        Route::post('/delete_transaction_file', 'DreamTransactionController@delete_transaction_file')->name('delete_transaction_file');

        Route::get('show_import_transactions_page', 'DreamTransactionController@show_import_transactions_page')->name('show_import_transactions_page');
        Route::post('upload_inventarisation_file', 'DreamTransactionController@upload_inventarisation_file')->name('upload_inventarisation_file');
        Route::post('/import_transaction', 'DreamTransactionController@import_transaction')->name('import_transaction');

        Route::get('/get_transactions_by_storage', 'DreamTransactionController@get_transactions_by_storage')->name('get_transactions_by_storage');
        Route::get('/filterOfFilters', 'DreamTransactionController@filterOfFilters')->name('filterOfFilters');
    });

    Route::group(['prefix'=>'request'], function(){

        Route::get('/request', 'DreamRequestController@index')->name('request');
        Route::post('/get_requests_belongs_storage', 'DreamRequestController@get_requests_belongs_storage')->name('get_requests_belongs_storage');
        Route::post('/add_request', 'DreamRequestController@add_request')->name('add_request');
        Route::post('/update_request', 'DreamRequestController@update_request')->name('update_request');

        Route::get('/changeFromStorageRemaining', 'DreamRequestController@changeFromStorageRemaining')->name('changeFromStorageRemaining');

        Route::post('/register_request', 'DreamRequestController@register_request')->name('register_request');
        Route::post('/accept_request', 'DreamRequestController@accept_request')->name('accept_request');

        Route::get('/requestItemsByStorage', 'DreamRequestController@requestItemsByStorage')->name('requestItemsByStorage');
        Route::get('/show_requests', 'DreamRequestController@show_requests_page')->name('show_requests_page');
        Route::post('/get_requests', 'DreamRequestController@get_requests')->name('get_requests');
        Route::get('/all_requests', 'DreamRequestController@all_requests')->name('all_requests');
        Route::get('/get_request_list_items', 'DreamRequestController@get_request_list_items')->name('get_request_list_items');
        Route::get('/get_request_list_items_by_storage', 'DreamRequestController@get_request_list_items_by_storage')->name('get_request_list_items_by_storage');
        Route::get('/get_accepted_request_list_history', 'DreamRequestController@get_accepted_request_list_history')->name('get_accepted_request_list_history');
        Route::post('/delete_request', 'DreamRequestController@delete_request')->name('delete_request');
        Route::post('/close_request', 'DreamRequestController@close_request')->name('close_request');
        Route::get('/replaceItems', 'DreamRequestController@replaceItems')->name('replaceItems');

        Route::get('/getNotAcceptedRequestsAmount', 'DreamRequestController@getNotAcceptedRequestsAmount')->name('getNotAcceptedRequestsAmount');
    });

    Route::group(['prefix'=>'defective'], function(){

        Route::get('/defective', 'DreamDefectiveController@index')->name('defective');
        Route::get('/create_defective', 'DreamDefectiveController@create_defective')->name('create_defective');
        Route::get('/get_defective_list', 'DreamDefectiveController@get_defective_list')->name('get_defective_list');
        Route::get('/showDefectiveListItemsForAccept', 'DreamDefectiveController@showDefectiveListItemsForAccept')->name('showDefectiveListItemsForAccept');
        Route::post('/getDefectivesForAccept', 'DreamDefectiveController@getDefectivesForAccept')->name('getDefectivesForAccept');
        Route::post('/get_defectives', 'DreamDefectiveController@get_defectives')->name('get_defectives');
        Route::get('/getStorageDefectiveItems', 'DreamDefectiveController@getStorageDefectiveItems')->name('getStorageDefectiveItems');

        Route::get('/defectiveInputOutputOperation', 'DreamDefectiveController@defectiveInputOutputOperation')->name('defectiveInputOutputOperation');
        Route::post('/collect_defective_item_input_output', 'DreamDefectiveController@collect_defective_item_input_output')->name('collect_defective_item_input_output');
        Route::post('/save_defective_inputs_outputs_items', 'DreamDefectiveController@save_defective_inputs_outputs_items')->name('save_defective_inputs_outputs_items');
        Route::get('/getDefectiveStorageRemainingItems', 'DreamDefectiveController@getDefectiveStorageRemainingItems')->name('getDefectiveStorageRemainingItems');

        Route::post('/collectDefectiveItems', 'DreamDefectiveController@collectDefectiveItems')->name('collect_defective_items');
        Route::post('/saveDefectiveItems', 'DreamDefectiveController@saveDefectiveItems')->name('saveDefectiveItems');
        Route::post('/update_defective', 'DreamDefectiveController@update_defective')->name('update_defective');
        Route::post('/update_defective_list', 'DreamDefectiveController@update_defective_list')->name('update_defective_list');
        Route::post('/remove_defective_list', 'DreamDefectiveController@remove_defective_list')->name('remove_defective_list');
        Route::post('/accept_defective_items', 'DreamDefectiveController@accept_defective_items')->name('accept_defective_items');
        Route::post('/delete_defective', 'DreamDefectiveController@delete_defective')->name('delete_defective');
        Route::post('/close_defective', 'DreamDefectiveController@close_defective')->name('close_defective');
        Route::get('/defective_invoice_generate/generate/{defective_id?}', 'DreamDefectiveController@defective_invoice_generate')->name('defective_invoice_generate');
    });

    Route::group(['prefix'=>'return'], function(){

        Route::get('/returns', 'DreamReturnController@index')->name('returns');
        Route::get('/create_returns', 'DreamReturnController@create_returns')->name('create_returns');
        Route::get('/getReturnList', 'DreamReturnController@getReturnList')->name('getReturnList');
        Route::post('/get_returns', 'DreamReturnController@getReturns')->name('get_returns');
        Route::get('/showReturnsListItemsForAccept', 'DreamReturnController@showreturnsListItemsForAccept')->name('showReturnsListItemsForAccept');
        Route::get('/returnsInputOutputOperation', 'DreamReturnController@returnsInputOutputOperation')->name('returnsInputOutputOperation');
        Route::post('/getReturnsForAccept', 'DreamReturnController@getReturnsForAccept')->name('getReturnsForAccept');
        Route::get('/getStorageReturnsItems', 'DreamReturnController@getStorageReturnsItems')->name('getStorageReturnsItems');
        Route::post('/collectReturnsItems', 'DreamReturnController@collectReturnsItems')->name('collectReturnsItems');
        Route::post('/saveReturnsItems', 'DreamReturnController@saveReturnsItems')->name('saveReturnsItems');

        Route::post('/update_returns', 'DreamReturnController@update_returns')->name('update_returns');
        Route::post('/update_returns_list', 'DreamReturnController@update_returns_list')->name('update_returns_list');
        Route::post('/accept_returns_items', 'DreamReturnController@accept_returns_items')->name('accept_returns_items');
        Route::post('/remove_returns_list', 'DreamReturnController@remove_returns_list')->name('remove_returns_list');
        Route::post('/delete_returns', 'DreamReturnController@delete_returns')->name('delete_returns');
        Route::post('/close_returns', 'DreamReturnController@close_returns')->name('close_returns');

        Route::get('/returns_invoice/generate/{returns_id?}', 'DreamReturnController@returns_invoice_generate')->name('returns_invoice_generate');

    });

    Route::group(['prefix'=>'movingRequest'], function(){

        Route::get('/movingBetweenStorages', 'DreamMovingRequestController@index')->name('movingBetweenStorages');
        Route::post('/getMovingRequest', 'DreamMovingRequestController@getMovingRequest')->name('getMovingRequest');
        Route::get('/createMovingRequest', 'DreamMovingRequestController@createMovingRequest')->name('createMovingRequest');
        Route::get('/getStorageMovingRequestItems', 'DreamMovingRequestController@getStorageMovingRequestItems')->name('getStorageMovingRequestItems');
        Route::get('/getMovingRequestList', 'DreamMovingRequestController@getMovingRequestList')->name('getMovingRequestList');
        Route::post('/collectMovingRequestItems', 'DreamMovingRequestController@collectMovingRequestItems')->name('collectMovingRequestItems');
        Route::post('/saveMovingRequestItems', 'DreamMovingRequestController@saveMovingRequestItems')->name('saveMovingRequestItems');
        Route::post('/saveMovingRequestItemsByStorage', 'DreamMovingRequestController@saveMovingRequestItemsByStorage')->name('saveMovingRequestItemsByStorage');
        Route::post('/deleteMovingRequest', 'DreamMovingRequestController@deleteMovingRequest')->name('deleteMovingRequest');
        Route::post('/closeMovingRequest', 'DreamMovingRequestController@closeMovingRequest')->name('closeMovingRequest');
        Route::post('/updateMovingRequest', 'DreamMovingRequestController@updateMovingRequest')->name('updateMovingRequest');
        Route::post('/updateMovingRequestList', 'DreamMovingRequestController@updateMovingRequestList')->name('updateMovingRequestList');
        Route::post('/deleteMovingRequestList', 'DreamMovingRequestController@deleteMovingRequestList')->name('deleteMovingRequestList');
        Route::post('/getMovingRequestsForAccept', 'DreamMovingRequestController@getMovingRequestsForAccept')->name('getMovingRequestsForAccept');
        Route::post('/acceptMovingRequestItems', 'DreamMovingRequestController@acceptMovingRequestItems')->name('acceptMovingRequestItems');

        Route::post('/collectItemsByStorageForMMovingRequest', 'DreamMovingRequestController@collectItemsByStorageForMMovingRequest')->name('collectItemsByStorageForMMovingRequest');
        Route::get('/showMovingRequestListItemsForAccept', 'DreamMovingRequestController@showMovingRequestListItemsForAccept')->name('showMovingRequestListItemsForAccept');

        Route::get('/moving_requests_invoice/generate/{moving_request_id?}', 'DreamMovingRequestController@moving_requests_invoice_generate')->name('moving_requests_invoice_generate');
    });

    Route::group(['prefix'=>'returning'], function(){

        Route::get('/returning', 'DreamDefectiveController@returning')->name('returning');
        Route::post('/update_defective', 'DreamDefectiveController@update_defective')->name('update_defective');
        Route::post('/save_defective_items', 'DreamDefectiveController@save_defective_items')->name('save_defective_items');
    });

    Route::group(['prefix'=>'user'], function (){

        Route::get('/index', 'UserController@index')->name('users');
        Route::get('/getUsers', 'UserController@getUsers')->name('getUsers');
        Route::get('/getUserValues', 'UserController@getUserValues')->name('getUserValues');
        Route::post('/createUser', 'UserController@createUser')->name('createUser');
        Route::post('/updateUser', 'UserController@updateUser')->name('updateUser');
        Route::post('/deleteUser', 'UserController@deleteUser')->name('deleteUser');

        Route::get('/permissions', 'PermissionController@index')->name('permissions');
        Route::post('/save_perm', 'PermissionController@store')->name('save_perm');

        Route::get('/inventarization', 'UserController@inventarization')->name('inventarization');

    });

    Route::group(['prefix'=>'plans'], function (){
        Route::get('/show_plan', 'DreamPlanController@index')->name('show_plan');
        Route::get('/getPlanTablesHeaderColumns', 'DreamPlanController@getPlanTablesHeaderColumns')->name('getPlanTablesHeaderColumns');
        Route::post('/get_plan_by_month', 'DreamPlanController@get_plan_by_month')->name('get_plan_by_month');
        Route::post('/save_monthly_plan', 'DreamPlanController@save_monthly_plan')->name('save_monthly_plan');
        Route::post('/get_plan_statistics', 'DreamPlanController@getPlanStatistics')->name('get_plan_statistics');
        Route::post('/show_detail', 'DreamPlanController@planByModal')->name('plan_show_detail');
        Route::post('/set_plan_values', 'DreamPlanController@set_plan_values')->name('set_plan_values');
        Route::post('/upload_plan_file', 'DreamPlanController@upload_plan_file')->name('upload_plan_file');
        Route::post('/import_plan', 'DreamPlanController@import_plan')->name('import_plan');
        Route::get('/show_import_plan_page', 'DreamPlanController@show_import_plan_page')->name('show_import_plan_page');

        Route::group(['prefix'=>'daily_plans'], function (){

            Route::get('/show_daily_plan', 'DreamPlanController@dailyPlan')->name('show_daily_plan');
            Route::post('/get_daily_plan', 'DreamPlanController@get_daily_plan')->name('get_daily_plan');
            Route::post('/getDailyPlanStatistics', 'DreamPlanController@getDailyPlanStatistics')->name('getDailyPlanStatistics');
            Route::post('/save_daily_plan', 'DreamPlanController@save_daily_plan')->name('save_daily_plan');
            Route::post('/set_daily_plan_values', 'DreamPlanController@set_daily_plan_values')->name('set_daily_plan_values');
            Route::post('/showPlanAddModal', 'DreamPlanController@showPlanAddModal')->name('showPlanAddModal');
        });
    });

    Route::group(['prefix'=>'order'], function (){
        Route::get('/orders_page', 'DreamOrderController@orders_page')->name('orders_page');
        Route::post('/all_orders', 'DreamOrderController@all_orders')->name('all_orders');
        Route::post('/delete_order', 'DreamOrderController@delete_order')->name('delete_order');
        Route::get('/showOrderList', 'DreamOrderController@showOrderList')->name('showOrderList');
//        Route::get('/show_order_list_by_storage', 'DreamOrderController@show_order_list_by_storage')->name('show_order_list_by_storage');
//        Route::get('/items_data_ordering', 'DreamOrderController@items_data_ordering')->name('items_data_ordering');
        Route::get('/create_order', 'DreamOrderController@create_order')->name('create_order');
        Route::get('/editOrder', 'DreamOrderController@editOrder')->name('editOrder');
        Route::post('/updateOrder', 'DreamOrderController@updateOrder')->name('updateOrder');
        Route::post('/save_order', 'DreamOrderController@save_order')->name('save_order');
        Route::post('/add_item_to_exist_order', 'DreamOrderController@add_item_to_exist_order')->name('add_item_to_exist_order');
        Route::post('/save_brought_items', 'DreamOrderController@save_brought_items')->name('save_brought_items');
        Route::post('/updateOrderList', 'DreamOrderController@updateOrderList')->name('updateOrderList');
        Route::post('/delete_order_list_item', 'DreamOrderController@delete_order_list_item')->name('delete_order_list_item');
        Route::post('/close_order', 'DreamOrderController@close_order')->name('close_order');
        Route::post('/upload_order_file', 'DreamOrderController@upload_order_file')->name('upload_order_file');

        Route::get('/edit_order_list', 'DreamOrderController@editOrderList')->name('edit_order_list');
        Route::get('/getNotConsistedItems', 'DreamOrderController@getNotConsistedItems')->name('getNotConsistedItems');
        Route::get('/get_supplier_storage_items', 'DreamOrderController@get_supplier_storage_items')->name('get_supplier_storage_items');
        Route::get('/getSupplierModels', 'DreamOrderController@getSupplierModels')->name('getSupplierModels');

        Route::get('/get_supplier_items', 'DreamOrderController@get_supplier_items')->name('get_supplier_items');
        Route::get('/get_supplier_items_by_model', 'DreamOrderController@get_supplier_items_by_model')->name('get_supplier_items_by_model');
        Route::post('/get_orders_belongs_storage', 'DreamOrderController@get_orders_belongs_storage')->name('get_orders_belongs_storage');
        Route::post('/save_input_items_on_session', 'DreamOrderController@save_input_items_on_session')->name('save_input_items_on_session');
        Route::get('/getOrderPrice', 'DreamOrderController@getOrderPrice')->name('getOrderPrice');
        Route::post('/historyOfOrder', 'DreamOrderController@historyOfOrder')->name('historyOfOrder');

    });

    Route::group(['prefix'=>'logistics'], function (){
        Route::get('/inputToBuffer', 'DreamLogisticsController@inputToBuffer')->name('inputToBuffer');
        Route::get('/logistics', 'DreamLogisticsController@index')->name('logistics');
        Route::get('/createLogistics', 'DreamLogisticsController@createLogistics')->name('createLogistics');
        Route::post('/removeFromLogisticsList', 'DreamLogisticsController@removeFromLogisticsList')->name('removeFromLogisticsList');
        Route::post('/clearLogisticsList', 'DreamLogisticsController@clearLogisticsList')->name('clearLogisticsList');
        Route::post('/getItemsOfBase', 'DreamLogisticsController@getItemsOfBase')->name('getItemsOfBase');
        Route::post('/getLogisticsList', 'DreamLogisticsController@getLogisticsList')->name('getLogisticsList');
        Route::post('/logisticsList', 'DreamLogisticsController@logisticsList')->name('logisticsList');
        Route::post('/saveArrivedLogistics', 'DreamLogisticsController@saveArrivedLogistics')->name('saveArrivedLogistics');
        Route::post('/save_brought_items_to_buffer', 'DreamLogisticsController@save_brought_items_to_buffer')->name('save_brought_items_to_buffer');
        Route::post('/get_orders_belongs_buffer_storage', 'DreamLogisticsController@get_orders_belongs_buffer_storage')->name('get_orders_belongs_buffer_storage');
        Route::post('/save_input_items_on_session_for_buffer', 'DreamLogisticsController@save_input_items_on_session_for_buffer')->name('save_input_items_on_session_for_buffer');
        Route::get('/saveInputValueInSession', 'DreamLogisticsController@saveInputValueInSession')->name('saveInputValueInSession');
        Route::post('/getItemsByInvoiceToAdd', 'DreamLogisticsController@getItemsByInvoiceToAdd')->name('getItemsByInvoiceToAdd');
        Route::post('/allLogistics', 'DreamLogisticsController@allLogistics')->name('allLogistics');
        Route::get('/showLogisticsList', 'DreamLogisticsController@showLogisticsList')->name('showLogisticsList');
        Route::get('/printFormattedLogistics/{request_id?}/{logistics_date?}', "DreamLogisticsController@printFormattedLogistics")->name("printFormattedLogistics");
        Route::post('/updateLogisticsList', 'DreamLogisticsController@updateLogisticsList')->name('updateLogisticsList');
        Route::post('/deleteLogisticsListItem', 'DreamLogisticsController@deleteLogisticsListItem')->name('deleteLogisticsListItem');
        Route::post('/updateLogistics', 'DreamLogisticsController@updateLogistics')->name('updateLogistics');
        Route::get('/editLogistics', 'DreamLogisticsController@editLogistics')->name('editLogistics');
        Route::post('/deleteLogistics', 'DreamLogisticsController@deleteLogistics')->name('deleteLogistics');
        Route::post('/closeLogistics', 'DreamLogisticsController@closeLogistics')->name('closeLogistics');
        Route::post('/addItemToExistLogistics', 'DreamLogisticsController@addItemToExistLogistics')->name('addItemToExistLogistics');
        Route::post('/saveLogistics', 'DreamLogisticsController@saveLogistics')->name('saveLogistics');
        Route::post('/excelExport', 'DreamLogisticsController@excelExport')->name('excelExport');
        Route::get('/edit_logistics_list', 'DreamLogisticsController@editLogisticsList')->name('edit_logistics_list');
        Route::get('/getNotConsistedItemsOfLogistics', 'DreamLogisticsController@getNotConsistedItemsOfLogistics')->name('getNotConsistedItemsOfLogistics');
        Route::get('/getCurrentItemInvoice', 'DreamLogisticsController@getCurrentItemInvoice')->name('getCurrentItemInvoice');
        Route::post('/onTheWay', 'DreamLogisticsController@onTheWay')->name('onTheWay');
        Route::get('/historyPage', 'DreamLogisticsController@historyPage')->name('historyPage');

    });

    Route::group(['prefix'=>'supplier'], function (){
        Route::get('/all_suppliers', 'DreamSupplierController@show_suppliers')->name('show_suppliers');
        Route::get('/getSuppliers', 'DreamSupplierController@getSuppliers')->name('getSuppliers');
        Route::get('/editSupplier', 'DreamSupplierController@editSupplier')->name('editSupplier');
        Route::post('/deleteSupplier', 'DreamSupplierController@deleteSupplier')->name('deleteSupplier');
        Route::get('/show_supplier_items_list', 'DreamSupplierController@show_supplier_items_list')->name('show_supplier_items_list');
        Route::get('/get_fit_items', 'DreamSupplierController@get_fit_items')->name('get_fit_items');
        Route::post('/updateSupplier', 'DreamSupplierController@updateSupplier')->name('updateSupplier');
        Route::post('/createSupplier', 'DreamSupplierController@createSupplier')->name('createSupplier');
        Route::post('/delete_supplier_list_item', 'DreamSupplierController@delete_supplier_list_item')->name('delete_supplier_list_item');
        Route::post('/add_supplier_item_list', 'DreamSupplierController@add_supplier_item_list')->name('add_supplier_item_list');
        Route::post('/upload_supplier_items_file', 'DreamSupplierController@upload_supplier_items_file')->name('upload_supplier_items_file');
        Route::post('/delete_all_supplier_items', 'DreamSupplierController@delete_all_supplier_items')->name('delete_all_supplier_items');

    });
});
